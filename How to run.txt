Software Required
Java JDK 8
Tomcat 9.0.x
MongoDB 3.4.x
Robo 3T (Robomongo)
Maven 3.5.x
Gradle 3.5
Node 8.x
Git 2.x
Set the System Variables
  Right click the my computer/ This PC, select properties
  Select Advanced system setting in left side of the window
  Go to under system environment
  Set path for maven, gradle, MongoDB, jdk
  Create a new variable JAVA_HOME and set the path for jdk home.

Import project to IntelliJ IDEA from Git.
Select VCS in menu bar
Click Checkout from Version Control > Git
Provide Git Repository URL from crendentials
Module's name will be set as directory name for each module.
Select parent directory for your project
Hit clone button
Provide username and password when prompted
Click "yes" when it prompt to open your project file
Note: MissionControl is Maven Project and others are gradle projects
Here we required MissionControl and Platform only.
MongoDB Setup
To run MongoDB create directories data/db in the root of installation local disk(eg:- C:/data/db)
Run mongod.exe in MongoDB\Server\3.4\bin folder
Open robomongo client and goto File->Connect.
Create a new connection named 'rsndb' and save.
You are connected to the locally installed mongodb server.
Add mongodb path to the system environment variables.
Download the zip file mongo_dump.zip from google drive and unzip it to mongo_dump.
Go to  'mongo_dump' file location(unzipped)  using cmder
Then type following command to import database
mongorestore -d rsndb rsndb
Your local db should be up and running with a copy of data now
Generate data files
In cmder, go to git folder platform\data. Run the following command

    gradle clean assemble
Deploy the war through Intellij Idea
Open the MissionControl module in IntelliJ IDEA .
Go to File in the menubar -> new ->module from existing source
Select platform module  ->
 select import from external module, and also select gradle -> next ->  check autoimport and uncheck as a seperate module and go on.
Select the run/configuration from the drop down just before the run button.
From the drop down select edit configuration.

In pop up window press plus button on the top - left corner and select the tomcat server as local.

Configure the path of server which we are using  in application server field.

Set the vm options as -Dorg.quartz.properties=quartz-qa.properties -Dspring.profiles.active=local

Give http://localhost:8080 in the open browser field.

Switch to Deployment tab and press plus button from the right side, then select artifact .

Add the exploded war of two modules.

Set application context of gradle module as 'fourdotfive-rest'

Now apply the changes and press OK.

Now Debug the application through Intellij Idea and war will be deployed and localhost will open after successfull deployment.

UI setup
Open missioncontrolapp directory.(cloned from https://gitlab.com/4dot5/missioncontrolapp.git)
Run the following commands from missioncontrolapp directory.
npm install( installs necessary packages for node server).
npm install -g bower
bower install
npm install nodemon -g
npm install -g gulp (UI build tool)
In config/default.json file change 'apiProxyURL' to mission control api url (http://localhost:8080/)
gulp build
npm run dev:server (Serves the UI in node server on port 3000)
Open 'http://localhost:3000' in browser.