package TeamPerformanceByMesure;

import java.util.List;

import com.fourdotfive.missioncontrol.pojo.reports.EnumReportType;

public class TeamPerformanceByMember {
	
	private List<Integer> red;
	private List<Integer> green;
	private List<Integer> timeToFill;
	private String memberName;
	private String memberId;
	// Type of report
	private EnumReportType reportType;
	
	
	
	public List<Integer> getRed() {
		return red;
	}
	public void setRed(List<Integer> red) {
		this.red = red;
	}
	public List<Integer> getGreen() {
		return green;
	}
	public void setGreen(List<Integer> green) {
		this.green = green;
	}
	public List<Integer> getTimeToFill() {
		return timeToFill;
	}
	public void setTimeToFill(List<Integer> timeToFill) {
		this.timeToFill = timeToFill;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public EnumReportType getReportType() {
		return reportType;
	}
	public void setReportType(EnumReportType reportType) {
		this.reportType = reportType;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result+ ((memberId == null) ? 0 : memberId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj){
			return true;
		}
		if (obj == null){
			return false;
		}
		if (getClass() != obj.getClass()){
			return false;
		}
		TeamPerformanceByMember other = (TeamPerformanceByMember) obj;
		if (memberId == null) {
			if (other.memberId != null){
				return false;
			}
		} else if (!memberId.equals(other.memberId)){
			return false;
		}
		return true;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TeamPerformanceByMember [red=");
		builder.append(red);
		builder.append(", green=");
		builder.append(green);
		builder.append(", timeToFill=");
		builder.append(timeToFill);
		builder.append(", memberName=");
		builder.append(memberName);
		builder.append(", memberId=");
		builder.append(memberId);
		builder.append(", reportType=");
		builder.append(reportType);
		builder.append("]");
		return builder.toString();
	}
	
}
