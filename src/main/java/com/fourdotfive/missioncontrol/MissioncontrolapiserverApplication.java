package com.fourdotfive.missioncontrol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MissioncontrolapiserverApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(MissioncontrolapiserverApplication.class, args);
	}
	
	
}
