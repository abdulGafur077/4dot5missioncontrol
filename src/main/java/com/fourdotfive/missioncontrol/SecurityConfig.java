/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fourdotfive.missioncontrol;

import com.fourdotfive.missioncontrol.security.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;

/**
 * @author pradeepkm, shalini
 */
@Configuration
@EnableWebSecurity
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    public static final Logger LOGGER = LoggerFactory
            .getLogger(SecurityConfig.class);

    @Autowired
    private JwtAuthenticationProvider jwtAuthenticationProvider;

    @Autowired
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @Bean
    public JwtAuthProcessingFilter authenticationTokenFilterBean() {
        JwtAuthProcessingFilter filter = new JwtAuthProcessingFilter();
        try {
            filter.setRequiresAuthenticationRequestMatcher(new OrRequestMatcher(
                    new AntPathRequestMatcher("/api/**"),
                    new AntPathRequestMatcher("/register/**")
            ));
            filter.setAuthenticationManager(authenticationManagerBean());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        filter.setAuthenticationSuccessHandler(new JwtAuthenticationSuccessHandler());
        return filter;
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() {

        AuthenticationManager manager = null;
        try {
            manager = super.authenticationManagerBean();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

        return manager;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder authManagerBuilder) {
        LOGGER.info(
                "\n\n\n\nJWT NOTE: configuring authentication provider {}\n\n\n\n",
                this.jwtAuthenticationProvider);
        authManagerBuilder
                .authenticationProvider(this.jwtAuthenticationProvider);
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) {

        LOGGER.info(
                "\n\n\n\nJWT NOTE: authentication entry point {}, authentication provider {} \n\n\n\n",
                jwtAuthenticationEntryPoint, jwtAuthenticationProvider);

        try {
            httpSecurity
                    .csrf()
                    .disable()
                    .exceptionHandling()
                    .authenticationEntryPoint(jwtAuthenticationEntryPoint)
                    .and()
                    // dont create sessions
                    .sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                    .and().authorizeRequests().antMatchers("/").permitAll()
                    .antMatchers("/*.*").permitAll().antMatchers("/**/*.*")
                    .permitAll()
                    .antMatchers("/external/**").permitAll()
                    .antMatchers("/internal/**").permitAll()
                    .antMatchers("/auth/**").permitAll().anyRequest()
                    .authenticated();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

        httpSecurity.addFilterBefore(authenticationTokenFilterBean(),
                UsernamePasswordAuthenticationFilter.class);

        try {
            httpSecurity.headers().cacheControl();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }
}
