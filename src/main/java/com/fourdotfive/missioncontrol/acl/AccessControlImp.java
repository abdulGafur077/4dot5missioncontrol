package com.fourdotfive.missioncontrol.acl;

import com.fourdotfive.missioncontrol.common.AppConstants;
import com.fourdotfive.missioncontrol.security.FourDotFiveUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class AccessControlImp implements AccessControlService {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(AccessControlImp.class);

    @Autowired
    private SuperAdminAclServiceImp superAdminSerivce;

    @Autowired
    private CompAdminAclServiceImp adminSerivce;

    @Autowired
    private RecSeniorRecAclServiceImp recSerivce;

    @Autowired
    private RecManagerAclServiceImp recManagerSerivce;

    @Autowired
    private EmployeeAclServiceImp employeeService;

    @Autowired
    private FeatureAccessControlService featureAccessControlService;

    private AllAccessControlService canAccessUser(FourDotFiveUser currentUser) {

        Set<GrantedAuthority> authorities = (Set<GrantedAuthority>) currentUser
                .getAuthorities();

        for (GrantedAuthority authority : authorities) {
            switch (authority.getAuthority()) {
                case AppConstants.FOURDOT5ADMIN_ROLEID:

                    return superAdminSerivce;
                case AppConstants.SUPERUSER_ROLEID:

                    return superAdminSerivce;
                case AppConstants.CORPORATE_ADMIN_ROLEID:

                    return adminSerivce;
                case AppConstants.STAFFING_ADMIN_ROLEID:

                    return adminSerivce;
                case AppConstants.CORPORATE_REC_MANAGER_ROLEID:

                    return recManagerSerivce;
                case AppConstants.STAFFING_REC_MANAGER_ROLEID:

                    return recManagerSerivce;
                case AppConstants.CORPORATE_SRREC_ROLEID:

                    return recSerivce;
                case AppConstants.STAFFING_SRREC_ROLEID:

                    return recSerivce;
                case AppConstants.CORPORATE_REC_ROLEID:

                    return recSerivce;
                case AppConstants.STAFFING_REC_ROLEID:

                    return recSerivce;
                case AppConstants.EMPLOYEE_ROLEID:

                    return employeeService;
                case AppConstants.CLIENT_EMPLOYEE_ROLEID:

                    return employeeService;
                case AppConstants.HIRING_MANAGER_ROLEID:

                    return recManagerSerivce;
                default:
                    return null;
            }

        }
        return null;

    }

    @Override
    public boolean canGet4Dot5Admins(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canGet4Dot5Admins();
    }

    @Override
    public boolean canGetAllCompanies(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canGetAllCompanies();
    }

    @Override
    public boolean canSaveCompany(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canSaveCompany();
    }

    @Override
    public boolean canChangecompanystate(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canChangecompanystate();
    }

    @Override
    public boolean canAssignClientOrbu(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canAssignClientOrbu();
    }

    @Override
    public boolean canGetCompanyDetails(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canGetCompanyDetails();
    }

    @Override
    public boolean canDeleteClient(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canDeleteClient();
    }

    @Override
    public boolean canDeleteCompany(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canDeleteCompany();
    }

    @Override
    public boolean canGetClientsOrBu(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canGetClientsOrBu();
    }

    @Override
    public boolean canSetLicensePreferences(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canSetLicensePreferences();
    }

    @Override
    public boolean canGetLicensePreferences(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canGetLicensePreferences();
    }

    @Override
    public boolean canGetClientLicensePreferences(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canGetClientLicensePreferences();
    }

    @Override
    public boolean canGetRoleEntityScope(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canGetRoleEntityScope();
    }

    @Override
    public boolean canCreateRoleEntityScope(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canCreateRoleEntityScope();
    }

    @Override
    public boolean canUpdateRoleEntityScope(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canUpdateRoleEntityScope();
    }

    @Override
    public boolean canGetParentCompanyIndustryList(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canGetParentCompanyIndustryList();
    }

    @Override
    public boolean canGetChildCompanyIndustryList(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canGetChildCompanyIndustryList();
    }

    @Override
    public boolean canGetRoles(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canGetRoles();
    }

    @Override
    public boolean canGetUserEnitityScope(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canGetUserEnitityScope();
    }

    @Override
    public boolean canCheckRoleEnitityScope(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canCheckRoleEnitityScope();
    }

    @Override
    public boolean canGetTeamMembers(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canGetTeamMembers();
    }

    @Override
    public boolean canGetUserDetails(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canGetUserDetails();
    }

    @Override
    public boolean canSaveUser(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        Boolean canSave  = accessControlService != null && accessControlService.canSaveUser();
        if (canSave) {
            canSave = featureAccessControlService.isAddUserFeatureAvailable(currentUser);
        }
        return canSave;
    }

    @Override
    public boolean canAssignManager(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canAssignManager();
    }

    @Override
    public boolean canDeleteUser(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canDeleteUser();
    }

    @Override
    public boolean canGetReportees(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canGetReportees();
    }

    @Override
    public boolean canLoggedInUserDetails(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canLoggedInUserDetails();
    }

    @Override
    public boolean canAssignCompanyFor4dot5Admin(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canAssignCompanyFor4dot5Admin();
    }

    @Override
    public boolean canGetForwardRecipients(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canGetForwardRecipients();
    }

    @Override
    public boolean canGetPossibleManagersForUpdate(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canGetPossibleManagersForUpdate();
    }

    @Override
    public boolean canSetScope(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canSetScope();
    }

    @Override
    public boolean canSaveForwardRecipient(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canSaveForwardRecipient();
    }

    @Override
    public boolean canUpdateUser(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canUpdateUser();
    }

    @Override
    public boolean canSaveQuestionList(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canSaveQuestionList();
    }

    @Override
    public boolean canGetQuestionList(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canGetQuestionList();
    }

    @Override
    public boolean canGetFourDotFiveCompanyDetails(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canGetFourDotFiveCompanyDetails();
    }

    @Override
    public boolean canGetRecruiterScreeningQuestions(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canGetRecruiterScreeningQuestions();
    }


    @Override
    public boolean canSaveRecruiterScreeningQuestions(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canSaveRecruiterScreeningQuestions();
    }


    @Override
    public boolean canGetJobFile(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canGetJobFile();
    }

    @Override
    public boolean canSaveJobMatchState(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canSaveJobMatchState();
    }

    @Override
    public boolean canSyncAssessments(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canSyncAssessments();
    }

    @Override
    public boolean canViewReports(FourDotFiveUser currentUser) {
        AllAccessControlService accessControlService = canAccessUser(currentUser);
        return accessControlService != null && accessControlService.canViewReports();
    }
}
