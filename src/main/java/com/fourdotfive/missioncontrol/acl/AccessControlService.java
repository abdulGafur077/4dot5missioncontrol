package com.fourdotfive.missioncontrol.acl;

import com.fourdotfive.missioncontrol.security.FourDotFiveUser;

public interface AccessControlService {

	// company
	boolean canGet4Dot5Admins(FourDotFiveUser currentUser);

	boolean canGetAllCompanies(FourDotFiveUser currentUser);

	boolean canSaveCompany(FourDotFiveUser currentUser);

	boolean canChangecompanystate(FourDotFiveUser currentUser);

	boolean canAssignClientOrbu(FourDotFiveUser currentUser);

	boolean canGetCompanyDetails(FourDotFiveUser currentUser);

	boolean canDeleteClient(FourDotFiveUser currentUser);

	boolean canDeleteCompany(FourDotFiveUser currentUser);

	boolean canGetClientsOrBu(FourDotFiveUser currentUser);

	boolean canSetLicensePreferences(FourDotFiveUser currentUser);

	boolean canGetLicensePreferences(FourDotFiveUser currentUser);

	boolean canGetClientLicensePreferences(FourDotFiveUser currentUser);

	boolean canGetRoleEntityScope(FourDotFiveUser currentUser);

	boolean canCreateRoleEntityScope(FourDotFiveUser currentUser);

	boolean canUpdateRoleEntityScope(FourDotFiveUser currentUser);

	boolean canGetParentCompanyIndustryList(FourDotFiveUser currentUser);

	boolean canGetChildCompanyIndustryList(FourDotFiveUser currentUser);

	// user
	boolean canGetRoles(FourDotFiveUser currentUser);

	boolean canGetUserEnitityScope(FourDotFiveUser currentUser);

	boolean canCheckRoleEnitityScope(FourDotFiveUser currentUser);

	boolean canSetScope(FourDotFiveUser currentUser);

	boolean canGetTeamMembers(FourDotFiveUser currentUser);

	boolean canGetUserDetails(FourDotFiveUser currentUser);

	boolean canSaveUser(FourDotFiveUser currentUser);

	boolean canAssignManager(FourDotFiveUser currentUser);

	boolean canDeleteUser(FourDotFiveUser currentUser);

	boolean canGetReportees(FourDotFiveUser currentUser);

	boolean canLoggedInUserDetails(FourDotFiveUser currentUser);

	boolean canAssignCompanyFor4dot5Admin(FourDotFiveUser currentUser);

	boolean canGetForwardRecipients(FourDotFiveUser currentUser);

	boolean canGetPossibleManagersForUpdate(FourDotFiveUser currentUser);
	
	boolean canSaveForwardRecipient(FourDotFiveUser currentUser);
	
	boolean canUpdateUser(FourDotFiveUser currentUser);

	boolean canSaveQuestionList(FourDotFiveUser currentUser);

	boolean canGetQuestionList(FourDotFiveUser currentUser);

	boolean canGetFourDotFiveCompanyDetails(FourDotFiveUser currentUser);

	boolean canGetRecruiterScreeningQuestions(FourDotFiveUser currentUser);

	boolean canSaveRecruiterScreeningQuestions(FourDotFiveUser currentUser);

	boolean canGetJobFile(FourDotFiveUser currentUser);

	boolean canSaveJobMatchState(FourDotFiveUser currentUser);

	boolean canSyncAssessments(FourDotFiveUser currentUser);

    boolean canViewReports(FourDotFiveUser currentUser);
}
