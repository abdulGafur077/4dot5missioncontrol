package com.fourdotfive.missioncontrol.acl;

public interface AllAccessControlService {

    // company access
    boolean canGet4Dot5Admins();

    boolean canGetAllCompanies();

    boolean canSaveCompany();

    boolean canChangecompanystate();

    boolean canAssignClientOrbu();

    boolean canGetCompanyDetails();

    boolean canDeleteClient();

    boolean canDeleteCompany();

    boolean canGetClientsOrBu();

    boolean canSetLicensePreferences();

    boolean canGetLicensePreferences();

    boolean canGetClientLicensePreferences();

    boolean canGetRoleEntityScope();

    boolean canCreateRoleEntityScope();

    boolean canUpdateRoleEntityScope();

    boolean canGetParentCompanyIndustryList();

    boolean canGetChildCompanyIndustryList();

    // user access

    boolean canGetRoles();

    boolean canGetUserEnitityScope();

    boolean canCheckRoleEnitityScope();

    boolean canSetScope();

    boolean canGetTeamMembers();

    boolean canGetUserDetails();

    boolean canSaveUser();

    boolean canSaveForwardRecipient();

    boolean canAssignManager();

    boolean canDeleteUser();

    boolean canGetReportees();

    boolean canLoggedInUserDetails();

    boolean canAssignCompanyFor4dot5Admin();

    boolean canGetForwardRecipients();

    boolean canGetPossibleManagersForUpdate();

    boolean canUpdateUser();

    boolean canSaveQuestionList();

    boolean canGetQuestionList();

    boolean canGetFourDotFiveCompanyDetails();

    boolean canGetRecruiterScreeningQuestions();

    boolean canSaveRecruiterScreeningQuestions();

    boolean canGetJobFile();

    boolean canSaveJobMatchState();

    // Assessment Access
    boolean canSyncAssessments();

    boolean canViewReports();

}
