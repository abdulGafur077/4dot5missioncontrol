package com.fourdotfive.missioncontrol.acl;

import org.springframework.stereotype.Service;

@Service
public class EmployeeAclServiceImp implements AllAccessControlService {

    @Override
    public boolean canGet4Dot5Admins() {
        return false;
    }

    @Override
    public boolean canGetAllCompanies() {
        return false;
    }

    @Override
    public boolean canSaveCompany() {
        return false;
    }

    @Override
    public boolean canChangecompanystate() {
        return false;
    }

    @Override
    public boolean canAssignClientOrbu() {
        return false;
    }

    @Override
    public boolean canGetCompanyDetails() {
        return false;
    }

    @Override
    public boolean canDeleteClient() {
        return false;
    }

    @Override
    public boolean canDeleteCompany() {
        return false;
    }

    @Override
    public boolean canGetClientsOrBu() {
        return false;
    }

    @Override
    public boolean canSetLicensePreferences() {
        return false;
    }

    @Override
    public boolean canGetLicensePreferences() {
        return false;
    }

    @Override
    public boolean canGetClientLicensePreferences() {
        return false;
    }

    @Override
    public boolean canGetRoleEntityScope() {
        return false;
    }

    @Override
    public boolean canCreateRoleEntityScope() {
        return false;
    }

    @Override
    public boolean canUpdateRoleEntityScope() {
        return false;
    }

    @Override
    public boolean canGetParentCompanyIndustryList() {
        return false;
    }

    @Override
    public boolean canGetChildCompanyIndustryList() {
        return false;
    }

    @Override
    public boolean canGetRoles() {
        return false;
    }

    @Override
    public boolean canGetUserEnitityScope() {
        return false;
    }

    @Override
    public boolean canCheckRoleEnitityScope() {
        return false;
    }

    @Override
    public boolean canGetTeamMembers() {
        return false;
    }

    @Override
    public boolean canGetUserDetails() {
        return false;
    }

    @Override
    public boolean canSaveUser() {
        return false;
    }

    @Override
    public boolean canAssignManager() {
        return false;
    }

    @Override
    public boolean canDeleteUser() {
        return false;
    }

    @Override
    public boolean canGetReportees() {
        return false;
    }

    @Override
    public boolean canLoggedInUserDetails() {
        return false;
    }

    @Override
    public boolean canAssignCompanyFor4dot5Admin() {
        return false;
    }

    @Override
    public boolean canGetForwardRecipients() {
        return false;
    }

    @Override
    public boolean canGetPossibleManagersForUpdate() {
        return false;
    }

    @Override
    public boolean canSetScope() {
        return false;
    }

    @Override
    public boolean canSaveForwardRecipient() {
        return false;
    }

    @Override
    public boolean canUpdateUser() {
        return false;
    }

    @Override
    public boolean canSaveQuestionList() {
        return false;
    }

    @Override
    public boolean canGetQuestionList() {
        return false;
    }

    @Override
    public boolean canGetFourDotFiveCompanyDetails() {
        return false;
    }

    @Override
    public boolean canGetRecruiterScreeningQuestions() {
        return true;
    }

    @Override
    public boolean canSaveRecruiterScreeningQuestions() {
        return true;
    }

    @Override
    public boolean canGetJobFile() {
        return false;
    }

    @Override
    public boolean canSaveJobMatchState() {
        return false;
    }

    @Override
    public boolean canSyncAssessments() {
        return false;
    }

    @Override
    public boolean canViewReports() {
        return false;
    }


}
