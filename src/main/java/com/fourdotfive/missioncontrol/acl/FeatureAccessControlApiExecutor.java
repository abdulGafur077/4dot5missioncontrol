package com.fourdotfive.missioncontrol.acl;


public interface FeatureAccessControlApiExecutor {

    boolean isAddUserFeatureAvailable(String userId);
    boolean isAddCandidateFeatureAvailable(String userId);
    boolean isAddRequisitionFeatureAvailable(String userId);
    boolean isMatchingFeatureAvailable(String userId);
    boolean isTechAssessmentFeatureAvailable(String userId);
    boolean isValueAssessmentFeatureAvailable(String userId);
    boolean isCustomizableRecruiterScreeningFeatureAvailable(String userId);
    boolean isCustomizablePhoneFeedbackFeatureAvailable(String userId);
    boolean isCustomizableInterviewFeedbackFeatureAvailable(String userId);
    boolean isJobBoardIntegrationFeatureAvailable(String userId);
}
