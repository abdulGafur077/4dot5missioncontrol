package com.fourdotfive.missioncontrol.acl;


import com.fourdotfive.missioncontrol.security.FourDotFiveUser;

public interface FeatureAccessControlService {

    boolean isAddUserFeatureAvailable(FourDotFiveUser user);
    boolean isAddCandidateFeatureAvailable(FourDotFiveUser user);
    boolean isAddRequisitionFeatureAvailable(FourDotFiveUser user);
    boolean isMatchingFeatureAvailable(FourDotFiveUser user);
    boolean isTechAssessmentFeatureAvailable(FourDotFiveUser user);
    boolean isValueAssessmentFeatureAvailable(FourDotFiveUser user);
    boolean isCustomizableRecruiterScreeningFeatureAvailable(FourDotFiveUser user);
    boolean isCustomizablePhoneFeedbackFeatureAvailable(FourDotFiveUser user);
    boolean isCustomizableInterviewFeedbackFeatureAvailable(FourDotFiveUser user);
    boolean isJobBoardIntegrationFeatureAvailable(FourDotFiveUser user);
}
