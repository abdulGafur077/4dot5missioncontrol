package com.fourdotfive.missioncontrol.acl;


import com.fourdotfive.missioncontrol.security.FourDotFiveUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FeatureAccessControlServiceImpl implements FeatureAccessControlService {

    @Autowired
    private FeatureAccessControlApiExecutor featureAccessControlApiExecutor;
    @Override
    public boolean isAddUserFeatureAvailable(FourDotFiveUser user) {
        return featureAccessControlApiExecutor.isAddUserFeatureAvailable(user.getId());
    }

    @Override
    public boolean isAddCandidateFeatureAvailable(FourDotFiveUser user) {
        return featureAccessControlApiExecutor.isAddCandidateFeatureAvailable(user.getId());
    }

    @Override
    public boolean isAddRequisitionFeatureAvailable(FourDotFiveUser user) {
        return featureAccessControlApiExecutor.isAddRequisitionFeatureAvailable(user.getId());
    }

    @Override
    public boolean isMatchingFeatureAvailable(FourDotFiveUser user) {
        return featureAccessControlApiExecutor.isMatchingFeatureAvailable(user.getId());
    }

    @Override
    public boolean isTechAssessmentFeatureAvailable(FourDotFiveUser user) {
        return featureAccessControlApiExecutor.isTechAssessmentFeatureAvailable(user.getId());
    }

    @Override
    public boolean isValueAssessmentFeatureAvailable(FourDotFiveUser user) {
        return featureAccessControlApiExecutor.isValueAssessmentFeatureAvailable(user.getId());
    }

    @Override
    public boolean isCustomizableRecruiterScreeningFeatureAvailable(FourDotFiveUser user) {
        return featureAccessControlApiExecutor.isCustomizableRecruiterScreeningFeatureAvailable(user.getId());
    }

    @Override
    public boolean isCustomizablePhoneFeedbackFeatureAvailable(FourDotFiveUser user) {
        return featureAccessControlApiExecutor.isCustomizablePhoneFeedbackFeatureAvailable(user.getId());
    }

    @Override
    public boolean isCustomizableInterviewFeedbackFeatureAvailable(FourDotFiveUser user) {
        return featureAccessControlApiExecutor.isCustomizableInterviewFeedbackFeatureAvailable(user.getId());
    }

    @Override
    public boolean isJobBoardIntegrationFeatureAvailable(FourDotFiveUser user) {
        return featureAccessControlApiExecutor.isJobBoardIntegrationFeatureAvailable(user.getId());
    }
}
