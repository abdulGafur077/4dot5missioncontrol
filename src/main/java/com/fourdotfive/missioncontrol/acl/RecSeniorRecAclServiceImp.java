package com.fourdotfive.missioncontrol.acl;

import org.springframework.stereotype.Service;

@Service
public class RecSeniorRecAclServiceImp implements AllAccessControlService {

	@Override
	public boolean canGet4Dot5Admins() {
		return false;
	}

	@Override
	public boolean canGetAllCompanies() {
		return false;
	}

	@Override
	public boolean canSaveCompany() {
		return false;
	}

	@Override
	public boolean canChangecompanystate() {
		return false;
	}

	@Override
	public boolean canAssignClientOrbu() {
		return false;
	}

	@Override
	public boolean canGetCompanyDetails() {
		return false;
	}

	@Override
	public boolean canDeleteClient() {
		return false;
	}

	@Override
	public boolean canDeleteCompany() {
		return false;
	}

	@Override
	public boolean canGetClientsOrBu() {
		return false;
	}

	@Override
	public boolean canSetLicensePreferences() {
		return false;
	}

	@Override
	public boolean canGetLicensePreferences() {
		return false;
	}

	@Override
	public boolean canGetClientLicensePreferences() {
		return true;
	}

	@Override
	public boolean canGetRoleEntityScope() {
		return false;
	}

	@Override
	public boolean canCreateRoleEntityScope() {
		return false;
	}

	@Override
	public boolean canUpdateRoleEntityScope() {
		return false;
	}

	@Override
	public boolean canGetParentCompanyIndustryList() {
		return false;
	}

	@Override
	public boolean canGetChildCompanyIndustryList() {
		return true;
	}

	@Override
	public boolean canGetRoles() {
		return true;
	}

	@Override
	public boolean canGetUserEnitityScope() {
		return true;
	}

	@Override
	public boolean canCheckRoleEnitityScope() {
		return true;
	}

	@Override
	public boolean canGetTeamMembers() {
		return true;
	}

	@Override
	public boolean canGetUserDetails() {
		return true;
	}

	@Override
	public boolean canSaveUser() {
		return false;
	}

	@Override
	public boolean canAssignManager() {
		return false;
	}

	@Override
	public boolean canDeleteUser() {
		return false;
	}

	@Override
	public boolean canGetReportees() {
		return false;
	}

	@Override
	public boolean canLoggedInUserDetails() {
		return true;
	}

	@Override
	public boolean canAssignCompanyFor4dot5Admin() {
		return false;
	}

	@Override
	public boolean canGetForwardRecipients() {
		return true;
	}

	@Override
	public boolean canGetPossibleManagersForUpdate() {
		return true;
	}

	@Override
	public boolean canSetScope() {
		return false;
	}

	@Override
	public boolean canSaveForwardRecipient() {
		return true;
	}

	@Override
	public boolean canUpdateUser() {
		return true;
	}

	@Override
	public boolean canSaveQuestionList() {
		return true;
	}

	@Override
	public boolean canGetQuestionList() {
		return true;
	}

	@Override
	public boolean canGetFourDotFiveCompanyDetails() {
		return true;
	}

	@Override
	public boolean canGetRecruiterScreeningQuestions()  { return true; }

	@Override
	public boolean canSaveRecruiterScreeningQuestions()  { return true; }

	@Override
	public boolean canGetJobFile()  { return true; }


	@Override
	public boolean canSaveJobMatchState()  { return true; }

	@Override
	public boolean canSyncAssessments() {
		return false;
	}

    @Override
    public boolean canViewReports() {
		return false;
	}


}
