package com.fourdotfive.missioncontrol.acl;

import org.springframework.stereotype.Service;

@Service
public class SuperAdminAclServiceImp implements AllAccessControlService {

    @Override
    public boolean canGet4Dot5Admins() {
        return true;
    }

    @Override
    public boolean canGetAllCompanies() {
        return true;
    }

    @Override
    public boolean canSaveCompany() {
        return true;
    }

    @Override
    public boolean canChangecompanystate() {
        return true;
    }

    @Override
    public boolean canAssignClientOrbu() {
        return true;
    }

    @Override
    public boolean canGetCompanyDetails() {
        return true;
    }

    @Override
    public boolean canDeleteClient() {
        return true;
    }

    @Override
    public boolean canDeleteCompany() {
        return true;
    }

    @Override
    public boolean canGetClientsOrBu() {
        return true;
    }

    @Override
    public boolean canSetLicensePreferences() {
        return true;
    }

    @Override
    public boolean canGetLicensePreferences() {
        return true;
    }

    @Override
    public boolean canGetClientLicensePreferences() {
        return true;
    }

    @Override
    public boolean canGetRoleEntityScope() {
        return true;
    }

    @Override
    public boolean canCreateRoleEntityScope() {
        return true;
    }

    @Override
    public boolean canUpdateRoleEntityScope() {
        return true;
    }

    @Override
    public boolean canGetParentCompanyIndustryList() {
        return true;
    }

    @Override
    public boolean canGetChildCompanyIndustryList() {
        return true;
    }

    @Override
    public boolean canGetRoles() {
        return true;
    }

    @Override
    public boolean canGetUserEnitityScope() {
        return true;
    }

    @Override
    public boolean canCheckRoleEnitityScope() {
        return true;
    }

    @Override
    public boolean canGetTeamMembers() {
        return true;
    }

    @Override
    public boolean canGetUserDetails() {
        return true;
    }

    @Override
    public boolean canSaveUser() {
        return true;
    }

    @Override
    public boolean canAssignManager() {
        return true;
    }

    @Override
    public boolean canDeleteUser() {
        return true;
    }

    @Override
    public boolean canGetReportees() {
        return true;
    }

    @Override
    public boolean canLoggedInUserDetails() {
        return true;
    }

    @Override
    public boolean canAssignCompanyFor4dot5Admin() {
        return true;
    }

    @Override
    public boolean canGetForwardRecipients() {
        return true;
    }

    @Override
    public boolean canGetPossibleManagersForUpdate() {
        return true;
    }

    @Override
    public boolean canSetScope() {
        return true;
    }

    @Override
    public boolean canSaveForwardRecipient() {
        return true;
    }

    @Override
    public boolean canUpdateUser() {
        return true;
    }

    @Override
    public boolean canSaveQuestionList() {
        return true;
    }

    @Override
    public boolean canGetQuestionList() {
        return true;
    }

    @Override
    public boolean canGetFourDotFiveCompanyDetails() {
        return true;
    }

    @Override
    public boolean canGetRecruiterScreeningQuestions() {
        return true;
    }

    @Override
    public boolean canSaveRecruiterScreeningQuestions() {
        return true;
    }

    @Override
    public boolean canGetJobFile() {
        return true;
    }

    @Override
    public boolean canSaveJobMatchState() {
        return true;
    }

    @Override
    public boolean canSyncAssessments() {
        return true;
    }

    @Override
    public boolean canViewReports() {
        return true;
    }

}
