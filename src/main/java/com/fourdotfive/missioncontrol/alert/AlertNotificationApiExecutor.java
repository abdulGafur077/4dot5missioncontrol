package com.fourdotfive.missioncontrol.alert;


import com.fourdotfive.missioncontrol.dtos.alert.NotificationFilterParamDto;
import com.fourdotfive.missioncontrol.dtos.alert.NotificationFilterParamsDto;
import com.fourdotfive.missioncontrol.dtos.alert.NotificationStateChangeDTO;

public interface AlertNotificationApiExecutor {
    String changeNotificationsState(NotificationStateChangeDTO notificationStateChange);

    String findAll(NotificationFilterParamsDto notificationFilterParamsDto);

    String changeStateForAllNotificationsOfLoggedInUser(String userId, String state);
}