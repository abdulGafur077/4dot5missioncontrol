package com.fourdotfive.missioncontrol.alert;

import com.fourdotfive.missioncontrol.dtos.alert.NotificationFilterParamDto;
import com.fourdotfive.missioncontrol.dtos.alert.NotificationStateChangeDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/notification/")
public class AlertNotificationController {
    private final NotificationService notificationService;

    @Autowired
    public AlertNotificationController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @RequestMapping(value = {"changenotificationstate"}, method = RequestMethod.POST)
    public String changeNotificationsState(@RequestBody NotificationStateChangeDTO notificationStateChange) {
        return notificationService.changeNotificationsState(notificationStateChange);
    }

    @RequestMapping(value = { "findall" }, method = RequestMethod.POST)
    public String findAll(@RequestBody NotificationFilterParamDto notificationFilterParam) {
        return notificationService.findAll(notificationFilterParam);
    }

    @RequestMapping(value = {"changestateforallnotificationsofuser"}, method = RequestMethod.GET)
    public String changeNotificationsState(@RequestParam(value = "state") String state) {
        return notificationService.changeStateForAllNotificationsOfLoggedInUser(state);
    }
}
