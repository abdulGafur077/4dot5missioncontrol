package com.fourdotfive.missioncontrol.alert;

import com.fourdotfive.missioncontrol.dtos.alert.NotificationFilterParamDto;
import com.fourdotfive.missioncontrol.dtos.alert.NotificationStateChangeDTO;
import com.fourdotfive.missioncontrol.pojo.alert.Notification;

public interface NotificationService {
    void sendMessage(Notification notification);

    String changeNotificationsState(NotificationStateChangeDTO notificationStateChange);

    String findAll(NotificationFilterParamDto notificationFilterParam);

    String changeStateForAllNotificationsOfLoggedInUser(String state);
}
