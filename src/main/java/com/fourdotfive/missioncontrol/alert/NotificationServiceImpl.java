package com.fourdotfive.missioncontrol.alert;

import com.fourdotfive.missioncontrol.dtos.alert.NotificationFilterParamDto;
import com.fourdotfive.missioncontrol.dtos.alert.NotificationFilterParamsDto;
import com.fourdotfive.missioncontrol.dtos.alert.NotificationStateChangeDTO;
import com.fourdotfive.missioncontrol.pojo.alert.Notification;
import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
public class NotificationServiceImpl implements NotificationService {
    private final AlertNotificationApiExecutor alertNotificationApiExecutor;
    private final UserService userService;
    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    public NotificationServiceImpl(AlertNotificationApiExecutor alertNotificationApiExecutor, UserService userService) {
        this.alertNotificationApiExecutor = alertNotificationApiExecutor;
        this.userService = userService;
    }

    @Override
    public void sendMessage(Notification notification) {
        simpMessagingTemplate.convertAndSend("/queue/" + notification.getRecipientId() + "/notifications", notification);
    }

    @Override
    public String changeNotificationsState(NotificationStateChangeDTO notificationStateChange) {
        return alertNotificationApiExecutor.changeNotificationsState(notificationStateChange);
    }

    @Override
    public String findAll(NotificationFilterParamDto notificationFilterParam) {
        User user = userService.getCurrentLoggedInUser();
        NotificationFilterParamsDto notificationFilterParamsDto = new NotificationFilterParamsDto(notificationFilterParam, user);
        return alertNotificationApiExecutor.findAll(notificationFilterParamsDto);
    }

    @Override
    public String changeStateForAllNotificationsOfLoggedInUser(String state) {
        User user = userService.getCurrentLoggedInUser();
        return alertNotificationApiExecutor.changeStateForAllNotificationsOfLoggedInUser(user.getId(), state);
    }
}
