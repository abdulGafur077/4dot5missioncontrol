package com.fourdotfive.missioncontrol.alert;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourdotfive.missioncontrol.chat.ChatMessage;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;

import java.io.IOException;

public class SQSChatAlertReader {
    private static final Logger LOGGER = LoggerFactory.getLogger(SQSChatAlertReader.class);
    @Autowired
    private SQSChatAlertService SQSChatAlertService;
    @JmsListener(destination = "${aws.sqs.chat.queue.name}")

    public void readChatAlert(String requestJSON) {
        LOGGER.info(new DateTime() + " : SQSChatAlertReader :: readChatAlert() :: Received :" + requestJSON);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            ChatMessage chatMessage = objectMapper.readValue(requestJSON, ChatMessage.class);
            SQSChatAlertService.sendMessage(chatMessage);
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
