package com.fourdotfive.missioncontrol.alert;

import com.fourdotfive.missioncontrol.chat.ChatMessage;

public interface SQSChatAlertService {
    void sendMessage(ChatMessage chatMessage);
}
