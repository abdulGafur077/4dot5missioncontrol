package com.fourdotfive.missioncontrol.alert;

import com.fourdotfive.missioncontrol.chat.ChatMessage;
import com.fourdotfive.missioncontrol.chat.ChatUserDetailDTO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
public class SQSChatAlertServiceImpl implements SQSChatAlertService {

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @Override
    public void sendMessage(ChatMessage chatMessage) {
        for (ChatUserDetailDTO recipient: chatMessage.getRecipients()) {
            if (StringUtils.isNotBlank(recipient.getId()))
                simpMessagingTemplate.convertAndSend("/queue/" + recipient.getId() + "/chatMessage", chatMessage);
        }
    }
}
