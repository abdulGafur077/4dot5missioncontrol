package com.fourdotfive.missioncontrol.alert;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourdotfive.missioncontrol.pojo.alert.Notification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;

import java.io.IOException;

public class SQSNotificationReader {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(SQSNotificationReader.class);

    @Autowired
    private NotificationService notificationService;

    @JmsListener(destination = "${aws.sqs.alertQueueName}")
    public void readNotification(String requestJSON) {
        LOGGER.info("Received " + requestJSON);

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            Notification notification = objectMapper.readValue(requestJSON, Notification.class);
            notificationService.sendMessage(notification);
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}