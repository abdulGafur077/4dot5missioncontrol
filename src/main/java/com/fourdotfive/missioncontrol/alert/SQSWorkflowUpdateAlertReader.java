package com.fourdotfive.missioncontrol.alert;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourdotfive.missioncontrol.pojo.alert.WorkflowUpdateAlertDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;

import java.io.IOException;

public class SQSWorkflowUpdateAlertReader {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(SQSWorkflowUpdateAlertReader.class);

    @Autowired
    private WorkflowUpdateAlertService workflowUpdateAlertService;

    @JmsListener(destination = "${aws.sqs.workflowAlertQueueName}")
    public void readWorkflowUpdateAlert(String requestJSON) {
        LOGGER.info("readWorkflowUpdateAlert() :: Received :" + requestJSON);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            WorkflowUpdateAlertDTO  updateAlertDTO = objectMapper.readValue(requestJSON, WorkflowUpdateAlertDTO.class);
            workflowUpdateAlertService.sendMessage(updateAlertDTO);
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
