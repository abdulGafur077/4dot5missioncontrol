package com.fourdotfive.missioncontrol.alert;

import com.fourdotfive.missioncontrol.pojo.alert.WorkflowUpdateAlertDTO;

public interface WorkflowUpdateAlertService {

    void sendMessage(WorkflowUpdateAlertDTO workflowUpdateAlertDTO);
}
