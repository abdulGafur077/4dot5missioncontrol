package com.fourdotfive.missioncontrol.alert;

import com.fourdotfive.missioncontrol.pojo.alert.WorkflowUpdateAlertDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
public class WorkflowUpdateAlertServiceImpl implements WorkflowUpdateAlertService {

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @Override
    public void sendMessage(WorkflowUpdateAlertDTO workflowUpdateAlert) {
        simpMessagingTemplate.convertAndSend("/queue/" + workflowUpdateAlert.getRecipientId() + "/workflowUpdate", workflowUpdateAlert);
    }
}
