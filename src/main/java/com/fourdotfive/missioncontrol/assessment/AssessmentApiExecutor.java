package com.fourdotfive.missioncontrol.assessment;

import com.fourdotfive.missioncontrol.dto.assessment.AccessTimeRequestDTO;

import java.util.List;
import java.util.Map;

public interface AssessmentApiExecutor {
    String syncAssessments(String companyId);

    String assignAssessment(Map<String, List<String>> assessments);

    String getAllValueAssessmentsByCompanyId(String companyId, String clientOrBuId);

    String getAllTechAssessmentsByCompanyId(String companyId, String clientOrBuId);

    String getAllUnassignedAssessmentsByCompanyId(String companyId, String clientOrBuId);

    String removeAssignedAssessmentByCompanyId(String companyId, String assessmentId);

    String getAccessTime(AccessTimeRequestDTO accessTimeRequestDTO);

    String getAssessmentDetailsByAssessmentId(String assessmentId);
}