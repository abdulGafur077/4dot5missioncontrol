package com.fourdotfive.missioncontrol.assessment;

import com.fourdotfive.missioncontrol.dto.assessment.AccessTimeRequestDTO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/assessment")
public class AssessmentController {
    private final AssessmentService assessmentService;

    @Autowired
    public AssessmentController(AssessmentService assessmentService) {
        this.assessmentService = assessmentService;
    }

    @PreAuthorize("@accessControlImp.canSyncAssessments(principal)")
    @RequestMapping(value = {"syncassessments"}, method = RequestMethod.PUT)
    public @ResponseBody
    ResponseEntity<String> syncAssessments(@RequestParam(name = "companyId", required = false) String companyId) {
        String result = assessmentService.syncAssessments(companyId);
        if (result == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
    }

    @RequestMapping(value = {"assignassessments"}, method = RequestMethod.PUT)
    public @ResponseBody
    ResponseEntity<String> assignAssessment(@RequestBody Map<String, List<String>> assessments) {
        String result = assessmentService.assignAssessment(assessments);
        if (result == null) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
    }

    @RequestMapping(value = {"getallunassignedassessments/{companyid}"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAllUnassignedAssessmentsByCompanyId(@PathVariable(value = "companyid") String companyId,
                                                         @RequestParam(value = "clientOrBuId", required = false, defaultValue = "") String clientOrBuId) {
        if (StringUtils.isEmpty(companyId)) {
            throw new IllegalArgumentException("Company Id is empty");
        }
        return assessmentService.getAllUnassignedAssessmentsByCompanyId(companyId, clientOrBuId);
    }

    @RequestMapping(value = {"getallvalueassessments/{companyid}"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAllValueAssessmentsByCompanyId(@PathVariable(value = "companyid") String companyId,
                                                    @RequestParam(value = "clientOrBuId", required = false, defaultValue = "") String clientOrBuId) {
        if (StringUtils.isEmpty(companyId)) {
            throw new IllegalArgumentException("Company Id is empty");
        }
        return assessmentService.getAllValueAssessmentsByCompanyId(companyId, clientOrBuId);
    }

    @RequestMapping(value = {"getalltechassessments/{companyid}"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAllTechAssessmentsByCompanyId(@PathVariable(value = "companyid") String companyId,
                                                   @RequestParam(value = "clientOrBuId", required = false, defaultValue = "") String clientOrBuId) {
        if (StringUtils.isEmpty(companyId)) {
            throw new IllegalArgumentException("Company Id is empty");
        }
        return assessmentService.getAllTechAssessmentsByCompanyId(companyId, clientOrBuId);
    }

    @RequestMapping(value = {"removeassignedassessment/{companyid}/{assessmentid}"}, method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public String removeAssignedAssessmentByCompanyId(@PathVariable(value = "companyid") String companyId, @PathVariable(value = "assessmentid") String assessmentId) {
        if (StringUtils.isEmpty(companyId)) {
            throw new IllegalArgumentException("Company Id is empty");
        }
        if (StringUtils.isEmpty(assessmentId)) {
            throw new IllegalArgumentException("Assessment Id is empty");
        }
        return assessmentService.removeAssignedAssessmentByCompanyId(companyId, assessmentId);
    }

    @RequestMapping(value = {"getaccesstime"}, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAssessmentAccessTime(@RequestBody AccessTimeRequestDTO accessTimeRequestDTO) {
        if (accessTimeRequestDTO == null) {
            throw new IllegalArgumentException("Request parameters canot be null");
        }

        if (StringUtils.isEmpty(accessTimeRequestDTO.getJobId())) {
            throw new IllegalArgumentException("Job id is empty");
        }

        if (StringUtils.isEmpty(String.valueOf(accessTimeRequestDTO.getLicensePreferenceEnum()))) {
            throw new IllegalArgumentException("Assessment type can not be null");
        }
       return assessmentService.getAccessTime(accessTimeRequestDTO);
    }

    @RequestMapping(value = {"getassessmentdetails/{assessmentId}"}, method = RequestMethod.GET)
    public String getAssessmentDetailsByAssessmentId(@PathVariable String assessmentId) {

        return assessmentService.getAssessmentDetailsByAssessmentId(assessmentId);
    }
}
