package com.fourdotfive.missioncontrol.assessment;

import com.fourdotfive.missioncontrol.dto.assessment.AccessTimeRequestDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class AssessmentServiceImpl implements AssessmentService {
    private final AssessmentApiExecutor assessmentApiExecutor;

    @Autowired
    public AssessmentServiceImpl(AssessmentApiExecutor assessmentApiExecutor) {
        this.assessmentApiExecutor = assessmentApiExecutor;
    }

    @Override
    public String syncAssessments(String companyId) {
        return assessmentApiExecutor.syncAssessments(companyId);
    }

    @Override
    public String assignAssessment(Map<String, List<String>> assessments) {
        return assessmentApiExecutor.assignAssessment(assessments);
    }

    @Override
    public String getAllValueAssessmentsByCompanyId(String companyId, String clientOrBuId) {
        return assessmentApiExecutor.getAllValueAssessmentsByCompanyId(companyId, clientOrBuId);
    }

    @Override
    public String getAllTechAssessmentsByCompanyId(String companyId, String clientOrBuId) {
        return assessmentApiExecutor.getAllTechAssessmentsByCompanyId(companyId, clientOrBuId);
    }

    @Override
    public String getAllUnassignedAssessmentsByCompanyId(String companyId, String clientOrBuId) {
        return assessmentApiExecutor.getAllUnassignedAssessmentsByCompanyId(companyId, clientOrBuId);
    }

    @Override
    public String removeAssignedAssessmentByCompanyId(String companyId, String assessmentId) {
        return assessmentApiExecutor.removeAssignedAssessmentByCompanyId(companyId, assessmentId);
    }

    @Override
    public String getAccessTime(AccessTimeRequestDTO accessTimeRequestDTO) {
        return assessmentApiExecutor.getAccessTime(accessTimeRequestDTO);
    }

    @Override
    public String getAssessmentDetailsByAssessmentId(String assessmentId) {
        return assessmentApiExecutor.getAssessmentDetailsByAssessmentId(assessmentId);
    }
}