package com.fourdotfive.missioncontrol.candidate;

import java.util.Arrays;

public class ActiveCandidatesReport {

	private int[] timeToPlaceCandidatesData;
	private int[] activeCandidatesData;
	private int[] placedCandidatesData;

	public int[] getTimeToPlaceCandidatesData() {
		return timeToPlaceCandidatesData;
	}

	public void setTimeToPlaceCandidatesData(int[] timeToPlaceCandidatesData) {
		this.timeToPlaceCandidatesData = timeToPlaceCandidatesData;
	}

	public int[] getActiveCandidatesData() {
		return activeCandidatesData;
	}

	public void setActiveCandidatesData(int[] activeCandidatesData) {
		this.activeCandidatesData = activeCandidatesData;
	}

	public int[] getPlacedCandidatesData() {
		return placedCandidatesData;
	}

	public void setPlacedCandidatesData(int[] placedCandidatesData) {
		this.placedCandidatesData = placedCandidatesData;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ActiveCandidatesReport [timeToPlaceCandidatesData=");
		builder.append(Arrays.toString(timeToPlaceCandidatesData));
		builder.append(", activeCandidatesData=");
		builder.append(Arrays.toString(activeCandidatesData));
		builder.append(", placedCandidatesData=");
		builder.append(Arrays.toString(placedCandidatesData));
		builder.append("]");
		return builder.toString();
	}

}
