package com.fourdotfive.missioncontrol.candidate;

import com.fourdotfive.missioncontrol.dtos.candidate.*;
import com.fourdotfive.missioncontrol.dtos.job.NewJobDto;
import com.fourdotfive.missioncontrol.dtos.jobmatch.AssignRecruiterDto;
import com.fourdotfive.missioncontrol.dtos.jobmatch.CandidateJobMatchInfoDto;
import com.fourdotfive.missioncontrol.dtos.jobmatch.FilterCandidateActivityDto;
import com.fourdotfive.missioncontrol.dtos.pdf.AssessmentReportInfoDto;
import com.fourdotfive.missioncontrol.dtos.user.DashBoardDto;
import com.fourdotfive.missioncontrol.dtos.pdf.CandidateAssessmentReportDetails;
import com.fourdotfive.missioncontrol.pojo.candidate.Candidate;
import com.fourdotfive.missioncontrol.pojo.candidate.CandidateCardDto;
import com.fourdotfive.missioncontrol.pojo.candidate.CandidateCardFilterParams;
import com.fourdotfive.missioncontrol.pojo.candidate.CandidateFilterParams;

import java.util.List;

public interface CandidateApiExecutor {

    List<Candidate> getAllCandidates(CandidateRequestDto requestDto);

    DashBoardDto getCandidateCount(String jobId);

    String getCandidateIdNamesByCompany(CandidateFilterParams filterParams);

    String getCandidateIdNamesByUser(CandidateFilterParams filterParams);

    CandidateCardDto getCandidateCard(String candidateId, String loggedInUserId, CandidateCardFilterParams filterParams);

    Candidate setStatus(JobStateChange jobStateChange);

    String getCandidateProfile(String candidateId);

    String schedulePhoneScreen(CandidateMeetingScheduleDto candidateMeetingSchedule);

    String scheduleInterview(CandidateMeetingScheduleDto candidateMeetingSchedule);

    String setReleased(List<CandidateJobMatchInfoDto> candidateJobMatchInfoDtos);

    String setNotInterested(List<CandidateJobMatchInfoDto> CandidateJobMatchInfoDto);

    String sendValueAssessment(SendAssessmentDto sendAssessmentDto);

    String sendTechAssessment(SendAssessmentDto sendAssessmentDto);

    String cancelValueAssessment(CancelAssessmentDto cancelAssessmentDto);

    String cancelTechAssessment(CancelAssessmentDto cancelAssessmentDto);

    String moveForward(ChangeStepDto changeStep);

    String moveBackward(ChangeStepDto changeStep);

    String moveToAny(CandidateMoveStepDto candidateMoveStep);

    CandidateStatusCountDto getCandidateStatusCountByCompany(String companyId, String visibility, String userId);

    CandidateStatusCountDto getCandidateStatusCountByUser(String userId, String companyId, String visibility);

    String rescheduleMeeting(String meetingScheduleId, MeetingScheduleUpdateDto meetingScheduleUpdateDto);

    String cancelScheduledMeeting(String meetingScheduleId, CancelMeetingDto cancelMeetingDto);

    byte[] getCandidatePdfReport(Long testInvitationId, boolean sectional);

    String getRecruiters(List<String> jobMatchIds, String companyId);

    String setRecruiter(AssignRecruiterDto assignRecruiterDto);

    String addCandidates(List<CandidateContactDetailDTO> candidateDetails);

    String findCandidatesForRequisition(CandidateRequisitionDto candidateRequisitionDto);

    String resendAssessment(SendAssessmentDto sendAssessmentDto);

    String getCandidateAssessmentDetail(CandidateAssessmentDetailRequestDto candidateAssessmentDetailRequestDto);

    String setAssessmentLeft(AssessmentLeftDto assessmentLeftDto);

    String getAssessmentSentDetails(String jobMatchId, CandidateAssessmentTestType testType);

    String getAvailableTimeZones();

    List<CandidateActivityDto> getCandidateActivity(FilterCandidateActivityDto filterCandidateActivityDto);

    String getCandidateClientOrBUs(String candidateId);

    String getCandidateJobAndRole(String candidateId);

    String getAllCandidateWorkflowCardStatus();

    String setCandidateAvailability(CandidateAvailabilityDTO candidateAvailabilityDTO);

    String getDaxtraAddedCandidatesWithLessScore(String jobId, String daxtraRequestId);

    String deleteCandidate(DeleteDTO deleteDTO);

    String deleteCandidateOnResumeRemove(String resumeId, String loggedInUserId);

    String addOrUpdateContactEmail(ContactDetailsDTO contactDetailsDTO);

    String addOrUpdateContactPhoneNumber(ContactDetailsDTO contactDetailsDTO);

    String addOrUpdateContactAddress(ContactDetailsDTO contactDetailsDTO);

    CandidateAssessmentDto getCandidateAssessmentDetails(Long testInvitationId);

    CandidatesEnabledStepDto getCandidatesPresentOnStep(String companyId, String userId, String jobId);

    String getAllPendingMeetingsAndAssessments(List<String> jobMatchIds);

    String updatePendingMeetingsAndAssessments(List<String> jobMatchIds, String loggedInUserId);

    String sendAssessments(SendAssessmentListDto sendAssessmentDto);

    String cancelAssessments(CancelAssessmentListDto cancelAssessmentListDto);

    CandidateAssessmentReportDetails updateCandidateReportDetails(CandidateAssessmentReportDetails candidateAssessmentReportDetails, String userId);

    CandidateAssessmentReportDetails deleteCommentFromCandidateReportDetails(String candidateAssessmentDetailId, String commentId, String id);

    CandidatesEnabledStepDto getCandidatesPresentOnStepByClientOrBuId(String companyId, String userId, List<NewJobDto> newJobDtos);

    CandidatesEnabledStepDto getCandidatesPresentOnStepByCompanyId(String companyId, String userId, List<String> allClientsOrBu);

    String addOrUpdateCandidateAdditionalDetails(UpdateCandidateAdditionalDetailsDto candidateAdditionalDetailsDto);

    String allowOverride(String candidateId);

    String updateCandidateReportInfo(String candidateAssessmentDetailId, AssessmentReportInfoDto assessmentReportInfoDto);

    String cancelAssessmentsOnAssessmentUpdate(List<String> candidateAssessmentIds, String currentCompanyId, String userId);

    String validateMeetingStartDateAndEndDate(String meetingScheduleId);

    CandidateDetailsResponseDTO getCandidateDetailsById(String candidateId);
}
