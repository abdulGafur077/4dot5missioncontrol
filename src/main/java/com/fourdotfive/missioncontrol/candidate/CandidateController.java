package com.fourdotfive.missioncontrol.candidate;

import com.fourdotfive.missioncontrol.dtos.candidate.*;
import com.fourdotfive.missioncontrol.dtos.jobmatch.AssignRecruiterDto;
import com.fourdotfive.missioncontrol.dtos.job.MockJobProfileDto;
import com.fourdotfive.missioncontrol.dtos.jobmatch.FilterCandidateActivityDto;
import com.fourdotfive.missioncontrol.dtos.jobmatch.JobMatchCandidateJobDto;
import com.fourdotfive.missioncontrol.dtos.pdf.AssessmentReportInfoDto;
import com.fourdotfive.missioncontrol.exception.PlatformException;
import com.fourdotfive.missioncontrol.dtos.pdf.CandidateAssessmentReportDetails;
import com.fourdotfive.missioncontrol.pojo.candidate.*;
import com.fourdotfive.missioncontrol.security.FourDotFiveUser;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author shalini, chakravarthy
 */
@RestController
@RequestMapping("/api/candidate")
public class CandidateController {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(CandidateController.class);

    private final CandidateService candidateService;

    @Autowired
    public CandidateController(CandidateService candidateService) {
        this.candidateService = candidateService;
    }

    @RequestMapping(value = "updateactivecandidatestatus/{candidateid}", method = RequestMethod.PUT)
    public ResponseEntity<String> updateCandidateStatus(
            @PathVariable("candidateid") String candidateId,
            @RequestParam("statetype") ActiveCandidateJobStateType stateType,
            @RequestParam("steptype") ActiveCandidateJobStepType stepType) {

        candidateService.updateActiveCandidateStatus(candidateId, stateType,
                stepType);

        return new ResponseEntity<>(HttpStatus.OK);

    }

    @RequestMapping(value = "updatepassivecandidatestatus/{candidateid}", method = RequestMethod.PUT)
    public ResponseEntity<String> updateCandidateStatus(
            @PathVariable("candidateid") String candidateId,
            @RequestParam("statetype") PassiveCandidateJobStateType stateType,
            @RequestParam("steptype") PassiveCandidateJobStepType stepType) {

        candidateService.updatePassiveCandidateStatus(candidateId, stateType,
                stepType);

        return new ResponseEntity<>(HttpStatus.OK);

    }

    /**
     * to get active or passive candidates
     */
    @RequestMapping(value = "getactivecandidates", method = RequestMethod.POST)
    public ResponseEntity<List<ActiveCandidateMinDto>> getActiveCandidates(
            @RequestBody CandidateRequestDto requestDto) {

        List<ActiveCandidateMinDto> candidateList = candidateService
                .getActiveCandidates(requestDto);

        return new ResponseEntity<>(candidateList,
                HttpStatus.OK);
    }

    /**
     * to get active or passive candidates
     */
    @RequestMapping(value = "getpassivecandidates", method = RequestMethod.POST)
    public ResponseEntity<List<PassiveCandidateMinDto>> getPassiveCandidates(
            @RequestBody CandidateRequestDto requestDto) {

        LOGGER.debug("Fetching passive candidates");
        List<PassiveCandidateMinDto> candidateList = candidateService
                .getPassiveCandidates(requestDto);

        return new ResponseEntity<>(candidateList,
                HttpStatus.OK);
    }

    /**
     * to get all candidates
     */
    @RequestMapping(value = "getallcandidates", method = RequestMethod.POST)
    public ResponseEntity<List<CandidateMinInfoDto>> getAllCandidates(
            @RequestBody CandidateRequestDto requestDto) {

        List<CandidateMinInfoDto> candidateList = candidateService
                .getAllCandidates(requestDto);

        return new ResponseEntity<>(candidateList,
                HttpStatus.OK);
    }

    @RequestMapping(value = "findcandidatesforrequisition", method = {RequestMethod.POST})
    public @ResponseBody
    ResponseEntity<String> findCandidatesForRequisition(@RequestBody CandidateRequisitionDto candidateRequisitionDto) {
        String candidateMinInfoDtos = candidateService.findCandidatesForRequisition(candidateRequisitionDto);
        if (candidateMinInfoDtos == null)
            return new ResponseEntity<>(candidateMinInfoDtos, HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(candidateMinInfoDtos, HttpStatus.OK);
    }

    @RequestMapping(value = "getcandidatedetails/{candidateid}")
    public ResponseEntity<CandidateDetailsDto> getCandidateDetails(
            @PathVariable("candidateid") String candidateId) {

        CandidateDetailsDto dto = candidateService
                .getCandidateDetails(candidateId);

        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @RequestMapping(value = "claimcandidate/{candidateid}/{recruiterid}", method = RequestMethod.PUT)
    public ResponseEntity<String> cliamCandidate(
            @PathVariable("candidateid") String candidateId,
            @PathVariable("recruiterid") String recruiterId,
            @RequestParam("statetype") PassiveCandidateJobStateType stateType,
            @RequestParam("steptype") PassiveCandidateJobStepType stepType) {

        return new ResponseEntity<>(HttpStatus.OK);

    }

    @RequestMapping(value = "getjobprofile")
    public ResponseEntity<MockJobProfileDto> getJobProfile() {

        MockJobProfileDto response = candidateService.getMockJobProfile();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/findidnames", method = RequestMethod.POST)
    public ResponseEntity<String> getCandidateIdNamesByCompanyId(@RequestBody CandidateFilterParams filterParams) {
        String candidates = candidateService.getCandidateIdNames(filterParams);
        if (candidates == null)
            return new ResponseEntity<>(candidates, HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(candidates, HttpStatus.OK);
    }

    @RequestMapping(value = "/candidatecard/{candidateid}/{userid}", method = RequestMethod.POST)
    public ResponseEntity<CandidateCardDto> getCandidateCard(@RequestBody CandidateCardFilterParams filterParams,
                                                             @PathVariable String candidateid, @PathVariable String userid) {
        CandidateCardDto responseCandidate = candidateService.getCandidateCard(candidateid, userid, filterParams);
        if (responseCandidate == null)
            return new ResponseEntity<>(responseCandidate, HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(responseCandidate, HttpStatus.OK);
    }

    @RequestMapping(value = {"setstatus"}, method = RequestMethod.POST)
    public ResponseEntity<Candidate> SetStatus(@RequestBody JobStateChange jobStateChange) {
        Candidate responseCandidate = candidateService.setStatus(jobStateChange);
        try {
            return new ResponseEntity<>(responseCandidate, HttpStatus.OK);

        } catch (Exception exception) {
            exception.printStackTrace();
            return new ResponseEntity<>(new Candidate(), HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/candidateprofile/{candidateid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getCandidateProfileById(@PathVariable(value = "candidateid") String candidateId) {
        if (StringUtils.isBlank(candidateId)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        String candidateProfile = candidateService.getCandidateProfile(candidateId);
        return new ResponseEntity<>(candidateProfile, HttpStatus.OK);
    }

    @RequestMapping(value = "/schedulephonescreen", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> schedulePhoneScreen(@RequestBody MeetingScheduleDto meetingSchedule) {
        String candidate = candidateService.schedulePhoneScreen(meetingSchedule);
        return new ResponseEntity<>(candidate, HttpStatus.OK);
    }

    @RequestMapping(value = "/scheduleinterview", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> scheduleInterview(@RequestBody MeetingScheduleDto meetingSchedule) {
        String candidate = candidateService.scheduleInterview(meetingSchedule);
        return new ResponseEntity<>(candidate, HttpStatus.OK);
    }

    @RequestMapping(value = "/setreleased", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String setReleased(@RequestBody List<JobMatchCandidateJobDto> multipleCandidateJobStepChanges) {
        if (CollectionUtils.isEmpty(multipleCandidateJobStepChanges)) {
            throw new IllegalArgumentException("JobMatchId is not found");
        }
        return candidateService.setReleased(multipleCandidateJobStepChanges);
    }

    @RequestMapping(value = "/setnotinterested", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String setNotInterested(@RequestBody List<JobMatchCandidateJobDto> multipleCandidateJobStepChanges) {
        if (CollectionUtils.isEmpty(multipleCandidateJobStepChanges)) {
            throw new IllegalArgumentException("No JobMatch id found");
        }
        return candidateService.setNotInterested(multipleCandidateJobStepChanges);
    }

    @RequestMapping(value = "/sendvalueassessment", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String sendValueAssessment(@RequestBody AssessmentDto assessmentDto) {
        if (StringUtils.isEmpty(assessmentDto.getJobMatchId())) {
            throw new IllegalArgumentException("Job Match id not found");
        }
        return candidateService.sendValueAssessment(assessmentDto);
    }

    @RequestMapping(value = "/sendtechassessment", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String sendTechAssessment(@RequestBody AssessmentDto assessmentDto) {
        if (StringUtils.isEmpty(assessmentDto.getJobMatchId()))
            throw new IllegalArgumentException("JobMatch id not found");
        return candidateService.sendTechAssessment(assessmentDto);
    }

    @RequestMapping(value = "/cancelvalueassessment", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String cancelValueAssessment(@RequestBody CancelValueTechAssessmentDto cancelValueTechAssessment) {
        if (StringUtils.isEmpty(cancelValueTechAssessment.getJobMatchId()))
            throw new IllegalArgumentException("JobMatch id not found");
        return candidateService.cancelValueAssessment(cancelValueTechAssessment);
    }


    @RequestMapping(value = "/canceltechassessment", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String cancelTechAssessment(@RequestBody CancelValueTechAssessmentDto cancelValueTechAssessment) {
        if (StringUtils.isEmpty(cancelValueTechAssessment.getJobMatchId()))
            throw new IllegalArgumentException("JobMatch id not found");
        return candidateService.cancelTechAssessment(cancelValueTechAssessment);
    }

    @RequestMapping(value = "/movebackward", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String moveBackward(@RequestBody ChangeStepDto changeStep) {
        if (StringUtils.isEmpty(changeStep.getJobMatchId()))
            throw new IllegalArgumentException("JobMatch id not found");
        return candidateService.moveBackward(changeStep);
    }

    @RequestMapping(value = "/moveforward", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String moveForward(@RequestBody ChangeStepDto changeStep) {
        if (StringUtils.isEmpty(changeStep.getJobMatchId()))
            throw new IllegalArgumentException("JobMatch id not found");
        return candidateService.moveForward(changeStep);
    }

    @RequestMapping(value = "/movetoany", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String moveToAny(@RequestBody CandidateMoveStepDto candidateMoveStep) {
        if (StringUtils.isEmpty(candidateMoveStep.getJobMatchId()))
            throw new IllegalArgumentException("JobMatch id not found");
        return candidateService.moveToAny(candidateMoveStep);
    }

    @RequestMapping(value = "/statuscount/{userid}/{companyid}", method = RequestMethod.GET)
    public ResponseEntity<CandidateStatusCountDto> candidateStatusCount(
            @PathVariable String companyid, @PathVariable String userid,
            @RequestParam(value = "visibility", required = false, defaultValue = "") String visibility) {
        CandidateStatusCountDto response = candidateService.getCandidateStatusCount(userid, companyid, visibility);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/reschedulemeeting/{meetingscheduleid}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String rescheduleMeeting(@PathVariable(value = "meetingscheduleid") String meetingScheduleId, @RequestBody MeetingScheduleUpdateDto meetingScheduleUpdateDto) {
        if (StringUtils.isEmpty(meetingScheduleId)) {
            throw new IllegalArgumentException("No Meeting Schedule id found");
        }
        return candidateService.rescheduleMeeting(meetingScheduleId, meetingScheduleUpdateDto);
    }

    @RequestMapping(value = "/cancelscheduledmeeting/{meetingscheduleid}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public String cancelScheduledMeeting(@PathVariable(value = "meetingscheduleid") String meetingScheduleId, @RequestBody CancelMeetingDto cancelMeetingDto) {
        if (StringUtils.isEmpty(meetingScheduleId)) {
            throw new IllegalArgumentException("No  Meeting Schedule id found");
        }
        return candidateService.cancelScheduledMeeting(meetingScheduleId, cancelMeetingDto);
    }

    @RequestMapping(value = "/candidatepdfreport", method = RequestMethod.GET, produces = MediaType.APPLICATION_PDF_VALUE)
    public byte[] getCandidatePdfReport(@RequestParam(value = "testinvitationid") Long testInvitationId,
                                        @RequestParam(value = "sectional", required = false, defaultValue = "false") boolean sectional,
                                        HttpServletResponse response) {
        CandidateAssessmentDto candidateAssessmentDto = candidateService.getCandidateAssessmentDetails(testInvitationId);
        String fileName = candidateAssessmentDto.getCandidateName() + "-" + candidateAssessmentDto.getTestName() + ".pdf";
        if (sectional) {
            fileName = String.format("%s-%s-sectional-graph-report.pdf", candidateAssessmentDto.getCandidateName(), candidateAssessmentDto.getTestName());
        }
        response.setHeader("Content-Disposition", "filename=" + fileName);
        return candidateService.getCandidatePdfReport(testInvitationId, sectional);
    }

    @RequestMapping(value = "/recruiters/{companyId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getRecruiters(@PathVariable String companyId,
                                @RequestBody List<String> jobMatchIds) {
        if (CollectionUtils.isEmpty(jobMatchIds))
            throw new IllegalArgumentException("No JobMatch id's found");
        return candidateService.getRecruiters(jobMatchIds, companyId);
    }

    @RequestMapping(value = "/setrecruiter", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String setRecruiter(@RequestBody Map<String, String> jobMatchAndRecruiterIds) {
        if (CollectionUtils.isEmpty(jobMatchAndRecruiterIds))
            throw new IllegalArgumentException("No job match and recruiter ids found");
        FourDotFiveUser principal = (FourDotFiveUser) (SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        String userName = null;
        if (principal != null)
            userName = principal.getUsername();
        AssignRecruiterDto assignRecruiterDto = new AssignRecruiterDto(userName, jobMatchAndRecruiterIds);
        return candidateService.setRecruiter(assignRecruiterDto);
    }

    @PreAuthorize("@featureAccessControlServiceImpl.isAddCandidateFeatureAvailable(principal)")
    @RequestMapping(value = "/addcandidates", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String addCandidates(@RequestBody List<CandidateContactDetailDTO> candidateDetails) {
        if (CollectionUtils.isEmpty(candidateDetails))
            throw new PlatformException("Candidate list is null");
        return candidateService.addCandidates(candidateDetails);
    }

    @RequestMapping(value = {"/resendassessment"}, method = RequestMethod.POST)
    public String resendTechAssessment(@RequestBody AssessmentDto assessmentDto) {
        return candidateService.resendAssessment(assessmentDto);
    }

    @RequestMapping(value = "/getcandidateassessmentdetail", method = RequestMethod.POST)
    public String getCandidateAssessmentDetail(@RequestBody CandidateAssessmentDetailRequestDto candidateAssessmentDetailRequestDto) {
        return candidateService.getCandidateAssessmentDetail(candidateAssessmentDetailRequestDto);
    }

    @RequestMapping(value = {"/setassessmentleft"}, method = RequestMethod.POST)
    public String setAssessmentLeft(@RequestBody AssessmentChangeStatusDto assessmentChangeStatus) {
        return candidateService.setAssessmentLeft(assessmentChangeStatus);
    }

    @RequestMapping(value = {"/sentassessmentdetails/{jobMatchId}"}, method = RequestMethod.GET)
    public String getAssessmentSentDetails(@PathVariable("jobMatchId") String jobMatchId,
                                           @RequestParam("assessmentType") CandidateAssessmentTestType testType) {
        return candidateService.getAssessmentSentDetails(jobMatchId, testType);
    }

    @RequestMapping(value = {"/getavailabletimezones"}, method = RequestMethod.GET)
    public String getAvailableTimeZones() {
        return candidateService.getAvailableTimeZones();
    }

    @RequestMapping(value = {"/candidateActivity"}, method = RequestMethod.POST)
    public ResponseEntity getCandidateActivity(@RequestBody FilterCandidateActivityDto filterCandidateActivityDto) {
        List<CandidateActivityDto> response = candidateService.getCandidateActivity(filterCandidateActivityDto);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = {"/getcandidateclientorbus/{candidateId}"}, method = RequestMethod.GET)
    public String getCandidateClientOrBUs(@PathVariable("candidateId") String candidateId) {
        return candidateService.getCandidateClientOrBUs(candidateId);
    }

    @RequestMapping(value = {"/getcandidatejobandrole/{candidateId}"}, method = RequestMethod.GET)
    public String getCandidateJobAndRole(@PathVariable("candidateId") String candidateId) {
        return candidateService.getCandidateJobAndRole(candidateId);
    }

    @RequestMapping(value = {"/setcandidateavailability"}, method = RequestMethod.POST)
    public String setCandidateAvailability(@RequestBody CandidateAvailabilityDTO candidateAvailabilityDTO) {
        if (StringUtils.isBlank(candidateAvailabilityDTO.getCandidateId())) {
            throw new PlatformException("Candidate id can not be null");
        }
        return candidateService.setCandidateAvailability(candidateAvailabilityDTO);
    }

    @RequestMapping(value = {"/getdaxtraaddedcandidateswithlessscore/{jobId}/{daxtraRequestId}"}, method = RequestMethod.GET)
    public String getDaxtraAddedCandidatesWithLessScore(@PathVariable("jobId") String jobId,
                                                        @PathVariable("daxtraRequestId") String daxtraRequestId) {
        return candidateService.getDaxtraAddedCandidatesWithLessScore(jobId, daxtraRequestId);
    }

    @RequestMapping(value = {"/delete"}, method = RequestMethod.DELETE)
    public String deleteCandidate(@RequestBody DeleteDTO deleteDTO) {
        if (deleteDTO == null) {
            throw new PlatformException("can not delete the candidate.");
        }
        if (StringUtils.isEmpty(deleteDTO.getId())) {
            throw new PlatformException("Candidate id can not be null.");
        }
        return candidateService.deleteCandidate(deleteDTO);
    }

    @RequestMapping(value = {"/deletecandidateonresumeremove/{resumeId}"}, method = RequestMethod.DELETE)
    public String deleteCandidateOnResumeRemove(@PathVariable("resumeId") String resumeId) {
        if (StringUtils.isEmpty(resumeId)) {
            throw new PlatformException("Candidate resume id can not be null.");
        }
        return candidateService.deleteCandidateOnResumeRemove(resumeId);
    }

    @RequestMapping(value = {"/addorupdatecontactemail"}, method = RequestMethod.POST)
    public String addOrUpdateContactEmail(@RequestBody ContactDetailsDTO contactDetailsDTO) {
        if (contactDetailsDTO == null || StringUtils.isEmpty(contactDetailsDTO.getEmailId())) {
            throw new PlatformException("Email address can not be null.");
        }
        return candidateService.addOrUpdateContactEmail(contactDetailsDTO);
    }

    @RequestMapping(value = {"/addorupdatecontactphone"}, method = RequestMethod.POST)
    public String addOrUpdateContactPhone(@RequestBody ContactDetailsDTO contactDetailsDTO) {
        if (contactDetailsDTO == null || StringUtils.isEmpty(contactDetailsDTO.getPhoneNumber())) {
            throw new PlatformException("Phone number can not be null.");
        }
        return candidateService.addOrUpdateContactPhoneNumber(contactDetailsDTO);
    }

    @RequestMapping(value = {"/addorupdatecontactaddress"}, method = RequestMethod.POST)
    public String addOrUpdateContactAddress(@RequestBody ContactDetailsDTO contactDetailsDTO) {
        if (contactDetailsDTO == null || contactDetailsDTO.getAddress() == null) {
            throw new PlatformException("Contact address can not be null.");
        }
        return candidateService.addOrUpdateContactAddress(contactDetailsDTO);
    }

    @RequestMapping(value = "/statuscountforcompany/{companyId}/{userId}", method = RequestMethod.GET)
    public ResponseEntity<CandidatesCountDto> candidateStatusCountForCompany(
            @PathVariable String companyId,
            @PathVariable String userId,
            @RequestParam(value = "visibility", required = false) String visibility) {
        CandidatesCountDto response = candidateService.getCandidateStatusCountByCompany(userId, companyId, visibility);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = {"/getallpendingmeetingsandassessments"}, method = RequestMethod.POST)
    public String getAllPendingMeetingsAndAssessments(@RequestBody List<String> jobMatchIds) {
        if (CollectionUtils.isEmpty(jobMatchIds))
            throw new PlatformException("Job match ids are not found.");
        return candidateService.getAllPendingMeetingsAndAssessments(jobMatchIds);
    }

    @RequestMapping(value = {"updatependingmeetingsandassessments"}, method = RequestMethod.POST)
    public String updatePendingMeetingsAndAssessments(@RequestBody List<String> jobMatchIds) {
        if (CollectionUtils.isEmpty(jobMatchIds))
            throw new PlatformException("Job match ids are not found.");
        return candidateService.updatePendingMeetingsAndAssessments(jobMatchIds);
    }


    @RequestMapping(value = "/sendvalueassessments", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String sendValueAssessments(@RequestBody AssessmentListDto assessmentListDto) {
        if (CollectionUtils.isEmpty(assessmentListDto.getJobMatchIds())) {
            throw new IllegalArgumentException("Job Match id not found");
        }
        if (assessmentListDto.getTestType() == null) {
            assessmentListDto.setTestType(CandidateAssessmentTestType.Value);
        }
        return candidateService.sendValueAssessments(assessmentListDto);
    }

    @RequestMapping(value = "/sendtechassessments", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String sendTechAssessments(@RequestBody AssessmentListDto assessmentListDto) {
        if (CollectionUtils.isEmpty(assessmentListDto.getJobMatchIds())) {
            throw new IllegalArgumentException("JobMatch ids not found");
        }
        if (assessmentListDto.getTestType() == null) {
            assessmentListDto.setTestType(CandidateAssessmentTestType.Technical);
        }
        return candidateService.sendTechAssessments(assessmentListDto);
    }

    @RequestMapping(value = "/cancelvalueassessments", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String cancelValueAssessments(@RequestBody CancelValueTechAssessmentListDto cancelValueTechAssessmentListDto) {
        if (CollectionUtils.isEmpty(cancelValueTechAssessmentListDto.getJobMatchIds())) {
            throw new IllegalArgumentException("JobMatch ids not found");
        }
        if (cancelValueTechAssessmentListDto.getTestType() == null) {
            cancelValueTechAssessmentListDto.setTestType(CandidateAssessmentTestType.Value);
        }
        return candidateService.cancelValueAssessments(cancelValueTechAssessmentListDto);
    }


    @RequestMapping(value = "/canceltechassessments", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String cancelTechAssessments(@RequestBody CancelValueTechAssessmentListDto cancelValueTechAssessmentListDto) {
        if (CollectionUtils.isEmpty(cancelValueTechAssessmentListDto.getJobMatchIds())) {
            throw new IllegalArgumentException("JobMatch ids not found");
        }
        if (cancelValueTechAssessmentListDto.getTestType() == null) {
            cancelValueTechAssessmentListDto.setTestType(CandidateAssessmentTestType.Technical);
        }
        return candidateService.cancelTechAssessments(cancelValueTechAssessmentListDto);
    }

    @PreAuthorize("@accessControlImp.canViewReports(principal)")
    @RequestMapping(value = "updatecandidatereportinfo/{candidateAssessmentDetailId}", method = {RequestMethod.POST})
    public @ResponseBody
    ResponseEntity<Map<String, String>> updateCandidateReportInfo(@PathVariable("candidateAssessmentDetailId") String candidateAssessmentDetailId,
                                                                  @RequestBody AssessmentReportInfoDto assessmentReportInfoDto) throws Exception {
        if (assessmentReportInfoDto == null) {
            throw new PlatformException("Report Details cannot be null.");
        }

        String response = candidateService.updateCandidateReportInfo(candidateAssessmentDetailId, assessmentReportInfoDto);
        Map<String, String> responseMessage = new HashMap<>();
        responseMessage.put("statusMessage", response);
        return new ResponseEntity<>(responseMessage, HttpStatus.OK);
    }

//    @PreAuthorize("@accessControlImp.canViewReports(principal)")
//    @RequestMapping(value = "updatecandidatereportdetails", method = {RequestMethod.POST})
//    public @ResponseBody
//    ResponseEntity<CandidateAssessmentReportDetails> updateCandidateReportDetails(
//            @RequestBody CandidateAssessmentReportDetails candidateAssessmentReportDetails) throws Exception {
//        if (candidateAssessmentReportDetails == null) {
//            throw new PlatformException("Report Details cannot be null.");
//        }
//
//        CandidateAssessmentReportDetails response = candidateService.updateCandidateReportDetails(candidateAssessmentReportDetails);
//
//        return new ResponseEntity<>(response, HttpStatus.OK);
//    }

    @PreAuthorize("@accessControlImp.canViewReports(principal)")
    @RequestMapping(value = "deletecommentfromcandidatereportdetails/{candidateAssessmentDetailId}", method = {RequestMethod.GET})
    public @ResponseBody
    ResponseEntity<CandidateAssessmentReportDetails> deleteCommentFromCandidateReportDetails(
            @PathVariable("candidateAssessmentDetailId") String candidateAssessmentDetailId, @RequestParam(value = "commentId", required = false) String commentId) throws Exception {
        if (StringUtils.isEmpty(candidateAssessmentDetailId)) {
            throw new PlatformException("Assessment Id cannot be null.");
        }
//        if (StringUtils.isEmpty(commentId)) {
//            throw new PlatformException("Comment Id cannot be null.");
//        }
        CandidateAssessmentReportDetails response = candidateService.deleteCommentFromCandidateReportDetails(candidateAssessmentDetailId, commentId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = {"/addorupdatecandidateadditionaldetails"}, method = RequestMethod.POST)
    public String addOrUpdateCandidateAdditionalDetails(@RequestBody UpdateCandidateAdditionalDetailsDto candidateAdditionalDetailsDto) {
        if (candidateAdditionalDetailsDto == null || StringUtils.isEmpty(candidateAdditionalDetailsDto.getCandidateId())) {
            throw new PlatformException("Candidate details can not be null.");
        }
        return candidateService.addOrUpdateCandidateAdditionalDetails(candidateAdditionalDetailsDto);
    }

    @RequestMapping(value = "/allowoverride/{candidateid}", method = RequestMethod.GET)
    public String allowOverride(@PathVariable String candidateId) {
        return candidateService.allowOverride(candidateId);
    }

    @RequestMapping(value = {"cancelassessmentsonassessmentupdate/{currentCompanyId}"}, method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<String> cancelAssessmentsOnAssessmentUpdate(@RequestBody List<String> candidateAssessmentIds,
                                                               @PathVariable("currentCompanyId") String currentCompanyId) {
        candidateService.cancelAssessmentsOnAssessmentUpdate(candidateAssessmentIds, currentCompanyId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = {"validatemeetingstartdateandenddate/{meetingScheduleId}"}, method = RequestMethod.GET)
    public @ResponseBody
    String validateMeetingStartDateAndEndDate(@PathVariable("meetingScheduleId") String meetingScheduleId) {
        if (StringUtils.isEmpty(meetingScheduleId))
            throw new PlatformException("'meetingScheduleId' can't be null");

        return candidateService.validateMeetingStartDateAndEndDate(meetingScheduleId);
    }
}