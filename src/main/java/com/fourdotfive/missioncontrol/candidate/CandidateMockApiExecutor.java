package com.fourdotfive.missioncontrol.candidate;

import java.util.List;

import com.fourdotfive.missioncontrol.dtos.candidate.ActiveCandidateMinDto;
import com.fourdotfive.missioncontrol.dtos.candidate.CandidateDetailsDto;
import com.fourdotfive.missioncontrol.dtos.candidate.CandidateMinInfoDto;
import com.fourdotfive.missioncontrol.dtos.candidate.PassiveCandidateMinDto;
import com.fourdotfive.missioncontrol.dtos.job.MockJobProfileDto;
import com.fourdotfive.missioncontrol.pojo.candidate.CandidateState;

public interface CandidateMockApiExecutor {

	List<ActiveCandidateMinDto> getAciveCandidates(CandidateState candidateState);

	List<PassiveCandidateMinDto> getPassiveCandidates(
			CandidateState candidateState);

	List<CandidateMinInfoDto> getAllCandidates();

	CandidateDetailsDto getCandidateDetails(String candidateId);

	/**
	 * To Get Job details, this method is called.
	 * 
	 * @return response from platform
	 */
	MockJobProfileDto getMockJobProfile();

}
