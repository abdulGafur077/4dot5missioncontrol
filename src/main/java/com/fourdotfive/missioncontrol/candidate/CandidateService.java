package com.fourdotfive.missioncontrol.candidate;

import com.fourdotfive.missioncontrol.dtos.candidate.*;
import com.fourdotfive.missioncontrol.dtos.job.NewJobDto;
import com.fourdotfive.missioncontrol.dtos.jobmatch.AssignRecruiterDto;
import com.fourdotfive.missioncontrol.dtos.job.MockJobProfileDto;
import com.fourdotfive.missioncontrol.dtos.jobmatch.FilterCandidateActivityDto;
import com.fourdotfive.missioncontrol.dtos.jobmatch.JobMatchCandidateJobDto;
import com.fourdotfive.missioncontrol.dtos.pdf.AssessmentReportInfoDto;
import com.fourdotfive.missioncontrol.dtos.user.DashBoardDto;
import com.fourdotfive.missioncontrol.dtos.pdf.CandidateAssessmentReportDetails;
import com.fourdotfive.missioncontrol.pojo.candidate.*;

import java.util.List;

public interface CandidateService {

    List<ActiveCandidateMinDto> getActiveCandidates(
            CandidateRequestDto requestDto);

    List<PassiveCandidateMinDto> getPassiveCandidates(
            CandidateRequestDto requestDto);

    CandidateDetailsDto getCandidateDetails(String candidateId);

    void updateActiveCandidateStatus(String candidateId,
                                     ActiveCandidateJobStateType stateType,
                                     ActiveCandidateJobStepType stepType);

    void updatePassiveCandidateStatus(String candidateId,
                                      PassiveCandidateJobStateType stateType,
                                      PassiveCandidateJobStepType stepType);

    List<CandidateMinInfoDto> getAllCandidates(CandidateRequestDto requestDto);

    DashBoardDto getCandidateCount(String userId);

    /**
     * To Get Job details, this method is called.
     *
     * @return response from platform
     */
    MockJobProfileDto getMockJobProfile();

    String getCandidateIdNames(CandidateFilterParams filterParams);

    CandidateCardDto getCandidateCard(String candidateId, String loggedInUserId, CandidateCardFilterParams filterParams);

    Candidate setStatus(JobStateChange jobStateChange);

    String getCandidateProfile(String candidateId);

    String schedulePhoneScreen(MeetingScheduleDto meetingSchedule);

    String scheduleInterview(MeetingScheduleDto meetingSchedule);

    String setReleased(List<JobMatchCandidateJobDto> multipleCandidateJobStepChanges);

    String setNotInterested(List<JobMatchCandidateJobDto> multipleCandidateJobStepChanges);

    String sendValueAssessment(AssessmentDto assessmentDto);

    String sendTechAssessment(AssessmentDto assessmentDto);

    String cancelValueAssessment(CancelValueTechAssessmentDto cancelValueTechAssessmentDto);

    String cancelTechAssessment(CancelValueTechAssessmentDto cancelValueTechAssessmentDto);

    String moveForward(ChangeStepDto changeStep);

    String moveBackward(ChangeStepDto changeStep);

    String moveToAny(CandidateMoveStepDto candidateMoveStep);

    CandidateStatusCountDto getCandidateStatusCount(String userId, String companyId, String visibility);

    String rescheduleMeeting(String meetingScheduleId, MeetingScheduleUpdateDto meetingScheduleUpdateDto);

    String cancelScheduledMeeting(String meetingScheduleId, CancelMeetingDto cancelMeetingDto);

    byte[] getCandidatePdfReport(Long testInvitationId, boolean sectional);

    String getRecruiters(List<String> jobMatchIds, String companyId);

    String setRecruiter(AssignRecruiterDto assignRecruiterDto);

    String addCandidates(List<CandidateContactDetailDTO> candidateDetails);

    String findCandidatesForRequisition(CandidateRequisitionDto candidateRequisitionDto);

    String resendAssessment(AssessmentDto assessment);

    String getCandidateAssessmentDetail(CandidateAssessmentDetailRequestDto candidateAssessmentDetailRequestDto);

    String setAssessmentLeft(AssessmentChangeStatusDto assessmentChangeStatusDto);

    String getAssessmentSentDetails(String jobMatchId, CandidateAssessmentTestType testType);

    String getAvailableTimeZones();

    List<CandidateActivityDto> getCandidateActivity(FilterCandidateActivityDto filterCandidateActivityDto);

    String getCandidateClientOrBUs(String candidateId);

    String getCandidateJobAndRole(String candidateId);

    String setCandidateAvailability(CandidateAvailabilityDTO candidateAvailabilityDTO);

    String getDaxtraAddedCandidatesWithLessScore(String jobId, String daxtraRequestId);

    String deleteCandidate(DeleteDTO deleteDTO);

    String deleteCandidateOnResumeRemove(String resumeId);

    String addOrUpdateContactEmail(ContactDetailsDTO contactDetailsDTO);

    String addOrUpdateContactPhoneNumber(ContactDetailsDTO contactDetailsDTO);

    String addOrUpdateContactAddress(ContactDetailsDTO contactDetailsDTO);

    CandidateAssessmentDto getCandidateAssessmentDetails(Long testInvitationId);

    CandidatesCountDto getCandidateStatusCountByCompany(String userId, String companyId, String visibilty);

    CandidatesEnabledStepDto getCandidatesPresentOnStep(String companyId, String userId, String jobId);

    String getAllPendingMeetingsAndAssessments(List<String> jobMatchIds);

    String updatePendingMeetingsAndAssessments(List<String> jobMatchIds);

    String sendValueAssessments(AssessmentListDto assessmentListDto);

    String sendTechAssessments(AssessmentListDto assessmentListDto);

    String cancelValueAssessments(CancelValueTechAssessmentListDto cancelValueTechAssessmentListDto);

    String cancelTechAssessments(CancelValueTechAssessmentListDto cancelValueTechAssessmentListDto);

    CandidateAssessmentReportDetails updateCandidateReportDetails(CandidateAssessmentReportDetails candidateAssessmentReportDetails);

    CandidateAssessmentReportDetails deleteCommentFromCandidateReportDetails(String candidateAssessmentDetailId, String commentId);

    CandidatesEnabledStepDto getCandidatesPresentOnStepByClientOrBuId(String companyId, String userId, List<NewJobDto> newJobDtos);

    CandidatesEnabledStepDto getCandidatesPresentOnStepByCompanyId(String companyId, String userId, List<String> allClientsOrBu);

    String addOrUpdateCandidateAdditionalDetails(UpdateCandidateAdditionalDetailsDto candidateAdditionalDetailsDto);

    String allowOverride(String candidateId);

    String updateCandidateReportInfo(String candidateAssessmentDetailId, AssessmentReportInfoDto assessmentReportInfoDto);

    String cancelAssessmentsOnAssessmentUpdate(List<String> candidateAssessmentIds, String currentCompanyId);

    String validateMeetingStartDateAndEndDate(String meetingScheduleId);

    CandidateDetailsResponseDTO getCandidateDetailsById(String candidateId);
}
