package com.fourdotfive.missioncontrol.candidate;

import com.fourdotfive.missioncontrol.common.AccessUtil;
import com.fourdotfive.missioncontrol.common.ApiConstants;
import com.fourdotfive.missioncontrol.dtos.candidate.*;
import com.fourdotfive.missioncontrol.dtos.job.NewJobDto;
import com.fourdotfive.missioncontrol.dtos.jobmatch.AssignRecruiterDto;
import com.fourdotfive.missioncontrol.dtos.job.MockJobProfileDto;
import com.fourdotfive.missioncontrol.dtos.jobmatch.CandidateJobMatchInfoDto;
import com.fourdotfive.missioncontrol.dtos.jobmatch.FilterCandidateActivityDto;
import com.fourdotfive.missioncontrol.dtos.jobmatch.JobMatchCandidateJobDto;
import com.fourdotfive.missioncontrol.dtos.pdf.AssessmentReportInfoDto;
import com.fourdotfive.missioncontrol.dtos.user.DashBoardDto;
import com.fourdotfive.missioncontrol.dtos.pdf.CandidateAssessmentReportDetails;
import com.fourdotfive.missioncontrol.pojo.candidate.*;
import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Shalini
 */
@Service
public class CandidateServiceImp implements CandidateService {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(CandidateServiceImp.class);

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private CandidateApiExecutor candidateApiExecutor;

    @Autowired
    private CandidateMockApiExecutor candidateMockApiExecutor;

    @Autowired
    private UserService userService;

    @Autowired
    private AccessUtil accessUtil;

    @Override
    public void updateActiveCandidateStatus(String candidateId,
                                            ActiveCandidateJobStateType stateType,
                                            ActiveCandidateJobStepType stepType) {

        String url = ApiConstants.UPDATE_CANDIDATE_STATUS;

        ResponseEntity<String> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.PUT, null,
                    String.class);
        } catch (HttpClientErrorException e) {
            LOGGER.error("An error occured while updating candidate status {}",
                    e.getMessage());
        }

        LOGGER.debug("Candidate - {} status updated successfully {}",
                candidateId, response);
    }

    @Override
    public void updatePassiveCandidateStatus(String candidateId,
                                             PassiveCandidateJobStateType stateType,
                                             PassiveCandidateJobStepType stepType) {

        String url = ApiConstants.UPDATE_CANDIDATE_STATUS;

        ResponseEntity<String> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.PUT, null,
                    String.class);
        } catch (HttpClientErrorException e) {
            LOGGER.error("An error occured while updating candidate status {}",
                    e.getMessage());
        }

        LOGGER.debug("Candidate - {} status updated successfully {}",
                candidateId, response);
    }

    @Override
    public List<ActiveCandidateMinDto> getActiveCandidates(
            CandidateRequestDto requestDto) {

        return candidateMockApiExecutor
                .getAciveCandidates(CandidateState.ACTIVE);
    }

    @Override
    public List<PassiveCandidateMinDto> getPassiveCandidates(
            CandidateRequestDto requestDto) {

        return candidateMockApiExecutor
                .getPassiveCandidates(CandidateState.PASSIVE);

    }

    @Override
    public CandidateDetailsDto getCandidateDetails(String candidateId) {
        return candidateMockApiExecutor.getCandidateDetails(candidateId);
    }

    @Override
    public List<CandidateMinInfoDto> getAllCandidates(
            CandidateRequestDto requestDto) {

        return candidateMockApiExecutor
                .getAllCandidates();

    }

    @Override
    public DashBoardDto getCandidateCount(String userId) {
        return candidateApiExecutor.getCandidateCount(userId);
    }

    @Override
    public MockJobProfileDto getMockJobProfile() {
        return candidateMockApiExecutor.getMockJobProfile();
    }

    @Override
    public String getCandidateIdNames(CandidateFilterParams filterParams) {
        User user = userService.getUserById(filterParams.getLoggedInUserId());
        if (filterParams.getCandidateType() == null) {
            filterParams.setCandidateType(CandidateType.VENDOR_CANDIDATES);
        }

        if (user.getRole() != null) {
            if (accessUtil.isSuperUser(user.getRole().getId())
                    || accessUtil.isFourDot5Admin(user.getRole())
                    || accessUtil.isCompanyAdmin(user.getRole())) {
                return candidateApiExecutor.getCandidateIdNamesByCompany(filterParams);
            } else {
                List<String> users = new ArrayList<>();
                users.add(filterParams.getLoggedInUserId());
                filterParams.setUsers(users);
                return candidateApiExecutor.getCandidateIdNamesByUser(filterParams);
            }
        }
        return null;
    }

    @Override
    public CandidateCardDto getCandidateCard(String candidateId, String loggedInUserId, CandidateCardFilterParams filterParams) {
        User user = userService.getUserById(loggedInUserId);
        if (filterParams.getCandidateType() == null) {
            filterParams.setCandidateType(CandidateType.OWN_CANDIDATES);
        }
        if (user.getRole() != null) {
            if (accessUtil.isSuperUser(user.getRole().getId())
                    || accessUtil.isFourDot5Admin(user.getRole())
                    || accessUtil.isCompanyAdmin(user.getRole())) {
                return candidateApiExecutor.getCandidateCard(candidateId, loggedInUserId, filterParams);
            } else {
                List<String> users = new ArrayList<>();
                users.add(loggedInUserId);
                filterParams.setUserIds(users);
                return candidateApiExecutor.getCandidateCard(candidateId, loggedInUserId, filterParams);
            }
        }
        return null;
    }

    @Override
    public Candidate setStatus(JobStateChange jobStateChange) {
        return candidateApiExecutor.setStatus(jobStateChange);
    }

    @Override
    public String getCandidateProfile(String candidateId) {
        return candidateApiExecutor.getCandidateProfile(candidateId);
    }

    @Override
    public String schedulePhoneScreen(MeetingScheduleDto meetingSchedule) {
        User currentLoggedInUser = userService.getCurrentLoggedInUser();
        CandidateMeetingScheduleDto candidateMeetingScheduleDto = new CandidateMeetingScheduleDto(meetingSchedule, currentLoggedInUser.getId());
        return candidateApiExecutor.schedulePhoneScreen(candidateMeetingScheduleDto);
    }


    @Override
    public String scheduleInterview(MeetingScheduleDto meetingSchedule) {
        User currentLoggedInUser = userService.getCurrentLoggedInUser();
        CandidateMeetingScheduleDto candidateMeetingScheduleDto = new CandidateMeetingScheduleDto(meetingSchedule, currentLoggedInUser.getId());
        return candidateApiExecutor.scheduleInterview(candidateMeetingScheduleDto);
    }

    @Override
    public String setReleased(List<JobMatchCandidateJobDto> multipleCandidateJobStepChanges) {
        return candidateApiExecutor.setReleased(getCandidateJobMatchInfo(multipleCandidateJobStepChanges));
    }

    @Override
    public String setNotInterested(List<JobMatchCandidateJobDto> multipleCandidateJobStepChanges) {
        return candidateApiExecutor.setNotInterested(getCandidateJobMatchInfo(multipleCandidateJobStepChanges));
    }

    @Override
    public String sendValueAssessment(AssessmentDto assessmentDto) {
        User user = userService.getCurrentLoggedInUser();
        if (user == null) {
            throw new IllegalArgumentException("User not found");
        }
        SendAssessmentDto sendAssessmentDto = new SendAssessmentDto(assessmentDto);
        sendAssessmentDto.setLoggedInUserId(user.getId());
        return candidateApiExecutor.sendValueAssessment(sendAssessmentDto);
    }

    @Override
    public String sendTechAssessment(AssessmentDto assessmentDto) {
        User user = userService.getCurrentLoggedInUser();
        if (user == null) {
            throw new IllegalArgumentException("User not found");
        }
        SendAssessmentDto sendAssessmentDto = new SendAssessmentDto(assessmentDto);
        sendAssessmentDto.setLoggedInUserId(user.getId());
        return candidateApiExecutor.sendTechAssessment(sendAssessmentDto);
    }

    @Override
    public String cancelValueAssessment(CancelValueTechAssessmentDto cancelValueTechAssessmentDto) {
        User user = userService.getCurrentLoggedInUser();
        if (user == null) {
            throw new IllegalArgumentException("User not found");
        }
        CancelAssessmentDto cancelAssessmentDto = new CancelAssessmentDto(cancelValueTechAssessmentDto);
        cancelAssessmentDto.setLoggedInUserId(user.getId());
        return candidateApiExecutor.cancelValueAssessment(cancelAssessmentDto);
    }

    @Override
    public String cancelTechAssessment(CancelValueTechAssessmentDto cancelValueTechAssessmentDto) {
        User user = userService.getCurrentLoggedInUser();
        if (user == null) {
            throw new IllegalArgumentException("User not found");
        }
        CancelAssessmentDto cancelAssessmentDto = new CancelAssessmentDto(cancelValueTechAssessmentDto);
        cancelAssessmentDto.setLoggedInUserId(user.getId());
        return candidateApiExecutor.cancelTechAssessment(cancelAssessmentDto);
    }

    @Override
    public String moveForward(ChangeStepDto changeStep) {
        User loggedInUser = userService.getCurrentLoggedInUser();
        if (loggedInUser != null) {
            changeStep.setUserId(loggedInUser.getId());
        }
        return candidateApiExecutor.moveForward(changeStep);
    }

    @Override
    public String moveBackward(ChangeStepDto changeStep) {
        User loggedInUser = userService.getCurrentLoggedInUser();
        if (loggedInUser != null) {
            changeStep.setUserId(loggedInUser.getId());
        }
        return candidateApiExecutor.moveBackward(changeStep);
    }

    @Override
    public String moveToAny(CandidateMoveStepDto candidateMoveStep) {
        User loggedInUser = userService.getCurrentLoggedInUser();
        if (loggedInUser != null) {
            candidateMoveStep.setUserId(loggedInUser.getId());
        }
        return candidateApiExecutor.moveToAny(candidateMoveStep);
    }

    @Override
    public CandidateStatusCountDto getCandidateStatusCount(String userId, String companyId, String visibility) {
        User user = userService.getUserById(userId);
        if (user.getRole() != null) {
            if (accessUtil.isSuperUser(user.getRole().getId())
                    || accessUtil.isFourDot5Admin(user.getRole())
                    || accessUtil.isCompanyAdmin(user.getRole())) {
                if (visibility == null || visibility.isEmpty())
                    visibility = "unassigned,assignedToOthers";
                return candidateApiExecutor.getCandidateStatusCountByCompany(companyId, visibility, userId);
            } else {
                if (visibility == null || visibility.isEmpty())
                    visibility = "unassigned,assignedToMe";
                return candidateApiExecutor.getCandidateStatusCountByUser(userId, companyId, visibility);
            }
        }
        return null;
    }

    private List<CandidateJobMatchInfoDto> getCandidateJobMatchInfo(List<JobMatchCandidateJobDto> multipleCandidateJobStepChanges) {
        List<CandidateJobMatchInfoDto> candidateJobMatchInfoDtos = new ArrayList<>();
        User user = userService.getCurrentLoggedInUser();
        for (JobMatchCandidateJobDto jobMatchCandidateJobDto : multipleCandidateJobStepChanges) {
            CandidateJobMatchInfoDto candidateJobMatchInfoDto = new CandidateJobMatchInfoDto();
            candidateJobMatchInfoDto.setJobMatchId(jobMatchCandidateJobDto.getJobMatchId());
            candidateJobMatchInfoDto.setLoggedInUserId(user.getId());
            candidateJobMatchInfoDto.setNotes(jobMatchCandidateJobDto.getNotes());
            candidateJobMatchInfoDto.setCompanyId(jobMatchCandidateJobDto.getCompanyId());
            candidateJobMatchInfoDto.setExternalNote(jobMatchCandidateJobDto.getExternalNote());
            candidateJobMatchInfoDto.setReasonCodeDTO(jobMatchCandidateJobDto.getReasonCodeDTO());
            candidateJobMatchInfoDtos.add(candidateJobMatchInfoDto);
        }
        return candidateJobMatchInfoDtos;
    }

    @Override
    public String rescheduleMeeting(String meetingScheduleId, MeetingScheduleUpdateDto meetingScheduleUpdateDto) {
        User user = userService.getCurrentLoggedInUser();
        String loggedInUserId = user.getId();
        meetingScheduleUpdateDto.setLoggedInUserId(loggedInUserId);
        return candidateApiExecutor.rescheduleMeeting(meetingScheduleId, meetingScheduleUpdateDto);
    }

    @Override
    public String cancelScheduledMeeting(String meetingScheduleId, CancelMeetingDto cancelMeetingDto) {
        User user = userService.getCurrentLoggedInUser();
        cancelMeetingDto.setLoggedInUserId(user.getId());
        return candidateApiExecutor.cancelScheduledMeeting(meetingScheduleId, cancelMeetingDto);
    }

    @Override
    public byte[] getCandidatePdfReport(Long testInvitationId, boolean sectional) {
        return candidateApiExecutor.getCandidatePdfReport(testInvitationId, sectional);
    }

    @Override
    public String getRecruiters(List<String> jobMatchIds, String companyId) {
        return candidateApiExecutor.getRecruiters(jobMatchIds, companyId);
    }

    @Override
    public String setRecruiter(AssignRecruiterDto assignRecruiterDto) {
        return candidateApiExecutor.setRecruiter(assignRecruiterDto);
    }

    @Override
    public String addCandidates(List<CandidateContactDetailDTO> candidateDetails) {
        User user = userService.getCurrentLoggedInUser();
        for (CandidateContactDetailDTO candidateContactDetailDTO : candidateDetails) {
            candidateContactDetailDTO.setLoggedInUserId(user.getId());
        }
        return candidateApiExecutor.addCandidates(candidateDetails);
    }

    @Override
    public String findCandidatesForRequisition(CandidateRequisitionDto candidateRequisitionDto) {
        return candidateApiExecutor.findCandidatesForRequisition(candidateRequisitionDto);
    }

    @Override
    public String resendAssessment(AssessmentDto assessmentDto) {
        User user = userService.getCurrentLoggedInUser();
        SendAssessmentDto sendAssessmentDto = new SendAssessmentDto(assessmentDto);
        sendAssessmentDto.setLoggedInUserId(user.getId());
        return candidateApiExecutor.resendAssessment(sendAssessmentDto);
    }

    @Override
    public String getCandidateAssessmentDetail(CandidateAssessmentDetailRequestDto candidateAssessmentDetailRequestDto) {
        return candidateApiExecutor.getCandidateAssessmentDetail(candidateAssessmentDetailRequestDto);
    }

    @Override
    public String setAssessmentLeft(AssessmentChangeStatusDto assessmentChangeStatusDto) {
        User user = userService.getCurrentLoggedInUser();
        AssessmentLeftDto assessmentLeftDto = new AssessmentLeftDto(assessmentChangeStatusDto, user.getId());
        return candidateApiExecutor.setAssessmentLeft(assessmentLeftDto);
    }

    @Override
    public String getAssessmentSentDetails(String jobMatchId, CandidateAssessmentTestType testType) {
        return candidateApiExecutor.getAssessmentSentDetails(jobMatchId, testType);
    }

    @Override
    public String getAvailableTimeZones() {
        return candidateApiExecutor.getAvailableTimeZones();
    }

    @Override
    public List<CandidateActivityDto> getCandidateActivity(FilterCandidateActivityDto filterCandidateActivityDto) {
        User loggedInUser = userService.getCurrentLoggedInUser();
        filterCandidateActivityDto.setUserId(loggedInUser.getId());
        return candidateApiExecutor.getCandidateActivity(filterCandidateActivityDto);
    }

    @Override
    public String getCandidateClientOrBUs(String candidateId) {
        return candidateApiExecutor.getCandidateClientOrBUs(candidateId);
    }

    @Override
    public String getCandidateJobAndRole(String candidateId) {
        return candidateApiExecutor.getCandidateJobAndRole(candidateId);
    }

    @Override
    public String setCandidateAvailability(CandidateAvailabilityDTO candidateAvailabilityDTO) {
        User user = userService.getCurrentLoggedInUser();
        candidateAvailabilityDTO.setLoggedInUser(user.getId());
        return candidateApiExecutor.setCandidateAvailability(candidateAvailabilityDTO);
    }

    @Override
    public String getDaxtraAddedCandidatesWithLessScore(String jobId, String daxtraRequestId) {
        return candidateApiExecutor.getDaxtraAddedCandidatesWithLessScore(jobId, daxtraRequestId);
    }

    @Override
    public String deleteCandidate(DeleteDTO deleteDTO) {
        String loggedInUserId = userService.getCurrentLoggedInUser().getId();
        deleteDTO.setLoggedInUserId(loggedInUserId);
        return candidateApiExecutor.deleteCandidate(deleteDTO);
    }

    @Override
    public String deleteCandidateOnResumeRemove(String resumeId) {
        String loggedInUserId = userService.getCurrentLoggedInUser().getId();
        return candidateApiExecutor.deleteCandidateOnResumeRemove(resumeId, loggedInUserId);
    }

    @Override
    public String addOrUpdateContactEmail(ContactDetailsDTO contactDetailsDTO) {
        return candidateApiExecutor.addOrUpdateContactEmail(contactDetailsDTO);
    }

    @Override
    public String addOrUpdateContactPhoneNumber(ContactDetailsDTO contactDetailsDTO) {
        return candidateApiExecutor.addOrUpdateContactPhoneNumber(contactDetailsDTO);
    }

    @Override
    public String addOrUpdateContactAddress(ContactDetailsDTO contactDetailsDTO) {
        String loggedInUserId = userService.getCurrentLoggedInUser().getId();
        contactDetailsDTO.setLoggedInUserId(loggedInUserId);
        return candidateApiExecutor.addOrUpdateContactAddress(contactDetailsDTO);
    }

    @Override
    public CandidateAssessmentDto getCandidateAssessmentDetails(Long testInvitationId) {
        return candidateApiExecutor.getCandidateAssessmentDetails(testInvitationId);
    }

    @Override
    public CandidatesCountDto getCandidateStatusCountByCompany(String userId, String companyId, String visibility) {
        CandidateStatusCountDto candidateStatusCountDto = candidateApiExecutor.getCandidateStatusCountByUser(userId, companyId, visibility);
        CandidatesCountDto candidatesCountDto = new CandidatesCountDto();
        candidatesCountDto.setActiveCandidatesCount(candidateStatusCountDto.getActiveCandidatesCount());
        candidatesCountDto.setPassiveCandidatesCount(candidateStatusCountDto.getPassiveCandidatesCount());
        candidatesCountDto.setActiveJobTypesCandidateCount(candidateStatusCountDto.getActiveJobTypesCandidateCount());
        candidatesCountDto.setHiredYTDJobTypeCandidateCount(candidateStatusCountDto.getHiredYTDJobTypeCandidateCount());
        candidatesCountDto.setUserId(userId);
        return candidatesCountDto;
    }

    @Override
    public CandidatesEnabledStepDto getCandidatesPresentOnStep(String companyId, String userId, String jobId) {
        return candidateApiExecutor.getCandidatesPresentOnStep(companyId, userId, jobId);
    }

    @Override
    public String getAllPendingMeetingsAndAssessments(List<String> jobMatchIds) {
        return candidateApiExecutor.getAllPendingMeetingsAndAssessments(jobMatchIds);
    }

    @Override
    public String updatePendingMeetingsAndAssessments(List<String> jobMatchIds) {
        User loggedInUser = userService.getCurrentLoggedInUser();
        return candidateApiExecutor.updatePendingMeetingsAndAssessments(jobMatchIds, loggedInUser.getId());
    }

    @Override
    public String sendValueAssessments(AssessmentListDto assessmentListDto) {
        User user = userService.getCurrentLoggedInUser();
        if (user == null) {
            throw new IllegalArgumentException("User not found");
        }
        SendAssessmentListDto sendAssessmentDto = new SendAssessmentListDto(user.getId(), assessmentListDto);
        return candidateApiExecutor.sendAssessments(sendAssessmentDto);
    }

    @Override
    public String sendTechAssessments(AssessmentListDto assessmentListDto) {
        User user = userService.getCurrentLoggedInUser();
        if (user == null) {
            throw new IllegalArgumentException("User not found");
        }
        SendAssessmentListDto sendAssessmentDto = new SendAssessmentListDto(user.getId(), assessmentListDto);
        return candidateApiExecutor.sendAssessments(sendAssessmentDto);
    }

    @Override
    public String cancelValueAssessments(CancelValueTechAssessmentListDto cancelValueTechAssessmentListDto) {
        User user = userService.getCurrentLoggedInUser();
        if (user == null) {
            throw new IllegalArgumentException("User not found");
        }
        CancelAssessmentListDto cancelAssessmentListDto = new CancelAssessmentListDto(user.getId(), cancelValueTechAssessmentListDto);
        return candidateApiExecutor.cancelAssessments(cancelAssessmentListDto);
    }

    @Override
    public String cancelTechAssessments(CancelValueTechAssessmentListDto cancelValueTechAssessmentListDto) {
        User user = userService.getCurrentLoggedInUser();
        if (user == null) {
            throw new IllegalArgumentException("User not found");
        }
        CancelAssessmentListDto cancelAssessmentListDto = new CancelAssessmentListDto(user.getId(), cancelValueTechAssessmentListDto);
        return candidateApiExecutor.cancelAssessments(cancelAssessmentListDto);
    }

    @Override
    public CandidateAssessmentReportDetails updateCandidateReportDetails(CandidateAssessmentReportDetails candidateAssessmentReportDetails) {
        User user = userService.getCurrentLoggedInUser();
        return candidateApiExecutor.updateCandidateReportDetails(candidateAssessmentReportDetails, user.getId());
    }

    @Override
    public CandidateAssessmentReportDetails deleteCommentFromCandidateReportDetails(String candidateAssessmentDetailId, String commentId) {
        User user = userService.getCurrentLoggedInUser();
        return candidateApiExecutor.deleteCommentFromCandidateReportDetails(candidateAssessmentDetailId, commentId, user.getId());
    }

    @Override
    public CandidatesEnabledStepDto getCandidatesPresentOnStepByClientOrBuId(String companyId, String userId, List<NewJobDto> newJobDtos) {
        return candidateApiExecutor.getCandidatesPresentOnStepByClientOrBuId(companyId, userId, newJobDtos);
    }

    @Override
    public CandidatesEnabledStepDto getCandidatesPresentOnStepByCompanyId(String companyId, String userId, List<String> allClientsOrBu) {
        return candidateApiExecutor.getCandidatesPresentOnStepByCompanyId(companyId, userId, allClientsOrBu);
    }

    @Override
    public String addOrUpdateCandidateAdditionalDetails(UpdateCandidateAdditionalDetailsDto candidateAdditionalDetailsDto) {
        candidateAdditionalDetailsDto.setLoggedInUserId(userService.getCurrentLoggedInUser().getId());
        return candidateApiExecutor.addOrUpdateCandidateAdditionalDetails(candidateAdditionalDetailsDto);
    }

    @Override
    public String allowOverride(String candidateId) {
        return candidateApiExecutor.allowOverride(candidateId);
    }

    @Override
    public String updateCandidateReportInfo(String candidateAssessmentDetailId, AssessmentReportInfoDto assessmentReportInfoDto) {
        User user = userService.getCurrentLoggedInUser();
        assessmentReportInfoDto.getCommentDto().getUser().setId(user.getId());
        return candidateApiExecutor.updateCandidateReportInfo(candidateAssessmentDetailId, assessmentReportInfoDto);
    }

    @Override
    public String cancelAssessmentsOnAssessmentUpdate(List<String> candidateAssessmentIds, String currentCompanyId) {
        User user = userService.getCurrentLoggedInUser();
        return candidateApiExecutor.cancelAssessmentsOnAssessmentUpdate(candidateAssessmentIds, currentCompanyId, user.getId());
    }

    @Override
    public String validateMeetingStartDateAndEndDate(String meetingScheduleId) {
        return candidateApiExecutor.validateMeetingStartDateAndEndDate(meetingScheduleId);
    }

    @Override
    public CandidateDetailsResponseDTO getCandidateDetailsById(String candidateId) {
        return candidateApiExecutor.getCandidateDetailsById(candidateId);
    }
}
