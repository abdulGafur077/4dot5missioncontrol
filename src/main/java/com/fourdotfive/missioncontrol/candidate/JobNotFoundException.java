package com.fourdotfive.missioncontrol.candidate;

public class JobNotFoundException extends RuntimeException{

	private static final long serialVersionUID = 7519412630889835578L;
	
	private String message;
	
	private String developerMessage;
	
	public JobNotFoundException(String message, String devMessage) {
		this.message = message;
		this.developerMessage = devMessage;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getDeveloperMessage() {
		return developerMessage;
	}

	public void setDeveloperMessage(String developerMessage) {
		this.developerMessage = developerMessage;
	}

	public JobNotFoundException(String message) {
		this.message = message;
	}
	
}
