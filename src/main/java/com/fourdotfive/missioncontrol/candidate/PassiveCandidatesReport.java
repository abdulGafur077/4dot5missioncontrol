package com.fourdotfive.missioncontrol.candidate;

import java.util.Arrays;

public class PassiveCandidatesReport {

	private int[] timeToConvertCandidatesData;
	private int[] passiveCandidatesData;
	private int[] convertedCandidatesData;

	public int[] getTimeToConvertCandidatesData() {
		return timeToConvertCandidatesData;
	}

	public void setTimeToConvertCandidatesData(int[] timeToConvertCandidatesData) {
		this.timeToConvertCandidatesData = timeToConvertCandidatesData;
	}

	public int[] getPassiveCandidatesData() {
		return passiveCandidatesData;
	}

	public void setPassiveCandidatesData(int[] passiveCandidatesData) {
		this.passiveCandidatesData = passiveCandidatesData;
	}

	public int[] getConvertedCandidatesData() {
		return convertedCandidatesData;
	}

	public void setConvertedCandidatesData(int[] convertedCandidatesData) {
		this.convertedCandidatesData = convertedCandidatesData;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PassiveCandidatesReport [timeToConvertCandidatesData=");
		builder.append(Arrays.toString(timeToConvertCandidatesData));
		builder.append(", passiveCandidatesData=");
		builder.append(Arrays.toString(passiveCandidatesData));
		builder.append(", convertedCandidatesData=");
		builder.append(Arrays.toString(convertedCandidatesData));
		builder.append("]");
		return builder.toString();
	}

}
