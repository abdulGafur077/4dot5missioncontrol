package com.fourdotfive.missioncontrol.candidate;

import java.util.Arrays;

public class RequisitionsReport {

	private int[] timeToFillRequisitionData;
	private int[] newRequisitionsData;
	private int[] fulfilledRequisitionsData;

	public int[] getTimeToFillRequisitionData() {
		return timeToFillRequisitionData;
	}

	public void setTimeToFillRequisitionData(int[] timeToFillRequisitionData) {
		this.timeToFillRequisitionData = timeToFillRequisitionData;
	}

	public int[] getNewRequisitionsData() {
		return newRequisitionsData;
	}

	public void setNewRequisitionsData(int[] newRequisitionsData) {
		this.newRequisitionsData = newRequisitionsData;
	}

	public int[] getFulfilledRequisitionsData() {
		return fulfilledRequisitionsData;
	}

	public void setFulfilledRequisitionsData(int[] fulfilledRequisitionsData) {
		this.fulfilledRequisitionsData = fulfilledRequisitionsData;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RequisitionsReport [timeToFillRequisitionData=");
		builder.append(Arrays.toString(timeToFillRequisitionData));
		builder.append(", newRequisitionsData=");
		builder.append(Arrays.toString(newRequisitionsData));
		builder.append(", fulfilledRequisitionsData=");
		builder.append(Arrays.toString(fulfilledRequisitionsData));
		builder.append("]");
		return builder.toString();
	}

}
