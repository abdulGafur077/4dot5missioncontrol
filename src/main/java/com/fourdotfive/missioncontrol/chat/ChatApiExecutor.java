package com.fourdotfive.missioncontrol.chat;


public interface ChatApiExecutor {

    String saveChat(ChatMessageDTO chatMessageDTO);
    String getAllChatsByUserIdAndJobMatchId(String loggedInUserId, String jobMatchId, ChatFilterDTO chatFilterDTO);

    String changeChatMessageState(String chatMessageId, String userId);
}
