package com.fourdotfive.missioncontrol.chat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/chat/")
public class ChatController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChatController.class);

    @Autowired
    private ChatService chatService;

    @RequestMapping(value = "savechat", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String saveChat(@RequestBody ChatMessageDTO chatMessageDTO) {
        return chatService.saveChat(chatMessageDTO);
    }


    @RequestMapping(value = "getallchatsbyuserid/{userId}/{jobMatchId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAllChatsByUserIdAndJobMatchId(@PathVariable("userId") String userId,
                                                   @PathVariable("jobMatchId") String jobMatchId,
                                                   @RequestBody ChatFilterDTO chatFilterDTO) {
        return chatService.getAllChatsByUserIdAndJobMatchId(userId, jobMatchId, chatFilterDTO);
    }

    @RequestMapping(value = "changechatmessagestate/{chatMessageId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String changeChatMessageState(@PathVariable("chatMessageId") String chatMessageId) {
        return chatService.changeChatMessageState(chatMessageId);
    }
}
