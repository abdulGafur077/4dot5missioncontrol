package com.fourdotfive.missioncontrol.chat;


import java.util.List;

public class ChatMessage {

    private String chatMessageId;
    private ChatUserDetailDTO sendBy;
    private String sendAt;
    private List<ChatUserDetailDTO> recipients;
    private String content;
    private boolean isSentUnderSameCompany;
    private boolean isSentToOtherCompany;
    private ChatMessageType chatMessageType;
    private String parentMessageId;
    private String jobMatchId;
    
    public String getChatMessageId() {
        return chatMessageId;
    }

    public void setChatMessageId(String chatMessageId) {
        this.chatMessageId = chatMessageId;
    }

    public String getSendAt() {
        return sendAt;
    }

    public void setSendAt(String sendAt) {
        this.sendAt = sendAt;
    }

    public ChatUserDetailDTO getSendBy() {
        return sendBy;
    }

    public void setSendBy(ChatUserDetailDTO sendBy) {
        this.sendBy = sendBy;
    }

    public List<ChatUserDetailDTO> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<ChatUserDetailDTO> recipients) {
        this.recipients = recipients;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isSentUnderSameCompany() {
        return isSentUnderSameCompany;
    }

    public void setSentUnderSameCompany(boolean sentUnderSameCompany) {
        isSentUnderSameCompany = sentUnderSameCompany;
    }

    public boolean isSentToOtherCompany() {
        return isSentToOtherCompany;
    }

    public void setSentToOtherCompany(boolean sentToOtherCompany) {
        isSentToOtherCompany = sentToOtherCompany;
    }

    public ChatMessageType getChatMessageType() {
        return chatMessageType;
    }

    public void setChatMessageType(ChatMessageType chatMessageType) {
        this.chatMessageType = chatMessageType;
    }

    public String getParentMessageId() {
        return parentMessageId;
    }

    public void setParentMessageId(String parentMessageId) {
        this.parentMessageId = parentMessageId;
    }

    public String getJobMatchId() {
        return jobMatchId;
    }

    public void setJobMatchId(String jobMatchId) {
        this.jobMatchId = jobMatchId;
    }
}
