package com.fourdotfive.missioncontrol.chat;


import java.util.List;

public class ChatMessageDTO {

    private String content;
    private String sendBy;
    private List<String> sendsTo;
    private ChatMessageType chatMessageType;
    private String parentMessageId;
    private String jobMatchId;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSendBy() {
        return sendBy;
    }

    public void setSendBy(String sendBy) {
        this.sendBy = sendBy;
    }

    public List<String> getSendsTo() {
        return sendsTo;
    }

    public void setSendsTo(List<String> sendsTo) {
        this.sendsTo = sendsTo;
    }

    public ChatMessageType getChatMessageType() {
        return chatMessageType;
    }

    public void setChatMessageType(ChatMessageType chatMessageType) {
        this.chatMessageType = chatMessageType;
    }

    public String getParentMessageId() {
        return parentMessageId;
    }

    public void setParentMessageId(String parentMessageId) {
        this.parentMessageId = parentMessageId;
    }

    public String getJobMatchId() {
        return jobMatchId;
    }

    public void setJobMatchId(String jobMatchId) {
        this.jobMatchId = jobMatchId;
    }
}
