package com.fourdotfive.missioncontrol.chat;


public enum ChatMessageType {

    INTERNAL, EXTERNAL;
}
