package com.fourdotfive.missioncontrol.chat;


public interface ChatService {

    String saveChat(ChatMessageDTO chatMessageDTO);
    String getAllChatsByUserIdAndJobMatchId(String userId, String jobMatchId, ChatFilterDTO chatFilterDTO);
    void sendMessage(ChatMessage chatMessage);

    String changeChatMessageState(String chatMessageId);
}
