package com.fourdotfive.missioncontrol.chat;

import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.user.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
public class ChatServiceImpl implements ChatService {

    @Autowired
    private ChatApiExecutor chatApiExecutor;
    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;
    @Autowired
    private UserService userService;


    @Override
    public String saveChat(ChatMessageDTO chatMessageDTO) {
        return chatApiExecutor.saveChat(chatMessageDTO);
    }

    @Override
    public String getAllChatsByUserIdAndJobMatchId(String userId, String jobMatchId, ChatFilterDTO chatFilterDTO) {
        return chatApiExecutor.getAllChatsByUserIdAndJobMatchId(userId, jobMatchId, chatFilterDTO);
    }

    @Override
    public void sendMessage(ChatMessage chatMessage) {
        for (ChatUserDetailDTO recipient: chatMessage.getRecipients()) {
            if (StringUtils.isNotBlank(recipient.getId()))
                simpMessagingTemplate.convertAndSend("/queue/" + recipient.getId() + "/chatMessage", chatMessage);
        }
    }

    @Override
    public String changeChatMessageState(String chatMessageId) {
        User user = userService.getCurrentLoggedInUser();
        return chatApiExecutor.changeChatMessageState(chatMessageId, user.getId());
    }

}
