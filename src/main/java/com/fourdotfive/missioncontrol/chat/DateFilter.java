package com.fourdotfive.missioncontrol.chat;

public enum DateFilter {
    OLDEST, LATEST;
}
