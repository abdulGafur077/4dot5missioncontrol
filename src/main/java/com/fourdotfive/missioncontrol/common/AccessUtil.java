package com.fourdotfive.missioncontrol.common;

import java.util.List;

import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.company.CompanyType;
import com.fourdotfive.missioncontrol.pojo.user.Role;
import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.pojo.user.UserForwardedReference;

public interface AccessUtil {

	boolean canRoleHaveReportees(String roleId);

	boolean isManagerApplicable(String roleId);

	boolean isRoleAtCompanyAdminLevel(String roleId);

	boolean is4dot5AdminViewable(String roleId);

	boolean is4dot5AdminAvailable(CompanyType companyType);

	boolean isRecruitingManager(Role role);

	boolean isCompanyAdmin(Role role);

	boolean isEmployee(Role role);

	boolean isSuperUser(String roleId);

	boolean isFourDot5Admin(Role role);

	boolean isStaffingCompanyRole(Role role);

	boolean isEntityScopeStaffingCompanyRole(Role role);

	boolean isEntityScopeCorporateCompanyRole(Role role);

	boolean isCorporateCompanyRole(Role role);

	boolean is4Dot5CompanyRole(Role role);

	boolean isHiringManagerRole(String roleId);

	boolean isHiringManagerRole(Role role);

	boolean isHostCompany(Company company);

	boolean isStaffingOrCorporateComp(Company type);

	boolean isClientOrBU(CompanyType type);

	boolean isCompanyNotActive(Company company);

	boolean isActiveFourDot5Admin(User user);

	boolean canRoleBeUpdated(User user, String roleId, int count);

	boolean isFourDot5Admin(String roleId);

	boolean isUserArchived(User user);
	
	boolean isUserArchived(UserForwardedReference user);

	boolean isHostCompany(String companyType);

	boolean isStaffingCompany(String companyType);

	boolean isCorporateCompany(String companyType);

	boolean isStaffingAdmin(Role role);

	boolean isCorporateAdmin(Role role);

	boolean isClientOrBU(Company comp);
	
	boolean isRecruiter(Role role);

	boolean isSeniorRec(Role role);

	List<Role> sortInAscending(List<Role> roles);

	boolean isStaffingOrCorporateComp(CompanyType companyType);

	boolean isClient(Company comp);
}
