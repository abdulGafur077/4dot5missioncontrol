package com.fourdotfive.missioncontrol.common;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.company.CompanyState;
import com.fourdotfive.missioncontrol.pojo.company.CompanyType;
import com.fourdotfive.missioncontrol.pojo.user.AccountState;
import com.fourdotfive.missioncontrol.pojo.user.Role;
import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.pojo.user.UserForwardedReference;

/**
 * @author Shalini
 */
@Service
public class AccessUtilImp implements AccessUtil {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(AccessUtilImp.class);

    public AccessUtilImp() {
    }

    /************************ For Roles **********************/
    @Override
    public boolean canRoleHaveReportees(String roleId) {
        if (roleId.equals(AppConstants.STAFFING_REC_ROLEID)
                || roleId.equals(AppConstants.STAFFING_SRREC_ROLEID)
                || roleId.equals(AppConstants.CORPORATE_REC_ROLEID)
                || roleId.equals(AppConstants.CORPORATE_SRREC_ROLEID)) {
            return false;
        }
        return true;
    }

    @Override
    public boolean isManagerApplicable(String role) {
        if (role.equals(AppConstants.SUPERUSER_ROLEID)
                || role.equals(AppConstants.FOURDOT5ADMIN_ROLEID)
                || role.equals(AppConstants.STAFFING_ADMIN_ROLEID)
                || role.equals(AppConstants.CORPORATE_ADMIN_ROLEID)) {
            return false;
        }
        return true;
    }

    @Override
    public boolean isRoleAtCompanyAdminLevel(String roleId) {
        if (roleId.equals(AppConstants.SUPERUSER_ROLEID)
                || roleId.equals(AppConstants.FOURDOT5ADMIN_ROLEID)
                || roleId.equals(AppConstants.STAFFING_ADMIN_ROLEID)
                || roleId.equals(AppConstants.CORPORATE_ADMIN_ROLEID)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean is4dot5AdminViewable(String roleId) {
        if (roleId.equals(AppConstants.SUPERUSER_ROLEID)
                || roleId.equals(AppConstants.FOURDOT5ADMIN_ROLEID)
                || roleId.equals(AppConstants.STAFFING_ADMIN_ROLEID)
                || roleId.equals(AppConstants.CORPORATE_ADMIN_ROLEID)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean is4dot5AdminAvailable(CompanyType companyType) {
        if (companyType.equals(CompanyType.StaffingCompany)
                || companyType.equals(CompanyType.Corporation)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isRecruitingManager(Role role) {
        if (role.getId().equals(AppConstants.CORPORATE_REC_MANAGER_ROLEID)
                || role.getId()
                .equals(AppConstants.STAFFING_REC_MANAGER_ROLEID)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isCompanyAdmin(Role role) {
        if (role.getId().equals(AppConstants.CORPORATE_ADMIN_ROLEID)
                || role.getId().equals(AppConstants.STAFFING_ADMIN_ROLEID)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isEmployee(Role role) {
        if (role.getId().equals(AppConstants.EMPLOYEE_ROLEID)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isStaffingAdmin(Role role) {
        if (role.getId().equals(AppConstants.STAFFING_ADMIN_ROLEID)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isCorporateAdmin(Role role) {
        if (role.getId().equals(AppConstants.CORPORATE_ADMIN_ROLEID)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isSuperUser(String roleId) {
        if (roleId.equals(AppConstants.SUPERUSER_ROLEID)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isFourDot5Admin(Role role) {
        if (role.getId().equals(AppConstants.FOURDOT5ADMIN_ROLEID)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isFourDot5Admin(String roleId) {
        if (roleId.equals(AppConstants.FOURDOT5ADMIN_ROLEID)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isActiveFourDot5Admin(User user) {
        String roleId = user.getRole().getId();
        String accountState = user.getAccessControlList().getAccountState();
        if (roleId.equals(AppConstants.FOURDOT5ADMIN_ROLEID)
                && !accountState.equals(AppConstants.ACCOUNT_STATE_ARCHIVED)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean is4Dot5CompanyRole(Role role) {
        if (role.getId().equals(AppConstants.SUPERUSER_ROLEID)
                || role.getId().equals(AppConstants.FOURDOT5ADMIN_ROLEID)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isStaffingCompanyRole(Role role) {
        if (role.getId().equals(AppConstants.STAFFING_REC_MANAGER_ROLEID)
                || role.getId().equals(AppConstants.STAFFING_SRREC_ROLEID)
                || role.getId().equals(AppConstants.STAFFING_REC_ROLEID)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isCorporateCompanyRole(Role role) {
        if (role.getId().equals(AppConstants.CORPORATE_REC_MANAGER_ROLEID)
                || role.getId().equals(AppConstants.CORPORATE_SRREC_ROLEID)
                || role.getId().equals(AppConstants.CORPORATE_REC_ROLEID)
                || role.getId().equals(AppConstants.HIRING_MANAGER_ROLEID)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isEntityScopeStaffingCompanyRole(Role role) {
        if (role.getId().equals(AppConstants.STAFFING_ADMIN_ROLEID)
                || role.getId()
                .equals(AppConstants.STAFFING_REC_MANAGER_ROLEID)
                || role.getId().equals(AppConstants.STAFFING_SRREC_ROLEID)
                || role.getId().equals(AppConstants.STAFFING_REC_ROLEID)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isEntityScopeCorporateCompanyRole(Role role) {

        if (role.getId().equals(AppConstants.CORPORATE_ADMIN_ROLEID)
                || role.getId().equals(
                AppConstants.CORPORATE_REC_MANAGER_ROLEID)
                || role.getId().equals(AppConstants.CORPORATE_SRREC_ROLEID)
                || role.getId().equals(AppConstants.CORPORATE_REC_ROLEID)
                || role.getId().equals(AppConstants.HIRING_MANAGER_ROLEID)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean canRoleBeUpdated(User user, String roleId, int count) {
        if ((!user.getRole().getId().equals(roleId))) {
            if (canRoleHaveReportees(roleId)) {

                return true;
            } else {
                if (count > 0) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public boolean isHiringManagerRole(String roleId) {
        return roleId.equals(AppConstants.HIRING_MANAGER_ROLEID);
    }

    @Override
    public boolean isHiringManagerRole(Role role) {
        return role.getId().equals(AppConstants.HIRING_MANAGER_ROLEID);
    }

    /************************** For Company **********************/
    @Override
    public boolean isHostCompany(Company company) {

        if (company.getCompanyType().equals(CompanyType.Host)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isStaffingOrCorporateComp(Company company) {
        if (company.getCompanyType().equals(CompanyType.StaffingCompany)
                || company.getCompanyType().equals(CompanyType.Corporation)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isClientOrBU(CompanyType type) {
        if (type.equals(CompanyType.Client)
                || type.equals(CompanyType.BusinessUnit)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isClientOrBU(Company comp) {

        CompanyType type = comp.getCompanyType();
        if (type.equals(CompanyType.Client)
                || type.equals(CompanyType.BusinessUnit)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isCompanyNotActive(Company company) {
        if (company.getCompanyState().equals(CompanyState.Archived.toString())) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isHostCompany(String companyType) {
        if (companyType.equals(CompanyType.Host.toString())) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isStaffingCompany(String companyType) {
        if (companyType.equals(CompanyType.StaffingCompany.toString())) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isCorporateCompany(String companyType) {
        if (companyType.equals(CompanyType.Corporation.toString())) {
            return true;
        }
        return false;
    }

    /*************** For user *****************/
    @Override
    public boolean isUserArchived(User user) {
        if (user.getAccessControlList().getAccountState()
                .equalsIgnoreCase(AccountState.ARCHIVED.toString())) {
            return true;
        }
        return false;
    }

    @Override
    public List<Role> sortInAscending(List<Role> roles) {
        List<Role> sortedRoles = new ArrayList<Role>();
        for (Role role : roles) {
            if (isCompanyAdmin(role)) {
                sortedRoles.set(0, role);
            } else if (isRecruitingManager(role)) {
                sortedRoles.set(1, role);
            } else if (isRecruiter(role)) {
                sortedRoles.set(2, role);
            } else if (isSeniorRec(role)) {
                sortedRoles.set(3, role);
            } else if (isHiringManagerRole(role)) {
                sortedRoles.set(4, role);
            }
        }
        return sortedRoles;
    }

    @Override
    public boolean isRecruiter(Role role) {
        if (role.getId().equals(AppConstants.STAFFING_REC_ROLEID)
                || role.getId().equals(AppConstants.CORPORATE_REC_ROLEID)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isSeniorRec(Role role) {
        if (role.getId().equals(AppConstants.STAFFING_SRREC_ROLEID)
                || role.getId().equals(AppConstants.CORPORATE_SRREC_ROLEID)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isUserArchived(UserForwardedReference user) {
        if (user != null && user.getUser().getAccessControlList().getAccountState()
                .equalsIgnoreCase(AccountState.ARCHIVED.toString())) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isStaffingOrCorporateComp(CompanyType companyType) {
        if (companyType.equals(CompanyType.StaffingCompany)
                || companyType.equals(CompanyType.Corporation)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isClient(Company comp) {

        CompanyType type = comp.getCompanyType();
        if (type.equals(CompanyType.Client)) {
            return true;
        }
        return false;
    }
}
