package com.fourdotfive.missioncontrol.common;

import com.amazonaws.services.dynamodbv2.xspec.S;

public final class ApiConstants {

    public static final String MOCK_BASE_URL = "http://192.168.1.157:8089/";
    public static final String PLATFORM_BASE_URL = "http://52.52.103.54:8080/fourdotfive-rest/";
    private static final String LOCAL_BASE_URL = "http://localhost:8080/fourdotfive-rest/";
    public static final String BASE_URL = LOCAL_BASE_URL;
    public static final String TEST_API = "user/verifypassword";
    public static final String HOST_COMPANY_NAME = "4dot5";
    public static final String VERIFY_PASSWORD = BASE_URL + "user/verifypassword";
    public static final String VERIFY_TOKEN = BASE_URL + "user/verifytoken";
    public static final String FORGOT_PASSWORD = BASE_URL + "user/forgotpassword";
    public static final String VERIFY_ONE_TIME_PASSWORD = BASE_URL + "user/verifyoneTimePassword";
    public static final String CHANGE_PASSWORD = BASE_URL + "user/changePassword";
    public static final String VERIFY_USERNAME = BASE_URL + "user/verifyusername";
    public static final String GET_TOKEN_PAYLOAD = BASE_URL + "user/gettokenpayload";
    // User management api
    public static final String GET_ROLES = BASE_URL + "masterdata/roles";
    public static final String GET_ROLES_SCREEN_REFERENCE = BASE_URL + "masterdata/rolescreens";
    public static final String GET_ENTITY_SCOPE = BASE_URL + "user/getscope";
    public static final String FORWARD_USER = BASE_URL + "user/forwarduser";
    public static final String GET_TEAM_MEMBERS = BASE_URL + "user/getteammembers";
    public static final String SAVE_USER = BASE_URL + "user/save";
    public static final String GET_USER = BASE_URL + "user/get";
    public static final String GET_RECIPIENT_USER = BASE_URL + "user/getRecipient";
    public static final String GET_ALL_COMPANY_ADMIN = BASE_URL + "user/getadmins";
    public static final String DELETE_MANAGER = BASE_URL + "user/removeManager";
    public static final String DELETE_USER = BASE_URL + "user/delete";
    public static final String GET_REPORTEES = BASE_URL + "user/directreports";
    public static final String GET_USERS_BASED_ON_ROLE = BASE_URL + "user/getUsersGivenRole";
    public static final String GET_FORWARDED_FROM_RECIPIENTS = BASE_URL + "user/getForwardedFromRecipients";
    public static final String ASSIGN_MANAGER = BASE_URL + "user/assignManager";
    public static final String ADD_FORWARDED_FROM_RECIPIENT = BASE_URL + "user/addForwardedFromRecipient";
    public static final String REMOVE_FORWARDED_FROM_RECIPIENT = BASE_URL + "user/removeForwardedFromRecipient";
    public static final String SET_ENTITY_SCOPE = BASE_URL + "user/setscope";
    public static final String GET_MANAGER = BASE_URL + "user/getManager";
    public static final String UPDATE_CANDIDATE_STATUS = BASE_URL + "";
    public static final String GET_DASHBOARD_DATA = "/dashboard";
    public static final String GET_POSSIBLE_MANAGERS_FOR_HIRING_MANAGER = BASE_URL + "user/getpossiblemanagersforhiringmanager";
    // for create company
    public static final String GET_COMPANY_TYPE = BASE_URL + "company/getcompanytype";
    public static final String GET_COMPANY_ENTITY_SCOPE = BASE_URL + "company/getscope";
    public static final String GET_DOTFIVE_ADMINS = BASE_URL + "company/get4dot5admin";
    public static final String SAVE_COMPANY = BASE_URL + "company/save";
    public static final String GET_COMPANY_BY_ID = BASE_URL + "company/find";
    public static final String GET_COMPANY_BY_NAME = BASE_URL + "company/findByName";
    public static final String ADD_CLIENT_OR_BU = BASE_URL + "company/addclient";
    public static final String DELETE_CLIENT_OR_BU = BASE_URL + "company/deleteclient";
    public static final String DELETE_COMPANY = BASE_URL + "company/delete";
    public static final String GET_CLIENT_OR_BU = BASE_URL + "company/clientorbulist";
    public static final String GET_INDUSTIRES = BASE_URL + "company/industrylist";
    public static final String GET_REQUISITION_ROLE = BASE_URL + "company/jobrequistionrole";
    public static final String GET_REQUISITION_CATEGORY = BASE_URL + "company/requisitioncategory";
    public static final String SAVE_LICENSE_PREFERENCES = BASE_URL + "company/setlicensepreferences";
    public static final String GET_ALL_COMPANIES = BASE_URL + "company/findall";
    public static final String GET_LICENSE_PREFERENCES = BASE_URL + "company/getlicensepreferences";
    public static final String GET_RECRUITER_LIST = BASE_URL + "company/recruiterlist";
    public static final String SAVE_FOURDOTFIVE_ADMIN = BASE_URL + "company/saveFourDotFiveAdmin/";
    public static final String GET_FOURDOTFIVE_ADMIN = BASE_URL + "company/getFourDotFiveAdmin/";
    public static final String SAVE_RECRUITER_QUESTIONLIST = BASE_URL + "company/setquestionlist/";
    public static final String GET_RECRUITER_QUESTIONLIST = BASE_URL + "company/questionlist/";
    public static final String GET_ALL_STAFFING_COMPANIES = BASE_URL + "company/findallstaffingcompanies";
    public static final String GET_CORPORATE_VENDOR_RELN = BASE_URL + "company/getcorporatevendorrelationship";
    public static final String SAVE_CORPORATE_VENDOR_RELN = BASE_URL + "company/saveCorporateVendorRelationship";
    public static final String SAVE_CORPORATE_VENDORS = BASE_URL + "company/saveCorporateVendors";
    public static final String FIND_ALL_CORPORATE_VENDORS = BASE_URL + "company/findallvendorsofcorporatecompany";
    public static final String FIND_ALL_BU_VENDORS = BASE_URL + "company/getallbuvendors";
    public static final String SAVE_BU_VENDORS = BASE_URL + "company/savebuvendors";
    public static final String GET_ALL_BUS_OF_CLIENT = BASE_URL + "company/getallbusofclient";
    public static final String UPDATE_CORPORATE_CONTACT_LIST = BASE_URL + "company/updatecorporatecompanycontactlist";
    public static final String GET_CLIENT_BU_LIST = BASE_URL + "company/getclientbulist";
    public static final String GET_CORPORATE_CONTACT_LIST = BASE_URL + "company/corporatecompanycontactlist";
    public static final String CHECK_DUPLICATE_HIRING_MANAGERS = BASE_URL + "company/duplicateemailcheckforhiringmanagers";
    public static final String SEARCH_COMPANIES_BY_NAME = BASE_URL + "company/searchforcompanies";
    // for Requisition
    public static final String UPLOAD_REQ_DOC = BASE_URL + "jobrequisition/upload";
    public static final String PROCESS_REQ_JOB = BASE_URL + "jobrequisition/processjobs";
    public static final String SAVE_AND_PARSE_HRXML_JOB = BASE_URL + "job/saveandparsejob";
    public static final String SAVE_JOB_OPENINGS = BASE_URL + "jobopening/save";
    public static final String RUN_JOB_FOR_SCORING = "job/runjobforscoring";
    public static final String SET_JOB_STATUS = BASE_URL + "job/setStatus";
    public static final String SAVE_JOB = BASE_URL + "job/save";
    public static final String GET_JOB = BASE_URL + "job/find";
    public static final String GET_REQUISITION_CARD = BASE_URL + "job/getRequisitionCard";
    public static final String GET_JOBS_BY_COMPANYID = BASE_URL + "job/filter";
    public static final String ASSIGN_VENDOR_RECRUITERS = BASE_URL + "job/assign/vendor/recruiters";
    public static final String COPY_REQUISITION = BASE_URL + "job/copyrequisition";
    public static final String SEARCH_ROLES_FOR_COMPANY = BASE_URL + "job/rolesbycompany";
    public static final String SEARCH_ROLES_BY_USER = BASE_URL + "job/rolesbyuser";
    public static final String GET_JOBS_BY_FILTER = BASE_URL + "job/getfilteredjobs";
    public static final String GET_JOB_PROFILE = "/job/jobprofile/find";
    public static final String GET_ALL_JOB = "job/findall";
    public static final String GET_OPEN_REQUISITIONS = BASE_URL + "";
    public static final String UPDATE_REQUISITION_STATUS = BASE_URL + "job/setStatus";
    public static final String GET_REQ_TRANSACTION_CANDIDATES_COUNT = BASE_URL + "jobmatches/getcountbyreqtransaction";
    public static final String GET_REQ_TRANSACTION_CANDIDATES = BASE_URL + "jobmatches/getcandidatesbyreqtransaction";
    public static final String GET_JOB_TYPES = BASE_URL + "job/jobtypes";
    public static final String GET_SPONSORSHIP_TYPES = BASE_URL + "jobrequisition/sponsorshiptypes";
    public static final String GET_SALARY_DURATION_OPTIONS = BASE_URL + "jobrequisition/salarydurationoptions";
    public static final String GET_CURRENCY_CODES = BASE_URL + "jobrequisition/currencycodes";
    public static final String GET_MARKER_VALUES = BASE_URL + "job/getMarkerValues";
    public static final String GET_STATUS_TYPES = BASE_URL + "job/statustypes";
    public static final String GET_CLIENT_RECRUITER_BY_ROLE = BASE_URL + "job/getclientandrecruitersbyroles";
    public static final String GET_CLIENT_RECRUITER_COMPANY_BY_ROLE = BASE_URL + "job/getclientandrecruiterscompanybyroles";
    public static final String GET_CLIENT_ROLE_BY_RECRUITER = BASE_URL + "job/getclientandrolebyrecruiters";
    public static final String GET_CLIENT_ROLE_COMPANY_BY_RECRUITER = BASE_URL + "job/getclientandrolecompanybyrecruiters";
    public static final String GET_ROLE_RECRUITER_BY_CLIENT = BASE_URL + "job/getroleandrecruitersbyclients";
    public static final String GET_ROLE_RECRUITER_COMPANY_BY_CLIENT = BASE_URL + "job/getroleandrecruiterscompanybyclients";
    public static final String SEARCH_BY_REQ_NUM = BASE_URL + "job/searchbynumber";
    public static final String SEARCH_BY_REQ_NUM_COMPANY = BASE_URL + "job/searchbynumbercompany";
    public static final String SEARCH_JOB_BY_TEXT = BASE_URL + "job/searchjob";
    public static final String SEARCH_JOB_COMPANY_BY_TEXT = BASE_URL + "job/searchjobcompany";
    public static final String GET_JOB_MATCH_BY_CANDIDATE = BASE_URL + "jobmatches/findbycandidate";
    public static final String GET_CANDIDATE_ACTIVITY = BASE_URL + "jobmatches/getactivity";
    public static final String CREATE_JOB = BASE_URL + "job/createjob";
    public static final String UPDATE_JOB = BASE_URL + "job/updatejob";
    public static final String GET_MORE_JB_CANDIDATES = BASE_URL + "job/getMoreJBCandidates";
    public static final String GET_ACTIVITY = BASE_URL + "job/getactivity";
    public static final String GET_ALL_ROLES_COMPANY = BASE_URL + "job/allrolesincompany";
    public static final String GET_JOB_COMPARISON = BASE_URL + "candidatejobcomparison/findcandidateprofilecomparisonbyjobmatchid";
    public static final String GET_JOB_DETAILS_BY_ID = BASE_URL + "/job/findjobdetailsbyid/";
    public static final String GET_MULTIPLE_CANDIDATE_SINGLE_JOB_COMPARISON = BASE_URL + "candidatejobcomparison/findmultiplecandidateprofilesinglejobcomparison";
    public static final String GET_MULTIPLE_JOB_SINGLE_CANDIDATE_COMPARISON = BASE_URL + "candidatejobcomparison/findmultiplejobsinglecandidateprofilecomparison";
    public static final String GET_CANDIDATE_FOR_REQ_STATUS = BASE_URL + "job/getcandidatesforreqstatus";
    public static final String GET_CANDIDATE_FOR_CURRENT_STATUS = BASE_URL + "job/getcandidatesforcurrentstate";
    public static final String GET_ACTIVITY_TYPES = BASE_URL + "job/activitytypes";
    public static final String GET_ENABLED_STEPS = BASE_URL + "job/enabledsteps/";
    public static final String GET_DASHBOARD_REQ_COUNT_COMPANY = BASE_URL + "job/dashboardreqcountforcompany/";
    public static final String GET_DASHBOARD_REQ_COUNT_USER = BASE_URL + "job/dashboardreqcountforuser/";
    public static final String JOBS_BY_ACTIVITY_COMPANY = BASE_URL + "job/filterjobsbyactivityforcompany/";
    public static final String JOBS_BY_ACTIVITY_USER = BASE_URL + "job/filterjobsbyactivityforuser/";
    public static final String JOB_BOARD_PARAMETERS = BASE_URL + "jobrequisition/jobboardparameters/";
    public static final String RADIUS_AND_RECENCY_OPTIONS_LIST = BASE_URL + "jobrequisition/radiusandrecencyoptionslist/";
    public static final String CANDIDATE_COUNT = BASE_URL + "job/candidatecountbyreqid/";
    public static final String SEARCH_INTERFACE = BASE_URL + "job/getsearchinterface/";
    public static final String GET_MORE_RESUMES_STATUS = BASE_URL + "job/getmoreresumesinprogress/";
    public static final String CANDIDATE_JOBMATCH_INFO = BASE_URL + "job/candidatejobmatchinfo/";
    public static final String JOB_BOARD_SEARCH_HISTORY = BASE_URL + "job/jobboardsearchhistory/";

    // Entity scope definition
    public static final String CREATE_ROLE_ENTITY_SCOPE = BASE_URL + "company/createscope";
    public static final String UPDATE_ROLE_ENTITY_SCOPE = BASE_URL + "company/updatescope";
    public static final String GET_ROLE_ENTITY_SCOPE = BASE_URL + "company/getscope";
    // API's for methods using Query DSL, written by Spaneos on Platform
    public static final String GET_CANDIDATE = BASE_URL +"candidate/find";
    public static final String GET_CANDIDATES = BASE_URL + "";
    public static final String GET_JOBMATCH = BASE_URL + "";
    public static final String GET_CANDIDATES_BY_REQID = BASE_URL + "";
    public static final String GET_CANDIDATES_COUNT = BASE_URL + "job/runjobforscoring";
    public static final String GET_ALL_CANDIDATES = "candidate/findall";
    // API's for Settings
    public static final String GET_FOURDOTFIVE_SETTINGS = BASE_URL + "setting/findhostcompany";
    public static final String GET_STAFFINGORCORP_SETTINGS = BASE_URL + "setting/findbycompany";
    public static final String GET_CLIENTORBU_SETTINGS = BASE_URL + "setting/findbycompany";
    public static final String SAVE_SETTINGS = BASE_URL + "setting/save";
    public static final String DELETE_SETTINGS = BASE_URL + "setting/deletecompany";
    public static final String GET_NOTIFICATION_PREFS = BASE_URL + "notificationpreference/findbyuser";
    public static final String SAVE_NOTIFICATION_PREFS = BASE_URL + "notificationpreference/save";
    //API's for Resume
    public static final String GET_RESUME_SOURCE_TYPES = BASE_URL + "resume/resumesourcetypes";
    public static final String GET_RESUME_SOURCE_TYPE_BY_ID = BASE_URL + "resume/resumesourcetype";
    public static final String UPLOAD_RESUME = BASE_URL + "resume/upload";
    public static final String PROCESS_RESUME = BASE_URL + "resume/processresumes";
    public static final String RUN_RESUME_FOR_SCORING = BASE_URL + "resume/runresumesforscoring";
    public static final String RUN_RESUME_FOR_ASSOCIATE = BASE_URL + "resume/runresumesforassociate";
    public static final String GET_CANDIDATE_COUNT_BY_RESUME_TRANSACTION_ID = BASE_URL + "jobmatches/getcandidatecountbyresumetransaction";
    public static final String GET_CANDIDATES_BY_RESUME_TRANSACTION_ID = BASE_URL + "jobmatches/getcandidatesbyresumetransaction";
    //API's for Candidates
    public static final String GET_CANDIDATE_ID_NAME_BY_COMPANY = BASE_URL + "candidate/findidnamesbycompany";
    public static final String GET_CANDIDATE_ID_NAME_BY_USER = BASE_URL + "candidate/findidnamesbyuser";
    public static final String GET_CANDIDATE_CARD_INFO = BASE_URL + "candidate/candidatecard";
    public static final String CANDIDATE_SET_STATUS = BASE_URL + "candidate/setstatus";
    public static final String GET_CANDIDATE_PROFILE_BY_ID = BASE_URL + "candidate/findcandidateprofilebyid";
    public static final String SCHEDULE_PHONE_SCREEN = BASE_URL + "/candidate/schedulephonescreen";
    public static final String SCHEDULE_INTERVIEW = BASE_URL + "/candidate/scheduleinterview";
    public static final String SET_RELEASED = BASE_URL + "/candidate/setreleased";
    public static final String SET_NOT_INTERESTED = BASE_URL + "/candidate/setnotinterested";
    public static final String GET_CANDIDATE_STATUS_COUNT_COMPANY = BASE_URL + "/candidate/statuscountforcompany/";
    public static final String GET_CANDIDATE_STATUS_COUNT_USER = BASE_URL + "/candidate/statuscountforuser/";
    public static final String RESCHEDULE_MEETING = BASE_URL + "/candidate/reschedulemeeting";
    public static final String CANCEL_SCHEDULED_MEETING = BASE_URL + "/candidate/cancelscheduledmeeting";
    public static final String GET_CANDIDATE_PDF_REPORT = BASE_URL + "/candidate/candidatepdfreport";
    public static final String ADD_CANDIDATES = BASE_URL + "/candidate/addcandidates";
    public static final String FIND_CANDIDATES_FOR_REQUISITION = BASE_URL + "/candidate/findcandidatesforrequisition";

    //API's for Job Matches
    public static final String CANDIDATE_CARD_WORKFLOW_COMPANY = BASE_URL + "jobmatches/jobcardviewcompany";
    public static final String CANDIDATE_CARD_WORKFLOW_USER = BASE_URL + "jobmatches/jobcardviewuser";
    public static final String CANDIDATE_CARD_INFO = BASE_URL + "jobmatches/candidateworkflowcard";
    public static final String GET_CONTACT_NOTES_BY_JOBMATCH = BASE_URL + "jobmatches/contactandnotes";
    public static final String SAVE_NOTE = BASE_URL + "jobmatches/notes";
    public static final String SAVE_CONTACT_NOTES = BASE_URL + "jobmatches/contactdetails";
    public static final String GET_PHONE_SCREENERS = BASE_URL + "jobmatches/getphonescreeners";
    public static final String GET_INTERVIEWERS = BASE_URL + "jobmatches/getinterviewers";
    public static final String GET_MEETING_SCHEDULE_DETAIL = BASE_URL + "jobmatches/getmeetingscheduledetails";
    public static final String CREATE_JOB_MATCHES = BASE_URL + "jobmatches/createjobmatches";

    public static final String SYNC_ASSESSMENT = BASE_URL + "/assessment/syncassessments";
    public static final String ASSIGN_ASSESSMENT = BASE_URL + "/assessment/assignassessment";
    public static final String REMOVE_ASSIGNED_ASSESSMENT = BASE_URL + "/assessment/removeassignedassessment";
    public static final String SAVE_FEEDBACK_FORM = BASE_URL + "feedback/savefeedback";
    public static final String GET_FEEDBACK_FORM = BASE_URL + "feedback/getfeedback";

    public static final String GET_ALL_RECRUITER_QUESTIONS = BASE_URL + "feedbackanswers/getallrecruiterquestions/";
    public static final String SAVE_RECRUITER_SCREENING_QUESTION = BASE_URL + "feedbackanswers/save/";
    public static final String GET_FILE = BASE_URL + "jobrequisition/getfile/";
    public static final String SAVE_JOB_MATCH_STATE = BASE_URL + "feedbackanswers/savejobmatchstate/";
    public static final String GET_ALL_BIGFIVE_QUESTIONS = BASE_URL + "valueassessment/getbigfivequestions/";
    public static final String GET_ALL_CANDIDATE_BIGFIVE_RESPONSES = BASE_URL + "valueassessment/getcandidatebigfiveresponses/";
    public static final String SAVE_CANDIDATE_BIGFIVE_RESPONSES = BASE_URL + "valueassessment/savecandidatebigfiveresponses/";
    public static final String GET_ALL_VALUE_ASSESSMENTS_BY_COMPANY_ID = BASE_URL + "/assessment/getallvalueassessments";
    public static final String GET_ALL_TECH_ASSESSMENTS_BY_COMPANY_ID = BASE_URL + "/assessment/getalltechassessments";
    public static final String GET_ALL_UNASSIGNED_ASSESSMENTS_BY_COMPANY_ID = BASE_URL + "/assessment/getallunassignedassessments";

    public static final String SEND_VALUE_ASSESSMENT = BASE_URL + "candidate/sendvalueassessment";
    public static final String SEND_TECH_ASSESSMENT = BASE_URL + "candidate/sendtechassessment";
    public static final String RESEND_ASSESSMENT = BASE_URL + "candidate/resendassessment";

    public static final String CANCEL_VALUE_ASSESSMENT = BASE_URL + "candidate/cancelvalueassessment";

    public static final String CANCEL_TECH_ASSESSMENT = BASE_URL + "candidate/canceltechassessment";

    public static final String MOVE_FORWARD = BASE_URL + "candidate/moveforward";

    public static final String MOVE_BACKWARD = BASE_URL + "candidate/movebackward";

    public static final String MOVE_TO_ANY = BASE_URL + "candidate/movetoany";

    public static final String GET_TYPES = BASE_URL + "utility/gettypes/";

    public static final String GET_TYPES_WITH_TEXT = BASE_URL + "utility/gettypeswithtext/";

    public static final String IM_CANDIDATE_REPORT_CALLBACK = BASE_URL + "external/imcandidatereportcallback/";

    public static final String GET_USER_LIST = BASE_URL + "feedbackanswers/getuserlist/";

    public static final String GET_CANDIDATE_NAME = BASE_URL + "feedbackanswers/getcandidatedetails";

    public static final String GET_RECRUITERS = BASE_URL + "candidate/recruiters";

    public static final String SET_RECRUITER = BASE_URL + "candidate/setrecruiter";

    public static final String FILTER_USERS = BASE_URL + "user/getfiltereduserdetails";

    public static final String GET_ACCESS_TIME = BASE_URL + "assessment/getaccesstime";

    public static final String GET_CANDIDATE_ASSESSMENT_DETAIL = BASE_URL + "candidate/getcandidateassessmentdetail";

    public static final String CHANGE_NOTIFICATION_STATE = BASE_URL + "notification/changenotificationstate";

    public static final String GET_ALL_NOTIFICATIONS = BASE_URL + "notification/findall";

    public static final String CHANGE_NOTIFICATION_STATE_USER = BASE_URL + "notification/changestateforallnotificationsofuser";

    public static final String SET_ASSESSMENT_LEFT = BASE_URL + "candidate/setassessmentleft";

    public static final String GET_ASSESSMENT_SENT_DETAILS = BASE_URL + "candidate/sentassessmentdetails";

    public static final String GET_ALL_TIME_ZONES = BASE_URL + "candidate/getavailabletimezones";

    public static final String UPDATE_INTELLIGENCE_SCORE = BASE_URL + "job/updatefourdotfiveintelligencescore";

    public static final String GET_ALL_CLIET_OR_BUS_OF_CANDIDATE = BASE_URL + "candidate/getcandidateclientorbus";

    public static final String GET_JOBS_AND_ROLE_OF_CANDIDATE = BASE_URL + "candidate/getcandidatejobandrole";

    public static final String GET_ALL_CANDIDATE_WORKFLOW_CARD_STATUS = BASE_URL + "jobmatches/getallcandidateworkflowcardstatus";


    public static final String UPDATE_LOCATION = BASE_URL + "job/updatelocation";

    public static final String ADD_SETTINGS = BASE_URL + "company/addsettings";

    public static final String SET_CANDIDATE_AVAILABILITY = BASE_URL + "candidate/setcandidateavailability";

    public static final String GET_DAXTRA_ADDED_CANDIDATES_WITH_LESS_SCORE = BASE_URL + "candidate/getdaxtraaddedcandidateswithlessscore";

    public static final String DELETE_CANDIDATE = BASE_URL + "candidate/delete";

    public static final String DELETE_CANDIDATE_ON_RESUME_REMOVE = BASE_URL + "candidate/deleteonresumeremove";

    public static final String DELETE_JOB = BASE_URL + "job/delete";

    public static final String SOFT_DELETE_USER = BASE_URL + "user/softdelete";

    public static final String GET_USER_DELETE_INFO = BASE_URL + "user/deleteinfos";

    public static final String GET_COMPANY_DELETE_INFO = BASE_URL + "company/companyorclientorbudeleteinfos";

    public static final String SOFT_DELETE_COMPANY = BASE_URL + "company/deletecompanyorclientorbu";

    public static final String ADD_OR_UPDATE_CANDIDATE_EMAIL = BASE_URL + "candidate/addorupdatecontactemail";

    public static final String ADD_OR_UPDATE_CANDIDATE_PHONE = BASE_URL + "candidate/addorupdatecontactphonenumber";

    public static final String ADD_OR_UPDATE_CANDIDATE_ADDRESS = BASE_URL + "candidate/addorupdatecontactaddres";

    public static final String ADD_OR_UPDATE_ADDITIONAL_DETAILS = BASE_URL + "candidate/addorupdatecandidateadditionaldetails";

    public static final String UPDATE_REPORT_INFO = BASE_URL + "candidate/updatecandidatereportinfo";

    public static final String SAVE_DAXTRA_PARAMETERS = BASE_URL + "job/savedaxtraparameters";
    public static final String REMOVE_DAXTRA_PARAMETERS = BASE_URL + "job/removedaxtraparameters";
    public static final String JOB_BOARD_SEARCH_AVAILABILITY_CHECK = BASE_URL + "job/jobboardsearchavailabilitycheck";

    public static final String GET_OTP = BASE_URL + "user/getOtp";

    public static final String GET_ALL_RECRUITER_IDS = BASE_URL + "job/getallrecruiterids";

    public static final String GET_JOB_BY_ID = BASE_URL + "job/findbyid";

    public static final String GET_CLIENT_BY_REQUISITION_ID = BASE_URL + "job/client";

    //API's for Plan
    public static final String GET_ALL_PLAN_FEATURES = BASE_URL + "plan/allplanfeatures/";
    public static final String GET_PLAN_FEATURE_BY_ID = BASE_URL + "plan/findplanfeaturescapabilitybyid/";
    public static final String GET_PLAN_FEATURE_BY_PLAN = BASE_URL + "plan/findplanfeaturescapabilitybyplan/";
    public static final String GET_PLAN_HEADER_BY_ID = BASE_URL + "plan/findplanheaderbyid/";
    public static final String GET_PLAN_HEADER_BY_COMPANY_ID = BASE_URL + "plan/findplanheaderbycompanyid/";
    public static final String SAVE_PLAN_HEADER = BASE_URL + "plan/saveplanheader/";
    public static final String GET_PLAN_DETAIL_BY_ID = BASE_URL + "plan/findplandetailbyid/";
    public static final String GET_PLAN_DETAIL_BY_PLAN_HEADER_ID = BASE_URL + "plan/findplandetailbyplanheaderid/";
    public static final String SAVE_PLAN_DETAIL = BASE_URL + "plan/saveplandetail/";
    public static final String SAVE_PLAN = BASE_URL + "plan/saveplan/";
    public static final String GET_PLAN_BY_PLAN_HEADER_ID = BASE_URL + "plan/findplanbyplanheaderid/";
    public static final String GET_PLAN_BY_COMPANY_ID = BASE_URL + "plan/findplanbycompanyid/";

    public static final String GET_PLAN_DETAILS = BASE_URL + "plan/getplandetails";
    public static final String GET_PLAN_DETAILS_BY_COMPANY_ID = BASE_URL + "plan/findplanfeaturescapabilitybycompanyid";
    public static final String IS_PLAN_ACTIVE = BASE_URL + "plan/isplanactive";
    public static final String CHECK_FEATURE_IS_ENABLED_OR_NOT = BASE_URL + "plan/isfeatureenabled";

    public static final String UPDATE_SCORE_BASED_ON_RECENT_RESUME = BASE_URL + "resume/updatescorebasedonpreviousresume";
    public static final String CHANGE_COMPANY_STATE = BASE_URL + "company/changeCompanyState";
    public static final String GET_CURRENT_PLAN_BY_COMPANY_ID = BASE_URL + "plan/findcurrentplanbycompanyid";
    public static final String GET_FUTURE_PLAN_BY_COMPANY_ID = BASE_URL + "plan/findfutureplanbycompanyid";
    public static final String GET_CURRENT_PLAN_HEADER_BY_COMPANY_ID = BASE_URL + "plan/findcurrentplanheaderbycompanyid";
    public static final String GET_FUTURE_PLAN_HEADER_BY_COMPANY_ID = BASE_URL + "plan/findfutureplanheaderbycompanyid";
    public static final String TECH_ASSESSMENTS_COUNT = BASE_URL + "plan/getavailabletechassessmentscount/";
    public static final String JOB_BOARD_RESUMES_COUNT = BASE_URL + "plan/getavailablejobboardresumescount/";
    public static final String GET_CURRENT_PLAN_MIN_DTO_BY_COMPANY_ID = BASE_URL + "plan/findcurrentplanmindtobycompanyId/";
    public static final String GET_EXPIRED_PLAN_BY_COMPANY_ID = BASE_URL + "plan/findexpiredplanbycompanyid";

    public static final String RUN_RESUME_WITHOUT_SCORING = BASE_URL + "resume/runresumeswithoutscoring";

    public static final String CHECK_RECRUITER_SCREENING_STATUS = BASE_URL + "feedbackanswers/checkrecruiterscreeningstatus/";

    public static final String SET_PASSWORD = BASE_URL + "user/setpassword";

    public static final String ADD_JOB_BOARD_CREDENTIAL = BASE_URL + "job/addjobboardcredentials";

    public static final String DELETE_JOB_BOARD_CREDENTIAL = BASE_URL + "job/deletejobboardcredentials";

    public static final String GET_CANDIDATE_AND_ASSESSMENT_DETAIL = BASE_URL + "candidate/getcandidateandassessmentdetail/";

    public static final String GET_RESUME_BY_ID = BASE_URL + "resume/getresume";

    public static final String GET_RESUME_HRXML_BY_ID = BASE_URL + "resume/getresumehrxml";

    public static final String GET_CANDIDATE_LATEST_RESUME = BASE_URL + "resume/getcandidatelatestresume";

    public static final String GET_REQUISITION_BY_ID = BASE_URL + "jobrequisition/getrequisitionbyid";

    public static final String GET_JOB_REQUISITION_FILE = BASE_URL + "jobrequisition/getrequisitionfile";

    public static final String RESEND_INVITE = BASE_URL + "user/resendinvite";

    public static final String UPDATE_JOB_MATCH_STATE = BASE_URL + "jobmatches/updatejobmatchstate";

    public static final String GET_VENDOR_LIST = BASE_URL + "company/getvendorlist";

    public static final String GET_ASSIGNED_VENDORS = BASE_URL + "job/getassignedvendors";

    public static final String CHECK_SHARED_REQUISITION = BASE_URL + "job/issharedrequisition";

    public static final String GET_VENDORS_OF_JOB = BASE_URL + "job/getvendorsofjob";

    public static final String GET_ASSIGNED_HIRINGMANAGERS_AND_RECRUITERS_OF_JOB = BASE_URL + "job/assigned/hiringmanagers/recruiters";

    public static final String GET_ASSIGNED_VENDORS_AND_RECRUITERS_OF_JOB = BASE_URL + "job/assigned/vendors/recruiters";

    public static final String GET_USER_DETAIL_BY_SCOPE = BASE_URL + "user/getusersofsharedrequisition";

    public static final String GET_RECRUITERS_OF_VENDOR = BASE_URL + "user/vendor/recruiters";

    public static final String GET_RECRUITERS_OF_VENDORS = BASE_URL + "user/vendors/recruiters";

    public static final String GET_ASSIGNED_VENDOR_RECRUITERS_OF_SHARED_REQUISITION = BASE_URL + "user/sharedrequisition/vendor/recruiters";

    public static final String GET_USERS_OF_SHARED_REQUISITION = BASE_URL + "user/getnotifyusersofrequisition";

    public static final String GET_CANDIDATE_PRESENT_ON_STEP = BASE_URL + "jobmatches/setcandidatespresentonstep";


    public static final String PENDING_CANDIDATE_JOB_CARD_ACTIVITES = BASE_URL + "candidate/getallpendingmeetingsandassessments";

    public static final String UPDATE_PENDING_CANDIDATE_JOB_CARD_ACTIVITES = BASE_URL + "candidate/updatependingmeetingsandassessments";

    public static final String SEND_ASSESSMENTS = BASE_URL + "candidate/sendassessments";

    public static final String CANCEL_ASSESSMENTS = BASE_URL + "candidate/cancelassessments";

    public static final String GET_USER_LOGGED_IN_STATUS = BASE_URL + "user/getuserloggedinstatus";

    public static final String UPDATE_COMPANY = BASE_URL + "company/updatecompany";

    public static final String COMPANY_CANDIDATE_REPORTS = BASE_URL + "company/getcompanycandidatereportdetails";

    public static final String UPDATE_CANDIDATE_REPORT_DETAILS = BASE_URL + "candidate/updatecandidatereportdetails";

    public static final String DELETE_COMMENT_CANDIDATE_REPORT_DETAILS = BASE_URL + "candidate/deletecommentfromcandidatereportdetails";

    public static final String GET_ALL_CANCELED_MEETINGS = BASE_URL + "jobmatches/getallcanceledmeetings";

    public static final String SOFT_DELETE_CORPORATE_VENDOR_RELN = BASE_URL + "company/deletecorporatevendorrelationship";

    public static final String SOFT_DELETE_BU_VENDOR_RELN = BASE_URL + "company/deletebuvendorrelationship";

    public static final String GET_SHARED_REQ_AND_BU_COUNT = BASE_URL + "company/getsharedrequisitionandbucounts";

    public static final String SAVE_CHAT = BASE_URL + "chat/savechat";

    public static final String GET_CHAT_BY_USER = BASE_URL + "chat/getallchatsbyuserid";

    public static final String CHANGE_CHAT_MESSAGE_STATE = BASE_URL + "chat/changechatmessagestate";

    public static final String CREATE_OR_UPDATE_EMPLOYEE = BASE_URL + "employee/createorupdate";
    public static final String SEARCH_EMPLOYEES = BASE_URL + "employee/searchemployees";
    public static final String GET_DEPARTMENTS = BASE_URL + "employee/getdepartments";
    public static final String SET_USER_TO_EMPLOYEE = BASE_URL + "employee/convertusertoemployee";
    public static final String SET_EMPLOYEE_TO_USER = BASE_URL + "employee/convertemployeetouser";
    public static final String GET_EMPLOYEE  = BASE_URL + "employee/getemployee";
    public static final String GET_BUS_BY_USER_SCOPE = BASE_URL + "employee/getbusbyuserscope";
    public static final String GET_ORGANIZATIONAL_MANAGER = BASE_URL + "employee/getorganizationalmanagers";
    public static final String GET_DEPARTMENT_BY_ID = BASE_URL + "employee/getdepartmentbyid";
    public static final String GET_PERMISSIBLE_MANAGERS = BASE_URL + "employee/getpermissiblemanagers";

    public static final String INTERVIEW_FEEDBACK_REPORT = BASE_URL + "report/interviewfeedbackreport";
    public static final String PHONE_SCREEN_FEEDBACK_REPORT = BASE_URL + "report/phonescreenfeedbackreport";
    public static final String SCREENING_FEEDBACK_REPORT = BASE_URL + "report/screeningfeedbackreport";
    public static final String FEEDBACK_REPORT_AVAILABILITY = BASE_URL + "report/getreportavailabilitymap";

    public static final String GET_ALL_JOBS_BY_CLIENT_OR_BU_ID = BASE_URL + "job/getjobsbyclientorbuid";
    public static final String GET_CANDIDATE_PRESENT_ON_STEP_BY_CLIENT_OR_BU_ID = BASE_URL + "jobmatches/getcandidatespresentonstepbyclientorbuid";
    public static final String GET_CANDIDATE_PRESENT_ON_STEP_BY_COMPANY_ID = BASE_URL + "jobmatches/getcandidatespresentonstepbycompanyid";
    public static final String GET_REQUISITION_DETAILS_BY_CLIENT_OR_BU_ID = BASE_URL + "jobmatches/getrequisitiondetailsbyclientorbuid";
    public static final String  GET_CLIENT_OR_BU_DETAILS = BASE_URL + "jobmatches/getclientorbudetails";

    public static final String SAVE_WORKFLOW_STEP_ORDER = BASE_URL + "workflowstep/create";
    public static final String GET_WORKFLOW_STEP_ORDER = BASE_URL + "workflowstep/getworkflowstepsorder";
    public static final String GET_WORKFLOW_STEPS_BY_COMPANY_ID = BASE_URL + "workflowstep/workflowstepsbycompanyid";
    public static final String GET_WORKFLOW_STEPS_BY_REQUSITION_ID = BASE_URL + "workflowstep/workflowstepsbyrequisitionid";
    public static final String SAVE_WORKFLOW_STEPS = BASE_URL + "workflowstep/saveworkflowsteps";
    public static final String SAVE_WORKFLOW_STEPS_BY_REQUISITION_ID = BASE_URL + "workflowstep/saveworkflowstepsbyrequisitionid";

    public static final String COMPANY_USERS_FOR_ACTIVITIES = BASE_URL + "user/getcompanyusersforactivities";

    public static final String GET_ASSESSMENT_REPORT_QUESTION_TYPE = BASE_URL + "company/getassessmentreportquestiontype";
    public static final String UPDATE_ASSESSMENT_REPORT_QUESTION_TYPE = BASE_URL + "company/updateassessmentreportquestiontype";

    public static final String GET_CORPORATE_BUS_OF_USER = BASE_URL + "user/getuserclientbuinfo";
    public static final String GET_SUBORDINATES_BY_BU = BASE_URL + "user/getsubordinatesbybu";


    public static final String GET_ALL_PERMISSIBLE_USER = BASE_URL + "user/getallpermissibleusers";
    public static final String GET_ALL_RECRUITERS_OR_HIRING_MANAGER = BASE_URL + "user/getrecruitersforfequisition";
    public static final String HAS_ACCESS_TO_CLIENT_OR_BU = BASE_URL + "user/hasaccesstoclientorbu";
    public static final String GET_ALL_CLIENT_OR_BUS_OF_USER = BASE_URL + "user/getclientorbusofuser";

    public static final String IS_DUPLICATE_COMPANY_BY_NAME =  BASE_URL + "company/isduplicatecompany";
    public static final String HAS_ACCESS_TO_ADD_CANDIDATE_WITHOUT_RESUME =  BASE_URL + "company/hasaccesstoaddcandidatewithorwithoutresume";

    public static final String ALLOW_OVERRIDE = BASE_URL + "candidate/allowoverride";

    public static final String GET_SHARED_CLIENT_DETAILS = BASE_URL + "company/getsharedclientdetails";
    public static final String GET_VENDORS_BY_USER_SCOPE = BASE_URL + "user/getvendorsbyuserscope";
    public static final String GET_RELEASED_JOB_DETAILS = BASE_URL + "job/getreleasedjobdetails";

    public static final String GET_ALL_JOBS_BY_CLIENT_OR_BU_ID_AND_COMPANY_ID = BASE_URL + "job/getjobsbyclientorbuidandcompanyid";

    public static final String GET_LAST_ADDED_CANDIDATE_SOURCE_TYPE_ID = BASE_URL + "job/getlastaddedcandidatesourcetypeid";
    public static final String FIND_JOB_BY_ID = BASE_URL + "job/find";
    public static final String GET_POC = BASE_URL + "job/getpointofcontact";
    public static final String GET_CLIENT_OR_BU_POINT_OF_CONTACTS = BASE_URL + "company/getclientorbupointofcontacts";
    public static final String SET_CORPORATE_VENDOR_RELN = BASE_URL + "company/setcorporatevendorrelationship";

    public static final String GET_REASON_CODES = BASE_URL + "jobmatches/getreleaseresons";
    public static final String SAVE_REASON_CODES = BASE_URL + "jobmatches/saveresoncodes";

    public static final String GET_SENT_ASSESSMENT_DETAILS_BY_ASSESSMENT_ID_AND_JOB_ID = BASE_URL + "job/getsentassessmentdetailsbyassessmentid";
    public static final String CANCEL_ASSESSMENTS_ON_ASSESSMENT_UPDATE = BASE_URL + "candidate/cancelassessmentsonassessmentupdate";

    public static final String UPLOAD_COMPANY_LOGO = BASE_URL + "company/uploadcompanylogo";

    public static final String GET_REQUISITIONS_BY_ASSESSMENT = BASE_URL + "job/getrequisitionsbyassement";

    public static final String VALIDATE_MEETING_START_DATE_AND_END_DATE = BASE_URL + "candidate/validatemeetingstartdateandenddate";

    public static final String GET_USER_BY_TOKEN = BASE_URL + "user/getuserbytoken";

    public static final String CREATE_CANDIDATE_SCORE_WITH_VENDOR_JOBS = BASE_URL + "job/createcandidatescorewithvendorjobs";

    public static final String GET_ASSESSMENT_DETAILS_BY_ASSESSMENT_ID = BASE_URL + "assessment/getassessmentdetails";

    public static final String GET_EMAIL_DELIVERY_HISTORY = BASE_URL + "jobmatches/getemaildeliveryhistory";

    public static final String GET_SHARED_JOB_WORKFLOW_DETAIL = BASE_URL + "job/getsharedjobworkflowdetails";

    private ApiConstants() {
    }

}
