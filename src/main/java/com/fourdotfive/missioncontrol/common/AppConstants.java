package com.fourdotfive.missioncontrol.common;

public final class AppConstants {

	private AppConstants() {
	}

	public static final String USERNAME = "username";
	public static final String NEW_PWD = "newPassword";
	public static final String CUR_PWD = "curPassword";
	public static final String PASSWORD = "password";

	public static final String STAFFING_COMAPNY = "Staffing";
	public static final String COPR_COMPANY = "Corporate";
	public static final String BU = "Business Unit";

	public static final String ROLE = "role";
	public static final String ROLE_SCREEN = "ROLE_SCREEN";
	public static final String USER_ID = "userId";
	public static final String COMPANY_ID = "companyId";
	public static final String FIRST_NAME = "FIRST_NAME";
	public static final String LAST_NAME = "LAST_NAME";
	public static final String USER_VERSION = "USER_VERSION";
	public static final String SECRET_KEY = "our_super_secret_key";
	public static final int REMEMBER_ME_TOKEN_VALIDITY = 2;
	public static final int ACCESS_TOKEN_VALIDITY = 1;
	public static final String ADMIN = "Admin";
	public static final String AUTHORIZATION = "Authorization";

	public static final String ACCOUNT_STATE_ARCHIVED = "Archived";

	public static final int PASSWORD_MIN_LENGTH = 8;
	public static final int PASSWORD_MAX_LENGTH = 15;

	public static final int AUTH_SUBTRING = 7;

	public static final String JOB_REQ = "jobrequisition";

	public static final long THREE = 3;
	public static final long ONE_ZERO_TWO_FOUR = 1024;

	public static final String ARCHIVED = "Archived";
	
	public static final String ACTIVE = "Active";
	
	public static final String MODIFIED = "(Modified)";

	/** Role id's **/
	public static final String SUPERUSER_ROLEID = "57c36f5a820fea2470d6b9e3";
	public static final String FOURDOT5ADMIN_ROLEID = "57c36f5a820fea2470d6b9e7";

	public static final String STAFFING_ADMIN_ROLEID = "57c36f5a820fea2470d6b9e4";
	public static final String STAFFING_REC_MANAGER_ROLEID = "57c36f5a820fea2470d6b9ec";
	public static final String STAFFING_SRREC_ROLEID = "57c36f5a820fea2470d6b9e5";
	public static final String STAFFING_REC_ROLEID = "57c36f5a820fea2470d6b9e6";

	public static final String CORPORATE_ADMIN_ROLEID = "57c36f5a820fea2470d6b9e8";
	public static final String CORPORATE_REC_MANAGER_ROLEID = "57c36f5a820fea2470d6b9e9";
	public static final String CORPORATE_SRREC_ROLEID = "57c36f5a820fea2470d6b9eb";
	public static final String CORPORATE_REC_ROLEID = "57c36f5a820fea2470d6b9ea";
	public static final String HIRING_MANAGER_ROLEID = "57c36f5a820fea2470d6b1ad";

	public static final String EMPLOYEE_ROLEID = "57c36f5a820fea2470d6b1aa";
	public static final String CLIENT_EMPLOYEE_ROLEID = "57c36f5a820fea2470d6b1ab";

    public static final String FOUR_DOT_FIVE_INTELLIGENCE_SCORE = "4dot5 Intelligence Score";

    public static final String AUTOMATED_MATCH = "Automated Match";

	public static final String TECH_ASSESSMENT = "Tech Assessment";

	public static final String VALUE_ASSESSMENT = "Value Assessment";

	public static final String EDUCATION_VERIFICATION = "Education Verification";

	public static final String EXPERIENCE_VERIFICATION = "Experience Verification";
	
	public static final String VERIFICATION = "verification";
	public static final String ASSESSMENT = "assessment";
	public static final String SOURCE = "source";
	public static final String FILL = "fill";
	public static final String CONVERT = "convert";
	public static final String PLACE = "place";
	public static final String SCREEN = "screen";
	

	public static final String DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

	public static final boolean CAN_DELETE = true; // the user is authorized to
													// delete
	public static final boolean CAN_EDIT = true; // update
	public static final boolean IS_A_FORWARDED_RECIPIENT = true; 
	public static final boolean CANNOT_DELETE = false; // the user is not
														// authorized
														// to delete
	public static final boolean CANNOT_EDIT = false; // not update

	public static final boolean CAN_VIEW_NOTIFICATIONPREF = true; //The user can view notification preferences of other users.
	public static final boolean CANNOT_VIEW_NOTIFICATIONPREF = false;
	
	public static final boolean CAN_DRILLDOWN = true;// the user can not drill
														// down

	public static final boolean CANNOT_DRILLDOWN = false;// the user can drill
															// down to view

	public static final int NO_REPORTEES = 0;// the user no has reportees
	public static final String UN_VERIFIED = "UnVerfied";
	
	//License preference
	public static final String ALL_VERIFICATION = "Verification ( Education & Experience )";

	public static final String FEATURE_USER = "Users";
	public static final String FEATURE_CANDIDATES = "Candidates";
	public static final String FEATURE_REQUISITIONS = "Requisitions";
	public static final String FEATURE_MATCHING = "Matching";
	public static final String FEATURE_TECH_ASSESSMENT = "Tech Assessments";
	public static final String FEATURE_VALUE_ASSESSMENT = "Value Assessments";
	public static final String FEATURE_CUSTOMIZABLE_RECRUITER_SCREENING = "Customizable Recruiter Screening";
	public static final String FEATURE_CUSTOMIZABLE_PHONE_FEEDBACK = "Customizable Phone Feedback";
	public static final String FEATURE_CUSTOMIZABLE_INTERVIEW_FEEDBACK = "Customizable Interview Feedback";
	public static final String FEATURE_JOB_BOARD_INTEGRATION = "Job Board Integration";
	public static final String HIRING_MANAGER_ROLE_ID = "57c36f5a820fea2470d6b1ad";

	public static final String PLAN_STEP_EXPIRY_VALIDATION_MESSAGE = "This step is disabled because the current plan expired.";

}
