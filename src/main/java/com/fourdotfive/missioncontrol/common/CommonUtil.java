package com.fourdotfive.missioncontrol.common;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import com.fourdotfive.missioncontrol.dtos.candidate.CandidateRequestDto;
import com.fourdotfive.missioncontrol.dtos.company.AccessControlDto;
import com.fourdotfive.missioncontrol.dtos.company.CompanyMinInfoDto;
import com.fourdotfive.missioncontrol.dtos.company.CompanyMinDto;
import com.fourdotfive.missioncontrol.dtos.company.ReqCategoryDto;
import com.fourdotfive.missioncontrol.dtos.company.RoleScopeDto;
import com.fourdotfive.missioncontrol.dtos.company.SelectedClientOrBuDto;
import com.fourdotfive.missioncontrol.dtos.company.SubIndustryDto;
import com.fourdotfive.missioncontrol.dtos.job.JobOpeningDto;
import com.fourdotfive.missioncontrol.dtos.user.ClientOwner;
import com.fourdotfive.missioncontrol.dtos.user.TeamMemberDto;
import com.fourdotfive.missioncontrol.dtos.user.UserDto;
import com.fourdotfive.missioncontrol.exception.FileUploadException;
import com.fourdotfive.missioncontrol.exception.ForwardRecipientException;
import com.fourdotfive.missioncontrol.exception.UserConflictException;
import com.fourdotfive.missioncontrol.licensepreference.LicensePrefEnum;
import com.fourdotfive.missioncontrol.pojo.candidate.CandidateState;
import com.fourdotfive.missioncontrol.pojo.candidate.ListWrapper;
import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.company.CompanyState;
import com.fourdotfive.missioncontrol.pojo.company.Industry;
import com.fourdotfive.missioncontrol.pojo.job.JobOpening;
import com.fourdotfive.missioncontrol.pojo.user.AccessControlList;
import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.pojo.user.UserForwardedReference;
import com.fourdotfive.missioncontrol.security.JwtUtil;

/**
 * @author shalini, chakravarthy
 */
public final class CommonUtil {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(CommonUtil.class);
    private static final String ALGORITHM_NAME = "SHA-256";
    private static List<String> staffingCompanyDefinedOrder = Arrays.asList(
            AppConstants.STAFFING_REC_MANAGER_ROLEID,
            AppConstants.STAFFING_SRREC_ROLEID,
            AppConstants.STAFFING_REC_ROLEID);
    private static List<String> corporateCompanyDefinedOrder = Arrays.asList(
            AppConstants.CORPORATE_REC_MANAGER_ROLEID,
            AppConstants.CORPORATE_SRREC_ROLEID,
            AppConstants.CORPORATE_REC_ROLEID,
            AppConstants.HIRING_MANAGER_ROLEID);

    /**
     * create SHA-256 password
     *
     * @param password
     * @return
     * @throws NoSuchAlgorithmException
     */

    private CommonUtil() {
    }

    public static String sha256Password(String password)
            throws NoSuchAlgorithmException {

        MessageDigest md = MessageDigest.getInstance(ALGORITHM_NAME);

        md.update(password.getBytes());

        byte byteData[] = md.digest();

        // convert the byte to hex format method 1

        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < byteData.length; i++) {

            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16)
                    .substring(1));

        }

        // convert the byte to hex format method 2

        StringBuffer hexString = new StringBuffer();

        for (int i = 0; i < byteData.length; i++) {

            String hex = Integer.toHexString(0xff & byteData[i]);

            if (hex.length() == 1) {
                hexString.append('0');
            }

            hexString.append(hex);

        }

        return hexString.toString();

    }

    public static String formatDateTime(DateTime dateTime) {
        String strDateTime = null;
        // DateTimeFormatter fmt =
        // DateTimeFormat.forPattern("MM/dd/yyyy hh:mm a");
        DateTimeFormatter fmt = DateTimeFormat
                .forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        strDateTime = fmt.print(dateTime);

        return strDateTime;
    }

    public static String formatDate(String date) {
        DateTimeFormatter fmt1 = DateTimeFormat
                .forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        DateTimeFormatter fmt2 = DateTimeFormat
                .forPattern("dd MM yyyy hh:mm a");

        DateTime fromDateTime = fmt1.parseDateTime(date);
        date = fmt2.print(fromDateTime);

        return date;
    }

    public static String formatForwardedDate(String date) {
        DateTimeFormatter fmt1 = DateTimeFormat
                .forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        DateTimeFormatter fmt2 = DateTimeFormat
                .forPattern("dd/MM/yyyy HH:mm:ss");

        DateTime fromDateTime = fmt1.parseDateTime(date);
        date = fmt2.print(fromDateTime);
        LOGGER.info("check date format {}", date);

        return date;
    }

    public static TokenDetails getTokenDetails(String authHeader) {
        String authToken = authHeader.substring(AppConstants.AUTH_SUBTRING);
        return JwtUtil.getTokenDetails(authToken);
    }

    public static List<TeamMemberDto> mapStubUserToTeamMembers(
            List<User> stubUsers) {

        List<TeamMemberDto> teamMembers = new ArrayList<TeamMemberDto>();

        for (User user : stubUsers) {
            TeamMemberDto memeberDto = new TeamMemberDto(user.getFirstname(),
                    user.getId(), user.getLastname(), user.getRole().getName(),
                    user.getRole().getDescription(), user.getRole().getId(),
                    user.getAccessControlList().getAccountState());
            teamMembers.add(memeberDto);
        }
        return teamMembers;
    }

    public static List<TeamMemberDto> mapUsersToTeamMembers(List<User> users) {
        List<TeamMemberDto> teamMembers = new ArrayList<>();
        for (User tempUser : users) {
            teamMembers.add(mapUserToTeamMember(tempUser));
        }
        return teamMembers;
    }

    public static TeamMemberDto mapUserToTeamMember(User user) {

        TeamMemberDto memeberDto = new TeamMemberDto(user.getFirstname(),
                user.getId(), user.getLastname(), user.getRole().getName(),
                user.getRole().getDescription(), user.getRole().getId(), user
                .getAccessControlList().getAccountState());
        return memeberDto;
    }

    public static boolean checkFileWhileCreatingReq(List<MultipartFile> files)
            throws FileUploadException {
        if (files == null || files.size() == 0) {
            throw new FileUploadException(Messages.FILE_WAS_NOT_UPLOADED);
        } else if (files.size() > 1) {
            throw new FileUploadException(
                    Messages.CANNOT_UPLOAD_MORE_THAN_ONE_FILE);
        }
        return true;
    }

    public static boolean checkFileWhileUploadResume(List<MultipartFile> files)
            throws FileUploadException {
        if (files == null || files.size() == 0) {
            throw new FileUploadException(Messages.FILE_WAS_NOT_UPLOADED);
        }
        return true;
    }

    public static CompanyMinInfoDto mapCompanytoCompanyMinInfoDto(Company company) {

        ModelMapper mapper = new ModelMapper();

        PropertyMap<Company, CompanyMinInfoDto> newOrderMap = new PropertyMap<Company, CompanyMinInfoDto>() {

            @Override
            protected void configure() {
                map(source.getId(), destination.getCompanyId());
                map(source.getName(), destination.getCompanyName());
                map(source.getCompanyType(), destination.getCompanyType());

            }
        };
        mapper.createTypeMap(company, CompanyMinInfoDto.class).addMappings(
                newOrderMap);
        return mapper.map(company, CompanyMinInfoDto.class);
    }

    public static CompanyMinDto mapCompanytoCompanyMinDto(Company company) {

        ModelMapper mapper = new ModelMapper();

        PropertyMap<Company, CompanyMinDto> newOrderMap = new PropertyMap<Company, CompanyMinDto>() {

            @Override
            protected void configure() {
                map(source.getId(), destination.getId());
                map(source.getName(), destination.getName());
                map(source.getCompanyType(), destination.getCompanyType());

            }
        };
        mapper.createTypeMap(company, CompanyMinDto.class).addMappings(
                newOrderMap);
        return mapper.map(company, CompanyMinDto.class);
    }

    public static List<SelectedClientOrBuDto> mapSelectedClientOrBuDto(
            List<Company> selectedClientCompany,
            List<Company> allClientCompany, ClientOwner clientOwner) {
        List<SelectedClientOrBuDto> mappedClients = new ArrayList<SelectedClientOrBuDto>();
        for (Company selectedCompany : selectedClientCompany) {
            for (Iterator<Company> clientCompanies = allClientCompany
                    .iterator(); clientCompanies.hasNext(); ) {
                Company obj = clientCompanies.next();
                if (obj.getId().equals(selectedCompany.getId())) {
                    // Remove the current element from the iterator and the
                    // list.
                    clientCompanies.remove();
                }
            }
        }
        mappedClients.addAll(getAllClients(selectedClientCompany, true,
                clientOwner));
        mappedClients
                .addAll(getAllClients(allClientCompany, false, clientOwner));
        return mappedClients;
    }

    public static List<ReqCategoryDto> mapSelectedReqCategories(
            List<String> selectedReqCategories, List<String> allReqCategories) {
        List<ReqCategoryDto> mappedClients = new ArrayList<ReqCategoryDto>();
        for (String selectedCompany : selectedReqCategories) {
            for (Iterator<String> clientCompanies = allReqCategories.iterator(); clientCompanies
                    .hasNext(); ) {
                String obj = clientCompanies.next();
                if (obj.equals(selectedCompany)) {
                    // Remove the current element from the iterator and the
                    // list.
                    clientCompanies.remove();
                }
            }
        }
        mappedClients.addAll(getAllReqCategories(selectedReqCategories, true));
        mappedClients.addAll(getAllReqCategories(allReqCategories, false));
        return mappedClients;
    }

    public static List<SelectedClientOrBuDto> getAllClients(
            List<Company> allClients, boolean isSelected,
            ClientOwner clientOwner) {
        List<SelectedClientOrBuDto> mappedClients = new ArrayList<SelectedClientOrBuDto>();
        for (Company clientCompany : allClients) {
            if (clientCompany != null
                    && clientCompany.getCompanyState() != null
                    && !clientCompany.getCompanyState().equals(
                    CompanyState.Archived.toString())
                    && !clientCompany.getCompanyState().equals(
                    CompanyState.Draft.toString())) {
                SelectedClientOrBuDto clientOrBuDto = new SelectedClientOrBuDto(
                        clientCompany.getName(), clientCompany.getId(),
                        clientCompany.getCompanyType(),
                        clientCompany.getCompanyState(), isSelected,
                        clientOwner);
                mappedClients.add(clientOrBuDto);
            }
        }
        return mappedClients;
    }

    public static List<SelectedClientOrBuDto> getAllDisabledClients(
            List<Company> allClients, boolean isSelected, boolean isDisabled,
            ClientOwner clientOwner) {
        Collections.sort(allClients, new CompanyNameComparator());
        List<SelectedClientOrBuDto> mappedClients = new ArrayList<SelectedClientOrBuDto>();
        for (Company clientCompany : allClients) {
            SelectedClientOrBuDto clientOrBuDto = new SelectedClientOrBuDto(
                    clientCompany.getName(), clientCompany.getId(),
                    clientCompany.getCompanyType(),
                    clientCompany.getCompanyState(), isSelected, isDisabled,
                    clientOwner);
            mappedClients.add(clientOrBuDto);
        }
        return mappedClients;
    }

    public static List<ReqCategoryDto> getAllReqCategories(
            List<String> reqCategories, boolean flag) {
        List<ReqCategoryDto> mappedClients = new ArrayList<ReqCategoryDto>();
        for (String reqCategory : reqCategories) {
            ReqCategoryDto reqDto = new ReqCategoryDto(reqCategory, flag);
            mappedClients.add(reqDto);
        }
        return mappedClients;
    }

    public static AccessControlDto mapScopetoACLdto(Company company,
                                                    AccessControlList scope, AccessControlDto accessControlDto) {
        accessControlDto.setAllclientOrBU(scope.isAllclientOrBU());
        accessControlDto.setAllIndustries(scope.isAllIndustries());
        accessControlDto.setAllJobRequisitionTitleList(scope
                .isAllJobRequisitionTitleList());
        accessControlDto.setRequisitionCategory(scope.isRequisitionCategory());
        return accessControlDto;
    }

    public static List<SubIndustryDto> getAllUserIndustries(
            List<Industry> selectedIndustries, List<Industry> allIndustries) {
        List<SubIndustryDto> mappedIndustries = new ArrayList<SubIndustryDto>();
        for (Industry selectedIndustry : selectedIndustries) {
            for (Iterator<Industry> industries = allIndustries.iterator(); industries
                    .hasNext(); ) {
                Industry obj = industries.next();
                if (obj.getName().equals(selectedIndustry.getName())) {
                    // Remove the current element from the iterator and the
                    // list.
                    industries.remove();
                }
            }
        }
        mappedIndustries.addAll(getUserIndustries(selectedIndustries, true));
        mappedIndustries.addAll(getUserIndustries(allIndustries, false));
        return mappedIndustries;
    }

    public static List<SubIndustryDto> getUserIndustries(
            List<Industry> allIndustries, boolean flag) {
        List<SubIndustryDto> mappedIndustries = new ArrayList<SubIndustryDto>();
        for (Industry industry : allIndustries) {
            SubIndustryDto subIndustry = new SubIndustryDto(industry.getId(),
                    industry.getDescription(), industry.getName(), flag);
            mappedIndustries.add(subIndustry);
        }
        return mappedIndustries;
    }

    public static List<SubIndustryDto> getSubIndustries(
            List<Industry> allIndustries, boolean flag) {
        List<SubIndustryDto> mappedIndustries = new ArrayList<SubIndustryDto>();
        for (Industry industry : allIndustries) {
            SubIndustryDto subIndustry = new SubIndustryDto(industry.getId(),
                    industry.getDescription(), industry.getName(), flag);
            mappedIndustries.add(subIndustry);
        }
        return mappedIndustries;
    }

    public static List<JobOpeningDto> getJobOpeningsDto(
            List<JobOpening> jobOpenings) {
        List<JobOpeningDto> jobOpeningDtos = new ArrayList<>();
        for (JobOpening jobOpening : jobOpenings) {
            JobOpeningDto jobOpeningDto = new JobOpeningDto();
            jobOpeningDto.setAssignedRecruiters(mapUsersToTeamMembers(jobOpening
                    .getAssignedRecruiters()));
            jobOpeningDto.setOpeningNumber(jobOpening.getOpeningNumber());
        }
        return jobOpeningDtos;
    }

    public static ListWrapper mapCandidateSearch(CandidateRequestDto requestDto) {

        ListWrapper listWrapper = new ListWrapper();
        if (requestDto != null) {
            listWrapper.setJobRequistionIds(requestDto.getJobReqList());
            listWrapper.setRecruiterIds(requestDto.getRecruiterList());
            listWrapper.setClientOrBuIds(requestDto.getClientOrBuList());
            if (requestDto.getCandidateState().equals(CandidateState.ACTIVE)) {
                listWrapper
                        .setCandidateActiveStateTypes(requestDto.getStates());
            } else if (requestDto.getCandidateState().equals(
                    CandidateState.PASSIVE)) {
                listWrapper.setCandidatePassiveStateTypes(requestDto
                        .getStates());

            }
            if (requestDto.getDateRange().equals(DateRange.DATE_RANGE)) {
                listWrapper.setfDate(requestDto.getFromDate());
                listWrapper.settDate(requestDto.getToDate());
            } else {
                DateTime[] dateTimes = CommonUtil.getDateTimeFormat(requestDto
                        .getDateRange());
                listWrapper.setfDate(dateTimes[0]);
                listWrapper.settDate(dateTimes[1]);
            }
        }
        return listWrapper;

    }

    public static DateTime[] getDateTimeFormat(DateRange dateRange) {

        switch (dateRange) {
            case THIS_WEEK: {
                DateTime dateTime = new DateTime();
                DateTime startDate = dateTime.dayOfWeek().withMinimumValue();
                DateTime endDate = new DateTime();
                return new DateTime[]{startDate, endDate};
            }
            case LAST_WEEK: {
                DateTime dateTime = new DateTime();
                DateTime startDate = dateTime.dayOfWeek().withMinimumValue()
                        .minusWeeks(1);
                DateTime endDate = startDate.dayOfWeek().withMaximumValue();
                return new DateTime[]{startDate, endDate};
            }
            case PAST_SIX_WEEKS: {
                DateTime dateTime = new DateTime();
                DateTime startDate = dateTime.dayOfWeek().withMinimumValue()
                        .minusWeeks(5);
                DateTime endDate = new DateTime();
                return new DateTime[]{startDate, endDate};
            }
            case THIS_MONTH: {
                DateTime dateTime = new DateTime();
                DateTime startDate = dateTime.withDayOfMonth(1);
                DateTime endDate = new DateTime();
                return new DateTime[]{startDate, endDate};
            }
            case LAST_MONTH: {
                DateTime dateTime = new DateTime();
                DateTime startDate = dateTime.minusMonths(1).withDayOfMonth(1);
                DateTime endDate = dateTime.minusMonths(1).dayOfMonth()
                        .withMaximumValue();
                return new DateTime[]{startDate, endDate};
            }
            case PAST_SIX_MONTHS: {
                DateTime dateTime = new DateTime();
                DateTime startDate = dateTime.minusMonths(5).withDayOfMonth(1);
                DateTime endDate = new DateTime();
                return new DateTime[]{startDate, endDate};
            }
            case THIS_QTR: {
                DateTime dateTime = new DateTime();
                int month = dateTime.getMonthOfYear();
                month = month % 3;
                DateTime startDate = dateTime.minusMonths(month).withDayOfMonth(1);
                DateTime endDate = new DateTime();
                return new DateTime[]{startDate, endDate};
            }
            case LAST_QTR: {
                DateTime dateTime = new DateTime();
                int month = dateTime.getMonthOfYear();
                month = month % 3;
                DateTime startDate = dateTime.minusMonths(month);
                startDate = startDate.minusMonths(3).withDayOfMonth(1);
                DateTime endDate = dateTime.minusMonths(month).minusDays(1);
                return new DateTime[]{startDate, endDate};
            }
            case PAST_SIX_QTRS: {
                DateTime dateTime = new DateTime();
                int month = dateTime.getMonthOfYear();
                month = month % 3;
                DateTime startDate = dateTime.minusMonths(month).withDayOfMonth(1);
                startDate = startDate.minusMonths(3 * 5);
                DateTime endDate = new DateTime();
                return new DateTime[]{startDate, endDate};
            }
            default:
                return null;
        }

    }

    public static boolean isForwardRecipientViewable(User user) {

        if (user.getForwardFromStartDate() == null
                || user.getForwardToEndDate() == null) {
            return false;
        }

        DateTime fromDate = new DateTime(Long.parseLong(user
                .getForwardFromStartDate()));
        DateTime toDate = new DateTime(Long.parseLong(user
                .getForwardToEndDate()));
        DateTime dateTime = new DateTime();
        if (dateTime.isAfter(fromDate) && dateTime.isBefore(toDate)) {
            return true;
        }
        return false;
    }

    public static boolean isForwardRecipientViewable(UserForwardedReference user) {

        if (user.getForwardFromStartDate() == null
                || user.getForwardToEndDate() == null) {
            return false;
        }

        DateTime fromDate = new DateTime(user.getForwardFromStartDate()
                .getYear(), user.getForwardFromStartDate().getMonthOfYear(),
                user.getForwardFromStartDate().getDayOfMonth(), user
                .getForwardFromStartDate().getHourOfDay(), user
                .getForwardFromStartDate().getMinuteOfHour());
        DateTime toDate = new DateTime(user.getForwardToEndDate().getYear(),
                user.getForwardToEndDate().getMonthOfYear(), user
                .getForwardToEndDate().getDayOfMonth(), user
                .getForwardToEndDate().getHourOfDay(), user
                .getForwardToEndDate().getMinuteOfHour());

        DateTime dateTime = new DateTime();
        if (dateTime.isAfter(fromDate) && dateTime.isBefore(toDate)) {
            return true;
        }
        return false;
    }

    public static void isForwardRecipientinDate(User newUpdatedUser,
                                                User forwardRecipient, User forwardRecipientsRecipient) {
        try {
            if (newUpdatedUser == null
                    || newUpdatedUser.getForwardFromStartDate() == null
                    || newUpdatedUser.getForwardToEndDate() == null) {
                LOGGER.info("users forward date is null ");
                return;
            }
            if (forwardRecipient == null
                    || forwardRecipient.getForwardFromStartDate() == null
                    || forwardRecipient.getForwardToEndDate() == null) {
                LOGGER.info("forward recipient forward date is null ");
                return;
            }
            // for current updating user
            DateTime userFromDate = new DateTime(
                    newUpdatedUser.getForwardFromStartDate());
            DateTime userToDate = new DateTime(
                    newUpdatedUser.getForwardToEndDate());
            LOGGER.info("user recipeint date: {}", userFromDate + "  "
                    + userToDate);
            // forward recipient's recipient user
            DateTime forwardUserFromDate = new DateTime(
                    Long.parseLong(forwardRecipient.getForwardFromStartDate()));
            DateTime forwardUserToDate = new DateTime(
                    Long.parseLong(forwardRecipient.getForwardToEndDate()));
            LOGGER.info("forward recipeint date: {}", forwardUserFromDate
                    + "  " + forwardUserToDate);
            String forwardRecipientName = getUserName(newUpdatedUser.getRecipientUser());
            String forwardRecipientsRecipientName = getUserName(forwardRecipientsRecipient);
            if ((userFromDate.isEqual(forwardUserFromDate) || userFromDate
                    .isAfter(forwardUserFromDate))
                    && (userFromDate.isEqual(forwardUserToDate) || userFromDate
                    .isBefore(forwardUserToDate))) {

                LOGGER.info("start date lies in between ");
                throw new UserConflictException(Messages.FORWARD_RECIPIENT_CONFLICT, forwardRecipientName, forwardRecipientName, forwardRecipientsRecipientName);
            }
            if ((userToDate.isEqual(forwardUserFromDate) || userToDate
                    .isAfter(forwardUserFromDate))
                    && (userToDate.isEqual(forwardUserFromDate) || userToDate
                    .isBefore(forwardUserToDate))) {

                LOGGER.info("end date lies in between ");
                throw new UserConflictException(Messages.FORWARD_RECIPIENT_CONFLICT, forwardRecipientName, forwardRecipientName, forwardRecipientsRecipientName);
            }
            if ((userFromDate.isEqual(forwardUserFromDate) || userFromDate
                    .isBefore(forwardUserFromDate))
                    && (userToDate.isEqual(forwardUserToDate) || userToDate
                    .isAfter(forwardUserToDate))) {

                LOGGER.info("start date lies before and end date lies after ");
                throw new UserConflictException(Messages.FORWARD_RECIPIENT_CONFLICT, forwardRecipientName, forwardRecipientName, forwardRecipientsRecipientName);
            }
        } catch (NumberFormatException e) {
            throw new ForwardRecipientException(Messages.EXCEPTION_WHILE_ADDING_FORWARD_RECIPIENT);
        }

    }


    public static List<User> getUnarchivedTeamMembers(List<User> reporteeList) {
        List<User> dtos = new ArrayList<User>();
        if (reporteeList != null && !reporteeList.isEmpty()) {
            for (User u : reporteeList) {
                if (!u.getAccessControlList().getAccountState()
                        .equals(AppConstants.ACCOUNT_STATE_ARCHIVED)) {
                    dtos.add(u);
                }
            }
        }
        return dtos;
    }

    public static String formatLicensePrefName(LicensePrefEnum licensePrefEnum) {

        String name = licensePrefEnum.toString();
        try {
            int index = licensePrefEnum.toString().indexOf("_");
            if (index > 1) {
                name = new StringBuilder()
                        .append(name.subSequence(0, 1).toString())
                        .append(name.subSequence(1, index).toString()
                                .toLowerCase())
                        .append(" ")
                        .append(name.subSequence(index + 1, index + 2)
                                .toString())
                        .append(name.subSequence(index + 2, name.length())
                                .toString().toLowerCase()).toString();
            } else {
                name = new StringBuilder()
                        .append(name.substring(0, 1).toString())
                        .append(name.substring(1, name.length()).toString()
                                .toLowerCase()).toString();
            }
        } catch (IndexOutOfBoundsException e) {
            LOGGER.error(
                    "Error occured while formatting license preference name {}",
                    e.getMessage());
        }
        return name;
    }

    public static String formatEnumToString(Object... args) {

        String[] names = args[0].toString().split("_");
        StringBuilder nameBuilder = new StringBuilder();
        String name = "";
        for (int i = 0; i < names.length; i++) {
            if (i == 0) {
                name = names[i].substring(0, 1)
                        + names[i].substring(1, names[i].length())
                        .toLowerCase();
            } else {
                if (names[i].equalsIgnoreCase(AppConstants.VERIFICATION)
                        || names[i].equalsIgnoreCase(AppConstants.ASSESSMENT)
                        || names[i].equalsIgnoreCase(AppConstants.SOURCE)
                        || names[i].equalsIgnoreCase(AppConstants.FILL)
                        || names[i].equalsIgnoreCase(AppConstants.CONVERT)
                        || names[i].equalsIgnoreCase(AppConstants.PLACE)
                        || names[i].equalsIgnoreCase(AppConstants.SCREEN)) {
                    name = names[i].substring(0, 1)
                            + names[i].substring(1, names[i].length())
                            .toLowerCase();
                } else {
                    name = names[i].toLowerCase();
                }
            }

            nameBuilder.append(name).append(" ");
        }

        return nameBuilder.toString();
    }

    public static String getUserName(UserDto userDto) {
        StringBuffer name = new StringBuffer();

        name.append(userDto.getUserDetails().getFirstName());

        if (userDto.getUserDetails().getLastName() != null) {
            name.append(" ").append(userDto.getUserDetails().getLastName());
        }

        return name.toString();
    }

    public static String getUserName(User user) {
        StringBuffer name = new StringBuffer();

        name.append(user.getFirstname());

        if (user.getLastname() != null) {
            name.append(" ").append(user.getLastname());
        }

        return name.toString();
    }

    public static boolean checkFileWhileUploadImage(MultipartFile file)
            throws FileUploadException {
        if (file == null || file.isEmpty()) {
            throw new FileUploadException(Messages.IMAGE_WAS_NOT_UPLOADED);
        }
        return true;
    }

    public static class StaffingCompanyComparator implements
            Comparator<RoleScopeDto> {

        @Override
        public int compare(RoleScopeDto o1, RoleScopeDto o2) {
            return Integer.valueOf(
                    staffingCompanyDefinedOrder.indexOf(o1.getRole().getId()))
                    .compareTo(
                            Integer.valueOf(staffingCompanyDefinedOrder
                                    .indexOf(o2.getRole().getId())));
        }
    }

    public static class CorportateCompanyComparator implements
            Comparator<RoleScopeDto> {

        @Override
        public int compare(RoleScopeDto o1, RoleScopeDto o2) {
            return Integer.valueOf(
                    corporateCompanyDefinedOrder.indexOf(o1.getRole().getId()))
                    .compareTo(
                            Integer.valueOf(corporateCompanyDefinedOrder
                                    .indexOf(o2.getRole().getId())));
        }
    }
}
