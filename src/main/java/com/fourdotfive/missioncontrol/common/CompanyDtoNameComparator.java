package com.fourdotfive.missioncontrol.common;

import java.io.Serializable;
import java.util.Comparator;

import com.fourdotfive.missioncontrol.dtos.company.SelectedClientOrBuDto;

public class CompanyDtoNameComparator implements
		Comparator<SelectedClientOrBuDto>, Serializable {

	private static final long serialVersionUID = -9098333092342245136L;

	@Override
	public int compare(SelectedClientOrBuDto o1, SelectedClientOrBuDto o2) {

		return o1.getCompanyName().compareTo(o2.getCompanyName());
	}

}
