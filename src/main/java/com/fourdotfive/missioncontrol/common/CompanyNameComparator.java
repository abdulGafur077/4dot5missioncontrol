package com.fourdotfive.missioncontrol.common;

import java.io.Serializable;
import java.util.Comparator;

import com.fourdotfive.missioncontrol.pojo.company.Company;

public class CompanyNameComparator implements Comparator<Company>, Serializable{

	private static final long serialVersionUID = -9098333092342245136L;

	@Override
	public int compare(Company o1, Company o2) {
		
		return o1.getName().toLowerCase().compareTo(o2.getName().toLowerCase());  
	}

}
