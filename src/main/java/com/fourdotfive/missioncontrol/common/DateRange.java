package com.fourdotfive.missioncontrol.common;

public enum DateRange {
	
	THIS_WEEK,LAST_WEEK,PAST_SIX_WEEKS,THIS_MONTH,LAST_MONTH,PAST_SIX_MONTHS,THIS_QTR,LAST_QTR,PAST_SIX_QTRS,DATE_RANGE

}
