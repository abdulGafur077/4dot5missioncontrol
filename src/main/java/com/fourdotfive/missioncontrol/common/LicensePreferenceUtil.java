package com.fourdotfive.missioncontrol.common;

import com.fourdotfive.missioncontrol.licensepreference.LicensePrefDetails;
import com.fourdotfive.missioncontrol.licensepreference.LicensePrefDto;
import com.fourdotfive.missioncontrol.licensepreference.LicensePrefDtoList;
import com.fourdotfive.missioncontrol.licensepreference.LicensePrefEnum;
import org.springframework.stereotype.Service;

@Service
public  class LicensePreferenceUtil {
	
	public static LicensePrefDto getLicensePrefDto(LicensePrefDtoList list, LicensePrefEnum licensePrefEnum) {
		
		LicensePrefDto licensePrefDto = null ;

		switch (licensePrefEnum) {
		case FOUR_DOT_FIVE_INTELLIGENCE:
			licensePrefDto = list.getFourDotFiveIntellList();
			break;
		case TECH_ASSESSMENT:
			licensePrefDto = list.getTechAssessmentList();
			break;
		case VALUE_ASSESSMENT:
			licensePrefDto = list.getValueAssessmentList();
			break;
		case EXPERIENCE_VERIFICATION:
			licensePrefDto = list.getVerificationList();
			break;
		case EDUCATION_VERIFICATION:
			licensePrefDto = list.getVerificationList();
			break;
		case VERIFICATION:
			licensePrefDto = list.getVerificationList();
			break;

		case PHONE_SCREEN:
			licensePrefDto = list.getPhoneScreenList();
			break;

		case INTERVIEW:
			licensePrefDto = list.getInterviewList();
		break;
			
		default:
			break;
		}

		return licensePrefDto;
	}
	
	public LicensePrefDetails getLicensePrefDetails(LicensePrefDtoList list, LicensePrefEnum licensePrefEnum){
		
		LicensePrefDto licensePrefDto = getLicensePrefDto(list, licensePrefEnum);
		LicensePrefDetails prefDetails = getLicensePrefDetails(licensePrefDto, licensePrefEnum);
		
		return prefDetails;
		
	}
	
	public LicensePrefDetails getLicensePrefDetails(LicensePrefDto pList,
			LicensePrefEnum licensePrefEnum) {
		LicensePrefDetails retPrefDetails = null;

		for (LicensePrefDetails details : pList.getLicensePrefDetailsList()) {

			if (details.getLicensePrefEnum() != licensePrefEnum) {
				continue;
			}

			switch (details.getLicensePrefEnum()) {

			case FOUR_DOT_FIVE_INTELLIGENCE:
				retPrefDetails = details;
				break;

			case TECH_ASSESSMENT:
				retPrefDetails = details;
				break;

			case VALUE_ASSESSMENT:
				retPrefDetails = details;
				break;

			case EDUCATION_VERIFICATION:
				retPrefDetails = details;
				break;

			case EXPERIENCE_VERIFICATION:
				retPrefDetails = details;
				break;

			case PHONE_SCREEN:
				retPrefDetails = details;
				break;
                case INTERVIEW:
				retPrefDetails = details;
				break;

                case AUTO_MATCH:
                    retPrefDetails = details;
                    break;

                case RECRUITER_SCREENING:
                    retPrefDetails = details;
                    break;

                default:
				retPrefDetails = null;
				break;
			}

			break;
		}

		return retPrefDetails;
	}


}
