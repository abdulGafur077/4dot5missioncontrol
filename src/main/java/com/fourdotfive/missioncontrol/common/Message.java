package com.fourdotfive.missioncontrol.common;

import org.springframework.http.HttpStatus;

/**
 * For status code for 400,401 and 500 from platform or internal server error(error occurring in 4dot5 app), the custom message that needs to be sent
 * to the UI will be built here along with the exception message and status code
 *
 * @author shalini
 */
public class Message {
    private String statusCode;
    private String message;
    private String developerMsg;
    private String exception;
    private Severity severity;

    public Message(String statusCode, String message, String developerMsg,
                   String exception, Severity severity) {
        super();
        this.statusCode = statusCode;
        this.message = message;
        this.developerMsg = developerMsg;
        this.exception = exception;
        this.severity = severity;
    }

    public Message() {
        super();
    }

    public static MessageBuilder statusCode(HttpStatus httpStatus) {
        return new MessageBuilder(httpStatus);
    }

    /**
     * @return the statusCode
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode the statusCode to set
     */
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the developerMsg
     */
    public String getDeveloperMsg() {
        return developerMsg;
    }

    /**
     * @param developerMsg the developerMsg to set
     */
    public void setDeveloperMsg(String developerMsg) {
        this.developerMsg = developerMsg;
    }

    /**
     * @return the exception
     */
    public String getException() {
        return exception;
    }

    /**
     * @param exception the exception to set
     */
    public void setException(String exception) {
        this.exception = exception;
    }

    @Override
    public String toString() {
        return "Message{" +
                "statusCode='" + statusCode + '\'' +
                ", message='" + message + '\'' +
                ", developerMsg='" + developerMsg + '\'' +
                ", exception='" + exception + '\'' +
                ", severity=" + severity +
                '}';
    }


    public Severity getSeverity() {
        return severity;
    }

    public void setSeverity(Severity severity) {
        this.severity = severity;
    }
}
