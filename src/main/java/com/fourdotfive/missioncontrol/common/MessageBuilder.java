package com.fourdotfive.missioncontrol.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import java.util.Objects;

/**
 * For the custom message that needs to be sent to the UI, those message will be
 * built here along with the exception message and status code
 *
 * @author shalini
 */
public class MessageBuilder {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageBuilder.class);
    private HttpStatus statusCode;
    private String message;
    private String developerMsg;
    private String exception;
    private Severity severity;

    MessageBuilder(HttpStatus statusCode) {
        super();
        this.statusCode = statusCode;
    }

    public static MessageBuilder statusCode(HttpStatus httpStatus) {
        return new MessageBuilder(httpStatus);
    }

    public MessageBuilder message(String message) {
        this.message = message;
        return this;
    }

    public MessageBuilder developerMsg(String developerMsg) {
        this.developerMsg = developerMsg;
        return this;
    }

    public MessageBuilder exception(String exception) {
        this.exception = exception;
        return this;
    }

    public MessageBuilder severity(Severity severity) {
        this.severity = severity;
        return this;
    }

    public Message build() {
        Objects.requireNonNull(this.statusCode, "statusCode must not be null");
        Message msg = null;
        try {
            if (severity == null) {
                switch (statusCode) {
                    case OK:
                    case CREATED:
                        severity = Severity.SUCCESS;
                        break;
                    default:
                        severity = Severity.DANGER;
                }
            }
            msg = new Message(this.statusCode.toString(), this.message, this.developerMsg, this.exception, this.severity);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

        return msg;
    }

}
