package com.fourdotfive.missioncontrol.common;

public final class Messages {

	private Messages() {
	}

	public static final String SHORTNAME_MANDATORY = "Short name is mandatory.";
	public static final String NAME_MANDATORY = "Name is mandatory.";
	public static final String COUNTRY_MANDATORY = "Country is mandatory.";
	public static final String ZIPCODE_MANDATORY = "Zipcode is mandatory.";
	public static final String ADDRESS1_MANDATORY = "Address1 is mandatory.";
	public static final String CITY_MANDATORY = "City is mandatory.";

	// Acccess control messages
	public static final String MSG = "Invalid credentials.";
	public static final String INVALID_EMAIL_OR_PASSWORD = "Invalid email or password.";
	public static final String INVALID_EMAIL = "Invalid email address.";
	public static final String INVALID_CREDENTIALS = "Please enter valid credentials.";
	public static final String INVALID_ORIG_PWD = "Invalid original password.";
	public static final String CHANGE_PWD_SUCCESS = "Your password has been changed successfully.";
	public static final String SET_PWD_SUCCESS = "Your password has been created successfully.";

	// Company messages

	public static final String DUPLICATE_COMPANY = "Company with Short Name '<b><i>%s</i></b>' or  Name '<b><i>%s</i></b>' already exists.";
	public static final String NAME_EXISTS = "%s with '<b><i>%s</i></b>' Name already exists.";
	public static final String SHORT_NAME_EXISTS = "%s with '<b><i>%s</i></b>' Short Name already exists.";
	public static final String NAME_AND_SHORTNAME_EXISTS = "%s with '<b><i>%s</i></b>' Short Name and '<b><i>%s</i></b>' Name already exists.";

	// Role entity scope definition
	public static final String ROLE_ENTITY_SCOPE_NOT_DEFINED = "Role Scope for this company has not been defined. Please define the scope for all roles within the company and then create/update users.";
	public static final String ROLE_ENTITY_SCOPE_NOT_DEFINED_HM = "Role Scope for this company has not been defined. Please define the scope for all roles within the company and then create hiring manager as contact.";

	// License preferences
	public static final String LICENSE_PREF_MODIFIED = "The License preferences have been modified, please set it again.";
	public static final String LICENSE_PREF_NOT_AVAILABLE_NOW = " is not available at the moment.";
	public static final String LICENSE_PREF_NOT_AVAILABLE = " is not available.";
	public static final int LICENSE_PREF_NOT_AVAILABLE_VALUE = 1 ;
	public static final int LICENSE_PREF_NOT_AVAILABLE_AT_THE_MOMENT_VALUE = 2 ;
	public static final String BOTH_NOT_AVAILABLE = "Education Verification and Experience Verification is not available.";
	public static final String BOTH_NOT_AVAILABLE_AT_THE_MOMENT = "Education Verification and Experience Verification is not available at the moment.";

	// User messages
	public static final String DUPLICATE_USER = "User with email id '<b><i>%s</i></b>' already exists.";
	public static final String EMAIL_MANDATORY = "Email is mandatory.";
	public static final String FIRST_NAME_MANDATORY = "First name is mandatory.";
	public static final String MOBILE_MANDATORY = "Mobile number is mandatory.";
	public static final String IM_MANDATORY = "Im is mandatory.";
	public static final String DELETE_FOURDOTFIVE_ADMIN = "Cannot delete this user as he/she has been assigned to one or more company.";
	public static final String DELETING_USER_HAS_FORWARD_RICIPIENT = "Cannot delete '<b><i><a>%s</a></i></b>' as he/she has been assigned as a forward recipient to";
	public static final String DELETE_USER_WITH_REPORTESS = "Cannot delete '<b><i>%s</i></b>' as he/she has direct reports assigned. Please re-assign them before deleting this user again.";
	public static final String REPORTEES_ASSIGN = "Cannot change the role of this user as he/she has direct reports assigned.Please re-assign them before trying to change the role again.";

	// File upload
	public static final String INVALID_FILE_FORMAT = "Upload only .pdf or .doc file.";
	public static final String CANNOT_UPLOAD_MORE_THAN_ONE_FILE = "Can't upload more than one file.";
	public static final String FILE_WAS_NOT_UPLOADED = "File was not uploaded.";
	public static final String FILE_SIZE_IS_GREATER = "File size should be less tham 3 mb.";

	//Image Upload
	public static final String IMAGE_WAS_NOT_UPLOADED = "Image was not uploaded.";

	// Platform messages
	public static final String INTERNAL_ERROR_MSG = "Internal error, please contact system admin.";
	public static final String NULL_RESPONSE = "Internal error";

	// Job
	public static final String JOB_NOT_FOUND = "Job match not found";

	public static final String CLIENT_ALL_LIST_CANNOT_HAVE_SAME_VALUE = "Client All and list cannot have same value";

	// License preference messages
	public static final String PREFERENCES_NOT_EQUAL = "The license preferences of the two companies does not match";

	public static final String EXCEPTION_WHILE_CREATING_USER = "Error while creating user '<b><i>%s</i></b>', please try to create the user again.";
	public static final String EXCEPTION_WHILE_CREATING_USER_AND_ROLLBACK = "Error while creating user '<b><i>%s</i></b>', please contact system admin.";
	public static final String EXCEPTION_WHILE_UPDATING_USER = "Error while updating user";

	public static final String EXCEPTION_WHILE_ADDING_FORWARD_RECIPIENT = "Error while creating/updating forward recipient, please contact system admin";

	public static final String EXCEPTION_WHILE_UPDATING_RES = "Error while creating Role Entity Scope this company, please contact system admin";
	public static final String EXCEPTION_WHILE_ROLE_UPDATE = "Error while updating role to '<b><i>%s</i></b>', please try again.";
	public static final String EXCEPTION_WHILE_ROLE_UPDATE_ROLLBACK = "Error while updating role to '<b><i>%s</i></b>', please contact system admin as rollback also failed.";
	public static final String EXCEPTION_WHILE_CREATING_COMPANY = "Error while creating/updating company, please contact system admin";
	public static final String EXCEPTION_WHILE_REMOVING_FORWARD_RECIPIENT = "Error while removing '<b><i>%s</i></b>' from Forward Recipient, please try again.";
	public static final String EXCEPTION_WHILE_CREATING_FORWARD_RECIPIENT = "Error while assigning '<b><i>%s</i></b>' as Forward Recipient, please try again.";
	public static final String EXCEPTION_WHILE_FETCHING_FOURDOTFIVE_ADMIN = "Error while fetching 4Dot5Admin";
	public static final String EXCEPTION_WHILE_ROLLING_BACK_FORWARD_RECIPIENT = "Error while assigning '<b><i>%s</i></b>' as Forward Recipient, please contact system admin as rolling back also failed.";
	public static final String EXCEPTION_WHILE_ASSIGNING_4_DOT_ADMIN = "Error while assigning '<b><i>%s</i></b>' as 4Dot5Admin, please try again.";
	public static final String EXCEPTION_WHILE_SAVING_4_DOT_5_ADMIN_COMPANY = "Error while assigning '<b><i>%s</i></b>' as 4Dot5Admin, please contact system admin as rollback also failed.";
	public static final String FORWARD_RECIPIENT_CONFLICT = new StringBuilder()
			.append("'<b><i>%s").append("</i></b>'")
			.append(" cannot be set as the forward recipient as ")
			.append("'<b><i>%s").append("</i></b>'")
			.append(" is already a forward recipient to ").append("'<b><i>%s")
			.append("</i></b>'").append(" during this time period.").toString();
	public static final String EXCEPTION_WHILE_DELETING_CLIENT = "Error while deleting Client.";
	public static final String EXCEPTION_WHILE_DELETING_BU = "Error while deleting BU.";
	public static final String EXCEPTION_WHILE_DELETING_COMPANY = "Error while deleting company.";
	public static final String EXCEPTION_WHILE_FETCHING_COMPANY = "Error while fetching company.";
	
	
	public static final String EX_SAVE_FOURDOTFIVE_ADMIN = "An exception occurred while saving four dot five admin";
	public static final String EX_GET_COMPANY = "An exception occurred while fetching company";

	public static final String EXCEPTION_WHILE_REGENERATING_REPORT = "Could not find Assessment Details for the Candidate";
}
