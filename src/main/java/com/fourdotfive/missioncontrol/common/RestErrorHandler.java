package com.fourdotfive.missioncontrol.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourdotfive.missioncontrol.candidate.CandidateController;
import com.fourdotfive.missioncontrol.candidate.JobNotFoundException;
import com.fourdotfive.missioncontrol.company.CompanyException;
import com.fourdotfive.missioncontrol.exception.*;
import com.fourdotfive.missioncontrol.pojo.FourDotFiveResponse;
import com.fourdotfive.missioncontrol.user.UserException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpStatusCodeException;

import java.io.IOException;

/**
 * @author shalini
 */
@ControllerAdvice
public class RestErrorHandler {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(CandidateController.class);

    @ExceptionHandler(value = {HttpStatusCodeException.class})
    @ResponseBody
    public ResponseEntity<Message> error(HttpStatusCodeException e) {
        Message errorMessage;
        LOGGER.error(e.getMessage(), e);
        if (e.getStatusCode().equals(HttpStatus.UNAUTHORIZED)) {
            errorMessage = Message.statusCode(HttpStatus.UNAUTHORIZED)
                    .developerMsg(e.getStatusCode().toString())
                    .message(Messages.INVALID_CREDENTIALS)
                    .exception(e.getClass().getName()).build();
            return new ResponseEntity<>(errorMessage, HttpStatus.UNAUTHORIZED);

        } else if (StringUtils.isNotBlank(e.getResponseBodyAsString())) {
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                FourDotFiveResponse fourDotFiveResponse = objectMapper.readValue(e.getResponseBodyAsString(), FourDotFiveResponse.class);
                errorMessage = Message
                        .statusCode(HttpStatus.INTERNAL_SERVER_ERROR)
                        .developerMsg(fourDotFiveResponse.getMessage())
                        .message("Internal error")
                        .exception(e.getClass().getName()).build();
            } catch (IOException e1) {
                errorMessage = Message
                        .statusCode(HttpStatus.INTERNAL_SERVER_ERROR)
                        .developerMsg(e.getResponseBodyAsString())
                        .message("Internal error")
                        .exception(e.getClass().getName()).build();
            }

            return new ResponseEntity<>(errorMessage, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            errorMessage = Message
                    .statusCode(HttpStatus.INTERNAL_SERVER_ERROR)
                    .developerMsg(
                            e.getStatusCode()
                                    + " occurred while invoking platform API")
                    .message("Internal error")
                    .exception(e.getClass().getName()).build();
            return new ResponseEntity<>(errorMessage, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ExceptionHandler(value = {PlatformException.class})
    @ResponseBody
    public ResponseEntity<Message> error(PlatformException e) {
        Message errorMessage = Message.statusCode(HttpStatus.INTERNAL_SERVER_ERROR)
                .developerMsg(e.getDeveloperMessage()).message(e.getMessage()).build();

        return new ResponseEntity<>(errorMessage, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = {UserException.class})
    @ResponseBody
    public Message error(UserException e) {
        return Message
                .statusCode(HttpStatus.INTERNAL_SERVER_ERROR)
                .developerMsg(e.getMessage()).message(e.getMessage()).build();
    }

    @ExceptionHandler(value = {ForwardRecipientException.class})
    @ResponseBody
    public Message error(ForwardRecipientException e) {
        return Message
                .statusCode(HttpStatus.INTERNAL_SERVER_ERROR)
                .developerMsg(e.getMessage()).message(e.getMessage()).build();
    }

    @ExceptionHandler(value = {SaveUserPlatformException.class})
    @ResponseBody
    public Message error(SaveUserPlatformException e) {
        return Message
                .statusCode(HttpStatus.INTERNAL_SERVER_ERROR)
                .developerMsg(e.getMessage()).message(e.getMessage()).build();
    }

    @ExceptionHandler(value = {DuplicateCompanyException.class})
    @ResponseBody
    public ResponseEntity<Message> error(DuplicateCompanyException e) {
         Message message =  Message.statusCode(HttpStatus.CONFLICT)
                .developerMsg(e.getDeveloperMessage()).message(e.getMessage())
                .build();
         return new ResponseEntity<>(message, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = {SaveUserMCException.class})
    @ResponseBody
    public Message error(SaveUserMCException e) {
        return Message.statusCode(HttpStatus.GONE)
                .developerMsg(e.getDeveloperMessage()).message(e.getMessage())
                .build();
    }

    @ExceptionHandler(value = {UserConflictException.class})
    @ResponseBody
    public Message error(UserConflictException e) {
        return Message.statusCode(HttpStatus.CONFLICT)
                .developerMsg(e.getMessage()).message(e.getMessage()).build();
    }

    @ExceptionHandler(value = {RoleEntityScopeConflictException.class})
    @ResponseBody
    public Message error(RoleEntityScopeConflictException e) {
        return Message.statusCode(HttpStatus.CONFLICT)
                .developerMsg(e.getMessage()).message(e.getMessage()).build();
    }

    @ExceptionHandler(value = {JobNotFoundException.class})
    @ResponseBody
    public Message error(JobNotFoundException e) {
        return Message
                .statusCode(HttpStatus.INTERNAL_SERVER_ERROR)
                .developerMsg(e.getDeveloperMessage()).message(e.getMessage())
                .build();
    }

    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    @ResponseBody
    public Message error(MethodArgumentNotValidException e) {
        BindingResult result = e.getBindingResult();
        String developerMsg = result.getFieldError().getField() +
                result.getFieldError().getDefaultMessage();

        return Message.statusCode(HttpStatus.BAD_REQUEST).message(developerMsg)
                .build();
    }

    @ExceptionHandler(value = {InvalidResetPwdCodeException.class})
    @ResponseBody
    public Message error(InvalidResetPwdCodeException e) {
        return Message.statusCode(HttpStatus.UNAUTHORIZED)
                .developerMsg(e.getDeveloperMessage()).message(e.getMessage())
                .exception(e.getClass().getName()).build();
    }

    @ExceptionHandler(value = {InvalidCredentialsException.class})
    @ResponseBody
    public Message error(InvalidCredentialsException e) {
        return Message.statusCode(HttpStatus.UNAUTHORIZED)
                .developerMsg(e.getDeveloperMessage()).message(e.getMessage())
                .exception(e.getClass().getName()).build();
    }

    @ExceptionHandler(value = {CompanyException.class})
    @ResponseBody
    public Message error(CompanyException e) {
        return Message.statusCode(HttpStatus.INTERNAL_SERVER_ERROR)
                .message(e.getMessage()).exception(e.getClass().getName())
                .build();
    }

    @ExceptionHandler(value = {DuplicateUserException.class})
    @ResponseBody
    public Message error(DuplicateUserException e) {
        return Message.statusCode(HttpStatus.CONFLICT)
                .message(e.getMessage()).exception(e.getClass().getName())
                .build();
    }

    @ExceptionHandler(value = {LicensePrefException.class})
    @ResponseBody
    public Message error(LicensePrefException e) {
        return Message.statusCode(HttpStatus.CONFLICT)
                .message(e.getMessage()).exception(e.getClass().getName())
                .build();
    }

    @ExceptionHandler(value = {ReportTypeException.class})
    @ResponseBody
    public ResponseEntity<?> reportTypeException(ReportTypeException e) {
        Message message = Message.statusCode(HttpStatus.INTERNAL_SERVER_ERROR)
                .developerMsg(e.getDeveloperMessage()).message(e.getMessage())
                .exception(e.getClass().getName()).build();

        return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = {AssignManagerPlatformException.class})
    @ResponseBody
    public Message error(AssignManagerPlatformException e) {
        return Message
                .statusCode(HttpStatus.INTERNAL_SERVER_ERROR)
                .developerMsg(e.getMessage()).message(e.getMessage()).build();
    }

    @ExceptionHandler(value = {IllegalArgumentException.class})
    @ResponseBody
    public Message error(IllegalArgumentException e) {
        LOGGER.error(e.getMessage(), e);
        return Message
                .statusCode(HttpStatus.BAD_REQUEST)
                .developerMsg(e.getMessage())
                .message(e.getMessage()).build();
    }

    @ExceptionHandler(value = {Exception.class})
    @ResponseBody
    public ResponseEntity<Message> error(Exception e) {
        LOGGER.error(e.getMessage(), e);
        Message message = Message
                .statusCode(HttpStatus.INTERNAL_SERVER_ERROR)
                .developerMsg(e.getMessage())
                .message(e.getMessage()).build();
        return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
