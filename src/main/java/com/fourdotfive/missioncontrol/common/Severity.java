package com.fourdotfive.missioncontrol.common;

public enum Severity {
    SUCCESS, INFO, WARNING, DANGER
}