package com.fourdotfive.missioncontrol.common;

import java.util.Set;

public class TokenDetails {

	private String email;
	private String userId;
	private String roleId;
	private String firstName;
	private String lastName;
	private Long version;
	private Set<String> screenIds;

	public Set<String> getScreenIds() {
		return screenIds;
	}

	public void setScreenIds(Set<String> screenIds) {
		this.screenIds = screenIds;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public TokenDetails(String email, String userId, String roleId,
			String firstName, String lastName, Long version) {
		super();
		this.email = email;
		this.userId = userId;
		this.roleId = roleId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.version = version;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public TokenDetails(String email, String userId, String roleId,
						String firstName, Set<String> screenIds) {
		super();
		this.email = email;
		this.userId = userId;
		this.roleId = roleId;
		this.firstName = firstName;
		this.screenIds = screenIds;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TokenDetails [email=").append(email)
				.append(", userId=").append(userId).append(", roleId=")
				.append(roleId).append("]");
		return builder.toString();
	}

}
