package com.fourdotfive.missioncontrol.common;

import java.io.Serializable;
import java.util.Comparator;

import com.fourdotfive.missioncontrol.dtos.user.TeamMemberDto;

public class UserNameComparator implements Comparator<TeamMemberDto>,
		Serializable {

	private static final long serialVersionUID = -9098333092342245136L;

	@Override
	public int compare(TeamMemberDto o1, TeamMemberDto o2) {
		String firstObj = o1.getFirstName().toLowerCase();
		if (o1.getLastName() != null) {
			firstObj = firstObj + "" + o1.getLastName().toLowerCase();
		}

		String secondObj = o2.getFirstName().toLowerCase();
		if (o2.getLastName() != null) {
			secondObj = secondObj + "" + o2.getLastName().toLowerCase();
		}
		return firstObj.compareTo(secondObj);
	}

}
