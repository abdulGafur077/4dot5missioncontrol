package com.fourdotfive.missioncontrol.company;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.fourdotfive.missioncontrol.dtos.candidate.DeleteDTO;
import com.fourdotfive.missioncontrol.dtos.company.*;
import com.fourdotfive.missioncontrol.dtos.pdf.QuestionType;
import com.fourdotfive.missioncontrol.dtos.vendor.*;
import com.fourdotfive.missioncontrol.licensepreference.LicensePreferences;
import com.fourdotfive.missioncontrol.dtos.pdf.AssessmentReportFilterDto;
import com.fourdotfive.missioncontrol.dtos.pdf.CandidateAssessmentReportDetails;
import com.fourdotfive.missioncontrol.pojo.company.*;
import com.fourdotfive.missioncontrol.pojo.user.User;
import org.springframework.web.multipart.MultipartFile;

public interface CompanyApiExecutor {

    Company saveFourDotFiveAdmin(String companyId, String userId);

    User getFourDotFiveAdmin(String companyId);

    Company getCompany(String companyId);

    Company saveCompany(Company company);

    boolean isDuplicateCompanyByName(String name, String id);

    Company addClientOrBU(String parentCompanyId, String childCompanyId);

    List<Company> getClientOrBUs(String companyId);

    void deleteClientOrBU(String parentCompanyId, String childCompanyId);

    void deleteCompany(String parentCompanyId);

    List<Company> getAllCompany(int page, int size, String userId, String sort,
                                String sortDir);

    Company setLicensePreferences(LicensePreferences licensePreferences,
                                  String companyId);

    Company getHostCompany(String name);

    List<User> getRecruitersByCompany(String companyId);

    List<Industry> getIndustries(String companyId);

    List<JobRequisitionRole> getJobRequisitionRoles(String companyId);

    List<String> getJobRequisitionCategories(String companyId);

    List<RoleEntityScope> getBasicEntityScope(String companyId);

    Company createRoleEnityScope(String companyId,
                                 List<RoleEntityScope> roleEntityScopeList);

    Company updateRoleEntityScope(String companyId,
                                  List<RoleEntityScope> roleEntityScopeList);

    Company saveQuestionList(String companyId, List<String> questionList);

    List<String> getQuestionList(String companyId);

    void addSettingsToCompany(String parentCompanyId, String companyId);

    String getCompanyDeleteInfo(String companyId);

    String deleteCompany(DeleteDTO deleteDTO);

    Company changeCompanyState(ChangeCompanyStateDto companyStateDto);

    List<StaffingCompanyDto> getAllStaffingCompanies(String name, int page, int size, String sort, String sortDir);

    CorporateVendorDto saveCorporateVendorRelationship(CorporateVendorDto corporateVendorDto,String loggedInUserId);

    List<CorporateVendorDto> saveCorporateVendors(AssociateVendorsDto associateVendorsDto,String loggedInUserId);

    List<CorporateVendorDto> findAllVendorsOfCorporateCompany(String companyId);

    BuVendorDto getAllBuVendors(String corporateCompanyId, String buId);

    BuVendorDto saveBuVendors(BuVendorDto buVendorDto,String loggedInUserId);

    Map<String, List<Company>> getAllBusOfClient(String companyId);

    CorporateVendorMinDto updateCorporateCompanyContactListInCorporateVendor(CorporateVendorDto corporateVendorDto);

    List<ClientBuMinDto> getClientBuList(String corporateCompanyId, String vendorId, String loggedInUserId);

    List<ContactDto> getCorporateCompanyContactList(String corporateCompanyId, String vendorId);

    CorporateVendorMinDto getCorporateVendorRelationship(String corporateCompanyId, String vendorId);

    void checkForDuplicateHiringManagers(Company company);

    String updateCompany(String companyId, String testLinkName);


    List<CandidateAssessmentReportDetails> getCompanyCandidateReportDetails(AssessmentReportFilterDto assessmentReportFilterDto);

    String deleteCorporateVendorRelationship(DeleteDTO deleteDTO, String userId);

    String deleteBuVendorRelationship(DeleteBuVendorDto deleteBuVendorDto);

    DeleteCountDto getSharedRequisitionAndBuCounts(DeleteBuVendorDto deleteBuVendorDto);

    List<ClientOrBuCountDTO> getClientOrBuDetails(ClientOrBuDetailsDTO clientOrBuDetailsDTO);

    List<CompanyMinDto> searchForCompanies(String name, Integer page, Integer size, String sort, String sortDir);

    String getAssessmentReportQuestionType(String companyId);

    String updateAssessmentReportQuestionType(String companyId, QuestionType questionType);

    String hasAccessToAddCandidateWithOrWithoutResume(String companyId, String clientOrBuId);

    String getSharedClientDetails(String companyId);

    String getVendorsByUserScope(String companyId, String userId);

    String getClientOrBuPointOfContacts(String corporateId, String clientOrBuId);

    CorporateVendorMinDto setCorporateVendorRelationship(String corporateCompanyId, String vendorId);

    String uploadCompanyLogo(String companyId, MultipartFile file, boolean isSmallHeaderLogo) throws IOException;
}
