package com.fourdotfive.missioncontrol.company;

import com.fourdotfive.missioncontrol.common.AccessUtil;
import com.fourdotfive.missioncontrol.common.AppConstants;
import com.fourdotfive.missioncontrol.common.CommonUtil;
import com.fourdotfive.missioncontrol.common.TokenDetails;
import com.fourdotfive.missioncontrol.dtos.candidate.DeleteDTO;
import com.fourdotfive.missioncontrol.dtos.company.*;
import com.fourdotfive.missioncontrol.dtos.pdf.QuestionType;
import com.fourdotfive.missioncontrol.dtos.user.TeamMemberDto;
import com.fourdotfive.missioncontrol.dtos.vendor.*;
import com.fourdotfive.missioncontrol.exception.FileUploadException;
import com.fourdotfive.missioncontrol.exception.PlatformException;
import com.fourdotfive.missioncontrol.dtos.pdf.AssessmentReportFilterDto;
import com.fourdotfive.missioncontrol.dtos.pdf.CandidateAssessmentReportDetails;
import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.company.CompanyState;
import com.fourdotfive.missioncontrol.user.UserService;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shalini, chakravarthy
 */
@RestController
@RequestMapping("/api/company")
public class CompanyController {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(CompanyController.class);

    @Autowired
    private CompanyService companyService;

    @Autowired
    private UserService userService;

    @Autowired
    private AccessUtil accessUtil;

    /**
     * To Get 4dot5 Admins while creating company, this method is called.
     *
     * @return List<TeamMemeberDto>
     */
    @PreAuthorize("@accessControlImp.canGet4Dot5Admins(principal)")
    @RequestMapping(value = "/get4dot5admins", method = RequestMethod.GET)
    public ResponseEntity<List<TeamMemberDto>> get4Dot5Admins(
            HttpServletRequest request) {
        final String authHeader = request.getHeader(AppConstants.AUTHORIZATION);
        TokenDetails tokenDetails = CommonUtil.getTokenDetails(authHeader);
        List<TeamMemberDto> response = companyService.getFourDotFiveAdmins(
                tokenDetails.getUserId(), tokenDetails.getRoleId());

        return new ResponseEntity<List<TeamMemberDto>>(response, HttpStatus.OK);
    }

    /**
     * To Get List of Companies for a superuser or 4dot5Admins, this method is
     * called.
     *
     * @return List<CompanyListDto>
     */
    @PreAuthorize("@accessControlImp.canGetAllCompanies(principal)")
    @RequestMapping(value = "/getallcompanies/{roleId}/{userid}", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyMinInfoDto>> getAllCompanies(
            @PathVariable(value = "roleId") String roleId,
            @PathVariable(value = "userid") String userId,
            @RequestParam(value = "page") int page,
            @RequestParam(value = "size") int size,
            @RequestParam(value = "sort") String sort,
            @RequestParam(value = "sortDir") String sortDir) {

        List<CompanyMinInfoDto> response = new ArrayList<CompanyMinInfoDto>();
        if (accessUtil.isSuperUser(roleId)) {
            response = companyService.getCompanyList(page, size, userId, sort,
                    sortDir);
        } else if (accessUtil.isFourDot5Admin(roleId)) {
            response = companyService.get4Dot5CompanyList(userId);
        }

        return new ResponseEntity<List<CompanyMinInfoDto>>(response,
                HttpStatus.OK);
    }

    /**
     * To Save Company, this method is called.
     *
     * @return CompanyDto
     */
    @PreAuthorize("@accessControlImp.canSaveCompany(principal)")
    @RequestMapping(value = "/savecompany/{companyid}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CompanyDto> saveCompany(
            @PathVariable(value = "companyid") String parentCompanyId,
            @RequestBody CompanyDto companyDto, HttpServletRequest request) {
        CompanyDto.validateFields(companyDto);
        final String authHeader = request.getHeader(AppConstants.AUTHORIZATION);
        TokenDetails tokenDetails = CommonUtil.getTokenDetails(authHeader);

        Boolean isDuplicate = companyService.isDuplicateByName(companyDto.getCompany().getName(), companyDto.getCompany().getId());
        if (BooleanUtils.isTrue(isDuplicate))
            throw new PlatformException("Company with name '" + companyDto.getCompany().getName() + "' already exists.");

        CompanyDto response = companyService.saveCompany(companyDto, tokenDetails, parentCompanyId);

        return new ResponseEntity<CompanyDto>(response, HttpStatus.OK);
    }

    /**
     * To Change Company State, this method is called.
     *
     * @return CompanyDto
     */
    @PreAuthorize("@accessControlImp.canChangecompanystate(principal)")
    @RequestMapping(value = "/changecompanystate/{companyid}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CompanyDto> changecompanystate(
            @PathVariable(value = "companyid") String parentCompanyId,
            @RequestBody CompanyDto companyDto, HttpServletRequest request) {
        final String authHeader = request.getHeader(AppConstants.AUTHORIZATION);
        TokenDetails tokenDetails = CommonUtil.getTokenDetails(authHeader);

        CompanyDto response = companyService.updateCompanyState(companyDto,
                tokenDetails, parentCompanyId);

        if (response == null) {
            response = companyDto;
        }
        return new ResponseEntity<CompanyDto>(response, HttpStatus.OK);
    }

    /**
     * To assign client or bu to a Company, this method is called.
     *
     * @return CompanyDto
     */
    @PreAuthorize("@accessControlImp.canAssignClientOrbu(principal)")
    @RequestMapping(value = "/assignclientorbu/{parentcompanyid}/{childcompanyid}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CompanyDto> assignClientOrbu(
            @PathVariable(value = "parentcompanyid") String parentCompanyId,
            @PathVariable(value = "childcompanyid") String childCompanyId) {

        CompanyDto response = companyService.addClientOrBU(parentCompanyId,
                childCompanyId);

        return new ResponseEntity<CompanyDto>(response, HttpStatus.OK);
    }

    /**
     * To Get Company details, this method is called.
     *
     * @return CompanyDto
     */

    @RequestMapping(value = "/getcompanydetails/{companyid}", method = RequestMethod.GET)
    public ResponseEntity<CompanyDto> getCompanyDetails(
            @PathVariable(value = "companyid") String companyId,
            HttpServletRequest request) {
        final String authHeader = request.getHeader(AppConstants.AUTHORIZATION);
        TokenDetails tokenDetails = CommonUtil.getTokenDetails(authHeader);
        CompanyDto response = companyService.getCompanyDto(companyId,
                tokenDetails.getUserId(), tokenDetails.getRoleId());

        return new ResponseEntity<CompanyDto>(response, HttpStatus.OK);
    }

    /**
     * To Delete child company, this method is called.
     *
     * @return EntityScope
     */
    @PreAuthorize("@accessControlImp.canDeleteClient(principal)")
    @RequestMapping(value = "/deleteclient/{childcompanyid}/{parentcompanyid}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteClient(
            @PathVariable(value = "childcompanyid") String childCompanyId,
            @PathVariable(value = "parentcompanyid") String parentCompanyId,
            HttpServletRequest request) {

        companyService.deleteClientOrBU(parentCompanyId, childCompanyId);

        return new ResponseEntity<String>(HttpStatus.OK);
    }

    /**
     * To Delete company, this method is called.
     *
     * @return EntityScope
     */
    @PreAuthorize("@accessControlImp.canDeleteCompany(principal)")
    @RequestMapping(value = "/deletecompany/{companyid}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteCompany(
            @PathVariable(value = "companyid") String companyId,
            HttpServletRequest request) {

        companyService.deleteCompany(companyId);

        return new ResponseEntity<String>(HttpStatus.OK);
    }

    /**
     * To Get client or bu of a parent company, this method is called.
     *
     * @return CompanyDto
     */
    @RequestMapping(value = "/getclientsorbu/{companyid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CompanyDto>> getClientsOrBu(
            @PathVariable(value = "companyid") String parentCompanyId,
            HttpServletRequest request) {
        List<CompanyDto> response = companyService.getClientOrBuList(parentCompanyId);

        return new ResponseEntity<List<CompanyDto>>(response, HttpStatus.OK);
    }

    /**
     * To Get client or bu of a parent company, this method is called.
     *
     * @return CompanyDto
     */
    @PreAuthorize("@accessControlImp.canGetClientsOrBu(principal)")
    @RequestMapping(value = "/getclientsorbuwithoutglobalsetting/{companyid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CompanyDto>> getClientsOrBUWithoutGlobalSetting(
            @PathVariable(value = "companyid") String parentCompanyId,
            HttpServletRequest request) {
        List<CompanyDto> response = companyService.getClientsOrBUWithoutGlobalSetting(parentCompanyId);

        return new ResponseEntity<List<CompanyDto>>(response, HttpStatus.OK);
    }

    @PreAuthorize("@accessControlImp.canGetClientsOrBu(principal)")
    @RequestMapping(value = "/getactiveclientsorbu/{companyid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CompanyDto>> getactiveclientsorbu(
            @PathVariable(value = "companyid") String parentCompanyId,
            HttpServletRequest request) {
        List<CompanyDto> response = companyService.getActiveClientOrBuDtoList(parentCompanyId);
        return new ResponseEntity<List<CompanyDto>>(response, HttpStatus.OK);
    }

    /**
     * Get the role entity scope for a company
     *
     * @param request
     * @return List<RoleEntityScopeDto>
     */
    @PreAuthorize("@accessControlImp.canGetRoleEntityScope(principal)")
    @RequestMapping(value = "/getroleentityscope/{companyid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RoleEntityScopeDto> getRoleEntityScope(
            @PathVariable(value = "companyid") String companyId,
            HttpServletRequest request) {
        RoleEntityScopeDto dto = companyService.getRoleEntityScope(companyId);

        return new ResponseEntity<RoleEntityScopeDto>(dto, HttpStatus.OK);
    }

    /**
     * Create role entity scope for a company
     *
     * @param companyDto
     * @param request
     * @return
     */
    @PreAuthorize("@accessControlImp.canCreateRoleEntityScope(principal)")
    @RequestMapping(value = "/createroleentityscope/{companyid}", method = RequestMethod.POST)
    public ResponseEntity<RoleEntityScopeDto> createRoleEntityScope(
            @PathVariable(value = "companyid") String companyId,
            @RequestBody RoleEntityScopeDto dto, HttpServletRequest request) {

        RoleEntityScopeDto response = companyService.createRoleEnityScope(
                companyId, dto);

        return new ResponseEntity<RoleEntityScopeDto>(response, HttpStatus.OK);
    }

    /**
     * Update role entity scope for a company
     *
     * @param companyDto
     * @param request
     * @return
     */
    @PreAuthorize("@accessControlImp.canUpdateRoleEntityScope(principal)")
    @RequestMapping(value = "/updateroleentityscope/{companyid}", method = RequestMethod.POST)
    public ResponseEntity<RoleEntityScopeDto> updateRoleEntityScope(
            @PathVariable(value = "companyid") String companyId,
            @RequestBody RoleEntityScopeDto dto, HttpServletRequest request) {

        RoleEntityScopeDto response = companyService.updateRoleEntityScope(
                companyId, dto);

        return new ResponseEntity<RoleEntityScopeDto>(response, HttpStatus.OK);
    }

    /**
     * To get Industry Master data list for parent company, This method is
     * called
     *
     * @return List<String>
     */
    @PreAuthorize("@accessControlImp.canGetParentCompanyIndustryList(principal)")
    @RequestMapping(value = "/getindustrylist/{parentcompanyid}", method = RequestMethod.GET)
    public ResponseEntity<List<MasterIndustryDto>> getParentCompanyIndustryList(
            @PathVariable(value = "parentcompanyid") String parentCompanyId) {

        List<MasterIndustryDto> industryMasterList = companyService
                .getParentIndustryList(parentCompanyId);

        return new ResponseEntity<List<MasterIndustryDto>>(industryMasterList,
                HttpStatus.OK);
    }

    /**
     * To get Industry Master data list for client company, This method is
     * called
     *
     * @return List<String>
     */
    @PreAuthorize("@accessControlImp.canGetChildCompanyIndustryList(principal)")
    @RequestMapping(value = "/getindustrylist/{parentcompanyid}/{childcompanyid}", method = RequestMethod.GET)
    public ResponseEntity<List<MasterIndustryDto>> getChildCompanyIndustryList(
            @PathVariable(value = "parentcompanyid") String parentCompanyId,
            @PathVariable(value = "childcompanyid") String childCompanyId) {

        List<MasterIndustryDto> industryMasterList = companyService
                .getChildIndustryList(parentCompanyId, childCompanyId);

        return new ResponseEntity<List<MasterIndustryDto>>(industryMasterList,
                HttpStatus.OK);
    }

    /**
     * To Save questionList, this method is called.
     *
     * @return CompanyDto
     */
    @PreAuthorize("@accessControlImp.canSaveQuestionList(principal)")
    @RequestMapping(value = "/savequestionlist/{companyid}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Company> saveQuestionList(
            @PathVariable(value = "companyid") String parentCompanyId,
            @RequestBody List<String> questionList, HttpServletRequest request) {

        Company response = companyService.saveQuestionList(parentCompanyId, questionList);

        return new ResponseEntity<Company>(response, HttpStatus.OK);
    }

    /**
     * To get Question ist for  company, This method is
     * called
     *
     * @return List<String>
     */
    @PreAuthorize("@accessControlImp.canGetQuestionList(principal)")
    @RequestMapping(value = "/getquestionlist/{companyid}", method = RequestMethod.GET)
    public ResponseEntity<List<String>> getQuestionList(
            @PathVariable(value = "companyid") String companyId) {


        List<String> questionList = companyService
                .getQuestionList(companyId);

        return new ResponseEntity<List<String>>(questionList,
                HttpStatus.OK);
    }

    /**
     * To Get Four Dot Five Company details, this method is called.
     *
     * @return Company
     */
    @PreAuthorize("@accessControlImp.canGetFourDotFiveCompanyDetails(principal)")
    @RequestMapping(value = "/gethostcompanydetails", method = RequestMethod.GET)
    public ResponseEntity<Company> getCompanyDetails(
            HttpServletRequest request) {
        Company response = companyService.getHostCompany();
        return new ResponseEntity<Company>(response, HttpStatus.OK);
    }

    @RequestMapping(value = {"/companyorclientorbudeleteinfos/{companyId}"}, method = RequestMethod.GET)
    public String getCompanyDeleteInfo(@PathVariable(value = "companyId") String companyId) {
        if (StringUtils.isEmpty(companyId)) {
            throw new PlatformException("Id can not be null.");
        }
        return companyService.getCompanyDeleteInfo(companyId);
    }

    @RequestMapping(value = {"/deletecompanyorclientorbu"}, method = RequestMethod.DELETE)
    public String deleteCompany(@RequestBody DeleteDTO deleteDTO) {
        if (deleteDTO == null) {
            throw new PlatformException("Can not delete company.");
        }
        if (StringUtils.isEmpty(deleteDTO.getId())) {
            throw new PlatformException("Company id can not be null.");
        }
        return companyService.deleteCompany(deleteDTO);
    }

    @RequestMapping(value = "changeCompanyState", method = {RequestMethod.POST})
    public ResponseEntity<Company> changeCompanyState(@RequestBody ChangeCompanyStateDto companyStateDto) throws Exception {
        if (companyStateDto == null || StringUtils.isEmpty(companyStateDto.getCompanyId())) {
            throw new PlatformException("Company id cannot be null.");
        }

        if (companyStateDto.getCompanyState() == null || !EnumUtils.isValidEnum(CompanyState.class, companyStateDto.getCompanyState().toString())) {
            throw new PlatformException("Company State is not valid.");
        }

        Company response = companyService.changeCompanyState(companyStateDto);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/getallstaffingcompanies", method = RequestMethod.GET)
    public ResponseEntity<List<StaffingCompanyDto>> getAllStaffingCompanies(
            @RequestParam(value = "name", required = false, defaultValue = "") String name,
            @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
            @RequestParam(value = "size", required = false, defaultValue = "5") Integer size,
            @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "sortDir", required = false) String sortDir) {

        List<StaffingCompanyDto> response = companyService.getAllStaffingCompanies(name, page, size, sort, sortDir);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "saveCorporateVendor", method = {RequestMethod.POST})
    public ResponseEntity<CorporateVendorDto> saveCorporateVendorRelationship(@RequestBody CorporateVendorDto corporateVendorDto) throws Exception

    {
        if (corporateVendorDto == null || StringUtils.isEmpty(corporateVendorDto.getCorporateCompanyId())) {
            throw new PlatformException("Corporate Company id cannot be null.");
        }

        if (StringUtils.isEmpty(corporateVendorDto.getVendorId())) {
            throw new PlatformException("Vendor id cannot be null.");
        }

        CorporateVendorDto response = companyService.saveCorporateVendorRelationship(corporateVendorDto, userService.getCurrentLoggedInUser().getId());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = {"/deletecorporatevendorrelationship"}, method = RequestMethod.DELETE)
    public String deleteCorporateVendorRelationship(@RequestBody DeleteDTO deleteDTO) {
        if (deleteDTO == null) {
            throw new PlatformException("Can not delete corporate vendor association.");
        }
        if (StringUtils.isEmpty(deleteDTO.getId())) {
            throw new PlatformException("Corporate Vendor Association Id can not be null.");
        }
        return companyService.deleteCorporateVendorRelationship(deleteDTO);
    }

    @RequestMapping(value = "saveCorporateVendors", method = {RequestMethod.POST})
    public ResponseEntity<List<CorporateVendorDto>> saveCorporateVendors(@RequestBody AssociateVendorsDto associateVendorsDto) throws Exception {
        if (associateVendorsDto == null || StringUtils.isEmpty(associateVendorsDto.getCorporateCompanyId())) {
            throw new PlatformException("Corporate Company id cannot be null.");
        }

        if (associateVendorsDto.getVendorsList() == null || associateVendorsDto.getVendorsList().isEmpty()) {
            throw new PlatformException("Vendor list cannot be null or empty.");
        }

        List<CorporateVendorDto> response = companyService.saveCorporateVendors(associateVendorsDto, userService.getCurrentLoggedInUser().getId());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = {"/findallcorporatevendors/{companyId}"}, method = RequestMethod.GET)
    public ResponseEntity<List<CorporateVendorDto>> findAllVendorsOfCorporateCompany(@PathVariable(value = "companyId") String companyId) {
        if (StringUtils.isEmpty(companyId)) {
            throw new PlatformException("Id cannot be null.");
        }
        List<CorporateVendorDto> response = companyService.findAllVendorsOfCorporateCompany(companyId);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = {"/getallbuvendors/{corporateCompanyId}/{buId}"}, method = RequestMethod.GET)
    public ResponseEntity<BuVendorDto> getAllBuVendors(@PathVariable String corporateCompanyId, @PathVariable String buId) {
        if (StringUtils.isEmpty(corporateCompanyId)) {
            throw new PlatformException("Company Id cannot be null.");
        }
        if (StringUtils.isEmpty(buId)) {
            throw new PlatformException("bu Id cannot be null.");
        }
        BuVendorDto response = companyService.getAllBuVendors(corporateCompanyId, buId);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "savebuvendors", method = {RequestMethod.POST})
    public @ResponseBody
    ResponseEntity<BuVendorDto> saveBuVendors(@RequestBody BuVendorDto buVendorDto) throws Exception {
        if (buVendorDto == null || StringUtils.isEmpty(buVendorDto.getCorporateCompanyId())) {
            throw new PlatformException("Corporate Company id cannot be null.");
        }
        if (StringUtils.isEmpty(buVendorDto.getBuId())) {
            throw new PlatformException("BU id cannot be null.");
        }

        BuVendorDto response = companyService.saveBuVendors(buVendorDto, userService.getCurrentLoggedInUser().getId());

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = {"/deletebuvendorrelationship"}, method = RequestMethod.DELETE)
    public String deleteBuVendorRelationship(@RequestBody DeleteBuVendorDto deleteBuVendorDto) {
        if (deleteBuVendorDto == null) {
            throw new PlatformException("Can not delete bu vendor association.");
        }
        return companyService.deleteBuVendorRelationship(deleteBuVendorDto);
    }

    @RequestMapping(value = "updatecorporatecompanycontactlist", method = {RequestMethod.POST})
    public ResponseEntity<CorporateVendorMinDto> updateCorporateCompanyContactListInCorporateVendor(@RequestBody CorporateVendorDto corporateVendorDto) throws Exception {
        if (corporateVendorDto == null || StringUtils.isEmpty(corporateVendorDto.getCorporateCompanyId())) {
            throw new PlatformException("Corporate Company id cannot be null.");
        }

        if (StringUtils.isEmpty(corporateVendorDto.getVendorId())) {
            throw new PlatformException("Vendor id cannot be null.");
        }

        if (corporateVendorDto.getCorporateCompanyContactList() == null || corporateVendorDto.getCorporateCompanyContactList().isEmpty()) {
            throw new PlatformException("Contact list cannot be null.");
        }

        CorporateVendorMinDto response = companyService.updateCorporateCompanyContactListInCorporateVendor(corporateVendorDto);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = {"/getclientbulist"}, method = RequestMethod.GET)
    public ResponseEntity<List<ClientBuMinDto>> getClientBuList(@RequestParam(value = "vendorId") String vendorId
            , @RequestParam(value = "corporateCompanyId") String corporateCompanyId
            , @RequestParam(value = "userId") String loggedInUserId) {
        if (StringUtils.isEmpty(corporateCompanyId)) {
            throw new PlatformException("Corporate Company Id cannot be null.");
        }
        if (StringUtils.isEmpty(vendorId)) {
            throw new PlatformException("Vendor Id cannot be null.");
        }
        if (StringUtils.isEmpty(loggedInUserId)) {
            throw new PlatformException("user Id cannot be null.");
        }
        List<ClientBuMinDto> response = companyService.getClientBuList(corporateCompanyId, vendorId, loggedInUserId);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = {"/getcorporatecompanycontactlist"}, method = RequestMethod.GET)
    public ResponseEntity<List<ContactDto>> getCorporateCompanyContactList(@RequestParam(value = "vendorId") String vendorId
            , @RequestParam(value = "corporateCompanyId") String corporateCompanyId) {
        if (StringUtils.isEmpty(corporateCompanyId)) {
            throw new PlatformException("Corporate Company Id cannot be null.");
        }
        if (StringUtils.isEmpty(vendorId)) {
            throw new PlatformException("Vendor Id cannot be null.");
        }
        List<ContactDto> response = companyService.getCorporateCompanyContactList(corporateCompanyId, vendorId);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PreAuthorize("@accessControlImp.canViewReports(principal)")
    @RequestMapping(value = "getallcandidatereportdetails", method = {RequestMethod.POST})
    public @ResponseBody
    ResponseEntity<List<CandidateAssessmentReportDetails>> getAllCandidateReportDetails(@RequestBody AssessmentReportFilterDto assessmentReportFilterDto) throws Exception {
//        if (assessmentReportFilterDto == null || StringUtils.isEmpty(assessmentReportFilterDto.getCompanyId())) {
//            throw new PlatformException("Company id cannot be null.");
//        }

        List<CandidateAssessmentReportDetails> response = companyService.getCompanyCandidateReportDetails(assessmentReportFilterDto);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = {"/getsharedrequisitionandbucounts"}, method = RequestMethod.POST)
    public DeleteCountDto getSharedRequisitionAndBuCounts(@RequestBody DeleteBuVendorDto deleteBuVendorDto) {
        if (deleteBuVendorDto == null) {
            throw new PlatformException("Company Ids cannot be null");
        }
        return companyService.getSharedRequisitionAndBuCounts(deleteBuVendorDto);
    }

    @RequestMapping(value = {"/getclientorbudetails"}, method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<List<ClientOrBuCountDTO>> getClientOrBuDetails(@RequestBody ClientOrBuDetailsRequestDTO clientOrBuDetailsRequestDTO) {
        List<ClientOrBuCountDTO> clientOrBuCountDTOS = companyService.getClientOrBuDetails(clientOrBuDetailsRequestDTO);
        return new ResponseEntity<>(clientOrBuCountDTOS, HttpStatus.OK);
    }

    @PreAuthorize("@accessControlImp.canViewReports(principal)")
    @RequestMapping(value = "/searchforcompanies", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyMinDto>> searchForCompanies(
            @RequestParam(value = "name", required = false, defaultValue = "") String name,
            @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
            @RequestParam(value = "size", required = false, defaultValue = "5") Integer size,
            @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "sortDir", required = false) String sortDir) {

        List<CompanyMinDto> response = companyService.searchForCompanies(name, page, size, sort, sortDir);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PreAuthorize("@accessControlImp.canViewReports(principal)")
    @RequestMapping(value = {"/getassessmentreportquestiontype/{companyId}"}, method = RequestMethod.GET)
    public ResponseEntity<String> getAssessmentReportQuestionType(@PathVariable(value = "companyId") String companyId) {
        if (StringUtils.isEmpty(companyId)) {
            throw new PlatformException("Company Id cannot be null.");
        }

        String response = companyService.getAssessmentReportQuestionType(companyId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PreAuthorize("@accessControlImp.canViewReports(principal)")
    @RequestMapping(value = {"/updateassessmentreportquestiontype/{companyId}"}, method = RequestMethod.GET)
    public ResponseEntity<String> updateAssessmentReportQuestionType(@PathVariable(value = "companyId") String companyId,
                                                                     @RequestParam(value = "questionType") QuestionType questionType) {
        if (StringUtils.isEmpty(companyId)) {
            throw new PlatformException("Company Id cannot be null.");
        }
        if (questionType == null) {
            throw new PlatformException("Question Type cannot be null.");
        }
        String response = companyService.updateAssessmentReportQuestionType(companyId, questionType);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = {"/hasaccesstoaddcandidatewithorwithoutresume/{companyId}"}, method = RequestMethod.GET)
    public ResponseEntity<String> hasAccessToAddCandidateWithOrWithoutResume(@PathVariable(value = "companyId") String companyId,
                                                                             @RequestParam(value = "clientOrBuId", required = false, defaultValue = "") String clientOrBuId) {
        if (StringUtils.isEmpty(companyId)) {
            throw new PlatformException("Company Id cannot be null.");
        }

        String response = companyService.hasAccessToAddCandidateWithOrWithoutResume(companyId, clientOrBuId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = {"/getsharedclientdetails/{companyId}"}, method = RequestMethod.GET)
    public ResponseEntity<String> getSharedClientDetails(@PathVariable(value = "companyId") String companyId) {
        String sharedClientDetails = companyService.getSharedClientDetails(companyId);
        return new ResponseEntity<>(sharedClientDetails, HttpStatus.OK);
    }

    @RequestMapping(value = {"/getvendorsbyuserscope/{companyId}"}, method = RequestMethod.GET)
    public ResponseEntity<String> getVendorsByUserScope(@PathVariable(value = "companyId") String companyId) {
        String sharedClientDetails = companyService.getVendorsByUserScope(companyId);
        return new ResponseEntity<>(sharedClientDetails, HttpStatus.OK);
    }

    @RequestMapping(value = "/getclientorbupointofcontacts", method = {RequestMethod.GET})
    public @ResponseBody
    String getClientOrBuPointOfContacts(@RequestParam(value = "corporateId", required = false, defaultValue = "") String corporateId,
                                        @RequestParam(value = "clientOrBuId", required = false, defaultValue = "") String clientOrBuId) {

        return companyService.getClientOrBuPointOfContacts(corporateId, clientOrBuId);
    }

    @RequestMapping(value = "/getsharedclientbus/{companyId}/{sharedClientBuOrCorpId}", method = {RequestMethod.GET})
    public ResponseEntity<List<ClientOrBuPointOfContactDTO>> getSharedClientBus(@PathVariable(value = "companyId") String companyId,
                                                                                @PathVariable(value = "sharedClientBuOrCorpId") String sharedClientBuOrCorpId) {
        List<ClientOrBuPointOfContactDTO> clientOrBuPointOfContactDTOS = companyService.getSharedClientBus(companyId, sharedClientBuOrCorpId);
        return new ResponseEntity<>(clientOrBuPointOfContactDTOS, HttpStatus.OK);
    }

    @RequestMapping(value = "/uploadcompanylogo/{companyId}", method = {RequestMethod.POST})
    public ResponseEntity<String> uploadCompanyLogo(@PathVariable(value = "companyId") String companyId,
                                                    @RequestParam("file") MultipartFile file,
                                                    @RequestParam(value = "isSmallHeaderLogo", required = false, defaultValue = "false") boolean isSmallHeaderLogo)
            throws FileUploadException, IOException {
        String s3Url = null;
        boolean flag = CommonUtil.checkFileWhileUploadImage(file);
        if (flag) {
            s3Url = companyService.uploadCompanyLogo(companyId, file, isSmallHeaderLogo);
            if (s3Url != null)
                return new ResponseEntity<>(s3Url, HttpStatus.OK);
            else return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
