package com.fourdotfive.missioncontrol.company;

public class CompanyException extends RuntimeException{

	private static final long serialVersionUID = -9055074168069875313L;
	
	private String message;
	

	public String getMessage() {
		return message;
	}

	public CompanyException() {
	}

	public CompanyException(String message, Object... args) {
		this.message = String.format(message, args); 
	}
	
	@Override
	public String toString() {
		return "CompanyException [message=" + message + "]";
	}

}
