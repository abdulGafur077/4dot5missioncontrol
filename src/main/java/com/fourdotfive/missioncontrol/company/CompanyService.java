package com.fourdotfive.missioncontrol.company;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.fourdotfive.missioncontrol.common.TokenDetails;
import com.fourdotfive.missioncontrol.dtos.candidate.DeleteDTO;
import com.fourdotfive.missioncontrol.dtos.company.*;
import com.fourdotfive.missioncontrol.dtos.pdf.QuestionType;
import com.fourdotfive.missioncontrol.dtos.user.TeamMemberDto;
import com.fourdotfive.missioncontrol.dtos.vendor.*;
import com.fourdotfive.missioncontrol.dtos.pdf.AssessmentReportFilterDto;
import com.fourdotfive.missioncontrol.dtos.pdf.CandidateAssessmentReportDetails;
import com.fourdotfive.missioncontrol.pojo.company.*;
import com.fourdotfive.missioncontrol.pojo.user.User;
import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * @author chakravarthy,shalini
 *
 */
public interface CompanyService {

	/**
	 * To Create and Update Company, this method is called.
	 * 
	 * @return response from platform
	 */
	CompanyDto saveCompany(CompanyDto company, TokenDetails tokenDetails,
			String companyId);

	boolean isDuplicateByName(String name, String id);

	/**
	 * To Update Company state, this method is called.
	 * 
	 * @return response from platform
	 */
	CompanyDto updateCompanyState(CompanyDto company,
			TokenDetails tokenDetails, String companyId);

	/**
	 * To Get Company Details, this method is called.
	 * 
	 * @return response from platform
	 */
	CompanyDto getCompanyDto(String companyId, String userId, String roleId);

	/**
	 * To Assign client or bu to its parent Company, this method is called.
	 * 
	 * @return response from platform
	 */
	CompanyDto addClientOrBU(String parentCompanyId, String childCompanyId);

	/**
	 * To Delete client or bu by its parent Company, this method is called.
	 * 
	 */
	void deleteClientOrBU(String parentCompanyId, String childCompanyId);

	/**
	 * To Delete parent Company, this method is called.
	 * 
	 */
	void deleteCompany(String parentCompanyId);

	/**
	 * To Get client or bu Companies for a parent company, this method is
	 * called.
	 * 
	 * @return response from platform
	 */
	List<CompanyDto> getClientOrBuList(String companyId);

	/**
	 * To Get active client or bu Companies for a parent company, this method is
	 * called.
	 * 
	 * @return response from platform
	 */
	List<CompanyDto> getActiveClientOrBuDtoList(String companyId);

	/**
	 * To Get Companies for the super user, this method is called.
	 * 
	 * @return response from platform
	 */
	List<CompanyMinInfoDto> getCompanyList(int page, int size, String userId,
			String sort, String sortDir);

	/********** start of role entity scope definition **********/
	/**
	 * Get the role entity scope of a company
	 * 
	 * @param companyId
	 * @return RoleEntityScopeDto
	 */
	RoleEntityScopeDto getRoleEntityScope(String companyId);

	/**
	 * Create the role entity Scope of a company.
	 * 
	 * @param companyId
	 * @param companyScopeList
	 */
	RoleEntityScopeDto createRoleEnityScope(String companyId,
			RoleEntityScopeDto dto);

	/**
	 * Update the role entity Scope of a company.
	 * 
	 * @param companyId
	 * @param companyScopeList
	 */
	RoleEntityScopeDto updateRoleEntityScope(String companyId,
			RoleEntityScopeDto dto);

	/**
	 * To get 4Dot5Company based on userID, this method is called.
	 * 
	 * @return UserDto
	 */
	List<CompanyMinInfoDto> get4Dot5CompanyList(String userId);

	Company getCompany(String companyId);

	/**
	 * To Get 4Dot5 Admins who are not assigned, this method is called.
	 * 
	 * @return List<TeamMemeberDto>
	 */
	List<TeamMemberDto> getFourDotFiveAdmins(String userId, String roleid);

	/**
	 * To Get Industry list, this method is called.
	 * 
	 * @return List<MasterIndustryDto>
	 */
	List<MasterIndustryDto> getParentIndustryList(String parentCompanyId);

	/**
	 * To Get Industry list, this method is called.
	 * 
	 * @return List<MasterIndustryDto>
	 */
	List<MasterIndustryDto> getChildIndustryList(String parentCompanyId,
			String childCompanyId);

	/**
	 * To Get Client or BU list, this method is called.
	 * 
	 * @return List<Company>
	 */
	List<Company> getClientOrBUs(String companyId);

	/**
	 * To Get Industry Sub list, this method is called.
	 * 
	 * @return List<Industry>
	 */
	List<Industry> getIndustries(String companyId);

	/**
	 * To Get JobRequisitionRole, this method is called.
	 * 
	 * @return List<JobRequisitionRole>
	 */
	List<JobRequisitionRole> getJobRequisitionRoles(String companyId);

	/**
	 * To Get JobRequisition Categories, this method is called.
	 * 
	 * @return List<String>
	 */
	List<String> getJobRequisitionCategories(String companyId);

	/**
	 * To Get Recruiters with in the company, this method is called.
	 *
	 * @return List<String>
	 */
	List<User> getRecruiters(String companyId);

	List<RoleEntityScope> getBasicEntityScope(String companyId);

	CompanyDto composeCompanyDto(Company company, String roleId);

	Company getHostCompany();

	List<CompanyDto> getClientsOrBUWithoutGlobalSetting(String companyId);

	List<Company> getActiveClientOrBu(String companyId);

	Company saveQuestionList(String companyId, List<String> questionList);

	List<String> getQuestionList(String companyId);

	String getCompanyDeleteInfo(String companyId);

	String deleteCompany(DeleteDTO deleteDTO);

    Company changeCompanyState(ChangeCompanyStateDto companyStateDto);

	List<StaffingCompanyDto> getAllStaffingCompanies(String name, int page, int size, String sort, String sortDir);

    CorporateVendorDto saveCorporateVendorRelationship(CorporateVendorDto corporateVendorDto,String loggedInUserId);

    List<CorporateVendorDto> saveCorporateVendors(AssociateVendorsDto associateVendorsDto,String loggedInUserId);

	List<CorporateVendorDto> findAllVendorsOfCorporateCompany(String companyId);

	BuVendorDto getAllBuVendors(String corporateCompanyId, String buId);

	BuVendorDto saveBuVendors(BuVendorDto buVendorDto,String loggedInUserId);

	Map<String,List<Company>> getAllBusOfClient(String companyId);

	CorporateVendorMinDto updateCorporateCompanyContactListInCorporateVendor(CorporateVendorDto corporateVendorDto);

	List<ClientBuMinDto> getClientBuList(String corporateCompanyId, String vendorId, String loggedInUserId);

	List<ContactDto> getCorporateCompanyContactList(String corporateCompanyId, String vendorId);

    CorporateVendorMinDto getCorporateVendorRelationship(String corporateCompanyId, String vendorId);

    String updateCompany(String companyId, String testLinkName);

	List<CandidateAssessmentReportDetails> getCompanyCandidateReportDetails(AssessmentReportFilterDto assessmentReportFilterDto);

    String deleteCorporateVendorRelationship(DeleteDTO deleteDTO);

    String deleteBuVendorRelationship(DeleteBuVendorDto deleteBuVendorDto);

	DeleteCountDto getSharedRequisitionAndBuCounts(DeleteBuVendorDto deleteBuVendorDto);

	List<ClientOrBuCountDTO> getClientOrBuDetails(ClientOrBuDetailsRequestDTO clientOrBuDetailsRequestDTO);

    List<CompanyMinDto> searchForCompanies(String name, Integer page, Integer size, String sort, String sortDir);

	String getAssessmentReportQuestionType(String companyId);

	String updateAssessmentReportQuestionType(String companyId, QuestionType questionType);

	String hasAccessToAddCandidateWithOrWithoutResume(String companyId, String clientOrBuId);

	String getSharedClientDetails(String companyId);

	String getVendorsByUserScope(String companyId);

	String getClientOrBuPointOfContacts(String corporateId, String clientOrBuId);

	List<ClientOrBuPointOfContactDTO> getSharedClientBus(String companyId, String sharedClientBuOrCorpId);

	CorporateVendorMinDto setCorporateVendorRelationship(String corporateCompanyId, String vendorId);

    String uploadCompanyLogo(String companyId, MultipartFile file, boolean isSmallHeaderLogo) throws IOException;
}
