package com.fourdotfive.missioncontrol.company;

import com.fourdotfive.missioncontrol.common.*;
import com.fourdotfive.missioncontrol.dtos.candidate.DeleteDTO;
import com.fourdotfive.missioncontrol.dtos.company.*;
import com.fourdotfive.missioncontrol.dtos.pdf.QuestionType;
import com.fourdotfive.missioncontrol.dtos.user.TeamMemberDto;
import com.fourdotfive.missioncontrol.dtos.vendor.*;
import com.fourdotfive.missioncontrol.exception.DuplicateCompanyException;
import com.fourdotfive.missioncontrol.exception.PlatformException;
import com.fourdotfive.missioncontrol.exception.RoleEntityScopeConflictException;
import com.fourdotfive.missioncontrol.industry.IndustryService;
import com.fourdotfive.missioncontrol.licensepreference.LicensePrefService;
import com.fourdotfive.missioncontrol.licensepreference.LicensePrefToLicensePrefDto;
import com.fourdotfive.missioncontrol.dtos.pdf.AssessmentReportFilterDto;
import com.fourdotfive.missioncontrol.dtos.pdf.CandidateAssessmentReportDetails;
import com.fourdotfive.missioncontrol.pojo.company.*;
import com.fourdotfive.missioncontrol.pojo.user.AccessControlList;
import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.roleentityscope.RoleEntityScopeService;
import com.fourdotfive.missioncontrol.setting.SettingService;
import com.fourdotfive.missioncontrol.user.UserApiExecutor;
import com.fourdotfive.missioncontrol.user.UserService;
import com.fourdotfive.missioncontrol.userentityscope.FetchUserEntityScopeService;
import com.fourdotfive.missioncontrol.userentityscope.UserScopeService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.fourdotfive.missioncontrol.pojo.user.User.userNameComparator;

@Service
public class CompanyServiceImp implements CompanyService {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(CompanyServiceImp.class);

    @Autowired
    private UserService userService;

    @Autowired
    private UserApiExecutor userApiExecutor;

    @Autowired
    private AccessUtil accessUtil;

    @Autowired
    private LicensePrefToLicensePrefDto toLicensePrefDto;

    @Autowired
    private SettingService settingService;

    @Autowired
    private LicensePrefService licensePrefService;

    @Autowired
    private CompanyApiExecutor apiExecutor;

    @Autowired
    private IndustryService industryService;

    @Autowired
    private RoleEntityScopeService entityScopeService;

    @Autowired
    private UserScopeService userScopeService;

    @Autowired
    private CreateUpdateCompanyService createUpdateCompanyService;

    @Autowired
    private FetchUserEntityScopeService fetchUserScopeService;

    @Override
    public List<TeamMemberDto> getFourDotFiveAdmins(String userId, String roleId) {

        String id = userId;
        if (roleId.equals(AppConstants.FOURDOT5ADMIN_ROLEID)) {
            id = userService.getManager(userId).getId();
        }

        return CommonUtil.mapStubUserToTeamMembers(filterFourDotFiveAdmins(id));
    }

    /**
     * To filter only to get 4Dot5Admins
     *
     * @param userId
     * @return
     */
    private List<User> filterFourDotFiveAdmins(String userId) {
        List<User> fourDotFiveAdmins = new ArrayList<>();
        List<User> users = userService.getReporteeList(userId);
        for (User u : users) {
            if (accessUtil.isFourDot5Admin(u.getRole())) {
                fourDotFiveAdmins.add(u);
            }
        }
        if (fourDotFiveAdmins.size() > 1) {
            Collections.sort(fourDotFiveAdmins, userNameComparator);
        }
        return fourDotFiveAdmins;
    }

    @Override
    public CompanyDto saveCompany(CompanyDto companyDto, TokenDetails tokenDetails, String parentCompanyId) {
        try {
            return createUpdateCompanyService.saveCompany(companyDto,
                    tokenDetails, parentCompanyId);
        } catch (DuplicateCompanyException e) {
            LOGGER.error(Messages.EXCEPTION_WHILE_CREATING_COMPANY + " {}", e);
            throw new DuplicateCompanyException(e.getMessage(), e.getMessage());
        } catch (PlatformException e) {
            LOGGER.error(Messages.EXCEPTION_WHILE_CREATING_COMPANY + " {}", e);
            throw new PlatformException(e.getMessage());
        } catch (CompanyException e) {
            LOGGER.error(Messages.EXCEPTION_WHILE_CREATING_COMPANY + " {}", e);
            throw new CompanyException(Messages.DUPLICATE_COMPANY, companyDto
                    .getCompany().getName());
        } catch (Exception e) {
            LOGGER.error(Messages.EXCEPTION_WHILE_CREATING_COMPANY + " {}", e);
            throw new CompanyException(
                    Messages.EXCEPTION_WHILE_CREATING_COMPANY);
        }

    }

    @Override
    public boolean isDuplicateByName(String name, String id) {
        return createUpdateCompanyService.isDuplicateByName(name, id);
    }

    @Override
    public CompanyDto composeCompanyDto(Company company, String roleId) {

        CompanyDto companyDto = CompanyDto.mapCompanytoCompanyDto(company);
        if (accessUtil.is4dot5AdminAvailable(company.getCompanyType())
                && accessUtil.is4dot5AdminViewable(roleId)) {
            User fourdot5admin = apiExecutor.getFourDotFiveAdmin(company
                    .getId());

            if (fourdot5admin != null) {
                TeamMemberDto fourdot5adminDto = TeamMemberDto
                        .mapUserToTeamMember(fourdot5admin);
                companyDto.setFourDotFiveAdmin(fourdot5adminDto);
            }
        }

        // If the company is a host company enable the license preferences
        if (company.getLicensePreferences() != null) {
            companyDto.setLicensePreferences(toLicensePrefDto.convert(company
                    .getLicensePreferences()));
        }
        // If the company is a host company enable the license preferences
        if (accessUtil.isHostCompany(company)) {
            licensePrefService.enableLicensePref(companyDto
                    .getLicensePreferences());
        }

        return companyDto;
    }

    @Override
    public CompanyDto addClientOrBU(String parentCompanyId,
                                    String childCompanyId) {

        Company company = apiExecutor.addClientOrBU(parentCompanyId,
                childCompanyId);
        return CompanyDto.mapCompanytoCompanyDto(company);
    }

    @Override
    public void deleteClientOrBU(String parentCompanyId, String childCompanyId) {
        Company company = getCompany(childCompanyId);
        try {
            apiExecutor.deleteClientOrBU(parentCompanyId, childCompanyId);
            deleteCompany(childCompanyId);
        } catch (Exception e) {
            if (company.getCompanyType().equals(CompanyType.BusinessUnit)) {
                throw new PlatformException(
                        Messages.EXCEPTION_WHILE_DELETING_BU);
            } else {
                throw new PlatformException(
                        Messages.EXCEPTION_WHILE_DELETING_CLIENT);
            }

        }

        userScopeService.deleteClientFromClientList(parentCompanyId,
                childCompanyId);
    }

    @Override
    public void deleteCompany(String companyId) {
        try {
            apiExecutor.deleteCompany(companyId);
        } catch (Exception e) {
            throw new PlatformException(
                    Messages.EXCEPTION_WHILE_DELETING_COMPANY);

        }
    }

    @Override
    public List<CompanyDto> getClientOrBuList(String companyId) {


        List<CompanyDto> companyDtos = new ArrayList<CompanyDto>();
        for (Company company : getClientOrBUsBasedOnUserScopeOfLoggedInUser(companyId)) {
            if (company != null && company.getCompanyState() != null
                    && !company.getCompanyState().equals(AppConstants.ARCHIVED)) {
                CompanyDto companyDto = CompanyDto
                        .mapCompanytoCompanyDto(company);
                companyDtos.add(companyDto);
            }
        }
        return companyDtos;
    }

    @Override
    public List<CompanyDto> getActiveClientOrBuDtoList(String companyId) {

        List<CompanyDto> companyDtos = new ArrayList<CompanyDto>();
        List<Company> companyList = getClientOrBUs(companyId);
        Collections.sort(companyList, new CompanyNameComparator());
        for (Company company : companyList) {
            if (company != null && company.getCompanyState() != null
                    && company.getCompanyState().equals(AppConstants.ACTIVE)) {
                CompanyDto companyDto = CompanyDto
                        .mapCompanytoCompanyDto(company);
                companyDtos.add(companyDto);
            }
        }
        return companyDtos;
    }

    @Override
    public List<Company> getActiveClientOrBu(String companyId) {

        List<Company> companyList = new ArrayList<Company>();
        for (Company company : getClientOrBUs(companyId)) {
            if (company != null && company.getCompanyState() != null
                    && company.getCompanyState().equals(AppConstants.ACTIVE)) {
                companyList.add(company);
            }
        }
        return companyList;
    }

    @Override
    public List<CompanyDto> getClientsOrBUWithoutGlobalSetting(String companyId) {

        List<CompanyDto> list = getActiveClientOrBuDtoList(companyId);
        List<CompanyDto> clients = new ArrayList<CompanyDto>();

        for (CompanyDto client : list) {
            String clientId = client.getCompany().getId();
            if (!settingService.getClientsOrBUWithoutGlobalSetting(companyId,
                    clientId)) {
                clients.add(client);
            }
        }

        return clients;
    }

    private List<Company> getClientOrBUsBasedOnUserScopeOfLoggedInUser(String companyId) {
        User loggedInUser = userService.getCurrentLoggedInUser();
        if (accessUtil.isCompanyAdmin(loggedInUser.getRole())
                || accessUtil.is4Dot5CompanyRole(loggedInUser.getRole())
                || accessUtil.isSuperUser(loggedInUser.getRole().getId())) {
            return getClientOrBUs(companyId);
        } else {
            return userApiExecutor.getAllClientOrBusOfUser(loggedInUser.getId(), companyId);
        }
    }


    @Override
    public List<Company> getClientOrBUs(String companyId) {

        List<Company> companyList = apiExecutor.getClientOrBUs(companyId);

        return companyList;
    }

    @Override
    public List<CompanyMinInfoDto> getCompanyList(int page, int size,
                                                  String userId, String sort, String sortDir) {

        List<Company> companyList = apiExecutor.getAllCompany(page, size,
                userId, sort, sortDir);

        List<CompanyMinInfoDto> companyDtos = new ArrayList<CompanyMinInfoDto>();
        if (page == 0) {
            User stubUser = userService.getUserById(userId);
            CompanyMinInfoDto companyDto = new CompanyMinInfoDto(stubUser
                    .getEmployer().getName(), stubUser.getEmployer().getId(),
                    stubUser.getEmployer().getCompanyType(), stubUser
                    .getEmployer().getCompanyState());

            companyDtos.add(companyDto);

        }
        for (Company company : companyList) {
            if (accessUtil.isStaffingOrCorporateComp(company)) {
                CompanyMinInfoDto companyDto = new CompanyMinInfoDto(
                        company.getName(), company.getId(),
                        company.getCompanyType(), company.getCompanyState());
                companyDtos.add(companyDto);
            }
        }
        return companyDtos;
    }

    @Override
    public Company getHostCompany() {
        return apiExecutor.getHostCompany(ApiConstants.HOST_COMPANY_NAME);
    }

    /**************** start of entity scope definition **************/
    @Override
    public RoleEntityScopeDto getRoleEntityScope(String companyId) {

        return entityScopeService.getRoleEntityScope(companyId);
    }

    @Override
    public RoleEntityScopeDto createRoleEnityScope(String companyId,
                                                   RoleEntityScopeDto dto) {
        try {
            RoleScopeDto.validateFields(dto.getScopeList());
            return entityScopeService.createRoleEnityScope(companyId, dto);
        } catch (CompanyException e) {
            throw new CompanyException(
                    Messages.CLIENT_ALL_LIST_CANNOT_HAVE_SAME_VALUE,
                    Messages.CLIENT_ALL_LIST_CANNOT_HAVE_SAME_VALUE);
        } catch (Exception e) {
            throw new RoleEntityScopeConflictException(
                    Messages.EXCEPTION_WHILE_UPDATING_RES,
                    Messages.EXCEPTION_WHILE_UPDATING_RES);
        }
    }

    @Override
    public RoleEntityScopeDto updateRoleEntityScope(String companyId,
                                                    RoleEntityScopeDto dto) {
        try {
            RoleScopeDto.validateFields(dto.getScopeList());
            return entityScopeService.updateRoleEntityScope(companyId, dto);
        } catch (CompanyException e) {
            throw new CompanyException(
                    Messages.CLIENT_ALL_LIST_CANNOT_HAVE_SAME_VALUE,
                    Messages.CLIENT_ALL_LIST_CANNOT_HAVE_SAME_VALUE);
        } catch (Exception e) {
            throw new RoleEntityScopeConflictException(
                    Messages.EXCEPTION_WHILE_UPDATING_RES,
                    Messages.EXCEPTION_WHILE_UPDATING_RES);
        }

    }

    @Override
    public List<CompanyMinInfoDto> get4Dot5CompanyList(String userId) {

        User user = userService.getUserById(userId);
        List<CompanyMinInfoDto> sortedCompanyList = new ArrayList<CompanyMinInfoDto>();
        List<Company> compList = user.getAccessControlList().getCompanyList();
        if (compList != null && !compList.isEmpty()) {
            Collections.sort(compList, new CompanyNameComparator());
            for (Company company : compList) {
                if (company.getCompanyState() != null
                        && !company.getCompanyState().equals(
                        AppConstants.ARCHIVED)) {
                    sortedCompanyList.add(new CompanyMinInfoDto(company
                            .getName(), company.getId(), company
                            .getCompanyType(), company.getCompanyState()));
                }
            }
        }
        return sortedCompanyList;
    }

    @Override
    public List<MasterIndustryDto> getParentIndustryList(String parentCompanyId) {
        return industryService.getParentIndustryList(parentCompanyId);
    }

    @Override
    public List<MasterIndustryDto> getChildIndustryList(String parentCompanyId,
                                                        String childCompanyId) {
        return industryService.getChildIndustryList(parentCompanyId,
                childCompanyId);
    }

    @Override
    public List<Industry> getIndustries(String companyId) {
        return apiExecutor.getIndustries(companyId);
    }

    @Override
    public CompanyDto updateCompanyState(CompanyDto company,
                                         TokenDetails tokenDetails, String companyId) {


        if (company.getCompanyState().equals(CompanyState.Draft.toString())) {
            company.setCompanyState(CompanyState.Active.toString());
            CompanyDto companyDto = saveCompany(company, tokenDetails,
                    companyId);
            return companyDto;
        } else {
            return company;
        }

    }

    @Override
    public CompanyDto getCompanyDto(String companyId, String userId,
                                    String roleId) {

        Company company = apiExecutor.getCompany(companyId);
        return composeCompanyDto(company, roleId);
    }

    @Override
    public Company getCompany(String companyId) {
        if (StringUtils.isBlank(companyId))
            return null;

        return apiExecutor.getCompany(companyId);
    }

    @Override
    public List<JobRequisitionRole> getJobRequisitionRoles(String companyId) {
        return apiExecutor.getJobRequisitionRoles(companyId);
    }

    @Override
    public List<String> getJobRequisitionCategories(String companyId) {
        return apiExecutor.getJobRequisitionCategories(companyId);
    }

    @Override
    public List<User> getRecruiters(String companyId) {

        return apiExecutor.getRecruitersByCompany(companyId);
    }

    @Override
    public List<RoleEntityScope> getBasicEntityScope(String companyId) {
        return apiExecutor.getBasicEntityScope(companyId);
    }


    @Override
    public Company saveQuestionList(String companyId, List<String> questionList) {

        LOGGER.info("Setting Recruiter questionList");

        Company returnedCompDto = apiExecutor.saveQuestionList(companyId, questionList);


        return returnedCompDto;
    }

    @Override
    public List<String> getQuestionList(String companyId) {
        return apiExecutor.getQuestionList(companyId);
    }

    @Override
    public String getCompanyDeleteInfo(String companyId) {
        return apiExecutor.getCompanyDeleteInfo(companyId);
    }

    @Override
    public String deleteCompany(DeleteDTO deleteDTO) {
        User loggedInUser = userService.getCurrentLoggedInUser();
        deleteDTO.setLoggedInUserId(loggedInUser.getId());
        return apiExecutor.deleteCompany(deleteDTO);
    }

    @Override
    public Company changeCompanyState(ChangeCompanyStateDto companyStateDto) {
        return apiExecutor.changeCompanyState(companyStateDto);

    }

    @Override
    public List<StaffingCompanyDto> getAllStaffingCompanies(String name, int page, int size, String sort, String sortDir) {
        return apiExecutor.getAllStaffingCompanies(name, page, size, sort, sortDir);
    }

    @Override
    public CorporateVendorDto saveCorporateVendorRelationship(CorporateVendorDto corporateVendorDto, String loggedInUserId) {
        return apiExecutor.saveCorporateVendorRelationship(corporateVendorDto, loggedInUserId);
    }

    @Override
    public List<CorporateVendorDto> saveCorporateVendors(AssociateVendorsDto associateVendorsDto, String loggedInUserId) {
        return apiExecutor.saveCorporateVendors(associateVendorsDto, loggedInUserId);
    }

    @Override
    public List<CorporateVendorDto> findAllVendorsOfCorporateCompany(String companyId) {

        return apiExecutor.findAllVendorsOfCorporateCompany(companyId);
    }

    @Override
    public BuVendorDto getAllBuVendors(String corporateCompanyId, String buId) {
        return apiExecutor.getAllBuVendors(corporateCompanyId, buId);
    }

    @Override
    public BuVendorDto saveBuVendors(BuVendorDto buVendorDto, String loggedInUserId) {
        return apiExecutor.saveBuVendors(buVendorDto, loggedInUserId);
    }

    @Override
    public Map<String, List<Company>> getAllBusOfClient(String companyId) {
        return apiExecutor.getAllBusOfClient(companyId);
    }

    @Override
    public CorporateVendorMinDto updateCorporateCompanyContactListInCorporateVendor(CorporateVendorDto corporateVendorDto) {
        return apiExecutor.updateCorporateCompanyContactListInCorporateVendor(corporateVendorDto);
    }

    @Override
    public List<ClientBuMinDto> getClientBuList(String corporateCompanyId, String vendorId, String loggedInUserId) {
        return apiExecutor.getClientBuList(corporateCompanyId, vendorId, loggedInUserId);
    }

    @Override
    public List<ContactDto> getCorporateCompanyContactList(String corporateCompanyId, String vendorId) {
        return apiExecutor.getCorporateCompanyContactList(corporateCompanyId, vendorId);
    }

    @Override
    public CorporateVendorMinDto getCorporateVendorRelationship(String corporateCompanyId, String vendorId) {
        return apiExecutor.getCorporateVendorRelationship(corporateCompanyId, vendorId);
    }

    @Override
    public List<CandidateAssessmentReportDetails> getCompanyCandidateReportDetails(AssessmentReportFilterDto assessmentReportFilterDto) {
        User user = userService.getCurrentLoggedInUser();
        assessmentReportFilterDto.setLoggedInUserId(user.getId());
        return apiExecutor.getCompanyCandidateReportDetails(assessmentReportFilterDto);
    }

    @Override
    public String deleteCorporateVendorRelationship(DeleteDTO deleteDTO) {
        User user = userService.getCurrentLoggedInUser();
        return apiExecutor.deleteCorporateVendorRelationship(deleteDTO, user.getId());
    }

    @Override
    public String deleteBuVendorRelationship(DeleteBuVendorDto deleteBuVendorDto) {
        User user = userService.getCurrentLoggedInUser();
        deleteBuVendorDto.setLoggedInUserId(user.getId());
        return apiExecutor.deleteBuVendorRelationship(deleteBuVendorDto);
    }

    @Override
    public DeleteCountDto getSharedRequisitionAndBuCounts(DeleteBuVendorDto deleteBuVendorDto) {
        return apiExecutor.getSharedRequisitionAndBuCounts(deleteBuVendorDto);
    }

    @Override
    public List<ClientOrBuCountDTO> getClientOrBuDetails(ClientOrBuDetailsRequestDTO clientOrBuDetailsRequestDTO) {
        List<CompanyDto> allClientsOrBu = getClientOrBuList(clientOrBuDetailsRequestDTO.getCompanyId());
        List<String> allClientOrBuIds = new ArrayList<>();
        for (CompanyDto companyDto : allClientsOrBu) {
            allClientOrBuIds.add(companyDto.getCompany().getId());
        }
        ClientOrBuDetailsDTO clientOrBuDetailsDTO = new ClientOrBuDetailsDTO();
        clientOrBuDetailsDTO.setCompanyId(clientOrBuDetailsRequestDTO.getCompanyId());
        clientOrBuDetailsDTO.setWorkflowStep(clientOrBuDetailsRequestDTO.getWorkflowStep());
        clientOrBuDetailsDTO.setAllClientOrBu(allClientOrBuIds);
        if (!CollectionUtils.isEmpty(allClientsOrBu)) {
            return apiExecutor.getClientOrBuDetails(clientOrBuDetailsDTO);
        }
        return null;
    }

    @Override
    public List<CompanyMinDto> searchForCompanies(String name, Integer page, Integer size, String sort, String sortDir) {
        return apiExecutor.searchForCompanies(name, page, size, sort, sortDir);
    }

    @Override
    public String getAssessmentReportQuestionType(String companyId) {
        return apiExecutor.getAssessmentReportQuestionType(companyId);
    }

    @Override
    public String updateAssessmentReportQuestionType(String companyId, QuestionType questionType) {
        return apiExecutor.updateAssessmentReportQuestionType(companyId, questionType);
    }

    @Override
    public String hasAccessToAddCandidateWithOrWithoutResume(String companyId, String clientOrBuId) {
        return apiExecutor.hasAccessToAddCandidateWithOrWithoutResume(companyId, clientOrBuId);
    }

    @Override
    public String getSharedClientDetails(String companyId) {
        return apiExecutor.getSharedClientDetails(companyId);
    }

    @Override
    public String getVendorsByUserScope(String companyId) {
        String userId = userService.getCurrentLoggedInUser().getId();
        return apiExecutor.getVendorsByUserScope(companyId, userId);
    }

    @Override
    public String getClientOrBuPointOfContacts(String corporateId, String clientOrBuId) {
        return apiExecutor.getClientOrBuPointOfContacts(corporateId, clientOrBuId);
    }

    @Override
    public List<ClientOrBuPointOfContactDTO> getSharedClientBus(String companyId, String sharedClientBuOrCorpId) {
        User user = userService.getCurrentLoggedInUser();
        Company company = getCompany(companyId);
        List<ClientOrBuPointOfContactDTO> clientOrBuPointOfContactDTOS = new ArrayList<>();
        AccessControlDto accessControlDto = new AccessControlDto();
        AccessControlList userScope = userApiExecutor.getEnityScope(user.getId());
        userScope.setAllBusOfClient(false);
        userScope.setAllclientOrBU(false);
        accessControlDto = fetchUserScopeService.getClientOrBu(accessControlDto, user.getId(),
                company, userScope, true);

        if (accessControlDto.getAllBusOfClientList() != null) {
            Map<String, List<SelectedClientOrBuDto>> allBusOfClients = accessControlDto.getAllBusOfClientList();

            if (allBusOfClients.containsKey(sharedClientBuOrCorpId)) {
                List<SelectedClientOrBuDto> selectedClientOrBuDtos = allBusOfClients.get(sharedClientBuOrCorpId);
                for (SelectedClientOrBuDto selectedClientOrBuDto : selectedClientOrBuDtos) {
                    clientOrBuPointOfContactDTOS.add(new ClientOrBuPointOfContactDTO(selectedClientOrBuDto));
                }
            }
        }
        return clientOrBuPointOfContactDTOS;
    }

    @Override
    public CorporateVendorMinDto setCorporateVendorRelationship(String corporateCompanyId, String vendorId) {
        return apiExecutor.setCorporateVendorRelationship(corporateCompanyId, vendorId);
    }

    @Override
    public String uploadCompanyLogo(String companyId, MultipartFile file, boolean isSmallHeaderLogo) throws IOException {
        return apiExecutor.uploadCompanyLogo(companyId, file, isSmallHeaderLogo);
    }

    @Override
    public String updateCompany(String companyId, String testLinkName) {
        return apiExecutor.updateCompany(companyId, testLinkName);
    }


}
