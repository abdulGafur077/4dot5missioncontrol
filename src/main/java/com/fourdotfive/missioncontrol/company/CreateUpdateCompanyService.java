package com.fourdotfive.missioncontrol.company;

import com.fourdotfive.missioncontrol.common.TokenDetails;
import com.fourdotfive.missioncontrol.dtos.company.CompanyDto;

public interface CreateUpdateCompanyService {

	CompanyDto saveCompany(CompanyDto companyDto,
			TokenDetails tokenDetails, String parentCompanyId);

	boolean isDuplicateByName(String name, String id);

}
