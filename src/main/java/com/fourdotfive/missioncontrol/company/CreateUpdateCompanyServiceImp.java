package com.fourdotfive.missioncontrol.company;

import com.fourdotfive.missioncontrol.common.AccessUtil;
import com.fourdotfive.missioncontrol.common.AppConstants;
import com.fourdotfive.missioncontrol.common.Messages;
import com.fourdotfive.missioncontrol.common.TokenDetails;
import com.fourdotfive.missioncontrol.dtos.company.AuditDto;
import com.fourdotfive.missioncontrol.dtos.company.CompanyDto;
import com.fourdotfive.missioncontrol.exception.DuplicateCompanyException;
import com.fourdotfive.missioncontrol.exception.PlatformException;
import com.fourdotfive.missioncontrol.licensepreference.LicensePrefDtoToLicensePref;
import com.fourdotfive.missioncontrol.licensepreference.LicensePreferences;
import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.company.CompanyState;
import com.fourdotfive.missioncontrol.pojo.company.CompanyType;
import com.fourdotfive.missioncontrol.pojo.user.Contact;
import com.fourdotfive.missioncontrol.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CreateUpdateCompanyServiceImp implements
        CreateUpdateCompanyService {
    @Autowired
    private CompanyService companyService;

    @Autowired
    private AccessUtil accessUtil;

    @Autowired
    private LicensePrefDtoToLicensePref dtoToLicensePref;

    @Autowired
    private CompanyApiExecutor apiExecutor;

    @Autowired
    private UserService userService;

    @Override
    public CompanyDto saveCompany(CompanyDto companyDto,
                                  TokenDetails tokenDetails, String parentCompanyId) {
        companyDto = AuditDto.setAuditDetails(companyDto, tokenDetails);
        Company company = CompanyDto.mapCompanyDtoToCompany(companyDto);
        if (accessUtil.isClientOrBU(companyDto.getCompany().getCompanyType())) {
            List<Company> clientOrBuList = companyService.getClientOrBUs(parentCompanyId);
            checkForDuplicateCompany(clientOrBuList, company);
        }
        if (accessUtil.isClientOrBU(company)) {
            checkForDuplicateHiringManagers(company);
            if (company.getCompanyType().equals(CompanyType.BusinessUnit) && company.getContactList() != null) {
                checkRoleEntityScopeIsDefined(company, parentCompanyId);
            }
        }

        company = setLicensePrefsAndState(companyDto, company);

        Company response = apiExecutor.saveCompany(company);
        apiExecutor.addSettingsToCompany(parentCompanyId, response.getId());
        setIndustryList();
        CompanyDto mappedCompany = companyService.composeCompanyDto(response,
                tokenDetails.getRoleId());
        mappedCompany.getCompany().setIndustryGroup(
                companyDto.getCompany().getIndustryGroup());
        mappedCompany.getCompany().setIndustryList(
                companyDto.getCompany().getIndustryList());
        /*mappedCompany.getCompany().setQuestionList(
                companyDto.getCompany().getQuestionList());*/

        return mappedCompany;
    }

    @Override
    public boolean isDuplicateByName(String name, String id) {
        return apiExecutor.isDuplicateCompanyByName(name, id);
    }

    private void checkRoleEntityScopeIsDefined(Company company, String parentCompanyId) {
        boolean checkRoleEntityScope = false;
        for (Contact contact : company.getContactList()) {
            if (contact.getIsHiringManager() && contact.getUserId() == null && contact.getManagerId() != null) {
                checkRoleEntityScope = true;
                break;
            }
        }
        if (checkRoleEntityScope) {
            if (!userService.checkRoleEntityScopeIsDefined(parentCompanyId)) {
                throw new PlatformException(Messages.ROLE_ENTITY_SCOPE_NOT_DEFINED_HM);
            }
        }
    }

    private void checkForDuplicateHiringManagers(Company company) {
        apiExecutor.checkForDuplicateHiringManagers(company);
    }

    private void setIndustryList() {
        /*
         * if (companyDto.getCompany().getIndustryList() != null &&
		 * !companyDto.getCompany().getIndustryList().isEmpty()) {
		 * 
		 * if (companyDto.getCompany().getCompanyType()
		 * .equals(CompanyType.Client) ||
		 * companyDto.getCompany().getCompanyType()
		 * .equals(CompanyType.BusinessUnit)) { List<Industry> industryList =
		 * CompanyDto .getIndustryListFromIndustry(companyDto.getCompany()
		 * .getIndustryList());
		 * industryService.updateParentCompanyIndustryList(industryList,
		 * parentCompanyId); }
		 * 
		 * }
		 */
    }

    private Company setLicensePrefsAndState(CompanyDto companyDto,
                                            Company company) {
        LicensePreferences licePref = null;
        if (company.getId() != null) {
            Company existComp = companyService.getCompany(company.getId());

            // Setting company state
            if (company.getCompanyState() == null) {
                company.setCompanyState(existComp.getCompanyState());
            }

            // Setting license preferences
            if (companyDto.getLicensePreferences() != null) {
                licePref = dtoToLicensePref.convert(companyDto
                        .getLicensePreferences());
            } else {
                if (existComp.getLicensePreferences() != null) {
                    licePref = existComp.getLicensePreferences();
                    if (company.getCompanyType() == CompanyType.Corporation) {
                        licePref.setAddCandidateOnlyWithResume(true);
                    } else if (company.getCompanyType() == CompanyType.Client) {
                        licePref.setAddCandidateOnlyWithResume(true);
                    }
                }
            }

            // set contact details
            if (company.getContactList() == null
                    && existComp.getContactList() != null) {
                company.setContactList(existComp.getContactList());
            }

        } else {
            // Setting the state of the company to draft for the first time. The
            // state is activated after the license preferences is set.
            company.setCompanyState(CompanyState.Draft.toString());
        }
        company.setLicensePreferences(licePref);

        return company;
    }

    private void checkForDuplicateCompany(List<Company> clientOrBuList, Company company) {

        String shortName = null;
        String name = null;
        String newShortName = null;
        String newName = null;

        if (clientOrBuList != null && !clientOrBuList.isEmpty()) {
            // We have two for loops because we are checking for short name
            // first and then for name, the order is important!

            // Check for duplicate short name
            for (Company clientCompany : clientOrBuList) {

                shortName = clientCompany.getShortName();
                name = clientCompany.getName();
                newShortName = company.getShortName();
                newName = company.getName();

                String companyType = clientCompany.getCompanyType().toString();
                if (companyType.equals(CompanyType.Client)) {
                    companyType = CompanyType.Client.toString();
                } else if (companyType.equals(CompanyType.BusinessUnit.toString())) {
                    companyType = AppConstants.BU;
                }
                if (!clientCompany.getId().equals(company.getId()) && !accessUtil.isCompanyNotActive(clientCompany)) {
                    if (shortName.equalsIgnoreCase(newShortName) && name.equalsIgnoreCase(newName)) {

                        DuplicateCompanyException e = new DuplicateCompanyException(
                                Messages.NAME_AND_SHORTNAME_EXISTS, companyType, shortName, name);
                        e.setDeveloperMessage(DuplicateCompanyException.SHORTNAME_AND_NAME);
                        throw e;

                    } else if (shortName.equalsIgnoreCase(newShortName)) {
                        DuplicateCompanyException e = new DuplicateCompanyException(Messages.SHORT_NAME_EXISTS, companyType,
                                shortName);
                        e.setDeveloperMessage(DuplicateCompanyException.SHORTNAME);
                        throw e;
                    } else if (name.equalsIgnoreCase(newShortName)) {
                        DuplicateCompanyException e = new DuplicateCompanyException(Messages.NAME_EXISTS, companyType, name);
                        e.setDeveloperMessage(DuplicateCompanyException.SHORTNAME);
                        throw e;
                    }
                }
            }

            // check for duplicate name
            for (Company clientCompany : clientOrBuList) {

                shortName = clientCompany.getShortName();
                name = clientCompany.getName();
                newShortName = company.getShortName();
                newName = company.getName();

                String companyType = clientCompany.getCompanyType().toString();
                if (companyType.equals(CompanyType.Client)) {
                    companyType = CompanyType.Client.toString();
                } else if (companyType.equals(CompanyType.BusinessUnit.toString())) {
                    companyType = AppConstants.BU;
                }
                if (!clientCompany.getId().equals(company.getId()) && !accessUtil.isCompanyNotActive(clientCompany)) {
                    if (shortName.equalsIgnoreCase(newName)) {
                        DuplicateCompanyException e = new DuplicateCompanyException(Messages.SHORT_NAME_EXISTS, companyType,
                                shortName);
                        e.setDeveloperMessage(DuplicateCompanyException.NAME);
                        throw e;
                    } else if (name.equalsIgnoreCase(newName)) {
                        DuplicateCompanyException e = new DuplicateCompanyException(Messages.NAME_EXISTS, companyType, name);
                        e.setDeveloperMessage(DuplicateCompanyException.NAME);
                        throw e;
                    }
                }
            }
        }
    }

    private void compare() {

    }

}
