package com.fourdotfive.missioncontrol.config;

import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.fourdotfive.missioncontrol.alert.SQSChatAlertReader;
import com.fourdotfive.missioncontrol.alert.SQSNotificationReader;
import com.fourdotfive.missioncontrol.alert.SQSWorkflowUpdateAlertReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.destination.DynamicDestinationResolver;

import javax.jms.Session;

@Configuration
@EnableJms
@Profile("!local")
public class SQSJmsConfig {
    private final AWSCredentials awsCredentials = new BasicAWSCredentials("AKIAIUGQZD2OIX57QD2Q", "YlDPbf5aZGa5lyUWU/4TBiVzHD4t20b4Bf2jYz1Z");
    private final AWSCredentialsProvider awsCredentialsProvider = new AWSStaticCredentialsProvider(awsCredentials);

    private SQSConnectionFactory connectionFactory =
            SQSConnectionFactory.builder()
                    .withRegion(Region.getRegion(Regions.US_EAST_1))
                    .withAWSCredentialsProvider(awsCredentialsProvider)
                    .build();

    @Bean
    public DefaultJmsListenerContainerFactory jmsListenerContainerFactory() {
        DefaultJmsListenerContainerFactory factory =
                new DefaultJmsListenerContainerFactory();
        factory.setConnectionFactory(this.connectionFactory);
        factory.setDestinationResolver(new DynamicDestinationResolver());
        factory.setConcurrency("3-10");
        factory.setSessionAcknowledgeMode(Session.CLIENT_ACKNOWLEDGE);
        return factory;
    }

    @Bean
    public JmsTemplate defaultJmsTemplate() {
        return new JmsTemplate(this.connectionFactory);
    }

    @Bean
    public SQSNotificationReader sqsEmailReader() {
        return new SQSNotificationReader();
    }

    @Bean
    public SQSWorkflowUpdateAlertReader sqsWorkflowUpdateAlertReader() {
        return new SQSWorkflowUpdateAlertReader();
    }

    @Bean
    public SQSChatAlertReader sqsChatAlertReader() {
        return new SQSChatAlertReader();
    }
}
