package com.fourdotfive.missioncontrol.dto.assessment;

import com.fourdotfive.missioncontrol.assessment.LicensePreferenceEnum;

public class AccessTimeRequestDTO {
    private LicensePreferenceEnum licensePreferenceEnum;
    private String jobId;

    public LicensePreferenceEnum getLicensePreferenceEnum() {
        return licensePreferenceEnum;
    }

    public void setLicensePreferenceEnum(LicensePreferenceEnum licensePreferenceEnum) {
        this.licensePreferenceEnum = licensePreferenceEnum;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }
}
