package com.fourdotfive.missioncontrol.dtos.alert;

import java.util.List;

public class NotificationStateChangeDTO {
    private List<String> notificationIds;
    private String state;

    public List<String> getNotificationIds() {
        return notificationIds;
    }

    public void setNotificationIds(List<String> notificationIds) {
        this.notificationIds = notificationIds;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
