package com.fourdotfive.missioncontrol.dtos.candidate;

import java.util.List;

public class ActiveCandidateMinDto {

	private String firstName;

	private String lastName;

	private String phoneNumber;

	private String email;

	private String im;

	private String id;

	private String lastContact;

	private List<String> activities;

	private String state;

	private List<ActiveJobMatchMinDto> jobMatches;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIm() {
		return im;
	}

	public void setIm(String im) {
		this.im = im;
	}

	public String getLastContact() {
		return lastContact;
	}

	public void setLastContact(String lastContact) {
		this.lastContact = lastContact;
	}

	public List<ActiveJobMatchMinDto> getJobMatches() {
		return jobMatches;
	}

	public void setJobMatches(List<ActiveJobMatchMinDto> jobMatches) {
		this.jobMatches = jobMatches;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<String> getActivities() {
		return activities;
	}

	public void setActivities(List<String> activities) {
		this.activities = activities;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ActiveCandidateMinDto [firstName=");
		builder.append(firstName);
		builder.append(", lastName=");
		builder.append(lastName);
		builder.append(", phoneNumber=");
		builder.append(phoneNumber);
		builder.append(", email=");
		builder.append(email);
		builder.append(", im=");
		builder.append(im);
		builder.append(", id=");
		builder.append(id);
		builder.append(", lastContact=");
		builder.append(lastContact);
		builder.append(", activities=");
		builder.append(activities);
		builder.append(", state=");
		builder.append(state);
		builder.append(", jobMatches=");
		builder.append(jobMatches);
		builder.append("]");
		return builder.toString();
	}

}
