package com.fourdotfive.missioncontrol.dtos.candidate;

public class ActiveJobMatchMinDto extends JobMatchMinDto {

	private int techAssessmentScore;
	private boolean techAssessment;
	private boolean valueAssessment;
	private boolean verification;

	public int getTechAssessmentScore() {
		return techAssessmentScore;
	}

	public void setTechAssessmentScore(int techAssessmentScore) {
		this.techAssessmentScore = techAssessmentScore;
	}

	public boolean isTechAssessment() {
		return techAssessment;
	}

	public void setTechAssessment(boolean techAssessment) {
		this.techAssessment = techAssessment;
	}

	public boolean isValueAssessment() {
		return valueAssessment;
	}

	public void setValueAssessment(boolean valueAssessment) {
		this.valueAssessment = valueAssessment;
	}

	public boolean isVerification() {
		return verification;
	}

	public void setVerification(boolean verification) {
		this.verification = verification;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ActiveJobMatchMinDto [techAssessmentScore=");
		builder.append(techAssessmentScore);
		builder.append(", techAssessment=");
		builder.append(techAssessment);
		builder.append(", valueAssessment=");
		builder.append(valueAssessment);
		builder.append(", verification=");
		builder.append(verification);
		builder.append("]");
		return builder.toString();
	}

}
