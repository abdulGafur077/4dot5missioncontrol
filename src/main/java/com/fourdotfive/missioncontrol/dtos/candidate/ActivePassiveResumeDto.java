package com.fourdotfive.missioncontrol.dtos.candidate;

import java.util.Date;

public class ActivePassiveResumeDto {

	private Date uploadedTime;
	private String fileName;
	private String cloudFileLocation;
	
	public Date getUploadedTime() {
		return uploadedTime;
	}

	public void setUploadedTime(Date uploadedTime) {
		this.uploadedTime = uploadedTime;
	}
	

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getCloudFileLocation() {
		return cloudFileLocation;
	}

	public void setCloudFileLocation(String cloudFileLocation) {
		this.cloudFileLocation = cloudFileLocation;
	}
	
	
	
}
