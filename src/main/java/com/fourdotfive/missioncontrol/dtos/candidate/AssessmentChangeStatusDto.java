package com.fourdotfive.missioncontrol.dtos.candidate;

public class AssessmentChangeStatusDto {
    private String jobMatchId;
    private String notes;
    private CandidateAssessmentTestType testType;

    public String getJobMatchId() {
        return jobMatchId;
    }

    public void setJobMatchId(String jobMatchId) {
        this.jobMatchId = jobMatchId;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public CandidateAssessmentTestType getTestType() {
        return testType;
    }

    public void setTestType(CandidateAssessmentTestType testType) {
        this.testType = testType;
    }
}
