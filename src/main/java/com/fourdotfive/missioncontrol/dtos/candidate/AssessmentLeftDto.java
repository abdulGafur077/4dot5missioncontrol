package com.fourdotfive.missioncontrol.dtos.candidate;

public class AssessmentLeftDto {
    private String jobMatchId;
    private String notes;
    private String loggedInUserId;
    private CandidateAssessmentTestType testType;

    public AssessmentLeftDto(AssessmentChangeStatusDto assessmentChangeStatus, String loggedInUserId) {
        this.jobMatchId = assessmentChangeStatus.getJobMatchId();
        this.notes = assessmentChangeStatus.getNotes();
        this.testType = assessmentChangeStatus.getTestType();
        this.loggedInUserId = loggedInUserId;
    }

    public String getJobMatchId() {
        return jobMatchId;
    }

    public void setJobMatchId(String jobMatchId) {
        this.jobMatchId = jobMatchId;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getLoggedInUserId() {
        return loggedInUserId;
    }

    public void setLoggedInUserId(String loggedInUserId) {
        this.loggedInUserId = loggedInUserId;
    }

    public CandidateAssessmentTestType getTestType() {
        return testType;
    }

    public void setTestType(CandidateAssessmentTestType testType) {
        this.testType = testType;
    }
}
