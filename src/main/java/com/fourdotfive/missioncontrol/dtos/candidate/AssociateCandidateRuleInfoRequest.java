package com.fourdotfive.missioncontrol.dtos.candidate;

import java.util.List;

public class AssociateCandidateRuleInfoRequest {

    private String companyId;
    private String jobId;
    private List<String> candidateIds;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public List<String> getCandidateIds() {
        return candidateIds;
    }

    public void setCandidateIds(List<String> candidateIds) {
        this.candidateIds = candidateIds;
    }
}
