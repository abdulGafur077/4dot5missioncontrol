package com.fourdotfive.missioncontrol.dtos.candidate;

public class CancelAssessmentDto {
    private String jobMatchId;
    private String loggedInUserId;
    private String notes;
    private String companyId;
    private String externalNote;

    public CancelAssessmentDto() {
    }

    public CancelAssessmentDto(CancelValueTechAssessmentDto cancelValueTechAssessmentDto) {
        this.jobMatchId = cancelValueTechAssessmentDto.getJobMatchId();
        this.notes = cancelValueTechAssessmentDto.getNotes();
        this.companyId = cancelValueTechAssessmentDto.getCompanyId();
        this.externalNote = cancelValueTechAssessmentDto.getExternalNote();
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getJobMatchId() {
        return jobMatchId;
    }

    public void setJobMatchId(String jobMatchId) {
        this.jobMatchId = jobMatchId;
    }

    public String getLoggedInUserId() {
        return loggedInUserId;
    }

    public void setLoggedInUserId(String loggedInUserId) {
        this.loggedInUserId = loggedInUserId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getExternalNote() {
        return externalNote;
    }

    public void setExternalNote(String externalNote) {
        this.externalNote = externalNote;
    }
}
