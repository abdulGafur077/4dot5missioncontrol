package com.fourdotfive.missioncontrol.dtos.candidate;


import java.util.List;

public class CancelAssessmentListDto {
    private List<String> jobMatchIds;
    private CandidateAssessmentTestType testType;
    private String loggedInUserId;
    private String notes;
    private String companyId;
    private String externalNote;

    public CancelAssessmentListDto() {
    }

    public CancelAssessmentListDto(String loggedInUserId, CancelValueTechAssessmentListDto cancelValueTechAssessmentListDto) {
        this.jobMatchIds = cancelValueTechAssessmentListDto.getJobMatchIds();
        this.testType = cancelValueTechAssessmentListDto.getTestType();
        this.loggedInUserId = loggedInUserId;
        this.notes = cancelValueTechAssessmentListDto.getNotes();
        this.companyId = cancelValueTechAssessmentListDto.getCompanyId();
        this.externalNote = cancelValueTechAssessmentListDto.getExternalNote();
    }


    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public List<String> getJobMatchIds() {
        return jobMatchIds;
    }

    public void setJobMatchIds(List<String> jobMatchIds) {
        this.jobMatchIds = jobMatchIds;
    }

    public CandidateAssessmentTestType getTestType() {
        return testType;
    }

    public void setTestType(CandidateAssessmentTestType testType) {
        this.testType = testType;
    }

    public String getLoggedInUserId() {
        return loggedInUserId;
    }

    public void setLoggedInUserId(String loggedInUserId) {
        this.loggedInUserId = loggedInUserId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getExternalNote() {
        return externalNote;
    }

    public void setExternalNote(String externalNote) {
        this.externalNote = externalNote;
    }
}
