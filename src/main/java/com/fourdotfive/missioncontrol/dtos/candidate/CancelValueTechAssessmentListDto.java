package com.fourdotfive.missioncontrol.dtos.candidate;

import java.util.List;

public class CancelValueTechAssessmentListDto {
    private List<String> jobMatchIds;
    private CandidateAssessmentTestType testType;
    private String notes;
    private String companyId;
    private String externalNote;

    public List<String> getJobMatchIds() {
        return jobMatchIds;
    }

    public void setJobMatchIds(List<String> jobMatchIds) {
        this.jobMatchIds = jobMatchIds;
    }

    public CandidateAssessmentTestType getTestType() {
        return testType;
    }

    public void setTestType(CandidateAssessmentTestType testType) {
        this.testType = testType;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getExternalNote() {
        return externalNote;
    }

    public void setExternalNote(String externalNote) {
        this.externalNote = externalNote;
    }
}
