package com.fourdotfive.missioncontrol.dtos.candidate;

import com.fourdotfive.missioncontrol.pojo.user.Address;

import java.util.List;

public class CandidateActivityDto {

    private String jobMatchId;
    private String requisitionId;
    private String clientorbu;
    private String candidateId;
    private String requisitionNumber;
    private String title;
    private String activityType;
    private String fromState;
    private String toState;
    private String createdBy;
    private String createdDate;
    private String clientOrBuName;
    private Address jobLocation;
    private String note;
    private List<String> candidateNameList;
    private String companyName;
    private String count;
    private String testName;
    private String externalNote;
    private String reasonNote;
    private String date;
    private String meetingCount;

    public String getJobMatchId() {
        return jobMatchId;
    }

    public void setJobMatchId(String jobMatchId) {
        this.jobMatchId = jobMatchId;
    }

    public String getRequisitionId() {
        return requisitionId;
    }

    public void setRequisitionId(String requisitionId) {
        this.requisitionId = requisitionId;
    }

    public String getClientorbu() {
        return clientorbu;
    }

    public void setClientorbu(String clientorbu) {
        this.clientorbu = clientorbu;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(String candidateId) {
        this.candidateId = candidateId;
    }

    public String getRequisitionNumber() {
        return requisitionNumber;
    }

    public void setRequisitionNumber(String requisitionNumber) {
        this.requisitionNumber = requisitionNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public String getFromState() {
        return fromState;
    }

    public void setFromState(String fromState) {
        this.fromState = fromState;
    }

    public String getToState() {
        return toState;
    }

    public void setToState(String toState) {
        this.toState = toState;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getClientOrBuName() {
        return clientOrBuName;
    }

    public void setClientOrBuName(String clientOrBuName) {
        this.clientOrBuName = clientOrBuName;
    }


    public Address getJobLocation() {
        return jobLocation;
    }

    public void setJobLocation(Address jobLocation) {
        this.jobLocation = jobLocation;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<String> getCandidateNameList() {
        return candidateNameList;
    }

    public void setCandidateNameList(List<String> candidateNameList) {
        this.candidateNameList = candidateNameList;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public String getExternalNote() {
        return externalNote;
    }

    public void setExternalNote(String externalNote) {
        this.externalNote = externalNote;
    }

    public String getReasonNote() {
        return reasonNote;
    }

    public void setReasonNote(String reasonNote) {
        this.reasonNote = reasonNote;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMeetingCount() {
        return meetingCount;
    }

    public void setMeetingCount(String meetingCount) {
        this.meetingCount = meetingCount;
    }
}
