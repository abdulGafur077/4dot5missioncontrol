package com.fourdotfive.missioncontrol.dtos.candidate;

import com.fourdotfive.missioncontrol.dtos.job.JobSalaryDto;

public class CandidateAdditionalDetailsCardDto {

    private JobSalaryDto candidateSalary;

    private JobSalaryDto candidateEqualSalary;

    private JobSalaryDto requisitionSalary;

    private SalaryIcon salaryIcon;

    private String[] sponsorships;

    private String sponsorshipsNote;

    private String willingToRelocate;

    private String relocationNote;

    public JobSalaryDto getCandidateSalary() {
        return candidateSalary;
    }

    public void setCandidateSalary(JobSalaryDto candidateSalary) {
        this.candidateSalary = candidateSalary;
    }

    public JobSalaryDto getCandidateEqualSalary() {
        return candidateEqualSalary;
    }

    public void setCandidateEqualSalary(JobSalaryDto candidateEqualSalary) {
        this.candidateEqualSalary = candidateEqualSalary;
    }

    public JobSalaryDto getRequisitionSalary() {
        return requisitionSalary;
    }

    public void setRequisitionSalary(JobSalaryDto requisitionSalary) {
        this.requisitionSalary = requisitionSalary;
    }

    public SalaryIcon getSalaryIcon() {
        return salaryIcon;
    }

    public void setSalaryIcon(SalaryIcon salaryIcon) {
        this.salaryIcon = salaryIcon;
    }

    public String[] getSponsorships() {
        return sponsorships;
    }

    public void setSponsorships(String[] sponsorships) {
        this.sponsorships = sponsorships;
    }

    public String getSponsorshipsNote() {
        return sponsorshipsNote;
    }

    public void setSponsorshipsNote(String sponsorshipsNote) {
        this.sponsorshipsNote = sponsorshipsNote;
    }

    public String getWillingToRelocate() {
        return willingToRelocate;
    }

    public void setWillingToRelocate(String willingToRelocate) {
        this.willingToRelocate = willingToRelocate;
    }

    public String getRelocationNote() {
        return relocationNote;
    }

    public void setRelocationNote(String relocationNote) {
        this.relocationNote = relocationNote;
    }

}
