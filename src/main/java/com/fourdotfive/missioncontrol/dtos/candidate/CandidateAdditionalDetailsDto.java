package com.fourdotfive.missioncontrol.dtos.candidate;


import com.fourdotfive.missioncontrol.dtos.job.JobSalaryDto;

public class CandidateAdditionalDetailsDto {

    private JobSalaryDto salary;

    private String[] sponsorships;

    private String sponsorshipsNote;

    private String willingToRelocate;

    private String relocationNote;

    public JobSalaryDto getSalary() {
        return salary;
    }

    public void setSalary(JobSalaryDto salary) {
        this.salary = salary;
    }

    public String[] getSponsorships() {
        return sponsorships;
    }

    public void setSponsorships(String[] sponsorships) {
        this.sponsorships = sponsorships;
    }

    public String getSponsorshipsNote() {
        return sponsorshipsNote;
    }

    public void setSponsorshipsNote(String sponsorshipsNote) {
        this.sponsorshipsNote = sponsorshipsNote;
    }

    public String getWillingToRelocate() {
        return willingToRelocate;
    }

    public void setWillingToRelocate(String willingToRelocate) {
        this.willingToRelocate = willingToRelocate;
    }

    public String getRelocationNote() {
        return relocationNote;
    }

    public void setRelocationNote(String relocationNote) {
        this.relocationNote = relocationNote;
    }
}
