package com.fourdotfive.missioncontrol.dtos.candidate;

public class CandidateAssessmentDetailRequestDto {
    private String jobMatchId;
    private String assessmentType;

    public String getJobMatchId() {
        return jobMatchId;
    }

    public void setJobMatchId(String jobMatchId) {
        this.jobMatchId = jobMatchId;
    }

    public String getAssessmentType() {
        return assessmentType;
    }

    public void setAssessmentType(String assessmentType) {
        this.assessmentType = assessmentType;
    }
}
