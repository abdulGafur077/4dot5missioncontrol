package com.fourdotfive.missioncontrol.dtos.candidate;

public class CandidateAssessmentDto {
    private String candidateName;
    private String testName;

    public String getCandidateName() {
        return candidateName;
    }

    public void setCandidateName(String candidateName) {
        this.candidateName = candidateName;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }
}
