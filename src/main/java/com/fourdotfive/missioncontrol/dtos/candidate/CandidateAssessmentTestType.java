package com.fourdotfive.missioncontrol.dtos.candidate;


public enum CandidateAssessmentTestType {
    Technical, Value
}
