package com.fourdotfive.missioncontrol.dtos.candidate;

import java.util.List;

public class CandidateAsset {

	private String assetType;

	private List<CandidateAssetDetails> assetTypeDetails;

	public String getAssetType() {
		return assetType;
	}

	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}

	public List<CandidateAssetDetails> getAssetTypeDetails() {
		return assetTypeDetails;
	}

	public void setAssetTypeDetails(
			List<CandidateAssetDetails> assetTypeDetails) {
		this.assetTypeDetails = assetTypeDetails;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CandidateAssest [assetType=");
		builder.append(assetType);
		builder.append(", assetTypeDetails=");
		builder.append(assetTypeDetails);
		builder.append("]");
		return builder.toString();
	}

}
