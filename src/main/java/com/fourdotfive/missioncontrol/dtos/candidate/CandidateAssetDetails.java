package com.fourdotfive.missioncontrol.dtos.candidate;

public class CandidateAssetDetails {

	private String assetName;

	private String candidateExp;

	private String candidateData;

	private boolean isSummary;

	private String candidatematchType;

	public String getAssetName() {
		return assetName;
	}

	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}

	public String getCandidateExp() {
		return candidateExp;
	}

	public void setCandidateExp(String candidateExp) {
		this.candidateExp = candidateExp;
	}

	public String getCandidateData() {
		return candidateData;
	}

	public void setCandidateData(String candidateData) {
		this.candidateData = candidateData;
	}

	public boolean isSummary() {
		return isSummary;
	}

	public void setSummary(boolean isSummary) {
		this.isSummary = isSummary;
	}

	public String getCandidatematchType() {
		return candidatematchType;
	}

	public void setCandidatematchType(String candidatematchType) {
		this.candidatematchType = candidatematchType;
	}

}
