package com.fourdotfive.missioncontrol.dtos.candidate;


import com.fourdotfive.missioncontrol.dtos.jobmatch.ReasonCodeDTO;

public class CandidateAvailabilityDTO {
    private String candidateId;
    private String availabilityDate;
    private String jobLeftDate;
    private String loggedInUser;
    private String jobLeftNote;
    private ReasonCodeDTO reasonCodeDTO;
    private String companyId;

    public String getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(String candidateId) {
        this.candidateId = candidateId;
    }

    public String getAvailabilityDate() {
        return availabilityDate;
    }

    public void setAvailabilityDate(String availabilityDate) {
        this.availabilityDate = availabilityDate;
    }

    public String getJobLeftDate() {
        return jobLeftDate;
    }

    public void setJobLeftDate(String jobLeftDate) {
        this.jobLeftDate = jobLeftDate;
    }

    public String getLoggedInUser() {
        return loggedInUser;
    }

    public void setLoggedInUser(String loggedInUser) {
        this.loggedInUser = loggedInUser;
    }

    public String getJobLeftNote() {
        return jobLeftNote;
    }

    public void setJobLeftNote(String jobLeftNote) {
        this.jobLeftNote = jobLeftNote;
    }

    public ReasonCodeDTO getReasonCodeDTO() {
        return reasonCodeDTO;
    }

    public void setReasonCodeDTO(ReasonCodeDTO reasonCodeDTO) {
        this.reasonCodeDTO = reasonCodeDTO;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
}
