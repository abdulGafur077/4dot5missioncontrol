package com.fourdotfive.missioncontrol.dtos.candidate;

public class CandidateCompareProfileDto {

	private String key;
	private String value;
	private Integer score;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

}
