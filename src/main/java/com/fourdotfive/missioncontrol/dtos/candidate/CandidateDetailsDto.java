package com.fourdotfive.missioncontrol.dtos.candidate;

import java.util.List;

public class CandidateDetailsDto {

	private String firstName;

	private String lastName;

	private String jobType;

	private String companyName;

	private List<CandidateAsset> assetList;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public List<CandidateAsset> getAssetList() {
		return assetList;
	}

	public void setAssetList(List<CandidateAsset> assetList) {
		this.assetList = assetList;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CandidateDetailsDto [firstName=");
		builder.append(firstName);
		builder.append(", lastName=");
		builder.append(lastName);
		builder.append(", jobType=");
		builder.append(jobType);
		builder.append(", companyName=");
		builder.append(companyName);
		builder.append(", assetList=");
		builder.append(assetList);
		builder.append("]");
		return builder.toString();
	}

}
