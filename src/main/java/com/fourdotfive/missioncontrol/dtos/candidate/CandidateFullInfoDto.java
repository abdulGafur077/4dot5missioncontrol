package com.fourdotfive.missioncontrol.dtos.candidate;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;

import com.fourdotfive.missioncontrol.dtos.job.JobProfileDto;
import com.fourdotfive.missioncontrol.pojo.job.JobMatch;

/**
 * @author Shalini
 */
public class CandidateFullInfoDto {
	
	private CandidateProfileDto candidateProfile;
	
	private JobProfileDto jobProfileDto;
	
	//TODO add job match info

	public CandidateProfileDto getCandidateProfile() {
		return candidateProfile;
	}

	public void setCandidateProfile(CandidateProfileDto candidateProfile) {
		this.candidateProfile = candidateProfile;
	}

	public JobProfileDto getJobProfileDto() {
		return jobProfileDto;
	}

	public void setJobProfileDto(JobProfileDto jobProfileDto) {
		this.jobProfileDto = jobProfileDto;
	}

	@Override
	public String toString() {
		return "CandidateFullInfoDto [candidateProfile=" + candidateProfile + ", jobProfileDto=" + jobProfileDto + "]";
	}
	
	public static CandidateFullInfoDto mapJobMatchToCandidateFullInfoDto(JobMatch jobMatch){
		
			CandidateFullInfoDto candidate = new CandidateFullInfoDto();
			
			candidate.setJobProfileDto(getJobProfileDto(jobMatch));
			candidate.setCandidateProfile(getCandidateProfileDto(jobMatch));
			
			return candidate;
	}
	
	public static CandidateProfileDto getCandidateProfileDto(JobMatch jobmatch){
		
		ModelMapper mapper = new ModelMapper();

		PropertyMap<JobMatch, CandidateProfileDto> profiledDto = new PropertyMap<JobMatch, CandidateProfileDto>() {

			@Override
			protected void configure() {
				map(source.getCandidate().getContact().getFirstname(), destination.getFirstName());
				map(source.getCandidate().getContact().getLastname(), destination.getLastName());
				map(source.getJob().getTitle(), destination.getRole());
				map(source.getJob().getCompany().getName(), destination.getCompanyName());
			}
		};
		mapper.createTypeMap(jobmatch, CandidateProfileDto.class).addMappings(profiledDto);
		return mapper.map(jobmatch, CandidateProfileDto.class);
		
	}
	
   public static JobProfileDto getJobProfileDto(JobMatch jobmatch){
		
		ModelMapper mapper = new ModelMapper();

		PropertyMap<JobMatch, JobProfileDto> profileDto = new PropertyMap<JobMatch, JobProfileDto>() {

			@Override
			protected void configure() {
				map(source.getJob().getTitle(), destination.getRole());
				map(source.getJob().getCompany().getName(), destination.getCompanyName());
				map(source.getJob().getEducationList(),destination.getEducationList());
				map(source.getJob().getExperienceList(), destination.getExperienceList());
				map(source.getJob().getTechnicalSkills(), destination.getTechnicalSkillList());
				map(source.getJob().getSoftSkills(), destination.getSoftSkillList());
				map(source.getJob().getCertifications(), destination.getCertificationList());
			}
		};
		mapper.createTypeMap(jobmatch, JobProfileDto.class).addMappings(profileDto);
		return mapper.map(jobmatch, JobProfileDto.class);
		
	}
}
