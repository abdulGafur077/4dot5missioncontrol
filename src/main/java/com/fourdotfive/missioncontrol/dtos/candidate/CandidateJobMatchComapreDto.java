package com.fourdotfive.missioncontrol.dtos.candidate;

import java.util.List;

public class CandidateJobMatchComapreDto {

	private String jobName;
	private String companyName;
	private List<JobComparetDto> companyRequirementDtos;

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public List<JobComparetDto> getCompanyRequirementDtos() {
		return companyRequirementDtos;
	}

	public void setCompanyRequirementDtos(
			List<JobComparetDto> companyRequirementDtos) {
		this.companyRequirementDtos = companyRequirementDtos;
	}

}
