package com.fourdotfive.missioncontrol.dtos.candidate;

import java.util.Date;

public class CandidateJobStateChange {

    private String jobMatchId;
    private String jobStepType;
    private String candidateJobStateType;
    private Date statusDate;
    private String note;
    private String modifiedBy;
    private Boolean isForward;
    private String loggedInUserId;

    public String getJobMatchId() {
        return jobMatchId;
    }

    public void setJobMatchId(String jobMatchId) {
        this.jobMatchId = jobMatchId;
    }

    public String getJobStepType() {
        return jobStepType;
    }

    public void setJobStepType(String jobStepType) {
        this.jobStepType = jobStepType;
    }

    public String getCandidateJobStateType() {
        return candidateJobStateType;
    }

    public void setCandidateJobStateType(String candidateJobStateType) {
        this.candidateJobStateType = candidateJobStateType;
    }

    public Date getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }


    public String getLoggedInUserId() {
        return loggedInUserId;
    }

    public void setLoggedInUserId(String loggedInUserId) {
        this.loggedInUserId = loggedInUserId;
    }

    public Boolean getIsForward() {
        return isForward;
    }

    public void setIsForward(Boolean forward) {
        isForward = forward;
    }
}
