package com.fourdotfive.missioncontrol.dtos.candidate;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CandidateMeetingEmailDeliveryInfo {

    @JsonProperty("isEmailSendToCandidate")
    private boolean isEmailSendToCandidate;
    private String candidateEmail;
    private String eventType;
    private String reason;

    public boolean isEmailSendToCandidate() {
        return isEmailSendToCandidate;
    }

    public void setEmailSendToCandidate(boolean emailSendToCandidate) {
        isEmailSendToCandidate = emailSendToCandidate;
    }

    public String getCandidateEmail() {
        return candidateEmail;
    }

    public void setCandidateEmail(String candidateEmail) {
        this.candidateEmail = candidateEmail;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
