package com.fourdotfive.missioncontrol.dtos.candidate;

import java.util.List;

public class CandidateMeetingScheduleDto {
    private String jobMatchId;
    private String note;
    private String company;
    private String startDateTime;
    private String endDateTime;
    private String loggedInUserId;
    private boolean isCompany;
    private String timeZoneName;
    private List<InterviewerDto> users;
    private String externalNote;
    private String feedbackType;

    public CandidateMeetingScheduleDto(MeetingScheduleDto meetingSchedule, String loggedInUserId) {
        this.company = meetingSchedule.getCompany();
        this.endDateTime = meetingSchedule.getEndDateTime();
        this.startDateTime = meetingSchedule.getStartDateTime();
        this.jobMatchId = meetingSchedule.getJobMatchId();
        this.note = meetingSchedule.getNote();
        this.loggedInUserId = loggedInUserId;
        this.isCompany = meetingSchedule.getIsCompany();
        this.timeZoneName = meetingSchedule.getTimeZoneName();
        this.users = meetingSchedule.getUsers();
        this.externalNote = meetingSchedule.getExternalNote();
        this.feedbackType = meetingSchedule.getFeedbackType();
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    public String getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
    }

    public String getJobMatchId() {
        return jobMatchId;
    }

    public void setJobMatchId(String jobMatchId) {
        this.jobMatchId = jobMatchId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getLoggedInUserId() {
        return loggedInUserId;
    }

    public void setLoggedInUserId(String loggedInUserId) {
        this.loggedInUserId = loggedInUserId;
    }

    public boolean getIsCompany() {
        return isCompany;
    }

    public void setIsCompany(boolean company) {
        isCompany = company;
    }

    public String getTimeZoneName() {
        return timeZoneName;
    }

    public void setTimeZoneName(String timeZoneName) {
        this.timeZoneName = timeZoneName;
    }

    public List<InterviewerDto> getUsers() {
        return users;
    }

    public void setUsers(List<InterviewerDto> users) {
        this.users = users;
    }

    public String getExternalNote() {
        return externalNote;
    }

    public void setExternalNote(String externalNote) {
        this.externalNote = externalNote;
    }

    public String getFeedbackType() {
        return feedbackType;
    }

    public void setFeedbackType(String feedbackType) {
        this.feedbackType = feedbackType;
    }
}
