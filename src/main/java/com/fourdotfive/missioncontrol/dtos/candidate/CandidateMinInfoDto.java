package com.fourdotfive.missioncontrol.dtos.candidate;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;

import com.fourdotfive.missioncontrol.pojo.candidate.Candidate;
import com.fourdotfive.missioncontrol.pojo.job.JobMatch;
import com.fourdotfive.missioncontrol.pojo.job.JobStateStep;

public class CandidateMinInfoDto {

	private String firstName;

	private String lastName;

	private String phoneNumber;

	private String email;

	private String im;

	private String lastContact;

	private String id;

	private String state;

	private List<String> activities;

	private List<JobMatchMinDto> jobMatches;

	public String getLastContact() {
		return lastContact;
	}

	public void setLastContact(String lastContact) {
		this.lastContact = lastContact;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIm() {
		return im;
	}

	public void setIm(String im) {
		this.im = im;
	}

	public List<JobMatchMinDto> getJobMatches() {
		return jobMatches;
	}

	public void setJobMatches(List<JobMatchMinDto> jobMatches) {
		this.jobMatches = jobMatches;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CandidateMinInfoDto [firstName=");
		builder.append(firstName);
		builder.append(", lastName=");
		builder.append(lastName);
		builder.append(", phoneNumber=");
		builder.append(phoneNumber);
		builder.append(", email=");
		builder.append(email);
		builder.append(", im=");
		builder.append(im);
		builder.append(", lastContact=");
		builder.append(lastContact);
		builder.append(", id=");
		builder.append(id);
		builder.append(", state=");
		builder.append(state);
		builder.append(", activities=");
		builder.append(activities);
		builder.append(", jobMatches=");
		builder.append(jobMatches);
		builder.append("]");
		return builder.toString();
	}

	public static List<CandidateMinInfoDto> getCandidateMinInfoDto(
			List<Candidate> candidates) {
		List<CandidateMinInfoDto> list = new ArrayList<CandidateMinInfoDto>();

		for (Candidate candidate : candidates) {
			CandidateMinInfoDto dto = new CandidateMinInfoDto();

			dto.setFirstName(candidate.getContact().getFirstname());
			dto.setLastName(candidate.getContact().getLastname());
			dto.setEmail(candidate.getContact().getEmail());
			dto.setPhoneNumber(candidate.getContact().getMobilephone());
			dto.setIm(candidate.getContact().getIm());

			for (JobMatch jobMatch : candidate.getJobMatchList()) {
				JobMatchMinDto jobDto = new JobMatchMinDto();

				jobDto.setJobTitle(jobMatch.getJob().getTitle());
				jobDto.setCompanyName(jobMatch.getJob().getCompany().getName());
				jobDto.setScore(jobMatch.getScore());

				List<JobStateStep> jobStateSteps = jobMatch.getJobStateSteps();
				DateTime curDate = new DateTime();
				for (JobStateStep jobStateStep : jobStateSteps) {

					DateTime date = jobStateStep.getLastModifiedDate()
							.getDateTime();
					if (date.isAfter(curDate)) {
						jobDto.setStatus(jobStateStep
								.getActiveCandidateJobStateType().toString());
					}
				}
			}

			list.add(dto);
		}

		return list;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public List<String> getActivities() {
		return activities;
	}

	public void setActivities(List<String> activities) {
		this.activities = activities;
	}

}
