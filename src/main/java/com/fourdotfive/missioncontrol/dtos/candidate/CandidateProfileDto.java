package com.fourdotfive.missioncontrol.dtos.candidate;

import java.util.List;

import com.fourdotfive.missioncontrol.dtos.requisition.EducationExperienceDto;
import com.fourdotfive.missioncontrol.dtos.requisition.MainSkillDto;

/**
 * @author Shalini
 */
public class CandidateProfileDto {

	private String firstName;

	private String lastName;

	private String description;

	private String role;

	private String companyName;

	private List<EducationExperienceDto> allEducationExperience;

	private List<MainSkillDto> skillList;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public List<MainSkillDto> getSkillList() {
		return skillList;
	}

	public void setSkillList(List<MainSkillDto> skillList) {
		this.skillList = skillList;
	}

	public List<EducationExperienceDto> getAllEducationExperience() {
		return allEducationExperience;
	}

	public void setAllEducationExperience(
			List<EducationExperienceDto> allEducationExperience) {
		this.allEducationExperience = allEducationExperience;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CandidateProfileDto [firstName=");
		builder.append(firstName);
		builder.append(", lastName=");
		builder.append(lastName);
		builder.append(", description=");
		builder.append(description);
		builder.append(", role=");
		builder.append(role);
		builder.append(", companyName=");
		builder.append(companyName);
		builder.append(", allEducationExperience=");
		builder.append(allEducationExperience);
		builder.append(", skillList=");
		builder.append(skillList);
		builder.append("]");
		return builder.toString();
	}

}
