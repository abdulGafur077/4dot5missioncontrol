package com.fourdotfive.missioncontrol.dtos.candidate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

//import org.joda.time.DateTime;

import com.fourdotfive.missioncontrol.pojo.candidate.Contact;
//import com.fourdotfive.missioncontrol.pojo.job.JobMatch;
import com.fourdotfive.missioncontrol.dtos.jobmatch.JobMatchMinInfoDto;


public class CandidateReqResumeMinInfoDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	//private DateTime createdDate;
	//private DateTime lastModifiedDate;
	private Contact contact;
	private List<JobMatchMinInfoDto> jobMatches = new ArrayList<>();
	private List<ActivePassiveResumeDto> resumes = new ArrayList<>();
//	private NotificationPayload notifications;
//
//	public NotificationPayload getNotifications() {
//		return notifications;
//	}
//
//	public void setNotifications(NotificationPayload notifications) {
//		this.notifications = notifications;
//	}

	public List<ActivePassiveResumeDto> getResumes() {
		return resumes;
	}

	public void setResumes(List<ActivePassiveResumeDto> resumes) {
		this.resumes = resumes;
	}

	/**
	 * get contact
	 * 
	 * @return the contact
	 *
	 */
	public Contact getContact() {
		return contact;
	}

	/**
	 * set contact
	 * 
	 * @param contact
	 *            the contact to set
	 *
	 */
	public void setContact(Contact contact) {
		this.contact = contact;
	}

	/**
	 * get jobMatches
	 * 
	 * @return the jobMatches
	 *
	 */
	public List<JobMatchMinInfoDto> getJobMatches() {
		return jobMatches;
	}

	/**
	 * set jobMatches
	 * 
	 * @param jobMatches
	 *            the jobMatches to set
	 *
	 */
	public void setJobMatches(List<JobMatchMinInfoDto> jobMatches) {
		this.jobMatches = jobMatches;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

//	public DateTime getCreatedDate() {
//		return createdDate;
//	}
//
//	public void setCreatedDate(DateTime createdDate) {
//		this.createdDate = createdDate;
//	}
//
//	public DateTime getLastModifiedDate() {
//		return lastModifiedDate;
//	}
//
//	public void setLastModifiedDate(DateTime lastModifiedDate) {
//		this.lastModifiedDate = lastModifiedDate;
//	}

}
