package com.fourdotfive.missioncontrol.dtos.candidate;

import java.util.List;

import org.joda.time.DateTime;

import com.fourdotfive.missioncontrol.common.DateRange;
import com.fourdotfive.missioncontrol.pojo.candidate.CandidateState;

/**
 * @author Shalini
 */

public class CandidateRequestDto {
	
	private CandidateState candidateState;
	
	private DateRange dateRange;
	
	private DateTime fromDate;
	
	private DateTime toDate;
	
	private List<String> clientOrBuList;
	
	private List<String> recruiterList;
	
	private List<String> jobReqList;
	
	private String requisitionNo;
	
	private List<String> states;
	
	private String searchString;
	
	//Do we add page information here
	
	public CandidateRequestDto() {
		// TODO Auto-generated constructor stub
	}

	public CandidateState getCandidateState() {
		return candidateState;
	}

	public void setCandidateState(CandidateState candidateState) {
		this.candidateState = candidateState;
	}

	public DateRange getDateRange() {
		return dateRange;
	}

	public void setDateRange(DateRange dateRange) {
		this.dateRange = dateRange;
	}

	public DateTime getFromDate() {
		return fromDate;
	}

	public void setFromDate(DateTime fromDate) {
		this.fromDate = fromDate;
	}

	public DateTime getToDate() {
		return toDate;
	}

	public void setToDate(DateTime toDate) {
		this.toDate = toDate;
	}

	public List<String> getClientOrBuList() {
		return clientOrBuList;
	}

	public void setClientOrBuList(List<String> clientOrBuList) {
		this.clientOrBuList = clientOrBuList;
	}

	public List<String> getRecruiterList() {
		return recruiterList;
	}

	public void setRecruiterList(List<String> recruiterList) {
		this.recruiterList = recruiterList;
	}

	public List<String> getJobReqList() {
		return jobReqList;
	}

	public void setJobReqList(List<String> jobReqList) {
		this.jobReqList = jobReqList;
	}

	public String getRequisitionNo() {
		return requisitionNo;
	}

	public void setRequisitionNo(String requisitionNo) {
		this.requisitionNo = requisitionNo;
	}

	public List<String> getStates() {
		return states;
	}

	public void setStates(List<String> states) {
		this.states = states;
	}

	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

	@Override
	public String toString() {
		return "CandidateRequestDto [candidateState=" + candidateState + ", dateRange=" + dateRange + ", fromDate="
				+ fromDate + ", toDate=" + toDate + ", clientOrBuList=" + clientOrBuList + ", recruiterList="
				+ recruiterList + ", jobReqList=" + jobReqList + ", requisitionNo=" + requisitionNo + ", states="
				+ states + ", searchString=" + searchString + "]";
	}
	
}
