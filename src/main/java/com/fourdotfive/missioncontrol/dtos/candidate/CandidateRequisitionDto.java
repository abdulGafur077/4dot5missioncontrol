package com.fourdotfive.missioncontrol.dtos.candidate;

public class CandidateRequisitionDto {
    private String requisitionId;
    private String companyId;
    private String searchText;
    private int page;
    private int size;
    private String sortDirection;
    private double minimumScore;
    private double maximumScore;
    private int numberOfDays;
    private boolean withoutJobMatchScore;

    public String getRequisitionId() {
        return requisitionId;
    }

    public void setRequisitionId(String requisitionId) {
        this.requisitionId = requisitionId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getSortDirection() {
        return sortDirection;
    }

    public void setSortDirection(String sortDirection) {
        this.sortDirection = sortDirection;
    }

    public double getMinimumScore() {
        return minimumScore;
    }

    public void setMinimumScore(double minimumScore) {
        this.minimumScore = minimumScore;
    }

    public double getMaximumScore() {
        return maximumScore;
    }

    public void setMaximumScore(double maximumScore) {
        this.maximumScore = maximumScore;
    }

    public int getNumberOfDays() {
        return numberOfDays;
    }

    public void setNumberOfDays(int numberOfDays) {
        this.numberOfDays = numberOfDays;
    }

    public boolean isWithoutJobMatchScore() {
        return withoutJobMatchScore;
    }

    public void setWithoutJobMatchScore(boolean withoutJobMatchScore) {
        this.withoutJobMatchScore = withoutJobMatchScore;
    }
}
