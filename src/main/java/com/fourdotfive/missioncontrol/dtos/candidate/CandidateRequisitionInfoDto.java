package com.fourdotfive.missioncontrol.dtos.candidate;

import java.util.List;
import java.util.Map;

public class CandidateRequisitionInfoDto {
    private String requisitionId;
    private List<String> candidateIds;
    private String loggedInUserId;
    private String jbiTransactionId;
    private String transactionId;
    private boolean manuallyMatchedFromUI;
    private String companyId;
    private boolean isAddCandidate;
    private boolean isAssociateCandidate;
    private List<OverrideNoteDTO> overrideNoteDetails;

    public String getRequisitionId() {
        return requisitionId;
    }

    public void setRequisitionId(String requisitionId) {
        this.requisitionId = requisitionId;
    }

    public List<String> getCandidateIds() {
        return candidateIds;
    }

    public void setCandidateIds(List<String> candidateIds) {
        this.candidateIds = candidateIds;
    }

    public String getLoggedInUserId() {
        return loggedInUserId;
    }

    public void setLoggedInUserId(String loggedInUserId) {
        this.loggedInUserId = loggedInUserId;
    }

    public String getJbiTransactionId() {
        return jbiTransactionId;
    }

    public void setJbiTransactionId(String jbiTransactionId) {
        this.jbiTransactionId = jbiTransactionId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public boolean isManuallyMatchedFromUI() {
        return manuallyMatchedFromUI;
    }

    public void setManuallyMatchedFromUI(boolean manuallyMatchedFromUI) {
        this.manuallyMatchedFromUI = manuallyMatchedFromUI;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public boolean getIsAddCandidate() {
        return isAddCandidate;
    }

    public void setAddCandidate(boolean addCandidate) {
        isAddCandidate = addCandidate;
    }

    public boolean isAssociateCandidate() {
        return isAssociateCandidate;
    }

    public void setAssociateCandidate(boolean associateCandidate) {
        isAssociateCandidate = associateCandidate;
    }

    public boolean isAddCandidate() {
        return isAddCandidate;
    }

    public List<OverrideNoteDTO> getOverrideNoteDetails() {
        return overrideNoteDetails;
    }

    public void setOverrideNoteDetails(List<OverrideNoteDTO> overrideNoteDetails) {
        this.overrideNoteDetails = overrideNoteDetails;
    }
}
