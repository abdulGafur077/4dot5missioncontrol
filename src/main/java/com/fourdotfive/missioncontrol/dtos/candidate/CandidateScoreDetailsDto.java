package com.fourdotfive.missioncontrol.dtos.candidate;

public class CandidateScoreDetailsDto {

    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private String candidateStatus;
    private String bestMatchJobTitle;
    private double bestMatchFourDotFiveIntelligenceScore;
    private double currentJobMatchScore;
    private double currentJobThresholdScore;
    private int noOfExistingJobMatches;
    private boolean isMultipleRolesMatchEnabled;
    private boolean hasJobMatchUnderTheSameClient;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCandidateStatus() {
        return candidateStatus;
    }

    public void setCandidateStatus(String candidateStatus) {
        this.candidateStatus = candidateStatus;
    }

    public String getBestMatchJobTitle() {
        return bestMatchJobTitle;
    }

    public void setBestMatchJobTitle(String bestMatchJobTitle) {
        this.bestMatchJobTitle = bestMatchJobTitle;
    }

    public double getBestMatchFourDotFiveIntelligenceScore() {
        return bestMatchFourDotFiveIntelligenceScore;
    }

    public void setBestMatchFourDotFiveIntelligenceScore(double bestMatchFourDotFiveIntelligenceScore) {
        this.bestMatchFourDotFiveIntelligenceScore = bestMatchFourDotFiveIntelligenceScore;
    }

    public double getCurrentJobMatchScore() {
        return currentJobMatchScore;
    }

    public void setCurrentJobMatchScore(double currentJobMatchScore) {
        this.currentJobMatchScore = currentJobMatchScore;
    }

    public double getCurrentJobThresholdScore() {
        return currentJobThresholdScore;
    }

    public void setCurrentJobThresholdScore(double currentJobThresholdScore) {
        this.currentJobThresholdScore = currentJobThresholdScore;
    }

    public int getNoOfExistingJobMatches() {
        return noOfExistingJobMatches;
    }

    public void setNoOfExistingJobMatches(int noOfExistingJobMatches) {
        this.noOfExistingJobMatches = noOfExistingJobMatches;
    }

    public boolean isMultipleRolesMatchEnabled() {
        return isMultipleRolesMatchEnabled;
    }

    public void setMultipleRolesMatchEnabled(boolean multipleRolesMatchEnabled) {
        isMultipleRolesMatchEnabled = multipleRolesMatchEnabled;
    }

    public boolean isHasJobMatchUnderTheSameClient() {
        return hasJobMatchUnderTheSameClient;
    }

    public void setHasJobMatchUnderTheSameClient(boolean hasJobMatchUnderTheSameClient) {
        this.hasJobMatchUnderTheSameClient = hasJobMatchUnderTheSameClient;
    }
}
