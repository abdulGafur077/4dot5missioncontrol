package com.fourdotfive.missioncontrol.dtos.candidate;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;

import com.fourdotfive.missioncontrol.pojo.candidate.Candidate;
import com.fourdotfive.missioncontrol.pojo.job.JobMatch;

public class CandidateStateMinInfoDto {
	
	private String firstName;
	
	private String lastName;
	
	private String jobRequisitionRole;
	
	private String companyname;
	
	private String phonenNumber;
	
	private String email;
	
	/*private State state;
	
	private SubState subState;*/
	
	private float jobMatchScore;
	
	private float techAssessmentScore;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getJobRequisitionRole() {
		return jobRequisitionRole;
	}

	public void setJobRequisitionRole(String jobRequisitionRole) {
		this.jobRequisitionRole = jobRequisitionRole;
	}

	public String getCompanyname() {
		return companyname;
	}

	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}

	public String getPhonenNumber() {
		return phonenNumber;
	}

	public void setPhonenNumber(String phonenNumber) {
		this.phonenNumber = phonenNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public float getJobMatchScore() {
		return jobMatchScore;
	}

	public void setJobMatchScore(float jobMatchScore) {
		this.jobMatchScore = jobMatchScore;
	}

	public float getTechAssessmentScore() {
		return techAssessmentScore;
	}

	public void setTechAssessmentScore(float techAssessmentScore) {
		this.techAssessmentScore = techAssessmentScore;
	}

	@Override
	public String toString() {
		return "CandidateMinInfoDto [firstName=" + firstName + ", lastName=" + lastName + ", jobRequisitionRole="
				+ jobRequisitionRole + ", companyname=" + companyname + ", phonenNumber=" + phonenNumber + ", email="
				+ email + ", jobMatchScore=" + jobMatchScore + ", techAssessmentScore=" + techAssessmentScore + "]";
	}

	public static List<CandidateStateMinInfoDto> mapCandidatetoCandidateMinDto(List<Candidate> candidateList) {

		ArrayList<CandidateStateMinInfoDto> list = new ArrayList<CandidateStateMinInfoDto>();

		for (Candidate candidate : candidateList) {
			ModelMapper mapper = new ModelMapper();
			PropertyMap<Candidate, CandidateStateMinInfoDto> candidateMap = new PropertyMap<Candidate, CandidateStateMinInfoDto>() {

				@Override
				protected void configure() {
					// personal details
					//map(source., destination.getUserId());
					map(source.getContact().getFirstname(),destination.getFirstName());
					map(source.getContact().getLastname(),destination.getLastName());
					map(source.getContact().getMobilephone(),destination.getPhonenNumber());
					map(source.getContact().getEmail(),destination.getEmail());
					//TODO need to get the correct job match
					JobMatch jobMatch = source.getJobMatchList().get(0);
					map(jobMatch.getJob().getCompany().getName(),destination.getCompanyname());
					map(jobMatch.getJob().getTitle(), destination.getJobRequisitionRole());
					map(jobMatch.getScore(), destination.getJobMatchScore());
				}
			};
			mapper.createTypeMap(candidate, CandidateStateMinInfoDto.class).addMappings(candidateMap);
			CandidateStateMinInfoDto dto = mapper.map(candidate, CandidateStateMinInfoDto.class);
			list.add(dto);
		}

		return list;
	}
	
}
