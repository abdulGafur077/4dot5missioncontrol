package com.fourdotfive.missioncontrol.dtos.candidate;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class CandidateStatusCountDto {

	private int activeCandidatesCount = 0;
	private int passiveCandidatesCount = 0;
	private int totalCandidatesCount = 0;
	private LinkedHashMap<String, Integer> activeJobTypesCandidateCount;
	private LinkedHashMap<String, Integer> hiredYTDJobTypeCandidateCount;

	public int getActiveCandidatesCount() {
		return activeCandidatesCount;
	}

	public void setActiveCandidatesCount(int activeCandidatesCount) {
		this.activeCandidatesCount = activeCandidatesCount;
	}

	public int getPassiveCandidatesCount() {
		return passiveCandidatesCount;
	}

	public void setPassiveCandidatesCount(int passiveCandidatesCount) {
		this.passiveCandidatesCount = passiveCandidatesCount;
	}

	public int getTotalCandidatesCount() {
		return totalCandidatesCount;
	}

	public void setTotalCandidatesCount(int totalCandidatesCount) {
		this.totalCandidatesCount = totalCandidatesCount;
	}

	public HashMap<String, Integer> getActiveJobTypesCandidateCount() {
		return activeJobTypesCandidateCount;
	}

	public void setActiveJobTypesCandidateCount(LinkedHashMap<String, Integer> activeJobTypesCandidateCount) {
		this.activeJobTypesCandidateCount = activeJobTypesCandidateCount;
	}

	public LinkedHashMap<String, Integer> getHiredYTDJobTypeCandidateCount() {
		return hiredYTDJobTypeCandidateCount;
	}

	public void setHiredYTDJobTypeCandidateCount(LinkedHashMap<String, Integer> hiredYTDJobTypeCandidateCount) {
		this.hiredYTDJobTypeCandidateCount = hiredYTDJobTypeCandidateCount;
	}
}
