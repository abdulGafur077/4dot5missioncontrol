package com.fourdotfive.missioncontrol.dtos.candidate;

public class CandidateUploadResumeMinInfoDto {
    private String candidateId;
    private String candidateName;

    public String getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(String candidateId) {
        this.candidateId = candidateId;
    }

    public String getCandidateName() {
        return candidateName;
    }

    public void setCandidateName(String candidateName) {
        this.candidateName = candidateName;
    }
}
