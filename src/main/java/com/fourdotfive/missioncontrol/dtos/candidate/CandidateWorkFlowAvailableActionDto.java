package com.fourdotfive.missioncontrol.dtos.candidate;

public class CandidateWorkFlowAvailableActionDto {
	private String actionType;
	private String action;
	private boolean isAllowed;
	private String message;

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public boolean isAllowed() {
		return isAllowed;
	}

	public void setAllowed(boolean allowed) {
		isAllowed = allowed;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
