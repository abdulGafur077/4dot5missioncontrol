package com.fourdotfive.missioncontrol.dtos.candidate;

import com.fourdotfive.missioncontrol.dtos.job.RecruiterDetailDto;
import com.fourdotfive.missioncontrol.dtos.jobmatch.PhoneScreenInterviewEmailDeliveryInfo;

import java.util.List;
import java.util.Map;

public class CandidateWorkflowCardDto {

    /* Display the Photo, Name, Contact(Telephone #, Msg Icon, Email) Role, Job Match Date, Req #,
    # of current openings, Client or BU, State,
    Days since resume has been uploaded, Score, Recruiter(s) and Action Buttons */

    /**
     *
     */

    private static final long serialVersionUID = 1L;
    private String id;
    private String firstName;
    private String lastName;
    private String workphone;
    private String mobilephone;
    private String email;
    private String profileUrl;
    private String profileType;
    private String im;
    private String jobMatchTitle;
    private String jobMatchRequisitionNum;
    private String jobMatchRequisitionStatus;
    private String jobMatchJobId;
    private Double jobMatchScore;
    private List<RecruiterDetailDto> recruitersList;
    private String jobMatchClientId;
    private String jobMatchClientName;
    private String jobMatchCurrentState;
    private String jobMatchCompanyId;
    //private DateTime lastUploadedDate;
    private int noOfDaysSinceLastUpload;
    private String jobMatchDate;
    private String candidateStatus;
    private String jobMatchStatus;
    private int numberOfFilledOpenings;
    private int totalOpenings;
    private Map<String, String> actions;
    private List<CandidateWorkFlowAvailableActionDto> availableActions;
    private Double fourDotFiveIntelligenceScore;
    private Double techAssessmentScore;
    private Double valueAssessmentScore;
    private Double techAssessmentThresholdScore;
    private String techAssessmentPdfReportURL;
    private String valueAssessmentPdfReportURL;
    private int numberOfOpenPhoneScreens;
    private int numberOfOpenInterviews;
    private String jobState;
    private String filledDate;
    private String joinedDate;
    private Boolean isConflictPhoneNumber;
    private Boolean isConflictEmail;
    private Boolean isInCompletePhoneNumber;
    private Boolean isInCompleteEmail;
    private Boolean isManualMatch;
    private Long testInvitationId;
    private Integer noOfDaysInCurrentStep;
    private boolean isReadOnly;
    private String otherSourceTypeNotes;
    private String referrerName;
    private String referrerEmail;
    private boolean isFromOtherSource;
    private boolean isReferred;
    private boolean isCommunicationExist;
    private boolean isNotesExist;
    private boolean isFilledOrJoinedInOtherJob;
    private String vendorName;
    private boolean isSharedRequisition;
    private boolean isLastStep;
    private boolean isVendorLevelRequisition;
    private boolean isCorporateLevelRequisition;
    private boolean  isClientOrBuLevelRequisition;
    private String corporateCompanyName;
    private String clientOrBUName;
    private String corporateCompanyId;
    private String clientOrBUId;
    private FilledOrJoinedRequisitionDetailDTO filledOrJoinedRequisitionDetails;
    private String resumeFileName;
    private String techAssessmentName;
    private String valueAssessmentName;
    private boolean isAssociated;
    private String reason;
    private String currentStep;
    private PhoneScreenInterviewEmailDeliveryInfo phoneScreenInterviewEmailDeliveryInfo;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getWorkphone() {
        return workphone;
    }

    public void setWorkphone(String workphone) {
        this.workphone = workphone;
    }

    public String getMobilephone() {
        return mobilephone;
    }

    public void setMobilephone(String mobilephone) {
        this.mobilephone = mobilephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIm() {
        return im;
    }

    public void setIm(String im) {
        this.im = im;
    }

    public String getJobMatchTitle() {
        return jobMatchTitle;
    }

    public void setJobMatchTitle(String jobMatchTitle) {
        this.jobMatchTitle = jobMatchTitle;
    }

    public String getJobMatchRequisitionNum() {
        return jobMatchRequisitionNum;
    }

    public void setJobMatchRequisitionNum(String jobMatchRequisitionNum) {
        this.jobMatchRequisitionNum = jobMatchRequisitionNum;
    }

    public Double getJobMatchScore() {
        return jobMatchScore;
    }

    public void setJobMatchScore(Double jobMatchScore) {
        this.jobMatchScore = jobMatchScore;
    }

    public List<RecruiterDetailDto> getRecruitersList() {
        return recruitersList;
    }

    public void setRecruitersList(List<RecruiterDetailDto> recruitersList) {
        this.recruitersList = recruitersList;
    }

    public String getJobMatchClientName() {
        return jobMatchClientName;
    }

    public void setJobMatchClientName(String jobMatchClientName) {
        this.jobMatchClientName = jobMatchClientName;
    }

    public String getJobMatchCurrentState() {
        return jobMatchCurrentState;
    }

    public void setJobMatchCurrentState(String jobMatchCurrentState) {
        this.jobMatchCurrentState = jobMatchCurrentState;
    }

    public String getJobMatchCompanyId() {
        return jobMatchCompanyId;
    }

    public void setJobMatchCompanyId(String jobMatchCompanyId) {
        this.jobMatchCompanyId = jobMatchCompanyId;
    }

    //    public DateTime getLastUploadedDate() {
//        return lastUploadedDate;
//    }
//
//    public void setLastUploadedDate(DateTime lastUploadedDate) {
//        this.lastUploadedDate = lastUploadedDate;
//    }

    public int getNoOfDaysSinceLastUpload() {
        return noOfDaysSinceLastUpload;
    }

    public void setNoOfDaysSinceLastUpload(int noOfDaysSinceLastUpload) {
        this.noOfDaysSinceLastUpload = noOfDaysSinceLastUpload;
    }

    public String getJobMatchDate() {
        return jobMatchDate;
    }

    public void setJobMatchDate(String jobMatchDate) {
        this.jobMatchDate = jobMatchDate;
    }

    public String getCandidateStatus() {
        return candidateStatus;
    }

    public void setCandidateStatus(String candidateStatus) {
        this.candidateStatus = candidateStatus;
    }

    public int getNumberOfFilledOpenings() {
        return numberOfFilledOpenings;
    }

    public void setNumberOfFilledOpenings(int numberOfFilledOpenings) {
        this.numberOfFilledOpenings = numberOfFilledOpenings;
    }

    public String getJobMatchRequisitionStatus() {
        return jobMatchRequisitionStatus;
    }

    public void setJobMatchRequisitionStatus(String jobMatchRequisitionStatus) {
        this.jobMatchRequisitionStatus = jobMatchRequisitionStatus;
    }

    public String getJobMatchJobId() {
        return jobMatchJobId;
    }

    public void setJobMatchJobId(String jobMatchJobId) {
        this.jobMatchJobId = jobMatchJobId;
    }

    public int getTotalOpenings() {
        return totalOpenings;
    }

    public void setTotalOpenings(int totalOpenings) {
        this.totalOpenings = totalOpenings;
    }

    public Double getFourDotFiveIntelligenceScore() {
        return fourDotFiveIntelligenceScore;
    }

    public void setFourDotFiveIntelligenceScore(Double fourDotFiveIntelligenceScore) {
        this.fourDotFiveIntelligenceScore = fourDotFiveIntelligenceScore;
    }

    public Double getTechAssessmentScore() {
        return techAssessmentScore;
    }

    public void setTechAssessmentScore(Double techAssessmentScore) {
        this.techAssessmentScore = techAssessmentScore;
    }

    public Double getValueAssessmentScore() {
        return valueAssessmentScore;
    }

    public void setValueAssessmentScore(Double valueAssessmentScore) {
        this.valueAssessmentScore = valueAssessmentScore;
    }

    public Double getTechAssessmentThresholdScore() {
        return techAssessmentThresholdScore;
    }

    public void setTechAssessmentThresholdScore(Double techAssessmentThresholdScore) {
        this.techAssessmentThresholdScore = techAssessmentThresholdScore;
    }

    public String getTechAssessmentPdfReportURL() {
        return techAssessmentPdfReportURL;
    }

    public void setTechAssessmentPdfReportURL(String techAssessmentPdfReportURL) {
        this.techAssessmentPdfReportURL = techAssessmentPdfReportURL;
    }

    public String getValueAssessmentPdfReportURL() {
        return valueAssessmentPdfReportURL;
    }

    public void setValueAssessmentPdfReportURL(String valueAssessmentPdfReportURL) {
        this.valueAssessmentPdfReportURL = valueAssessmentPdfReportURL;
    }

    public Boolean getManualMatch() {
        return isManualMatch;
    }

    public void setManualMatch(Boolean manualMatch) {
        isManualMatch = manualMatch;
    }

    public Integer getNoOfDaysInCurrentStep() {
        return noOfDaysInCurrentStep;
    }

    public void setNoOfDaysInCurrentStep(Integer noOfDaysInCurrentStep) {
        this.noOfDaysInCurrentStep = noOfDaysInCurrentStep;
    }

    public Map<String, String> getActions() {
        return actions;
    }

    public void setActions(Map<String, String> actions) {
        this.actions = actions;
    }

    public String getJobMatchStatus() {
        return jobMatchStatus;
    }

    public void setJobMatchStatus(String jobMatchStatus) {
        this.jobMatchStatus = jobMatchStatus;
    }

    public int getNumberOfOpenPhoneScreens() {
        return numberOfOpenPhoneScreens;
    }

    public void setNumberOfOpenPhoneScreens(int numberOfOpenPhoneScreens) {
        this.numberOfOpenPhoneScreens = numberOfOpenPhoneScreens;
    }

    public int getNumberOfOpenInterviews() {
        return numberOfOpenInterviews;
    }

    public void setNumberOfOpenInterviews(int numberOfOpenInterviews) {
        this.numberOfOpenInterviews = numberOfOpenInterviews;
    }

    public String getJobMatchClientId() {
        return jobMatchClientId;
    }

    public void setJobMatchClientId(String jobMatchClientId) {
        this.jobMatchClientId = jobMatchClientId;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public String getJobState() {
        return jobState;
    }

    public void setJobState(String jobState) {
        this.jobState = jobState;
    }

    public String getFilledDate() {
        return filledDate;
    }

    public void setFilledDate(String filledDate) {
        this.filledDate = filledDate;
    }

    public String getJoinedDate() {
        return joinedDate;
    }

    public void setJoinedDate(String joinedDate) {
        this.joinedDate = joinedDate;
    }

    public Boolean getConflictPhoneNumber() {
        return isConflictPhoneNumber;
    }

    public void setConflictPhoneNumber(Boolean conflictPhoneNumber) {
        isConflictPhoneNumber = conflictPhoneNumber;
    }

    public Boolean getConflictEmail() {
        return isConflictEmail;
    }

    public void setConflictEmail(Boolean conflictEmail) {
        isConflictEmail = conflictEmail;
    }

    public Boolean getInCompletePhoneNumber() {
        return isInCompletePhoneNumber;
    }

    public void setInCompletePhoneNumber(Boolean inCompletePhoneNumber) {
        isInCompletePhoneNumber = inCompletePhoneNumber;
    }

    public Boolean getInCompleteEmail() {
        return isInCompleteEmail;
    }

    public void setInCompleteEmail(Boolean inCompleteEmail) {
        isInCompleteEmail = inCompleteEmail;
    }

    public String getProfileType() {
        return profileType;
    }

    public void setProfileType(String profileType) {
        this.profileType = profileType;
    }

    public List<CandidateWorkFlowAvailableActionDto> getAvailableActions() {
        return availableActions;
    }

    public void setAvailableActions(List<CandidateWorkFlowAvailableActionDto> availableActions) {
        this.availableActions = availableActions;
    }

    public Long getTestInvitationId() {
        return testInvitationId;
    }

    public void setTestInvitationId(Long testInvitationId) {
        this.testInvitationId = testInvitationId;
    }

    public boolean isReadOnly() {
        return isReadOnly;
    }

    public void setReadOnly(boolean readOnly) {
        isReadOnly = readOnly;
    }

    public String getOtherSourceTypeNotes() {
        return otherSourceTypeNotes;
    }

    public void setOtherSourceTypeNotes(String otherSourceTypeNotes) {
        this.otherSourceTypeNotes = otherSourceTypeNotes;
    }

    public String getReferrerName() {
        return referrerName;
    }

    public void setReferrerName(String referrerName) {
        this.referrerName = referrerName;
    }

    public String getReferrerEmail() {
        return referrerEmail;
    }

    public void setReferrerEmail(String referrerEmail) {
        this.referrerEmail = referrerEmail;
    }

    public boolean isFromOtherSource() {
        return isFromOtherSource;
    }

    public void setFromOtherSource(boolean fromOtherSource) {
        isFromOtherSource = fromOtherSource;
    }

    public boolean isReferred() {
        return isReferred;
    }

    public void setReferred(boolean referred) {
        isReferred = referred;
    }

    public boolean isCommunicationExist() {
        return isCommunicationExist;
    }

    public void setCommunicationExist(boolean communicationExist) {
        isCommunicationExist = communicationExist;
    }

    public boolean isNotesExist() {
        return isNotesExist;
    }

    public void setNotesExist(boolean notesExist) {
        isNotesExist = notesExist;
    }

    public boolean isFilledOrJoinedInOtherJob() {
        return isFilledOrJoinedInOtherJob;
    }

    public void setFilledOrJoinedInOtherJob(boolean filledOrJoinedInOtherJob) {
        isFilledOrJoinedInOtherJob = filledOrJoinedInOtherJob;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public boolean getIsSharedRequisition() {
        return isSharedRequisition;
    }

    public void setIsSharedRequisition(boolean sharedRequisition) {
        isSharedRequisition = sharedRequisition;
    }

    public boolean getIsLastStep() {
        return isLastStep;
    }

    public void setIsLastStep(boolean lastStep) {
        isLastStep = lastStep;
    }

    public boolean getIsVendorLevelRequisition() {
        return isVendorLevelRequisition;
    }

    public void setIsVendorLevelRequisition(boolean vendorLevelRequisition) {
        isVendorLevelRequisition = vendorLevelRequisition;
    }

    public boolean getIsCorporateLevelRequisition() {
        return isCorporateLevelRequisition;
    }

    public void setIsCorporateLevelRequisition(boolean corporateLevelRequisition) {
        isCorporateLevelRequisition = corporateLevelRequisition;
    }

    public boolean getIsClientOrBuLevelRequisition() {
        return isClientOrBuLevelRequisition;
    }

    public void setIsClientOrBuLevelRequisition(boolean clientOrBuLevelRequisition) {
        isClientOrBuLevelRequisition = clientOrBuLevelRequisition;
    }

    public String getCorporateCompanyName() {
        return corporateCompanyName;
    }

    public void setCorporateCompanyName(String corporateCompanyName) {
        this.corporateCompanyName = corporateCompanyName;
    }

    public String getClientOrBUName() {
        return clientOrBUName;
    }

    public void setClientOrBUName(String clientOrBUName) {
        this.clientOrBUName = clientOrBUName;
    }

    public String getCorporateCompanyId() {
        return corporateCompanyId;
    }

    public void setCorporateCompanyId(String corporateCompanyId) {
        this.corporateCompanyId = corporateCompanyId;
    }

    public String getClientOrBUId() {
        return clientOrBUId;
    }

    public void setClientOrBUId(String clientOrBUId) {
        this.clientOrBUId = clientOrBUId;
    }

    public FilledOrJoinedRequisitionDetailDTO getFilledOrJoinedRequisitionDetails() {
        return filledOrJoinedRequisitionDetails;
    }

    public void setFilledOrJoinedRequisitionDetails(FilledOrJoinedRequisitionDetailDTO filledOrJoinedRequisitionDetails) {
        this.filledOrJoinedRequisitionDetails = filledOrJoinedRequisitionDetails;
    }

    public String getResumeFileName() {
        return resumeFileName;
    }

    public void setResumeFileName(String resumeFileName) {
        this.resumeFileName = resumeFileName;
    }

    public String getTechAssessmentName() {
        return techAssessmentName;
    }

    public void setTechAssessmentName(String techAssessmentName) {
        this.techAssessmentName = techAssessmentName;
    }

    public String getValueAssessmentName() {
        return valueAssessmentName;
    }

    public void setValueAssessmentName(String valueAssessmentName) {
        this.valueAssessmentName = valueAssessmentName;
    }

    public boolean isAssociated() {
        return isAssociated;
    }

    public void setAssociated(boolean associated) {
        isAssociated = associated;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getCurrentStep() {
        return currentStep;
    }

    public void setCurrentStep(String currentStep) {
        this.currentStep = currentStep;
    }

    public PhoneScreenInterviewEmailDeliveryInfo getPhoneScreenInterviewEmailDeliveryInfo() {
        return phoneScreenInterviewEmailDeliveryInfo;
    }

    public void setPhoneScreenInterviewEmailDeliveryInfo(PhoneScreenInterviewEmailDeliveryInfo phoneScreenInterviewEmailDeliveryInfo) {
        this.phoneScreenInterviewEmailDeliveryInfo = phoneScreenInterviewEmailDeliveryInfo;
    }

    @Override
    public String toString() {
        return "CandidateWorkflowCardDto{" +
                "id='" + id + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", workphone='" + workphone + '\'' +
                ", mobilephone='" + mobilephone + '\'' +
                ", email='" + email + '\'' +
                ", im='" + im + '\'' +
                ", jobMatchTitle='" + jobMatchTitle + '\'' +
                ", jobMatchRequisitionNum='" + jobMatchRequisitionNum + '\'' +
                ", jobMatchRequisitionStatus='" + jobMatchRequisitionStatus + '\'' +
                ", jobMatchJobId='" + jobMatchJobId + '\'' +
                ", jobMatchScore=" + jobMatchScore +
                ", recruitersList=" + recruitersList +
                ", jobMatchClientId='" + jobMatchClientId + '\'' +
                ", jobMatchClientName='" + jobMatchClientName + '\'' +
                ", jobMatchCurrentState='" + jobMatchCurrentState + '\'' +
                ", noOfDaysSinceLastUpload=" + noOfDaysSinceLastUpload +
                ", jobMatchDate='" + jobMatchDate + '\'' +
                ", candidateStatus='" + candidateStatus + '\'' +
                ", jobMatchStatus='" + jobMatchStatus + '\'' +
                ", numberOfFilledOpenings=" + numberOfFilledOpenings +
                ", totalOpenings=" + totalOpenings +
                ", actions=" + actions +
                ", fourDotFiveIntelligenceScore=" + fourDotFiveIntelligenceScore +
                ", techAssessmentScore=" + techAssessmentScore +
                ", valueAssessmentScore=" + valueAssessmentScore +
                ", techAssessmentThresholdScore=" + techAssessmentThresholdScore +
                ", techAssessmentPdfReportURL='" + techAssessmentPdfReportURL + '\'' +
                ", valueAssessmentPdfReportURL='" + valueAssessmentPdfReportURL + '\'' +
                ", numberOfOpenPhoneScreens=" + numberOfOpenPhoneScreens +
                ", numberOfOpenInterviews=" + numberOfOpenInterviews +
                ", isManualMatch =" + isManualMatch +
                ", noOfDaysInCurrentStep =" + noOfDaysInCurrentStep +
                '}';
    }
}
