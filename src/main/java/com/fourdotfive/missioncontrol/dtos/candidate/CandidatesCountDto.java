package com.fourdotfive.missioncontrol.dtos.candidate;

import java.util.HashMap;

public class CandidatesCountDto {
    private String userId;
    private int activeCandidatesCount = 0;
    private int passiveCandidatesCount = 0;
    private HashMap<String, Integer> activeJobTypesCandidateCount = new HashMap<>();
    private  HashMap<String, Integer> hiredYTDJobTypeCandidateCount = new HashMap<>();


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getActiveCandidatesCount() {
        return activeCandidatesCount;
    }

    public void setActiveCandidatesCount(int activeCandidatesCount) {
        this.activeCandidatesCount = activeCandidatesCount;
    }

    public int getPassiveCandidatesCount() {
        return passiveCandidatesCount;
    }

    public void setPassiveCandidatesCount(int passiveCandidatesCount) {
        this.passiveCandidatesCount = passiveCandidatesCount;
    }

    public HashMap<String, Integer> getActiveJobTypesCandidateCount() {
        return activeJobTypesCandidateCount;
    }

    public void setActiveJobTypesCandidateCount(HashMap<String, Integer> activeJobTypesCandidateCount) {
        this.activeJobTypesCandidateCount = activeJobTypesCandidateCount;
    }

    public HashMap<String, Integer> getHiredYTDJobTypeCandidateCount() {
        return hiredYTDJobTypeCandidateCount;
    }

    public void setHiredYTDJobTypeCandidateCount(HashMap<String, Integer> hiredYTDJobTypeCandidateCount) {
        this.hiredYTDJobTypeCandidateCount = hiredYTDJobTypeCandidateCount;
    }
}
