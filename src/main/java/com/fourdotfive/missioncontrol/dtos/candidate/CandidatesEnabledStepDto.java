package com.fourdotfive.missioncontrol.dtos.candidate;

public class CandidatesEnabledStepDto {

    private boolean candidatesPresentOnRecruiterScreening;
    private int candidatesCountOnRecruiterScreening;
    private int requisitionCountOnRecruiterScreening;
    private int clientOrBuCountOnRecruiterScreening;
    private boolean candidatesPresentOnTechAssessment;
    private int candidatesCountOnTechAssessment;
    private int requisitionCountOnTechAssessment;
    private int clientOrBuCountOnTechAssessment;
    private boolean candidatesPresentOnInterview;
    private int candidatesCountOnInterview;
    private int requisitionCountOnInterview;
    private int clientOrBuCountOnInterview;
    private boolean candidatesPresentOnPhoneScreen;
    private int candidatesCountOnPhoneScreen;
    private int requisitionCountOnPhoneScreen;
    private int clientOrBuCountOnPhoneScreen;
    private boolean candidatesPresentOnValueAssessment;
    private int candidatesCountOnValueAssessment;
    private int requisitionCountOnValueAssessment;
    private int clientOrBuCountOnValueAssessment;
    private boolean candidatesPresentOnVerification;
    private int candidatesCountOnVerification;
    private int requisitionCountOnVerification;
    private int clientOrBuCountOnVerification;

    public boolean isCandidatesPresentOnRecruiterScreening() {
        return candidatesPresentOnRecruiterScreening;
    }

    public void setCandidatesPresentOnRecruiterScreening(boolean candidatesPresentOnRecruiterScreening) {
        this.candidatesPresentOnRecruiterScreening = candidatesPresentOnRecruiterScreening;
    }

    public boolean isCandidatesPresentOnTechAssessment() {
        return candidatesPresentOnTechAssessment;
    }

    public void setCandidatesPresentOnTechAssessment(boolean candidatesPresentOnTechAssessment) {
        this.candidatesPresentOnTechAssessment = candidatesPresentOnTechAssessment;
    }

    public boolean isCandidatesPresentOnInterview() {
        return candidatesPresentOnInterview;
    }

    public void setCandidatesPresentOnInterview(boolean candidatesPresentOnInterview) {
        this.candidatesPresentOnInterview = candidatesPresentOnInterview;
    }

    public boolean isCandidatesPresentOnPhoneScreen() {
        return candidatesPresentOnPhoneScreen;
    }

    public void setCandidatesPresentOnPhoneScreen(boolean candidatesPresentOnPhoneScreen) {
        this.candidatesPresentOnPhoneScreen = candidatesPresentOnPhoneScreen;
    }

    public boolean isCandidatesPresentOnValueAssessment() {
        return candidatesPresentOnValueAssessment;
    }

    public void setCandidatesPresentOnValueAssessment(boolean candidatesPresentOnValueAssessment) {
        this.candidatesPresentOnValueAssessment = candidatesPresentOnValueAssessment;
    }

    public boolean isCandidatesPresentOnVerification() {
        return candidatesPresentOnVerification;
    }

    public void setCandidatesPresentOnVerification(boolean candidatesPresentOnVerification) {
        this.candidatesPresentOnVerification = candidatesPresentOnVerification;
    }

    public int getCandidatesCountOnRecruiterScreening() {
        return candidatesCountOnRecruiterScreening;
    }

    public void setCandidatesCountOnRecruiterScreening(int candidatesCountOnRecruiterScreening) {
        this.candidatesCountOnRecruiterScreening = candidatesCountOnRecruiterScreening;
    }

    public int getCandidatesCountOnTechAssessment() {
        return candidatesCountOnTechAssessment;
    }

    public void setCandidatesCountOnTechAssessment(int candidatesCountOnTechAssessment) {
        this.candidatesCountOnTechAssessment = candidatesCountOnTechAssessment;
    }

    public int getCandidatesCountOnInterview() {
        return candidatesCountOnInterview;
    }

    public void setCandidatesCountOnInterview(int candidatesCountOnInterview) {
        this.candidatesCountOnInterview = candidatesCountOnInterview;
    }

    public int getCandidatesCountOnPhoneScreen() {
        return candidatesCountOnPhoneScreen;
    }

    public void setCandidatesCountOnPhoneScreen(int candidatesCountOnPhoneScreen) {
        this.candidatesCountOnPhoneScreen = candidatesCountOnPhoneScreen;
    }

    public int getCandidatesCountOnValueAssessment() {
        return candidatesCountOnValueAssessment;
    }

    public void setCandidatesCountOnValueAssessment(int candidatesCountOnValueAssessment) {
        this.candidatesCountOnValueAssessment = candidatesCountOnValueAssessment;
    }

    public int getCandidatesCountOnVerification() {
        return candidatesCountOnVerification;
    }

    public void setCandidatesCountOnVerification(int candidatesCountOnVerification) {
        this.candidatesCountOnVerification = candidatesCountOnVerification;
    }

    public int getRequisitionCountOnRecruiterScreening() {
        return requisitionCountOnRecruiterScreening;
    }

    public void setRequisitionCountOnRecruiterScreening(int requisitionCountOnRecruiterScreening) {
        this.requisitionCountOnRecruiterScreening = requisitionCountOnRecruiterScreening;
    }

    public int getRequisitionCountOnTechAssessment() {
        return requisitionCountOnTechAssessment;
    }

    public void setRequisitionCountOnTechAssessment(int requisitionCountOnTechAssessment) {
        this.requisitionCountOnTechAssessment = requisitionCountOnTechAssessment;
    }

    public int getRequisitionCountOnInterview() {
        return requisitionCountOnInterview;
    }

    public void setRequisitionCountOnInterview(int requisitionCountOnInterview) {
        this.requisitionCountOnInterview = requisitionCountOnInterview;
    }

    public int getRequisitionCountOnPhoneScreen() {
        return requisitionCountOnPhoneScreen;
    }

    public void setRequisitionCountOnPhoneScreen(int requisitionCountOnPhoneScreen) {
        this.requisitionCountOnPhoneScreen = requisitionCountOnPhoneScreen;
    }

    public int getRequisitionCountOnValueAssessment() {
        return requisitionCountOnValueAssessment;
    }

    public void setRequisitionCountOnValueAssessment(int requisitionCountOnValueAssessment) {
        this.requisitionCountOnValueAssessment = requisitionCountOnValueAssessment;
    }

    public int getRequisitionCountOnVerification() {
        return requisitionCountOnVerification;
    }

    public void setRequisitionCountOnVerification(int requisitionCountOnVerification) {
        this.requisitionCountOnVerification = requisitionCountOnVerification;
    }

    public int getClientOrBuCountOnRecruiterScreening() {
        return clientOrBuCountOnRecruiterScreening;
    }

    public void setClientOrBuCountOnRecruiterScreening(int clientOrBuCountOnRecruiterScreening) {
        this.clientOrBuCountOnRecruiterScreening = clientOrBuCountOnRecruiterScreening;
    }

    public int getClientOrBuCountOnTechAssessment() {
        return clientOrBuCountOnTechAssessment;
    }

    public void setClientOrBuCountOnTechAssessment(int clientOrBuCountOnTechAssessment) {
        this.clientOrBuCountOnTechAssessment = clientOrBuCountOnTechAssessment;
    }

    public int getClientOrBuCountOnInterview() {
        return clientOrBuCountOnInterview;
    }

    public void setClientOrBuCountOnInterview(int clientOrBuCountOnInterview) {
        this.clientOrBuCountOnInterview = clientOrBuCountOnInterview;
    }

    public int getClientOrBuCountOnPhoneScreen() {
        return clientOrBuCountOnPhoneScreen;
    }

    public void setClientOrBuCountOnPhoneScreen(int clientOrBuCountOnPhoneScreen) {
        this.clientOrBuCountOnPhoneScreen = clientOrBuCountOnPhoneScreen;
    }

    public int getClientOrBuCountOnValueAssessment() {
        return clientOrBuCountOnValueAssessment;
    }

    public void setClientOrBuCountOnValueAssessment(int clientOrBuCountOnValueAssessment) {
        this.clientOrBuCountOnValueAssessment = clientOrBuCountOnValueAssessment;
    }

    public int getClientOrBuCountOnVerification() {
        return clientOrBuCountOnVerification;
    }

    public void setClientOrBuCountOnVerification(int clientOrBuCountOnVerification) {
        this.clientOrBuCountOnVerification = clientOrBuCountOnVerification;
    }
}
