package com.fourdotfive.missioncontrol.dtos.candidate;

import com.fourdotfive.missioncontrol.dtos.jobmatch.ReasonCodeDTO;

public class ChangeStepDto {
    private String jobMatchId;
    private String notes;
    private String userId;
    private boolean isSkip;
    private String companyId;
    private String externalNote;
    private ReasonCodeDTO reasonCodeDTO;

    public String getJobMatchId() {
        return jobMatchId;
    }

    public void setJobMatchId(String jobMatchId) {
        this.jobMatchId = jobMatchId;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean getIsSkip() {
        return isSkip;
    }

    public void setIsSkip(boolean isSkip) {
        this.isSkip = isSkip;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getExternalNote() {
        return externalNote;
    }

    public void setExternalNote(String externalNote) {
        this.externalNote = externalNote;
    }

    public ReasonCodeDTO getReasonCodeDTO() {
        return reasonCodeDTO;
    }

    public void setReasonCodeDTO(ReasonCodeDTO reasonCodeDTO) {
        this.reasonCodeDTO = reasonCodeDTO;
    }
}
