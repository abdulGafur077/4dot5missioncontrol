package com.fourdotfive.missioncontrol.dtos.candidate;


import com.fasterxml.jackson.annotation.JsonProperty;

public class DeleteDTO {
    private String id;
    private String note;
    private boolean hardDelete;
    private String loggedInUserId;
    @JsonProperty("isClientOrBuDelete")
    private boolean isClientOrBuDelete;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public boolean isHardDelete() {
        return hardDelete;
    }

    public void setHardDelete(boolean hardDelete) {
        this.hardDelete = hardDelete;
    }

    public String getLoggedInUserId() {
        return loggedInUserId;
    }

    public void setLoggedInUserId(String loggedInUserId) {
        this.loggedInUserId = loggedInUserId;
    }

    public boolean isClientOrBuDelete() {
        return isClientOrBuDelete;
    }

    public void setClientOrBuDelete(boolean clientOrBuDelete) {
        isClientOrBuDelete = clientOrBuDelete;
    }
}
