package com.fourdotfive.missioncontrol.dtos.candidate;


public class FilledOrJoinedRequisitionDetailDTO {

    private String name;
    private String requisitionNumber;
    private UserDTO assignedRecruiter;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRequisitionNumber() {
        return requisitionNumber;
    }

    public void setRequisitionNumber(String requisitionNumber) {
        this.requisitionNumber = requisitionNumber;
    }

    public UserDTO getAssignedRecruiter() {
        return assignedRecruiter;
    }

    public void setAssignedRecruiter(UserDTO assignedRecruiter) {
        this.assignedRecruiter = assignedRecruiter;
    }
}
