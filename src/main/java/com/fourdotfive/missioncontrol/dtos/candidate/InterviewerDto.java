package com.fourdotfive.missioncontrol.dtos.candidate;

import com.fourdotfive.missioncontrol.dtos.employee.DepartmentDto;

public class InterviewerDto {
    private String emailAddress;
    private String firstName;
    private String lastName;
    private String role;
    private DepartmentDto department;
    private String phoneNumber;
    private String userId;
    private String buId;
    private String title;
    private boolean isIndividual;

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public DepartmentDto getDepartment() {
        return department;
    }

    public void setDepartment(DepartmentDto department) {
        this.department = department;
    }

    public String getBuId() {
        return buId;
    }

    public void setBuId(String buId) {
        this.buId = buId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean getIsIndividual() {
        return isIndividual;
    }

    public void setIsIndividual(boolean individual) {
        isIndividual = individual;
    }
}
