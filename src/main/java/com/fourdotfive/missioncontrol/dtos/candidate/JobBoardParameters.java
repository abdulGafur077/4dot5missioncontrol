package com.fourdotfive.missioncontrol.dtos.candidate;

import java.util.HashMap;
import java.util.List;


public class JobBoardParameters {
    private List<String> daxtraSearchParameterList;
    private HashMap<String, List<String>> jobBoardParameterMap;


    public JobBoardParameters() {
    }

    public JobBoardParameters(List<String> daxtraSearchParameterList, HashMap<String, List<String>> jobBoardParameterMap) {
        this.daxtraSearchParameterList = daxtraSearchParameterList;
        this.jobBoardParameterMap = jobBoardParameterMap;
    }

    public List<String> getDaxtraSearchParameterList() {
        return daxtraSearchParameterList;
    }

    public void setDaxtraSearchParameterList(List<String> daxtraSearchParameterList) {
        this.daxtraSearchParameterList = daxtraSearchParameterList;
    }

    public HashMap<String, List<String>> getJobBoardParameterMap() {
        return jobBoardParameterMap;
    }

    public void setJobBoardParameterMap(HashMap<String, List<String>> jobBoardParameterMap) {
        this.jobBoardParameterMap = jobBoardParameterMap;
    }
}