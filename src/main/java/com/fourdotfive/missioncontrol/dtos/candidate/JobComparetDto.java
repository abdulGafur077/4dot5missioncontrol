package com.fourdotfive.missioncontrol.dtos.candidate;

import java.util.List;

public class JobComparetDto {

	private String name;
	private List<CompanyRequirementDto> companyRequirement;
	private List<CandidateCompareProfileDto> candidateValue;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<CompanyRequirementDto> getCompanyRequirement() {
		return companyRequirement;
	}

	public void setCompanyRequirement(
			List<CompanyRequirementDto> companyRequirement) {
		this.companyRequirement = companyRequirement;
	}

	public List<CandidateCompareProfileDto> getCandidateValue() {
		return candidateValue;
	}

	public void setCandidateValue(
			List<CandidateCompareProfileDto> candidateValue) {
		this.candidateValue = candidateValue;
	}

}
