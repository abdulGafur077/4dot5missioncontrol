package com.fourdotfive.missioncontrol.dtos.candidate;

import java.util.Date;

public class JobStateChange {

    private String candidateId;
    private String jobId;
    private String jobStepType;
    private String candidateJobStateType;
    private Date statusDate;
    private String note;
    private String modifiedBy;
    private Boolean isForward;
    private String loggedInUserId;

    /**
     * get candidateId
     *
     * @return the candidateId
     */
    public String getCandidateId() {
        return candidateId;
    }

    /**
     * set candidateId
     *
     * @param candidateId the candidateId to set
     */
    public void setCandidateId(String candidateId) {
        this.candidateId = candidateId;
    }

    /**
     * get jobId
     *
     * @return the jobId
     */
    public String getJobId() {
        return jobId;
    }

    /**
     * set jobId
     *
     * @param jobId the jobId to set
     */
    public void setJobId(String jobId) {
        this.jobId = jobId;
    }


    public String getJobStepType() {
        return jobStepType;
    }

    public void setJobStepType(String jobStepType) {
        this.jobStepType = jobStepType;
    }

    public String getCandidateJobStateType() {
        return candidateJobStateType;
    }

    public void setCandidateJobStateType(String candidateJobStateType) {
        this.candidateJobStateType = candidateJobStateType;
    }

    /**
     * get statusDate
     *
     * @return the statusDate
     */
    public Date getStatusDate() {
        return statusDate;
    }

    /**
     * set statusDate
     *
     * @param statusDate the statusDate to set
     */
    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }


    /**
     * get note
     *
     * @return the note
     */
    public String getNote() {
        return note;
    }

    /**
     * set note
     *
     * @param note the note to set
     */
    public void setNote(String note) {
        this.note = note;
    }


    /**
     * get modifiedBy
     *
     * @return the modifiedBy
     */
    public String getModifiedBy() {
        return modifiedBy;
    }

    /**
     * set modifiedBy
     *
     * @param modifiedBy the modifiedBy to set
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }


    public Boolean getIsForward() {
        return isForward;
    }

    public void setIsForward(Boolean isForward) {
        this.isForward = isForward;
    }

    public String getLoggedInUserId() {
        return loggedInUserId;
    }

    public void setLoggedInUserId(String loggedInUserId) {
        this.loggedInUserId = loggedInUserId;
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "JobStateChange{" +
                "candidateId='" + candidateId + '\'' +
                ", jobId='" + jobId + '\'' +
                ", jobStepType='" + jobStepType + '\'' +
                ", candidateJobStateType='" + candidateJobStateType + '\'' +
                ", statusDate=" + statusDate +
                ", note='" + note + '\'' +
                ", modifiedBy='" + modifiedBy + '\'' +
                ", isForward=" + isForward +
                ", loggedInUserId='" + loggedInUserId + '\'' +
                '}';
    }


}
