package com.fourdotfive.missioncontrol.dtos.candidate;

public class OverrideNoteDTO {

    private String firstName;
    private String lastName;
    private String recruiterEmail;
    private String overrideNote;
    private String candidateId;


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getRecruiterEmail() {
        return recruiterEmail;
    }

    public void setRecruiterEmail(String recruiterEmail) {
        this.recruiterEmail = recruiterEmail;
    }

    public String getOverrideNote() {
        return overrideNote;
    }

    public void setOverrideNote(String overrideNote) {
        this.overrideNote = overrideNote;
    }

    public String getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(String candidateId) {
        this.candidateId = candidateId;
    }
}
