package com.fourdotfive.missioncontrol.dtos.candidate;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ParticipantMeetingEmailDeliveryInfo {

    @JsonProperty("isEmailSendToParticipant")
    private boolean isEmailSendToParticipant;
    private String participantEmail;
    private String eventType;
    private String reason;

    public boolean isEmailSendToParticipant() {
        return isEmailSendToParticipant;
    }

    public void setEmailSendToParticipant(boolean emailSendToParticipant) {
        isEmailSendToParticipant = emailSendToParticipant;
    }

    public String getParticipantEmail() {
        return participantEmail;
    }

    public void setParticipantEmail(String participantEmail) {
        this.participantEmail = participantEmail;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getReason() {

        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
