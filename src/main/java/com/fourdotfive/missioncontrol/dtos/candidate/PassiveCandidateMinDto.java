package com.fourdotfive.missioncontrol.dtos.candidate;

import java.util.List;

public class PassiveCandidateMinDto {

	private String firstName;

	private String lastName;

	private String phoneNumber;

	private String email;

	private String im;

	private String lastContact;

	private List<JobMatchMinDto> jobMatches;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIm() {
		return im;
	}

	public void setIm(String im) {
		this.im = im;
	}

	public String getLastContact() {
		return lastContact;
	}

	public void setLastContact(String lastContact) {
		this.lastContact = lastContact;
	}

	public List<JobMatchMinDto> getJobMatches() {
		return jobMatches;
	}

	public void setJobMatches(List<JobMatchMinDto> jobMatches) {
		this.jobMatches = jobMatches;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PassiveCandidateMinDto [firstName=");
		builder.append(firstName);
		builder.append(", lastName=");
		builder.append(lastName);
		builder.append(", phoneNumber=");
		builder.append(phoneNumber);
		builder.append(", email=");
		builder.append(email);
		builder.append(", im=");
		builder.append(im);
		builder.append(", lastContact=");
		builder.append(lastContact);
		builder.append(", jobMatches=");
		builder.append(jobMatches);
		builder.append("]");
		return builder.toString();
	}

}
