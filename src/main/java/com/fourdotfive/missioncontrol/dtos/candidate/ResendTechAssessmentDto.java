package com.fourdotfive.missioncontrol.dtos.candidate;

public class ResendTechAssessmentDto {
    private String jobMatchId;
    private String notes;

    public String getJobMatchId() {
        return jobMatchId;
    }

    public void setJobMatchId(String jobMatchId) {
        this.jobMatchId = jobMatchId;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
