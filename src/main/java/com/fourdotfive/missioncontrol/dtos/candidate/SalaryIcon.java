package com.fourdotfive.missioncontrol.dtos.candidate;

public enum SalaryIcon {
    GREEN, RED, UNAVAILABLE, WHITE
}
