package com.fourdotfive.missioncontrol.dtos.candidate;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class SendAssessmentDto {
    private String jobMatchId;
    private String startDateTime;
    private String endDateTime;
    private String loggedInUserId;
    private String notes;
    private String accessTime;
    private Integer reminder;
    private String timeZoneId;
    private Integer timeToComplete;
    private String timeZoneName;
    private CandidateAssessmentTestType testType;
    private String companyId;
    private String externalNote;
    private String sentDateTime;

    public SendAssessmentDto() {
    }

    public SendAssessmentDto(AssessmentDto assessmentDto) {
        this.jobMatchId = assessmentDto.getJobMatchId();
        this.startDateTime = assessmentDto.getStartDateTime();
        this.endDateTime = assessmentDto.getEndDateTime();
        this.notes = assessmentDto.getNotes();
        this.accessTime = assessmentDto.getAccessTime();
        this.reminder = assessmentDto.getReminder();
        this.timeZoneId = assessmentDto.getTimeZoneId();
        this.timeToComplete = assessmentDto.getTimeToComplete();
        this.timeZoneName = assessmentDto.getTimeZoneName();
        this.testType = assessmentDto.getTestType();
        this.companyId = assessmentDto.getCompanyId();
        this.externalNote = assessmentDto.getExternalNote();
        this.sentDateTime = assessmentDto.getSentDateTime();
    }

    public String getJobMatchId() {
        return jobMatchId;
    }

    public void setJobMatchId(String jobMatchId) {
        this.jobMatchId = jobMatchId;
    }

    public String getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    public String getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
    }

    public String getLoggedInUserId() {
        return loggedInUserId;
    }

    public void setLoggedInUserId(String loggedInUserId) {
        this.loggedInUserId = loggedInUserId;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getAccessTime() {
        return accessTime;
    }

    public void setAccessTime(String accessTime) {
        this.accessTime = accessTime;
    }

    public Integer getReminder() {
        return reminder;
    }

    public void setReminder(Integer reminder) {
        this.reminder = reminder;
    }

    public String getTimeZoneId() {
        return timeZoneId;
    }

    public void setTimeZoneId(String timeZoneId) {
        this.timeZoneId = timeZoneId;
    }

    public Integer getTimeToComplete() {
        return timeToComplete;
    }

    public void setTimeToComplete(Integer timeToComplete) {
        this.timeToComplete = timeToComplete;
    }

    public String getTimeZoneName() {
        return timeZoneName;
    }

    public void setTimeZoneName(String timeZoneName) {
        this.timeZoneName = timeZoneName;
    }

    public CandidateAssessmentTestType getTestType() {
        return testType;
    }

    public void setTestType(CandidateAssessmentTestType testType) {
        this.testType = testType;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getExternalNote() {
        return externalNote;
    }

    public void setExternalNote(String externalNote) {
        this.externalNote = externalNote;
    }

    public String getSentDateTime() {
        return sentDateTime;
    }

    public void setSentDateTime(String sentDateTime) {
        this.sentDateTime = sentDateTime;
    }
}
