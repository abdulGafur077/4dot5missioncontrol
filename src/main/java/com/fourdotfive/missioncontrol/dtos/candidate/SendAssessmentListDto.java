package com.fourdotfive.missioncontrol.dtos.candidate;

import java.util.List;

public class SendAssessmentListDto {
    private List<String> jobMatchIds;
    private String startDateTime;
    private String endDateTime;
    private String loggedInUserId;
    private String notes;
    private String accessTime;
    private Integer reminder;
    private String timeZoneId;
    private Integer timeToComplete;
    private String timeZoneName;
    private CandidateAssessmentTestType testType;

    public SendAssessmentListDto() {
    }

    public SendAssessmentListDto(String loggedInUserId, AssessmentListDto assessmentListDto) {
        this.jobMatchIds = assessmentListDto.getJobMatchIds();
        this.startDateTime = assessmentListDto.getStartDateTime();
        this.endDateTime = assessmentListDto.getEndDateTime();
        this.loggedInUserId = loggedInUserId;
        this.notes = assessmentListDto.getNotes();
        this.accessTime = assessmentListDto.getAccessTime();
        this.reminder = assessmentListDto.getReminder();
        this.timeZoneId = assessmentListDto.getTimeZoneId();
        this.timeToComplete = assessmentListDto.getTimeToComplete();
        this.timeZoneName = assessmentListDto.getTimeZoneName();
        this.testType = assessmentListDto.getTestType();
    }

    public List<String> getJobMatchIds() {
        return jobMatchIds;
    }

    public void setJobMatchIds(List<String> jobMatchIds) {
        this.jobMatchIds = jobMatchIds;
    }

    public String getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    public String getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
    }

    public String getLoggedInUserId() {
        return loggedInUserId;
    }

    public void setLoggedInUserId(String loggedInUserId) {
        this.loggedInUserId = loggedInUserId;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getAccessTime() {
        return accessTime;
    }

    public void setAccessTime(String accessTime) {
        this.accessTime = accessTime;
    }

    public Integer getReminder() {
        return reminder;
    }

    public void setReminder(Integer reminder) {
        this.reminder = reminder;
    }

    public String getTimeZoneId() {
        return timeZoneId;
    }

    public void setTimeZoneId(String timeZoneId) {
        this.timeZoneId = timeZoneId;
    }

    public Integer getTimeToComplete() {
        return timeToComplete;
    }

    public void setTimeToComplete(Integer timeToComplete) {
        this.timeToComplete = timeToComplete;
    }

    public String getTimeZoneName() {
        return timeZoneName;
    }

    public void setTimeZoneName(String timeZoneName) {
        this.timeZoneName = timeZoneName;
    }

    public CandidateAssessmentTestType getTestType() {
        return testType;
    }

    public void setTestType(CandidateAssessmentTestType testType) {
        this.testType = testType;
    }
}
