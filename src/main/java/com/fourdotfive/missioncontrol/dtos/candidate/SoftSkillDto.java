package com.fourdotfive.missioncontrol.dtos.candidate;

public class SoftSkillDto {
	
	private String name;
	
	private boolean hasSkill;
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isHasSkill() {
		return hasSkill;
	}

	public void setHasSkill(boolean hasSkill) {
		this.hasSkill = hasSkill;
	}
	

	@Override
	public String toString() {
		return "SoftSkillsDto [name=" + name + ", hasSkill=" + hasSkill + "]";
	}
	

}
