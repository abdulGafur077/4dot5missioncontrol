package com.fourdotfive.missioncontrol.dtos.candidate;

public class TechnicalSkillDto {
	
	private String name;
	
	private int noOfYears;
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNoOfYears() {
		return noOfYears;
	}

	public void setNoOfYears(int noOfYears) {
		this.noOfYears = noOfYears;
	}

	
	@Override
	public String toString() {
		return "TechnicalSkillsDto [name=" + name + ", noOfYears=" + noOfYears + "]";
	}
	
}
