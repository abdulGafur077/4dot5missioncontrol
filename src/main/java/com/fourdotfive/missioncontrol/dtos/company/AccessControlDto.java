package com.fourdotfive.missioncontrol.dtos.company;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.fourdotfive.missioncontrol.common.AppConstants;
import com.fourdotfive.missioncontrol.pojo.company.JobRequisitionRole;

public class AccessControlDto {

	private boolean allIndustries;

	private boolean allJobRequisitionTitleList;

	private boolean allclientOrBU;

	private boolean allBusOfClient;

	private boolean requisitionCategory;

	private List<SelectedClientOrBuDto> clientOrBUList;

	private Map<String, List<SelectedClientOrBuDto>> allBusOfClientList;

	private List<SubIndustryDto> industryList;

	private List<JobRequisitionRole> jobRequisitionTitleList;

	private List<ReqCategoryDto> requisitionCategoryList;

	private boolean hasForwardedRecipientClientList;

	private boolean disabled;

	private boolean withBuAndNoRES;

	private UserClientBUinfo userClientBUinfo;

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public List<ReqCategoryDto> getRequisitionCategoryList() {
		return requisitionCategoryList;
	}

	public void setRequisitionCategoryList(
			List<ReqCategoryDto> requisitionCategoryList) {
		this.requisitionCategoryList = requisitionCategoryList;
	}

	public boolean isAllIndustries() {
		return allIndustries;
	}

	public void setAllIndustries(boolean allIndustries) {
		this.allIndustries = allIndustries;
	}

	public boolean isAllJobRequisitionTitleList() {
		return allJobRequisitionTitleList;
	}

	public void setAllJobRequisitionTitleList(boolean allJobRequisitionTitleList) {
		this.allJobRequisitionTitleList = allJobRequisitionTitleList;
	}

	public boolean isAllclientOrBU() {
		return allclientOrBU;
	}

	public void setAllclientOrBU(boolean allclientOrBU) {
		this.allclientOrBU = allclientOrBU;
	}

	public boolean isRequisitionCategory() {
		return requisitionCategory;
	}

	public void setRequisitionCategory(boolean requisitionCategory) {
		this.requisitionCategory = requisitionCategory;
	}

	public List<SelectedClientOrBuDto> getClientOrBUList() {
		return clientOrBUList;
	}

	public void setClientOrBUList(List<SelectedClientOrBuDto> clientOrBUList) {
		if (clientOrBUList != null && !clientOrBUList.isEmpty()) {

			Iterator<SelectedClientOrBuDto> iterator = clientOrBUList
					.iterator();

			SelectedClientOrBuDto obj = iterator.next();

			if (obj.getCompanyState().equals(AppConstants.ARCHIVED)) {
				// Remove the current element from the iterator and the
				// list.
				iterator.remove();
			}

		}
		this.clientOrBUList = clientOrBUList;
	}

	public List<SubIndustryDto> getIndustryList() {
		return industryList;
	}

	public void setIndustryList(List<SubIndustryDto> industryList) {
		this.industryList = industryList;
	}

	public List<JobRequisitionRole> getJobRequisitionTitleList() {
		return jobRequisitionTitleList;
	}

	public void setJobRequisitionTitleList(
			List<JobRequisitionRole> jobRequisitionTitleList) {
		this.jobRequisitionTitleList = jobRequisitionTitleList;
	}

	public boolean isHasForwardedRecipientClientList() {
		return hasForwardedRecipientClientList;
	}

	public void setHasForwardedRecipientClientList(
			boolean hasForwardedRecipientClientList) {
		this.hasForwardedRecipientClientList = hasForwardedRecipientClientList;
	}

	public UserClientBUinfo getUserClientBUinfo() {
		return userClientBUinfo;
	}

	public void setUserClientBUinfo(UserClientBUinfo userClientBUinfo) {
		this.userClientBUinfo = userClientBUinfo;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AccessControlDto [allIndustries=")
				.append(allIndustries).append(", allJobRequisitionTitleList=")
				.append(allJobRequisitionTitleList).append(", allclientOrBU=")
				.append(allclientOrBU).append(", requisitionCategory=")
				.append(requisitionCategory).append(", clientOrBUList=")
				.append(clientOrBUList).append(", industryList=")
				.append(industryList).append(", jobRequisitionTitleList=")
				.append(jobRequisitionTitleList)
				.append(", requisitionCategoryList=")
				.append(requisitionCategoryList)
				.append(", hasForwardedRecipientClientList=")
				.append(hasForwardedRecipientClientList).append(", disabled=")
				.append(disabled).append("]");
		return builder.toString();
	}

	public boolean isAllBusOfClient() {
		return allBusOfClient;
	}

	public void setAllBusOfClient(boolean allBusOfClient) {
		this.allBusOfClient = allBusOfClient;
	}

	public Map<String, List<SelectedClientOrBuDto>> getAllBusOfClientList() {
		return allBusOfClientList;
	}

	public void setAllBusOfClientList(Map<String, List<SelectedClientOrBuDto>> allBusOfClientList) {
		this.allBusOfClientList = allBusOfClientList;
	}

	public boolean isWithBuAndNoRES() {
		return withBuAndNoRES;
	}

	public void setWithBuAndNoRES(boolean withBuAndNoRES) {
		this.withBuAndNoRES = withBuAndNoRES;
	}

}
