package com.fourdotfive.missioncontrol.dtos.company;

import com.fourdotfive.missioncontrol.pojo.company.Company;

import java.util.ArrayList;
import java.util.List;

public class AllBusOfClientList {

    private List<Company> allBusOfClient = new ArrayList<>();


    public List<Company> getAllBusOfClient() {
        return allBusOfClient;
    }

    public void setAllBusOfClient(List<Company> allBusOfClient) {
        this.allBusOfClient = allBusOfClient;
    }

}
