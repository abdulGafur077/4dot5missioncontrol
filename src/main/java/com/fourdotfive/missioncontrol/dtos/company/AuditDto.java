package com.fourdotfive.missioncontrol.dtos.company;

import com.fourdotfive.missioncontrol.common.TokenDetails;
import com.fourdotfive.missioncontrol.dtos.user.UserDto;
import com.fourdotfive.missioncontrol.util.DateTimeUtil;
import org.joda.time.DateTime;

public class AuditDto {

	private String lastModifiedBy;

	private String createdBy;

	private String createdOn;

	private String modifiedOn;

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(String modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public static AuditDto getAuditDto(String createdBy, String modifiedBy,
			DateTime createdDate, DateTime modifiedDate) {

		AuditDto auditDto = new AuditDto();
		auditDto.setCreatedBy(createdBy);
		auditDto.setLastModifiedBy(modifiedBy);
		auditDto.setCreatedOn(DateTimeUtil.dateTimeFormatter(createdDate));
		auditDto.setModifiedOn(DateTimeUtil.dateTimeFormatter(modifiedDate));

		return auditDto;
	}

	public static CompanyDto setAuditDetails(CompanyDto companyDto,
			TokenDetails tokenDetails) {
		// Setting audit details
		AuditDto auditDto = new AuditDto();
		//During edit
		if (companyDto.getCompany().getId() != null) {

			if (tokenDetails.getLastName() != null) {
				auditDto.setLastModifiedBy(tokenDetails.getFirstName() + " "
						+ tokenDetails.getLastName());
			} else {
				auditDto.setLastModifiedBy(tokenDetails.getFirstName());
			}
			companyDto.setAuditDto(auditDto);
		}//During create 
		else {
			if (tokenDetails.getLastName() != null) {
				auditDto.setCreatedBy(tokenDetails.getFirstName() + " "
						+ tokenDetails.getLastName());
				auditDto.setLastModifiedBy(tokenDetails.getFirstName() + " "
						+ tokenDetails.getLastName());
			} else {
				auditDto.setCreatedBy(tokenDetails.getFirstName());
				auditDto.setLastModifiedBy(tokenDetails.getFirstName());
			}
			companyDto.setAuditDto(auditDto);
		}
		
		return companyDto;
	}
	
	public static UserDto setAuditDetails(UserDto userDto,
			TokenDetails tokenDetails) {
		// Setting audit details
		AuditDto auditDto = new AuditDto();
		if (userDto.getUserDetails().getUserId() != null) {

			if (tokenDetails.getLastName() != null) {
				auditDto.setLastModifiedBy(tokenDetails.getFirstName() + " "
						+ tokenDetails.getLastName());
			} else {
				auditDto.setLastModifiedBy(tokenDetails.getFirstName());
			}
			userDto.setAuditDto(auditDto);
		} else {
			if (tokenDetails.getLastName() != null) {
				auditDto.setCreatedBy(tokenDetails.getFirstName() + " "
						+ tokenDetails.getLastName());
			} else {
				auditDto.setCreatedBy(tokenDetails.getFirstName());
			}
			userDto.setAuditDto(auditDto);
		}
		return userDto;
	}

	@Override
	public String toString() {
		return "AuditDto [lastModifiedBy=" + lastModifiedBy + ", createdBy="
				+ createdBy + ", createdOn=" + createdOn + ", modifiedOn="
				+ modifiedOn + "]";
	}

}
