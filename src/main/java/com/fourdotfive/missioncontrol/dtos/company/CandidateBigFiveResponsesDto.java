package com.fourdotfive.missioncontrol.dtos.company;

import com.fourdotfive.missioncontrol.pojo.bigfivequestions.BigFiveQuestions;
import com.fourdotfive.missioncontrol.pojo.bigfivequestions.CandidateBigFiveResponses;
import java.util.ArrayList;
import java.util.List;

public class CandidateBigFiveResponsesDto {

    public List<BigFiveQuestions> bigFiveQuestionsList = new ArrayList();

    public List<CandidateBigFiveResponses> candidateBigFiveResponses = new ArrayList();

    public List<BigFiveQuestions> getBigFiveQuestionsList() {
        return bigFiveQuestionsList;
    }

    public void setBigFiveQuestionsList(List<BigFiveQuestions> bigFiveQuestionsList) {
        this.bigFiveQuestionsList = bigFiveQuestionsList;
    }

    public List<CandidateBigFiveResponses> getCandidateBigFiveResponses() {
        return candidateBigFiveResponses;
    }

    public void setCandidateBigFiveResponses(List<CandidateBigFiveResponses> candidateBigFiveResponses) {
        this.candidateBigFiveResponses = candidateBigFiveResponses;
    }
}

