package com.fourdotfive.missioncontrol.dtos.company;

import com.fourdotfive.missioncontrol.pojo.company.CompanyState;

public class ChangeCompanyStateDto {

    private String companyId;
    private CompanyState companyState;
    private String note;
    private String userId;
    private int techAssessmentLimitForSoftHold;
    private int jbiLimitForSoftHold;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public CompanyState getCompanyState() {
        return companyState;
    }

    public void setCompanyState(CompanyState companyState) {
        this.companyState = companyState;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getTechAssessmentLimitForSoftHold() {
        return techAssessmentLimitForSoftHold;
    }

    public void setTechAssessmentLimitForSoftHold(int techAssessmentLimitForSoftHold) {
        this.techAssessmentLimitForSoftHold = techAssessmentLimitForSoftHold;
    }

    public int getJbiLimitForSoftHold() {
        return jbiLimitForSoftHold;
    }

    public void setJbiLimitForSoftHold(int jbiLimitForSoftHold) {
        this.jbiLimitForSoftHold = jbiLimitForSoftHold;
    }
}
