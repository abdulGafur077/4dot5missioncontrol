package com.fourdotfive.missioncontrol.dtos.company;

import java.util.List;

public class ClientBUInfo {

    private String id;
    private String name;
    private List<UserCorporateClientBUInfo> buInfos;
    private boolean isAvailable;
    private boolean isEnabled;
    private boolean subordinatesBelongToBu;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<UserCorporateClientBUInfo> getBuInfos() {
        return buInfos;
    }

    public void setBuInfos(List<UserCorporateClientBUInfo> buInfos) {
        this.buInfos = buInfos;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isSubordinatesBelongToBu() {
        return subordinatesBelongToBu;
    }

    public void setSubordinatesBelongToBu(boolean subordinatesBelongToBu) {
        this.subordinatesBelongToBu = subordinatesBelongToBu;
    }
}
