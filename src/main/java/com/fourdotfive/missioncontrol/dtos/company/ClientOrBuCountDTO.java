package com.fourdotfive.missioncontrol.dtos.company;

import com.fourdotfive.missioncontrol.dtos.requisition.RequisitionDetailsResponseDTO;

import java.util.List;

public class ClientOrBuCountDTO {

    private String clientOrBuName;
    private String clientOrBuLocation;
    private List<RequisitionDetailsResponseDTO> requisitionDetailResponseDTO;

    public String getClientOrBuName() {
        return clientOrBuName;
    }

    public void setClientOrBuName(String clientOrBuName) {
        this.clientOrBuName = clientOrBuName;
    }

    public String getClientOrBuLocation() {
        return clientOrBuLocation;
    }

    public void setClientOrBuLocation(String clientOrBuLocation) {
        this.clientOrBuLocation = clientOrBuLocation;
    }

    public List<RequisitionDetailsResponseDTO> getRequisitionDetailResponseDTO() {
        return requisitionDetailResponseDTO;
    }

    public void setRequisitionDetailResponseDTO(List<RequisitionDetailsResponseDTO> requisitionDetailResponseDTO) {
        this.requisitionDetailResponseDTO = requisitionDetailResponseDTO;
    }
}
