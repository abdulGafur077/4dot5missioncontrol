package com.fourdotfive.missioncontrol.dtos.company;

import java.util.List;

public class ClientOrBuDetailsDTO {
    private String companyId;
    private String workflowStep;
    List<String> allClientOrBu;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getWorkflowStep() {
        return workflowStep;
    }

    public void setWorkflowStep(String workflowStep) {
        this.workflowStep = workflowStep;
    }

    public List<String> getAllClientOrBu() {
        return allClientOrBu;
    }

    public void setAllClientOrBu(List<String> allClientOrBu) {
        this.allClientOrBu = allClientOrBu;
    }
}
