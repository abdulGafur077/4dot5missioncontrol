package com.fourdotfive.missioncontrol.dtos.company;

public class ClientOrBuDetailsRequestDTO {

    private String companyId;
    private String workflowStep;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getWorkflowStep() {
        return workflowStep;
    }

    public void setWorkflowStep(String workflowStep) {
        this.workflowStep = workflowStep;
    }
}
