package com.fourdotfive.missioncontrol.dtos.company;

public class ClientOrBuPointOfContactDTO {

    private String id;
    private String name;

    public ClientOrBuPointOfContactDTO(SelectedClientOrBuDto selectedClientOrBuDto){
        this.id = selectedClientOrBuDto.getCompanyId();
        this.name = selectedClientOrBuDto.getCompanyName();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
