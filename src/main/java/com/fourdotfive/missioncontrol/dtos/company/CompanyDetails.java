package com.fourdotfive.missioncontrol.dtos.company;

import java.util.List;

import com.fourdotfive.missioncontrol.pojo.company.CompanyType;

public class CompanyDetails {

	private String id;
	private CompanyType companyType;
	private String companyShortName;
	private String description;
	private String name;
	private String address1;
	private String address2;
	private String city;
	private String addressState;
	private String zipcode;
	private String county;
	private String country;
	private String countryCode;
	private String industryGroup;
	private List<SubIndustryDto> industryList;
	private List<String> questionList;


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public CompanyType getCompanyType() {
		return companyType;
	}

	public void setCompanyType(CompanyType companyType) {
		this.companyType = companyType;
	}

	public String getCompanyShortName() {
		return companyShortName;
	}

	public void setCompanyShortName(String companyShortName) {
		this.companyShortName = companyShortName;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddressState() {
		return addressState;
	}

	public void setAddressState(String addressState) {
		this.addressState = addressState;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIndustryGroup() {
		return industryGroup;
	}

	public void setIndustryGroup(String industryGroup) {
		this.industryGroup = industryGroup;
	}

	public List<SubIndustryDto> getIndustryList() {
		return industryList;
	}

	public void setIndustryList(List<SubIndustryDto> industryList) {
		this.industryList = industryList;
	}

	public List<String> getQuestionList() {
		return questionList;
	}

	public void setQuestionList(List<String> questionList) {
		this.questionList = questionList;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CompanyDetails [id=");
		builder.append(id);
		builder.append(", companyType=");
		builder.append(companyType);
		builder.append(", companyShortName=");
		builder.append(companyShortName);
		builder.append(", description=");
		builder.append(description);
		builder.append(", name=");
		builder.append(name);
		builder.append(", address1=");
		builder.append(address1);
		builder.append(", address2=");
		builder.append(address2);
		builder.append(", city=");
		builder.append(city);
		builder.append(", addressState=");
		builder.append(addressState);
		builder.append(", zipcode=");
		builder.append(zipcode);
		builder.append(", county=");
		builder.append(county);
		builder.append(", country=");
		builder.append(country);
		builder.append(", industryGroup=");
		builder.append(industryGroup);
		builder.append(", industryList=");
		builder.append(industryList);
		builder.append(", questionList=");
		builder.append(questionList);
		builder.append("]");
		return builder.toString();
	}

}
