package com.fourdotfive.missioncontrol.dtos.company;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.Validate;

import com.fourdotfive.missioncontrol.common.Messages;
import com.fourdotfive.missioncontrol.dtos.user.TeamMemberDto;
import com.fourdotfive.missioncontrol.licensepreference.LicensePrefDtoList;
import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.company.Industry;
import com.fourdotfive.missioncontrol.pojo.user.Address;
import com.fourdotfive.missioncontrol.pojo.user.City;

public class CompanyDto {

	private CompanyDetails company;

	private List<ContactDto> contactList;

	private LicensePrefDtoList licensePreferences;

	private List<RoleEntityScopeDto> companyScopeList;

	private TeamMemberDto fourDotFiveAdmin;

	private String companyState;

	private AuditDto auditDto;

	public TeamMemberDto getFourDotFiveAdmin() {
		return fourDotFiveAdmin;
	}

	public void setFourDotFiveAdmin(TeamMemberDto fourDotFiveAdmin) {
		this.fourDotFiveAdmin = fourDotFiveAdmin;
	}

	public String getCompanyState() {
		return companyState;
	}

	public void setCompanyState(String companyState) {
		this.companyState = companyState;
	}

	public AuditDto getAuditDto() {
		return auditDto;
	}

	public void setAuditDto(AuditDto auditDto) {
		this.auditDto = auditDto;
	}

	public CompanyDetails getCompany() {
		return company;
	}

	public void setCompany(CompanyDetails company) {
		this.company = company;
	}

	public List<ContactDto> getContactList() {
		return contactList;
	}

	public void setContactList(List<ContactDto> contactList) {
		this.contactList = contactList;
	}

	public List<RoleEntityScopeDto> getCompanyScopeList() {
		return companyScopeList;
	}

	public void setCompanyScopeList(List<RoleEntityScopeDto> companyScopeList) {
		this.companyScopeList = companyScopeList;
	}

	public LicensePrefDtoList getLicensePreferences() {
		return licensePreferences;
	}

	public void setLicensePreferences(LicensePrefDtoList licensePreferences) {
		this.licensePreferences = licensePreferences;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CompanyDto [company=").append(company)
				.append(", contactList=").append(contactList)
				.append(", licensePreferences=").append(licensePreferences)
				.append(", companyScopeList=").append(companyScopeList)
				.append(", fourDotFiveAdmin=").append(fourDotFiveAdmin)
				.append(", companyState=").append(companyState)
				.append(", auditDto=").append(auditDto).append("]");
		return builder.toString();
	}

	public static CompanyDto mapCompanytoCompanyDto(Company company) {

		CompanyDto companyDto = new CompanyDto();

		CompanyDetails companyDetails = new CompanyDetails();

		companyDetails.setId(company.getId());
		companyDetails.setName(company.getName());
		companyDetails.setCompanyType(company.getCompanyType());
		companyDetails.setDescription(company.getDescription());
		companyDetails.setQuestionList(company.getQuestionList());
		companyDetails.setCompanyShortName(company.getShortName());
		if (company.getAddress() != null) {
			companyDetails.setAddress1(company.getAddress().getAddress1());
			companyDetails.setAddress2(company.getAddress().getAddress2());
			companyDetails.setCity(company.getAddress().getCity().getName());
			companyDetails.setCountry(company.getAddress().getCountry());
			companyDetails.setCounty(company.getAddress().getCounty());
			companyDetails.setAddressState(company.getAddress().getState());
			companyDetails.setZipcode(company.getAddress().getZipcode());
			companyDetails.setCountryCode(company.getAddress().getCountryCode());
		}
		{
			/*if (company.getIndustryList() != null) {
				List<Industry> industryList = company.getIndustryList();
				companyDetails.setIndustryGroup(new IndustryData()
						.getMasterListName(industryList));
				List<SubIndustryDto> subIndustryDtos = new ArrayList<SubIndustryDto>();
				for (Industry industry : industryList) {
					SubIndustryDto subIndustryDto = new SubIndustryDto(
							industry.getId(), industry.getDescription(),
							industry.getName(), true, true);
					subIndustryDtos.add(subIndustryDto);

				}
				companyDetails.setIndustryList(subIndustryDtos);
			}*/
		}
		if (company.getContactList() != null) {
			companyDto.setContactList(ContactDto.getContactListDto(company
					.getContactList()));
		}

		String lastModifiedUser = company.getLastModifiedUser();
		if(lastModifiedUser == null || lastModifiedUser.isEmpty()){
			lastModifiedUser = company.getCreatedUser();
		}
		
		AuditDto auditDto = AuditDto.getAuditDto(company.getCreatedUser(),
				company.getLastModifiedUser(), company.getCreatedDateTime(),
				company.getModifiedDateTime());
		companyDto.setAuditDto(auditDto);
		
		companyDto.setCompanyState(company.getCompanyState());

		companyDto.setCompany(companyDetails);
		return companyDto;
	}

	public static Company mapCompanyDtoToCompany(CompanyDto dto) {
		Company company = new Company();
		company.setId(dto.getCompany().getId());

		String name = dto.getCompany().getName();
		name = name.toUpperCase().charAt(0) + name.substring(1);
		company.setName(name);

		String shortName = dto.getCompany().getCompanyShortName();
		shortName = shortName.toUpperCase().charAt(0) + shortName.substring(1);
		company.setShortName(shortName);
		
		company.setDescription(dto.getCompany().getDescription());
		company.setCompanyType(dto.getCompany().getCompanyType());
		company.setCompanyState(dto.getCompanyState());

		Address address = new Address();
		address.setAddress1(dto.getCompany().getAddress1());
		address.setAddress2(dto.getCompany().getAddress2());
		City city = new City();
		city.setName(dto.getCompany().getCity());
		address.setCity(city);
		address.setCountry(dto.getCompany().getCountry());
		address.setCountryCode(dto.getCompany().getCountryCode());
		address.setCounty(dto.getCompany().getCounty());
		address.setState(dto.getCompany().getAddressState());
		address.setZipcode(dto.getCompany().getZipcode());
		company.setAddress(address);

		company.setQuestionList((dto.getCompany().getQuestionList()));

		company.setCreatedUser(dto.getAuditDto().getCreatedBy());
		company.setLastModifiedUser(dto.getAuditDto().getLastModifiedBy());

		if (dto.getContactList() != null) {
			company.setContactList(ContactDto.getContactList(dto
					.getContactList()));
		}
		/*if (dto.getCompany().getIndustryList() != null
				&& !dto.getCompany().getIndustryList().isEmpty()) {
			company.setIndustryList(getIndustryListFromIndustry(dto
					.getCompany().getIndustryList()));
		}*/
		return company;
	}

	public static void validateFields(CompanyDto companyDto) {
		if (companyDto != null) {
			Validate.notNull(companyDto.getCompany().getCompanyShortName(),Messages.SHORTNAME_MANDATORY);
			Validate.notNull(companyDto.getCompany().getName(), Messages.NAME_MANDATORY);
			Validate.notNull(companyDto.getCompany().getCountry(), Messages.COUNTRY_MANDATORY);
			Validate.notNull(companyDto.getCompany().getZipcode(), Messages.ZIPCODE_MANDATORY);
			Validate.notNull(companyDto.getCompany().getAddress1(), Messages.ADDRESS1_MANDATORY);
			Validate.notNull(companyDto.getCompany().getCity(), Messages.CITY_MANDATORY);
			/*if (!companyDto.getCompany().getCompanyType()
					.equals(CompanyType.Host)) {
				Validate.notNull(companyDto.getCompany().getIndustryGroup(),
						"Industry group " + AppConstants.CAN_NOT_BE_EMPTY);
				Validate.notNull(companyDto.getCompany().getIndustryList(),
						"Industries " + AppConstants.CAN_NOT_BE_EMPTY);
			}*/

			if (companyDto.getContactList() != null
					&& !companyDto.getContactList().isEmpty()) {
				for (ContactDto contactDto : companyDto.getContactList()) {
					Validate.notNull(contactDto.getEmail(), Messages.EMAIL_MANDATORY);
					Validate.notNull(contactDto.getFirstName(), Messages.FIRST_NAME_MANDATORY);
					Validate.notNull(contactDto.getMobilePhone(), Messages.MOBILE_MANDATORY);
                }
			}
			validateBlankFields(companyDto);
		}
	}

	private static void validateBlankFields(CompanyDto companyDto) {
		Validate.notEmpty(companyDto.getCompany().getCompanyShortName(), Messages.SHORTNAME_MANDATORY);
		Validate.notEmpty(companyDto.getCompany().getName(), Messages.NAME_MANDATORY);
		Validate.notEmpty(companyDto.getCompany().getCountry(), Messages.COUNTRY_MANDATORY);
		Validate.notEmpty(companyDto.getCompany().getZipcode(), Messages.ZIPCODE_MANDATORY);
		Validate.notEmpty(companyDto.getCompany().getAddress1(), Messages.ADDRESS1_MANDATORY);
		Validate.notEmpty(companyDto.getCompany().getCity(), Messages.CITY_MANDATORY);
		if (companyDto.getContactList() != null
				&& !companyDto.getContactList().isEmpty()) {
			for (ContactDto contactDto : companyDto.getContactList()) {
				Validate.notEmpty(contactDto.getEmail(),  Messages.EMAIL_MANDATORY);
				Validate.notEmpty(contactDto.getFirstName(), Messages.FIRST_NAME_MANDATORY);
				Validate.notEmpty(contactDto.getMobilePhone(), Messages.MOBILE_MANDATORY);
            }
		}
	}

	public static List<Industry> getIndustryListFromIndustry(
			List<SubIndustryDto> subIndustryList) {

		List<Industry> industryList = new ArrayList<Industry>();
		for (SubIndustryDto subIndustry : subIndustryList) {
			industryList.add(subIndustry);
		}
		return industryList;

	}

}
