package com.fourdotfive.missioncontrol.dtos.company;

public class CompanyDuplicateCheckRequestDTO {

    private String name;
    private String id;

    public CompanyDuplicateCheckRequestDTO() {}

    public CompanyDuplicateCheckRequestDTO(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
