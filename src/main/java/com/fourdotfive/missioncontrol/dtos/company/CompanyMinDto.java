package com.fourdotfive.missioncontrol.dtos.company;

import java.util.Comparator;

public class CompanyMinDto {
    private String id;
    private String name;
    private String companyState;
    private String companyType;
    private String shortName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompanyState() {
        return companyState;
    }

    public void setCompanyState(String companyState) {
        this.companyState = companyState;
    }

    public String getCompanyType() {
        return companyType;
    }

    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public CompanyMinDto(String id, String name, String companyState, String companyType, String shortName) {
        super();
        this.id = id;
        this.name = name;
        this.companyState = companyState;
        this.companyType = companyType;
        this.shortName = shortName;
    }

    public CompanyMinDto() {
        super();
    }

    public static Comparator<CompanyMinDto> nameComparator = new Comparator<CompanyMinDto>() {
        public int compare(CompanyMinDto company, CompanyMinDto nextCompany) {
            String companyName = company.getName();
            String nextCompanyName = nextCompany.getName();
            if (org.springframework.util.StringUtils.isEmpty(companyName)) {
                return (org.springframework.util.StringUtils.isEmpty(nextCompanyName)) ? 0 : -1;
            }
            if (org.springframework.util.StringUtils.isEmpty(nextCompanyName)) {
                return 1;
            }
            return companyName.toUpperCase().compareTo(nextCompanyName.toUpperCase());
        }
    };

    @Override
    public String toString() {
        return "CompanyMinDto{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", companyState='" + companyState + '\'' +
                ", companyType='" + companyType + '\'' +
                ", shortName='" + shortName + '\'' +
                '}';
    }
}
