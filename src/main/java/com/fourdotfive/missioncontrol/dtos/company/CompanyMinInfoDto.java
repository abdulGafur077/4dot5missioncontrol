package com.fourdotfive.missioncontrol.dtos.company;

import com.fourdotfive.missioncontrol.pojo.company.CompanyType;

public class CompanyMinInfoDto {

	private String companyName;
	private String companyId;
	private CompanyType companyType;
	private String companyState;
	private String aliasName;

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public CompanyType getCompanyType() {
		return companyType;
	}

	public void setCompanyType(CompanyType companyType) {
		this.companyType = companyType;
	}

	public String getCompanyState() {
		return companyState;
	}

	public void setCompanyState(String companyState) {
		this.companyState = companyState;
	}

	public String getAliasName() {
		return aliasName;
	}

	public void setAliasName(String aliasName) {
		this.aliasName = aliasName;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CompanyMinDto [companyName=").append(companyName)
				.append(", companyId=").append(companyId)
				.append(", companyType=").append(companyType)
				.append(", companyState=").append(companyState)
				.append(", aliasName=").append(aliasName)
				.append("]");
		return builder.toString();
	}

	public CompanyMinInfoDto(String companyName, String companyId,
			CompanyType companyType, String companyState) {
		super();
		this.companyName = companyName;
		this.companyId = companyId;
		this.companyType = companyType;
		this.companyState = companyState;
	}

	public CompanyMinInfoDto() {
		super();
	}
	
	

}
