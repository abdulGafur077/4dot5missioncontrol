package com.fourdotfive.missioncontrol.dtos.company;

import java.util.ArrayList;
import java.util.List;


import com.fourdotfive.missioncontrol.pojo.company.CompanyType;
import com.fourdotfive.missioncontrol.pojo.company.CompanyState;

public class CompanyReqResumeMinInfoDto {

	private String name;
	private String id;
	private CompanyType companyType;
	private String companyState;
	private List<CompanyReqResumeMinInfoDto> clientOrBUList = new ArrayList<>();
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public CompanyType getCompanyType() {
		return companyType;
	}
	public void setCompanyType(CompanyType companyType) {
		this.companyType = companyType;
	}
	public String getCompanyState() {
		return companyState;
	}
	public void setCompanyState(String companyState) {
		this.companyState = companyState;
	}
	public CompanyReqResumeMinInfoDto(String name, String id, CompanyType companyType, String companyState) {
		super();
		this.name = name;
		this.id = id;
		this.companyType = companyType;
		this.companyState = companyState;
	}
	public CompanyReqResumeMinInfoDto() {
		super();
	}
	public List<CompanyReqResumeMinInfoDto> getClientOrBUList() {
		return clientOrBUList;
	}
	public void setClientOrBUList(List<CompanyReqResumeMinInfoDto> clientOrBUList) {
		this.clientOrBUList = clientOrBUList;
	}


	
}
	
	
