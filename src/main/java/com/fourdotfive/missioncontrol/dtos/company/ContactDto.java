package com.fourdotfive.missioncontrol.dtos.company;

import java.util.ArrayList;
import java.util.List;

import com.fourdotfive.missioncontrol.pojo.user.Contact;

public class ContactDto {

    private String firstName;
    private String lastName;
    private String workPhone;
    private String mobilePhone;
    private String email;
    private String im;
    private boolean isPrimary;
    private boolean isHiringManager;
    private String userId;
    private String managerId;

    public ContactDto(String firstName, String lastName, String workPhone,
                      String mobilePhone, String email, String iM, boolean isPrimary) {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.workPhone = workPhone;
        this.mobilePhone = mobilePhone;
        this.email = email;
        this.im = iM;
        this.isPrimary = isPrimary;
    }

    public ContactDto() {
        super();
    }

    public static List<Contact> getContactList(List<ContactDto> contactDtoList) {
        List<Contact> contactList = new ArrayList<Contact>();

        for (ContactDto contactPerson : contactDtoList) {
            Contact contact = new Contact();
            contact.setFirstname(contactPerson.getFirstName());
            contact.setLastname(contactPerson.getLastName());
            contact.setEmail(contactPerson.getEmail());
            contact.setIm(contactPerson.getIm());
            contact.setMobilephone(contactPerson.getMobilePhone());
            contact.setWorkphone(contactPerson.getWorkPhone());
            contact.setPrimary(contactPerson.getIsPrimary());
            contact.setHiringManager(contactPerson.getIsHiringManager());
            contact.setUserId(contactPerson.getUserId());
            contact.setManagerId(contactPerson.getManagerId());
            contactList.add(contact);
        }
        return contactList;
    }

    public static List<ContactDto> getContactListDto(List<Contact> contactList) {

        List<ContactDto> contactDtoList = new ArrayList<ContactDto>();

        for (Contact contact : contactList) {
            ContactDto dto = new ContactDto();
            dto.setFirstName(contact.getFirstname());
            dto.setLastName(contact.getLastname());
            dto.setEmail(contact.getEmail());
            dto.setIm(contact.getIm());
            dto.setMobilePhone(contact.getMobilephone());
            dto.setWorkPhone(contact.getWorkphone());
            dto.setPrimary(contact.getIsPrimary());
            dto.setHiringManager(contact.getIsHiringManager());
            dto.setUserId(contact.getUserId());
            dto.setManagerId(contact.getManagerId());
            contactDtoList.add(dto);
        }
        return contactDtoList;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getWorkPhone() {
        return workPhone;
    }

    public void setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIm() {
        return im;
    }

    public void setIm(String im) {
        this.im = im;
    }

    public boolean getIsPrimary() {
        return isPrimary;
    }

    public void setPrimary(boolean isPrimary) {
        this.isPrimary = isPrimary;
    }

    public boolean getIsHiringManager() {
        return isHiringManager;
    }

    public void setHiringManager(boolean hiringManager) {
        isHiringManager = hiringManager;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    @Override
    public String toString() {
        return "ContactDto [firstName=" + firstName + ", lastName=" + lastName + ", workPhone=" + workPhone
                + ", mobilePhone=" + mobilePhone + ", email=" + email + ", im=" + im + ", isPrimary=" + isPrimary +
                ", isHiringManager=" + isHiringManager + ", userId=" + userId + ", managerId=" + managerId + "]";
    }

}
