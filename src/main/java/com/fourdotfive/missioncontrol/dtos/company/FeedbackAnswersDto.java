package com.fourdotfive.missioncontrol.dtos.company;

import com.fourdotfive.missioncontrol.pojo.candidate.Candidate;
import com.fourdotfive.missioncontrol.pojo.feedback.FeedbackAnswers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FeedbackAnswersDto {

    private String companyName;

    private HashMap<String, String> clientJobMatchDetails = new HashMap<>();

    private HashMap<String, List<String>> clientListJobMatchDetails = new HashMap<>();

    private HashMap<String, String> clientDetails = new HashMap<>();

    private HashMap<String, JobDetailsRecruiterScreening> jobDetails = new HashMap<>();

    private HashMap<String, List<FeedbackAnswers>> jobQuestionsResults = new HashMap<>();

    private HashMap<String, List<FeedbackAnswers>> companyQuestionsResults = new HashMap<>();

    private List<String> sortedClientList = new ArrayList();

    private HashMap<String, List<FeedbackAnswers>> clientQuestionsResults = new HashMap<>();

    private Candidate candidate;

    private boolean readOnly;

    private String meetingScheduleId;

    private boolean isParentMeetingSchedule;


    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }


    public Candidate getCandidate() {
        return candidate;
    }

    public void setCandidate(Candidate candidate) {
        this.candidate = candidate;
    }

    public HashMap<String, String> getClientDetails() {
        return clientDetails;
    }

    public void setClientDetails(HashMap<String, String> clientDetails) {
        this.clientDetails = clientDetails;
    }

    public HashMap<String, JobDetailsRecruiterScreening> getJobDetails() {
        return jobDetails;
    }

    public void setJobDetails(HashMap<String, JobDetailsRecruiterScreening> jobDetails) {
        this.jobDetails = jobDetails;
    }

    public HashMap<String, String> getClientJobMatchDetails() {
        return clientJobMatchDetails;
    }

    public void setClientJobMatchDetails(HashMap<String, String> clientJobMatchDetails) {
        this.clientJobMatchDetails = clientJobMatchDetails;
    }

    public HashMap<String, List<FeedbackAnswers>> getJobQuestionsResults() {
        return jobQuestionsResults;
    }

    public void setJobQuestionsResults(HashMap<String, List<FeedbackAnswers>> jobQuestionsResults) {
        this.jobQuestionsResults = jobQuestionsResults;
    }

    public HashMap<String, List<FeedbackAnswers>> getCompanyQuestionsResults() {
        return companyQuestionsResults;
    }

    public void setCompanyQuestionsResults(HashMap<String, List<FeedbackAnswers>> companyQuestionsResults) {
        this.companyQuestionsResults = companyQuestionsResults;
    }

    public HashMap<String, List<FeedbackAnswers>> getClientQuestionsResults() {
        return clientQuestionsResults;
    }

    public void setClientQuestionsResults(HashMap<String, List<FeedbackAnswers>> clientQuestionsResults) {
        this.clientQuestionsResults = clientQuestionsResults;
    }


    public HashMap<String, List<String>> getClientListJobMatchDetails() {
        return clientListJobMatchDetails;
    }

    public void setClientListJobMatchDetails(HashMap<String, List<String>> clientListJobMatchDetails) {
        this.clientListJobMatchDetails = clientListJobMatchDetails;
    }

    public List<String> getSortedClientList() {
        return sortedClientList;
    }

    public void setSortedClientList(List<String> sortedClientList) {
        this.sortedClientList = sortedClientList;
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    public String getMeetingScheduleId() {
        return meetingScheduleId;
    }

    public void setMeetingScheduleId(String meetingScheduleId) {
        this.meetingScheduleId = meetingScheduleId;
    }

    public boolean getIsParentMeetingSchedule() {
        return isParentMeetingSchedule;
    }

    public void setIsParentMeetingSchedule(boolean parentMeetingSchedule) {
        isParentMeetingSchedule = parentMeetingSchedule;
    }
}
