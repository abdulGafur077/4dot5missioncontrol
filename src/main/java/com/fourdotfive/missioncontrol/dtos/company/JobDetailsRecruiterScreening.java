package com.fourdotfive.missioncontrol.dtos.company;


import java.util.List;

public class JobDetailsRecruiterScreening {


    public String description;

    private Double score;

    private String matchDate;

    private String recruiterName;

    private String title;

    private String feedbackType;

    private String clientName;

    private boolean isCompleted;

    private String reqNumber;

    private String jobMatchId;

    private boolean interested;

    public String jobRawFileLocation;

    public String currentState;

    public Boolean isNewJob;

    private boolean isSharedJob;

    private String corporateName;

    private String assignedRecruiterName;

    private List<String> recruiterNames;


    public String getJobRawFileLocation() {
        return jobRawFileLocation;
    }

    public void setJobRawFileLocation(String jobRawFileLocation) {
        this.jobRawFileLocation = jobRawFileLocation;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public String getMatchDate() {
        return matchDate;
    }

    public void setMatchDate(String matchDate) {
        this.matchDate = matchDate;
    }

    public String getRecruiterName() {
        return recruiterName;
    }

    public void setRecruiterName(String recruiterName) {
        this.recruiterName = recruiterName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }


    public String getReqNumber() {
        return reqNumber;
    }

    public void setReqNumber(String reqNumber) {
        this.reqNumber = reqNumber;
    }

    public boolean isInterested() {
        return interested;
    }

    public void setInterested(boolean interested) {
        this.interested = interested;
    }

    public String getJobMatchId() {
        return jobMatchId;
    }

    public void setJobMatchId(String jobMatchId) {
        this.jobMatchId = jobMatchId;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public void setCompleted(boolean completed) {
        isCompleted = completed;
    }

    public String getFeedbackType() {
        return feedbackType;
    }

    public void setFeedbackType(String feedbackType) {
        this.feedbackType = feedbackType;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public Boolean getNewJob() {
        return isNewJob;
    }

    public void setNewJob(Boolean newJob) {
        isNewJob = newJob;
    }

    public boolean isSharedJob() {
        return isSharedJob;
    }

    public void setSharedJob(boolean sharedJob) {
        isSharedJob = sharedJob;
    }

    public String getCorporateName() {
        return corporateName;
    }

    public void setCorporateName(String corporateName) {
        this.corporateName = corporateName;
    }

    public String getAssignedRecruiterName() {
        return assignedRecruiterName;
    }

    public void setAssignedRecruiterName(String assignedRecruiterName) {
        this.assignedRecruiterName = assignedRecruiterName;
    }

    public List<String> getRecruiterNames() {
        return recruiterNames;
    }

    public void setRecruiterNames(List<String> recruiterNames) {
        this.recruiterNames = recruiterNames;
    }
}
