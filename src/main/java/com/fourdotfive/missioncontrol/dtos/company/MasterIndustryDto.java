package com.fourdotfive.missioncontrol.dtos.company;

import java.util.List;

public class MasterIndustryDto {

	private String name;
	private boolean isSelected;
	private boolean isDisabled;
	private List<SubIndustryDto> industryDtos;

	public boolean isDisabled() {
		return isDisabled;
	}

	public void setDisabled(boolean isDisabled) {
		this.isDisabled = isDisabled;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public List<SubIndustryDto> getIndustryDtos() {
		return industryDtos;
	}

	public void setIndustryDtos(List<SubIndustryDto> industryDtos) {
		this.industryDtos = industryDtos;
	}

	public MasterIndustryDto() {
		super();
	}

	public MasterIndustryDto(String name, boolean isSelected,
			List<SubIndustryDto> industryDtos) {
		super();
		this.name = name;
		this.isSelected = isSelected;
		this.industryDtos = industryDtos;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MasterIndustryDto [name=");
		builder.append(name);
		builder.append(", isSelected=");
		builder.append(isSelected);
		builder.append(", isDisabled=");
		builder.append(isDisabled);
		builder.append(", industryDtos=");
		builder.append(industryDtos);
		builder.append("]");
		return builder.toString();
	}

}
