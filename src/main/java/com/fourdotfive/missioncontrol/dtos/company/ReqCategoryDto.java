package com.fourdotfive.missioncontrol.dtos.company;

public class ReqCategoryDto {
	private String name;
	private boolean isSelected;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean getIsSelected() {
		return isSelected;
	}

	public void setIsSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ReqCategoryDto [name=");
		builder.append(name);
		builder.append(", isSelected=");
		builder.append(isSelected);
		builder.append("]");
		return builder.toString();
	}

	public ReqCategoryDto(String name, boolean isSelected) {
		super();
		this.name = name;
		this.isSelected = isSelected;
	}
	public ReqCategoryDto() {
	}

}
