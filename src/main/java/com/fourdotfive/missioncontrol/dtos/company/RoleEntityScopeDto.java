package com.fourdotfive.missioncontrol.dtos.company;

import java.util.List;

import com.fourdotfive.missioncontrol.pojo.company.CompanyType;

public class RoleEntityScopeDto {

	private CompanyType companyType;

	private List<RoleScopeDto> scopeList;

	private boolean newScope;

	private boolean isDisabled;

	public boolean isDisabled() {
		return isDisabled;
	}

	public void setDisabled(boolean isDisabled) {
		this.isDisabled = isDisabled;
	}

	public CompanyType getCompanyType() {
		return companyType;
	}

	public void setCompanyType(CompanyType companyType) {
		this.companyType = companyType;
	}

	public List<RoleScopeDto> getScopeList() {
		return scopeList;
	}

	public void setScopeList(List<RoleScopeDto> scopeList) {
		this.scopeList = scopeList;
	}

	public boolean isNewScope() {
		return newScope;
	}

	public void setNewScope(boolean newScope) {
		this.newScope = newScope;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RoleEntityScopeDto [companyType=").append(companyType)
				.append(", scopeList=").append(scopeList).append(", newScope=")
				.append(newScope).append(", isDisabled=").append(isDisabled)
				.append("]");
		return builder.toString();
	}

}
