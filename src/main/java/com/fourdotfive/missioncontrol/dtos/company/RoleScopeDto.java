package com.fourdotfive.missioncontrol.dtos.company;

import java.util.List;

import com.fourdotfive.missioncontrol.common.Messages;
import com.fourdotfive.missioncontrol.company.CompanyException;
import com.fourdotfive.missioncontrol.pojo.company.RoleEntityScope;
import com.fourdotfive.missioncontrol.pojo.user.Role;

public class RoleScopeDto {

    private boolean isDisabled;
    private boolean allClientsOrBU;
    private boolean allIndustries;
    private boolean allJobRequisitionTitles;
    private boolean allRequisitionCategories;
    private boolean clientOrBUList;
    private boolean industryList;
    private boolean jobRequisitionTitleList;
    private boolean requisitionCategoryList;
    private boolean allBUsOfClients;
    private boolean buListOfClients;
    private Role role;

    public RoleScopeDto() {
        super();
    }

    public RoleScopeDto(boolean isDisabled, Role role) {
        super();
        this.isDisabled = isDisabled;
        this.role = role;
    }

    public static void validateFields(List<RoleScopeDto> roleScopeDtos) {
        for (RoleScopeDto roleScopeDto : roleScopeDtos) {
            if (roleScopeDto.isAllClientsOrBU() == roleScopeDto.isClientOrBUList()) {
                throw new CompanyException(
                        Messages.CLIENT_ALL_LIST_CANNOT_HAVE_SAME_VALUE,
                        Messages.CLIENT_ALL_LIST_CANNOT_HAVE_SAME_VALUE);
            }
        }
    }

    public static RoleScopeDto mapRoleEntityScopeToDto(
            RoleEntityScope roleEntityScope) {
        RoleScopeDto roleScopeDto = new RoleScopeDto();
        roleScopeDto.setRole(roleEntityScope.getRole());
        roleScopeDto.setRole(roleEntityScope.getRole());
        roleScopeDto.setAllClientsOrBU(roleEntityScope.isAllClients());
        roleScopeDto.setClientOrBUList(!roleEntityScope.isAllClients());
        roleScopeDto.setAllIndustries(roleEntityScope.isAllIndustries());
        roleScopeDto.setIndustryList(!roleEntityScope.isAllIndustries());
        roleScopeDto.setAllJobRequisitionTitles(roleEntityScope.isAllJobRequisitionTitles());
        roleScopeDto.setJobRequisitionTitleList(!roleEntityScope.isAllJobRequisitionTitles());
        roleScopeDto.setAllRequisitionCategories(roleEntityScope.isAllRequisitionCategories());
        roleScopeDto.setRequisitionCategoryList(!roleEntityScope.isAllRequisitionCategories());
        roleScopeDto.setAllBUsOfClients(roleEntityScope.isAllBUsOfClients());
        roleScopeDto.setBuListOfClients(!roleEntityScope.isAllBUsOfClients());
        return roleScopeDto;
    }

    public boolean isDisabled() {
        return isDisabled;
    }

    public void setDisabled(boolean isDisabled) {
        this.isDisabled = isDisabled;
    }

    public boolean isAllClientsOrBU() {
        return allClientsOrBU;
    }

    public void setAllClientsOrBU(boolean allClients) {
        this.allClientsOrBU = allClients;
    }

    public boolean isAllIndustries() {
        return allIndustries;
    }

    public void setAllIndustries(boolean allIndustries) {
        this.allIndustries = allIndustries;
    }

    public boolean isAllJobRequisitionTitles() {
        return allJobRequisitionTitles;
    }

    public void setAllJobRequisitionTitles(boolean allJobRequisitionTitles) {
        this.allJobRequisitionTitles = allJobRequisitionTitles;
    }

    public boolean isAllRequisitionCategories() {
        return allRequisitionCategories;
    }

    public void setAllRequisitionCategories(boolean allRequisitionCategories) {
        this.allRequisitionCategories = allRequisitionCategories;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public boolean isClientOrBUList() {
        return clientOrBUList;
    }

    public void setClientOrBUList(boolean clientOrBUList) {
        this.clientOrBUList = clientOrBUList;
    }

    public boolean isIndustryList() {
        return industryList;
    }

    public void setIndustryList(boolean industryList) {
        this.industryList = industryList;
    }

    public boolean isJobRequisitionTitleList() {
        return jobRequisitionTitleList;
    }

    public void setJobRequisitionTitleList(boolean jobRequisitionTitleList) {
        this.jobRequisitionTitleList = jobRequisitionTitleList;
    }

    public boolean isRequisitionCategoryList() {
        return requisitionCategoryList;
    }

    public void setRequisitionCategoryList(boolean requisitionCategoryList) {
        this.requisitionCategoryList = requisitionCategoryList;
    }

    public boolean isAllBUsOfClients() {
        return allBUsOfClients;
    }

    public void setAllBUsOfClients(boolean allBUsOfClients) {
        this.allBUsOfClients = allBUsOfClients;
    }

    public boolean isBuListOfClients() {
        return buListOfClients;
    }

    public void setBuListOfClients(boolean buListOfClients) {
        this.buListOfClients = buListOfClients;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("RoleScopeDto [isDisabled=").append(isDisabled)
                .append(", allClientsOrBU=").append(allClientsOrBU)
                .append(", allIndustries=").append(allIndustries)
                .append(", allJobRequisitionTitles=")
                .append(allJobRequisitionTitles)
                .append(", allRequisitionCategories=")
                .append(allRequisitionCategories).append(", clientOrBUList=")
                .append(clientOrBUList).append(", industryList=")
                .append(industryList).append(", jobRequisitionTitleList=")
                .append(jobRequisitionTitleList)
                .append(", requisitionCategoryList=")
                .append(requisitionCategoryList).append(", role=").append(role)
                .append(", allBUsOfClients=").append(allBUsOfClients)
                .append(", buListOfClients=").append(buListOfClients)
                .append("]");
        return builder.toString();
    }

}
