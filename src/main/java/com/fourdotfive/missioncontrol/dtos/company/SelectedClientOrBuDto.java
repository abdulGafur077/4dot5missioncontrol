package com.fourdotfive.missioncontrol.dtos.company;

import com.fourdotfive.missioncontrol.dtos.user.ClientOwner;
import com.fourdotfive.missioncontrol.pojo.company.CompanyType;

public class SelectedClientOrBuDto extends CompanyMinInfoDto {

	private boolean isSelected;

	private ClientOwner clientOwner;

	private boolean isDisabled;

	private boolean subordinatesBelongToClientOrBu;
	
	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public boolean isDisabled() {
		return isDisabled;
	}

	public void setDisabled(boolean isDisabled) {
		this.isDisabled = isDisabled;
	}

	public ClientOwner getClientOwner() {
		return clientOwner;
	}

	public void setClientOwner(ClientOwner clientOwner) {
		this.clientOwner = clientOwner;
	}

	public boolean isSubordinatesBelongToClientOrBu() {
		return subordinatesBelongToClientOrBu;
	}

	public void setSubordinatesBelongToClientOrBu(boolean subordinatesBelongToClientOrBu) {
		this.subordinatesBelongToClientOrBu = subordinatesBelongToClientOrBu;
	}

	public SelectedClientOrBuDto() {
	}

	public SelectedClientOrBuDto(String companyName, String companyId,
			CompanyType companyType, String companyState, boolean isSelected,ClientOwner clientOwner) {
		super(companyName, companyId, companyType, companyState);
		this.isSelected = isSelected;
		this.clientOwner = clientOwner;
	}

	public SelectedClientOrBuDto(String companyName, String companyId,
			CompanyType companyType, String companyState, boolean isSelected,
			boolean isDisabled) {
		super(companyName, companyId, companyType, companyState);
		this.isSelected = isSelected;
		this.isDisabled = isDisabled;
	}

	public SelectedClientOrBuDto(String companyName, String companyId,
			CompanyType companyType, String companyState, boolean isSelected,
			boolean isDisabled, ClientOwner clientOwner) {
		super(companyName, companyId, companyType, companyState);
		this.isSelected = isSelected;
		this.isDisabled = isDisabled;
		this.clientOwner = clientOwner;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SelectedClientOrBuDto [isSelected=");
		builder.append(isSelected);
		builder.append("]");
		return builder.toString();
	}

}
