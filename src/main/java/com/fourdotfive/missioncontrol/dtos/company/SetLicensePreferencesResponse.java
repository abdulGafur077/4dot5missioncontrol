package com.fourdotfive.missioncontrol.dtos.company;


import com.fourdotfive.missioncontrol.pojo.company.Company;

public class SetLicensePreferencesResponse {
    private Company company;
    private String errorMessage;

    public SetLicensePreferencesResponse() {
    }

    public SetLicensePreferencesResponse(Company company, String errorMessage) {
        this.company = company;
        this.errorMessage = errorMessage;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
