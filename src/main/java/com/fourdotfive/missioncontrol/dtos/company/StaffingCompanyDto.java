package com.fourdotfive.missioncontrol.dtos.company;

import com.fourdotfive.missioncontrol.pojo.company.CompanyState;
import com.fourdotfive.missioncontrol.pojo.company.CompanyType;
import com.fourdotfive.missioncontrol.pojo.user.Address;
import com.fourdotfive.missioncontrol.pojo.user.Contact;

import java.util.ArrayList;
import java.util.List;

public class StaffingCompanyDto {

    private String id;
    private String name;
    private String shortName;
    private CompanyType companyType;
    private CompanyState companyState;
    private Address address;
    private List<Contact> contactList = new ArrayList<Contact>();

    public StaffingCompanyDto() {
    }

    public StaffingCompanyDto(String id, String name, String shortName, CompanyType companyType, CompanyState companyState
            , Address address, List<Contact> contactList) {
        this.id = id;
        this.name = name;
        this.shortName = shortName;
        this.companyType = companyType;
        this.companyState = companyState;
        this.address = address;
        this.contactList = contactList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public CompanyType getCompanyType() {
        return companyType;
    }

    public void setCompanyType(CompanyType companyType) {
        this.companyType = companyType;
    }

    public CompanyState getCompanyState() {
        return companyState;
    }

    public void setCompanyState(CompanyState companyState) {
        this.companyState = companyState;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<Contact> getContactList() {
        return contactList;
    }

    public void setContactList(List<Contact> contactList) {
        this.contactList = contactList;
    }
}

	
