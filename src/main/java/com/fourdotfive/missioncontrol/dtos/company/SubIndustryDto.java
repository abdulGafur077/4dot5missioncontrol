package com.fourdotfive.missioncontrol.dtos.company;

import com.fourdotfive.missioncontrol.pojo.company.Industry;

public class SubIndustryDto extends Industry {

	private boolean isSelected;
	private boolean isDisabled;

	public boolean isDisabled() {
		return isDisabled;
	}

	public void setDisabled(boolean isDisabled) {
		this.isDisabled = isDisabled;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public SubIndustryDto(String id, String description, String name,
			boolean isSelected) {
		super(id, description, name);
		this.isSelected = isSelected;
	}

	public SubIndustryDto(String id, String description, String name,
			boolean isSelected, boolean isDisabled) {
		super(id, description, name);
		this.isSelected = isSelected;
		this.isDisabled = isDisabled;
	}

	public SubIndustryDto() {
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SubIndustryDto [isSelected=");
		builder.append(isSelected);
		builder.append(", isDisabled=");
		builder.append(isDisabled);
		builder.append("]");
		return builder.toString();
	}

}
