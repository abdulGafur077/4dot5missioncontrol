package com.fourdotfive.missioncontrol.dtos.company;

import java.util.Map;

public class UserClientBUinfo {

    private boolean hasAllBuOfClientEnabledForParent;
    private boolean hasAllBuOrClientEnabledForParent;
    private Map<String, ClientBUInfo>  clientBUInfo;

    public boolean isHasAllBuOfClientEnabledForParent() {
        return hasAllBuOfClientEnabledForParent;
    }

    public void setHasAllBuOfClientEnabledForParent(boolean hasAllBuOfClientEnabledForParent) {
        this.hasAllBuOfClientEnabledForParent = hasAllBuOfClientEnabledForParent;
    }

    public boolean isHasAllBuOrClientEnabledForParent() {
        return hasAllBuOrClientEnabledForParent;
    }

    public void setHasAllBuOrClientEnabledForParent(boolean hasAllBuOrClientEnabledForParent) {
        this.hasAllBuOrClientEnabledForParent = hasAllBuOrClientEnabledForParent;
    }

    public Map<String, ClientBUInfo> getClientBUInfo() {
        return clientBUInfo;
    }

    public void setClientBUInfo(Map<String, ClientBUInfo> clientBUInfo) {
        this.clientBUInfo = clientBUInfo;
    }
}
