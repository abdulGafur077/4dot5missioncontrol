package com.fourdotfive.missioncontrol.dtos.company;

import com.fourdotfive.missioncontrol.pojo.company.CompanyState;
import com.fourdotfive.missioncontrol.pojo.company.CompanyType;

public class UserCorporateClientBUInfo {

    private String buId;
    private boolean isAvailable;
    private boolean isEnabled;
    private CompanyState companyState;
    private String name;
    private CompanyType Type;

    public String getBuId() {
        return buId;
    }

    public void setBuId(String buId) {
        this.buId = buId;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public CompanyState getCompanyState() {
        return companyState;
    }

    public void setCompanyState(CompanyState companyState) {
        this.companyState = companyState;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CompanyType getType() {
        return Type;
    }

    public void setType(CompanyType type) {
        Type = type;
    }
}
