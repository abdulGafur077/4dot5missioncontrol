package com.fourdotfive.missioncontrol.dtos.employee;

public class BUFilterDTO {
    private String companyId;
    private String searchText;
    private boolean isAdmin;
    private String loggedInUserId;

    public BUFilterDTO(String companyId, String searchText, boolean isAdmin, String loggedInUserId) {
        this.companyId = companyId;
        this.searchText = searchText;
        this.isAdmin = isAdmin;
        this.loggedInUserId = loggedInUserId;
    }

    public BUFilterDTO() {}

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public String getLoggedInUserId() {
        return loggedInUserId;
    }

    public void setLoggedInUserId(String loggedInUserId) {
        this.loggedInUserId = loggedInUserId;
    }

}
