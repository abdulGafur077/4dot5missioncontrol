package com.fourdotfive.missioncontrol.dtos.employee;

public class BUSearchDTO {
    private String searchText;
    private String companyId;

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
}
