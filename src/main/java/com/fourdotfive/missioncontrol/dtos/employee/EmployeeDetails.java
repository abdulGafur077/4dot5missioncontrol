package com.fourdotfive.missioncontrol.dtos.employee;

import com.fourdotfive.missioncontrol.dtos.company.CompanyDto;
import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.company.DepartmentDetails;
import com.fourdotfive.missioncontrol.pojo.company.DepartmentDetailsDto;
import com.fourdotfive.missioncontrol.pojo.user.User;

public class EmployeeDetails {

    private String id;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private String title;
    private OrganizationManagerDto organizationManager;
    private DepartmentDetailsDto department;
    private Company BU;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public OrganizationManagerDto getOrganizationManager() {
        return organizationManager;
    }

    public void setOrganizationManager(OrganizationManagerDto organizationManager) {
        this.organizationManager = organizationManager;
    }

    public DepartmentDetailsDto getDepartment() {
        return department;
    }

    public void setDepartment(DepartmentDetailsDto department) {
        this.department = department;
    }

    public Company getBU() {
        return BU;
    }

    public void setBU(Company BU) {
        this.BU = BU;
    }
}
