package com.fourdotfive.missioncontrol.dtos.employee;

public class EmployeeSearchDTO {

    private String searchText;
    private Integer page;
    private Integer size;
    private String sortColumn;
    private String sortDirection;
    private boolean isAdmin;
    private String loggedInUser;
    private String companyId;

    public EmployeeSearchDTO(String searchText, Integer page, Integer size, String sortColumn, String sortDirection, boolean isAdmin, String loggedInUser, String companyId) {
        this.searchText = searchText;
        this.page = page;
        this.size = size;
        this.sortColumn = sortColumn;
        this.sortDirection = sortDirection;
        this.isAdmin = isAdmin;
        this.loggedInUser = loggedInUser;
        this.companyId = companyId;
    }

    public EmployeeSearchDTO() { }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getSortColumn() {
        return sortColumn;
    }

    public void setSortColumn(String sortColumn) {
        this.sortColumn = sortColumn;
    }

    public String getSortDirection() {
        return sortDirection;
    }

    public void setSortDirection(String sortDirection) {
        this.sortDirection = sortDirection;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public String getLoggedInUser() {
        return loggedInUser;
    }

    public void setLoggedInUser(String loggedInUser) {
        this.loggedInUser = loggedInUser;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
}
