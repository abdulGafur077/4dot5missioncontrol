package com.fourdotfive.missioncontrol.dtos.employee;

public class EmployeeToUserRequestDTO {

    private String userId;
    private String organizationalManagerId;
    private String title;
    private String loggedInUserId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOrganizationalManagerId() {
        return organizationalManagerId;
    }

    public void setOrganizationalManagerId(String organizationalManagerId) {
        this.organizationalManagerId = organizationalManagerId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLoggedInUserId() {
        return loggedInUserId;
    }

    public void setLoggedInUserId(String loggedInUserId) {
        this.loggedInUserId = loggedInUserId;
    }
}
