package com.fourdotfive.missioncontrol.dtos.employee;


public class EmployeeUserCovertDTO {

    private String id;
    private String roleId;
    private String applicationManagerId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getApplicationManagerId() {
        return applicationManagerId;
    }

    public void setApplicationManagerId(String applicationManagerId) {
        this.applicationManagerId = applicationManagerId;
    }
}
