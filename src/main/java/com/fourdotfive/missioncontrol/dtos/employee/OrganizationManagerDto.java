package com.fourdotfive.missioncontrol.dtos.employee;

import org.apache.commons.lang3.StringUtils;

public class OrganizationManagerDto {

    private String id;
    private String firstName;
    private String lastName;

    public OrganizationManagerDto(EmployeeDetails employeeDetails) {
        this.id = employeeDetails.getOrganizationManager().getId();
        this.firstName = employeeDetails.getOrganizationManager().getFirstName();
        if (StringUtils.isNotEmpty(employeeDetails.getOrganizationManager().getLastName()))
            this.lastName = employeeDetails.getOrganizationManager().getLastName();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
