package com.fourdotfive.missioncontrol.dtos.employee;

import com.fourdotfive.missioncontrol.pojo.company.Industry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserScopeDTO {

    private boolean allIndustries;

    private List<Industry> industryList = new ArrayList<>();

    private boolean allClients;

    private List<String> clientList = new ArrayList<>();

    private List<String> companyIDList = new ArrayList<>();

    private boolean allJobRequisitionTitles;

    private List<String> jobRequisitionTitleList = new ArrayList<>();

    private boolean allRequisitionCategories;

    private List<String> requisitionCategoryList = new ArrayList<>();

    private boolean allBusOfClient;

    private Map<String, List<String>> allBusOfClientList = new HashMap<>();

    public boolean isAllIndustries() {
        return allIndustries;
    }

    public void setAllIndustries(boolean allIndustries) {
        this.allIndustries = allIndustries;
    }

    public List<Industry> getIndustryList() {
        return industryList;
    }

    public void setIndustryList(List<Industry> industryList) {
        this.industryList = industryList;
    }

    public boolean isAllClients() {
        return allClients;
    }

    public void setAllClients(boolean allClients) {
        this.allClients = allClients;
    }

    public List<String> getClientList() {
        return clientList;
    }

    public void setClientList(List<String> clientList) {
        this.clientList = clientList;
    }

    public List<String> getCompanyIDList() {
        return companyIDList;
    }

    public void setCompanyIDList(List<String> companyIDList) {
        this.companyIDList = companyIDList;
    }

    public boolean isAllJobRequisitionTitles() {
        return allJobRequisitionTitles;
    }

    public void setAllJobRequisitionTitles(boolean allJobRequisitionTitles) {
        this.allJobRequisitionTitles = allJobRequisitionTitles;
    }

    public List<String> getJobRequisitionTitleList() {
        return jobRequisitionTitleList;
    }

    public void setJobRequisitionTitleList(List<String> jobRequisitionTitleList) {
        this.jobRequisitionTitleList = jobRequisitionTitleList;
    }

    public boolean isAllRequisitionCategories() {
        return allRequisitionCategories;
    }

    public void setAllRequisitionCategories(boolean allRequisitionCategories) {
        this.allRequisitionCategories = allRequisitionCategories;
    }

    public List<String> getRequisitionCategoryList() {
        return requisitionCategoryList;
    }

    public void setRequisitionCategoryList(List<String> requisitionCategoryList) {
        this.requisitionCategoryList = requisitionCategoryList;
    }

    public boolean isAllBusOfClient() {
        return allBusOfClient;
    }

    public void setAllBusOfClient(boolean allBusOfClient) {
        this.allBusOfClient = allBusOfClient;
    }

    public Map<String, List<String>> getAllBusOfClientList() {
        return allBusOfClientList;
    }

    public void setAllBusOfClientList(Map<String, List<String>> allBusOfClientList) {
        this.allBusOfClientList = allBusOfClientList;
    }
}
