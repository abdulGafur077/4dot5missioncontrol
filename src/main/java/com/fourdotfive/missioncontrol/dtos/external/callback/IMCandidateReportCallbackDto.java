package com.fourdotfive.missioncontrol.dtos.external.callback;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IMCandidateReportCallbackDto {
    @JsonProperty("CandidateEmailId")
    private String candidateEmailId;
    @JsonProperty("AttemptedOn")
    private String attemptedOn;
    @JsonProperty("TotalScore")
    private double totalScore;
    @JsonProperty("CandidateScore")
    private double candidateScore;
    @JsonProperty("ReportPDFUrl")
    private String reportPDFUrl;
    @JsonProperty("TestInvitationId")
    private long testInvitationId;
    @JsonProperty("Status")
    private String status;

    public String getCandidateEmailId() {
        return candidateEmailId;
    }

    public void setCandidateEmailId(String candidateEmailId) {
        this.candidateEmailId = candidateEmailId;
    }

    public String getAttemptedOn() {
        return attemptedOn;
    }

    public void setAttemptedOn(String attemptedOn) {
        this.attemptedOn = attemptedOn;
    }

    public double getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(double totalScore) {
        this.totalScore = totalScore;
    }

    public double getCandidateScore() {
        return candidateScore;
    }

    public void setCandidateScore(double candidateScore) {
        this.candidateScore = candidateScore;
    }

    public String getReportPDFUrl() {
        return reportPDFUrl;
    }

    public void setReportPDFUrl(String reportPDFUrl) {
        this.reportPDFUrl = reportPDFUrl;
    }

    public long getTestInvitationId() {
        return testInvitationId;
    }

    public void setTestInvitationId(long testInvitationId) {
        this.testInvitationId = testInvitationId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "IMCandidateReportCallbackDto{" +
                "candidateEmailId='" + candidateEmailId + '\'' +
                ", attemptedOn='" + attemptedOn + '\'' +
                ", totalScore=" + totalScore +
                ", candidateScore=" + candidateScore +
                ", reportPDFUrl='" + reportPDFUrl + '\'' +
                ", testInvitationId=" + testInvitationId +
                ", status=" + status +
                '}';
    }
}

