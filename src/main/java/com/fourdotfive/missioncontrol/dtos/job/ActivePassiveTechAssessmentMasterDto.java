package com.fourdotfive.missioncontrol.dtos.job;
import com.fourdotfive.missioncontrol.dtos.company.CompanyMinInfoDto;

public class ActivePassiveTechAssessmentMasterDto {

	private String assessmentName;

	private String testURL;

	private CompanyMinInfoDto company;

	public String getAssessmentName() {
		return assessmentName;
	}

	public void setAssessmentName(String assessmentName) {
		this.assessmentName = assessmentName;
	}

	public String getTestURL() {
		return testURL;
	}

	public void setTestURL(String testURL) {
		this.testURL = testURL;
	}

	public CompanyMinInfoDto getCompany() {
		return company;
	}

	public void setCompany(CompanyMinInfoDto company) {
		this.company = company;
	}

}
