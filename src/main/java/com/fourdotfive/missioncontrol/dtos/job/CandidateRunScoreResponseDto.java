package com.fourdotfive.missioncontrol.dtos.job;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fourdotfive.missioncontrol.pojo.candidate.Candidate;

public class CandidateRunScoreResponseDto {
    private String candidateId;
    private String firstName;
    private String lastName;
    private String phone;
    private String email;
    private double score;
    private double jobThresholdScore;
    private boolean isJobMatchCreatedForEligibleScore;
    private boolean isAutoMatchCountExceeded;
    private boolean isJobMatchNotCreatedForInEligibleScore;
    private boolean isJobMatchManuallyCreated;
    private String jobId;
    private boolean allowOverride;
    private String releaseDate;
    private int suspendedMonths;
    private boolean noMatchDueToRelease;
    private boolean hasJobMatchUnderDifferentVendorsForSameRole;
    private boolean hasJobMatchUnderTheSameClient;
    private boolean isMultipleRolesMatchEnabled;
    private boolean hasJobMatchUnderDifferentVendorsForDifferentRole;
    private boolean mobilePhoneCountryCodePresent;
    private ReleasedCandidateDetailDTO releasedCandidateDetailDTO;
    @JsonProperty("isVendorCandidateJobLeftMatchingRuleSatisfied")
    private boolean vendorCandidateJobLeftMatchingRuleSatisfied;
    @JsonProperty("isCorporateCandidateJobLeftMatchingRuleSatisfied")
    private boolean corporateCandidateJobLeftMatchingRuleSatisfied;
    private JobLeftCandidateDetailDTO jobLeftCandidateDetailDTO;


    public CandidateRunScoreResponseDto() {
    }

    public CandidateRunScoreResponseDto(Candidate candidate, double score, double jobThresholdScore) {
        this.candidateId = candidate.getId();
        this.firstName = candidate.getContact().getFirstname();
        this.lastName = candidate.getContact().getLastname();
        this.phone = candidate.getContact().getMobilephone();
        this.email = candidate.getContact().getEmail();
        this.score = score;
        this.jobThresholdScore = jobThresholdScore;
    }

    public String getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(String candidateId) {
        this.candidateId = candidateId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public double getJobThresholdScore() {
        return jobThresholdScore;
    }

    public void setJobThresholdScore(double jobThresholdScore) {
        this.jobThresholdScore = jobThresholdScore;
    }

    public boolean isJobMatchCreatedForEligibleScore() {
        return isJobMatchCreatedForEligibleScore;
    }

    public void setJobMatchCreatedForEligibleScore(boolean jobMatchCreatedForEligibleScore) {
        isJobMatchCreatedForEligibleScore = jobMatchCreatedForEligibleScore;
    }

    public boolean isAutoMatchCountExceeded() {
        return isAutoMatchCountExceeded;
    }

    public void setAutoMatchCountExceeded(boolean autoMatchCountExceeded) {
        isAutoMatchCountExceeded = autoMatchCountExceeded;
    }

    public boolean isJobMatchNotCreatedForInEligibleScore() {
        return isJobMatchNotCreatedForInEligibleScore;
    }

    public void setJobMatchNotCreatedForInEligibleScore(boolean jobMatchNotCreatedForInEligibleScore) {
        isJobMatchNotCreatedForInEligibleScore = jobMatchNotCreatedForInEligibleScore;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public boolean isJobMatchManuallyCreated() {
        return isJobMatchManuallyCreated;
    }

    public void setJobMatchManuallyCreated(boolean jobMatchManuallyCreated) {
        isJobMatchManuallyCreated = jobMatchManuallyCreated;
    }

    public boolean isAllowOverride() {
        return allowOverride;
    }

    public void setAllowOverride(boolean allowOverride) {
        this.allowOverride = allowOverride;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public int getSuspendedMonths() {
        return suspendedMonths;
    }

    public void setSuspendedMonths(int suspendedMonths) {
        this.suspendedMonths = suspendedMonths;
    }

    public boolean isNoMatchDueToRelease() {
        return noMatchDueToRelease;
    }

    public void setNoMatchDueToRelease(boolean noMatchDueToRelease) {
        this.noMatchDueToRelease = noMatchDueToRelease;
    }

    public boolean isHasJobMatchUnderDifferentVendorsForSameRole() {
        return hasJobMatchUnderDifferentVendorsForSameRole;
    }

    public void setHasJobMatchUnderDifferentVendorsForSameRole(boolean hasJobMatchUnderDifferentVendorsForSameRole) {
        this.hasJobMatchUnderDifferentVendorsForSameRole = hasJobMatchUnderDifferentVendorsForSameRole;
    }

    public boolean isHasJobMatchUnderTheSameClient() {
        return hasJobMatchUnderTheSameClient;
    }

    public void setHasJobMatchUnderTheSameClient(boolean hasJobMatchUnderTheSameClient) {
        this.hasJobMatchUnderTheSameClient = hasJobMatchUnderTheSameClient;
    }

    public boolean isMultipleRolesMatchEnabled() {
        return isMultipleRolesMatchEnabled;
    }

    public void setMultipleRolesMatchEnabled(boolean multipleRolesMatchEnabled) {
        isMultipleRolesMatchEnabled = multipleRolesMatchEnabled;
    }

    public boolean isHasJobMatchUnderDifferentVendorsForDifferentRole() {
        return hasJobMatchUnderDifferentVendorsForDifferentRole;
    }

    public void setHasJobMatchUnderDifferentVendorsForDifferentRole(boolean hasJobMatchUnderDifferentVendorsForDifferentRole) {
        this.hasJobMatchUnderDifferentVendorsForDifferentRole = hasJobMatchUnderDifferentVendorsForDifferentRole;
    }

    public boolean isMobilePhoneCountryCodePresent() {
        return mobilePhoneCountryCodePresent;
    }

    public void setMobilePhoneCountryCodePresent(boolean mobilePhoneCountryCodePresent) {
        this.mobilePhoneCountryCodePresent = mobilePhoneCountryCodePresent;
    }

    public ReleasedCandidateDetailDTO getReleasedCandidateDetailDTO() {
        return releasedCandidateDetailDTO;
    }

    public void setReleasedCandidateDetailDTO(ReleasedCandidateDetailDTO releasedCandidateDetailDTO) {
        this.releasedCandidateDetailDTO = releasedCandidateDetailDTO;
    }

    public boolean isVendorCandidateJobLeftMatchingRuleSatisfied() {
        return vendorCandidateJobLeftMatchingRuleSatisfied;
    }

    public void setVendorCandidateJobLeftMatchingRuleSatisfied(boolean vendorCandidateJobLeftMatchingRuleSatisfied) {
        this.vendorCandidateJobLeftMatchingRuleSatisfied = vendorCandidateJobLeftMatchingRuleSatisfied;
    }

    public boolean isCorporateCandidateJobLeftMatchingRuleSatisfied() {
        return corporateCandidateJobLeftMatchingRuleSatisfied;
    }

    public void setCorporateCandidateJobLeftMatchingRuleSatisfied(boolean corporateCandidateJobLeftMatchingRuleSatisfied) {
        this.corporateCandidateJobLeftMatchingRuleSatisfied = corporateCandidateJobLeftMatchingRuleSatisfied;
    }

    public JobLeftCandidateDetailDTO getJobLeftCandidateDetailDTO() {
        return jobLeftCandidateDetailDTO;
    }

    public void setJobLeftCandidateDetailDTO(JobLeftCandidateDetailDTO jobLeftCandidateDetailDTO) {
        this.jobLeftCandidateDetailDTO = jobLeftCandidateDetailDTO;
    }

    @Override
    public String toString() {
        return "CandidateRunScoreResponseDto{" +
                "candidateId='" + candidateId + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", score=" + score +
                ", jobThresholdScore=" + jobThresholdScore +
                ", isJobMatchCreatedForEligibleScore=" + isJobMatchCreatedForEligibleScore +
                ", isAutoMatchCountExceeded=" + isAutoMatchCountExceeded +
                ", isJobMatchNotCreatedForInEligibleScore=" + isJobMatchNotCreatedForInEligibleScore +
                ", isJobMatchManuallyCreated=" + isJobMatchManuallyCreated +
                ", jobId='" + jobId + '\'' +
                ", allowOverride=" + allowOverride +
                ", releaseDate='" + releaseDate + '\'' +
                ", suspendedMonths=" + suspendedMonths +
                ", noMatchDueToRelease=" + noMatchDueToRelease +
                ", hasJobMatchUnderDifferentVendorsForSameRole=" + hasJobMatchUnderDifferentVendorsForSameRole +
                ", hasJobMatchUnderTheSameClient=" + hasJobMatchUnderTheSameClient +
                ", isMultipleRolesMatchEnabled=" + isMultipleRolesMatchEnabled +
                ", hasJobMatchUnderDifferentVendorsForDifferentRole=" + hasJobMatchUnderDifferentVendorsForDifferentRole +
                ", mobilePhoneCountryCodePresent=" + mobilePhoneCountryCodePresent +
                ", releasedCandidateDetailDTO=" + releasedCandidateDetailDTO +
                ", vendorCandidateJobLeftMatchingRuleSatisfied=" + vendorCandidateJobLeftMatchingRuleSatisfied +
                ", corporateCandidateJobLeftMatchingRuleSatisfied=" + corporateCandidateJobLeftMatchingRuleSatisfied +
                '}';
    }
}
