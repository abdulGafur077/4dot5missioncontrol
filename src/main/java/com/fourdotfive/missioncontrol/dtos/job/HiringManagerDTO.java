package com.fourdotfive.missioncontrol.dtos.job;


import com.fasterxml.jackson.annotation.JsonProperty;

public class HiringManagerDTO {

    private String id;
    private String firstName;
    private String lastName;
    @JsonProperty("isPrimary")
    private boolean isPrimary;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isPrimary() {
        return isPrimary;
    }

    public void setPrimary(boolean primary) {
        isPrimary = primary;
    }
}
