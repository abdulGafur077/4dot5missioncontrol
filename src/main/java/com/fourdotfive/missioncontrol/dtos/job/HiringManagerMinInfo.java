package com.fourdotfive.missioncontrol.dtos.job;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HiringManagerMinInfo {

    private String name;
    @JsonProperty("isPrimary")
    private boolean isPrimary;
    private boolean isPoc;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isPrimary() {
        return isPrimary;
    }

    public void setPrimary(boolean primary) {
        isPrimary = primary;
    }

    public boolean getIsPoc() {
        return isPoc;
    }

    public void setPoc(boolean poc) {
        isPoc = poc;
    }
}
