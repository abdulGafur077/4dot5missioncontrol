package com.fourdotfive.missioncontrol.dtos.job;

public class JobAssessmentRequestDTO {

    private String companyId;
    private String testOwner;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getTestOwner() {
        return testOwner;
    }

    public void setTestOwner(String testOwner) {
        this.testOwner = testOwner;
    }
}
