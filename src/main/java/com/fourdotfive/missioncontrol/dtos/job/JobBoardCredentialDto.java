package com.fourdotfive.missioncontrol.dtos.job;

public class JobBoardCredentialDto {
    private String username;
    private String password;
    private String jobBoardId;
    private String key;
    private String error;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getJobBoardId() {
        return jobBoardId;
    }

    public void setJobBoardId(String jobBoardId) {
        this.jobBoardId = jobBoardId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
