package com.fourdotfive.missioncontrol.dtos.job;


import java.util.List;
import java.util.Map;


public class JobBoardSearchHistory {

    private String jbiTransactionId;
    private String jobId;
    private List<String> jobBoardId;
    private String requestedDate;
    private String completedDate;
    private Integer resumesToPullPerJobBoardCount;
    private Integer recency;
    private int radius;
    private String location;
    private String postcode;
    private Map<String, String> daxtraParameters;
    private int noOfResumesPulled;
    private String searchStatus;
    private boolean partialFailure;
    private String advancedParametersImage;

    public JobBoardSearchHistory() {
    }

    public String getJbiTransactionId() {
        return jbiTransactionId;
    }

    public void setJbiTransactionId(String jbiTransactionId) {
        this.jbiTransactionId = jbiTransactionId;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public List<String> getJobBoardId() {
        return jobBoardId;
    }

    public void setJobBoardId(List<String> jobBoardId) {
        this.jobBoardId = jobBoardId;
    }

    public String getRequestedDate() {
        return requestedDate;
    }

    public void setRequestedDate(String requestedDate) {
        this.requestedDate = requestedDate;
    }

    public String getCompletedDate() {
        return completedDate;
    }

    public void setCompletedDate(String completedDate) {
        this.completedDate = completedDate;
    }

    public Integer getResumesToPullPerJobBoardCount() {
        return resumesToPullPerJobBoardCount;
    }

    public void setResumesToPullPerJobBoardCount(Integer resumesToPullPerJobBoardCount) {
        this.resumesToPullPerJobBoardCount = resumesToPullPerJobBoardCount;
    }

    public Integer getRecency() {
        return recency;
    }

    public void setRecency(Integer recency) {
        this.recency = recency;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public Map<String, String> getDaxtraParameters() {
        return daxtraParameters;
    }

    public void setDaxtraParameters(Map<String, String> daxtraParameters) {
        this.daxtraParameters = daxtraParameters;
    }

    public int getNoOfResumesPulled() {
        return noOfResumesPulled;
    }

    public void setNoOfResumesPulled(int noOfResumesPulled) {
        this.noOfResumesPulled = noOfResumesPulled;
    }

    public String getSearchStatus() {
        return searchStatus;
    }

    public void setSearchStatus(String searchStatus) {
        this.searchStatus = searchStatus;
    }

    public boolean isPartialFailure() {
        return partialFailure;
    }

    public void setPartialFailure(boolean partialFailure) {
        this.partialFailure = partialFailure;
    }

    public String getAdvancedParametersImage() {
        return advancedParametersImage;
    }

    public void setAdvancedParametersImage(String advancedParametersImage) {
        this.advancedParametersImage = advancedParametersImage;
    }
}
