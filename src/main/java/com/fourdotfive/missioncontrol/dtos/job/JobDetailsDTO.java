package com.fourdotfive.missioncontrol.dtos.job;

import com.fourdotfive.missioncontrol.pojo.user.User;

import java.util.List;

public class JobDetailsDTO {

    private String id;
    private String requisitionRole;
    private String clientOrBu;
    private String clientOrBuLocation;
    private List<User> recruiters;
    private List<User> hiringManagers;
    private String techAssessmentName;
    private String valueAssessmentName;
    private String requisitionNumber;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRequisitionRole() {
        return requisitionRole;
    }

    public void setRequisitionRole(String requisitionRole) {
        this.requisitionRole = requisitionRole;
    }

    public String getClientOrBu() {
        return clientOrBu;
    }

    public void setClientOrBu(String clientOrBu) {
        this.clientOrBu = clientOrBu;
    }

    public String getClientOrBuLocation() {
        return clientOrBuLocation;
    }

    public void setClientOrBuLocation(String clientOrBuLocation) {
        this.clientOrBuLocation = clientOrBuLocation;
    }

    public List<User> getRecruiters() {
        return recruiters;
    }

    public void setRecruiters(List<User> recruiters) {
        this.recruiters = recruiters;
    }

    public List<User> getHiringManagers() {
        return hiringManagers;
    }

    public void setHiringManagers(List<User> hiringManagers) {
        this.hiringManagers = hiringManagers;
    }

    public String getTechAssessmentName() {
        return techAssessmentName;
    }

    public void setTechAssessmentName(String techAssessmentName) {
        this.techAssessmentName = techAssessmentName;
    }

    public String getValueAssessmentName() {
        return valueAssessmentName;
    }

    public void setValueAssessmentName(String valueAssessmentName) {
        this.valueAssessmentName = valueAssessmentName;
    }

    public String getRequisitionNumber() {
        return requisitionNumber;
    }

    public void setRequisitionNumber(String requisitionNumber) {
        this.requisitionNumber = requisitionNumber;
    }
}
