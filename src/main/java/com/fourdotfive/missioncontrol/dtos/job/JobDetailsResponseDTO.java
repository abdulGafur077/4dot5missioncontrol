package com.fourdotfive.missioncontrol.dtos.job;

import java.util.List;
import com.fourdotfive.missioncontrol.dtos.user.UserManagerDTO;

public class JobDetailsResponseDTO {

    private String id;
    private String requisitionRole;
    private String clientOrBu;
    private String clientOrBuLocation;
    private List<UserManagerDTO> recruiters;
    private List<UserManagerDTO> hiringManagers;
    private String techAssessmentName;
    private String valueAssessmentName;
    private String requisitionNumber;

    public JobDetailsResponseDTO(JobDetailsDTO jobDetailsDTO) {
        this.id = jobDetailsDTO.getId();
        this.requisitionRole = jobDetailsDTO.getRequisitionRole();
        this.clientOrBu = jobDetailsDTO.getClientOrBu();
        this.clientOrBuLocation = jobDetailsDTO.getClientOrBuLocation();
        this.techAssessmentName = jobDetailsDTO.getTechAssessmentName();
        this.valueAssessmentName = jobDetailsDTO.getValueAssessmentName();
        this.requisitionNumber = jobDetailsDTO.getRequisitionNumber();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRequisitionRole() {
        return requisitionRole;
    }

    public void setRequisitionRole(String requisitionRole) {
        this.requisitionRole = requisitionRole;
    }

    public String getClientOrBu() {
        return clientOrBu;
    }

    public void setClientOrBu(String clientOrBu) {
        this.clientOrBu = clientOrBu;
    }

    public String getClientOrBuLocation() {
        return clientOrBuLocation;
    }

    public void setClientOrBuLocation(String clientOrBuLocation) {
        this.clientOrBuLocation = clientOrBuLocation;
    }

    public List<UserManagerDTO> getRecruiters() {
        return recruiters;
    }

    public void setRecruiters(List<UserManagerDTO> recruiters) {
        this.recruiters = recruiters;
    }

    public List<UserManagerDTO> getHiringManagers() {
        return hiringManagers;
    }

    public void setHiringManagers(List<UserManagerDTO> hiringManagers) {
        this.hiringManagers = hiringManagers;
    }

    public String getTechAssessmentName() {
        return techAssessmentName;
    }

    public void setTechAssessmentName(String techAssessmentName) {
        this.techAssessmentName = techAssessmentName;
    }

    public String getValueAssessmentName() {
        return valueAssessmentName;
    }

    public void setValueAssessmentName(String valueAssessmentName) {
        this.valueAssessmentName = valueAssessmentName;
    }

    public String getRequisitionNumber() {
        return requisitionNumber;
    }

    public void setRequisitionNumber(String requisitionNumber) {
        this.requisitionNumber = requisitionNumber;
    }
}
