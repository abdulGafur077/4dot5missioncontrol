package com.fourdotfive.missioncontrol.dtos.job;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fourdotfive.missioncontrol.dtos.company.CompanyMinDto;
import com.fourdotfive.missioncontrol.licensepreference.LicensePrefDtoList;
import com.fourdotfive.missioncontrol.pojo.job.JobRequisition;

public class JobDto {

	private List<JobOpeningDto> jobOpenings;
	private List<JobRequisition> jobRequisitionList;
	private LicensePrefDtoList licensePreferenceDetails;
	private String id;
	private String title;
	private String jobOpeningsAvailable;
	private String requisitionNumber;
	private String jobType;
	private String currentJobState;
	private String noOfFilledOpenings;
	private String noOfJustFilledOpenings;
	private Integer numberOfOpenings;
	private JobProfileDto jobProfile;
	private CompanyMinDto clientorbu;
	private CompanyMinDto company;
	private List<String> sponsorships;
	private List<HiringManagerDTO> hiringManagerDetails;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public CompanyMinDto getClientorbu() {
		return clientorbu;
	}

	public void setClientorbu(CompanyMinDto clientorbu) {
		this.clientorbu = clientorbu;
	}

	public List<JobOpeningDto> getJobOpenings() {
		return jobOpenings;
	}

	public void setJobOpenings(List<JobOpeningDto> jobOpenings) {
		this.jobOpenings = jobOpenings;
	}

	public JobProfileDto getJobProfile() {
		return jobProfile;
	}

	public void setJobProfile(JobProfileDto jobProfile) {
		this.jobProfile = jobProfile;
	}

	public Integer getNumberOfOpenings() {
		return numberOfOpenings;
	}

	public void setNumberOfOpenings(Integer numberOfOpenings) {
		this.numberOfOpenings = numberOfOpenings;
	}

	public List<JobRequisition> getJobRequisitionList() {
		return jobRequisitionList;
	}

	public void setJobRequisitionList(List<JobRequisition> jobRequisitionList) {
		this.jobRequisitionList = jobRequisitionList;
	}

	public LicensePrefDtoList getLicensePrefDtoList() {
		return licensePreferenceDetails;
	}

	public void setLicensePrefDtoList(LicensePrefDtoList licensePreferenceDetails) {
		this.licensePreferenceDetails = licensePreferenceDetails;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getJobOpeningsAvailable() {
		return jobOpeningsAvailable;
	}

	public void setJobOpeningsAvailable(String jobOpeningsAvailable) {
		this.jobOpeningsAvailable = jobOpeningsAvailable;
	}

	public String getRequisitionNumber() {
		return requisitionNumber;
	}

	public void setRequisitionNumber(String requisitionNumber) {
		this.requisitionNumber = requisitionNumber;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public String getCurrentJobState() {
		return currentJobState;
	}

	public void setCurrentJobState(String currentJobState) {
		this.currentJobState = currentJobState;
	}

	public CompanyMinDto getCompany() {
		return company;
	}

	public void setCompany(CompanyMinDto company) {
		this.company = company;
	}

	public List<String> getSponsorships() {
		return sponsorships;
	}

	public void setSponsorships(List<String> sponsorship) {
		this.sponsorships = sponsorship;
	}

	public String getNoOfFilledOpenings() {
		return noOfFilledOpenings;
	}

	public void setNoOfFilledOpenings(String noOfFilledOpenings) {
		this.noOfFilledOpenings = noOfFilledOpenings;
	}

	public String getNoOfJustFilledOpenings() {
		return noOfJustFilledOpenings;
	}

	public void setNoOfJustFilledOpenings(String noOfJustFilledOpenings) {
		this.noOfJustFilledOpenings = noOfJustFilledOpenings;
	}

	public List<HiringManagerDTO> getHiringManagerDetails() {
		return hiringManagerDetails;
	}

	public void setHiringManagerDetails(List<HiringManagerDTO> hiringManagerDetails) {
		this.hiringManagerDetails = hiringManagerDetails;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("JobDto [jobOpenings=");
		builder.append(jobOpenings);
		builder.append(", id=");
		builder.append(id);
		builder.append(", numberOfOpenings=");
		builder.append(numberOfOpenings);
		builder.append(", jobProfile=");
		builder.append(jobProfile);
		builder.append(", clientorbu=");
		builder.append(clientorbu);
		builder.append("]");
		return builder.toString();
	}

	
	

}
