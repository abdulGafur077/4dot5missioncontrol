package com.fourdotfive.missioncontrol.dtos.job;

import com.fourdotfive.missioncontrol.dtos.candidate.UserDTO;

import java.util.List;

public class JobLeftCandidateDetailDTO {

    private String candidateId;
    private int numberOfMonthsToWait;
    private String role;
    private List<UserDTO> recruiters;
    private List<UserDTO> hiringManagers;
    private String leftAt;
    private String requisitionNumber;

    public String getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(String candidateId) {
        this.candidateId = candidateId;
    }

    public int getNumberOfMonthsToWait() {
        return numberOfMonthsToWait;
    }

    public void setNumberOfMonthsToWait(int numberOfMonthsToWait) {
        this.numberOfMonthsToWait = numberOfMonthsToWait;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<UserDTO> getRecruiters() {
        return recruiters;
    }

    public void setRecruiters(List<UserDTO> recruiters) {
        this.recruiters = recruiters;
    }

    public List<UserDTO> getHiringManagers() {
        return hiringManagers;
    }

    public void setHiringManagers(List<UserDTO> hiringManagers) {
        this.hiringManagers = hiringManagers;
    }

    public String getLeftAt() {
        return leftAt;
    }

    public void setLeftAt(String leftAt) {
        this.leftAt = leftAt;
    }

    public String getRequisitionNumber() {
        return requisitionNumber;
    }

    public void setRequisitionNumber(String requisitionNumber) {
        this.requisitionNumber = requisitionNumber;
    }
}
