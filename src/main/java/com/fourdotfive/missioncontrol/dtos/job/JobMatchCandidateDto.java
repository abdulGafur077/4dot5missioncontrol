package com.fourdotfive.missioncontrol.dtos.job;

import com.fourdotfive.missioncontrol.pojo.BestJobMatchJobLocationDto;
import com.fourdotfive.missioncontrol.pojo.user.Address;

import java.util.List;

public class JobMatchCandidateDto {

    private String jobMatchId;
    private String jobMatchDate;
    private String title;
    private String company;
    private String jobMatchStatus;
    private String requisitionNumber;
    private String requisitionStatus;
    private List<String> recruiters;
    private Double fourDotFiveIntelligentScore;
    private Double fourDotFiveIntelligentThresholdScore;
    private Double techAssessmentScore;
    private Double techAssessmentThresholdScore;
    private String currentStep;
    private String currentState;
    private String jobId;
    private Address companyLocation;
    private boolean isJobMatchDisabled;
    private Integer jobMatchRank;
    private BestJobMatchJobLocationDto jobLocation;
    private RecruiterMinInfoDto assignedRecruiter;
    private boolean sharedJob;
    private String corporateId;
    private String corporateName;
    private String clientOrBuId;

    public String getJobMatchId() {
        return jobMatchId;
    }

    public void setJobMatchId(String jobMatchId) {
        this.jobMatchId = jobMatchId;
    }

    public String getJobMatchDate() {
        return jobMatchDate;
    }

    public void setJobMatchDate(String jobMatchDate) {
        this.jobMatchDate = jobMatchDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getJobMatchStatus() {
        return jobMatchStatus;
    }

    public void setJobMatchStatus(String jobMatchStatus) {
        this.jobMatchStatus = jobMatchStatus;
    }

    public String getRequisitionNumber() {
        return requisitionNumber;
    }

    public void setRequisitionNumber(String requisitionNumber) {
        this.requisitionNumber = requisitionNumber;
    }

    public String getRequisitionStatus() {
        return requisitionStatus;
    }

    public void setRequisitionStatus(String requisitionStatus) {
        this.requisitionStatus = requisitionStatus;
    }

    public List<String> getRecruiters() {
        return recruiters;
    }

    public void setRecruiters(List<String> recruiters) {
        this.recruiters = recruiters;
    }

    public Double getIntelligentScore() {
        return fourDotFiveIntelligentScore;
    }

    public void setIntelligentScore(Double intelligentScore) {
        this.fourDotFiveIntelligentScore = intelligentScore;
    }

    public Double getTechAssessmentScore() {
        return techAssessmentScore;
    }

    public void setTechAssessmentScore(Double techAssessmentScore) {
        this.techAssessmentScore = techAssessmentScore;
    }

    public Double getFourDotFiveIntelligentScore() {
        return fourDotFiveIntelligentScore;
    }

    public void setFourDotFiveIntelligentScore(Double fourDotFiveIntelligentScore) {
        this.fourDotFiveIntelligentScore = fourDotFiveIntelligentScore;
    }

    public String getCurrentStep() {
        return currentStep;
    }

    public void setCurrentStep(String currentStep) {
        this.currentStep = currentStep;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public Double getFourDotFiveIntelligentThresholdScore() {
        return fourDotFiveIntelligentThresholdScore;
    }

    public void setFourDotFiveIntelligentThresholdScore(Double fourDotFiveIntelligentThresholdScore) {
        this.fourDotFiveIntelligentThresholdScore = fourDotFiveIntelligentThresholdScore;
    }

    public Double getTechAssessmentThresholdScore() {
        return techAssessmentThresholdScore;
    }

    public void setTechAssessmentThresholdScore(Double techAssessmentThresholdScore) {
        this.techAssessmentThresholdScore = techAssessmentThresholdScore;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public Address getCompanyLocation() {
        return companyLocation;
    }

    public void setCompanyLocation(Address companyLocation) {
        this.companyLocation = companyLocation;
    }

    public boolean isJobMatchDisabled() {
        return isJobMatchDisabled;
    }

    public void setJobMatchDisabled(boolean jobMatchDisabled) {
        isJobMatchDisabled = jobMatchDisabled;
    }

    public Integer getJobMatchRank() {
        return jobMatchRank;
    }

    public void setJobMatchRank(Integer jobMatchRank) {
        this.jobMatchRank = jobMatchRank;
    }

    public BestJobMatchJobLocationDto getJobLocation() {
        return jobLocation;
    }

    public void setJobLocation(BestJobMatchJobLocationDto jobLocation) {
        this.jobLocation = jobLocation;
    }

    public RecruiterMinInfoDto getAssignedRecruiter() {
        return assignedRecruiter;
    }

    public void setAssignedRecruiter(RecruiterMinInfoDto assignedRecruiter) {
        this.assignedRecruiter = assignedRecruiter;
    }

    public boolean isSharedJob() {
        return sharedJob;
    }

    public void setSharedJob(boolean sharedJob) {
        this.sharedJob = sharedJob;
    }

    public String getCorporateId() {
        return corporateId;
    }

    public void setCorporateId(String corporateId) {
        this.corporateId = corporateId;
    }

    public String getCorporateName() {
        return corporateName;
    }

    public void setCorporateName(String corporateName) {
        this.corporateName = corporateName;
    }

    public String getClientOrBuId() {
        return clientOrBuId;
    }

    public void setClientOrBuId(String clientOrBuId) {
        this.clientOrBuId = clientOrBuId;
    }

    @Override
    public String toString() {
        return "JobMatchCandidateDto{" +
                "jobMatchId='" + jobMatchId + '\'' +
                ", jobMatchDate='" + jobMatchDate + '\'' +
                ", title='" + title + '\'' +
                ", company='" + company + '\'' +
                ", jobMatchStatus='" + jobMatchStatus + '\'' +
                ", requisitionNumber='" + requisitionNumber + '\'' +
                ", requisitionStatus='" + requisitionStatus + '\'' +
                ", recruiters=" + recruiters +
                ", fourDotFiveIntelligentScore=" + fourDotFiveIntelligentScore +
                ", fourDotFiveIntelligentThresholdScore=" + fourDotFiveIntelligentThresholdScore +
                ", techAssessmentScore=" + techAssessmentScore +
                ", techAssessmentThresholdScore=" + techAssessmentThresholdScore +
                ", currentStep='" + currentStep + '\'' +
                ", currentState='" + currentState + '\'' +
                ", jobId='" + jobId + '\'' +
                ", companyLocation=" + companyLocation +
                ", isJobMatchDisabled=" + isJobMatchDisabled +
                ", jobMatchRank=" + jobMatchRank +
                ", jobLocation=" + jobLocation +
                ", assignedRecruiter=" + assignedRecruiter +
                ", sharedJob=" + sharedJob +
                ", corporateId='" + corporateId + '\'' +
                ", corporateName='" + corporateName + '\'' +
                ", clientOrBuId='" + clientOrBuId + '\'' +
                '}';
    }
}
