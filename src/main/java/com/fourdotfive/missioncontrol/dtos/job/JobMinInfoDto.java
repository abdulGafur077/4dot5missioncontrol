package com.fourdotfive.missioncontrol.dtos.job;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fourdotfive.missioncontrol.pojo.job.JobType;
import com.fourdotfive.missioncontrol.dtos.company.CompanyReqResumeMinInfoDto;
import com.fourdotfive.missioncontrol.pojo.job.CurrentJobState;
import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.pojo.job.Job;
import com.fourdotfive.missioncontrol.pojo.job.JobCandidatesAvailable;
public class JobMinInfoDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String title;
	private String description;
	private JobType jobType;
	private Double fourDotFiveIntelligenceScore;
	private Double techAssessmentScore;
	private String typeOfSource;
	private String requisitionNumber;
	private CompanyReqResumeMinInfoDto company;
	private CompanyReqResumeMinInfoDto clientorbu;
	private List<RecruiterMinInfoDto> recruiters = new ArrayList<>();
	private List<CurrentJobState> currentJobStateList = new ArrayList<>();
	private String id;
	private JobCandidatesAvailable jobCandidatesAvailable;
	public JobMinInfoDto(String title, String description, JobType jobType, Double fourDotFiveIntelligenceScore,
			Double techAssessmentScore, String typeOfSource, String requisitionNumber,
			List<CurrentJobState> currentJobStateList, String id) {
		super();
		this.title = title;
		this.description = description;
		this.jobType = jobType;
		this.fourDotFiveIntelligenceScore = fourDotFiveIntelligenceScore;
		this.techAssessmentScore = techAssessmentScore;
		this.typeOfSource = typeOfSource;
		this.requisitionNumber = requisitionNumber;
		this.currentJobStateList = currentJobStateList;
		this.id = id;
	}

	public JobMinInfoDto() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public JobType getJobType() {
		return jobType;
	}

	public void setJobType(JobType jobType) {
		this.jobType = jobType;
	}

	public Double getFourDotFiveIntelligenceScore() {
		return fourDotFiveIntelligenceScore;
	}

	public void setFourDotFiveIntelligenceScore(Double fourDotFiveIntelligenceScore) {
		this.fourDotFiveIntelligenceScore = fourDotFiveIntelligenceScore;
	}

	public Double getTechAssessmentScore() {
		return techAssessmentScore;
	}

	public void setTechAssessmentScore(Double techAssessmentScore) {
		this.techAssessmentScore = techAssessmentScore;
	}

	public String getTypeOfSource() {
		return typeOfSource;
	}

	public void setTypeOfSource(String typeOfSource) {
		this.typeOfSource = typeOfSource;
	}

	public String getRequisitionNumber() {
		return requisitionNumber;
	}

	public void setRequisitionNumber(String requisitionNumber) {
		this.requisitionNumber = requisitionNumber;
	}

	public CompanyReqResumeMinInfoDto getCompany() {
		return company;
	}

	public void setCompany(CompanyReqResumeMinInfoDto company) {
		this.company = company;
	}

	public List<RecruiterMinInfoDto> getRecruiters() {
		return recruiters;
	}

	public void setRecruiters(List<RecruiterMinInfoDto> recruiters) {
		this.recruiters = recruiters;
	}

	/**
	 * get currentJobStateList
	 * 
	 * @return the currentJobStateList
	 *
	 */
	public List<CurrentJobState> getCurrentJobStateList() {
		return currentJobStateList;
	}

	/**
	 * set currentJobStateList
	 * 
	 * @param currentJobStateList
	 *            the currentJobStateList to set
	 *
	 */
	public void setCurrentJobStateList(List<CurrentJobState> currentJobStateList) {
		this.currentJobStateList = currentJobStateList;
	}

	public CompanyReqResumeMinInfoDto getClientorbu() {
		return clientorbu;
	}

	public void setClientorbu(CompanyReqResumeMinInfoDto clientorbu) {
		this.clientorbu = clientorbu;
	}

	public JobCandidatesAvailable getJobCandidatesAvailable() {
		return jobCandidatesAvailable;
	}

	public void setJobCandidatesAvailable(JobCandidatesAvailable jobCandidatesAvailable) {
		this.jobCandidatesAvailable = jobCandidatesAvailable;
	}

	public static JobMinInfoDto mapFromJob(Job job) {
		JobMinInfoDto dto = new JobMinInfoDto(job.getTitle(), job.getDescription(), job.getJobType(),
				job.getFourDotFiveIntelligenceScore(), job.getTechAssessmentScore(), job.getTypeOfSource(),
				job.getRequisitionNumber(), job.getCurrentJobStateList(), job.getId());
		dto.setRecruiters(getRecruiters(job));
		dto.setCompany(new CompanyReqResumeMinInfoDto(job.getCompany().getName(), job.getCompany().getId(),
				job.getCompany().getCompanyType(), job.getCompany().getCompanyState()));
		if (job.getClientorbu() != null) {
			dto.setClientorbu(new CompanyReqResumeMinInfoDto(job.getClientorbu().getName(), job.getClientorbu().getId(),
					job.getClientorbu().getCompanyType(), job.getClientorbu().getCompanyState()));
		}
		return dto;

	}

	private static List<RecruiterMinInfoDto> getRecruiters(Job job) {
		List<RecruiterMinInfoDto> infoDtos = new ArrayList<>();
		for (User user : job.getRecruiters()) {
			infoDtos.add(new RecruiterMinInfoDto(user.getFirstname(), user.getLastname(), user.getId()));

		}
		return infoDtos;
	}

}
