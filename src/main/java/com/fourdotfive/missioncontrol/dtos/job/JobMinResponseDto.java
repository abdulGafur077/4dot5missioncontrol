package com.fourdotfive.missioncontrol.dtos.job;

import java.util.List;

public class JobMinResponseDto {

    private int totalCount;
    private List<JobMinDetailsDto> jobMinDetailsDtos;
    private List<NewJobDto> jobDetails;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<JobMinDetailsDto> getJobMinDetailsDtos() {
        return jobMinDetailsDtos;
    }

    public void setJobMinDetailsDtos(List<JobMinDetailsDto> jobMinDetailsDtos) {
        this.jobMinDetailsDtos = jobMinDetailsDtos;
    }

    public List<NewJobDto> getJobDetails() {
        return jobDetails;
    }

    public void setJobDetails(List<NewJobDto> jobDetails) {
        this.jobDetails = jobDetails;
    }
}
