package com.fourdotfive.missioncontrol.dtos.job;

import java.util.Date;
import java.util.List;

import com.fourdotfive.missioncontrol.dtos.user.TeamMemberDto;
import com.fourdotfive.missioncontrol.pojo.candidate.Candidate;

/**
 * Job Opening Entity
 *
 */
public class JobOpeningDto {

	private Integer openingNumber;

	private String requisitionOpeningNumber;

	private List<TeamMemberDto> assignedRecruiters;

	private Candidate assignedCandidate;

	private Date fillDate = new Date();

	private String note;

	/**
	 * get openingNumber
	 * 
	 * @return the openingNumber
	 *
	 */
	public Integer getOpeningNumber() {
		return openingNumber;
	}

	/**
	 * set openingNumber
	 * 
	 * @param openingNumber
	 *            the openingNumber to set
	 *
	 */
	public void setOpeningNumber(Integer openingNumber) {
		this.openingNumber = openingNumber;
	}

	/**
	 * get requisitionOpeningNumber
	 * 
	 * @return the requisitionOpeningNumber
	 *
	 */
	public String getRequisitionOpeningNumber() {
		return requisitionOpeningNumber;
	}

	/**
	 * set requisitionOpeningNumber
	 * 
	 * @param requisitionOpeningNumber
	 *            the requisitionOpeningNumber to set
	 *
	 */
	public void setRequisitionOpeningNumber(String requisitionOpeningNumber) {
		this.requisitionOpeningNumber = requisitionOpeningNumber;
	}

	/**
	 * get assignedRecruiter
	 * 
	 * @return the assignedRecruiter
	 *
	 */
	public List<TeamMemberDto> getAssignedRecruiters() {
		return assignedRecruiters;
	}

	/**
	 * set assingedRecruiter
	 * 
	 * @param assignedRecruiters
	 *           the assignedRecruiters to set
	 *
	 */
	public void setAssignedRecruiters(List<TeamMemberDto> assignedRecruiters) {
		this.assignedRecruiters = assignedRecruiters;
	}

	/**
	 * get assignedCandidate
	 * 
	 * @return the assignedCandidate
	 *
	 */
	public Candidate getAssignedCandidate() {
		return assignedCandidate;
	}

	/**
	 * set assignedCandidate
	 * 
	 * @param assignedCandidate
	 *            the assignedCandidate to set
	 *
	 */
	public void setAssignedCandidate(Candidate assignedCandidate) {
		this.assignedCandidate = assignedCandidate;
	}

	/**
	 * get fillDate
	 * 
	 * @return the fillDate
	 *
	 */
	public Date getFillDate() {
		return fillDate;
	}

	/**
	 * set fillDate
	 * 
	 * @param fillDate
	 *            the fillDate to set
	 *
	 */
	public void setFillDate(Date fillDate) {
		this.fillDate = fillDate;
	}

	/**
	 * get note
	 * 
	 * @return the note
	 *
	 */
	public String getNote() {
		return note;
	}

	/**
	 * set note
	 * 
	 * @param note
	 *            the note to set
	 *
	 */
	public void setNote(String note) {
		this.note = note;
	}

}