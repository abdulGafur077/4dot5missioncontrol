package com.fourdotfive.missioncontrol.dtos.job;

import com.fourdotfive.missioncontrol.pojo.company.CompanyState;
import com.fourdotfive.missioncontrol.pojo.company.CompanyType;

public class JobProfileCompanyDto {

	private String description;

	private String role;

	private String name;

	private String id;

	private CompanyState companyState;

	private CompanyType companyType;

	private String jobOpenings;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public CompanyState getCompanyState() {
		return companyState;
	}

	public void setCompanyState(CompanyState companyState) {
		this.companyState = companyState;
	}

	public CompanyType getCompanyType() {
		return companyType;
	}

	public void setCompanyType(CompanyType companyType) {
		this.companyType = companyType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getJobOpenings() {
		return jobOpenings;
	}

	public void setJobOpenings(String jobOpenings) {
		this.jobOpenings = jobOpenings;
	}

}
