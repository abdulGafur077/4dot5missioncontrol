package com.fourdotfive.missioncontrol.dtos.job;

import java.util.List;

import com.fourdotfive.missioncontrol.dtos.candidate.SoftSkillDto;
import com.fourdotfive.missioncontrol.dtos.candidate.TechnicalSkillDto;

public class JobProfileDto {

	private String description;
	
	private String role;
	
	private String companyName;
	
	private List<String> educationList;
	
	private List<String> experienceList;
	
	private List<String> certificationList;
	
	private List<TechnicalSkillDto> technicalSkillList;
	
	private List<SoftSkillDto> softSkillList;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public List<String> getEducationList() {
		return educationList;
	}

	public void setEducationList(List<String> educationList) {
		this.educationList = educationList;
	}

	public List<String> getExperienceList() {
		return experienceList;
	}

	public void setExperienceList(List<String> experienceList) {
		this.experienceList = experienceList;
	}

	public List<String> getCertificationList() {
		return certificationList;
	}

	public void setCertificationList(List<String> certificationList) {
		this.certificationList = certificationList;
	}

	public List<TechnicalSkillDto> getTechnicalSkillList() {
		return technicalSkillList;
	}

	public void setTechnicalSkillList(List<TechnicalSkillDto> technicalSkillList) {
		this.technicalSkillList = technicalSkillList;
	}

	public List<SoftSkillDto> getSoftSkillList() {
		return softSkillList;
	}

	public void setSoftSkillList(List<SoftSkillDto> softSkillList) {
		this.softSkillList = softSkillList;
	}

	@Override
	public String toString() {
		return "JobProfileDto [description=" + description + ", role=" + role + ", companyName=" + companyName
				+ ", educationList=" + educationList + ", experienceList=" + experienceList + ", certificationList="
				+ certificationList + ", technicalSkillList=" + technicalSkillList + ", softSkillList=" + softSkillList
				+ "]";
	}

}
