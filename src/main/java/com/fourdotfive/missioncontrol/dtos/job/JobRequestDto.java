package com.fourdotfive.missioncontrol.dtos.job;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class JobRequestDto {
	private List<String> clientOrBU;

	private List<String> roleList;

	private List<String> recruiters;

	private List<String> requisitionNums;

	private List<String> requisitionStates;

	private List<String> scopeUsers;

	private String fromDate;

	private String toDate;
	
	private boolean emptyRequisitionFlag;
	
	private String loggedInUserId;

	private String sortColumn;

	private String sortDirection;

	private String pageNum;

	private String size;

	@JsonProperty("isAssignedToMe")
    private boolean isAassignedToMe;

	@JsonProperty("isAssignedToOthers")
	private boolean isAssignedToOthers;

    @JsonProperty("isUnassigned")
    private boolean isUnassigned;

	private String searchText;

	private boolean isAllRequisition;

	private boolean isSharedWithMe;

	private List<String> jobTypes;

	private List<RequisitionType> requisitionTypes;

	public String getPageNum() {
		return pageNum;
	}

	public void setPageNum(String pageNum) {
		this.pageNum = pageNum;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getSortColumn() {
		return sortColumn;
	}

	public void setSortColumn(String sortColumn) {
		this.sortColumn = sortColumn;
	}

	public String getSortDirection() {
		return sortDirection;
	}

	public void setSortDirection(String sortDirection) {
		this.sortDirection = sortDirection;
	}

	public String getLoggedInUserId() {
		return loggedInUserId;
	}

	public void setLoggedInUserId(String loggedInUserId) {
		this.loggedInUserId = loggedInUserId;
	}

	public List<String> getClientOrBU() {
		return clientOrBU;
	}

	public void setClientOrBU(List<String> clientOrBU) {
		this.clientOrBU = clientOrBU;
	}

	public List<String> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<String> roleList) {
		this.roleList = roleList;
	}

	public List<String> getRecruiters() {
		return recruiters;
	}

	public void setRecruiters(List<String> recruiters) {
		this.recruiters = recruiters;
	}

	public List<String> getRequisitionNums() {
		return requisitionNums;
	}

	public void setRequisitionNums(List<String> requisitionNums) {
		this.requisitionNums = requisitionNums;
	}

	public List<String> getRequisitionStates() {
		return requisitionStates;
	}

	public void setRequisitionStates(List<String> requisitionStates) {
		this.requisitionStates = requisitionStates;
	}

	public List<String> getScopeUsers() {
		return scopeUsers;
	}

	public void setScopeUsers(List<String> scopeUsers) {
		this.scopeUsers = scopeUsers;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public boolean isEmptyRequisitionFlag() {
		return emptyRequisitionFlag;
	}

	public void setEmptyRequisitionFlag(boolean emptyRequisitionFlag) {
		this.emptyRequisitionFlag = emptyRequisitionFlag;
	}

    public boolean isAassignedToMe() {
        return isAassignedToMe;
    }

    public void setAassignedToMe(boolean aassignedToMe) {
        isAassignedToMe = aassignedToMe;
    }

	public boolean isAssignedToOthers() {
		return isAssignedToOthers;
    }

	public void setAssignedToOthers(boolean assignedToOthers) {
		isAssignedToOthers = assignedToOthers;
    }

    public boolean isUnassigned() {
        return isUnassigned;
    }

    public void setUnassigned(boolean unassigned) {
        isUnassigned = unassigned;
    }

	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public boolean getIsAllRequisition() {
		return isAllRequisition;
	}

	public void setIsAllRequisition(boolean allRequisition) {
		isAllRequisition = allRequisition;
	}

	public boolean getIsSharedWithMe() {
		return isSharedWithMe;
	}

	public void setIsSharedWithMe(boolean sharedWithMe) {
		isSharedWithMe = sharedWithMe;
	}

	public List<RequisitionType> getRequisitionTypes() {
		return requisitionTypes;
	}

	public void setRequisitionTypes(List<RequisitionType> requisitionTypes) {
		this.requisitionTypes = requisitionTypes;
	}

	public List<String> getJobTypes() {
		return jobTypes;
	}

	public void setJobTypes(List<String> jobTypes) {
		this.jobTypes = jobTypes;
	}

	@Override
	public String toString() {
		return "JobRequestDto [clientOrBU=" + clientOrBU + ", roleList=" + roleList +
				", recruiters=" + recruiters + ", requisitionNums=" + requisitionNums +
				", requisitionStates=" + requisitionStates + ", scopeUsers=" + scopeUsers +
				", fromDate=" + fromDate + ", toDate=" + toDate +
				", emptyRequisitionFlag=" + emptyRequisitionFlag + "]";
	}
}
