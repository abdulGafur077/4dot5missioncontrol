package com.fourdotfive.missioncontrol.dtos.job;

import java.io.Serializable;

public class JobSalaryDto implements Serializable {

    private int maxSalary;
    private int minSalary;
    private String salaryType;
    private String salaryDuration;
    private String currency;

    public int getMaxSalary() {
        return maxSalary;
    }

    public void setMaxSalary(int maxSalary) {
        this.maxSalary = maxSalary;
    }

    public int getMinSalary() {
        return minSalary;
    }

    public void setMinSalary(int minSalary) {
        this.minSalary = minSalary;
    }

    public String getSalaryType() {
        return salaryType;
    }

    public void setSalaryType(String salaryType) {
        this.salaryType = salaryType;
    }

    public String getSalaryDuration() {
        return salaryDuration;
    }

    public void setSalaryDuration(String salaryDuration) {
        this.salaryDuration = salaryDuration;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}