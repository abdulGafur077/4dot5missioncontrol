package com.fourdotfive.missioncontrol.dtos.job;

import java.util.List;

public class MockJobProfileDto {

	private JobProfileCompanyDto company;

	private List<CompensationDto> educationList;

	private List<CompensationDto> experienceList;

	private List<CompensationDto> compensationList;

	private List<CompensationDto> technicalSkills;

	private List<CompensationDto> softSkills;

	public List<CompensationDto> getEducationList() {
		return educationList;
	}

	public void setEducationList(List<CompensationDto> educationList) {
		this.educationList = educationList;
	}

	public List<CompensationDto> getExperienceList() {
		return experienceList;
	}

	public void setExperienceList(List<CompensationDto> experienceList) {
		this.experienceList = experienceList;
	}

	public JobProfileCompanyDto getCompany() {
		return company;
	}

	public void setCompany(JobProfileCompanyDto company) {
		this.company = company;
	}

	public List<CompensationDto> getSoftSkills() {
		return softSkills;
	}

	public void setSoftSkills(List<CompensationDto> softSkills) {
		this.softSkills = softSkills;
	}

	public List<CompensationDto> getTechnicalSkills() {
		return technicalSkills;
	}

	public void setTechnicalSkills(List<CompensationDto> technicalSkills) {
		this.technicalSkills = technicalSkills;
	}

	public List<CompensationDto> getCompensationList() {
		return compensationList;
	}

	public void setCompensationList(List<CompensationDto> compensationList) {
		this.compensationList = compensationList;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MockJobProfileDto [company=");
		builder.append(company);
		builder.append(", educationList=");
		builder.append(educationList);
		builder.append(", experienceList=");
		builder.append(experienceList);
		builder.append(", compensationList=");
		builder.append(compensationList);
		builder.append(", technicalSkills=");
		builder.append(technicalSkills);
		builder.append(", softSkills=");
		builder.append(softSkills);
		builder.append("]");
		return builder.toString();
	}

}
