package com.fourdotfive.missioncontrol.dtos.job;

import com.fourdotfive.missioncontrol.pojo.job.CurrentJobState;
import com.fourdotfive.missioncontrol.pojo.job.JobOpening;
import com.fourdotfive.missioncontrol.pojo.user.Address;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class NewJobDto {

    private String requisitionActualNumber;

    private String requisitionNumber;

    private String requisitionRole;

    private String companyName;

    private String clientName;

    private String corporateName;

    private String corporateId;

    private String corporateBU;

    private Address location;

    private String description;

    private String jobType;

    private String openDate;

    private String sourcedDate;

    private String filledDate;

    private String requisitionStatus;

    private String jobCandidatesAvailable;

    private String jobOpeningsAvailable;

    private String noOfOpenings;

    private String noOfFilledOpenings;

    private List<String> recruiters = new ArrayList<>();

    private ActivePassiveTechAssessmentMasterDto techAssessmentList;

    private List<CurrentJobState> currentJobStateList = new ArrayList<>();

    private List<JobOpening> jobOpenings = new ArrayList<>();

    //private List<PerformanceSettings> performanceSettings;
    private String sourceMinValue;

    private String sourceMaxValue;

    private String fillMinValue;

    private String fillMaxValue;

    private int daysToSource;

    private int daysToFill;

    private String disableSourcedState;

    private String disableFilledState;

    private String disableJoinState;

    private String disableCompleteState;

    private String disablePausedState;

    private String disableCancelState;

    private String disableOpenState;

    private String disableFilledCandidate;

    private String disableCompletedCandidate;

    private String canEdit;

    private String canDelete;

    private int activeCandidateCount;

    private int passiveCandidateCount;

    private int inertCandidateCount;

    private Double fourDotFiveIntelligenceMarkerValue;

    private Double techAssessmentMarkerValue;

    private String markerValueOfCompnany;

    private String createdBy;

    private String noOfJustFilledOpenings;

    private String reqLabel;

    private Boolean isAutoMatedMatchSelected;

    private Boolean isAutoMatedMatchEnabled;

    private String clientOrBuId;

    //private NotificationPayload notifications;

    //private List<CurrentJobStateMinInfoDto> currentJobStateMinInfoDtos;

    private String recruiterObjectList;

    private boolean sharedRequisition;

    private List<HiringManagerDTO> hiringManagerDetails;

    private JobSalaryDto jobSalaryDto;

    private String[] sponsorships;

    private RequisitionOpeningsCount requisitionOpeningsCount;

    private List<RequisitionVendorsDetail> vendorsDetails;

    private Map<String, List<String>> assignedVendorsAndVendorRecruiters;

    private List<String> assignedHiringManagers;

    List<RecruiterMinInfo> corporateRecruiters;

    List<String> vendorRecruiters;

    private List<HiringManagerMinInfo> corporateHiringManagers;

    private List<HiringManagerMinInfo> vendorHiringManagers;

    Map<String, List<String>> otherVendorRecruiters;

    private Map<String, List<String>> otherHiringManagers;

    private String lastAddedCandidateSourceTypeId;

    private String pointOfContact;

    private String sharedClientBuName;

    private String techAssessmentId;

    private String techAssessmentName;

    private String valueAssessmentId;

    private String valueAssessmentName;

    private boolean hasSharedCorporateOrBUClient;

    public Map<String, List<String>> getAssignedVendorsAndVendorRecruiters() {
        return assignedVendorsAndVendorRecruiters;
    }

    public void setAssignedVendorsAndVendorRecruiters(Map<String, List<String>> assignedVendorsAndVendorRecruiters) {
        this.assignedVendorsAndVendorRecruiters = assignedVendorsAndVendorRecruiters;
    }

    public List<String> getAssignedHiringManagers() {
        return assignedHiringManagers;
    }

    public void setAssignedHiringManagers(List<String> assignedHiringManagers) {
        this.assignedHiringManagers = assignedHiringManagers;
    }

    public int getInertCandidateCount() {
        return inertCandidateCount;
    }

    public void setInertCandidateCount(int inertCandidateCount) {
        this.inertCandidateCount = inertCandidateCount;
    }

    public String getRecruiterObjectList() {
        return recruiterObjectList;
    }

    public void setRecruiterObjectList(String recruiterObjectList) {
        this.recruiterObjectList = recruiterObjectList;
    }


    public String getRequisitionNumber() {
        return requisitionNumber;
    }

    public void setRequisitionNumber(String requisitionNumber) {
        this.requisitionNumber = requisitionNumber;
    }

    public String getRequisitionActualNumber() {
        return requisitionActualNumber;
    }

    public void setRequisitionActualNumber(String requisitionActualNumber) {
        this.requisitionActualNumber = requisitionActualNumber;
    }

    public String getRequisitionRole() {
        return requisitionRole;
    }

    public void setRequisitionRole(String requisitionRole) {
        this.requisitionRole = requisitionRole;
    }

    public Address getLocation() {
        return location;
    }

    public void setLocation(Address location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public String getRequisitionStatus() {
        return requisitionStatus;
    }

    public void setRequisitionStatus(String requisitionStatus) {
        this.requisitionStatus = requisitionStatus;
    }

    public String getJobCandidatesAvailable() {
        return jobCandidatesAvailable;
    }

    public void setJobCandidatesAvailable(String jobCandidatesAvailable) {
        this.jobCandidatesAvailable = jobCandidatesAvailable;
    }

    public String getJobOpeningsAvailable() {
        return jobOpeningsAvailable;
    }

    public void setJobOpeningsAvailable(String jobOpeningsAvailable) {
        this.jobOpeningsAvailable = jobOpeningsAvailable;
    }

    public String getNoOfOpenings() {
        return noOfOpenings;
    }

    public void setNoOfOpenings(String noOfOpenings) {
        this.noOfOpenings = noOfOpenings;
    }

    public String getNoOfFilledOpenings() {
        return noOfFilledOpenings;
    }

    public void setNoOfFilledOpenings(String noOfFilledOpenings) {
        this.noOfFilledOpenings = noOfFilledOpenings;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public List<String> getRecruiters() {
        return recruiters;
    }

    public void setRecruiters(List<String> recruiters) {
        this.recruiters = recruiters;
    }

    public ActivePassiveTechAssessmentMasterDto getTechAssessmentList() {
        return techAssessmentList;
    }

    public void setTechAssessmentList(ActivePassiveTechAssessmentMasterDto techAssessmentList) {
        this.techAssessmentList = techAssessmentList;
    }

    public List<CurrentJobState> getCurrentJobStateList() {
        return currentJobStateList;
    }

    public void setCurrentJobStateList(List<CurrentJobState> currentJobStateList) {
        this.currentJobStateList = currentJobStateList;
    }

    public List<JobOpening> getJobOpenings() {
        return jobOpenings;
    }

    public void setJobOpenings(List<JobOpening> jobOpenings) {
        this.jobOpenings = jobOpenings;
    }

    public String getSourceMinValue() {
        return sourceMinValue;
    }

    public void setSourceMinValue(String sourceMinValue) {
        this.sourceMinValue = sourceMinValue;
    }

    public String getSourceMaxValue() {
        return sourceMaxValue;
    }

    public void setSourceMaxValue(String sourceMaxValue) {
        this.sourceMaxValue = sourceMaxValue;
    }

    public String getFillMinValue() {
        return fillMinValue;
    }

    public void setFillMinValue(String fillMinValue) {
        this.fillMinValue = fillMinValue;
    }

    public String getFillMaxValue() {
        return fillMaxValue;
    }

    public void setFillMaxValue(String fillMaxValue) {
        this.fillMaxValue = fillMaxValue;
    }

    public String getMaxValue() {
        return sourceMaxValue;
    }

    public void setMaxValue(String maxValue) {
        this.sourceMaxValue = maxValue;
    }

    public String getOpenDate() {
        return openDate;
    }

    public void setOpenDate(String openDate) {
        this.openDate = openDate;
    }

    public int getDaysToSource() {
        return daysToSource;
    }

    public void setDaysToSource(int daysToSource) {
        this.daysToSource = daysToSource;
    }

    public int getDaysToFill() {
        return daysToFill;
    }

    public void setDaysToFill(int daysToFill) {
        this.daysToFill = daysToFill;
    }

    public String getDisableSourcedState() {
        return disableSourcedState;
    }

    public void setDisableSourcedState(String disableSourcedState) {
        this.disableSourcedState = disableSourcedState;
    }

    public String getDisableFilledState() {
        return disableFilledState;
    }

    public void setDisableFilledState(String disableFilledState) {
        this.disableFilledState = disableFilledState;
    }

    public String getDisableCompleteState() {
        return disableCompleteState;
    }

    public void setDisableCompleteState(String disableCompleteState) {
        this.disableCompleteState = disableCompleteState;
    }

    public String getDisablePausedState() {
        return disablePausedState;
    }

    public void setDisablePausedState(String disablePausedState) {
        this.disablePausedState = disablePausedState;
    }

    public String getDisableCancelState() {
        return disableCancelState;
    }

    public void setDisableCancelState(String disableCancelState) {
        this.disableCancelState = disableCancelState;
    }

    public String getDisableOpenState() {
        return disableOpenState;
    }

    public void setDisableOpenState(String disableOpenState) {
        this.disableOpenState = disableOpenState;
    }

    public String getCanEdit() {
        return canEdit;
    }

    public void setCanEdit(String canEdit) {
        this.canEdit = canEdit;
    }

    public String getCanDelete() {
        return canDelete;
    }

    public void setCanDelete(String canDelete) {
        this.canDelete = canDelete;
    }

    public int getActiveCandidateCount() {
        return activeCandidateCount;
    }

    public void setActiveCandidateCount(int activeCandidateCount) {
        this.activeCandidateCount = activeCandidateCount;
    }

    public int getPassiveCandidateCount() {
        return passiveCandidateCount;
    }

    public void setPassiveCandidateCount(int passiveCandidateCount) {
        this.passiveCandidateCount = passiveCandidateCount;
    }

    public Double getFourDotFiveIntelligenceMarkerValue() {
        return fourDotFiveIntelligenceMarkerValue;
    }

    public void setFourDotFiveIntelligenceMarkerValue(Double fourDotFiveIntelligenceMarkerValue) {
        this.fourDotFiveIntelligenceMarkerValue = fourDotFiveIntelligenceMarkerValue;
    }

    public Double getTechAssessmentMarkerValue() {
        return techAssessmentMarkerValue;
    }

    public void setTechAssessmentMarkerValue(Double techAssessmentMarkerValue) {
        this.techAssessmentMarkerValue = techAssessmentMarkerValue;
    }

    public String getMarkerValueOfCompnany() {
        return markerValueOfCompnany;
    }

    public void setMarkerValueOfCompnany(String markerValueOfCompnany) {
        this.markerValueOfCompnany = markerValueOfCompnany;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getDisableFilledCandidate() {
        return disableFilledCandidate;
    }

    public void setDisableFilledCandidate(String disableFilledCandidate) {
        this.disableFilledCandidate = disableFilledCandidate;
    }

    public String getDisableCompletedCandidate() {
        return disableCompletedCandidate;
    }

    public void setDisableCompletedCandidate(String disableCompletedCandidate) {
        this.disableCompletedCandidate = disableCompletedCandidate;
    }

    public String getSourcedDate() {
        return sourcedDate;
    }

    public void setSourcedDate(String sourcedDate) {
        this.sourcedDate = sourcedDate;
    }

    public String getFilledDate() {
        return filledDate;
    }

    public void setFilledDate(String filledDate) {
        this.filledDate = filledDate;
    }

    public String getNoOfJustFilledOpenings() {
        return noOfJustFilledOpenings;
    }

    public void setNoOfJustFilledOpenings(String noOfJustFilledOpenings) {
        this.noOfJustFilledOpenings = noOfJustFilledOpenings;
    }

    public String getDisableJoinState() {
        return disableJoinState;
    }

    public void setDisableJoinState(String disableJoinState) {
        this.disableJoinState = disableJoinState;
    }

    public String getReqLabel() {
        return reqLabel;
    }

    public void setReqLabel(String reqLabel) {
        this.reqLabel = reqLabel;
    }

    public Boolean getAutoMatedMatchSelected() {
        return isAutoMatedMatchSelected;
    }

    public void setAutoMatedMatchSelected(Boolean autoMatedMatchSelected) {
        isAutoMatedMatchSelected = autoMatedMatchSelected;
    }

    public String getClientOrBuId() {
        return clientOrBuId;
    }

    public void setClientOrBuId(String clientOrBuId) {
        this.clientOrBuId = clientOrBuId;
    }

    public Boolean getAutoMatedMatchEnabled() {
        return isAutoMatedMatchEnabled;
    }

    public void setAutoMatedMatchEnabled(Boolean autoMatedMatchEnabled) {
        isAutoMatedMatchEnabled = autoMatedMatchEnabled;
    }

    public boolean isSharedRequisition() {
        return sharedRequisition;
    }

    public void setSharedRequisition(boolean sharedRequisition) {
        this.sharedRequisition = sharedRequisition;
    }

    public List<HiringManagerDTO> getHiringManagerDetails() {
        return hiringManagerDetails;
    }

    public void setHiringManagerDetails(List<HiringManagerDTO> hiringManagerDetails) {
        this.hiringManagerDetails = hiringManagerDetails;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getCorporateName() {
        return corporateName;
    }

    public void setCorporateName(String corporateName) {
        this.corporateName = corporateName;
    }

    public String getCorporateBU() {
        return corporateBU;
    }

    public void setCorporateBU(String corporateBU) {
        this.corporateBU = corporateBU;
    }

    public JobSalaryDto getJobSalaryDto() {
        return jobSalaryDto;
    }

    public void setJobSalaryDto(JobSalaryDto jobSalaryDto) {
        this.jobSalaryDto = jobSalaryDto;
    }

    public String[] getSponsorships() {
        return sponsorships;
    }

    public void setSponsorships(String[] sponsorships) {
        this.sponsorships = sponsorships;
    }

    public RequisitionOpeningsCount getRequisitionOpeningsCount() {
        return requisitionOpeningsCount;
    }

    public void setRequisitionOpeningsCount(RequisitionOpeningsCount requisitionOpeningsCount) {
        this.requisitionOpeningsCount = requisitionOpeningsCount;
    }

    public List<RequisitionVendorsDetail> getVendorsDetails() {
        return vendorsDetails;
    }

    public void setVendorsDetails(List<RequisitionVendorsDetail> vendorsDetails) {
        this.vendorsDetails = vendorsDetails;
    }

    public List<RecruiterMinInfo> getCorporateRecruiters() {
        return corporateRecruiters;
    }

    public void setCorporateRecruiters(List<RecruiterMinInfo> corporateRecruiters) {
        this.corporateRecruiters = corporateRecruiters;
    }

    public List<String> getVendorRecruiters() {
        return vendorRecruiters;
    }

    public void setVendorRecruiters(List<String> vendorRecruiters) {
        this.vendorRecruiters = vendorRecruiters;
    }

    public Map<String, List<String>> getOtherVendorRecruiters() {
        return otherVendorRecruiters;
    }

    public void setOtherVendorRecruiters(Map<String, List<String>> otherVendorRecruiters) {
        this.otherVendorRecruiters = otherVendorRecruiters;
    }

    public List<HiringManagerMinInfo> getCorporateHiringManagers() {
        return corporateHiringManagers;
    }

    public void setCorporateHiringManagers(List<HiringManagerMinInfo> corporateHiringManagers) {
        this.corporateHiringManagers = corporateHiringManagers;
    }

    public List<HiringManagerMinInfo> getVendorHiringManagers() {
        return vendorHiringManagers;
    }

    public void setVendorHiringManagers(List<HiringManagerMinInfo> vendorHiringManagers) {
        this.vendorHiringManagers = vendorHiringManagers;
    }

    public Map<String, List<String>> getOtherHiringManagers() {
        return otherHiringManagers;
    }

    public void setOtherHiringManagers(Map<String, List<String>> otherHiringManagers) {
        this.otherHiringManagers = otherHiringManagers;
    }

    public String getCorporateId() {
        return corporateId;
    }

    public void setCorporateId(String corporateId) {
        this.corporateId = corporateId;
    }

    public String getLastAddedCandidateSourceTypeId() {
        return lastAddedCandidateSourceTypeId;
    }

    public void setLastAddedCandidateSourceTypeId(String lastAddedCandidateSourceTypeId) {
        this.lastAddedCandidateSourceTypeId = lastAddedCandidateSourceTypeId;
    }

    public String getPointOfContact() {
        return pointOfContact;
    }

    public void setPointOfContact(String pointOfContact) {
        this.pointOfContact = pointOfContact;
    }

    public String getSharedClientBuName() {
        return sharedClientBuName;
    }

    public void setSharedClientBuName(String sharedClientBuName) {
        this.sharedClientBuName = sharedClientBuName;
    }

    public String getTechAssessmentId() {
        return techAssessmentId;
    }

    public void setTechAssessmentId(String techAssessmentId) {
        this.techAssessmentId = techAssessmentId;
    }

    public String getTechAssessmentName() {
        return techAssessmentName;
    }

    public void setTechAssessmentName(String techAssessmentName) {
        this.techAssessmentName = techAssessmentName;
    }

    public String getValueAssessmentId() {
        return valueAssessmentId;
    }

    public void setValueAssessmentId(String valueAssessmentId) {
        this.valueAssessmentId = valueAssessmentId;
    }

    public String getValueAssessmentName() {
        return valueAssessmentName;
    }

    public void setValueAssessmentName(String valueAssessmentName) {
        this.valueAssessmentName = valueAssessmentName;
    }

    public boolean isHasSharedCorporateOrBUClient() {
        return hasSharedCorporateOrBUClient;
    }

    public void setHasSharedCorporateOrBUClient(boolean hasSharedCorporateOrBUClient) {
        this.hasSharedCorporateOrBUClient = hasSharedCorporateOrBUClient;
    }
}
