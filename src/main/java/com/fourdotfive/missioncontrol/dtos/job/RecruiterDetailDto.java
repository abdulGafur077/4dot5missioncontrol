package com.fourdotfive.missioncontrol.dtos.job;

public class RecruiterDetailDto {
    private String firstName;
    private String lastName;
    private String id;
    private boolean isPrimary;
    private boolean isVendorRecruiter;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean getIsPrimary() {
        return isPrimary;
    }

    public void setIsPrimary(boolean isPrimary) {
        this.isPrimary = isPrimary;
    }

    public boolean isVendorRecruiter() {
        return isVendorRecruiter;
    }

    public void setVendorRecruiter(boolean vendorRecruiter) {
        isVendorRecruiter = vendorRecruiter;
    }

    @Override
    public String toString() {
        return "RecruiterDetailDto{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", id='" + id + '\'' +
                ", isPrimary=" + isPrimary +
                ", isVendorRecruiter=" + isVendorRecruiter +
                '}';
    }
}
