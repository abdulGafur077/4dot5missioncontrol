package com.fourdotfive.missioncontrol.dtos.job;

public class RecruiterMinInfo {

    private String name;
    private boolean isPoc;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getIsPoc() {
        return isPoc;
    }

    public void setPoc(boolean poc) {
        isPoc = poc;
    }
}
