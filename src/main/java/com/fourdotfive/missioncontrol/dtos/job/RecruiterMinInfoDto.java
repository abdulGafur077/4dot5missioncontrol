package com.fourdotfive.missioncontrol.dtos.job;

import java.io.Serializable;

import com.fourdotfive.missioncontrol.pojo.user.User;

public class RecruiterMinInfoDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String firstname;
	private String lastname;
	private String id;

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public RecruiterMinInfoDto(String firstname, String lastname, String id) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.id = id;
	}

	public RecruiterMinInfoDto() {
		super();
	}

	public static RecruiterMinInfoDto mapRecruiter(User user) {
		RecruiterMinInfoDto dto = null;
		if (user != null) {
			if (user.getLastname() != null) {
				dto = new RecruiterMinInfoDto(user.getFirstname(), user.getLastname(), user.getId());
			} else {
				dto = new RecruiterMinInfoDto(user.getFirstname(), null, user.getId());
			}
		} 
		return dto;
	}

}
