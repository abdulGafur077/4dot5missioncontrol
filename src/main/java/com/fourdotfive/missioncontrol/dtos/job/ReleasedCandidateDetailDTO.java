package com.fourdotfive.missioncontrol.dtos.job;

import com.fourdotfive.missioncontrol.dtos.candidate.UserDTO;

import java.util.List;

public class ReleasedCandidateDetailDTO {
    private String candidateId;
    private int numberOfMonthsToWait;
    private String releasedUpTo;
    private String role;
    private List<UserDTO> recruiters;
    private List<UserDTO> hiringManagers;
    private String releasedAt;
    private String requisitionNumber;

    public String getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(String candidateId) {
        this.candidateId = candidateId;
    }

    public int getNumberOfMonthsToWait() {
        return numberOfMonthsToWait;
    }

    public void setNumberOfMonthsToWait(int numberOfMonthsToWait) {
        this.numberOfMonthsToWait = numberOfMonthsToWait;
    }

    public String getReleasedUpTo() {
        return releasedUpTo;
    }

    public void setReleasedUpTo(String releasedUpTo) {
        this.releasedUpTo = releasedUpTo;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<UserDTO> getRecruiters() {
        return recruiters;
    }

    public void setRecruiters(List<UserDTO> recruiters) {
        this.recruiters = recruiters;
    }

    public List<UserDTO> getHiringManagers() {
        return hiringManagers;
    }

    public void setHiringManagers(List<UserDTO> hiringManagers) {
        this.hiringManagers = hiringManagers;
    }

    public String getReleasedAt() {
        return releasedAt;
    }

    public void setReleasedAt(String releasedAt) {
        this.releasedAt = releasedAt;
    }

    public String getRequisitionNumber() {
        return requisitionNumber;
    }

    public void setRequisitionNumber(String requisitionNumber) {
        this.requisitionNumber = requisitionNumber;
    }
}
