package com.fourdotfive.missioncontrol.dtos.job;

import javax.validation.constraints.NotNull;

public class RequisitionActivityFilterDto {

    @NotNull
    private String jobId;
    private String searchText;
    private int page;
    private int size;
    private String column;
    private String sortDirection;
    private String companyId;
    private String userId;
    private String activityMode;


    public RequisitionActivityFilterDto() {
    }

    public RequisitionActivityFilterDto(String jobId, String searchText, int page, int size, String column,
                                        String sortDirection, String companyId, String userId) {
        this.jobId = jobId;
        this.searchText = searchText;
        this.page = page;
        this.size = size;
        this.column = column;
        this.sortDirection = sortDirection;
        this.companyId = companyId;
        this.userId = userId;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getSortDirection() {
        return sortDirection;
    }

    public void setSortDirection(String sortDirection) {
        this.sortDirection = sortDirection;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getActivityMode() {
        return activityMode;
    }

    public void setActivityMode(String activityMode) {
        this.activityMode = activityMode;
    }
}
