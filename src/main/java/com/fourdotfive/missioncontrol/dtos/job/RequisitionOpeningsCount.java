package com.fourdotfive.missioncontrol.dtos.job;

public class RequisitionOpeningsCount {

    private int noOfOpenings;
    private int noOfFilled;
    private int noOfJoined;

    public int getNoOfOpenings() {
        return noOfOpenings;
    }

    public void setNoOfOpenings(int noOfOpenings) {
        this.noOfOpenings = noOfOpenings;
    }

    public int getNoOfFilled() {
        return noOfFilled;
    }

    public void setNoOfFilled(int noOfFilled) {
        this.noOfFilled = noOfFilled;
    }

    public int getNoOfJoined() {
        return noOfJoined;
    }

    public void setNoOfJoined(int noOfJoined) {
        this.noOfJoined = noOfJoined;
    }

}
