package com.fourdotfive.missioncontrol.dtos.job;

import java.util.List;

import com.fourdotfive.missioncontrol.pojo.candidate.Candidate;
import com.fourdotfive.missioncontrol.dtos.candidate.CandidateInfoDto;

public class RequisitionStatusResponse {

	private String requisitionOpeningNumber;
	private List<Candidate> resultentCandidates;
	private List<CandidateInfoDto> candidates;


	public String getRequisitionOpeningNumber() {
		return requisitionOpeningNumber;
	}

	public void setRequisitionOpeningNumber(String requisitionOpeningNumber) {
		this.requisitionOpeningNumber = requisitionOpeningNumber;
	}

	public List<Candidate> getResultentCandidates() {
		return resultentCandidates;
	}

	public void setResultentCandidates(List<Candidate> resultentCandidates) {
		this.resultentCandidates = resultentCandidates;
	}

	public List<CandidateInfoDto> getCandidates() {
		return candidates;
	}

	public void setCandidates(List<CandidateInfoDto> candidates) {
		this.candidates = candidates;
	}

}
