package com.fourdotfive.missioncontrol.dtos.job;

public enum RequisitionType {

    INTERNAL, EXTERNAL, SHARED

}
