package com.fourdotfive.missioncontrol.dtos.jobmatch;

import java.util.Map;

public class AssignRecruiterDto {
    private String userName;
    private Map<String, String> jobMatchAndRecruiterIds;

    public AssignRecruiterDto() {
    }

    public AssignRecruiterDto(String userName, Map<String, String> jobMatchAndRecruiterIds) {
        this.userName = userName;
        this.jobMatchAndRecruiterIds = jobMatchAndRecruiterIds;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Map<String, String> getJobMatchAndRecruiterIds() {
        return jobMatchAndRecruiterIds;
    }

    public void setJobMatchAndRecruiterIds(Map<String, String> jobMatchAndRecruiterIds) {
        this.jobMatchAndRecruiterIds = jobMatchAndRecruiterIds;
    }

    @Override
    public String toString() {
        return "AssignRecruiterDto{" +
                "userName='" + userName + '\'' +
                ", jobMatchAndRecruiterIds=" + jobMatchAndRecruiterIds +
                '}';
    }
}
