package com.fourdotfive.missioncontrol.dtos.jobmatch;

public class CandidateJobMatchInfoDto {
    private String jobMatchId;
    private String notes;
    private String loggedInUserId;
    private String companyId;
    private String externalNote;
    private String releaseNote;
    private String notInterestedNote;
    private ReasonCodeDTO reasonCodeDTO;

    public String getJobMatchId() {
        return jobMatchId;
    }

    public void setJobMatchId(String jobMatchId) {
        this.jobMatchId = jobMatchId;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getLoggedInUserId() {
        return loggedInUserId;
    }

    public void setLoggedInUserId(String loggedInUserId) {
        this.loggedInUserId = loggedInUserId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getExternalNote() {
        return externalNote;
    }

    public void setExternalNote(String externalNote) {
        this.externalNote = externalNote;
    }

    public String getReleaseNote() {
        return releaseNote;
    }

    public void setReleaseNote(String releaseNote) {
        this.releaseNote = releaseNote;
    }

    public String getNotInterestedNote() {
        return notInterestedNote;
    }

    public void setNotInterestedNote(String notInterestedNote) {
        this.notInterestedNote = notInterestedNote;
    }

    public ReasonCodeDTO getReasonCodeDTO() {
        return reasonCodeDTO;
    }

    public void setReasonCodeDTO(ReasonCodeDTO reasonCodeDTO) {
        this.reasonCodeDTO = reasonCodeDTO;
    }
}
