package com.fourdotfive.missioncontrol.dtos.jobmatch;


public class ContactsNotesDto {

    private String noteId;
    private String jobMatchId;
    private String contactMethod;
    private Integer duration;
    private String dateTime;
    private String note;
    private UserDto user;
    private String companyId;
    private String externalNote;

    public String getNoteId() {
        return noteId;
    }

    public void setNoteId(String noteId) {
        this.noteId = noteId;
    }

    public String getJobMatchId() {
        return jobMatchId;
    }

    public void setJobMatchId(String jobMatchId) {
        this.jobMatchId = jobMatchId;
    }

    public String getContactMethod() {
        return contactMethod;
    }

    public void setContactMethod(String contactMethod) {
        this.contactMethod = contactMethod;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getExternalNote() {
        return externalNote;
    }

    public void setExternalNote(String externalNote) {
        this.externalNote = externalNote;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ContactsNotesDto [noteId=");
        builder.append(noteId);
        builder.append(", jobMatchId=");
        builder.append(jobMatchId);
        builder.append(", contactMethod=");
        builder.append(contactMethod);
        builder.append(", duration=");
        builder.append(duration);
        builder.append(", dateTime=");
        builder.append(dateTime);
        builder.append(", note=");
        builder.append(note);
        builder.append(", user=");
        builder.append(user);
        builder.append("]");
        return builder.toString();
    }


}
