package com.fourdotfive.missioncontrol.dtos.jobmatch;


public class DeleteNoteDto {

    private String noteId;
    private String jobMatchId;
    private String userId;
    private String companyId;

    public DeleteNoteDto() {
    }

    public DeleteNoteDto(String noteId, String jobMatchId, String userId, String companyId) {
        this.noteId = noteId;
        this.jobMatchId = jobMatchId;
        this.userId = userId;
        this.companyId = companyId;
    }

    public String getNoteId() {
        return noteId;
    }

    public void setNoteId(String noteId) {
        this.noteId = noteId;
    }

    public String getJobMatchId() {
        return jobMatchId;
    }

    public void setJobMatchId(String jobMatchId) {
        this.jobMatchId = jobMatchId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
}
