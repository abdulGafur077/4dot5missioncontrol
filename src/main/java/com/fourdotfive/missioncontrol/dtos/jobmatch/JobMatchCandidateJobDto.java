package com.fourdotfive.missioncontrol.dtos.jobmatch;

public class JobMatchCandidateJobDto {

    private String jobMatchId;
    private String notes;
    private String companyId;
    private String externalNote;
    private ReasonCodeDTO reasonCodeDTO;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getJobMatchId() {
        return jobMatchId;
    }

    public void setJobMatchId(String jobMatchId) {
        this.jobMatchId = jobMatchId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getExternalNote() {
        return externalNote;
    }

    public void setExternalNote(String externalNote) {
        this.externalNote = externalNote;
    }

    public ReasonCodeDTO getReasonCodeDTO() {
        return reasonCodeDTO;
    }

    public void setReasonCodeDTO(ReasonCodeDTO reasonCodeDTO) {
        this.reasonCodeDTO = reasonCodeDTO;
    }
}
