package com.fourdotfive.missioncontrol.dtos.jobmatch;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class JobMatchFilterDto {


	private ArrayList<String> clientOrBU = new ArrayList<>();
	private ArrayList<String> recruiters = new ArrayList<>();
	private ArrayList<String> roleList = new ArrayList<>();
	private List<String> requisitionStates = new ArrayList<>();
	private ArrayList<String> requisitionNums = new ArrayList<>();
	private String firstName;
	private String lastName;
	private String phone;
	private String email;
	private ArrayList<String> workflowStates = new ArrayList<>();
	private ArrayList<String> workflowSteps = new ArrayList<>();
	private String searchString;
	private int pageNum;
	private int pageSize;
	private String column;
	private String sortDirection;
	private boolean filtered;
	private List<String> userIdScope = new ArrayList<>();
	private List<String> forwardedUserIds = new ArrayList<>();
	private boolean isUnAssignedCandidate = false;
	@JsonProperty("isAssignedToOthers")
	private boolean isAssignedToOthers;
    @JsonProperty("isAssignedToMe")
    private boolean isAssignedToMe;
    @JsonProperty("isUnassigned")
    private boolean isUnassigned;
	private List<String> vendors;


	public List<String> getForwardedUserIds() {
		return forwardedUserIds;
	}
	public void setForwardedUserIds(List<String> forwardedUserIds) {
		this.forwardedUserIds = forwardedUserIds;
	}
	public ArrayList<String> getClientOrBU() {
		return clientOrBU;
	}
	public void setClientOrBU(ArrayList<String> clientOrBU) {
		this.clientOrBU = clientOrBU;
	}
	public ArrayList<String> getRecruiters() {
		return recruiters;
	}
	public void setRecruiters(ArrayList<String> recruiters) {
		this.recruiters = recruiters;
	}
	public ArrayList<String> getRoleList() {
		return roleList;
	}
	public void setRoleList(ArrayList<String> roleList) {
		this.roleList = roleList;
	}
	public List<String> getRequisitionStates() {
		return requisitionStates;
	}
	public void setRequisitionStates(List<String> requisitionStates) {
		this.requisitionStates = requisitionStates;
	}
	public ArrayList<String> getRequisitionNums() {
		return requisitionNums;
	}
	public void setRequisitionNums(ArrayList<String> requisitionNums) {
		this.requisitionNums = requisitionNums;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public ArrayList<String> getWorkflowStates() {
		return workflowStates;
	}
	public void setWorkflowStates(ArrayList<String> workflowStates) {
		this.workflowStates = workflowStates;
	}
	public ArrayList<String> getWorkflowSteps() {
		return workflowSteps;
	}

	public void setWorkflowSteps(ArrayList<String> workflowSteps) {
		this.workflowSteps = workflowSteps;
	}
	public String getSearchString() {
		return searchString;
	}
	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}
	public int getPageNum() {
		return pageNum;
	}
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public String getColumn() {
		return column;
	}
	public void setColumn(String column) {
		this.column = column;
	}
	public String getSortDirection() {
		return sortDirection;
	}
	public void setSortDirection(String sortDirection) {
		this.sortDirection = sortDirection;
	}
	public boolean isFiltered() {
		return filtered;
	}
	public void setFiltered(boolean filtered) {
		this.filtered = filtered;
	}
	public List<String> getUserIdScope() {
		return userIdScope;
	}
	public void setUserIdScope(List<String> userIdScope) {
		this.userIdScope = userIdScope;
	}
	public boolean isUnAssignedCandidate() {
		return isUnAssignedCandidate;
	}
	public void setUnAssignedCandidate(boolean isUnAssignedCandidate) {
		this.isUnAssignedCandidate = isUnAssignedCandidate;
	}

	public boolean isAssignedToOthers() {
		return isAssignedToOthers;
    }

	public void setAssignedToOthers(boolean assignedToOthers) {
		isAssignedToOthers = assignedToOthers;
    }

    public boolean isAssignedToMe() {
        return isAssignedToMe;
    }

    public void setAssignedToMe(boolean assignedToMe) {
        isAssignedToMe = assignedToMe;
    }

    public boolean isUnassigned() {
        return isUnassigned;
    }

    public void setUnassigned(boolean unassigned) {
        isUnassigned = unassigned;
    }

	public List<String> getVendors() {
		return vendors;
	}

	public void setVendors(List<String> vendors) {
		this.vendors = vendors;
	}

	@Override
	public String toString() {
		return "JobMatchFilterDto [clientOrBU=" +
				clientOrBU +
				", recruiters=" +
				recruiters +
				", roleList=" +
				roleList +
				", requisitionStates=" +
				requisitionStates +
				", requisitionNums=" +
				requisitionNums +
				", firstName=" +
				firstName +
				", lastName=" +
				lastName +
				", phone=" +
				phone +
				", email=" +
				email +
				", workflowStates=" +
				workflowStates +
				", searchString=" +
				searchString +
				", pageNum=" +
				pageNum +
				", pageSize=" +
				pageSize +
				", column=" +
				column +
				", sortDirection=" +
				sortDirection +
				", filtered=" +
				filtered +
				", userIdScope=" +
				userIdScope +
				", isUnAssignedCandidate=" +
				isUnAssignedCandidate +
				"]";
	}
}
