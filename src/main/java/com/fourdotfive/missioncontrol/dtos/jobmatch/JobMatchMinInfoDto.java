package com.fourdotfive.missioncontrol.dtos.jobmatch;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
//import com.fourdotfive.missioncontrol.dtos.jobmatch;

//import org.joda.time.DateTime;

import com.fourdotfive.missioncontrol.pojo.candidate.Contact;
import com.fourdotfive.missioncontrol.pojo.job.JobMatch;
import com.fourdotfive.missioncontrol.pojo.job.JobmatchStatus;
import com.fourdotfive.missioncontrol.pojo.job.Job;
import com.fourdotfive.missioncontrol.dtos.job.RecruiterMinInfoDto;
import com.fourdotfive.missioncontrol.dtos.job.JobMinInfoDto;


public class JobMatchMinInfoDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//private DateTime date;

	private JobMinInfoDto job;

	private Double score;

	private String id;

	//private DateTime createdDate;
	
	private CandidateJobMatchMinInfoDto candidate;

	private RecruiterMinInfoDto assignedRecruiter;

	private JobmatchStatus jobMatchStatus;

	private String currentStep;

	private String currentState;
	
	private Job newJob;

	public RecruiterMinInfoDto getAssignedRecruiter() {
		return assignedRecruiter;
	}

	public void setAssignedRecruiter(RecruiterMinInfoDto assignedRecruiter) {
		this.assignedRecruiter = assignedRecruiter;
	}

	/**
	 * returns the document id
	 * 
	 * @return String
	 * 
	 */
	public String getId() {
		return id;
	}

	/**
	 * sets the document id
	 * 
	 * @param id
	 *            String
	 * 
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Returns the creation date of the entity.
	 * 
	 * @return the createdDate
	 */
//	public DateTime getCreatedDate() {
//		return this.createdDate;
//	}
//
//	/**
//	 * Sets the creation date of the entity.
//	 *
//	 * @param creationDate
//	 *            the creation date to set
//	 */
//	public void setCreatedDate(final DateTime creationDate) {
//		this.createdDate = creationDate;
//	}

	/**
	 * get date
	 * 
	 * @return the date
	 *
	 */
//	public DateTime getDate() {
//		return date;
//	}
//
//	/**
//	 * set date
//	 *
//	 * @param date
//	 *            the date to set
//	 *
//	 */
//	public void setDate(DateTime date) {
//		this.date = date;
//	}

	/**
	 * get job
	 * 
	 * @return the job
	 *
	 */
	public JobMinInfoDto getJob() {
		return job;
	}

	/**
	 * set job
	 * 
	 * @param job
	 *            the job to set
	 *
	 */
	public void setJob(JobMinInfoDto job) {
		this.job = job;
	}

	/**
	 * get score
	 * 
	 * @return the score
	 *
	 */
	public Double getScore() {
		return score;
	}

	/**
	 * set score
	 * 
	 * @param score
	 *            the score to set
	 *
	 */
	public void setScore(Double score) {
		this.score = score;
	}

	public List<JobMatchMinInfoDto> mapFromJobMatches(List<JobMatch> jobMatchs) {
		List<JobMatchMinInfoDto> dtos = new ArrayList<>();

		for (JobMatch jobMatch : jobMatchs) {
			dtos.add(mapFromJobMatch(jobMatch));
		}
		return dtos;
	}

	public static JobMatchMinInfoDto mapFromJobMatch(JobMatch jobMatch) {
		JobMatchMinInfoDto jobMatchMinInfoDto = new JobMatchMinInfoDto();
		//jobMatchMinInfoDto.setCreatedDate(jobMatch.getCreatedDate());
		//jobMatchMinInfoDto.setDate(jobMatch.getDate());
		jobMatchMinInfoDto.setId(jobMatch.getId());
		jobMatchMinInfoDto.setScore(jobMatch.getScore());
		jobMatchMinInfoDto.setAssignedRecruiter(RecruiterMinInfoDto.mapRecruiter(jobMatch.getAssignedRecruiter()));
		jobMatchMinInfoDto.setJob(JobMinInfoDto.mapFromJob(jobMatch.getJob()));
		jobMatchMinInfoDto.setJobMatchStatus(jobMatch.getJobMatchStatus());
		jobMatchMinInfoDto.setCurrentState(jobMatch.getCurrentState());
		jobMatchMinInfoDto.setCurrentStep(jobMatch.getCurrentStep());
		return jobMatchMinInfoDto;
	}

	public JobmatchStatus getJobMatchStatus() {
		return jobMatchStatus;
	}

	public void setJobMatchStatus(JobmatchStatus jobMatchStatus) {
		this.jobMatchStatus = jobMatchStatus;
	}

	public String getCurrentStep() {
		return currentStep;
	}

	public void setCurrentStep(String currentStep) {
		this.currentStep = currentStep;
	}

	public String getCurrentState() {
		return currentState;
	}

	public void setCurrentState(String currentState) {
		this.currentState = currentState;
	}

	public CandidateJobMatchMinInfoDto getCandidate() {
		return candidate;
	}

	public void setCandidate(CandidateJobMatchMinInfoDto candidate) {
		this.candidate = candidate;
	}

	public Job getNewJob() {
		return newJob;
	}

	public void setNewJob(Job newJob) {
		this.newJob = newJob;
	}


}