package com.fourdotfive.missioncontrol.dtos.jobmatch;

import com.fourdotfive.missioncontrol.dtos.candidate.CandidateWorkflowCardDto;

import java.util.List;

public class JobMatchWorkflowDto {
    private String recruiterScreeningInfo;
	private String qualifiedInfo;
	private String techAssessmentInfo;
	private String valueAssessmentInfo;
	private String verificationInfo;
	private String phoneScreenInfo;
	private String interviewInfo;
	private String likelyInfo;
	private String notInterestedInfo;
	private String releasedInfo;
    private int totalCardCount;

    public String getRecruiterScreeningInfo() {
        return recruiterScreeningInfo;
    }

    public void setRecruiterScreeningInfo(String recruiterScreeningInfo) {
        this.recruiterScreeningInfo = recruiterScreeningInfo;
	}

	public String getQualifiedInfo() {
		return qualifiedInfo;
	}

	public void setQualifiedInfo(String qualifiedInfo) {
		this.qualifiedInfo = qualifiedInfo;
	}

	public String getTechAssessmentInfo() {
		return techAssessmentInfo;
	}

	public void setTechAssessmentInfo(String techAssessmentInfo) {
		this.techAssessmentInfo = techAssessmentInfo;
	}

	public String getValueAssessmentInfo() {
		return valueAssessmentInfo;
	}

	public void setValueAssessmentInfo(String valueAssessmentInfo) {
		this.valueAssessmentInfo = valueAssessmentInfo;
	}

	public String getVerificationInfo() {
		return verificationInfo;
	}

	public void setVerificationInfo(String verificationInfo) {
		this.verificationInfo = verificationInfo;
	}

	public String getPhoneScreenInfo() {
		return phoneScreenInfo;
	}

	public void setPhoneScreenInfo(String phoneScreenInfo) {
		this.phoneScreenInfo = phoneScreenInfo;
	}

	public String getInterviewInfo() {
		return interviewInfo;
	}

	public void setInterviewInfo(String interviewInfo) {
		this.interviewInfo = interviewInfo;
	}

	public String getLikelyInfo() {
		return likelyInfo;
	}

	public void setLikelyInfo(String likelyInfo) {
		this.likelyInfo = likelyInfo;
	}

	public String getNotInterestedInfo() {
		return notInterestedInfo;
	}

	public void setNotInterestedInfo(String notInterestedInfo) {
		this.notInterestedInfo = notInterestedInfo;
	}

	public String getReleasedInfo() {
		return releasedInfo;
	}

	public void setReleasedInfo(String releasedInfo) {
		this.releasedInfo = releasedInfo;
	}

    public int getTotalCardCount() {
        return totalCardCount;
    }

    public void setTotalCardCount(int totalCardCount) {
        this.totalCardCount = totalCardCount;
    }

}
