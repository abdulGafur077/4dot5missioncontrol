package com.fourdotfive.missioncontrol.dtos.jobmatch;

public class PhoneScreenInterviewEmailDeliveryInfo {

    private boolean isAllMailFailedToDeliver;
    private boolean isSomeMailFailedToDeliver;
    private boolean isNoMailFailedToDeliver;

    public boolean isAllMailFailedToDeliver() {
        return isAllMailFailedToDeliver;
    }

    public void setAllMailFailedToDeliver(boolean allMailFailedToDeliver) {
        isAllMailFailedToDeliver = allMailFailedToDeliver;
    }

    public boolean isSomeMailFailedToDeliver() {
        return isSomeMailFailedToDeliver;
    }

    public void setSomeMailFailedToDeliver(boolean someMailFailedToDeliver) {
        isSomeMailFailedToDeliver = someMailFailedToDeliver;
    }

    public boolean isNoMailFailedToDeliver() {
        return isNoMailFailedToDeliver;
    }

    public void setNoMailFailedToDeliver(boolean noMailFailedToDeliver) {
        isNoMailFailedToDeliver = noMailFailedToDeliver;
    }
}
