package com.fourdotfive.missioncontrol.dtos.jobmatch;

import java.util.List;

public class ReasonNoteDTO {

    private List<ReasonCodeDTO> reasonCodeDTOs;
    private String companyId;

    public List<ReasonCodeDTO> getReasonCodeDTOs() {
        return reasonCodeDTOs;
    }

    public void setReasonCodeDTOs(List<ReasonCodeDTO> reasonCodeDTOs) {
        this.reasonCodeDTOs = reasonCodeDTOs;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
}
