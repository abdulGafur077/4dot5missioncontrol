package com.fourdotfive.missioncontrol.dtos.login;

import com.fourdotfive.missioncontrol.login.SetPasswordEmailType;

public class PasswordDetailsDto {
    private String token;
    private String newPassword;
    private String confirmPassword;
    private SetPasswordEmailType actionType;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public SetPasswordEmailType getActionType() {
        return actionType;
    }

    public void setActionType(SetPasswordEmailType actionType) {
        this.actionType = actionType;
    }
}
