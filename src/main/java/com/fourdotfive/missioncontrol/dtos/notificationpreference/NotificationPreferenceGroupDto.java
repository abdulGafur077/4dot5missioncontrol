package com.fourdotfive.missioncontrol.dtos.notificationpreference;

import java.util.List;

import com.fourdotfive.missioncontrol.pojo.notificationpreference.NotificationGroupEnum;

public class NotificationPreferenceGroupDto {
	private String name;

	private NotificationGroupEnum notificationGroupEnum;

	private List<NotificationPreferencesListDto> notificationPreferencesDtos;

	private List<NotificationPrefsDetailsDto> orderedNotificationPreferences;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<NotificationPreferencesListDto> getNotificationPreferencesDtos() {
		return notificationPreferencesDtos;
	}

	public void setNotificationPreferencesDtos(List<NotificationPreferencesListDto> notificationPreferencesDtos) {
		this.notificationPreferencesDtos = notificationPreferencesDtos;
	}

	public NotificationGroupEnum getNotificationGroupEnum() {
		return notificationGroupEnum;
	}

	public void setNotificationGroupEnum(NotificationGroupEnum notificationGroupEnum) {
		this.notificationGroupEnum = notificationGroupEnum;
	}

	public List<NotificationPrefsDetailsDto> getOrderedNotificationPreferences() {
		return orderedNotificationPreferences;
	}

	public void setOrderedNotificationPreferences(List<NotificationPrefsDetailsDto> orderedNotificationPreferences) {
		this.orderedNotificationPreferences = orderedNotificationPreferences;
	}
}
