package com.fourdotfive.missioncontrol.dtos.notificationpreference;

import com.fourdotfive.missioncontrol.pojo.notificationpreference.NotificationPreferenceType;

public class NotificationPreferencesListDto {

	private String name;

	private boolean isEnabledForApp;

	private boolean isEnabledForEmail;

	private boolean isEnabledForText;

	private boolean isEnabled;

	private boolean isOnForApp;

	private boolean isOnForEmail;

	private boolean isOnForText;

	private String message;

	private String notificationPreferenceType;

	public String getNotificationPreferenceType() {
		return notificationPreferenceType;
	}

	public void setNotificationPreferenceType(String notificationPreferenceType) {
		this.notificationPreferenceType = notificationPreferenceType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isEnabledForApp() {
		return isEnabledForApp;
	}

	public void setEnabledForApp(boolean isEnabledForApp) {
		this.isEnabledForApp = isEnabledForApp;
	}

	public boolean isEnabledForEmail() {
		return isEnabledForEmail;
	}

	public void setEnabledForEmail(boolean isEnabledForEmail) {
		this.isEnabledForEmail = isEnabledForEmail;
	}

	public boolean isEnabledForText() {
		return isEnabledForText;
	}

	public void setEnabledForText(boolean isEnabledForText) {
		this.isEnabledForText = isEnabledForText;
	}

	public boolean isEnabled() {
		return isEnabled;
	}

	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	public boolean isOnForApp() {
		return isOnForApp;
	}

	public void setOnForApp(boolean isOnForApp) {
		this.isOnForApp = isOnForApp;
	}

	public boolean isOnForEmail() {
		return isOnForEmail;
	}

	public void setOnForEmail(boolean isOnForEmail) {
		this.isOnForEmail = isOnForEmail;
	}

	public boolean isOnForText() {
		return isOnForText;
	}

	public void setOnForText(boolean isOnForText) {
		this.isOnForText = isOnForText;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
