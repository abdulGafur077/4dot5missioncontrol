package com.fourdotfive.missioncontrol.dtos.notificationpreference;

import java.util.List;

public class NotificationPrefsDetailsDto {
	private String notificationPreferenceType;
	private List<NotificationPreferencesListDto> notificationPreferencesListDtos;

	public String getNotificationPreferenceType() {
		return notificationPreferenceType;
	}

	public void setNotificationPreferenceType(String notificationPreferenceType) {
		this.notificationPreferenceType = notificationPreferenceType;
	}

	public List<NotificationPreferencesListDto> getNotificationPreferencesListDtos() {
		return notificationPreferencesListDtos;
	}

	public void setNotificationPreferencesListDtos(List<NotificationPreferencesListDto> notificationPreferencesListDtos) {
		this.notificationPreferencesListDtos = notificationPreferencesListDtos;
	}
}
