package com.fourdotfive.missioncontrol.dtos.notificationpreference;

import java.util.List;

import com.fourdotfive.missioncontrol.dtos.user.UserDto;

public class NotificationPrefsDto {

	private String id;
	private UserDto user;
	private List<NotificationPreferenceGroupDto> groups;

	public UserDto getUser() {
		return user;
	}

	public void setUser(UserDto user) {
		this.user = user;
	}

	public List<NotificationPreferenceGroupDto> getGroups() {
		return groups;
	}

	public void setGroups(List<NotificationPreferenceGroupDto> groups) {
		this.groups = groups;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
