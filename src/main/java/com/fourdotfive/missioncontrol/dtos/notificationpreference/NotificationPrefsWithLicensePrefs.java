package com.fourdotfive.missioncontrol.dtos.notificationpreference;

import java.util.List;

import com.fourdotfive.missioncontrol.licensepreference.LicensePrefDtoList;
import com.fourdotfive.missioncontrol.pojo.notificationpreference.NotificationPreferenceGroup;

public class NotificationPrefsWithLicensePrefs {

	private List<NotificationPreferenceGroup> groups;

	private LicensePrefDtoList licensePrefDtoList;

	public List<NotificationPreferenceGroup> getGroups() {
		return groups;
	}

	public void setGroups(List<NotificationPreferenceGroup> groups) {
		this.groups = groups;
	}

	public LicensePrefDtoList getLicensePrefDtoList() {
		return licensePrefDtoList;
	}

	public void setLicensePrefDtoList(LicensePrefDtoList licensePrefDtoList) {
		this.licensePrefDtoList = licensePrefDtoList;
	}

	public NotificationPrefsWithLicensePrefs(
			List<NotificationPreferenceGroup> groups,
			LicensePrefDtoList licensePrefDtoList) {
		super();
		this.groups = groups;
		this.licensePrefDtoList = licensePrefDtoList;
	}

}
