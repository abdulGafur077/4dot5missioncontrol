package com.fourdotfive.missioncontrol.dtos.pdf;


import java.util.List;

public class AdditionalAssessmentInfoDto {
    private String type;
    private String reason;
    private List<CommentDto> commentList;
    private boolean isViolation;
    private String commentAddedOn;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public List<CommentDto> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<CommentDto> commentList) {
        this.commentList = commentList;
    }

    public boolean getIsViolation() {
        return isViolation;
    }

    public void setIsViolation(boolean isViolation) {
        this.isViolation = isViolation;
    }

    public String getCommentAddedOn() {
        return commentAddedOn;
    }

    public void setCommentAddedOn(String commentAddedOn) {
        this.commentAddedOn = commentAddedOn;
    }
}
