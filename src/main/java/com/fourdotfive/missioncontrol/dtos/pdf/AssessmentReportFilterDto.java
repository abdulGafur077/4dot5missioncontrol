package com.fourdotfive.missioncontrol.dtos.pdf;

public class AssessmentReportFilterDto {
    private String companyId;
    private String searchText;
    private String startDate;
    private String endDate;
    private String loggedInUserId;
    private int page;
    private int size;
    private String column;
    private String sortDir;
    private boolean isVendorCandidates;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getLoggedInUserId() {
        return loggedInUserId;
    }

    public void setLoggedInUserId(String loggedInUserId) {
        this.loggedInUserId = loggedInUserId;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getSortDir() {
        return sortDir;
    }

    public void setSortDir(String sortDir) {
        this.sortDir = sortDir;
    }

    public boolean getIsVendorCandidates() {
        return isVendorCandidates;
    }

    public void setVendorCandidates(boolean vendorCandidates) {
        isVendorCandidates = vendorCandidates;
    }
}
