package com.fourdotfive.missioncontrol.dtos.pdf;


public class AssessmentReportInfoDto {
    private String type;
    private String reason;
    private CommentDto commentDto;
    private boolean isViolation;
    private boolean addProctoringImagesToReport;

    public AssessmentReportInfoDto() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public CommentDto getCommentDto() {
        return commentDto;
    }

    public void setCommentDto(CommentDto commentDto) {
        this.commentDto = commentDto;
    }

    public boolean getIsViolation() {
        return isViolation;
    }

    public void setViolation(boolean violation) {
        isViolation = violation;
    }

    public boolean getAddProctoringImagesToReport() {
        return addProctoringImagesToReport;
    }

    public void setAddProctoringImagesToReport(boolean addProctoringImagesToReport) {
        this.addProctoringImagesToReport = addProctoringImagesToReport;
    }
}
