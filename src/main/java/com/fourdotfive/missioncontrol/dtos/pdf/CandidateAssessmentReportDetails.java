package com.fourdotfive.missioncontrol.dtos.pdf;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class CandidateAssessmentReportDetails {
    private String candidateAssessmentDetailId;
    private String testInvitationId;
    private String appearedOn;
    private String candidateName;
    private String email;
    private String testName;
    private String status;
    private String score;
    private String pdfReportURL;
    @JsonProperty("imageProctoring")
    private boolean imageProctoring;
    @JsonProperty("addProctoringImagesToReport")
    private boolean addProctoringImagesToReport;
    private AdditionalAssessmentInfoDto additionalAssessmentInfo;
    private String requisitionNumber;
    private List<AssessmentReportInfoDto> assessmentReportInfoList = new ArrayList<>();

    public String getCandidateAssessmentDetailId() {
        return candidateAssessmentDetailId;
    }

    public void setCandidateAssessmentDetailId(String candidateAssessmentDetailId) {
        this.candidateAssessmentDetailId = candidateAssessmentDetailId;
    }

    public String getTestInvitationId() {
        return testInvitationId;
    }

    public void setTestInvitationId(String testInvitationId) {
        this.testInvitationId = testInvitationId;
    }

    public String getAppearedOn() {
        return appearedOn;
    }

    public void setAppearedOn(String appearedOn) {
        this.appearedOn = appearedOn;
    }

    public String getCandidateName() {
        return candidateName;
    }

    public void setCandidateName(String candidateName) {
        this.candidateName = candidateName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getPdfReportURL() {
        return pdfReportURL;
    }

    public void setPdfReportURL(String pdfReportURL) {
        this.pdfReportURL = pdfReportURL;
    }

    public boolean isImageProctoring() {
        return imageProctoring;
    }

    public void setImageProctoring(boolean imageProctoring) {
        this.imageProctoring = imageProctoring;
    }

    public AdditionalAssessmentInfoDto getAdditionalAssessmentInfo() {
        return additionalAssessmentInfo;
    }

    public void setAdditionalAssessmentInfo(AdditionalAssessmentInfoDto additionalAssessmentInfo) {
        this.additionalAssessmentInfo = additionalAssessmentInfo;
    }

    public boolean isAddProctoringImagesToReport() {
        return addProctoringImagesToReport;
    }

    public void setAddProctoringImagesToReport(boolean addProctoringImagesToReport) {
        this.addProctoringImagesToReport = addProctoringImagesToReport;
    }

    public String getRequisitionNumber() {
        return requisitionNumber;
    }

    public void setRequisitionNumber(String requisitionNumber) {
        this.requisitionNumber = requisitionNumber;
    }

    public List<AssessmentReportInfoDto> getAssessmentReportInfoList() {
        return assessmentReportInfoList;
    }

    public void setAssessmentReportInfoList(List<AssessmentReportInfoDto> assessmentReportInfoList) {
        this.assessmentReportInfoList = assessmentReportInfoList;
    }
}
