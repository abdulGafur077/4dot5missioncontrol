package com.fourdotfive.missioncontrol.dtos.pdf;

public class FileDto {
    private String fileName;
    private byte[] fileData;

    public FileDto() {
    }

    public FileDto(String fileName, byte[] fileData) {
        this.fileName = fileName;
        this.fileData = fileData;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public byte[] getFileData() {
        return fileData;
    }

    public void setFileData(byte[] fileData) {
        this.fileData = fileData;
    }
}
