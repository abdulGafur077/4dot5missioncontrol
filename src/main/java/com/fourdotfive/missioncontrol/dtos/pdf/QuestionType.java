package com.fourdotfive.missioncontrol.dtos.pdf;

public enum QuestionType {

    ALL("all"), SOME("some"), NONE("none");

    private String name;

    QuestionType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
