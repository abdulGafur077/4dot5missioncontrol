package com.fourdotfive.missioncontrol.dtos.plan;

public class PlanDetailDto {

    private String id;
    private String planHeaderId;
    private int taAllocated;
    private int taUsed;
    private int taAdded;
    private int jbiAllocated;
    private int jbiUsed;
    private int jbiAdded;

    public PlanDetailDto() {
    }


    public PlanDetailDto(String id, String planHeaderId, int taAllocated, int taUsed, int taAdded, int jbiAllocated
            , int jbiUsed, int jbiAdded) {
        this.id = id;
        this.planHeaderId = planHeaderId;
        this.taAllocated = taAllocated;
        this.taUsed = taUsed;
        this.taAdded = taAdded;
        this.jbiAllocated = jbiAllocated;
        this.jbiUsed = jbiUsed;
        this.jbiAdded = jbiAdded;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlanHeaderId() {
        return planHeaderId;
    }

    public void setPlanHeaderId(String planHeaderId) {
        this.planHeaderId = planHeaderId;
    }

    public int getTaAllocated() {
        return taAllocated;
    }

    public void setTaAllocated(int taAllocated) {
        this.taAllocated = taAllocated;
    }

    public int getTaUsed() {
        return taUsed;
    }

    public void setTaUsed(int taUsed) {
        this.taUsed = taUsed;
    }

    public int getTaAdded() {
        return taAdded;
    }

    public void setTaAdded(int taAdded) {
        this.taAdded = taAdded;
    }

    public int getJbiAllocated() {
        return jbiAllocated;
    }

    public void setJbiAllocated(int jbiAllocated) {
        this.jbiAllocated = jbiAllocated;
    }

    public int getJbiUsed() {
        return jbiUsed;
    }

    public void setJbiUsed(int jbiUsed) {
        this.jbiUsed = jbiUsed;
    }

    public int getJbiAdded() {
        return jbiAdded;
    }

    public void setJbiAdded(int jbiAdded) {
        this.jbiAdded = jbiAdded;
    }
}
