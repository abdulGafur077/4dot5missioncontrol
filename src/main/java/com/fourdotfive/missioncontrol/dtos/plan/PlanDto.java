package com.fourdotfive.missioncontrol.dtos.plan;

public class PlanDto {

    private PlanHeaderDto planHeaderDto;
    private PlanDetailDto planDetailDto;
    private String loggedInUserId;

    public PlanDto() {
    }

    public PlanDto(PlanHeaderDto planHeaderDto, PlanDetailDto planDetailDto) {
        this.planHeaderDto = planHeaderDto;
        this.planDetailDto = planDetailDto;
    }

    public PlanHeaderDto getPlanHeaderDto() {
        return planHeaderDto;
    }

    public void setPlanHeaderDto(PlanHeaderDto planHeaderDto) {
        this.planHeaderDto = planHeaderDto;
    }

    public PlanDetailDto getPlanDetailDto() {
        return planDetailDto;
    }

    public void setPlanDetailDto(PlanDetailDto planDetailDto) {
        this.planDetailDto = planDetailDto;
    }

    public String getLoggedInUserId() {
        return loggedInUserId;
    }

    public void setLoggedInUserId(String loggedInUserId) {
        this.loggedInUserId = loggedInUserId;
    }
}
