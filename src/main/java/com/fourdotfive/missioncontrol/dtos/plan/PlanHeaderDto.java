package com.fourdotfive.missioncontrol.dtos.plan;

public class PlanHeaderDto {

    private String id;
    private String planFeaturesCapabilityId;
    private String companyId;
    private String planType;
    private String planStartDate;
    private String planEndDate;
    private String planState;
    private String planName;
    private String timeZoneName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlanFeaturesCapabilityId() {
        return planFeaturesCapabilityId;
    }

    public void setPlanFeaturesCapabilityId(String planFeaturesCapabilityId) {
        this.planFeaturesCapabilityId = planFeaturesCapabilityId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getPlanType() {
        return planType;
    }

    public void setPlanType(String planType) {
        this.planType = planType;
    }

    public String getPlanStartDate() {
        return planStartDate;
    }

    public void setPlanStartDate(String planStartDate) {
        this.planStartDate = planStartDate;
    }

    public String getPlanEndDate() {
        return planEndDate;
    }

    public void setPlanEndDate(String planEndDate) {
        this.planEndDate = planEndDate;
    }

    public String getPlanState() {
        return planState;
    }

    public void setPlanState(String planState) {
        this.planState = planState;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getTimeZoneName() {
        return timeZoneName;
    }

    public void setTimeZoneName(String timeZoneName) {
        this.timeZoneName = timeZoneName;
    }
}
