package com.fourdotfive.missioncontrol.dtos.plan;

public class PlanMinDto {

    private String planHeaderId;
    private String planName;
    private String companyId;
    private String planStartDate;
    private String planEndDate;
    private String message;
    private String planType;
    private String timeZoneName;

    public PlanMinDto() {
    }

    public String getPlanHeaderId() {
        return planHeaderId;
    }

    public void setPlanHeaderId(String planHeaderId) {
        this.planHeaderId = planHeaderId;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getPlanStartDate() {
        return planStartDate;
    }

    public void setPlanStartDate(String planStartDate) {
        this.planStartDate = planStartDate;
    }

    public String getPlanEndDate() {
        return planEndDate;
    }

    public void setPlanEndDate(String planEndDate) {
        this.planEndDate = planEndDate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPlanType() {
        return planType;
    }

    public void setPlanType(String planType) {
        this.planType = planType;
    }

    public String getTimeZoneName() {
        return timeZoneName;
    }

    public void setTimeZoneName(String timeZoneName) {
        this.timeZoneName = timeZoneName;
    }
}
