package com.fourdotfive.missioncontrol.dtos.requisition;

import java.util.List;
import java.util.Map;

public class ActivityDto {

    private String requisitionNumber;
    private String requisitionId;
    private String candidateId;
    private String title;
    private String activityType;
    private String fromState;
    private String toState;
    private String createdBy;
    private String createdDate;
    private String note;
    private String count;
    private List<String> jobBoardNames;
    private Map<String, String> jobBoardResumesCountMap;
    private List<String> candidateNameList;
    private String companyName;
    private String externalNote;
    private String reasonNote;
    private String date;

    public String getRequisitionNumber() {
        return requisitionNumber;
    }

    public void setRequisitionNumber(String requisitionNumber) {
        this.requisitionNumber = requisitionNumber;
    }

    public String getRequisitionId() {
        return requisitionId;
    }

    public void setRequisitionId(String requisitionId) {
        this.requisitionId = requisitionId;
    }

    public String getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(String candidateId) {
        this.candidateId = candidateId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public String getFromState() {
        return fromState;
    }

    public void setFromState(String fromState) {
        this.fromState = fromState;
    }

    public String getToState() {
        return toState;
    }

    public void setToState(String toState) {
        this.toState = toState;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public List<String> getJobBoardNames() {
        return jobBoardNames;
    }

    public void setJobBoardNames(List<String> jobBoardNames) {
        this.jobBoardNames = jobBoardNames;
    }

    public Map<String, String> getJobBoardResumesCountMap() {
        return jobBoardResumesCountMap;
    }

    public void setJobBoardResumesCountMap(Map<String, String> jobBoardResumesCountMap) {
        this.jobBoardResumesCountMap = jobBoardResumesCountMap;
    }

    public List<String> getCandidateNameList() {
        return candidateNameList;
    }

    public void setCandidateNameList(List<String> candidateNameList) {
        this.candidateNameList = candidateNameList;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getExternalNote() {
        return externalNote;
    }

    public void setExternalNote(String externalNote) {
        this.externalNote = externalNote;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getReasonNote() {
        return reasonNote;
    }

    public void setReasonNote(String reasonNote) {
        this.reasonNote = reasonNote;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
