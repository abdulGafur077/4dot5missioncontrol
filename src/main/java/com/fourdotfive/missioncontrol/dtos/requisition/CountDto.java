package com.fourdotfive.missioncontrol.dtos.requisition;

public class CountDto {

	private int activeCandidatesCount;
	private int passiveCandidatesCount;
	private int matchedCandidateCount;
	private int unMatchedCandidateCount;
	private int justAddedCandidateCount;
	private int notProcessedCandidateCount;

	public int getActiveCandidatesCount() {
		return activeCandidatesCount;
	}

	public void setActiveCandidatesCount(int activeCandidatesCount) {
		this.activeCandidatesCount = activeCandidatesCount;
	}

	public int getPassiveCandidatesCount() {
		return passiveCandidatesCount;
	}

	public void setPassiveCandidatesCount(int passiveCandidatesCount) {
		this.passiveCandidatesCount = passiveCandidatesCount;
	}

	public int getMatchedCandidateCount() {
		return matchedCandidateCount;
	}

	public void setMatchedCandidateCount(int matchedCandidateCount) {
		this.matchedCandidateCount = matchedCandidateCount;
	}

	public int getUnMatchedCandidateCount() {
		return unMatchedCandidateCount;
	}

	public void setUnMatchedCandidateCount(int unMatchedCandidateCount) {
		this.unMatchedCandidateCount = unMatchedCandidateCount;
	}

	public int getJustAddedCandidateCount() {
		return justAddedCandidateCount;
	}

	public void setJustAddedCandidateCount(int justAddedCandidateCount) {
		this.justAddedCandidateCount = justAddedCandidateCount;
	}

	public int getNotProcessedCandidateCount() {
		return notProcessedCandidateCount;
	}

	public void setNotProcessedCandidateCount(int notProcessedCandidateCount) {
		this.notProcessedCandidateCount = notProcessedCandidateCount;
	}

	@Override
	public String toString() {
		return "CountDto {" +
				"activeCandidatesCount=" + activeCandidatesCount +
				", passiveCandidatesCount=" + passiveCandidatesCount +
				", matchedCandidateCount=" + matchedCandidateCount +
				", unMatchedCandidateCount=" + unMatchedCandidateCount +
				", justAddedCandidateCount=" + justAddedCandidateCount +
				", notProcessedCandidateCount=" + notProcessedCandidateCount +
				"}";
	}
}
