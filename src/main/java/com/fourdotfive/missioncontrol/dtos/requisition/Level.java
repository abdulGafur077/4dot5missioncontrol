package com.fourdotfive.missioncontrol.dtos.requisition;

/**
 * @author shalini
 */
public enum Level {
	
	LOW, MEDIUM, HIGH

}
