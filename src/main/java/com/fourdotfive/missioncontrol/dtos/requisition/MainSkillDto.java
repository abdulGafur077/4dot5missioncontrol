package com.fourdotfive.missioncontrol.dtos.requisition;

import java.util.List;

public class MainSkillDto {

	private String name;
	private List<SkillDto> skillType;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<SkillDto> getSkillType() {
		return skillType;
	}

	public void setSkillType(List<SkillDto> skillType) {
		this.skillType = skillType;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MainSkillDto [name=");
		builder.append(name);
		builder.append(", skillType=");
		builder.append(skillType);
		builder.append("]");
		return builder.toString();
	}

}
