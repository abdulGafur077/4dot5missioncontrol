package com.fourdotfive.missioncontrol.dtos.requisition;

import java.util.List;

import com.fourdotfive.missioncontrol.common.DateRange;

public class OpenRequisitionRequestDto {
	
	private List<String> clientOrBuList;
	
	private List<String> jobReqRole;
	
	private DateRange dateRange;
	
	private List<String> recruiterList;
	
	private String requisitionNumber;
	
	private RequisitionState requisitionState;

	public List<String> getClientOrBuList() {
		return clientOrBuList;
	}

	public void setClientOrBuList(List<String> clientOrBuList) {
		this.clientOrBuList = clientOrBuList;
	}

	public List<String> getJobReqRole() {
		return jobReqRole;
	}

	public void setJobReqRole(List<String> jobReqRole) {
		this.jobReqRole = jobReqRole;
	}

	public DateRange getDateRange() {
		return dateRange;
	}

	public void setDateRange(DateRange dateRange) {
		this.dateRange = dateRange;
	}

	public List<String> getRecruiterList() {
		return recruiterList;
	}

	public void setRecruiterList(List<String> recruiterList) {
		this.recruiterList = recruiterList;
	}

	public String getRequisitionNumber() {
		return requisitionNumber;
	}

	public void setRequisitionNumber(String requisitionNumber) {
		this.requisitionNumber = requisitionNumber;
	}

	public RequisitionState getRequisitionState() {
		return requisitionState;
	}

	public void setRequisitionState(RequisitionState requisitionState) {
		this.requisitionState = requisitionState;
	}

	@Override
	public String toString() {
		return "OpenRequisitionRequestDto [clientOrBuList=" + clientOrBuList + ", jobReqRole=" + jobReqRole
				+ ", dateRange=" + dateRange + ", recruiterList=" + recruiterList + ", requisitionNumber="
				+ requisitionNumber + ", requisitionState=" + requisitionState + "]";
	}
	
}
