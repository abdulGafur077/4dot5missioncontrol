package com.fourdotfive.missioncontrol.dtos.requisition;


/**
 * @author shalini
 */
public class OpenRequisitionsResponseDto {
	
	private String requisitionId;
	
	private String requisitionRole ; 
	
	private String companyName;
	
	private RequisitionState state;
	
	
	private int activeCandidateCount;
	
	private int passiveCandidateCount;
	
	
	private int daysToSource;
	
	private Level levelOfDaysToSource;
	
	
	private int daysToFill;
	
	private Level levelOfDaysToFill;

	public OpenRequisitionsResponseDto(String requisitionId, String requisitionRole, String companyName,
			RequisitionState state, int activeCandidateCount, int passiveCandidateCount, int daysToSource,
			Level levelOfDaysToSource, int daysToFill, Level levelOfDaysToFill) {
		super();
		this.requisitionId = requisitionId;
		this.requisitionRole = requisitionRole;
		this.companyName = companyName;
		this.state = state;
		this.activeCandidateCount = activeCandidateCount;
		this.passiveCandidateCount = passiveCandidateCount;
		this.daysToSource = daysToSource;
		this.levelOfDaysToSource = levelOfDaysToSource;
		this.daysToFill = daysToFill;
		this.levelOfDaysToFill = levelOfDaysToFill;
	}

	public String getRequisitionId() {
		return requisitionId;
	}

	public void setRequisitionId(String requisitionId) {
		this.requisitionId = requisitionId;
	}

	public String getRequisitionRole() {
		return requisitionRole;
	}

	public void setRequisitionRole(String requisitionRole) {
		this.requisitionRole = requisitionRole;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public RequisitionState getState() {
		return state;
	}

	public void setState(RequisitionState state) {
		this.state = state;
	}

	public int getActiveCandidateCount() {
		return activeCandidateCount;
	}

	public void setActiveCandidateCount(int activeCandidateCount) {
		this.activeCandidateCount = activeCandidateCount;
	}

	public int getPassiveCandidateCount() {
		return passiveCandidateCount;
	}

	public void setPassiveCandidateCount(int passiveCandidateCount) {
		this.passiveCandidateCount = passiveCandidateCount;
	}

	public int getDaysToSource() {
		return daysToSource;
	}

	public void setDaysToSource(int daysToSource) {
		this.daysToSource = daysToSource;
	}

	public Level getLevelOfDaysToSource() {
		return levelOfDaysToSource;
	}

	public void setLevelOfDaysToSource(Level levelOfDaysToSource) {
		this.levelOfDaysToSource = levelOfDaysToSource;
	}

	public int getDaysToFill() {
		return daysToFill;
	}

	public void setDaysToFill(int daysToFill) {
		this.daysToFill = daysToFill;
	}

	public Level getLevelOfDaysToFill() {
		return levelOfDaysToFill;
	}

	public void setLevelOfDaysToFill(Level levelOfDaysToFill) {
		this.levelOfDaysToFill = levelOfDaysToFill;
	}


	@Override
	public String toString() {
		return "Requisitions [requisitionId=" + requisitionId + ", requisitionRole=" + requisitionRole
				+ ", companyName=" + companyName + ", state=" + state + ", activeCandidateCount=" + activeCandidateCount
				+ ", passiveCandidateCount=" + passiveCandidateCount + ", daysToSource=" + daysToSource
				+ ", levelOfDaysToSource=" + levelOfDaysToSource + ", daysToFill=" + daysToFill + ", levelOfDaysToFill="
				+ levelOfDaysToFill + "]";
	}
	
	/*public static OpenRequisitionsResponseDto getJobProfileDto(JobMatch jobmatch){
			
			ModelMapper mapper = new ModelMapper();

			PropertyMap<JobMatch, OpenRequisitionsResponseDto> profileDto = new PropertyMap<JobMatch, OpenRequisitionsResponseDto>() {

				@Override
				protected void configure() {
					map(source.getJob().getTitle(), destination.getRole());
					map(source.getJob().getCompany().getName(), destination.getCompanyName());
					map(source.getJob().getEducationList(),destination.getEducationList());
					map(source.getJob().getExperienceList(), destination.getExperienceList());
					map(source.getJob().getTechnicalSkills(), destination.getTechnicalSkillList());
					map(source.getJob().getSoftSkills(), destination.getSoftSkillList());
					map(source.getJob().getCertifications(), destination.getCertificationList());
				}
			};
			mapper.createTypeMap(jobmatch, JobProfileDto.class).addMappings(profileDto);
			return mapper.map(jobmatch, JobProfileDto.class);
			
		}*/
	

}
