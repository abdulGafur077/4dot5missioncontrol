package com.fourdotfive.missioncontrol.dtos.requisition;

import java.util.List;

public class RequisitionDetailedDto {

	private String jobName;
	private String jobId;
	private String companyName;
	private List<EducationExperienceDto> allEducationExperience;
	private List<MainSkillDto> allSkill;

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public List<EducationExperienceDto> getAllEducationExperience() {
		return allEducationExperience;
	}

	public void setAllEducationExperience(
			List<EducationExperienceDto> allEducationExperience) {
		this.allEducationExperience = allEducationExperience;
	}

	public List<MainSkillDto> getAllSkill() {
		return allSkill;
	}

	public void setAllSkill(List<MainSkillDto> allSkill) {
		this.allSkill = allSkill;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RequisitionMaxDto [jobName=");
		builder.append(jobName);
		builder.append(", jobId=");
		builder.append(jobId);
		builder.append(", companyName=");
		builder.append(companyName);
		builder.append(", allEducationExperience=");
		builder.append(allEducationExperience);
		builder.append(", allSkill=");
		builder.append(allSkill);
		builder.append("]");
		return builder.toString();
	}

}
