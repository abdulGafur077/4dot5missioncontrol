package com.fourdotfive.missioncontrol.dtos.requisition;

import com.fourdotfive.missioncontrol.dtos.job.NewJobDto;

import java.util.List;

public class RequisitionDetailsDTO {

    private String companyId;
    private String userId;
    private List<NewJobDto> newJobDtos;
    private String workflowStep;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<NewJobDto> getNewJobDtos() {
        return newJobDtos;
    }

    public void setNewJobDtos(List<NewJobDto> newJobDtos) {
        this.newJobDtos = newJobDtos;
    }

    public String getWorkflowStep() {
        return workflowStep;
    }

    public void setWorkflowStep(String workflowStep) {
        this.workflowStep = workflowStep;
    }
}
