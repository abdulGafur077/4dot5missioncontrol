package com.fourdotfive.missioncontrol.dtos.requisition;

public class RequisitionDetailsRequestDTO {

    private String companyId;
    private String clientOrBuId;
    private String workflowStep;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getClientOrBuId() {
        return clientOrBuId;
    }

    public void setClientOrBuId(String clientOrBuId) {
        this.clientOrBuId = clientOrBuId;
    }

    public String getWorkflowStep() {
        return workflowStep;
    }

    public void setWorkflowStep(String workflowStep) {
        this.workflowStep = workflowStep;
    }
}
