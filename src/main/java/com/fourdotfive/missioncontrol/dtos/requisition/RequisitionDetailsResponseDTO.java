package com.fourdotfive.missioncontrol.dtos.requisition;

public class RequisitionDetailsResponseDTO {

    private String name;
    private String requisitionActualNumber;
    private int numberOfCandidates;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRequisitionActualNumber() {
        return requisitionActualNumber;
    }

    public void setRequisitionActualNumber(String requisitionActualNumber) {
        this.requisitionActualNumber = requisitionActualNumber;
    }

    public int getNumberOfCandidates() {
        return numberOfCandidates;
    }

    public void setNumberOfCandidates(int numberOfCandidates) {
        this.numberOfCandidates = numberOfCandidates;
    }
}
