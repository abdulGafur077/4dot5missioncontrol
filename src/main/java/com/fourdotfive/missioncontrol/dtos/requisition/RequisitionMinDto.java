package com.fourdotfive.missioncontrol.dtos.requisition;

import java.util.List;

public class RequisitionMinDto {

	private String role;
	private String id;
	private String company;
	private String daysToSource;
	private String daysToFill;
	private String activeCandidates;
	private String passiveCandidates;
	private String views;
	private List<String> recruiter;

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getDaysToSource() {
		return daysToSource;
	}

	public void setDaysToSource(String daysToSource) {
		this.daysToSource = daysToSource;
	}

	public String getDaysToFill() {
		return daysToFill;
	}

	public void setDaysToFill(String daysToFill) {
		this.daysToFill = daysToFill;
	}

	public String getActiveCandidates() {
		return activeCandidates;
	}

	public void setActiveCandidates(String activeCandidates) {
		this.activeCandidates = activeCandidates;
	}

	public String getPassiveCandidates() {
		return passiveCandidates;
	}

	public void setPassiveCandidates(String passiveCandidates) {
		this.passiveCandidates = passiveCandidates;
	}

	public String getViews() {
		return views;
	}

	public void setViews(String views) {
		this.views = views;
	}

	public List<String> getRecruiter() {
		return recruiter;
	}

	public void setRecruiter(List<String> recruiter) {
		this.recruiter = recruiter;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RequisitionMinDto [role=");
		builder.append(role);
		builder.append(", id=");
		builder.append(id);
		builder.append(", company=");
		builder.append(company);
		builder.append(", daysToSource=");
		builder.append(daysToSource);
		builder.append(", daysToFill=");
		builder.append(daysToFill);
		builder.append(", activeCandidates=");
		builder.append(activeCandidates);
		builder.append(", passiveCandidates=");
		builder.append(passiveCandidates);
		builder.append(", views=");
		builder.append(views);
		builder.append(", recruiter=");
		builder.append(recruiter);
		builder.append("]");
		return builder.toString();
	}

}
