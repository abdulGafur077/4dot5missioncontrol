package com.fourdotfive.missioncontrol.dtos.requisition;

/**
 * @author shalini
 */
public enum RequisitionState {
	
	SOURCED, FILLED, COMPLETE, PAUSE, CANCEL

}
