package com.fourdotfive.missioncontrol.dtos.requisition;

public class SkillDto {

	private String name;
	private String value;
	private Integer skillMeasure;

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SkillDto [name=");
		builder.append(name);
		builder.append(", value=");
		builder.append(value);
		builder.append(", skillMeasure=");
		builder.append(skillMeasure);
		builder.append("]");
		return builder.toString();
	}

	public Integer getSkillMeasure() {
		return skillMeasure;
	}

	public void setSkillMeasure(Integer skillMeasure) {
		this.skillMeasure = skillMeasure;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
