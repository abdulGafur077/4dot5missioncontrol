package com.fourdotfive.missioncontrol.dtos.requisition;


import java.util.List;

/**
 * @author shalini
 */
public class VendorRecruiterUpdateDto {
    private String jobId;
    private String vendorId;
    private List<String> recruiterIds;
        private String loggedInUserId;

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public List<String> getRecruiterIds() {
        return recruiterIds;
    }

    public void setRecruiterIds(List<String> recruiterIds) {
        this.recruiterIds = recruiterIds;
    }

    public String getLoggedInUserId() {
        return loggedInUserId;
    }

    public void setLoggedInUserId(String loggedInUserId) {
        this.loggedInUserId = loggedInUserId;
    }

    @Override
    public String toString() {
        return "VendorRecruiterUpdateDto{" +
                "jobId='" + jobId + '\'' +
                ", vendorId='" + vendorId + '\'' +
                ", recruiterIds=" + recruiterIds +
                '}';
    }
}
