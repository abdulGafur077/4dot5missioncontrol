package com.fourdotfive.missioncontrol.dtos.resume;

import com.fourdotfive.missioncontrol.licensepreference.JobBoardCredential;
import com.fourdotfive.missioncontrol.pojo.job.ResumeSourceType;

import java.util.List;

public class ResumeResponseDto {

    private List<ResumeSourceType> resumeSourceTypes;
    private JobBoardCredential availableJobBoardCredential;

    public List<ResumeSourceType> getResumeSourceTypes() {
        return resumeSourceTypes;
    }

    public void setResumeSourceTypes(List<ResumeSourceType> resumeSourceTypes) {
        this.resumeSourceTypes = resumeSourceTypes;
    }

    public JobBoardCredential getAvailableJobBoardCredential() {
        return availableJobBoardCredential;
    }

    public void setAvailableJobBoardCredential(JobBoardCredential availableJobBoardCredential) {
        this.availableJobBoardCredential = availableJobBoardCredential;
    }
}
