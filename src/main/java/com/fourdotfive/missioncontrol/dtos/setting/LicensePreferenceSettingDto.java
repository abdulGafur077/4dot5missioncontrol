package com.fourdotfive.missioncontrol.dtos.setting;

import java.util.List;

import com.fourdotfive.missioncontrol.licensepreference.LicensePrefEnum;

public class LicensePreferenceSettingDto {
	
	private String id;

	private LicensePrefEnum licensePreferenceEnum;
	
	private String name;

	private Double score;

	private Double timeToComplete;

	private Integer reminderInterval;

	private Integer year;

	private Integer month;

	private Integer day;

	private Boolean isEnabled;

	private Integer maxLimitForTimeToComplete;

	private List<NotifyEventDto> notifyEvents;
	
	private String message;
	
	public LicensePreferenceSettingDto() {
		// TODO Auto-generated constructor stub
	}
	
	public LicensePreferenceSettingDto(String id, LicensePrefEnum licensePreferenceEnum, Double score,
			Double timeToComplete, Integer reminderInterval, Integer year, Integer month, Integer day, Integer maxLimitForTimeToComplete,
			List<NotifyEventDto> notifyEvent, String message) {
		super();
		this.id = id;
		this.licensePreferenceEnum = licensePreferenceEnum;
		this.score = score;
		this.timeToComplete = timeToComplete;
		this.reminderInterval = reminderInterval;
		this.year = year;
		this.month = month;
		this.day = day;
		this.maxLimitForTimeToComplete = maxLimitForTimeToComplete;
		this.notifyEvents = notifyEvent;
		this.message = message;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public LicensePrefEnum getLicensePreferenceEnum() {
		return licensePreferenceEnum;
	}

	public void setLicensePreferenceEnum(LicensePrefEnum licensePreferenceEnum) {
		this.licensePreferenceEnum = licensePreferenceEnum;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	public Double getTimeToComplete() {
		return timeToComplete;
	}

	public void setTimeToComplete(Double timeToComplete) {
		this.timeToComplete = timeToComplete;
	}

	public Integer getReminderInterval() {
		return reminderInterval;
	}

	public void setReminderInterval(Integer reminderInterval) {
		this.reminderInterval = reminderInterval;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Integer getDay() {
		return day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public Boolean getIsEnabled() {
		return isEnabled;
	}

	public void setIsEnabled(Boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	public Integer getMaxLimitForTimeToComplete() {
		return maxLimitForTimeToComplete;
	}

	public void setMaxLimitForTimeToComplete(Integer maxLimitForTimeToComplete) {
		this.maxLimitForTimeToComplete = maxLimitForTimeToComplete;
	}

	public List<NotifyEventDto> getNotifyEvents() {
		return notifyEvents;
	}

	public void setNotifyEvents(List<NotifyEventDto> notifyEvents) {
		this.notifyEvents = notifyEvents;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "LicensePreferenceSettingDto [id=" + id + ", licensePreferenceEnum=" + licensePreferenceEnum + ", name="
				+ name + ", score=" + score + ", timeToComplete=" + timeToComplete + ", reminderInterval="
				+ reminderInterval + ", year=" + year + ", month=" + month + ", day=" + day + ", isEnabled=" + isEnabled
				+ ", maxLimitForTimeToComplete=" + maxLimitForTimeToComplete + ", notifyEvents=" + notifyEvents
				+ ", message=" + message + "]";
	}

}
