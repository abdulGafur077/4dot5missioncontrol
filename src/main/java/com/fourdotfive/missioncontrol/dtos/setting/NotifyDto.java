package com.fourdotfive.missioncontrol.dtos.setting;

import com.fourdotfive.missioncontrol.pojo.setting.NotifyEnum;

public class NotifyDto {
	
	private NotifyEnum notifyEnum;
	
	private String name;

	private boolean isEnabled;
	
	private boolean isReadOnly;
	
	public NotifyDto() {
		// TODO Auto-generated constructor stub
	}

	public NotifyEnum getNotifyEnum() {
		return notifyEnum;
	}

	public void setNotifyEnum(NotifyEnum notifyEnum) {
		this.notifyEnum = notifyEnum;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isEnabled() {
		return isEnabled;
	}

	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}
	
	public boolean isReadOnly() {
		return isReadOnly;
	}

	public void setReadOnly(boolean isReadOnly) {
		this.isReadOnly = isReadOnly;
	}

	@Override
	public String toString() {
		return "NotifyDto [notifyEnum=" + notifyEnum + ", name=" + name + ", isEnabled=" + isEnabled + ", isReadOnly="
				+ isReadOnly + "]";
	}

}
