package com.fourdotfive.missioncontrol.dtos.setting;

import java.util.List;

import com.fourdotfive.missioncontrol.pojo.setting.NotifyEventEnum;

public class NotifyEventDto {
	
	private NotifyEventEnum notifyEventEnum;
	
	private String name;

	private boolean isDetectOn;
	
	private List<NotifyDto> notifyOn;


	private boolean isDetectOnReadOnly;//Not part of platform pojo

	public NotifyEventDto() {
		// TODO Auto-generated constructor stub
	}

	public NotifyEventEnum getNotifyEventEnum() {
		return notifyEventEnum;
	}

	public void setNotifyEventEnum(NotifyEventEnum notifyEventEnum) {
		this.notifyEventEnum = notifyEventEnum;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<NotifyDto> getNotifyOn() {
		return notifyOn;
	}

	public void setNotifyOn(List<NotifyDto> notifyOn) {
		this.notifyOn = notifyOn;
	}

	public boolean isDetectOn() {
		return isDetectOn;
	}

	public void setDetectOn(boolean detectOn) {
		isDetectOn = detectOn;
	}

	public boolean isDetectOnReadOnly() {
		return isDetectOnReadOnly;
	}

	public void setDetectOnReadOnly(boolean detectOnReadOnly) {
		isDetectOnReadOnly = detectOnReadOnly;
	}

	@Override
	public String toString() {
		return "NotifyEventDto [notifyEventEnum=" + notifyEventEnum + ", name=" + name +  ", isDetectOn =" + isDetectOn +    ", isDetectOnReadOnly =" + isDetectOnReadOnly +   ", notifyOn=" + notifyOn + "]";
	}

}
