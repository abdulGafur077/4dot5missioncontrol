package com.fourdotfive.missioncontrol.dtos.setting;

import java.util.List;

import com.fourdotfive.missioncontrol.pojo.setting.PerformanceEnum;

public class PerformanceSettingDto {
	
	private String id;

	private PerformanceEnum performanceEnum;
	
	private String name;

	private double minValue;

	private double maxValue;

	private double maxLimit;

	private List<NotifyEventDto> notifyEvents;
	
	public PerformanceSettingDto() {
		// TODO Auto-generated constructor stub
	}
	
	public PerformanceSettingDto(String id, PerformanceEnum performanceEnum, String name, double minValue,
			double maxValue, double maxLimit, List<NotifyEventDto> notifyEvents) {
		super();
		this.id = id;
		this.performanceEnum = performanceEnum;
		this.name = name;
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.maxLimit = maxLimit;
		this.notifyEvents = notifyEvents;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public PerformanceEnum getPerformanceEnum() {
		return performanceEnum;
	}

	public void setPerformanceEnum(PerformanceEnum performanceEnum) {
		this.performanceEnum = performanceEnum;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getMinValue() {
		return minValue;
	}

	public void setMinValue(double minValue) {
		this.minValue = minValue;
	}

	public double getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(double maxValue) {
		this.maxValue = maxValue;
	}

	public double getMaxLimit() {
		return maxLimit;
	}

	public void setMaxLimit(double maxLimit) {
		this.maxLimit = maxLimit;
	}

	public List<NotifyEventDto> getNotifyEvents() {
		return notifyEvents;
	}

	public void setNotifyEvents(List<NotifyEventDto> notifyEvents) {
		this.notifyEvents = notifyEvents;
	}

	@Override
	public String toString() {
		return "PerformanceSettingDto [id=" + id + ", performanceEnum=" + performanceEnum + ", name=" + name
				+ ", minValue=" + minValue + ", maxValue=" + maxValue + ", maxLimit=" + maxLimit + ", notifyEvents="
				+ notifyEvents + "]";
	}

}
