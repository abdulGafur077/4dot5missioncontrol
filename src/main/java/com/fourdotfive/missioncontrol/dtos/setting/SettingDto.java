package com.fourdotfive.missioncontrol.dtos.setting;

import java.util.List;

public class SettingDto {

	private String id;

	private List<LicensePreferenceSettingDto> licensePreferenceSetting;

	private List<PerformanceSettingDto> performanceSetting;

	private List<UserRoleSettingDto> userRoleSetting;

	private boolean isGlobal;
	
	public SettingDto() {
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<LicensePreferenceSettingDto> getLicensePreferenceSetting() {
		return licensePreferenceSetting;
	}

	public void setLicensePreferenceSetting(List<LicensePreferenceSettingDto> licensePreferenceSetting) {
		this.licensePreferenceSetting = licensePreferenceSetting;
	}

	public List<PerformanceSettingDto> getPerformanceSetting() {
		return performanceSetting;
	}

	public void setPerformanceSetting(List<PerformanceSettingDto> performanceSetting) {
		this.performanceSetting = performanceSetting;
	}

	public boolean isGlobal() {
		return isGlobal;
	}

	public void setGlobal(boolean isGlobal) {
		this.isGlobal = isGlobal;
	}

	public List<UserRoleSettingDto> getUserRoleSetting() {
		return userRoleSetting;
	}

	public void setUserRoleSetting(List<UserRoleSettingDto> userRoleSetting) {
		this.userRoleSetting = userRoleSetting;
	}

	@Override
	public String toString() {
		return "SettingDto [id=" + id + ", licensePreferenceSetting=" + licensePreferenceSetting
				+ ", performanceSetting=" + performanceSetting  + ", userRoleSetting =" + userRoleSetting  + ", isGlobal=" + isGlobal + "]";
	}

}
