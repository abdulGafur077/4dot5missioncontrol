package com.fourdotfive.missioncontrol.dtos.setting;

import com.fourdotfive.missioncontrol.pojo.setting.NotifyEvent;
import com.fourdotfive.missioncontrol.pojo.setting.NotifyEventEnum;
import com.fourdotfive.missioncontrol.pojo.setting.UserRoleEnum;

import java.util.List;
import java.util.Map;

public class UserRoleSettingDto {

    private String id;

    private String name;

    private UserRoleEnum userRoleEnum;

    private List<NotifyEventDto> notifyEvents;

    public UserRoleSettingDto() {

    }

    public UserRoleSettingDto(String id, UserRoleEnum userRoleEnum, String name, List<NotifyEventDto> notifyEvents) {
        super();
        this.id = id;
        this.name = name;
        this.userRoleEnum = userRoleEnum;
        this.notifyEvents = notifyEvents;

    }

    public UserRoleEnum getUserRoleEnum() {
        return userRoleEnum;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public void setUserRoleEnum(UserRoleEnum userRoleEnum) {
        this.userRoleEnum = userRoleEnum;
    }


    public void setNotifyEvents(List<NotifyEventDto> notifyEvents) {
        this.notifyEvents = notifyEvents;
    }


    public List<NotifyEventDto> getNotifyEvents() {
        return notifyEvents;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "UserRoleSetting [id=" + id  + "name = " +  name + "userRoleEnum=" + userRoleEnum  + ", notifyEvents=" + notifyEvents + "]";
    }
}
