package com.fourdotfive.missioncontrol.dtos.user;

import com.fourdotfive.missioncontrol.candidate.ActiveCandidatesReport;
import com.fourdotfive.missioncontrol.candidate.PassiveCandidatesReport;
import com.fourdotfive.missioncontrol.candidate.RequisitionsReport;

public class DashBoardDto {
	private long activeCandidateCount;
	private long passiveCandidateCount;
	private long openRequisitionsCount;
	private long timeToSource;
	private long timeToConvert;
	private long timeToFill;
	private long timeToPlace;
	private ActiveCandidatesReport activeCandidatesReport;
	private PassiveCandidatesReport passiveCandidatesReport;
	private RequisitionsReport requisitionsReport;

	public long getActiveCandidateCount() {
		return activeCandidateCount;
	}

	public void setActiveCandidateCount(long activeCandidateCount) {
		this.activeCandidateCount = activeCandidateCount;
	}

	public long getPassiveCandidateCount() {
		return passiveCandidateCount;
	}

	public void setPassiveCandidateCount(long passiveCandidateCount) {
		this.passiveCandidateCount = passiveCandidateCount;
	}

	public long getOpenRequisitionsCount() {
		return openRequisitionsCount;
	}

	public void setOpenRequisitionsCount(long openRequisitionsCount) {
		this.openRequisitionsCount = openRequisitionsCount;
	}

	public long getTimeToSource() {
		return timeToSource;
	}

	public void setTimeToSource(long timeToSource) {
		this.timeToSource = timeToSource;
	}

	public long getTimeToConvert() {
		return timeToConvert;
	}

	public void setTimeToConvert(long timeToConvert) {
		this.timeToConvert = timeToConvert;
	}

	public long getTimeToFill() {
		return timeToFill;
	}

	public void setTimeToFill(long timeToFill) {
		this.timeToFill = timeToFill;
	}

	public long getTimeToPlace() {
		return timeToPlace;
	}

	public void setTimeToPlace(long timeToPlace) {
		this.timeToPlace = timeToPlace;
	}

	public ActiveCandidatesReport getActiveCandidatesReport() {
		return activeCandidatesReport;
	}

	public void setActiveCandidatesReport(
			ActiveCandidatesReport activeCandidatesReport) {
		this.activeCandidatesReport = activeCandidatesReport;
	}

	public PassiveCandidatesReport getPassiveCandidatesReport() {
		return passiveCandidatesReport;
	}

	public void setPassiveCandidatesReport(
			PassiveCandidatesReport passiveCandidatesReport) {
		this.passiveCandidatesReport = passiveCandidatesReport;
	}

	public RequisitionsReport getRequisitionsReport() {
		return requisitionsReport;
	}

	public void setRequisitionsReport(RequisitionsReport requisitionsReport) {
		this.requisitionsReport = requisitionsReport;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DashBoardDto [activeCandidateCount=");
		builder.append(activeCandidateCount);
		builder.append(", passiveCandidateCount=");
		builder.append(passiveCandidateCount);
		builder.append(", openRequisitionsCount=");
		builder.append(openRequisitionsCount);
		builder.append(", timeToSource=");
		builder.append(timeToSource);
		builder.append(", timeToConvert=");
		builder.append(timeToConvert);
		builder.append(", timeToFill=");
		builder.append(timeToFill);
		builder.append(", timeToPlace=");
		builder.append(timeToPlace);
		builder.append(", activeCandidatesReport=");
		builder.append(activeCandidatesReport);
		builder.append(", passiveCandidatesReport=");
		builder.append(passiveCandidatesReport);
		builder.append(", requisitionsReport=");
		builder.append(requisitionsReport);
		builder.append("]");
		return builder.toString();
	}

}