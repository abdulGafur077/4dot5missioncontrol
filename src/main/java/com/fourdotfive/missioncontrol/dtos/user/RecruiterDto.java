package com.fourdotfive.missioncontrol.dtos.user;

import org.apache.commons.lang3.StringUtils;

public class RecruiterDto {

    private String id;
    private String name;

    public RecruiterDto(){
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
