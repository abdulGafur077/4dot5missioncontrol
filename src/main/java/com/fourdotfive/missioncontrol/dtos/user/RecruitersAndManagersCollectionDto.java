package com.fourdotfive.missioncontrol.dtos.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RecruitersAndManagersCollectionDto {
    private Map<String, List<TeamMemberDto>> vendorRecruiters = new HashMap<>();
    private List<TeamMemberDto> recruiters = new ArrayList<>();
    private List<TeamMemberDto> hiringManagers = new ArrayList<>();

    public Map<String, List<TeamMemberDto>> getVendorRecruiters() {
        return vendorRecruiters;
    }

    public void setVendorRecruiters(Map<String, List<TeamMemberDto>> vendorRecruiters) {
        this.vendorRecruiters = vendorRecruiters;
    }

    public List<TeamMemberDto> getRecruiters() {
        return recruiters;
    }

    public void setRecruiters(List<TeamMemberDto> recruiters) {
        this.recruiters = recruiters;
    }

    public List<TeamMemberDto> getHiringManagers() {
        return hiringManagers;
    }

    public void setHiringManagers(List<TeamMemberDto> hiringManagers) {
        this.hiringManagers = hiringManagers;
    }

    @Override
    public String toString() {
        return "RecruitersAndManagers{" +
                "recruiters=" + recruiters +
                ", hiringManagers=" + hiringManagers +
                ", vendorRecruiters=" + vendorRecruiters +
                '}';
    }
}
