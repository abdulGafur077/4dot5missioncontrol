package com.fourdotfive.missioncontrol.dtos.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fourdotfive.missioncontrol.pojo.user.Role;

public class RoleDto {

	private String id;

	private String name;
	
	private String description;
	
	private String createdBy; 
	
	private String modifiedBy; 

	@JsonProperty("new")
	private boolean isNew;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}
	
	public static RoleDto getRoleDto(Role role){
		RoleDto roleDto = new RoleDto();
		roleDto.setId(role.getId());
		roleDto.setDescription(role.getDescription());
		roleDto.setName(role.getName());
		roleDto.setCreatedBy(role.getCreatedBy());
		roleDto.setModifiedBy(role.getLastModifiedBy());
		
		return roleDto;
	}

	@Override
	public String toString() {
		return "Role [id=" + id + ", name=" + name + ", description=" + description + ", createdBy=" + createdBy
				+ ", modifiedBy=" + modifiedBy + ", isNew=" + isNew + "]";
	}
	
}
