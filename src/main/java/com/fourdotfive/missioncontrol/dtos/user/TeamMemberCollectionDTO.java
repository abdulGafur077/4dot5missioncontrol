package com.fourdotfive.missioncontrol.dtos.user;


import java.util.ArrayList;
import java.util.List;

public class TeamMemberCollectionDTO {

    private List<TeamMemberDto> recruiters = new ArrayList<>();
    private List<TeamMemberDto> hiringManagers = new ArrayList<>();

    public List<TeamMemberDto> getRecruiters() {
        return recruiters;
    }

    public void setRecruiters(List<TeamMemberDto> recruiters) {
        this.recruiters = recruiters;
    }

    public List<TeamMemberDto> getHiringManagers() {
        return hiringManagers;
    }

    public void setHiringManagers(List<TeamMemberDto> hiringManagers) {
        this.hiringManagers = hiringManagers;
    }
}
