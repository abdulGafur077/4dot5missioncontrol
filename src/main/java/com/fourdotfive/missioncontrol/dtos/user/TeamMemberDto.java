package com.fourdotfive.missioncontrol.dtos.user;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.fourdotfive.missioncontrol.common.AppConstants;
import com.fourdotfive.missioncontrol.pojo.user.User;
import org.springframework.util.StringUtils;

public class TeamMemberDto {

    private String firstName;
    private String userId;
    private String lastName;
    private String fullName;
    private String roleName;
    private String roleDescription;
    private int currentCandidates;
    private String outreaches;
    private int reporteesCount;
    private int convertedCandidates;
    private boolean canDrillDown;
    private boolean canViewNotificationPref;
    private String roleId;
    private boolean canDelete;
    private boolean canEdit;
    private String userState;
    private boolean isForwardRecipient;
    private String forwardFromUser;
    private String forwardFromId;
    private String forwardToUser;
    private String forwardToId;
    private boolean isTokenExpired;
    private boolean isBUOrClientListEmptyForUser;
    private boolean isBUsOrClientsAvailable;


    public boolean isForwardRecipient() {
        return isForwardRecipient;
    }

    public void setForwardRecipient(boolean isForwardRecipient) {
        this.isForwardRecipient = isForwardRecipient;
    }

    public boolean getIsBUOrClientListEmptyForUser() {
        return isBUOrClientListEmptyForUser;
    }

    public void setIsBUOrClientListEmptyForUser(boolean BUOrClientListEmptyForUser) {
        isBUOrClientListEmptyForUser = BUOrClientListEmptyForUser;
    }

    public boolean getIsBUsOrClientsAvailable() {
        return isBUsOrClientsAvailable;
    }

    public void setIsBUsOrClientsAvailable(boolean BUsOrClientsAvailable) {
        isBUsOrClientsAvailable = BUsOrClientsAvailable;
    }

    public TeamMemberDto() {
        super();
    }

    public TeamMemberDto(String userId, String firstName, String lastName) {
        super();
        this.firstName = firstName;
        this.userId = userId;
        this.lastName = lastName;
        this.fullName = firstName + " " + lastName;
    }

    public TeamMemberDto(String firstName, String userId, String lastName,
                         String roleName, String roleDescription, String roleId,
                         String userState) {
        super();
        this.firstName = firstName;
        this.userId = userId;
        this.lastName = lastName;
        this.fullName = firstName + " " + lastName;
        this.roleName = roleName;
        this.roleDescription = roleDescription;
        this.roleId = roleId;
        this.userState = userState;
    }


    public String getUserState() {
        return userState;
    }

    public void setUserState(String userState) {
        this.userState = userState;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public boolean isCanDrillDown() {
        return canDrillDown;
    }

    public void setCanDrillDown(boolean canDrillDown) {
        this.canDrillDown = canDrillDown;
    }

    public boolean isCanViewNotificationPref() {
        return canViewNotificationPref;
    }

    public void setCanViewNotificationPref(boolean canViewNotificationPref) {
        this.canViewNotificationPref = canViewNotificationPref;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public int getCurrentCandidates() {
        return currentCandidates;
    }

    public void setCurrentCandidates(int currentCandidates) {
        this.currentCandidates = currentCandidates;
    }

    public String getOutreaches() {
        return outreaches;
    }

    public void setOutreaches(String outreaches) {
        this.outreaches = outreaches;
    }

    public int getConvertedCandidates() {
        return convertedCandidates;
    }

    public void setConvertedCandidates(int convertedCandidates) {
        this.convertedCandidates = convertedCandidates;
    }

    public int getReporteesCount() {
        return reporteesCount;
    }

    public void setReporteesCount(int reporteesCount) {
        this.reporteesCount = reporteesCount;
    }

    public boolean isCanDelete() {
        return canDelete;
    }

    public void setCanDelete(boolean canDelete) {
        this.canDelete = canDelete;
    }

    public boolean isCanEdit() {
        return canEdit;
    }

    public void setCanEdit(boolean canEdit) {
        this.canEdit = canEdit;
    }

    public String getRoleDescription() {
        return roleDescription;
    }

    public void setRoleDescription(String roleDescription) {
        this.roleDescription = roleDescription;
    }

    public String getForwardFromUser() {
        return forwardFromUser;
    }

    public void setForwardFromUser(String forwardFromUser) {
        this.forwardFromUser = forwardFromUser;
    }

    public String getForwardFromId() {
        return forwardFromId;
    }

    public void setForwardFromId(String forwardFromId) {
        this.forwardFromId = forwardFromId;
    }

    public String getForwardToUser() {
        return forwardToUser;
    }

    public void setForwardToUser(String forwardToUser) {
        this.forwardToUser = forwardToUser;
    }

    public String getForwardToId() {
        return forwardToId;
    }

    public void setForwardToId(String forwardToId) {
        this.forwardToId = forwardToId;
    }

    public boolean isTokenExpired() {
        return isTokenExpired;
    }

    public void setTokenExpired(boolean tokenExpired) {
        isTokenExpired = tokenExpired;
    }

    public boolean isBUOrClientListEmptyForUser() {
        return isBUOrClientListEmptyForUser;
    }

    public void setBUOrClientListEmptyForUser(boolean BUOrClientListEmptyForUser) {
        isBUOrClientListEmptyForUser = BUOrClientListEmptyForUser;
    }

    public boolean isBUsOrClientsAvailable() {
        return isBUsOrClientsAvailable;
    }

    public void setBUsOrClientsAvailable(boolean BUsOrClientsAvailable) {
        isBUsOrClientsAvailable = BUsOrClientsAvailable;
    }

    public static Comparator<TeamMemberDto> getTeamMemberNameComparator() {
        return teamMemberNameComparator;
    }

    public static void setTeamMemberNameComparator(Comparator<TeamMemberDto> teamMemberNameComparator) {
        TeamMemberDto.teamMemberNameComparator = teamMemberNameComparator;
    }

    public static List<TeamMemberDto> mapUsersToTeamMembers(List<User> users) {
        List<TeamMemberDto> memberDtos = new ArrayList<>();
        for (User user : users) {
            memberDtos.add(mapUserToTeamMember(user));
        }
        return memberDtos;
    }

    public static TeamMemberDto mapUserToTeamMember(User user) {
        if (user.getAccessControlList() != null) {
            TeamMemberDto memberDto = new TeamMemberDto(user.getFirstname(),
                    user.getId(), user.getLastname(), user.getRole().getName(),
                    user.getRole().getDescription(), user.getRole().getId(),
                    user.getAccessControlList().getAccountState());

            if (memberDto.getRoleId().equals(
                    AppConstants.CORPORATE_ADMIN_ROLEID)
                    || memberDto.getRoleId().equals(
                    AppConstants.STAFFING_ADMIN_ROLEID)) {
                memberDto.setRoleName(AppConstants.ADMIN);
            }
            return memberDto;
        }
        return null;

    }

    public static TeamMemberDto mapUserToTeamMember(User user, boolean canEdit,
                                                    boolean canDelete, boolean canDrillDown, int reporteesCount,
                                                    boolean canViewNotificationPref) {
        TeamMemberDto memberDto = new TeamMemberDto(user.getFirstname(),
                user.getId(), user.getLastname(), user.getRole().getName(),
                user.getRole().getDescription(), user.getRole().getId(), user
                .getAccessControlList().getAccountState());

        if (memberDto.getRoleId().equals(AppConstants.CORPORATE_ADMIN_ROLEID)
                || memberDto.getRoleId().equals(
                AppConstants.STAFFING_ADMIN_ROLEID)) {
            memberDto.setRoleName(AppConstants.ADMIN);
        }
        memberDto.setUserState(user.getAccessControlList().getAccountState());
        memberDto.setCanEdit(canEdit);
        memberDto.setCanDelete(canDelete);
        memberDto.setCanDrillDown(canDrillDown);
        memberDto.setReporteesCount(reporteesCount);
        memberDto.setForwardRecipient(false);
        memberDto.setCanViewNotificationPref(canViewNotificationPref);
        memberDto.setTokenExpired(user.isTokenExpired());
        return memberDto;
    }

    public static TeamMemberDto mapUserToTeamMember(User user, boolean canEdit,
                                                    boolean canDelete, boolean canDrillDown, int reporteesCount,
                                                    boolean isForwardRecipient, boolean canViewNotificationPref) {
        TeamMemberDto memberDto = new TeamMemberDto(user.getFirstname(),
                user.getId(), user.getLastname(), user.getRole().getName(),
                user.getRole().getDescription(), user.getRole().getId(), user
                .getAccessControlList().getAccountState());

        if (memberDto.getRoleId().equals(AppConstants.CORPORATE_ADMIN_ROLEID)
                || memberDto.getRoleId().equals(
                AppConstants.STAFFING_ADMIN_ROLEID)) {
            memberDto.setRoleName(AppConstants.ADMIN);
        }
        memberDto.setUserState(user.getAccessControlList().getAccountState());
        memberDto.setCanEdit(canEdit);
        memberDto.setCanDelete(canDelete);
        memberDto.setCanDrillDown(canDrillDown);
        memberDto.setReporteesCount(reporteesCount);
        memberDto.setForwardRecipient(isForwardRecipient);
        memberDto.setCanViewNotificationPref(canViewNotificationPref);
        return memberDto;
    }

    public static TeamMemberDto mapUserDtoToTeamMember(UserDto userDto) {
        TeamMemberDto memeberDto = new TeamMemberDto(userDto.getUserDetails()
                .getFirstName(), userDto.getUserDetails().getUserId(), userDto
                .getUserDetails().getLastName(), userDto.getUserDetails()
                .getRole().getName(), userDto.getUserDetails().getRole()
                .getDescription(), userDto.getUserDetails().getRole().getId(),
                userDto.getUserState());
        memeberDto.setIsBUOrClientListEmptyForUser(userDto.getIsBUOrClientListEmptyForUser());
        memeberDto.setIsBUsOrClientsAvailable(userDto.getIsBUsOrClientsAvailable());

        return memeberDto;
    }

    public static TeamMemberDto changeUserStatus(boolean canEdit,
                                                 boolean canDelete, boolean canDrillDown, int reporteeCount,
                                                 TeamMemberDto teamMember, boolean isForwardRecipient) {
        teamMember.setCanDelete(canDelete);
        teamMember.setCanEdit(canEdit);
        teamMember.setCanDrillDown(canDrillDown);
        teamMember.setReporteesCount(reporteeCount);
        teamMember.setForwardRecipient(isForwardRecipient);
        return teamMember;
    }

    public static TeamMemberDto getForwardRecipientUser(TeamMemberDto teamMember) {
        return changeUserStatus(AppConstants.CANNOT_EDIT,
                AppConstants.CANNOT_DELETE, AppConstants.CANNOT_DRILLDOWN,
                AppConstants.NO_REPORTEES, teamMember,
                AppConstants.IS_A_FORWARDED_RECIPIENT);
    }

    public static Comparator<TeamMemberDto> teamMemberNameComparator = new Comparator<TeamMemberDto>() {
        @Override
        public int compare(TeamMemberDto teamMember, TeamMemberDto nextTeamMember) {
            String teamMemberName = teamMember.getFirstName();
            String nextTeamMemberName = nextTeamMember.getFirstName();
            if (StringUtils.isEmpty(teamMemberName)) {
                return (StringUtils.isEmpty(nextTeamMemberName)) ? 0 : -1;
            }
            if (StringUtils.isEmpty(nextTeamMemberName)) {
                return 1;
            }
            return teamMemberName.toUpperCase().compareTo(nextTeamMemberName.toUpperCase());
        }
    };

    public static Comparator<TeamMemberDto> memberNameComparator = new Comparator<TeamMemberDto>() {
        @Override
        public int compare(TeamMemberDto member, TeamMemberDto nextMember) {
            String memberName = member.getFirstName();
            String nextMemberName = nextMember.getFirstName();
            if (StringUtils.isEmpty(memberName)) {
                return (StringUtils.isEmpty(nextMemberName)) ? 0 : -1;
            }
            if (StringUtils.isEmpty(nextMemberName)) {
                return 1;
            }
            return memberName.toUpperCase().compareTo(nextMemberName.toUpperCase());
        }
    };

}
