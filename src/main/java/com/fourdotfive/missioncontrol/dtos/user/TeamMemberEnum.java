package com.fourdotfive.missioncontrol.dtos.user;

public enum TeamMemberEnum {
    HIRING_MANAGERS("hiring managers"),
    VENDOR_RECRUITERS("vendor recruiters"),
    VENDORS("vendors"),
    RECRUITERS("recruiters");

    private final String description;

    TeamMemberEnum(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
