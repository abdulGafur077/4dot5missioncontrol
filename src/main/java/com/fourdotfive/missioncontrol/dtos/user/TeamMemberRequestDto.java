package com.fourdotfive.missioncontrol.dtos.user;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class TeamMemberRequestDto {
    @JsonProperty("isShared")
    private boolean isShared;
    private List<String> vendorIds;
    private String requisitionId;
    private String clientId;
    private String companyId;

    public List<String> getVendorIds() {
        return vendorIds;
    }

    public void setVendorIds(List<String> vendorIds) {
        this.vendorIds = vendorIds;
    }

    public String getRequisitionId() {
        return requisitionId;
    }

    public void setRequisitionId(String requisitionId) {
        this.requisitionId = requisitionId;
    }

    public boolean isShared() {
        return isShared;
    }

    public void setShared(boolean shared) {
        isShared = shared;
    }

    public boolean getIsShared() {
        return isShared;
    }

    public void setIsShared(boolean shared) {
        isShared = shared;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    @Override
    public String toString() {
        return "TeamMemberRequestDto{" +
                "isShared=" + isShared +
                ", vendorIds='" + vendorIds + '\'' +
                ", requisitionId='" + requisitionId + '\'' +
                ", clientId='" + clientId + '\'' +
                ", companyId='" + companyId + '\'' +
                '}';
    }
}
