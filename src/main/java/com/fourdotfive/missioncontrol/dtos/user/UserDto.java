package com.fourdotfive.missioncontrol.dtos.user;

import com.fourdotfive.missioncontrol.dtos.company.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.joda.time.DateTime;

import com.fourdotfive.missioncontrol.common.Messages;
import com.fourdotfive.missioncontrol.pojo.user.Address;
import com.fourdotfive.missioncontrol.pojo.user.City;
import com.fourdotfive.missioncontrol.pojo.user.Role;
import com.fourdotfive.missioncontrol.pojo.user.User;

public class UserDto {

	private UserPersonalDetailsDto userDetails;

	private UserForwardRecipientDetailsDto userForwardRecipientDetails;

	private AccessControlDto userScopeDetails;

	private AuditDto auditDto;

	private int reporteeCount;

	private String userState;

	private boolean isBUOrClientListEmptyForUser;

	private boolean isBUsOrClientsAvailable;

	private String buId;

	private String buName;

	private String departmentId;

	private String departmentName;

	private UserManagerDTO manager;

	private boolean isPointOfContact;

	public String getUserState() {
		return userState;
	}

	public void setUserState(String userState) {
		this.userState = userState;
	}

	public AuditDto getAuditDto() {
		return auditDto;
	}

	public void setAuditDto(AuditDto auditDto) {
		this.auditDto = auditDto;
	}

	public UserPersonalDetailsDto getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserPersonalDetailsDto userDetails) {
		this.userDetails = userDetails;
	}

	public UserForwardRecipientDetailsDto getUserForwardRecipientDetails() {
		return userForwardRecipientDetails;
	}

	public void setUserForwardRecipientDetails(
			UserForwardRecipientDetailsDto userForwardRecipientDetails) {
		this.userForwardRecipientDetails = userForwardRecipientDetails;
	}

	public AccessControlDto getUserScopeDetails() {
		return userScopeDetails;
	}

	public void setUserScopeDetails(AccessControlDto userScopeDetails) {
		this.userScopeDetails = userScopeDetails;
	}

	public int getReporteeCount() {
		return reporteeCount;
	}

	public void setReporteeCount(int reporteeCount) {
		this.reporteeCount = reporteeCount;
	}

	public boolean getIsBUOrClientListEmptyForUser() {
		return isBUOrClientListEmptyForUser;
	}

	public void setIsBUOrClientListEmptyForUser(boolean BUOrClientListEmptyForUser) {
		isBUOrClientListEmptyForUser = BUOrClientListEmptyForUser;
	}

	public boolean getIsBUsOrClientsAvailable() {
		return isBUsOrClientsAvailable;
	}

	public void setIsBUsOrClientsAvailable(boolean BUsOrClientsAvailable) {
		isBUsOrClientsAvailable = BUsOrClientsAvailable;
	}

	public String getBuId() {
		return buId;
	}

	public void setBuId(String buId) {
		this.buId = buId;
	}

	public String getBuName() {
		return buName;
	}

	public void setBuName(String buName) {
		this.buName = buName;
	}

	public String getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public UserManagerDTO getManager() {
		return manager;
	}

	public void setManager(UserManagerDTO manager) {
		this.manager = manager;
	}

	public boolean getIsPointOfContact() {
		return isPointOfContact;
	}

	public void setIsPointOfContact(boolean pointOfContact) {
		isPointOfContact = pointOfContact;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UserDto [userDetails=");
		builder.append(userDetails);
		builder.append(", userForwardRecipientDetails=");
		builder.append(userForwardRecipientDetails);
		builder.append(", userScopeDetails=");
		builder.append(userScopeDetails);
		builder.append(", auditDto=");
		builder.append(auditDto);
		builder.append(", reporteeCount=");
		builder.append(reporteeCount);
		builder.append(", userState=");
		builder.append(userState);
		builder.append("]");
		return builder.toString();
	}

	public static User mapUserDtoToUser(UserDto dto) {

		User user = new User();
		user.setId(dto.getUserDetails().getUserId());
		user.setEmail(dto.getUserDetails().getEmail());
		user.setMobilephone(dto.getUserDetails().getMobileNumber());
		user.setFirstname(dto.getUserDetails().getFirstName());
		user.setLastname(dto.getUserDetails().getLastName());
		if (StringUtils.isNotBlank(dto.getUserDetails().getIm())) {
			user.setIm(dto.getUserDetails().getIm());
		}
		user.setWorkphone(dto.getUserDetails().getWorkTelephone());

		Role role = new Role();
		role.setId(dto.getUserDetails().getRole().getId());
		role.setName(dto.getUserDetails().getRole().getName());
		user.setRole(role);

		Address address = new Address();
		City city = new City();
		city.setName(dto.getUserDetails().getCity());
		address.setAddress1(dto.getUserDetails().getAddress1());
		address.setAddress2(dto.getUserDetails().getAddress2());
		address.setCity(city);
		address.setCounty(dto.getUserDetails().getCounty());
		address.setCountry(dto.getUserDetails().getCountry());
		address.setState(dto.getUserDetails().getState());
		address.setZipcode(dto.getUserDetails().getZipcode());
		user.setAddress(address);
		user.setTitle(dto.getUserDetails().getTitle());

		user.setCreatedUser(dto.getAuditDto().getCreatedBy());
		user.setLastModifiedUser(dto.getAuditDto().getLastModifiedBy());

		user.setAccessControlList(null);
		if(StringUtils.isNotEmpty(dto.getDepartmentId()))
			user.setDepartment(dto.getDepartmentId());
		if(StringUtils.isNotEmpty(dto.getBuId()))
			user.setBuId(dto.getBuId());
		user.setIsPointOfContact(dto.getIsPointOfContact());
		return user;
	}

	public static UserDto mapUserToUserDto(User user) {

		UserDto dto = new UserDto();

		UserPersonalDetailsDto userDetails = new UserPersonalDetailsDto();
		userDetails.setUserId(user.getId());
		userDetails.setEmail(user.getEmail());
		userDetails.setFirstName(user.getFirstname());
		userDetails.setLastName(user.getLastname());
		userDetails.setIm(user.getIm());
		userDetails.setMobileNumber(user.getMobilephone());
		userDetails.setWorkTelephone(user.getWorkphone());
		userDetails.setRole(user.getRole());
		if (user.getAddress() != null) {

			userDetails.setAddress1(user.getAddress().getAddress1());
			userDetails.setAddress2(user.getAddress().getAddress2());
			userDetails.setCity(user.getAddress().getCity().getName());
			userDetails.setCountry(user.getAddress().getCountry());
			userDetails.setCounty(user.getAddress().getCounty());
			userDetails.setState(user.getAddress().getState());
			userDetails.setZipcode(user.getAddress().getZipcode());
		}
		userDetails.setTitle(user.getTitle());
		dto.setUserDetails(userDetails);

		UserForwardRecipientDetailsDto recipientDto = new UserForwardRecipientDetailsDto();
		if (user.getForwardFromStartDate() != null) {

			DateTime startDate = new DateTime(Long.parseLong(user
					.getForwardFromStartDate()));
			recipientDto.setFromDate(startDate.toString());
		}
		if (user.getForwardToEndDate() != null) {

			DateTime toDate = new DateTime(Long.parseLong(user
					.getForwardToEndDate()));
			recipientDto.setToDate(toDate.toString());
		}

		dto.setUserForwardRecipientDetails(recipientDto);

		AuditDto auditDto = new AuditDto();
		if (user.getCreatedUser() != null) {
			auditDto.setCreatedBy(user.getCreatedUser());
		}
		if (user.getLastModifiedUser() != null) {
			auditDto.setLastModifiedBy(user.getLastModifiedUser());
		}
		dto.setUserState(user.getAccessControlList().getAccountState());
		dto.setAuditDto(auditDto);
		dto.setIsPointOfContact(user.getIsPointOfContact());
		return dto;
	}

	public static void validateFields(UserDto userDto) {
		Validate.notNull(userDto.getUserDetails().getEmail(), Messages.EMAIL_MANDATORY);
		Validate.notNull(userDto.getUserDetails().getFirstName(), Messages.FIRST_NAME_MANDATORY);
		Validate.notNull(userDto.getUserDetails().getZipcode(), Messages.ZIPCODE_MANDATORY);
		Validate.notNull(userDto.getUserDetails().getCountry(),Messages.COUNTRY_MANDATORY);
		Validate.notNull(userDto.getUserDetails().getMobileNumber(),Messages.MOBILE_MANDATORY);
		Validate.notNull(userDto.getUserDetails().getCity(), Messages.CITY_MANDATORY);
		Validate.notNull(userDto.getUserDetails().getAddress1(), Messages.ADDRESS1_MANDATORY);

		validateEmptyFields(userDto);
	}

	private static void validateEmptyFields(UserDto userDto) {

		Validate.notBlank(userDto.getUserDetails().getEmail(), Messages.EMAIL_MANDATORY);
		Validate.notBlank(userDto.getUserDetails().getFirstName(),Messages.FIRST_NAME_MANDATORY);
		Validate.notBlank(userDto.getUserDetails().getZipcode(), Messages.ZIPCODE_MANDATORY);
		Validate.notBlank(userDto.getUserDetails().getCountry(), Messages.COUNTRY_MANDATORY);
		Validate.notBlank(userDto.getUserDetails().getMobileNumber(), Messages.MOBILE_MANDATORY);
		Validate.notBlank(userDto.getUserDetails().getCity(), Messages.CITY_MANDATORY);
		Validate.notBlank(userDto.getUserDetails().getAddress1(), Messages.ADDRESS1_MANDATORY);
	}

}
