package com.fourdotfive.missioncontrol.dtos.user;

public class UserFilterDTO {
    private String companyOrClientBuId;
    private String firstName;
    private String lastName;
    private String email;
    private String requisitionId;
    private String loggedInCompanyId;

    public String getCompanyOrClientBuId() {
        return companyOrClientBuId;
    }

    public void setCompanyOrClientBuId(String companyOrClientBuId) {
        this.companyOrClientBuId = companyOrClientBuId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRequisitionId() {
        return requisitionId;
    }

    public void setRequisitionId(String requisitionId) {
        this.requisitionId = requisitionId;
    }

    public String getLoggedInCompanyId() {
        return loggedInCompanyId;
    }

    public void setLoggedInCompanyId(String loggedInCompanyId) {
        this.loggedInCompanyId = loggedInCompanyId;
    }
}
