package com.fourdotfive.missioncontrol.dtos.user;

public class UserForwardRecipientDetailsDto {

	private String fromDate;

	private String toDate;

	private TeamMemberDto forwardRecipient;

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public TeamMemberDto getForwardRecipient() {
		return forwardRecipient;
	}

	public void setForwardRecipient(TeamMemberDto forwardRecipient) {
		this.forwardRecipient = forwardRecipient;
	}

	@Override
	public String toString() {
		return "UserForwardRecipientDetailsDto [fromDate=" + fromDate + ", toDate=" + toDate + ", forwardRecipient="
				+ forwardRecipient + "]";
	}

}