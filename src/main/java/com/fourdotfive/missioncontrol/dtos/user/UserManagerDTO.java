package com.fourdotfive.missioncontrol.dtos.user;

import com.fourdotfive.missioncontrol.pojo.user.User;
import org.apache.commons.lang3.StringUtils;

public class UserManagerDTO {

    private String id;
    private String firstName;
    private String lastName;

    public UserManagerDTO() {
    }

    public UserManagerDTO(User user) {
        this.id = user.getId();
        if(StringUtils.isNotEmpty(user.getLastname()))
            this.lastName = user.getLastname();
        this.firstName = user.getFirstname();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
