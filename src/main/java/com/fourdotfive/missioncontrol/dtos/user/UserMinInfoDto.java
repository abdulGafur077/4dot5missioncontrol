package com.fourdotfive.missioncontrol.dtos.user;

import com.fourdotfive.missioncontrol.dtos.company.CompanyMinInfoDto;
import com.fourdotfive.missioncontrol.dtos.plan.PlanMinDto;
import com.fourdotfive.missioncontrol.pojo.user.Role;
import com.fourdotfive.missioncontrol.pojo.user.RoleScreenXRef;
import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.pojo.user.VerifyPassword;

public class UserMinInfoDto {

	private String id;

	private String firstname;

	private String lastname;

	private String emailId;

	private String token;

	private CompanyMinInfoDto company;

	private String refreshToken;

	private String accountState;

	private RoleScreenXRef roleScreenRef;

	private Role role;

	private PlanMinDto planMinDto;

	private boolean shouldPopupBeDisplayed;

	private boolean userScopeChanged;

	private boolean userHasNoClients;

	private Long version;

	public boolean isShouldPopupBeDisplayed() {
		return shouldPopupBeDisplayed;
	}

	public void setShouldPopupBeDisplayed(boolean shouldPopupBeDisplayed) {
		this.shouldPopupBeDisplayed = shouldPopupBeDisplayed;
	}

	public UserMinInfoDto() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public String getAccountState() {
		return accountState;
	}

	public void setAccountState(String accountState) {
		this.accountState = accountState;
	}

	public RoleScreenXRef getRoleScreenRef() {
		return roleScreenRef;
	}

	public void setRoleScreenRef(RoleScreenXRef roleScreenRef) {
		this.roleScreenRef = roleScreenRef;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public CompanyMinInfoDto getCompany() {
		return company;
	}

	public void setCompany(CompanyMinInfoDto company) {
		this.company = company;
	}

	public PlanMinDto getPlanMinDto() {
		return planMinDto;
	}

	public void setPlanMinDto(PlanMinDto planMinDto) {
		this.planMinDto = planMinDto;
	}

	public static UserMinInfoDto getMinInfo(VerifyPassword user) {

		UserMinInfoDto dto = new UserMinInfoDto();
		dto.setFirstname(user.getFirstname());
		dto.setLastname(user.getLastname());
		dto.setAccountState(user.getAccountState());
		dto.setRoleScreenRef(user.getRoleScreenXRef());
		dto.setCompany(user.getCompany());
		return dto;
	}

	public static UserMinInfoDto getMinInfo(User user, RoleScreenXRef screenRef) {
	    UserMinInfoDto userMinInfoDto = createUserMinInfoDto(user);
        userMinInfoDto.setRoleScreenRef(screenRef);
        userMinInfoDto.getRoleScreenRef().setRole(user.getRole());
        return userMinInfoDto;
	}

	public static UserMinInfoDto getMinInfo(User user, RoleScreenXRef screenRef, PlanMinDto planMinDto) {
	    UserMinInfoDto userMinInfoDto = createUserMinInfoDto(user);
        userMinInfoDto.setRoleScreenRef(screenRef);
        userMinInfoDto.getRoleScreenRef().setRole(user.getRole());
        userMinInfoDto.setPlanMinDto(planMinDto);
        userMinInfoDto.setVersion(user.getVersion());
        return userMinInfoDto;
	}

	public static UserMinInfoDto createUserMinInfoDto(User user) {
        UserMinInfoDto dto = new UserMinInfoDto();
        dto.setId(user.getId());
        dto.setFirstname(user.getFirstname());
        dto.setCompany(new CompanyMinInfoDto(user.getEmployer().getName(), user
                .getEmployer().getId(), user.getEmployer().getCompanyType(),
                user.getEmployer().getCompanyState()));
        dto.setLastname(user.getLastname());
        dto.setAccountState(user.getAccessControlList().getAccountState());
        return dto;
    }


	public static UserMinInfoDto getUserMinInfo(User user, PlanMinDto planMinDto) {
        UserMinInfoDto userMinInfoDto = createUserMinInfoDto(user);
        userMinInfoDto.setPlanMinDto(planMinDto);
		userMinInfoDto.setVersion(user.getVersion());
        return userMinInfoDto;
	}


	public boolean isUserScopeChanged() {
		return userScopeChanged;
	}

	public void setUserScopeChanged(boolean userScopeChanged) {
		this.userScopeChanged = userScopeChanged;
	}

	public boolean isUserHasNoClients() {
		return userHasNoClients;
	}

	public void setUserHasNoClients(boolean userHasNoClients) {
		this.userHasNoClients = userHasNoClients;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return "UserMinInfoDto{" +
				"id='" + id + '\'' +
				", firstname='" + firstname + '\'' +
				", lastname='" + lastname + '\'' +
				", emailId='" + emailId + '\'' +
				", token='" + token + '\'' +
				", company=" + company +
				", refreshToken='" + refreshToken + '\'' +
				", accountState='" + accountState + '\'' +
				", roleScreenRef=" + roleScreenRef +
				", role=" + role +
				", planMinDto=" + planMinDto +
				", shouldPopupBeDisplayed=" + shouldPopupBeDisplayed +
				", userScopeChanged=" + userScopeChanged +
				", userHasNoClients=" + userHasNoClients +
				'}';
	}
}
