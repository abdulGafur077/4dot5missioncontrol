package com.fourdotfive.missioncontrol.dtos.user;

import java.util.List;

public class VendorRecruitersCollectionRequestDto {

    private List<String> vendorIds;

    private String requisitionId;

    public List<String> getVendorIds() {
        return vendorIds;
    }

    public void setVendorIds(List<String> vendorIds) {
        this.vendorIds = vendorIds;
    }

    public String getRequisitionId() {
        return requisitionId;
    }

    public void setRequisitionId(String requisitionId) {
        this.requisitionId = requisitionId;
    }
}
