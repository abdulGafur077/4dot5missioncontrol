package com.fourdotfive.missioncontrol.dtos.user;

public class VerifyOtpDto {

	private String oneTimePassword;

	private String emailId;

	public String getOneTimePassword() {
		return oneTimePassword;
	}

	public void setOneTimePassword(String oneTimePassword) {
		this.oneTimePassword = oneTimePassword;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	public VerifyOtpDto() {
	}

	public VerifyOtpDto( String emailId , String oneTimePassword) {
		super();
		this.oneTimePassword = oneTimePassword;
		this.emailId = emailId;
	}

	@Override
	public String toString() {
		return "VerifyOtpDto [oneTimePassword=" + oneTimePassword + ", emailId=" + emailId + "]";
	}
	
}
