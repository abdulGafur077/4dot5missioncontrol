package com.fourdotfive.missioncontrol.dtos.vendor;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class AssociateVendorsDto {

    @NotNull
    @NotEmpty
    private String corporateCompanyId;

    @NotNull
    @NotEmpty
    private List<VendorDto> vendorsList = new ArrayList<>();


    public String getCorporateCompanyId() {
        return corporateCompanyId;
    }

    public void setCorporateCompanyId(String corporateCompanyId) {
        this.corporateCompanyId = corporateCompanyId;
    }

    public List<VendorDto> getVendorsList() {
        return vendorsList;
    }

    public void setVendorsList(List<VendorDto> vendorsList) {
        this.vendorsList = vendorsList;
    }
}
