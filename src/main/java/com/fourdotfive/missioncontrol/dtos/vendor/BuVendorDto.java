package com.fourdotfive.missioncontrol.dtos.vendor;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class BuVendorDto {

    @NotNull
    @NotEmpty
    private String buId;

    @NotNull
    @NotEmpty
    private String corporateCompanyId;

    private List<SelectedVendorDto> vendorDtoList = new ArrayList<>();

    private String note;

    public String getBuId() {
        return buId;
    }

    public void setBuId(String buId) {
        this.buId = buId;
    }

    public String getCorporateCompanyId() {
        return corporateCompanyId;
    }

    public void setCorporateCompanyId(String corporateCompanyId) {
        this.corporateCompanyId = corporateCompanyId;
    }

    public List<SelectedVendorDto> getVendorDtoList() {
        return vendorDtoList;
    }

    public void setVendorDtoList(List<SelectedVendorDto> vendorDtoList) {
        this.vendorDtoList = vendorDtoList;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
