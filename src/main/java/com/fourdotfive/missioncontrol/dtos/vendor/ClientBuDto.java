package com.fourdotfive.missioncontrol.dtos.vendor;

import com.fourdotfive.missioncontrol.pojo.user.Contact;

import java.util.ArrayList;
import java.util.List;

public class ClientBuDto {

    private String buId;

    private String buName;

    private String buShortName;

    private List<Contact> buContactList = new ArrayList<>();

    private boolean userPermissibility;

    public String getBuId() {
        return buId;
    }

    public void setBuId(String buId) {
        this.buId = buId;
    }

    public String getBuName() {
        return buName;
    }

    public void setBuName(String buName) {
        this.buName = buName;
    }

    public String getBuShortName() {
        return buShortName;
    }

    public void setBuShortName(String buShortName) {
        this.buShortName = buShortName;
    }

    public List<Contact> getBuContactList() {
        return buContactList;
    }

    public void setBuContactList(List<Contact> buContactList) {
        this.buContactList = buContactList;
    }

    public boolean isUserPermissibility() {
        return userPermissibility;
    }

    public void setUserPermissibility(boolean userPermissibility) {
        this.userPermissibility = userPermissibility;
    }
}
