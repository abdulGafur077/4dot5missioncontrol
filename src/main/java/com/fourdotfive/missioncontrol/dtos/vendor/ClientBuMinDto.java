package com.fourdotfive.missioncontrol.dtos.vendor;

import com.fourdotfive.missioncontrol.dtos.company.ContactDto;

import java.util.ArrayList;
import java.util.List;

public class ClientBuMinDto {

    private String buId;

    private String buName;

    private String buShortName;

    private List<ContactDto> buContactList = new ArrayList<>();

    private boolean userPermissibility;

    public ClientBuMinDto() {
    }

    public ClientBuMinDto(ClientBuDto clientBuDto) {
        this.buId = clientBuDto.getBuId();
        this.buName = clientBuDto.getBuName();
        this.buShortName = clientBuDto.getBuShortName();
        this.userPermissibility = clientBuDto.isUserPermissibility();
        if (clientBuDto.getBuContactList() != null)
            this.buContactList = ContactDto.getContactListDto(clientBuDto.getBuContactList());
    }

    public String getBuId() {
        return buId;
    }

    public void setBuId(String buId) {
        this.buId = buId;
    }

    public String getBuName() {
        return buName;
    }

    public void setBuName(String buName) {
        this.buName = buName;
    }

    public String getBuShortName() {
        return buShortName;
    }

    public void setBuShortName(String buShortName) {
        this.buShortName = buShortName;
    }

    public List<ContactDto> getBuContactList() {
        return buContactList;
    }

    public void setBuContactList(List<ContactDto> buContactList) {
        this.buContactList = buContactList;
    }

    public boolean isUserPermissibility() {
        return userPermissibility;
    }

    public void setUserPermissibility(boolean userPermissibility) {
        this.userPermissibility = userPermissibility;
    }
}
