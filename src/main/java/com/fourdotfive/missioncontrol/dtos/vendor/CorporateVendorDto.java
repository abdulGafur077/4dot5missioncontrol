package com.fourdotfive.missioncontrol.dtos.vendor;


import com.fourdotfive.missioncontrol.dtos.company.ContactDto;
import com.fourdotfive.missioncontrol.licensepreference.LicensePreferences;
import com.fourdotfive.missioncontrol.pojo.setting.Setting;
import com.fourdotfive.missioncontrol.pojo.user.Contact;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class CorporateVendorDto {

    private String id;

    @NotNull
    @NotEmpty
    private String corporateCompanyId;

    @NotNull
    @NotEmpty
    private String vendorId;

    private String vendorName;

    private String vendorShortName;

    private List<Contact> contactList = new ArrayList<Contact>();

    private List<Contact> corporateCompanyContactList = new ArrayList<>();

    private LicensePreferences clientLicensePreferences;

    private Setting clientSettings;

    public CorporateVendorDto() {
    }

    public CorporateVendorDto(CorporateVendorMinDto corporateVendorMinDto) {
        this.id = corporateVendorMinDto.getId();
        this.corporateCompanyId = corporateVendorMinDto.getCorporateCompanyId();
        this.vendorId = corporateVendorMinDto.getVendorId();
        this.vendorName = corporateVendorMinDto.getVendorName();
        this.vendorShortName = corporateVendorMinDto.getVendorShortName();
        if (corporateVendorMinDto.getContactList() != null)
            this.contactList = ContactDto.getContactList(corporateVendorMinDto.getContactList());
        if (corporateVendorMinDto.getCorporateCompanyContactList() != null)
            this.corporateCompanyContactList = ContactDto.getContactList(corporateVendorMinDto.getCorporateCompanyContactList());
        this.clientLicensePreferences = corporateVendorMinDto.getClientLicensePreferences();
        this.clientSettings = corporateVendorMinDto.getClientSettings();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCorporateCompanyId() {
        return corporateCompanyId;
    }

    public void setCorporateCompanyId(String corporateCompanyId) {
        this.corporateCompanyId = corporateCompanyId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public List<Contact> getContactList() {
        return contactList;
    }

    public void setContactList(List<Contact> contactList) {
        this.contactList = contactList;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorShortName() {
        return vendorShortName;
    }

    public void setVendorShortName(String vendorShortName) {
        this.vendorShortName = vendorShortName;
    }

    public List<Contact> getCorporateCompanyContactList() {
        return corporateCompanyContactList;
    }

    public void setCorporateCompanyContactList(List<Contact> corporateCompanyContactList) {
        this.corporateCompanyContactList = corporateCompanyContactList;
    }

    public LicensePreferences getClientLicensePreferences() {
        return clientLicensePreferences;
    }

    public void setClientLicensePreferences(LicensePreferences clientLicensePreferences) {
        this.clientLicensePreferences = clientLicensePreferences;
    }

    public Setting getClientSettings() {
        return clientSettings;
    }

    public void setClientSettings(Setting clientSettings) {
        this.clientSettings = clientSettings;
    }
}
