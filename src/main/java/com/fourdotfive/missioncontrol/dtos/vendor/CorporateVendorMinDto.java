package com.fourdotfive.missioncontrol.dtos.vendor;


import com.fourdotfive.missioncontrol.dtos.company.ContactDto;
import com.fourdotfive.missioncontrol.licensepreference.LicensePreferences;
import com.fourdotfive.missioncontrol.pojo.setting.Setting;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class CorporateVendorMinDto {

    private String id;

    @NotNull
    @NotEmpty
    private String corporateCompanyId;

    @NotNull
    @NotEmpty
    private String vendorId;

    private String vendorName;

    private String vendorShortName;

    private List<ContactDto> contactList = new ArrayList<>();

    private List<ContactDto> corporateCompanyContactList = new ArrayList<>();

    private LicensePreferences clientLicensePreferences;

    private Setting clientSettings;

    public CorporateVendorMinDto() {
    }

    public CorporateVendorMinDto(CorporateVendorDto corporateVendorDto) {
        this.id = corporateVendorDto.getId();
        this.corporateCompanyId = corporateVendorDto.getCorporateCompanyId();
        this.vendorId = corporateVendorDto.getVendorId();
        this.vendorName = corporateVendorDto.getVendorName();
        this.vendorShortName = corporateVendorDto.getVendorShortName();
        if (corporateVendorDto.getContactList() != null)
            this.contactList = ContactDto.getContactListDto(corporateVendorDto.getContactList());
        if (corporateVendorDto.getCorporateCompanyContactList() != null)
            this.corporateCompanyContactList = ContactDto.getContactListDto(corporateVendorDto.getCorporateCompanyContactList());
        this.clientLicensePreferences = corporateVendorDto.getClientLicensePreferences();
        if(corporateVendorDto.getClientSettings() != null)
            this.clientSettings = corporateVendorDto.getClientSettings();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCorporateCompanyId() {
        return corporateCompanyId;
    }

    public void setCorporateCompanyId(String corporateCompanyId) {
        this.corporateCompanyId = corporateCompanyId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorShortName() {
        return vendorShortName;
    }

    public void setVendorShortName(String vendorShortName) {
        this.vendorShortName = vendorShortName;
    }

    public List<ContactDto> getContactList() {
        return contactList;
    }

    public void setContactList(List<ContactDto> contactList) {
        this.contactList = contactList;
    }

    public List<ContactDto> getCorporateCompanyContactList() {
        return corporateCompanyContactList;
    }

    public void setCorporateCompanyContactList(List<ContactDto> corporateCompanyContactList) {
        this.corporateCompanyContactList = corporateCompanyContactList;
    }

    public LicensePreferences getClientLicensePreferences() {
        return clientLicensePreferences;
    }

    public void setClientLicensePreferences(LicensePreferences clientLicensePreferences) {
        this.clientLicensePreferences = clientLicensePreferences;
    }

    public Setting getClientSettings() {
        return clientSettings;
    }

    public void setClientSettings(Setting clientSettings) {
        this.clientSettings = clientSettings;
    }
}
