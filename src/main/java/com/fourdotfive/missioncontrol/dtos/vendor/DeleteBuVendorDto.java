package com.fourdotfive.missioncontrol.dtos.vendor;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

public class DeleteBuVendorDto {

    private String buId;

    @NotNull
    @NotEmpty
    private String corporateCompanyId;

    @NotNull
    @NotEmpty
    private String vendorId;

    private String loggedInUserId;

    public DeleteBuVendorDto() {
    }

    public String getBuId() {
        return buId;
    }

    public void setBuId(String buId) {
        this.buId = buId;
    }

    public String getCorporateCompanyId() {
        return corporateCompanyId;
    }

    public void setCorporateCompanyId(String corporateCompanyId) {
        this.corporateCompanyId = corporateCompanyId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getLoggedInUserId() {
        return loggedInUserId;
    }

    public void setLoggedInUserId(String loggedInUserId) {
        this.loggedInUserId = loggedInUserId;
    }
}
