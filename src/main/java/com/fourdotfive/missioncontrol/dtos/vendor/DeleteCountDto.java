package com.fourdotfive.missioncontrol.dtos.vendor;

public class DeleteCountDto {
    private int requisitionCount;
    private int buCount;

    public int getRequisitionCount() {
        return requisitionCount;
    }

    public void setRequisitionCount(int requisitionCount) {
        this.requisitionCount = requisitionCount;
    }

    public int getBuCount() {
        return buCount;
    }

    public void setBuCount(int buCount) {
        this.buCount = buCount;
    }
}
