package com.fourdotfive.missioncontrol.dtos.vendor;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

public class SelectedVendorDto {

    @NotNull
    @NotEmpty
    private String vendorId;

    private String vendorName;

    private String vendorShortName;

    private boolean isSelected;

    public SelectedVendorDto() {
    }

    public SelectedVendorDto(String vendorId, String vendorName, String vendorShortName) {
        this.vendorId = vendorId;
        this.vendorName = vendorName;
        this.vendorShortName = vendorShortName;
        this.isSelected = false;
    }


    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorShortName() {
        return vendorShortName;
    }

    public void setVendorShortName(String vendorShortName) {
        this.vendorShortName = vendorShortName;
    }

    public boolean getIsSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
