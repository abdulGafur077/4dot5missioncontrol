package com.fourdotfive.missioncontrol.dtos.vendor;

import com.fourdotfive.missioncontrol.pojo.user.Contact;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;


public class VendorDto {

    @NotNull
    @NotEmpty
    private String vendorId;

    private List<Contact> contactList = new ArrayList<>();

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public List<Contact> getContactList() {
        return contactList;
    }

    public void setContactList(List<Contact> contactList) {
        this.contactList = contactList;
    }
}
