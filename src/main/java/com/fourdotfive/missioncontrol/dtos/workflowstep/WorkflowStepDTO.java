package com.fourdotfive.missioncontrol.dtos.workflowstep;

import com.fourdotfive.missioncontrol.licensepreference.JobLicensePreferences;

import java.util.List;
import java.util.Map;

public class WorkflowStepDTO {

    private Map<String, List<String>> workflowStepsOrder;
    private String companyId;
    private String jobId;
    private String userId;
    private JobLicensePreferences oldJobLicensePreferences;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public Map<String, List<String>> getWorkflowStepsOrder() {
        return workflowStepsOrder;
    }

    public void setWorkflowStepsOrder(Map<String, List<String>> workflowStepsOrder) {
        this.workflowStepsOrder = workflowStepsOrder;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public JobLicensePreferences getOldJobLicensePreferences() {
        return oldJobLicensePreferences;
    }

    public void setOldJobLicensePreferences(JobLicensePreferences oldJobLicensePreferences) {
        this.oldJobLicensePreferences = oldJobLicensePreferences;
    }
}
