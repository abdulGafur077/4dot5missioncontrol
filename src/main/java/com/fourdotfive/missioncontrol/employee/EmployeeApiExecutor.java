package com.fourdotfive.missioncontrol.employee;

import com.fourdotfive.missioncontrol.dtos.employee.*;

import com.fourdotfive.missioncontrol.pojo.employee.EmployeeDTO;

public interface EmployeeApiExecutor {

    String createOrUpdateEmployee(EmployeeDTO employeeDTO);

    String searchEmployee(EmployeeSearchDTO employeeSearchDTO);

    String getDepartments(DepartmentFilterDTO departmentFilterDTO);

    String convertUserToEmployee(EmployeeToUserRequestDTO employeeToUserRequestDTO);

    String convertEmployeeToUser(EmployeeUserCovertDTO employeeUserCovertDTO);

    String getEmployee(String employeeId);

    String getBusByUserScope(BUFilterDTO buFilterDTO);

    String getOrganizationalManager(OrganizationManagerRequestDTO organizationManagerRequestDto);

    DepartmentDto getDepartmentById(String departmentId);

    String  getPermissibleManagers(EmployeeManagerRequestDto employeeManagerRequestDto);
}
