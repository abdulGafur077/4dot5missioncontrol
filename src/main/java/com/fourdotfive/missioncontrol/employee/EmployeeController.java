package com.fourdotfive.missioncontrol.employee;

import com.fourdotfive.missioncontrol.dtos.employee.*;
import com.fourdotfive.missioncontrol.dtos.user.UserDto;
import com.fourdotfive.missioncontrol.exception.PlatformException;
import com.fourdotfive.missioncontrol.pojo.employee.EmployeeDTO;
import com.fourdotfive.missioncontrol.user.UserService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/employee")
public class EmployeeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeController.class);

    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/createorupdate", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String saveEmployee(@RequestBody EmployeeDTO employeeDTO) {
        if(StringUtils.isBlank(employeeDTO.getCompanyId()))
            throw new PlatformException("Company ID can't be null.");

        return employeeService.createOrUpdateEmployee(employeeDTO);
    }

    @RequestMapping(value = "/searchemployees", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String searchEmployee(@RequestBody EmployeeRequestDTO employeeRequestDto) {
        return employeeService.searchEmployee(employeeRequestDto);
    }

    @RequestMapping(value = "/getdepartments", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getDepartments(@RequestBody DepartmentSearchDTO departmentSearchDTO) {
        if (StringUtils.isBlank(departmentSearchDTO.getCompanyId()))
            throw new PlatformException("Company ID can't be null.");

        return employeeService.getDepartments(departmentSearchDTO);
    }

    @RequestMapping(value = "convertusertoemployee", method = {RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_VALUE)
    public String convertUserToEmployee(@RequestBody EmployeeToUserRequestDTO employeeToUserRequestDTO) {
        if (StringUtils.isBlank(employeeToUserRequestDTO.getUserId()))
            throw new PlatformException("User ID can't be null.");

        return employeeService.convertUserToEmployee(employeeToUserRequestDTO);
    }

    @RequestMapping(value = "convertemployeetouser", method = {RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity<UserDto>  convertEmployeeToUser(@RequestBody EmployeeUserCovertDTO employeeUserCovertDTO) {
        if (StringUtils.isBlank(employeeUserCovertDTO.getId()))
            throw new PlatformException("Employee ID can't be null.");
            String convertedUserId = employeeService.convertEmployeeToUser(employeeUserCovertDTO);
            if (StringUtils.isBlank(convertedUserId))
                throw new PlatformException("Employee to User conversion failed.");

            UserDto userDto = userService.getUserDtoById(convertedUserId);
            return new ResponseEntity<>(userDto, HttpStatus.OK);
    }

    @RequestMapping(value = "/getemployee/{employeeId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getEmployee(@PathVariable(value = "employeeId") String employeeId) {
        if (StringUtils.isBlank(employeeId))
            throw new PlatformException("Employee ID can't be null.");

        return employeeService.getEmployee(employeeId);
    }

    @RequestMapping(value = "/getbusbyuserscope", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getBusByUserUserScope(@RequestBody BUSearchDTO buSearchDTO) {
        if (StringUtils.isBlank(buSearchDTO.getCompanyId()))
            throw new PlatformException("Company ID can't be null.");

        return employeeService.getBusByUserUserScope(buSearchDTO);
    }

    @RequestMapping(value = "/getorganizationalmanagers", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getOrganizationalManager(@RequestBody OrganizationManagerRequestDTO organizationManagerRequestDto) {
        if (StringUtils.isBlank(organizationManagerRequestDto.getCompanyId()))
            throw new PlatformException("Company ID can't be null");

        return employeeService.getOrganizationalManager(organizationManagerRequestDto);
    }

    @RequestMapping(value = "getPermissibleManagers", method = {RequestMethod.POST})
    public @ResponseBody
    ResponseEntity<String> getPermissibleManagers(@RequestBody EmployeeManagerRequestDto employeeManagerRequestDto) {
        String employeeManagerDTO = employeeService.getPermissibleManagers(employeeManagerRequestDto);
        return new ResponseEntity<>(employeeManagerDTO, HttpStatus.OK);
    }
}
