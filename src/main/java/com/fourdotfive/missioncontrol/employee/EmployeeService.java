package com.fourdotfive.missioncontrol.employee;

import com.fourdotfive.missioncontrol.dtos.employee.*;
import com.fourdotfive.missioncontrol.pojo.employee.EmployeeDTO;

public interface EmployeeService {

    String createOrUpdateEmployee(EmployeeDTO employeeDto);

    String searchEmployee(EmployeeRequestDTO employeeRequestDto);

    String getDepartments(DepartmentSearchDTO departmentSearchDto);

    String convertUserToEmployee(EmployeeToUserRequestDTO employeeToUserRequestDTO);

    String convertEmployeeToUser(EmployeeUserCovertDTO employeeUserCovertDTO);

    String getEmployee(String employeeId);

    String getBusByUserUserScope(BUSearchDTO buSearchDTO);

    String getOrganizationalManager(OrganizationManagerRequestDTO organizationManagerRequestDto);

    DepartmentDto getDepartmentById(String departmentId);

    String getPermissibleManagers(EmployeeManagerRequestDto employeeManagerRequestDto);
}
