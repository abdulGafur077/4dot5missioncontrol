package com.fourdotfive.missioncontrol.employee;

import com.fourdotfive.missioncontrol.company.CompanyService;
import com.fourdotfive.missioncontrol.dtos.employee.*;
import com.fourdotfive.missioncontrol.pojo.employee.EmployeeDTO;
import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeServiceImp implements EmployeeService {

    @Autowired
    private EmployeeApiExecutor employeeApiExecutor;

    @Autowired
    private UserService userService;

    @Autowired
    private CompanyService companyService;

    @Override
    public String createOrUpdateEmployee(EmployeeDTO employeeDTO) {
        return employeeApiExecutor.createOrUpdateEmployee(employeeDTO);
    }

    @Override
    public String searchEmployee(EmployeeRequestDTO employeeRequestDto) {

        User loggedInUser = userService.getCurrentLoggedInUser();
        boolean isAdmin = userService.isAdminUser(loggedInUser);
        EmployeeSearchDTO employeeSearchDTO = new EmployeeSearchDTO(
                employeeRequestDto.getSearchText(),
                employeeRequestDto.getPage(),
                employeeRequestDto.getSize(),
                employeeRequestDto.getSortColumn(),
                employeeRequestDto.getSortDirection(),
                isAdmin,
                loggedInUser.getId(),
                employeeRequestDto.getCompanyId());
        return employeeApiExecutor.searchEmployee(employeeSearchDTO);
    }

    @Override
    public String getDepartments(DepartmentSearchDTO departmentSearchDto) {
        User currentLoggedInUser = userService.getCurrentLoggedInUser();
        boolean isAdmin = userService.isAdminUser(currentLoggedInUser);
        DepartmentFilterDTO departmentFilterDTO = new DepartmentFilterDTO(departmentSearchDto.getCompanyId(), departmentSearchDto.getSearchText(), isAdmin, currentLoggedInUser.getId());
        return employeeApiExecutor.getDepartments(departmentFilterDTO);
    }

    @Override
    public String convertUserToEmployee(EmployeeToUserRequestDTO employeeToUserRequestDTO) {
        User user = userService.getCurrentLoggedInUser();
        employeeToUserRequestDTO.setLoggedInUserId(user.getId());
        return employeeApiExecutor.convertUserToEmployee(employeeToUserRequestDTO);
    }

    @Override
    public String convertEmployeeToUser(EmployeeUserCovertDTO employeeUserCovertDTO) {
        return employeeApiExecutor.convertEmployeeToUser(employeeUserCovertDTO);
    }

    @Override
    public String getEmployee(String employeeId) {
        return employeeApiExecutor.getEmployee(employeeId);
    }

    @Override
    public String getBusByUserUserScope(BUSearchDTO buSearchDTO) {
        User currentLoggedInUser = userService.getCurrentLoggedInUser();
        boolean isAdmin = userService.isAdminUser(currentLoggedInUser);
        BUFilterDTO buFilterDTO = new BUFilterDTO(buSearchDTO.getCompanyId(), buSearchDTO.getSearchText(), isAdmin, currentLoggedInUser.getId());
        return employeeApiExecutor.getBusByUserScope(buFilterDTO);
    }

    @Override
    public String getOrganizationalManager(OrganizationManagerRequestDTO organizationManagerRequestDto) {
        return employeeApiExecutor.getOrganizationalManager(organizationManagerRequestDto);
    }

    @Override
    public DepartmentDto getDepartmentById(String departmentId) {
        return employeeApiExecutor.getDepartmentById(departmentId);
    }

    @Override
    public String getPermissibleManagers(EmployeeManagerRequestDto employeeManagerRequestDto) {
        return employeeApiExecutor.getPermissibleManagers(employeeManagerRequestDto);
    }
}
