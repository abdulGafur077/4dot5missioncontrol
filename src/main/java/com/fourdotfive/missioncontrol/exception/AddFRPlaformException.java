package com.fourdotfive.missioncontrol.exception;

public class AddFRPlaformException extends RuntimeException {

	private static final long serialVersionUID = -9174772883273084079L;

	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public AddFRPlaformException(String message, Object... args) {
		this.message = String.format(message, args);
	}
	public AddFRPlaformException(String message) {
		this.message = message;
	}
	@Override
	public String toString() {
		return "UserException [message=" + message + "]";
	}
}
