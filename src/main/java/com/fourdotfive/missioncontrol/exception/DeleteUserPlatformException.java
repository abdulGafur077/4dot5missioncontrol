package com.fourdotfive.missioncontrol.exception;

public class DeleteUserPlatformException extends RuntimeException{

	private static final long serialVersionUID = 2545450376493991305L;
	
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public DeleteUserPlatformException(String message, Object... args) {
		this.message = String.format(message, args);
	}
	public DeleteUserPlatformException(String message) {
		this.message = message;
	}
	@Override
	public String toString() {
		return "DeleteUserPlatformException [message=" + message + "]";
	}
}
