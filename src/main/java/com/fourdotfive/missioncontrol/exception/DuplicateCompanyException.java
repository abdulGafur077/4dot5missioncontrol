package com.fourdotfive.missioncontrol.exception;

public class DuplicateCompanyException extends RuntimeException{
	
	
	private static final long serialVersionUID = 1L;
	private String message;
	private String developerMessage;
	
	public static final String SHORTNAME_AND_NAME = "100";
	public static final String SHORTNAME = "101";
	public static final String NAME = "102";
	

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getDeveloperMessage() {
		return developerMessage;
	}

	public void setDeveloperMessage(String developerMessage) {
		this.developerMessage = developerMessage;
	}
	

	public DuplicateCompanyException(String message, Object... args) {
		this.message = String.format(message, args); 
	}

	@Override
	public String toString() {
		return "DuplicateCompanyException [message=" + message + ", developerMessage=" + developerMessage + "]";
	}
	
}
