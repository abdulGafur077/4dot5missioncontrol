package com.fourdotfive.missioncontrol.exception;

public class DuplicateUserException extends RuntimeException{

	private static final long serialVersionUID = 7758991448629306005L;
	
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public DuplicateUserException(String message, Object... args) {
		this.message = String.format(message, args); 
	}

	@Override
	public String toString() {
		return "DuplicateUserException [message=" + message + "]";
	}
	
}
