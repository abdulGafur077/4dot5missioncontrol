package com.fourdotfive.missioncontrol.exception;

public class FetchEnityScopePlatformException extends RuntimeException {

	private static final long serialVersionUID = 5880755626747533489L;

	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public FetchEnityScopePlatformException(String message, Object... args) {
		this.message = String.format(message, args);
	}
	public FetchEnityScopePlatformException(String message) {
		this.message =message;
	}
	@Override
	public String toString() {
		return "AssignManagerPlatformException [message=" + message + "]";
	}

}