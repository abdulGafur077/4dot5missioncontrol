package com.fourdotfive.missioncontrol.exception;

public class FileUploadException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public FileUploadException(String message) {
		setErrorMessage(message);
	}

	@Override
	public String getMessage() {
		return getErrorMessage();
	}

}
