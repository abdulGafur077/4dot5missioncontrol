package com.fourdotfive.missioncontrol.exception;

public class InvalidCredentialsException extends RuntimeException {

	private static final long serialVersionUID = -9181594798379436398L;
	
	private static final String DEV_MSG = "Invalid credentials";

	private String message;

	private String developerMessage;
	
	public InvalidCredentialsException() {
	}

	public InvalidCredentialsException(String message, Object... args) {
		this.message = String.format(message, args); 
		this.developerMessage = DEV_MSG;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDeveloperMessage() {
		return developerMessage;
	}

	public void setDeveloperMessage(String developerMessage) {
		this.developerMessage = developerMessage;
	}

}
