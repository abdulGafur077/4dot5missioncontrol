package com.fourdotfive.missioncontrol.exception;

/**
 * @author Shalini
 */

public class InvalidResetPwdCodeException extends RuntimeException {

	private static final long serialVersionUID = 547085356019519924L;

	private static final String DEV_MSG = "Invalid original password";

	private String message;

	private String developerMessage;

	public InvalidResetPwdCodeException(String message, Object... args) {
		this.message = String.format(message, args); 
		this.developerMessage = DEV_MSG;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDeveloperMessage() {
		return developerMessage;
	}

	public void setDeveloperMessage(String developerMessage) {
		this.developerMessage = developerMessage;
	}

}
