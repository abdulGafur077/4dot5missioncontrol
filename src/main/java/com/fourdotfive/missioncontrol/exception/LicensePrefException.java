package com.fourdotfive.missioncontrol.exception;

public class LicensePrefException extends RuntimeException{

	private static final long serialVersionUID = -8621364891404065876L;
	
	private String message;
	
	private String developerMessage;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDeveloperMessage() {
		return developerMessage;
	}

	public void setDeveloperMessage(String developerMessage) {
		this.developerMessage = developerMessage;
	}

	public LicensePrefException(String message, Object... args) {
		this.message = String.format(message, args); 
	}

	@Override
	public String toString() {
		return "LicensePrefException [message=" + message + "]";
	}
}
