package com.fourdotfive.missioncontrol.exception;

/**
 * @author Shalini
 */
public class PlatformException extends RuntimeException {

	private static final long serialVersionUID = 5437657828825428446L;

	private String message;

	private String developerMessage;

	public PlatformException() {
		// TODO Auto-generated constructor stub
	}

	public PlatformException(String message) {
		super();
		this.message = message;
	}

	public PlatformException(String message, Object... args ) {
		this.message = String.format(message, args); 
	}

	public PlatformException(String message, String developerMsg) {
		this.message = message;
		this.developerMessage = developerMsg;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDeveloperMessage() {
		return developerMessage;
	}

	public void setDeveloperMessage(String developerMessage) {
		this.developerMessage = developerMessage;
	}
}
