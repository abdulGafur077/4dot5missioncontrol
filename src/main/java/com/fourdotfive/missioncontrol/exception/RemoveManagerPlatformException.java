package com.fourdotfive.missioncontrol.exception;

public class RemoveManagerPlatformException extends RuntimeException {

	private static final long serialVersionUID = 6473116405912576398L;

	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public RemoveManagerPlatformException(String message, Object... args) {
		this.message = String.format(message, args);
	}
	
	public RemoveManagerPlatformException(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "RemoveManagerPlatformException [message=" + message + "]";
	}

}
