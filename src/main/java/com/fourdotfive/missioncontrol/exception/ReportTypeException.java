package com.fourdotfive.missioncontrol.exception;

public class ReportTypeException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1739423965105212455L;

	public static final String MSG = "Report type does not match";

	private static final String DEV_MSG = "Report type does not match";

	private String message;

	private String developerMessage;

	public ReportTypeException() {
		message = MSG;
		developerMessage = DEV_MSG;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDeveloperMessage() {
		return developerMessage;
	}

	public void setDeveloperMessage(String developerMessage) {
		this.developerMessage = developerMessage;
	}
}
