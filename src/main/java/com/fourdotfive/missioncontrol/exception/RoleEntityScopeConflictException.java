package com.fourdotfive.missioncontrol.exception;

public class RoleEntityScopeConflictException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public RoleEntityScopeConflictException(String message, Object... args) {
		this.message = String.format(message, args);
	}
	public RoleEntityScopeConflictException(String message) {
		this.message = message;
	}
	@Override
	public String toString() {
		return "DuplicateUserException [message=" + message + "]";
	}

}
