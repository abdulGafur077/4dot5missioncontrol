package com.fourdotfive.missioncontrol.external.callback;

import com.fourdotfive.missioncontrol.dtos.external.callback.IMCandidateReportCallbackDto;

public interface ExternalCallbackApiExecutor {
    String imCandidateReportCallback(IMCandidateReportCallbackDto callback);
}
