package com.fourdotfive.missioncontrol.external.callback;

import com.fourdotfive.missioncontrol.dtos.external.callback.IMCandidateReportCallbackDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/external/")
public class ExternalCallbackController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExternalCallbackController.class);
    private final ExternalCallbackService externalCallbackService;

    @Autowired
    public ExternalCallbackController(ExternalCallbackService externalCallbackService) {
        this.externalCallbackService = externalCallbackService;
    }

    @RequestMapping(value = "/imcandidatereportcallback", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String imCandidateReportCallback(@RequestBody IMCandidateReportCallbackDto imCandidateReportCallback) {
        LOGGER.info("imCandidateReportCallback: imCandidateReportCallback - " + imCandidateReportCallback);
        return externalCallbackService.imCandidateReportCallback(imCandidateReportCallback);
    }
}


