package com.fourdotfive.missioncontrol.external.callback;

import com.fourdotfive.missioncontrol.dtos.external.callback.IMCandidateReportCallbackDto;

public interface ExternalCallbackService {
    String imCandidateReportCallback(IMCandidateReportCallbackDto callback);
}
