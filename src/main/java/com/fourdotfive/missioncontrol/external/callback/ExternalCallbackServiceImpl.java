package com.fourdotfive.missioncontrol.external.callback;

import com.fourdotfive.missioncontrol.dtos.external.callback.IMCandidateReportCallbackDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExternalCallbackServiceImpl implements ExternalCallbackService {
    private final ExternalCallbackApiExecutor externalCallbackApiExecutor;

    @Autowired
    public ExternalCallbackServiceImpl(ExternalCallbackApiExecutor externalCallbackApiExecutor) {
        this.externalCallbackApiExecutor = externalCallbackApiExecutor;
    }

    @Override
    public String imCandidateReportCallback(IMCandidateReportCallbackDto callback) {
        return externalCallbackApiExecutor.imCandidateReportCallback(callback);
    }
}


