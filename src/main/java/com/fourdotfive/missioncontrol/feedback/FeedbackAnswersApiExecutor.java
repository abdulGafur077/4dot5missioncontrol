package com.fourdotfive.missioncontrol.feedback;

import com.fourdotfive.missioncontrol.dtos.company.JobDetailsRecruiterScreening;
import com.fourdotfive.missioncontrol.dtos.company.FeedbackAnswersDto;
import com.fourdotfive.missioncontrol.dtos.company.RecruitersScreeningDto;
import com.fourdotfive.missioncontrol.pojo.candidate.Contact;
import com.fourdotfive.missioncontrol.pojo.feedback.FeedbackExecutionResult;
import com.fourdotfive.missioncontrol.pojo.feedback.FileRetrieve;
import com.fourdotfive.missioncontrol.pojo.user.User;


import java.util.List;

public interface FeedbackAnswersApiExecutor {

    FeedbackAnswersDto getAllRecruiterQuestions(String meetingScheduleId, String jobMatchId, String userId, String companyId, String loggedInUserId);

    FeedbackAnswersDto save(FeedbackExecutionResult feedbackExecutionResult, String jobMatchId);

    void retrieveFile(FileRetrieve fileRetrieve);

    FeedbackAnswersDto saveJobMatchState(String statusOfSave, List<JobDetailsRecruiterScreening> jobDetailsRecruiterScreeningsList);

    List<User> getUserList(String jobMatchId, String userid);

    Contact getCandidateDetails(String jobMatchId);

    RecruitersScreeningDto checkRecruiterScreeningStatus(String jobMatchId, String userId, String companyId);
}
