package com.fourdotfive.missioncontrol.feedback;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.fourdotfive.missioncontrol.common.*;
import com.fourdotfive.missioncontrol.dtos.company.*;
import com.fourdotfive.missioncontrol.pojo.feedback.FeedbackAnswers;
import com.fourdotfive.missioncontrol.pojo.feedback.FeedbackExecutionResult;
import com.fourdotfive.missioncontrol.pojo.feedback.FileRetrieve;
import com.fourdotfive.missioncontrol.dtos.company.FeedbackAnswersDto;
import com.fourdotfive.missioncontrol.pojo.job.JobMatch;
import com.fourdotfive.missioncontrol.pojo.candidate.Contact;
import com.fourdotfive.missioncontrol.pojo.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/feedbackanswers")
public class FeedbackAnswersController {

    @Autowired
    private FeedbackAnswersService feedBackAnswersService;

    @Autowired
    private AccessUtil accessUtil;

    /**
     * To Get Recruiter Questions, this method is called.
     *
     * @return FeedBackAnswersDto
     */
    @PreAuthorize("@accessControlImp.canGetRecruiterScreeningQuestions(principal)")
    @RequestMapping(value = "/getrecruiterscreeningquestions/{jobMatchId}/{userId}", method = RequestMethod.GET)
    public ResponseEntity<FeedbackAnswersDto> getRecruiterScreeningQuestions(
            @PathVariable("jobMatchId") String jobMatchId, @PathVariable("userId") String userId,
            @RequestParam(value = "companyId") String companyId,
            @RequestParam(value = "meetingScheduleId", required = false) String meetingScheduleId) {
        FeedbackAnswersDto response = feedBackAnswersService.getAllRecruiterQuestions(meetingScheduleId, jobMatchId, userId, companyId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    /**
     * To save Recruiter Questions answers, this method is called.
     *
     * @return FeedBackAnswersDto
     */
    @PreAuthorize("@accessControlImp.canSaveRecruiterScreeningQuestions(principal)")
    @RequestMapping(value = "/saverecruiterscreeningquestions/{jobMatchId}", method = RequestMethod.POST, consumes =  MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FeedbackAnswersDto> save(
            @PathVariable("jobMatchId") String jobMatchId, @RequestBody FeedbackExecutionResult feedbackExecutionResult, HttpServletRequest request) {
        FeedbackAnswersDto response = feedBackAnswersService.save(feedbackExecutionResult,jobMatchId);
        return new ResponseEntity<FeedbackAnswersDto>(response, HttpStatus.OK);
    }


    /**
     * To save Recruiter Questions answers, this method is called.
     *
     * @return FeedBackAnswersDto
     */
    @PreAuthorize("@accessControlImp.canSaveJobMatchState(principal)")
    @RequestMapping(value = "/savejobmatchstate/{statusofsave}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FeedbackAnswersDto> savejobmatchstate(
            @PathVariable(value = "statusofsave") String statusofsave,
            @RequestBody JobDetailsRecruiterScreening[] jobDetailRecruiterScreeningList, HttpServletRequest request) {
        FeedbackAnswersDto response = feedBackAnswersService.saveJobMatchState(statusofsave, Arrays.asList(jobDetailRecruiterScreeningList));
        return new ResponseEntity<FeedbackAnswersDto>(response, HttpStatus.OK);
    }

    @PreAuthorize("@accessControlImp.canGetJobFile(principal)")
    @RequestMapping(value = "/getjobfile/{filelocation}/{filename}/{subdir}", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public void getjobfile(
            @PathVariable(value = "filelocation") String filelocation,
            @PathVariable(value = "filename") String filename,
            @PathVariable(value = "subdir") String subdir,
            @RequestBody String companyName, HttpServletRequest request
    ) {

        FileRetrieve fileRetrieve = new FileRetrieve();
        fileRetrieve.setFilelocation(filelocation);
        fileRetrieve.setFilename(filename + ".docx");
        fileRetrieve.setSubdir(subdir);
        feedBackAnswersService.retrieveFile(fileRetrieve);
    }


    @RequestMapping(value = "/getuserlist/{jobMatchId}/{userid}", method = RequestMethod.GET)
    public ResponseEntity<List<User>> getuserlist(
            @PathVariable("jobMatchId") String jobMatchId, @PathVariable("userid") String userid,
            HttpServletRequest request) {
        List<User> userList = feedBackAnswersService.getUserList(jobMatchId, userid);
        return new ResponseEntity<List<User>>(userList, HttpStatus.OK);
    }

    @RequestMapping(value = "/getcandidatedetails/{jobMatchId}", method = RequestMethod.GET)
    public ResponseEntity<Contact> getJobMatchDetails(
            @PathVariable("jobMatchId") String jobMatchId,
            HttpServletRequest request) {
        Contact contact = feedBackAnswersService.getCandidateDetails(jobMatchId);
        return new ResponseEntity<Contact>(contact, HttpStatus.OK);
    }

    @RequestMapping(value = "/checkrecruiterscreeningstatus/{jobMatchId}/{userId}", method = RequestMethod.GET)
    public ResponseEntity<RecruitersScreeningDto> checkRecruiterScreeningStatus(@PathVariable("jobMatchId") String jobMatchId,
                                                                                @PathVariable("userId") String userId,
                                                                                @RequestParam(value = "companyId") String companyId,
                                                                                HttpServletRequest request) {
        RecruitersScreeningDto response = feedBackAnswersService.checkRecruiterScreeningStatus(jobMatchId, userId, companyId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}