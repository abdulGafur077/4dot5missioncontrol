package com.fourdotfive.missioncontrol.feedback;

import com.fourdotfive.missioncontrol.dtos.company.JobDetailsRecruiterScreening;
import com.fourdotfive.missioncontrol.dtos.company.FeedbackAnswersDto;
import com.fourdotfive.missioncontrol.dtos.company.RecruitersScreeningDto;
import com.fourdotfive.missioncontrol.pojo.candidate.Contact;
import com.fourdotfive.missioncontrol.pojo.feedback.FeedbackExecutionResult;
import com.fourdotfive.missioncontrol.pojo.feedback.FileRetrieve;
import com.fourdotfive.missioncontrol.pojo.user.User;

import java.util.*;

public interface FeedbackAnswersService {

    /**
     * To get all the questions for a recruiter for a list of job matches.
     *
     * @return response from platform
     */
   FeedbackAnswersDto getAllRecruiterQuestions(String meetingScheduleId, String jobMatchId, String userId, String companyId);

    FeedbackAnswersDto save(FeedbackExecutionResult feedbackExecutionResult, String jobMatchId);

   void retrieveFile(FileRetrieve fileRetrieve);

    List<User> getUserList(String jobMatchId, String userid);

    FeedbackAnswersDto  saveJobMatchState(String statusOfSave, List<JobDetailsRecruiterScreening> jobDetailsRecruiterScreeningsList);

    Contact getCandidateDetails(String jobMatchId);

    RecruitersScreeningDto checkRecruiterScreeningStatus(String jobMatchId , String userId, String companyId);
}
