package com.fourdotfive.missioncontrol.feedback;


import com.fourdotfive.missioncontrol.dtos.company.FeedbackAnswersDto;
import com.fourdotfive.missioncontrol.dtos.company.JobDetailsRecruiterScreening;
import com.fourdotfive.missioncontrol.dtos.company.RecruitersScreeningDto;
import com.fourdotfive.missioncontrol.pojo.candidate.Contact;
import com.fourdotfive.missioncontrol.pojo.feedback.FeedbackAnswers;
import com.fourdotfive.missioncontrol.pojo.feedback.FeedbackExecutionResult;
import com.fourdotfive.missioncontrol.pojo.feedback.FileRetrieve;
import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.user.UserService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FeedbackAnswersServiceImp implements FeedbackAnswersService {


    private static final Logger LOGGER = LoggerFactory
            .getLogger(FeedbackAnswersServiceImp.class);

    @Autowired
    private FeedbackAnswersApiExecutor apiExecutor;

    @Autowired
    private UserService userService;

    @Override
    public FeedbackAnswersDto getAllRecruiterQuestions(String meetingScheduleId, String jobMatchId, String userId, String companyId) {
        LOGGER.info("Setting Recruiter questionList");
        List<FeedbackAnswers> clientResults = new ArrayList();
        List<FeedbackAnswers> companyResults = new ArrayList();
        User loggedInUserId = userService.getCurrentLoggedInUser();
        FeedbackAnswersDto returnedAllQuestions = apiExecutor.getAllRecruiterQuestions(meetingScheduleId, jobMatchId, userId, companyId, loggedInUserId.getId());

       /* Collection<List<FeedbackAnswers>> l = returnedAllQuestions.getCompanyQuestionsResults().values();
        for (Iterator iterator = l.iterator(); iterator.hasNext();) {
            ArrayList recQnsList = (ArrayList) iterator.next();
            for(int i=0; i<recQnsList.size(); i++)
            {
                FeedbackAnswers recQns = (FeedbackAnswers)recQnsList.get(i);
                recQns.getCompany().setCreatedDate(null);
                recQns.getCompany().setModifiedDateTime(null);
                recQns.getCompany().setCreatedDateTime(null);
                recQns.getCompany().setLastModifiedDate(null);
            }
        }

        Collection<List<FeedbackAnswers>> m = returnedAllQuestions.getClientQuestionsResults().values();
        for (Iterator iterator = m.iterator(); iterator.hasNext();) {
            ArrayList recQnsList = (ArrayList) iterator.next();
            for(int i=0; i<recQnsList.size(); i++)
            {
                FeedbackAnswers recQns = (FeedbackAnswers)recQnsList.get(i);
                recQns.getCompany().setCreatedDate(null);
                recQns.getCompany().setModifiedDateTime(null);
                recQns.getCompany().setCreatedDateTime(null);
                recQns.getCompany().setLastModifiedDate(null);

                recQns.getClientorbu().setCreatedDate(null);
                recQns.getClientorbu().setModifiedDateTime(null);
                recQns.getClientorbu().setCreatedDateTime(null);
                recQns.getClientorbu().setLastModifiedDate(null);
            }*/

        return returnedAllQuestions;

    }


    @Override
    public FeedbackAnswersDto save(FeedbackExecutionResult feedbackExecutionResult, String jobMatchId) {
        LOGGER.info("saving Recruiter question");
        User user = userService.getCurrentLoggedInUser();
        feedbackExecutionResult.setLoggedInUser(user.getId());
        for (FeedbackAnswers feedbackAnswer : feedbackExecutionResult.getFeedbackAnswersList()) {
            if (StringUtils.isEmpty(feedbackAnswer.getUserId())) {
                feedbackAnswer.setUserId(user.getId());
            }
            if (StringUtils.isEmpty(feedbackAnswer.getParticipantId())) {
                feedbackAnswer.setParticipantId(user.getId());
            }
        }
        return apiExecutor.save(feedbackExecutionResult, jobMatchId);
    }

    @Override
    public void retrieveFile(FileRetrieve fileRetrieve) {
        LOGGER.info("getting file content");
        apiExecutor.retrieveFile(fileRetrieve);
        return;
    }

    @Override
    public FeedbackAnswersDto saveJobMatchState(String statusOfSave, List<JobDetailsRecruiterScreening> jobDetailsRecruiterScreeningList) {
        LOGGER.info("saving Recruiter question");

        FeedbackAnswersDto response = apiExecutor.saveJobMatchState(statusOfSave, jobDetailsRecruiterScreeningList);
        return response;

    }


    public List<User> getUserList(String jobMatchId, String userId) {
        LOGGER.info("getting user  list");

        List<User> userList = apiExecutor.getUserList(jobMatchId, userId);
        return userList;


    }

    public Contact getCandidateDetails(String jobMatchId) {
        Contact contact = apiExecutor.getCandidateDetails(jobMatchId);
        return contact;
    }

    @Override
    public RecruitersScreeningDto checkRecruiterScreeningStatus(String jobMatchId, String userId, String companyId) {
        RecruitersScreeningDto recruitersScreeningDto = apiExecutor.checkRecruiterScreeningStatus(jobMatchId, userId, companyId);
        return recruitersScreeningDto;
    }
}


