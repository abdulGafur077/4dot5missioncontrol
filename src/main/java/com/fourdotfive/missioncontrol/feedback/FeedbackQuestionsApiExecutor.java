package com.fourdotfive.missioncontrol.feedback;


import com.fourdotfive.missioncontrol.pojo.feedback.FeedbackQuestions;

import java.util.List;

public interface FeedbackQuestionsApiExecutor {

    String saveFeedBack(FeedbackQuestions feedback);

    FeedbackQuestions getFeedBack(String companyId, String questionType);

}
