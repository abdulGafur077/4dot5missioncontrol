package com.fourdotfive.missioncontrol.feedback;



import com.fourdotfive.missioncontrol.common.Message;
import com.fourdotfive.missioncontrol.common.Messages;
import com.fourdotfive.missioncontrol.pojo.feedback.FeedbackQuestions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@RestController
@RequestMapping("/api/feedback")
public class FeedbackQuestionsController {

    @Autowired
    private FeedbackQuestionsService feedbackService;

    @RequestMapping(value = "/savefeedBack/{companyid}/{questionType}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
     public ResponseEntity<Message> saveFeedBack(
             @PathVariable(value = "companyid") String parentCompanyId,
             @PathVariable(value = "questionType") String questionType,
             @RequestBody List<String> questionList, HttpServletRequest request) {

         String response = feedbackService.saveFeedBack(parentCompanyId,questionList,questionType);
        Message msg = Message.statusCode(HttpStatus.OK)
                .message(Messages.CHANGE_PWD_SUCCESS).build();

        return new ResponseEntity<Message>(msg, HttpStatus.OK);

     }

   @RequestMapping(value = "/getfeedback/{companyid}/{questionType}", method = RequestMethod.GET)
    public ResponseEntity<FeedbackQuestions> saveQuestionList(
            @PathVariable(value = "companyid") String companyid,
            @PathVariable(value = "questionType") String questionType, HttpServletRequest request) {

       FeedbackQuestions feedback = feedbackService.getFeedBack(companyid,questionType);

        return new ResponseEntity<FeedbackQuestions>(feedback, HttpStatus.OK);

    }
}
