package com.fourdotfive.missioncontrol.feedback;


import com.fourdotfive.missioncontrol.pojo.feedback.FeedbackQuestions;

import java.util.List;

public interface FeedbackQuestionsService {

String saveFeedBack(String parentCompanyId, List<String> questionList, String questionType);

    FeedbackQuestions getFeedBack(String companyId, String questionType);

}
