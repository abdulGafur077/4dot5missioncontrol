package com.fourdotfive.missioncontrol.feedback;

import com.fourdotfive.missioncontrol.company.CompanyApiExecutor;

import com.fourdotfive.missioncontrol.pojo.feedback.FeedbackQuestions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FeedbackQuestionsServiceImp implements FeedbackQuestionsService {

    @Autowired
    private FeedbackQuestionsApiExecutor feedBackApiExecutor;

    @Autowired
    private CompanyApiExecutor companyApiExecutor;


 public String saveFeedBack(String companyId, List<String> questionList, String questionType) {

     FeedbackQuestions feedback = new FeedbackQuestions();
     feedback.setCompanyId(companyId);
     feedback.setQuestionList(questionList);
     feedback.setQuestionType(questionType);
     String success = feedBackApiExecutor.saveFeedBack(feedback);


     return success;
 }

    public FeedbackQuestions getFeedBack(String companyId,String questionType) {
        FeedbackQuestions retreivedFeedback = feedBackApiExecutor.getFeedBack(companyId,questionType);
       return retreivedFeedback;
    }

}


