package com.fourdotfive.missioncontrol.forwardrecipient;

import java.util.List;

import com.fourdotfive.missioncontrol.pojo.user.User;

public interface FetchPossibleForwardRecipient {
	
	List<User> getPossibleForwardedRecipient(String userId);

}
