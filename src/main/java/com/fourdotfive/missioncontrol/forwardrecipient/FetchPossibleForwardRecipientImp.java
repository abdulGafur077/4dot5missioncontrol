package com.fourdotfive.missioncontrol.forwardrecipient;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fourdotfive.missioncontrol.common.AccessUtil;
import com.fourdotfive.missioncontrol.common.AppConstants;
import com.fourdotfive.missioncontrol.common.CommonUtil;
import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.user.UserService;

@Service
public class FetchPossibleForwardRecipientImp implements
		FetchPossibleForwardRecipient {

	@Autowired
	private UserService userService;

	@Autowired
	private AccessUtil accessUtil;

	@Override
	public List<User> getPossibleForwardedRecipient(String userId) {
		User manager = userService.getManager(userId);
		List<User> forwardRecipientList = new ArrayList<User>();
		if (manager == null) {
			forwardRecipientList = fetchForCompanyAdmin(userId);
		} else {
			forwardRecipientList = fetchForOtherUser(manager, userId);
		}
		return forwardRecipientList;
	}

	private List<User> fetchForOtherUser(User manager, String userId) {
		List<User> forwardRecipientList = new ArrayList<User>();
		List<User> myTeamMembers = userService.getMyPeers(manager, userId);
		List<User> myReportees = userService.getReporteeList(userId);
		myTeamMembers.addAll(myReportees);
		myTeamMembers = CommonUtil.getUnarchivedTeamMembers(myTeamMembers);
		for (User temp : myTeamMembers) {
			if (!temp.getAccessControlList().getAccountState()
					.equals(AppConstants.UN_VERIFIED)) {
				forwardRecipientList.add(temp);
			}
		}
		if (!manager.getAccessControlList().getAccountState()
				.equals(AppConstants.UN_VERIFIED)) {
			forwardRecipientList.add(manager);
		}
		return forwardRecipientList;
	}

	private List<User> fetchForCompanyAdmin(String userId) {
		List<User> forwardRecipientList = new ArrayList<User>();
		User user = userService.getUserById(userId);
		if (accessUtil.isRoleAtCompanyAdminLevel(user.getRole().getId())) {
			List<User> admins = userService
					.getCompanyAdmins(user.getEmployer());
			admins = CommonUtil.getUnarchivedTeamMembers(admins);
			if (admins != null && !admins.isEmpty()) {
				for (User temp : admins) {
					if (!temp.getId().equals(userId)
							&& !temp.getAccessControlList().getAccountState()
									.equals(AppConstants.UN_VERIFIED)) {
						forwardRecipientList.add(temp);
					}
				}
			}

			if (accessUtil.isCompanyAdmin(user.getRole())) {
				List<User> myReportees = userService.getReporteeList(userId);
				myReportees = CommonUtil.getUnarchivedTeamMembers(myReportees);
				if (myReportees != null && !myReportees.isEmpty()) {
					for (User temp : myReportees) {
						if (!temp.getAccessControlList().getAccountState()
								.equals(AppConstants.UN_VERIFIED)) {
							forwardRecipientList.add(temp);
						}
					}
				}
			}
		}

		return forwardRecipientList;
	}


}
