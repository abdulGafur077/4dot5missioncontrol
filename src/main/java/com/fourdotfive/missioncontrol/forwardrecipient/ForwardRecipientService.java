package com.fourdotfive.missioncontrol.forwardrecipient;

import java.util.List;

import com.fourdotfive.missioncontrol.common.TokenDetails;
import com.fourdotfive.missioncontrol.dtos.user.TeamMemberDto;
import com.fourdotfive.missioncontrol.dtos.user.UserDto;
import com.fourdotfive.missioncontrol.pojo.user.User;

public interface ForwardRecipientService {

	User setForwardRecipient(User user, UserDto userDto);

	List<TeamMemberDto> getMyForwardRecipients(String userId);

	void checkUserIsForwardRecipient(User user);


	void addForwardFromRecipient(String fromDate, String toDate,
			String fromUserId, String toUserId);

	void removeForwardFromRecipient(String fromUserId, String toUserId);

	void checkForwardRecipientsRecipient(User updatingUser,
			TeamMemberDto forwardRecipient);

	void removeBackwardPointer(User user);
	
	User updateForwardRecipient(User user, UserDto userDto,User forwardRecipient);
	
	UserDto saveForwardRecipient(UserDto userDto,
			TokenDetails tokenDetails, String roleId, String companyId);
	
	
}
