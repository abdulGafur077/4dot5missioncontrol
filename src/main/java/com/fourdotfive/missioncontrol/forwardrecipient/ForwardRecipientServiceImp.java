package com.fourdotfive.missioncontrol.forwardrecipient;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fourdotfive.missioncontrol.common.AccessUtil;
import com.fourdotfive.missioncontrol.common.AppConstants;
import com.fourdotfive.missioncontrol.common.CommonUtil;
import com.fourdotfive.missioncontrol.common.Messages;
import com.fourdotfive.missioncontrol.common.TokenDetails;
import com.fourdotfive.missioncontrol.dtos.company.AuditDto;
import com.fourdotfive.missioncontrol.dtos.user.TeamMemberDto;
import com.fourdotfive.missioncontrol.dtos.user.UserDto;
import com.fourdotfive.missioncontrol.dtos.user.UserForwardRecipientDetailsDto;
import com.fourdotfive.missioncontrol.exception.AddFRPlaformException;
import com.fourdotfive.missioncontrol.exception.ForwardRecipientException;
import com.fourdotfive.missioncontrol.exception.RemoveFRPlatformException;
import com.fourdotfive.missioncontrol.exception.SaveUserPlatformException;
import com.fourdotfive.missioncontrol.exception.UserConflictException;
import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.pojo.user.UserForwardedReference;
import com.fourdotfive.missioncontrol.pojo.user.UserRecipient;
import com.fourdotfive.missioncontrol.user.UserApiExecutor;
import com.fourdotfive.missioncontrol.user.UserService;

@Service
public class ForwardRecipientServiceImp implements ForwardRecipientService {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ForwardRecipientServiceImp.class);

	@Autowired
	private UserService userService;

	@Autowired
	private AccessUtil accessUtil;

	@Autowired
	private UserApiExecutor apiUserExecutor;

	@Autowired
	private FetchPossibleForwardRecipient fetchPossibleForwardRecipient;

	@Override
	public User setForwardRecipient(User user, UserDto userDto) {
		// set forward recipient
		UserForwardRecipientDetailsDto forwardDto = userDto
				.getUserForwardRecipientDetails();
		if (forwardDto != null && forwardDto.getForwardRecipient() != null) {
			User forwardRecipientUser = userService.getUserById(forwardDto
					.getForwardRecipient().getUserId());
			forwardRecipientUser.setEmployer(null);
			forwardRecipientUser.setAccessControlList(null);
			user.setRecipientUser(forwardRecipientUser);
			user.setForwardFromStartDate(userDto
					.getUserForwardRecipientDetails().getFromDate());
			user.setForwardToEndDate(userDto.getUserForwardRecipientDetails()
					.getToDate());
		} else {
			user.setForwardFromStartDate(null);
			user.setForwardToEndDate(null);
			user.setRecipientUser(null);
		}
		return user;
	}

	@Override
	public void checkForwardRecipientsRecipient(User updatingUser,
			TeamMemberDto forwardRecipient) {
		// get forward recipient
		User forwardRecipientsRecipient = apiUserExecutor
				.getRecipientUser(forwardRecipient.getUserId());
		User forwardRecipientDate = apiUserExecutor
				.getUserById(forwardRecipient.getUserId());
		if (forwardRecipientsRecipient != null && forwardRecipientDate != null) {

			// get date for forward recipient user
			CommonUtil.isForwardRecipientinDate(updatingUser,
					forwardRecipientDate, forwardRecipientsRecipient);
		}

	}

	@Override
	public List<TeamMemberDto> getMyForwardRecipients(String userId) {
		List<TeamMemberDto> forwardedRecipients = TeamMemberDto
				.mapUsersToTeamMembers(fetchPossibleForwardRecipient
						.getPossibleForwardedRecipient(userId));
		return forwardedRecipients;

	}

	@Override
	public void checkUserIsForwardRecipient(User user) {

		if (accessUtil.isFourDot5Admin(user.getRole().getId())) {
			return;
		} else {
			List<String> namesList = new ArrayList<String>();
			List<UserForwardedReference> list = apiUserExecutor
					.getForwardedFromRecipients(user.getId());
			for (Iterator<UserForwardedReference> iterator = list.iterator(); iterator
					.hasNext();) {
				UserForwardedReference obj = iterator.next();
				String name = null;
				if (obj.getUser().getAccessControlList().getAccountState()
						.equals(AppConstants.ARCHIVED)) {
					// Remove the current element from the iterator and the
					// list.
					iterator.remove();
				} else {
					if (obj.getUser().getLastname() != null) {
						name = new StringBuilder().append(" '<b><i><a>")
								.append(obj.getUser().getFirstname())
								.append(" ")
								.append(obj.getUser().getLastname())
								.append("</a></i></b>'").toString();
					} else {
						name = new StringBuilder().append(" '<b><i><a>")
								.append(obj.getUser().getFirstname())
								.append("</a></i></b>'").toString();
					}
					namesList.add(name);
				}

			}
			StringBuilder messageBuilder = new StringBuilder();
			String userName = user.getFirstname();
			if (user.getLastname() != null) {
				userName = new StringBuilder().append(userName).append(" ")
						.append(user.getLastname()).toString();
			}
			if (namesList.size() == 1) {
				messageBuilder
						.append(Messages.DELETING_USER_HAS_FORWARD_RICIPIENT)
						.append(namesList.get(0)).append(".");
			} else if (namesList.size() == 2) {
				messageBuilder
						.append(Messages.DELETING_USER_HAS_FORWARD_RICIPIENT)
						.append(namesList.get(0)).append(" and ")
						.append(namesList.get(1)).append(".");
			} else if (namesList.size() > 2) {
				messageBuilder
						.append(Messages.DELETING_USER_HAS_FORWARD_RICIPIENT);
				for (int i = 0; i < namesList.size() - 1; i++) {
					if (i == namesList.size() - 2) {
						messageBuilder.append(namesList.get(i));
					} else {
						messageBuilder.append(namesList.get(i)).append(", ");
					}
				}
				messageBuilder.append(" and ")
						.append(namesList.get(namesList.size() - 1))
						.append(".");
			}
			if (namesList.size() > 0) {
				throw new UserConflictException(messageBuilder.toString(),
						userName);
			}
		}
	}

	@Override
	public void addForwardFromRecipient(String fromDate, String toDate,
			String fromUserId, String toUserId) {

		String from = CommonUtil.formatForwardedDate(fromDate);
		String to = CommonUtil.formatForwardedDate(toDate);

		UserRecipient userRecipient = new UserRecipient(from, to, fromUserId,
				toUserId);
		apiUserExecutor.addForwardedFromRecipient(userRecipient);

	}

	@Override
	public void removeForwardFromRecipient(String fromUserId, String toUserId) {
		UserRecipient userRecipient = new UserRecipient(fromUserId, toUserId);
		apiUserExecutor.removeForwardedFromRecipient(userRecipient);

	}

	@Override
	public User updateForwardRecipient(User user, UserDto userDto,
			User forwardRecipient) {
		User oldUserDetails = user;
		user = setForwardRecipient(user, userDto);
		UserForwardRecipientDetailsDto forwardDto = userDto
				.getUserForwardRecipientDetails();
		if (forwardDto != null && forwardDto.getForwardRecipient() != null) {
			checkForwardRecipientsRecipient(user,
					forwardDto.getForwardRecipient());
		}

		// remove forward recipient
		removeForwardRecipient(userDto, forwardRecipient, user);
		addForwardRecipient(userDto, forwardRecipient, oldUserDetails);
		return user;
	}

	private void removeForwardRecipient(UserDto userDto, User forwardRecipient,
			User user) {

		if (userDto.getUserForwardRecipientDetails().getForwardRecipient() == null
				&& forwardRecipient != null) {
			try {
				canRemoveForwardRecipient(user, forwardRecipient);
			} catch (RemoveFRPlatformException e) {
				String message = CommonUtil.getUserName(userDto);
				throw new ForwardRecipientException(
						Messages.EXCEPTION_WHILE_REMOVING_FORWARD_RECIPIENT,
						message);
			}

		} else if (userDto.getUserForwardRecipientDetails()
				.getForwardRecipient() != null
				&& forwardRecipient != null
				&& !userDto.getUserForwardRecipientDetails()
						.getForwardRecipient().getUserId()
						.equals(forwardRecipient.getId())) {
			try {
				// remove old forward recipient
				canRemoveForwardRecipient(user, forwardRecipient);
			} catch (RemoveFRPlatformException e) {
				String message = CommonUtil.getUserName(userDto);
				throw new ForwardRecipientException(
						Messages.EXCEPTION_WHILE_CREATING_FORWARD_RECIPIENT,
						message);
			}

		}
	}

	private void addForwardRecipient(UserDto userDto, User oldforwardRecipient,
			User oldUserDetails) {
		try {
			if (userDto.getUserForwardRecipientDetails() != null
					&& userDto.getUserForwardRecipientDetails()
							.getForwardRecipient() != null) {
				createForwardRecipient(userDto);
			}
		} catch (AddFRPlaformException e) {

			if (oldforwardRecipient != null) {
				rollBackforAddForwardRecipientFailed(userDto,
						oldforwardRecipient, oldUserDetails);

			} else {
				String message = CommonUtil.getUserName(userDto);
				throw new ForwardRecipientException(
						Messages.EXCEPTION_WHILE_CREATING_FORWARD_RECIPIENT,
						message);
			}

		}

	}

	private void rollBackforAddForwardRecipientFailed(UserDto userDto,
			User oldforwardRecipient, User oldUserDetails) {
		String message = CommonUtil.getUserName(userDto);
		try {
			addForwardFromRecipient(oldUserDetails.getForwardFromStartDate(),
					oldUserDetails.getForwardToEndDate(),
					oldUserDetails.getId(), oldforwardRecipient.getId());
			throw new ForwardRecipientException(
					Messages.EXCEPTION_WHILE_CREATING_FORWARD_RECIPIENT,
					message);
		} catch (AddFRPlaformException e) {

			throw new ForwardRecipientException(
					Messages.EXCEPTION_WHILE_ROLLING_BACK_FORWARD_RECIPIENT,
					message);
		}

	}

	private void canRemoveForwardRecipient(User oldUserDetails,
			User forwardRecipient) {
		removeForwardFromRecipient(oldUserDetails.getId(),
				forwardRecipient.getId());
	}

	private void createForwardRecipient(UserDto userDto) {

		// create obj to save forward recipient
		String fromDate = userDto.getUserForwardRecipientDetails()
				.getFromDate();
		String toDate = userDto.getUserForwardRecipientDetails().getToDate();
		String fromUserId = userDto.getUserDetails().getUserId();
		String toUserId = userDto.getUserForwardRecipientDetails()
				.getForwardRecipient().getUserId();
		if (fromDate != null && toDate != null && fromUserId != null
				&& toUserId != null) {
			addForwardFromRecipient(fromDate, toDate, fromUserId, toUserId);
		}

	}

	private void rollBackforSaveUserForForwardRecipientFailed(UserDto userDto,
			User oldforwardRecipient, User oldUserDetails) {
		String message = CommonUtil.getUserName(userDto);

		try {
			removeForwardFromRecipient(userDto.getUserDetails().getUserId(),
					userDto.getUserForwardRecipientDetails()
							.getForwardRecipient().getUserId());
			// add new forward recipient
			if (oldforwardRecipient != null) {
				addForwardFromRecipient(
						oldUserDetails.getForwardFromStartDate(),
						oldUserDetails.getForwardToEndDate(),
						oldUserDetails.getId(), oldforwardRecipient.getId());
			}
			throw new ForwardRecipientException(
					Messages.EXCEPTION_WHILE_CREATING_FORWARD_RECIPIENT, message);
		} catch (RemoveFRPlatformException e) {
			throw new ForwardRecipientException(
					Messages.EXCEPTION_WHILE_ROLLING_BACK_FORWARD_RECIPIENT,
					message);
		} catch (AddFRPlaformException e) {
			throw new ForwardRecipientException(
					Messages.EXCEPTION_WHILE_ROLLING_BACK_FORWARD_RECIPIENT,
					message);
		}

	}

	@Override
	public UserDto saveForwardRecipient(UserDto userDto,
			TokenDetails tokenDetails, String roleId, String companyId) {
		// Setting audit details
		userDto = AuditDto.setAuditDetails(userDto, tokenDetails);
		User user = UserDto.mapUserDtoToUser(userDto);
		User oldUserDetails = user;
		User forwardRecipient = apiUserExecutor.getRecipientUser(user.getId());
		// Setting forward recipient details
		if (userDto.getUserDetails().getUserId() != null) {
			user = updateForwardRecipient(user, userDto, forwardRecipient);
		}
		// forward pointing
		try {
			user = apiUserExecutor.saveUser(user, roleId, companyId);
		} catch (SaveUserPlatformException e) {

			rollBackforSaveUserForForwardRecipientFailed(userDto,
					forwardRecipient, oldUserDetails);
		}

		UserDto dto = userService.getUserDto(user);
		if (userDto.getUserScopeDetails() != null) {
			dto.setUserScopeDetails(userDto.getUserScopeDetails());
		}
		if (accessUtil.canRoleHaveReportees(user.getRole().getId())) {
			int count = userService.getReporteesCount(dto.getUserDetails()
					.getUserId());
			dto.setReporteeCount(count);
		}
		return dto;
	}

	public void removeBackwardPointer(User user) {
		User recipientUser = userService.getRecipientUser(user.getId());
		if (recipientUser != null) {
			List<UserForwardedReference> list = apiUserExecutor
					.getForwardedFromRecipients(recipientUser.getId());

			for (UserForwardedReference userForwardedReference : list) {
				if (userForwardedReference.getUser().getId()
						.equals(user.getId())) {
					removeForwardFromRecipient(user.getId(),
							recipientUser.getId());
				}
			}
		}
	}
}
