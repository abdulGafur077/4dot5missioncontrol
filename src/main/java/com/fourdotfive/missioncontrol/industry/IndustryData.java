package com.fourdotfive.missioncontrol.industry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fourdotfive.missioncontrol.dtos.company.MasterIndustryDto;
import com.fourdotfive.missioncontrol.dtos.company.SubIndustryDto;
import com.fourdotfive.missioncontrol.pojo.company.Industry;

@Service
public class IndustryData {

	// Master Data
	private static final String BASIC_MATERIALS = "Basic Materials";
	private static final String CONGLOMERATES = "Conglomerates";
	private static final String CONSUMER_GOODS = "Consumer Goods";
	private static final String FINANCIAL = "Financial";
	private static final String HEALTH_CARE = "Health Care";
	private static final String INDUSTRIAL_GOODS = "Industrial Goods";
	private static final String SERVICES = "Services";
	private static final String TECHNOLOGY = "Technology";
	private static final String UTILITIES = "utilities";

	// Basic material
	private static final String AGRICULTURAL_CHEMICALS = "Agricultural Chemicals";
	private static final String ALUMINIUM = "Aluminum";
	private static final String INDEPENDENT_OIL_AND_GAS = "Independent oil and gas";
	private static final String INDUSTRIAL_METALS_AND_MINERALS = "Industrial Metals & Minerals";

	private static final String NONMETALLIC_MINERAL_MINING = "Nonmetallic Mineral Mining";
	private static final String OIL_AND_GAS_DRILLING_AND_EXPLORATION = "Oil & Gas Drilling & Exploration";
	private static final String OIL_AND_GAS_EQUIPMENT_AND_SERVICES = "Oil & Gas Equipment & Services";
	private static final String OIL_AND_GAS_PIPELINES = "Oil & Gas Pipelines";
	private static final String OIL_AND_GAS_REFINING_AND_MARKETING = "Oil & Gas Refining & Marketing";
	private static final String SILVER = "Silver";
	private static final String SPECIALTY_CHEMICALS = "Specialty Chemicals";
	private static final String STEEL_AND_IRON = "Steel & iron";
	private static final String SYNTHETICS = "Synthetics";

	// Consumer goods
	private static final String APPLIANCES = "Appliances";

	private static final String BEVERAGES_BREWERS = "Beverages - Brewers";
	private static final String BEVERAGES_SOFTDRINKS = "Beverages - Soft Drinks";
	private static final String BEVERAGES_WINERIES_DISTILLERS = "Beverages - Wineries & Distillers";
	private static final String BUSINESS_EQUIPMENT = "Business Equipment";
	private static final String CIGARETTES = "Cigarettes";
	private static final String CLEANING_PRODUCTS = "Cleaning Products";
	private static final String CONFECTIONERS = "Confectioners";
	private static final String DAIRY_PRODUCTS = "Dairy Products";
	private static final String ELECTRONIC_EQUIPMENT = "Electronic Equipment";
	private static final String FARM_PRODUCTS = "Farm Products";
	private static final String FOOD = "Food";
	private static final String HOUSEWARE_AND_ACCESSORIES = "Housewares & Accessories";
	private static final String MEAT_PRODUCTS = "Meat Products";
	private static final String OFFICE_SUPPLIES = "Office Supplies";
	private static final String PACKAGING_AND_CONTAINERS = "Packaging & Containers";
	private static final String PAPER_AND_PAPER_PRODUCTS = "Paper & Paper Products";
	private static final String PERSONAL_PRODUCTS = "Personal Products";
	private static final String PHOTOGRAPHIC_EQUIPMENT_AND_SUPPLIES = "Photographic Equipment & Supplies";
	private static final String PROCESSED_AND_PACKAGED_GOODS = "Processed & Packaged Goods";
	private static final String RECREATIONAL_GOODS = "Recreational Goods";
	private static final String RECREATIONAL_VEHICLES = "Recreational Vehicles";
	private static final String RUBBER_AND_PLASTICS = "Rubber & Plastics";
	private static final String SPORTING_GOODS = "Sporting Goods";
	private static final String TEXTILE_APPAREL_CLOTHING = "Textile - Apparel Clothing";
	private static final String TEXTILE_APPAREL_FOOTWARE_AND_ACCESSORIES = "Textile - Apparel Footwear & Accessories";
	private static final String TOBACCO_PRODUCTS = "Tobacco Products";
	private static final String TOYS_AND_GAMES = "Toys & Games";
	private static final String TRUCKS_AND_VEHICLES = "Trucks & Other Vehicles";

	// Finance
	private static final String ACCIDENT_AND_HEALTH_INSURANCE = "Accident & Health Insurance";
	private static final String ASSET_MANAGEMENT = "Asset Management";
	private static final String CLOSED_END_FUND_DEBT = "Closed-End Fund - Debt";
	private static final String CLOSED_END_FUND_FOREIGN = "Closed-End Fund - Foreign";
	private static final String CREDIT_SERVICES = "Credit Services";
	private static final String DIVERSIFIED_INVESTMENTS = "Diversified Investments";
	private static final String FOREIGN_MONEY_CENTER_BANKS = "Foreign Money Center Banks";
	private static final String INSURANCE_BROKERS = "Insurance Brokers";
	private static final String INVESTMENT_BROKERAGE_NATIONAL = "Investment Brokerage - National";
	private static final String INVESTMENT_BROKERAGE_REGIONAL = "Investment Brokerage - Regional";
	private static final String LIFE_INSURANCE = "Life Insurance";
	private static final String MONEY_CENTER_BANKS = "Money Center Banks";
	private static final String MORTGAGE_INVESTMENT = "Mortgage Investment";
	private static final String PROPERTY_AND_CASUALITY_INSURANCE = "Property & Casualty Insurance";
	private static final String PROPERTY_MANAGEMENT = "Property Management";
	private static final String REIT_DIVERSIFIED = "REIT - Diversified";
	private static final String REIT_HEALTHCARE_FACILITIES = "REIT - Healthcare Facilities";
	private static final String REIT_HOTEL_MOTEL = "REIT - Hotel/Motel";
	private static final String REIT_INDUSTRIAL = "REIT - Industrial";
	private static final String REIT_OFFICE = "REIT - Office";
	private static final String REIT_RESIDENTIAL = "REIT - Residential";
	private static final String REIT_RETAIL = "REIT - Retail";
	private static final String REAL_ESTATE_DEVELOPEMENT = "Real Estate Development";
	private static final String REGIONAL_MIDATLANTIC_BANK = "Regional - Mid-Atlantic Banks";
	private static final String REGIONAL_MIDWEST_BANKS = "Regional - Midwest Banks";
	private static final String REGIONAL_NORTH_EAST_BANKS = "Regional - Northeast Banks";
	private static final String REGIONAL_PACIFIC_BANKS = "Regional - Pacific Banks";
	private static final String REGIONAL_SOUTH_EAST_BANKS = "Regional - Southeast Banks";
	private static final String REGIONAL_SOUTH_WEST_BANKS = "Regional - Southwest Banks";
	private static final String SAVINGS_AND_LOANS = "Savings & Loans";
	private static final String SURETY_ANDTITLE_INSURANCE = "Surety & Title Insurance";

	// Health Care
	private static final String BIOTECHNOLOGY = "Biotechnology";
	private static final String DIAGNOSTIC_SUBSTANCES = "Diagnostic Substances";
	private static final String DRUG_DELIVERY = "Drug Delivery";
	private static final String DRUG_MANUFACTURERS_MAJOR = "Drug Manufacturers - Major";
	private static final String DRUG_MANUFACTURERS_OTHER = "Drug Manufacturers - Other";
	private static final String DRUG_RELATED_PRODUCTS = "Drug Related Products";
	private static final String DRUG_GENERIC = "Drugs - Generic";
	private static final String HEALTH_CARE_PLANS = "Health Care Plans";
	private static final String HOME_HEALTH_CARE = "Home Health Care";
	private static final String HOSPITALS = "Hospitals";
	private static final String LONG_TERM_CARE_FACILITIES = "Long-Term Care Facilities";
	private static final String MEDICAL_APPLIANCES_AND_EQUIPMENT = "Medical Appliances & Equipment";
	private static final String MEDICAL_INSTRUMENTS_AND_SUPPLIES = "Medical Instruments & Supplies";
	private static final String MEDICAL_LABORATORIES_AND_RESEARCH = "Medical Laboratories & Research";
	private static final String MEDICAL_PRACTITIONERS = "Medical Practitioners";
	private static final String SPECIALIZED_HEALTH_SERVICES = "Specialized Health Services";

	// Industrial goods
	private static final String AEROSPACE_DEFENSE_MAJOR = "Aerospace/Defense - Major Diversified";
	private static final String AEROSPACE_DEFENSE_PRODUCTS = "Aerospace/Defense Products & Services";
	private static final String CEMENT = "Cement";
	private static final String DIVERSIFIED_MACHINERY = "Diversified Machinery";
	private static final String FARM_AND_CONSTRUCTUON_MACHINERY = "Farm & Construction Machinery";
	private static final String GENERAL_BUILDING_MATERIAL = "General Building Materials";
	private static final String GENERAL_CONTRACTORS = "General Contractors";
	private static final String HEAVY_CONSTRUCTION = "Heavy Construction";
	private static final String INDUSTRIAL_ELECTRICAL_EQUIPMENT = "Industrial Electrical Equipment";
	private static final String INDUSTRIAL_EQUIPMENT_AND_COMPONENTS = "Industrial Equipment & Components";
	private static final String LUMBER_WOOD = "Lumber, Wood Production";
	private static final String MACHINE_TOOLS = "Machine Tools & Accessories";
	private static final String MANUFACTURED_HOUSING = "Manufactured Housing";
	private static final String METAL_FABRICATION = "Metal Fabrication";
	private static final String POLLUTION_AND_TREATMENT_CONTROLS = "Pollution & Treatment Controls";
	private static final String RESIDENTIAL_CONTRUCTION = "Residential Construction";
	private static final String TEXTILE_INDUSTRIAL = "Textile Industrial";
	private static final String WASTE_MANEGEMENT = "Waste Management";

	// Services
	private static final String AIRDELIVERY_AND_FREIGHT_SERVICES = "Air Delivery & Freight Services";
	private static final String AIR_SERVICES = "Air Services";
	private static final String APPAREL_STORES = "Apparel Stores";
	private static final String AUTO_DEALERSHIP = "Auto Dealerships";
	private static final String AUTO_PARTS_STORES = "Auto Parts Stores";
	private static final String AUTO_PARTS_WHOLESALE = "Auto Parts Wholesale";
	private static final String BASIC_MATERIALS_WHOLESALE = "Basic Materials Wholesale";
	private static final String BROADCASTING_RADIO = "Broadcasting - Radio";
	private static final String BROADCASTING_TV = "Broadcasting - TV";
	private static final String BUILDING_MATERIALS_WHOLESALE = "Building Materials Wholesale";
	private static final String BUSINESS_SERVICES = "Business Services";
	private static final String CATV_SYSTEMS = "CATV Systems";
	private static final String CATALOG_AND_MAIL_ORDER_HOUSES = "Catalog & Mail Order Houses";
	private static final String COMPUTER_WHOLESALE = "Computers Wholesale";
	private static final String CONSUMER_SERVICES = "Consumer Services";
	private static final String DEPARTMENT_STORES = "Department Stores";
	private static final String DISCOUNT_VARIETY_STORES = "Discount, Variety Stores";
	private static final String DRUG_STORES = "Drug Stores";
	private static final String DRUG_WHOLESALE = "Drugs Wholesale";
	private static final String EDUCATION_AND_TRAINING_SERVICES = "Education & Training Services";
	private static final String ELECTRONIC_STORES = "Electronics Stores";
	private static final String ELECTRONIC_WHOLESALE = "Electronics Wholesale";
	private static final String ENTERTAINMENT_DIVERSIFIED = "Entertainment - Diversified";
	private static final String FODD_WHOLESALE = "Food Wholesale";
	private static final String GAMING_ACTIVITIES = "Gaming Activities";
	private static final String GENERAL_ENTERTAINMENT = "General Entertainment";
	private static final String GROCERY_STORES = "Grocery Stores";
	private static final String HOME_FURNISHING_STORES = "Home Furnishing Stores";
	private static final String HOME_IMPROVEMENT_STORES = "Home Improvement Stores";
	private static final String INDUSTRIAL_EUIPMENT_WHOLESALE = "Industrial Equipment Wholesale";
	private static final String JEWELLERY_STORES = "Jewelry Stores";
	private static final String LODGING = "Lodging";
	private static final String MAJOR_AIRLINES = "Major Airlines";
	private static final String MANAGEMENT_SERVICES = "Management Services";
	private static final String MARKETING_SERVICES = "Marketing Services";
	private static final String MEDICAL_EQUIPMENT_WHOLESALE = "Medical Equipment Wholesale";
	private static final String MOVIE_PRODUCTION_THEATERS = "Movie Production, Theaters";
	private static final String MUSIC_VIDEO_STORES = "Music & Video Stores";
	private static final String PERSONAL_SERVICES = "Personal Services";
	private static final String PLUBLISHING_BOOKS = "Publishing - Books";
	private static final String PLUBLISHING_NEWSPAPERS = "Publishing - Newspapers";
	private static final String PLUBLISHING_PERIODICALS = "Publishing - Periodicals";
	private static final String RAILROADS = "Railroads";
	private static final String REGIONAL_AIRLINES = "Regional Airlines";
	private static final String RENTAL_AND_LEASING_SERVICES = "Rental & Leasing Services";
	private static final String RESEARCH_SERVICES = "Research Services";
	private static final String RESORTS_AND_CASINOS = "Resorts & Casinos";
	private static final String RESTAURANTS = "Restaurants";
	private static final String SECURITY_AND_PROTECTION_SERVICES = "Security & Protection Services";
	private static final String SHIPPING = "Shipping";
	private static final String SPECIALITY_EATERIES = "Specialty Eateries";
	private static final String SPECIALITY_RETAIL_OTHER = "Specialty Retail, Other";
	private static final String SPORTING_ACTIVITIES = "Sporting Activities";
	private static final String SPORTING_GOODS_STORES = "Sporting Goods Stores";
	private static final String STAFFING_AND_OUTSOURCINS_SERVICES = "Staffing & Outsourcing Services";
	private static final String TECHNICAL_SERVICES = "Technical Services";
	private static final String TOY_AND_HOBBY = "Toy & Hobby Stores";
	private static final String TRUCKING = "Trucking";
	private static final String WHOLESALE_OTHER = "Wholesale, Other";

	// Technology
	private static final String APPLICATION_SOFTWARE = "Application Software";
	private static final String BUSINESS_SOFTWARE_AND_SERVICES = "Business Software & Services";
	private static final String COMMUNICATION_EQUIPMENT = "Communication Equipment";
	private static final String COMPUTER_BASED_SYSTEMS = "Computer Based Systems";
	private static final String COMPUTER_PERIPHERALS = "Computer Peripherals";
	private static final String DATA_STORAGE_DEVICES = "Data Storage Devices";
	private static final String DIVERSIFIED_COMMUNICATION_SERVICES = "Diversified Communication Services";
	private static final String DIVERSIFIED_COMPUTER_SYSTEMS = "Diversified Computer Systems";
	private static final String DIVERSIFIED_ELECTRONICS = "Diversified Electronics";
	private static final String HEALTH_CARE_INFORMATION_SERVICES = "Healthcare Information Services";
	private static final String INFORMATION_AND_DELIVERY_SERVICES = "Information & Delivery Services";
	private static final String INFORMATION_TECHNOLOGY_SERVICES = "Information Technology Services";
	private static final String INTERNET_INFORMATION_PROVIDERS = "Internet Information Providers";
	private static final String INTERNERT_SERVICE_PROVIDERS = "Internet Service Providers";
	private static final String INTERNET_SOFTWARE_SERVICES = "Internet Software & Services";
	private static final String LONG_DISTANCE_CARRIERS = "Long Distance Carriers";
	private static final String MULTIMEDIA_AND_GRAPHICS_SOFTWARE = "Multimedia and graghics software";
	private static final String NETWORKING_AND_COMMUNICATIONS_DEVICES = "Networking and Communications Devices";
	private static final String PERSONAL_COMPUTERS = "Personal Computers";
	private static final String PRINTED_CIRCUIT_BOARDS = "Printed Circuit Boards";
	private static final String PROCESSING_SYSTEMS_AND_PRODUCTS = "Processing Systems & Products";
	private static final String SCIENTIFIC_AND_TECHNICAL_INSTRUMENTS = "Scientific & Technical Instruments";
	private static final String SECURITY_SOFTWARE_AND_SERVICES = "Security software and services";
	private static final String SEMICONDUCTOR_BROADLINE = "Semiconductor - Broad Line";
	private static final String SEMICONDUCTOR_INTEGRATED_CIRCUITS = "Semiconductor - Integrated Circuits";
	private static final String SEMICONDUCTOR_SPECIALISED = "Semiconductor - Specialized";
	private static final String SEMICONDUCTOR_EQUIPMENT_AND_MATERIALS = "Semiconductor Equipment & Materials";
	private static final String SEMICONDUCTOR_INTEGRATED_MEMORYCHIPS = "Semiconductor- Memory Chips";
	private static final String TECHNICAL_AND_SYSTEM_SOFTWARE = "Technical and System Software";
	private static final String TELECOM_SERVICES_DOMESTIC = "Telecom Services - Domestic";
	private static final String TELECOM_SERVICES_FOREIGN = "Telecom Services - Foreign";
	private static final String WIRELESS_COMMUNICATIONS = "Wireless Communications";

	// UTILITIES
	private static final String DIVERSIFIED_UTILITIES = "Diversified Utilities";
	private static final String ELECTRIC_UTILITIES = "Electric Utilities";
	private static final String FOREIGN_UTILITIES = "Foreign Utilities";
	private static final String GAS_UTILITIES = "Gas Utilities";
	private static final String WATER_UTILITIES = "Water Utilities";

	private static final String INDUSTRY_ID = "123456789";
	private static final Logger LOGGER = LoggerFactory
			.getLogger(IndustryData.class);

	private List<MasterIndustryDto> getMasterIndustryList() {

		List<MasterIndustryDto> masterList = new ArrayList<MasterIndustryDto>();

		masterList.add(new MasterIndustryDto(BASIC_MATERIALS, false,
				getBasicMaterial()));
		masterList.add(new MasterIndustryDto(CONSUMER_GOODS, false,
				getConsumerGoods()));
		masterList.add(new MasterIndustryDto(FINANCIAL, false, getFinancial()));
		masterList.add(new MasterIndustryDto(HEALTH_CARE, false,
				getHealthCare()));
		masterList.add(new MasterIndustryDto(INDUSTRIAL_GOODS, false,
				getIndustrialGoods()));
		masterList.add(new MasterIndustryDto(SERVICES, false, getServiceIndustry()));
		masterList
				.add(new MasterIndustryDto(TECHNOLOGY, false, getTechnology()));
		masterList.add(new MasterIndustryDto(UTILITIES, false, getUtilities()));

		return masterList;
	}

	private List<SubIndustryDto> getIndustrySubList(String industry) {

		List<SubIndustryDto> industryList = new ArrayList<SubIndustryDto>();
		switch (industry) {
		case BASIC_MATERIALS:
			industryList = getBasicMaterial();
			break;
		case CONGLOMERATES:
			industryList = getConglomerates();
			break;
		case CONSUMER_GOODS:
			industryList = getConsumerGoods();
			break;
		case FINANCIAL:
			industryList = getFinancial();
			break;
		case HEALTH_CARE:
			industryList = getHealthCare();
			break;
		case INDUSTRIAL_GOODS:
			industryList = getIndustrialGoods();
			break;
		case SERVICES:
			industryList = getServiceIndustry();
			break;
		case TECHNOLOGY:
			industryList = getTechnology();
			break;
		case UTILITIES:
			industryList = getUtilities();
			break;
		default:
			break;
		}

		return industryList;
	}

	private List<SubIndustryDto> getBasicMaterial() {

		List<SubIndustryDto> basicMaterialList = new ArrayList<SubIndustryDto>();

		basicMaterialList.add(new SubIndustryDto(INDUSTRY_ID,
				AGRICULTURAL_CHEMICALS, AGRICULTURAL_CHEMICALS, false));
		basicMaterialList.add(new SubIndustryDto(INDUSTRY_ID, ALUMINIUM,
				ALUMINIUM, false));
		basicMaterialList.add(new SubIndustryDto(INDUSTRY_ID,
				INDEPENDENT_OIL_AND_GAS, INDEPENDENT_OIL_AND_GAS, false));
		basicMaterialList.add(new SubIndustryDto(INDUSTRY_ID,
				INDUSTRIAL_METALS_AND_MINERALS, INDUSTRIAL_METALS_AND_MINERALS,
				false));
		basicMaterialList.add(new SubIndustryDto(INDUSTRY_ID,
				NONMETALLIC_MINERAL_MINING, NONMETALLIC_MINERAL_MINING, false));
		basicMaterialList.add(new SubIndustryDto(INDUSTRY_ID,
				OIL_AND_GAS_DRILLING_AND_EXPLORATION,
				OIL_AND_GAS_DRILLING_AND_EXPLORATION, false));
		basicMaterialList.add(new SubIndustryDto(INDUSTRY_ID,
				OIL_AND_GAS_EQUIPMENT_AND_SERVICES,
				OIL_AND_GAS_EQUIPMENT_AND_SERVICES, false));
		basicMaterialList.add(new SubIndustryDto(INDUSTRY_ID,
				OIL_AND_GAS_PIPELINES, OIL_AND_GAS_PIPELINES, false));
		basicMaterialList.add(new SubIndustryDto(INDUSTRY_ID,
				OIL_AND_GAS_REFINING_AND_MARKETING,
				OIL_AND_GAS_REFINING_AND_MARKETING, false));
		basicMaterialList.add(new SubIndustryDto(INDUSTRY_ID, SILVER, SILVER,
				false));
		basicMaterialList.add(new SubIndustryDto(INDUSTRY_ID,
				SPECIALTY_CHEMICALS, SPECIALTY_CHEMICALS, false));
		basicMaterialList.add(new SubIndustryDto(INDUSTRY_ID, STEEL_AND_IRON,
				STEEL_AND_IRON, false));
		basicMaterialList.add(new SubIndustryDto(INDUSTRY_ID, SYNTHETICS,
				SYNTHETICS, false));

		return basicMaterialList;
	}

	private List<SubIndustryDto> getConglomerates() {

		List<SubIndustryDto> conglomerateList = new ArrayList<SubIndustryDto>();

		conglomerateList.add(new SubIndustryDto(INDUSTRY_ID, CONGLOMERATES,
				CONGLOMERATES, false));

		return conglomerateList;

	}

	private List<SubIndustryDto> getConsumerGoods() {

		List<SubIndustryDto> consumerGoodsList = new ArrayList<SubIndustryDto>();

		consumerGoodsList.add(new SubIndustryDto(INDUSTRY_ID, APPLIANCES,
				APPLIANCES, false));
		consumerGoodsList.add(new SubIndustryDto(INDUSTRY_ID,
				BEVERAGES_BREWERS, BEVERAGES_BREWERS, false));
		consumerGoodsList.add(new SubIndustryDto(INDUSTRY_ID,
				BEVERAGES_SOFTDRINKS, BEVERAGES_SOFTDRINKS, false));
		consumerGoodsList.add(new SubIndustryDto(INDUSTRY_ID,
				BEVERAGES_WINERIES_DISTILLERS, BEVERAGES_WINERIES_DISTILLERS,
				false));
		consumerGoodsList.add(new SubIndustryDto(INDUSTRY_ID,
				BUSINESS_EQUIPMENT, BUSINESS_EQUIPMENT, false));
		consumerGoodsList.add(new SubIndustryDto(INDUSTRY_ID, CIGARETTES,
				CIGARETTES, false));
		consumerGoodsList.add(new SubIndustryDto(INDUSTRY_ID,
				CLEANING_PRODUCTS, CLEANING_PRODUCTS, false));
		consumerGoodsList.add(new SubIndustryDto(INDUSTRY_ID, CONFECTIONERS,
				CONFECTIONERS, false));
		consumerGoodsList.add(new SubIndustryDto(INDUSTRY_ID, DAIRY_PRODUCTS,
				DAIRY_PRODUCTS, false));
		consumerGoodsList.add(new SubIndustryDto(INDUSTRY_ID, FARM_PRODUCTS,
				ELECTRONIC_EQUIPMENT, false));
		consumerGoodsList.add(new SubIndustryDto(INDUSTRY_ID,
				ELECTRONIC_EQUIPMENT, FARM_PRODUCTS, false));
		consumerGoodsList
				.add(new SubIndustryDto(INDUSTRY_ID, FOOD, FOOD, false));
		consumerGoodsList.add(new SubIndustryDto(INDUSTRY_ID,
				HOUSEWARE_AND_ACCESSORIES, HOUSEWARE_AND_ACCESSORIES, false));
		consumerGoodsList.add(new SubIndustryDto(INDUSTRY_ID, MEAT_PRODUCTS,
				MEAT_PRODUCTS, false));
		consumerGoodsList.add(new SubIndustryDto(INDUSTRY_ID, OFFICE_SUPPLIES,
				OFFICE_SUPPLIES, false));
		consumerGoodsList.add(new SubIndustryDto(INDUSTRY_ID,
				PACKAGING_AND_CONTAINERS, PACKAGING_AND_CONTAINERS, false));
		consumerGoodsList.add(new SubIndustryDto(INDUSTRY_ID,
				PAPER_AND_PAPER_PRODUCTS, PAPER_AND_PAPER_PRODUCTS, false));
		consumerGoodsList.add(new SubIndustryDto(INDUSTRY_ID,
				PERSONAL_PRODUCTS, PERSONAL_PRODUCTS, false));
		consumerGoodsList.add(new SubIndustryDto(INDUSTRY_ID,
				PHOTOGRAPHIC_EQUIPMENT_AND_SUPPLIES,
				PHOTOGRAPHIC_EQUIPMENT_AND_SUPPLIES, false));
		consumerGoodsList.add(new SubIndustryDto(INDUSTRY_ID,
				PROCESSED_AND_PACKAGED_GOODS, PROCESSED_AND_PACKAGED_GOODS,
				false));
		consumerGoodsList.add(new SubIndustryDto(INDUSTRY_ID,
				RECREATIONAL_GOODS, RECREATIONAL_GOODS, false));
		consumerGoodsList.add(new SubIndustryDto(INDUSTRY_ID,
				RECREATIONAL_VEHICLES, RECREATIONAL_VEHICLES, false));
		consumerGoodsList.add(new SubIndustryDto(INDUSTRY_ID,
				RUBBER_AND_PLASTICS, RUBBER_AND_PLASTICS, false));
		consumerGoodsList.add(new SubIndustryDto(INDUSTRY_ID, SPORTING_GOODS,
				SPORTING_GOODS, false));
		consumerGoodsList.add(new SubIndustryDto(INDUSTRY_ID,
				TEXTILE_APPAREL_CLOTHING, TEXTILE_APPAREL_CLOTHING, false));
		consumerGoodsList.add(new SubIndustryDto(INDUSTRY_ID,
				TEXTILE_APPAREL_FOOTWARE_AND_ACCESSORIES,
				TEXTILE_APPAREL_FOOTWARE_AND_ACCESSORIES, false));
		consumerGoodsList.add(new SubIndustryDto(INDUSTRY_ID,
				TEXTILE_APPAREL_FOOTWARE_AND_ACCESSORIES,
				TEXTILE_APPAREL_FOOTWARE_AND_ACCESSORIES, false));
		consumerGoodsList.add(new SubIndustryDto(INDUSTRY_ID, TOBACCO_PRODUCTS,
				TOBACCO_PRODUCTS, false));
		consumerGoodsList.add(new SubIndustryDto(INDUSTRY_ID, TOYS_AND_GAMES,
				TOYS_AND_GAMES, false));
		consumerGoodsList.add(new SubIndustryDto(INDUSTRY_ID,
				TRUCKS_AND_VEHICLES, TRUCKS_AND_VEHICLES, false));

		return consumerGoodsList;
	}

	private List<SubIndustryDto> getFinancial() {

		List<SubIndustryDto> financialList = new ArrayList<SubIndustryDto>();

		financialList.add(new SubIndustryDto(INDUSTRY_ID,
				ACCIDENT_AND_HEALTH_INSURANCE, ACCIDENT_AND_HEALTH_INSURANCE,
				false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID, ASSET_MANAGEMENT,
				ASSET_MANAGEMENT, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID, CLOSED_END_FUND_DEBT,
				CLOSED_END_FUND_DEBT, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID,
				CLOSED_END_FUND_FOREIGN, CLOSED_END_FUND_FOREIGN, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID, CREDIT_SERVICES,
				CREDIT_SERVICES, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID,
				DIVERSIFIED_INVESTMENTS, DIVERSIFIED_INVESTMENTS, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID,
				FOREIGN_MONEY_CENTER_BANKS, FOREIGN_MONEY_CENTER_BANKS, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID, INSURANCE_BROKERS,
				INSURANCE_BROKERS, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID,
				INVESTMENT_BROKERAGE_NATIONAL, INVESTMENT_BROKERAGE_NATIONAL,
				false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID,
				INVESTMENT_BROKERAGE_REGIONAL, INVESTMENT_BROKERAGE_REGIONAL,
				false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID, LIFE_INSURANCE,
				LIFE_INSURANCE, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID, MONEY_CENTER_BANKS,
				MONEY_CENTER_BANKS, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID, MORTGAGE_INVESTMENT,
				MORTGAGE_INVESTMENT, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID,
				PROPERTY_AND_CASUALITY_INSURANCE,
				PROPERTY_AND_CASUALITY_INSURANCE, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID, PROPERTY_MANAGEMENT,
				PROPERTY_MANAGEMENT, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID, REIT_DIVERSIFIED,
				REIT_DIVERSIFIED, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID,
				REIT_HEALTHCARE_FACILITIES, REIT_HEALTHCARE_FACILITIES, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID, REIT_HOTEL_MOTEL,
				REIT_HOTEL_MOTEL, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID, REIT_INDUSTRIAL,
				REIT_INDUSTRIAL, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID, REIT_OFFICE,
				REIT_OFFICE, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID, REIT_RESIDENTIAL,
				REIT_RESIDENTIAL, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID, REIT_RETAIL,
				REIT_RETAIL, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID,
				REAL_ESTATE_DEVELOPEMENT, REAL_ESTATE_DEVELOPEMENT, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID,
				REGIONAL_MIDATLANTIC_BANK, REGIONAL_MIDATLANTIC_BANK, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID,
				REGIONAL_MIDWEST_BANKS, REGIONAL_MIDWEST_BANKS, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID,
				REGIONAL_NORTH_EAST_BANKS, REGIONAL_NORTH_EAST_BANKS, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID,
				REGIONAL_PACIFIC_BANKS, REGIONAL_PACIFIC_BANKS, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID,
				REGIONAL_SOUTH_EAST_BANKS, REGIONAL_SOUTH_EAST_BANKS, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID,
				REGIONAL_SOUTH_WEST_BANKS, REGIONAL_SOUTH_WEST_BANKS, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID, SAVINGS_AND_LOANS,
				SAVINGS_AND_LOANS, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID,
				SURETY_ANDTITLE_INSURANCE, SURETY_ANDTITLE_INSURANCE, false));

		return financialList;
	}

	private List<SubIndustryDto> getHealthCare() {

		List<SubIndustryDto> financialList = new ArrayList<SubIndustryDto>();

		financialList.add(new SubIndustryDto(INDUSTRY_ID, BIOTECHNOLOGY,
				BIOTECHNOLOGY, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID,
				DIAGNOSTIC_SUBSTANCES, DIAGNOSTIC_SUBSTANCES, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID, DRUG_DELIVERY,
				DRUG_DELIVERY, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID,
				DRUG_MANUFACTURERS_MAJOR, DRUG_MANUFACTURERS_MAJOR, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID,
				DRUG_MANUFACTURERS_OTHER, DRUG_MANUFACTURERS_OTHER, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID,
				DRUG_RELATED_PRODUCTS, DRUG_RELATED_PRODUCTS, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID, DRUG_GENERIC,
				DRUG_GENERIC, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID, HEALTH_CARE_PLANS,
				HEALTH_CARE_PLANS, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID, HOSPITALS, HOSPITALS,
				false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID, HOME_HEALTH_CARE,
				HOME_HEALTH_CARE, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID,
				LONG_TERM_CARE_FACILITIES, LONG_TERM_CARE_FACILITIES, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID,
				MEDICAL_APPLIANCES_AND_EQUIPMENT,
				MEDICAL_APPLIANCES_AND_EQUIPMENT, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID,
				MEDICAL_INSTRUMENTS_AND_SUPPLIES,
				MEDICAL_INSTRUMENTS_AND_SUPPLIES, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID,
				MEDICAL_LABORATORIES_AND_RESEARCH,
				MEDICAL_LABORATORIES_AND_RESEARCH, false));
		financialList.add(new SubIndustryDto(INDUSTRY_ID,
				MEDICAL_PRACTITIONERS, MEDICAL_PRACTITIONERS, false));
		financialList
				.add(new SubIndustryDto(INDUSTRY_ID,
						SPECIALIZED_HEALTH_SERVICES,
						SPECIALIZED_HEALTH_SERVICES, false));

		return financialList;
	}

	private List<SubIndustryDto> getIndustrialGoods() {

		List<SubIndustryDto> industryList = new ArrayList<SubIndustryDto>();

		industryList.add(new SubIndustryDto(INDUSTRY_ID,
				AEROSPACE_DEFENSE_MAJOR, AEROSPACE_DEFENSE_MAJOR, false));
		industryList.add(new SubIndustryDto(INDUSTRY_ID,
				AEROSPACE_DEFENSE_PRODUCTS, AEROSPACE_DEFENSE_PRODUCTS, false));
		industryList
				.add(new SubIndustryDto(INDUSTRY_ID, CEMENT, CEMENT, false));
		industryList.add(new SubIndustryDto(INDUSTRY_ID, DIVERSIFIED_MACHINERY,
				DIVERSIFIED_MACHINERY, false));
		industryList.add(new SubIndustryDto(INDUSTRY_ID,
				FARM_AND_CONSTRUCTUON_MACHINERY,
				FARM_AND_CONSTRUCTUON_MACHINERY, false));
		industryList.add(new SubIndustryDto(INDUSTRY_ID,
				GENERAL_BUILDING_MATERIAL, GENERAL_BUILDING_MATERIAL, false));
		industryList.add(new SubIndustryDto(INDUSTRY_ID, GENERAL_CONTRACTORS,
				GENERAL_CONTRACTORS, false));
		industryList.add(new SubIndustryDto(INDUSTRY_ID, HEAVY_CONSTRUCTION,
				HEAVY_CONSTRUCTION, false));
		industryList.add(new SubIndustryDto(INDUSTRY_ID,
				INDUSTRIAL_ELECTRICAL_EQUIPMENT,
				INDUSTRIAL_ELECTRICAL_EQUIPMENT, false));
		industryList.add(new SubIndustryDto(INDUSTRY_ID,
				INDUSTRIAL_EQUIPMENT_AND_COMPONENTS,
				INDUSTRIAL_EQUIPMENT_AND_COMPONENTS, false));
		industryList.add(new SubIndustryDto(INDUSTRY_ID, LUMBER_WOOD,
				LUMBER_WOOD, false));
		industryList.add(new SubIndustryDto(INDUSTRY_ID, MACHINE_TOOLS,
				MACHINE_TOOLS, false));
		industryList.add(new SubIndustryDto(INDUSTRY_ID, MANUFACTURED_HOUSING,
				MANUFACTURED_HOUSING, false));
		industryList.add(new SubIndustryDto(INDUSTRY_ID, METAL_FABRICATION,
				METAL_FABRICATION, false));
		industryList.add(new SubIndustryDto(INDUSTRY_ID,
				POLLUTION_AND_TREATMENT_CONTROLS,
				POLLUTION_AND_TREATMENT_CONTROLS, false));
		industryList.add(new SubIndustryDto(INDUSTRY_ID,
				RESIDENTIAL_CONTRUCTION, RESIDENTIAL_CONTRUCTION, false));
		industryList.add(new SubIndustryDto(INDUSTRY_ID, TEXTILE_INDUSTRIAL,
				TEXTILE_INDUSTRIAL, false));
		industryList.add(new SubIndustryDto(INDUSTRY_ID, WASTE_MANEGEMENT,
				WASTE_MANEGEMENT, false));

		return industryList;
	}

	private List<SubIndustryDto> getServiceIndustry() {

		List<SubIndustryDto> servicesList = new ArrayList<SubIndustryDto>();

		servicesList.add(new SubIndustryDto(INDUSTRY_ID,
				AIRDELIVERY_AND_FREIGHT_SERVICES,
				AIRDELIVERY_AND_FREIGHT_SERVICES, false));

		servicesList.add(new SubIndustryDto(INDUSTRY_ID, AIR_SERVICES,
				AIR_SERVICES, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, APPAREL_STORES,
				APPAREL_STORES, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, AUTO_DEALERSHIP,
				AUTO_DEALERSHIP, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, AUTO_PARTS_STORES,
				AUTO_PARTS_STORES, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, AUTO_PARTS_WHOLESALE,
				AUTO_PARTS_WHOLESALE, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID,
				BASIC_MATERIALS_WHOLESALE, BASIC_MATERIALS_WHOLESALE, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, BROADCASTING_RADIO,
				BROADCASTING_RADIO, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, BROADCASTING_TV,
				BROADCASTING_TV, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID,
				BUILDING_MATERIALS_WHOLESALE, BUILDING_MATERIALS_WHOLESALE,
				false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, BUSINESS_SERVICES,
				BUSINESS_SERVICES, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, CATV_SYSTEMS,
				CATV_SYSTEMS, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID,
				CATALOG_AND_MAIL_ORDER_HOUSES, CATALOG_AND_MAIL_ORDER_HOUSES,
				false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, COMPUTER_WHOLESALE,
				COMPUTER_WHOLESALE, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, CONSUMER_SERVICES,
				CONSUMER_SERVICES, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, DEPARTMENT_STORES,
				DEPARTMENT_STORES, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID,
				DISCOUNT_VARIETY_STORES, DISCOUNT_VARIETY_STORES, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, DRUG_STORES,
				DRUG_STORES, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, DRUG_WHOLESALE,
				DRUG_WHOLESALE, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID,
				EDUCATION_AND_TRAINING_SERVICES,
				EDUCATION_AND_TRAINING_SERVICES, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, ELECTRONIC_STORES,
				ELECTRONIC_STORES, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, ELECTRONIC_WHOLESALE,
				ELECTRONIC_WHOLESALE, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID,
				ENTERTAINMENT_DIVERSIFIED, ENTERTAINMENT_DIVERSIFIED, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, FODD_WHOLESALE,
				FODD_WHOLESALE, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, GAMING_ACTIVITIES,
				GAMING_ACTIVITIES, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, GENERAL_ENTERTAINMENT,
				GENERAL_ENTERTAINMENT, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, GROCERY_STORES,
				GROCERY_STORES, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID,
				HOME_FURNISHING_STORES, HOME_FURNISHING_STORES, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID,
				HOME_IMPROVEMENT_STORES, HOME_IMPROVEMENT_STORES, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID,
				INDUSTRIAL_EUIPMENT_WHOLESALE, INDUSTRIAL_EUIPMENT_WHOLESALE,
				false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, JEWELLERY_STORES,
				JEWELLERY_STORES, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, LODGING, LODGING,
				false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, MAJOR_AIRLINES,
				MAJOR_AIRLINES, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, MANAGEMENT_SERVICES,
				MANAGEMENT_SERVICES, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, MARKETING_SERVICES,
				MARKETING_SERVICES, false));
		servicesList
				.add(new SubIndustryDto(INDUSTRY_ID,
						MEDICAL_EQUIPMENT_WHOLESALE,
						MEDICAL_EQUIPMENT_WHOLESALE, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID,
				MOVIE_PRODUCTION_THEATERS, MOVIE_PRODUCTION_THEATERS, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, MUSIC_VIDEO_STORES,
				MUSIC_VIDEO_STORES, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, PERSONAL_SERVICES,
				PERSONAL_SERVICES, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, PLUBLISHING_BOOKS,
				PLUBLISHING_BOOKS, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID,
				PLUBLISHING_NEWSPAPERS, PLUBLISHING_NEWSPAPERS, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID,
				PLUBLISHING_PERIODICALS, PLUBLISHING_PERIODICALS, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, RAILROADS, RAILROADS,
				false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, REGIONAL_AIRLINES,
				REGIONAL_AIRLINES, false));
		servicesList
				.add(new SubIndustryDto(INDUSTRY_ID,
						RENTAL_AND_LEASING_SERVICES,
						RENTAL_AND_LEASING_SERVICES, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, RESEARCH_SERVICES,
				RESEARCH_SERVICES, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, RESORTS_AND_CASINOS,
				RESORTS_AND_CASINOS, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, RESTAURANTS,
				RESTAURANTS, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID,
				SECURITY_AND_PROTECTION_SERVICES,
				SECURITY_AND_PROTECTION_SERVICES, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, SHIPPING, SHIPPING,
				false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, SPECIALITY_EATERIES,
				SPECIALITY_EATERIES, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID,
				SPECIALITY_RETAIL_OTHER, SPECIALITY_RETAIL_OTHER, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, SPORTING_ACTIVITIES,
				SPORTING_ACTIVITIES, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, SPORTING_GOODS_STORES,
				SPORTING_GOODS_STORES, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID,
				STAFFING_AND_OUTSOURCINS_SERVICES,
				STAFFING_AND_OUTSOURCINS_SERVICES, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, TECHNICAL_SERVICES,
				TECHNICAL_SERVICES, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, TOY_AND_HOBBY,
				TOY_AND_HOBBY, false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, TRUCKING, TRUCKING,
				false));
		servicesList.add(new SubIndustryDto(INDUSTRY_ID, WHOLESALE_OTHER,
				WHOLESALE_OTHER, false));

		return servicesList;
	}

	private List<SubIndustryDto> getTechnology() {

		List<SubIndustryDto> technologyList = new ArrayList<SubIndustryDto>();

		technologyList.add(new SubIndustryDto(INDUSTRY_ID,
				APPLICATION_SOFTWARE, APPLICATION_SOFTWARE, false));
		technologyList.add(new SubIndustryDto(INDUSTRY_ID,
				BUSINESS_SOFTWARE_AND_SERVICES, BUSINESS_SOFTWARE_AND_SERVICES,
				false));
		technologyList.add(new SubIndustryDto(INDUSTRY_ID,
				COMMUNICATION_EQUIPMENT, COMMUNICATION_EQUIPMENT, false));
		technologyList.add(new SubIndustryDto(INDUSTRY_ID,
				COMPUTER_BASED_SYSTEMS, COMPUTER_BASED_SYSTEMS, false));
		technologyList.add(new SubIndustryDto(INDUSTRY_ID,
				COMPUTER_PERIPHERALS, COMPUTER_PERIPHERALS, false));
		technologyList.add(new SubIndustryDto(INDUSTRY_ID,
				DATA_STORAGE_DEVICES, DATA_STORAGE_DEVICES, false));
		technologyList.add(new SubIndustryDto(INDUSTRY_ID,
				DIVERSIFIED_COMMUNICATION_SERVICES,
				DIVERSIFIED_COMMUNICATION_SERVICES, false));
		technologyList.add(new SubIndustryDto(INDUSTRY_ID,
				DIVERSIFIED_COMPUTER_SYSTEMS, DIVERSIFIED_COMPUTER_SYSTEMS,
				false));
		technologyList.add(new SubIndustryDto(INDUSTRY_ID,
				DIVERSIFIED_ELECTRONICS, DIVERSIFIED_ELECTRONICS, false));
		technologyList.add(new SubIndustryDto(INDUSTRY_ID,
				HEALTH_CARE_INFORMATION_SERVICES,
				HEALTH_CARE_INFORMATION_SERVICES, false));
		technologyList.add(new SubIndustryDto(INDUSTRY_ID,
				INFORMATION_AND_DELIVERY_SERVICES,
				INFORMATION_AND_DELIVERY_SERVICES, false));
		technologyList.add(new SubIndustryDto(INDUSTRY_ID,
				INFORMATION_TECHNOLOGY_SERVICES,
				INFORMATION_TECHNOLOGY_SERVICES, false));
		technologyList.add(new SubIndustryDto(INDUSTRY_ID,
				INTERNET_INFORMATION_PROVIDERS, INTERNET_INFORMATION_PROVIDERS,
				false));
		technologyList
				.add(new SubIndustryDto(INDUSTRY_ID,
						INTERNERT_SERVICE_PROVIDERS,
						INTERNERT_SERVICE_PROVIDERS, false));
		technologyList.add(new SubIndustryDto(INDUSTRY_ID,
				INTERNET_SOFTWARE_SERVICES, INTERNET_SOFTWARE_SERVICES, false));
		technologyList.add(new SubIndustryDto(INDUSTRY_ID,
				LONG_DISTANCE_CARRIERS, LONG_DISTANCE_CARRIERS, false));
		technologyList.add(new SubIndustryDto(INDUSTRY_ID,
				MULTIMEDIA_AND_GRAPHICS_SOFTWARE,
				MULTIMEDIA_AND_GRAPHICS_SOFTWARE, false));
		technologyList.add(new SubIndustryDto(INDUSTRY_ID,
				NETWORKING_AND_COMMUNICATIONS_DEVICES,
				NETWORKING_AND_COMMUNICATIONS_DEVICES, false));
		technologyList.add(new SubIndustryDto(INDUSTRY_ID, PERSONAL_COMPUTERS,
				PERSONAL_COMPUTERS, false));
		technologyList.add(new SubIndustryDto(INDUSTRY_ID,
				PRINTED_CIRCUIT_BOARDS, PRINTED_CIRCUIT_BOARDS, false));
		technologyList.add(new SubIndustryDto(INDUSTRY_ID,
				PROCESSING_SYSTEMS_AND_PRODUCTS,
				PROCESSING_SYSTEMS_AND_PRODUCTS, false));
		technologyList.add(new SubIndustryDto(INDUSTRY_ID,
				SCIENTIFIC_AND_TECHNICAL_INSTRUMENTS,
				SCIENTIFIC_AND_TECHNICAL_INSTRUMENTS, false));
		technologyList.add(new SubIndustryDto(INDUSTRY_ID,
				SECURITY_SOFTWARE_AND_SERVICES, SECURITY_SOFTWARE_AND_SERVICES,
				false));
		technologyList.add(new SubIndustryDto(INDUSTRY_ID,
				SEMICONDUCTOR_BROADLINE, SEMICONDUCTOR_BROADLINE, false));
		technologyList.add(new SubIndustryDto(INDUSTRY_ID,
				SEMICONDUCTOR_INTEGRATED_CIRCUITS,
				SEMICONDUCTOR_INTEGRATED_CIRCUITS, false));
		technologyList.add(new SubIndustryDto(INDUSTRY_ID,
				SEMICONDUCTOR_SPECIALISED, SEMICONDUCTOR_SPECIALISED, false));
		technologyList.add(new SubIndustryDto(INDUSTRY_ID,
				SEMICONDUCTOR_EQUIPMENT_AND_MATERIALS,
				SEMICONDUCTOR_EQUIPMENT_AND_MATERIALS, false));
		technologyList.add(new SubIndustryDto(INDUSTRY_ID,
				SEMICONDUCTOR_INTEGRATED_MEMORYCHIPS,
				SEMICONDUCTOR_INTEGRATED_MEMORYCHIPS, false));
		technologyList.add(new SubIndustryDto(INDUSTRY_ID,
				TECHNICAL_AND_SYSTEM_SOFTWARE, TECHNICAL_AND_SYSTEM_SOFTWARE,
				false));
		technologyList.add(new SubIndustryDto(INDUSTRY_ID,
				TELECOM_SERVICES_DOMESTIC, TELECOM_SERVICES_DOMESTIC, false));
		technologyList.add(new SubIndustryDto(INDUSTRY_ID,
				TELECOM_SERVICES_FOREIGN, TELECOM_SERVICES_FOREIGN, false));
		technologyList.add(new SubIndustryDto(INDUSTRY_ID,
				WIRELESS_COMMUNICATIONS, WIRELESS_COMMUNICATIONS, false));

		return technologyList;
	}

	private List<SubIndustryDto> getUtilities() {

		List<SubIndustryDto> utilityList = new ArrayList<SubIndustryDto>();

		utilityList.add(new SubIndustryDto(INDUSTRY_ID, DIVERSIFIED_UTILITIES,
				DIVERSIFIED_UTILITIES, false));
		utilityList.add(new SubIndustryDto(INDUSTRY_ID, ELECTRIC_UTILITIES,
				ELECTRIC_UTILITIES, false));
		utilityList.add(new SubIndustryDto(INDUSTRY_ID, FOREIGN_UTILITIES,
				FOREIGN_UTILITIES, false));
		utilityList.add(new SubIndustryDto(INDUSTRY_ID, GAS_UTILITIES,
				GAS_UTILITIES, false));
		utilityList.add(new SubIndustryDto(INDUSTRY_ID, WATER_UTILITIES,
				WATER_UTILITIES, false));

		return utilityList;
	}

	private Map<String, String> getAllIndustries() {
		HashMap<String, String> industries = new HashMap<String, String>();

		for (SubIndustryDto industry : getBasicMaterial()) {
			industries.put(industry.getName(), BASIC_MATERIALS);
		}
		for (SubIndustryDto industry : getConglomerates()) {
			industries.put(industry.getName(), CONGLOMERATES);
		}
		for (SubIndustryDto industry : getConsumerGoods()) {
			industries.put(industry.getName(), CONSUMER_GOODS);
		}
		for (SubIndustryDto industry : getFinancial()) {
			industries.put(industry.getName(), FINANCIAL);
		}
		for (SubIndustryDto industry : getIndustrialGoods()) {
			industries.put(industry.getName(), INDUSTRIAL_GOODS);
		}
		for (SubIndustryDto industry : getHealthCare()) {
			industries.put(industry.getName(), HEALTH_CARE);
		}
		for (SubIndustryDto industry : getServiceIndustry()) {
			industries.put(industry.getName(), SERVICES);
		}
		for (SubIndustryDto industry : getTechnology()) {
			industries.put(industry.getName(), TECHNOLOGY);
		}
		for (SubIndustryDto industry : getUtilities()) {
			industries.put(industry.getName(), UTILITIES);
		}
		return industries;

	}

	public List<MasterIndustryDto> getMasterIndustry(
			List<Industry> subIndustries) {
		// all master group
		List<MasterIndustryDto> masterIndustries = getMasterIndustryList();
		// get all master groups based on sub industry list
		if (subIndustries != null) {
			Set<String> selectedMasterList = new HashSet<String>();
			Map<String, String> industries = getAllIndustries();
			for (Industry industry : subIndustries) {
				String selectedMaster = (String) industries.get(industry
						.getName());
				selectedMasterList.add(selectedMaster);
			}
			for (String selectedMaster : selectedMasterList) {
				for (MasterIndustryDto master : masterIndustries) {
					if (master.getName().equals(selectedMaster)) {
						master.setSelected(true);
						List<SubIndustryDto> subIndustryDtos = compareDtos(
								getIndustrySubList(master.getName()),
								subIndustries);
						master.setIndustryDtos(subIndustryDtos);

					}
				}
			}

		} else {
			masterIndustries = getMasterIndustryList();
		}
		return masterIndustries;
	}

	public List<MasterIndustryDto> getMasterIndustryForClient(
			List<Industry> parentCompanyIndustries,
			List<Industry> childCompanyIndustries) {
		List<MasterIndustryDto> masterIndustries = getMasterIndustryList();
		if (parentCompanyIndustries != null) {

			LOGGER.info("Industry {} ", parentCompanyIndustries);
			if (parentCompanyIndustries.isEmpty()) {
				return new ArrayList<MasterIndustryDto>();
			}
			String selectedMaster = null;
			Map<String, String> industries = getAllIndustries();
			for (Industry industry : parentCompanyIndustries) {
				selectedMaster = (String) industries.get(industry.getName());
				break;
			}
			for (Iterator<MasterIndustryDto> iterator = masterIndustries
					.iterator(); iterator.hasNext();) {
				MasterIndustryDto obj = iterator.next();
				if (!obj.getName().equals(selectedMaster)) {
					// Remove the current element from the iterator and the
					// list.
					iterator.remove();
				} else {
					obj.setSelected(true);
					List<SubIndustryDto> dtos = compareIndustryForClient(parentCompanyIndustries);
					if (childCompanyIndustries == null) {
						obj.setIndustryDtos(dtos);
					} else {
						obj.setIndustryDtos(compareDtos(dtos,
								childCompanyIndustries));
					}
				}
			}

		} else {
			masterIndustries = new ArrayList<MasterIndustryDto>();
		}
		return masterIndustries;
	}

	public List<SubIndustryDto> compareIndustryForClient(
			List<Industry> parentCompanyIndustries) {
		List<SubIndustryDto> dtos = new ArrayList<SubIndustryDto>();
		for (Industry subDto : parentCompanyIndustries) {

			SubIndustryDto dto = new SubIndustryDto(subDto.getId(),
					subDto.getDescription(), subDto.getName(), false);
			dtos.add(dto);
		}

		return dtos;
	}

	public String getMasterListName(List<Industry> subIndustries) {
		if (subIndustries != null) {
			String selectedMaster = null;
			Map<String, String> industries = getAllIndustries();
			for (Industry industry : subIndustries) {
				selectedMaster = (String) industries.get(industry.getName());
				return selectedMaster;
			}
		}
		return null;

	}

	private List<SubIndustryDto> compareDtos(
			List<SubIndustryDto> subIndustryDtos, List<Industry> subIndustries) {
		for (SubIndustryDto subDto : subIndustryDtos) {
			for (Industry industry : subIndustries) {
				if (industry.getName().equals(subDto.getName())) {
					subDto.setSelected(true);
					break;
				}
			}
		}
		return subIndustryDtos;
	}
}
