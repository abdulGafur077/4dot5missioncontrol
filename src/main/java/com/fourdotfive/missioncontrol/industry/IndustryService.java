package com.fourdotfive.missioncontrol.industry;

import java.util.List;

import com.fourdotfive.missioncontrol.dtos.company.MasterIndustryDto;
import com.fourdotfive.missioncontrol.pojo.company.Industry;

public interface IndustryService {

	List<MasterIndustryDto> getChildIndustryList(String parentCompanyId,
			String childCompanyId);

	List<MasterIndustryDto> getParentIndustryList(String parentCompanyId);

	void updateParentCompanyIndustryList(List<Industry> industryList,
			String parentCompanyId);
	
}
