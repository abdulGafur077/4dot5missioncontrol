package com.fourdotfive.missioncontrol.industry;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fourdotfive.missioncontrol.common.AppConstants;
import com.fourdotfive.missioncontrol.company.CompanyApiExecutor;
import com.fourdotfive.missioncontrol.company.CompanyService;
import com.fourdotfive.missioncontrol.dtos.company.MasterIndustryDto;
import com.fourdotfive.missioncontrol.dtos.company.SubIndustryDto;
import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.company.CompanyType;
import com.fourdotfive.missioncontrol.pojo.company.Industry;

@Service
public class IndustryServiceImp implements IndustryService {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(IndustryServiceImp.class);

	@Autowired
	private IndustryData industryData;

	@Autowired
	private CompanyApiExecutor apiExecutor;

	@Autowired
	private CompanyService companyService;

	@Override
	public List<MasterIndustryDto> getChildIndustryList(String parentCompanyId,
			String childCompanyId) {
		return getIndustryList(childCompanyId);
	}

	@Override
	public List<MasterIndustryDto> getParentIndustryList(String parentCompanyId) {
		List<MasterIndustryDto> industryList = null;
		if (parentCompanyId.equals("0")) {
			industryList = industryData.getMasterIndustry(null);
		} else {
			Company company = apiExecutor.getCompany(parentCompanyId);
			List<Industry> industries = company.getIndustryList();
			industryList = industryData.getMasterIndustry(industries);
			if (!company.getCompanyType().equals(CompanyType.Host)) {
				industryList = getIndustryListFromClient(company, industryList);
			}

		}
		return industryList;
	}

	private List<MasterIndustryDto> getIndustryList(String companyId) {
		List<MasterIndustryDto> industryList = null;
		if (companyId.equals("0")) {
			industryList = industryData.getMasterIndustry(null);
		} else {
			Company company = apiExecutor.getCompany(companyId);
			List<Industry> industries = company.getIndustryList();
			industryList = industryData.getMasterIndustry(industries);
		}
		return industryList;
	}

	private List<MasterIndustryDto> getIndustryListFromClient(Company company,
			List<MasterIndustryDto> actualSelectedMasterList) {
		// all the main and sub industry for staffing company
		for (MasterIndustryDto actualIndustry : actualSelectedMasterList) {
			// industry list from clients
			for (MasterIndustryDto clientIndustry : getAllClientIndustryList(company)) {
				// based on group name and disability update staffing company
				// group industry to disable
				if (actualIndustry.getName().equals(clientIndustry.getName())
						&& clientIndustry.isDisabled()) {
					// based on industry name and disability update staffing
					// company industry to disable
					for (SubIndustryDto actualIndustryDto : actualIndustry
							.getIndustryDtos()) {
						for (SubIndustryDto clientIndustryDto : clientIndustry
								.getIndustryDtos()) {
							if (actualIndustryDto.getName().equals(
									clientIndustryDto.getName())
									&& clientIndustryDto.isDisabled()) {
								actualIndustryDto.setDisabled(true);
								actualIndustryDto.setSelected(true);
							}
						}
					}
				}
			}
		}
		// checking for logs
		for (MasterIndustryDto industryDto : actualSelectedMasterList) {
			if (industryDto.isSelected()) {
				LOGGER.debug("master group selected  {}", industryDto.getName());
			}
			if (industryDto.isDisabled()) {
				LOGGER.debug("master group disabled {}", industryDto.getName());
			}
			for (SubIndustryDto clientIndustryDto : industryDto
					.getIndustryDtos()) {
				if (clientIndustryDto.isSelected()) {
					LOGGER.debug("sub industry is selected {}",
							clientIndustryDto.getName());
				}
				if (clientIndustryDto.isDisabled()) {
					LOGGER.debug("sub industry is disabled {}",
							clientIndustryDto.getName());
				}
			}
		}
		return actualSelectedMasterList;
	}

	private List<MasterIndustryDto> getAllClientIndustryList(Company company) {
		List<Company> clientCompanyList = companyService.getClientOrBUs(company
				.getId());
		// remove archived company list
		for (Iterator<Company> iterator = clientCompanyList.iterator(); iterator
				.hasNext();) {
			Company obj = iterator.next();
			if (obj.getCompanyState().equals(AppConstants.ARCHIVED)) {
				// Remove the current element from the iterator and the
				// list.
				iterator.remove();
			}
		}
		// add all industry to set(to remove duplicate)
		Set<Industry> allIndustryList = new HashSet<Industry>();
		for (Company clientCompany : clientCompanyList) {
			allIndustryList.addAll(clientCompany.getIndustryList());
		}
		// change to list (make sure no duplicate industry gets added)
		List<Industry> selectedIndustryList = new ArrayList<>();
		for (Industry industry : allIndustryList) {
			if (!selectedIndustryList.contains(industry)) {
				selectedIndustryList.add(industry);
			}
		}
		// change main group and sub industry to disabled if the it has been
		// selected by clients
		List<MasterIndustryDto> industryList = industryData
				.getMasterIndustry(selectedIndustryList);
		for (MasterIndustryDto masterIndustry : industryList) {
			if (masterIndustry.isSelected()) {
				masterIndustry.setDisabled(true);
				for (SubIndustryDto subIndustryDto : masterIndustry
						.getIndustryDtos()) {
					if (subIndustryDto.isSelected()) {
						subIndustryDto.setDisabled(true);
					}
				}
			}

		}
		return industryList;
	}

	@Override
	public void updateParentCompanyIndustryList(
			List<Industry> clientIndustryList, String parentCompanyId) {
		// move the list to temporary list
		List<Industry> childIndustryList = clientIndustryList;
		// get industry of the parent
		Company parentCompany = companyService.getCompany(parentCompanyId);
		List<Industry> parentIndustryList = parentCompany.getIndustryList();
		// remove the common industry between staffing and client company
		for (Industry parentIndustry : parentIndustryList) {
			for (Iterator<Industry> iterator = childIndustryList.iterator(); iterator
					.hasNext();) {
				Industry obj = iterator.next();
				if (obj.getName().equals(parentIndustry.getName())) {
					// Remove the current element from the iterator and the
					// list.
					iterator.remove();
				}
			}
		}
		parentIndustryList.addAll(childIndustryList);
		parentCompany.setCreatedDate(null);
		parentCompany.setLastModifiedDate(null);
		// set industry list
		parentCompany.setIndustryList(parentIndustryList);
		// save staffing company
		apiExecutor.saveCompany(parentCompany);

	}

}
