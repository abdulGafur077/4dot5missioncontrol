package com.fourdotfive.missioncontrol.internal.callback;

import com.fourdotfive.missioncontrol.alert.NotificationService;
import com.fourdotfive.missioncontrol.alert.WorkflowUpdateAlertService;
import com.fourdotfive.missioncontrol.chat.ChatMessage;
import com.fourdotfive.missioncontrol.chat.ChatService;
import com.fourdotfive.missioncontrol.pojo.alert.Notification;
import com.fourdotfive.missioncontrol.pojo.alert.WorkflowUpdateAlertDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/internal/")
public class InternalCallbackController {
    private static final Logger LOGGER = LoggerFactory.getLogger(InternalCallbackController.class);

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private ChatService chatService;

    @Autowired
    private WorkflowUpdateAlertService workflowUpdateAlertService;

    @RequestMapping(value = "uinotificationcallback", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void uiNotificationCallback(@RequestBody Notification notification) {
        LOGGER.info("uiNotificationCallback: notification - " + notification);
        notificationService.sendMessage(notification);
    }

    @RequestMapping(value = "chatcallback", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void chatCallback(@RequestBody ChatMessage chatMessage) {
        chatService.sendMessage(chatMessage);
    }

    @RequestMapping(value = "workflow/alert/update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void workflowUpdateCallback(@RequestBody WorkflowUpdateAlertDTO workflowUpdateAlert) {
        LOGGER.info("workflow alert to : " + workflowUpdateAlert.getRecipientId());
        workflowUpdateAlertService.sendMessage(workflowUpdateAlert);
    }
}


