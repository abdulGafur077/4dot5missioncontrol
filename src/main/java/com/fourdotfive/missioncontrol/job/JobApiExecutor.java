package com.fourdotfive.missioncontrol.job;

import com.fourdotfive.missioncontrol.dtos.candidate.DeleteDTO;
import com.fourdotfive.missioncontrol.dtos.job.*;
import com.fourdotfive.missioncontrol.dtos.requisition.VendorRecruiterUpdateDto;
import com.fourdotfive.missioncontrol.pojo.FourDotFiveResponse;
import com.fourdotfive.missioncontrol.pojo.job.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface JobApiExecutor {
    /**
     * To get Jobs by company Id.
     */
    List<NewJobDto> getFilteredJobs(String companyId, String userId, JobRequestDto jobRequestDto);

    List<NewJobDto> getJobsByCompanyId(JobRequestDto jobRequestDto, String companyId, String userId);

    Job updateJobStatus(JobState jobState);

    /**
     * To Get Job Types, this method is called.
     */
    String getStatusTypes();

    FilterResponse getClientAndRecruitersByRoles(JobRequestDto jobRequestDto);

    FilterResponse getClientAndRecruitersOfCompanyByRoles(JobRequestDto jobRequestDto, String companyId);

    FilterResponse getClientAndRoleByRecruiters(JobRequestDto jobRequestDto);

    FilterResponse getClientAndRoleOfCompanyByRecruiters(JobRequestDto jobRequestDto, String companyId);

    FilterResponse getRoleAndRecruitersByClients(JobRequestDto jobRequestDto);

    FilterResponse getRoleAndRecruitersOfCompanyByClients(JobRequestDto jobRequestDto, String companyId);

    List<String> searchByReqNumber(String searchText, String companyId, List<String> userList);

    List<String> searchByReqNumberCompany(String searchText, String companyId);

    String searchJobByText(JobRequestDto jobRequestDto, String companyId, String userId);

    String searchJobInCompanyByText(JobRequestDto jobRequestDto, String companyId, String userId);

    List<JobMatchCandidateDto> jobMatchByCandidate(CandidateJobMatchRequestDTO candidateJobMatchRequestDTO, Integer page, Integer size, String column, String sortDirection);

    FourDotFiveResponse<JobResponseDto> createJob(JobCreationModel jobCreationModel, String companyId, String clientOrBuid);

    FourDotFiveResponse<JobResponseDto> updateJob(JobCreationModel jobCreationModel, String companyId, String userId, String clientOrBuid);

    FourDotFiveResponse<HashMap<String, Object>> getMoreJobBoardCandidatesForJob(GetJBCandidatesModel getJBCandidatesModel);

    String getCandidateJobComparisonByJobMatchId(String jobMatchId);

    String getJobDetailsById(String jobId);

    String getMultipleCandidateProfileSingleJobComparison(List<String> candidateIds, String jobId);

    String getMultipleJobSingleCandidateProfileComparison(List<String> jobIds, String candidateId);

    RequisitionStatusResponse getCandidatesForRequisitionStatus(String requisitionId, String requisitionStatus);

    List<JobOpening> getCandidatesForCurrentState(String requisitionId, String requisitionStatus);
    
    String getActivityTypes();

    String getEnabledSteps(String jobId);

    String getDashboardCountByJobStateForCompany(String companyId);

    String getDashboardCountByJobStateForUser(String userId);

    List<JobMinDetailsDto> getJobsByActivitiesForCompany(String companyId, String userId);

    List<JobMinDetailsDto> getJobsByActivitiesForUser(String companyId, String userId);

    String updateFourDotFiveIntelligenceScore(JobScoreDTO jobScoreDTO);

    String updateLocation(JobLocationUpdateDTO jobLocationUpdateDTO, String userId);

    String deleteJob(DeleteDTO jobDeleteDTO);

    FourDotFiveResponse<SearchInterfaceResult> getSearchInterface(SearchInterfaceRequest searchInterfaceRequest);

    Job saveDaxtraParameters(String jobId, Map<String, String> daxtraParameters);

    CandidateCount getCandidateCountByRequisitionId(String jobId, String jbiTransactionId);

    Boolean isGetMoreResumesInProgress(String jobId);

    List<CandidateRunScoreResponseDto> getCandidateJobMatchInfoByJbiTransactionId(String jbiTransactionId);

    List<JobBoardSearchHistory> getJobBoardSearchHistory(String jobId, int page, int size, String sort, String sortDir);

    JobBoardCredentialDto addJobBoardCredentials(JobBoardCredentialModel jobBoardCredentialModel);

    DeleteAccountResponse deleteJobBoardCredentials(String accountToken, String companyId);

    Job removeDaxtraParameters(String jobId);

    List<String> getJobNamesByCompany(String searchText, String companyId);

    List<String> getJobNamesByUser(String searchText, String userId, String companyId);

    Boolean jobBoardSearchAvailabilityCheck();

    String isSharedRequisition(String requisitionId);

    String assignVendorRecruiters(VendorRecruiterUpdateDto vendorRecruiterUpdate);

    String copyRequisition(String requisitionId, String companyId, String userId);

    List<NewJobDto> getAllJobsByClientOrBuId(JobRequestDto jobRequestDto, String companyId, String userId);

    String getReleasedJobDetails(String candidateId);

    List<NewJobDto> getAllJobsByClientOrBuIdAndCompanyId(JobRequestDto jobRequestDto, String companyId, String clientOrBuId, String userId);

    String getLastAddedCandidateSourceTypeId(String jobId);

    Job findJobById(String jobId);

    String getPointOfContact(String jobId);

    String getSentAssessmentDetailsByAssessmentId(String assessmentId, String jobId);

    List<JobDetailsDTO> getRequisitionsByAssement(JobAssessmentRequestDTO jobAssessmentRequestDTO);

    void saveCandidateScoreOnVendorAssociation(List<String> vendorIds, String jobId);

    String getSharedJobWorkflowDetails(String jobId);
}
