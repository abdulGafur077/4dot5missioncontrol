package com.fourdotfive.missioncontrol.job;

import com.fourdotfive.missioncontrol.dtos.candidate.DeleteDTO;
import com.fourdotfive.missioncontrol.dtos.job.*;
import com.fourdotfive.missioncontrol.dtos.requisition.VendorRecruiterUpdateDto;
import com.fourdotfive.missioncontrol.exception.PlatformException;
import com.fourdotfive.missioncontrol.pojo.FourDotFiveResponse;
import com.fourdotfive.missioncontrol.pojo.job.*;
import com.fourdotfive.missioncontrol.security.FourDotFiveUser;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/job")
public class JobController {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(JobController.class);
    @Autowired
    private JobService jobService;

    /**
     * To get jobs based on the role of the user.
     */
    @RequestMapping(value = "/userJobs/{userId}/{companyId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<NewJobDto>> getJobsByCompanyId(
            @RequestBody JobRequestDto jobRequestDto,
            @PathVariable(value = "userId") String userId,
            @PathVariable(value = "companyId") String companyId) {
        LOGGER.info("jobRequestDto====" + jobRequestDto);
        List<NewJobDto> response = jobService.getJobsBasedOnLoggedInUser(jobRequestDto, companyId, userId);
        return new ResponseEntity<List<NewJobDto>>(response,
                HttpStatus.OK);
    }

    @RequestMapping(value = "/status", method = RequestMethod.POST)
    public ResponseEntity<Job> setStatus(
            @RequestBody JobState jobState) {
        Job response = jobService.updateJobStatus(jobState);
        if (response == null)
            return new ResponseEntity<Job>(response, HttpStatus.NOT_FOUND);
        return new ResponseEntity<Job>(response, HttpStatus.OK);
    }

    /**
     * To Get Job Types, this method is called.
     */
    @RequestMapping(value = "/statustypes", method = RequestMethod.GET)
    public ResponseEntity<String> getStatusTypes() {
        String response = jobService.getStatusTypes();
        return new ResponseEntity<String>(response, HttpStatus.OK);
    }


    @RequestMapping(value = "/clientandrecruitersbyroles/{userId}/{companyId}", method = RequestMethod.POST)
    public ResponseEntity<FilterResponse> getClientAndRecruitersByRoles(
            @RequestBody JobRequestDto jobRequestDto,
            @PathVariable(value = "userId") String userId,
            @PathVariable(value = "companyId") String companyId) {
        FilterResponse filterResponse = new FilterResponse();
        filterResponse = jobService.getClientAndRecruitersByRoles(jobRequestDto, companyId, userId);
        return new ResponseEntity<FilterResponse>(filterResponse, HttpStatus.OK);
    }

    @RequestMapping(value = "/clientandrolebyrecruiters/{userId}/{companyId}", method = RequestMethod.POST)
    public ResponseEntity<FilterResponse> getClientAndRoleByRecruiters(
            @RequestBody JobRequestDto jobRequestDto,
            @PathVariable(value = "userId") String userId,
            @PathVariable(value = "companyId") String companyId) {
        FilterResponse filterResponse = new FilterResponse();
        filterResponse = jobService.getClientAndRoleByRecruiters(jobRequestDto, companyId, userId);
        return new ResponseEntity<FilterResponse>(filterResponse, HttpStatus.OK);
    }

    @RequestMapping(value = "/roleandrecruitersbyclients/{userId}/{companyId}", method = RequestMethod.POST)
    public ResponseEntity<FilterResponse> getRoleAndRecruitersByClients(
            @RequestBody JobRequestDto jobRequestDto,
            @PathVariable(value = "userId") String userId,
            @PathVariable(value = "companyId") String companyId) {
        LOGGER.info("jobRequestDto ===== in mission control" + jobRequestDto);
        FilterResponse filterResponse = new FilterResponse();
        filterResponse = jobService
                .getRoleAndRecruitersByClients(jobRequestDto, companyId, userId);
        return new ResponseEntity<FilterResponse>(filterResponse, HttpStatus.OK);
    }

    @RequestMapping(value = "/searchbyreqnum/{userId}/{companyId}/{searchText}", method = RequestMethod.POST)
    public ResponseEntity<List<String>> searchByReqNumber(
            @PathVariable(value = "searchText") String searchText,
            @PathVariable(value = "userId") String userId,
            @PathVariable(value = "companyId") String companyId) {
        LOGGER.info("search texr ===== in mission control" + searchText);
        List<String> response = jobService.searchByReqNumber(searchText, companyId, userId);
        return new ResponseEntity<List<String>>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/searchjob/{userId}/{companyId}", method = RequestMethod.POST)
    public ResponseEntity<String> searchJobByText(
            @RequestBody JobRequestDto jobRequestDto,
            @PathVariable(value = "userId") String userId,
            @PathVariable(value = "companyId") String companyId) {
        String response = jobService.searchJobByText(jobRequestDto, companyId, userId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/jobmatchbycandidate", method = RequestMethod.POST)
    public ResponseEntity getJobMatchByCandidate(@RequestBody CandidateJobMatchRequestDTO candidateJobMatchRequestDTO,
                                                 @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                                 @RequestParam(value = "size", required = false, defaultValue = "5") Integer size,
                                                 @RequestParam(value = "sort", required = false) String column,
                                                 @RequestParam(value = "sortDir", required = false) String sortDirection) {
        List<JobMatchCandidateDto> response = jobService.jobMatchByCandidate(candidateJobMatchRequestDTO , page, size, column, sortDirection);
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/createjob/{companyId}", method = RequestMethod.POST)
    public ResponseEntity<FourDotFiveResponse<JobResponseDto>> createJob(@RequestBody JobCreationModel jobCreationModel,
                                                                         @PathVariable("companyId") String companyId,
                                                                         @RequestParam(value = "clientOrBuid", required = false, defaultValue = "") String clientOrBuid) {

        FourDotFiveResponse<JobResponseDto> result = jobService.createJob(jobCreationModel, companyId, clientOrBuid);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/updatejob/{companyId}/{userId}", method = RequestMethod.POST)
    public ResponseEntity<FourDotFiveResponse<JobResponseDto>> updateJob(@RequestBody JobCreationModel jobCreationModel,
                                                                         @PathVariable("companyId") String companyId,
                                                                         @PathVariable("userId") String userId,
                                                                         @RequestParam(value = "clientOrBuid", required = false, defaultValue = "") String clientOrBuid) {

        FourDotFiveResponse<JobResponseDto> result = jobService.updateJob(jobCreationModel, companyId, userId, clientOrBuid);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "getMoreJBCandidates", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<FourDotFiveResponse<HashMap<String, Object>>> getMoreJBCandidates(@RequestBody GetJBCandidatesModel getJBCandidatesModel) {
        FourDotFiveUser principal = (FourDotFiveUser) (SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        String userName = null;
        if (principal != null)
            userName = principal.getUsername();
        getJBCandidatesModel.setUserName(userName);
        HttpStatus httpStatus = HttpStatus.OK;
        FourDotFiveResponse<HashMap<String, Object>> result = jobService.getMoreJobBoardCandidatesForJob(getJBCandidatesModel);
        if (result.getMessage() != null) {
            httpStatus = HttpStatus.ACCEPTED;
        }
        return new ResponseEntity<>(result, httpStatus);
    }


    @RequestMapping(value = {"candidateprofilecomparison/{jobmatchid}"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity<String> getCandidateProfileComparisonByJobMatchId(@PathVariable(value = "jobmatchid") String jobMatchId) {
        if (StringUtils.isBlank(jobMatchId)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        String candidateJobComparison = jobService.getCandidateJobComparisonByJobMatchId(jobMatchId);
        return new ResponseEntity<>(candidateJobComparison, HttpStatus.OK);
    }

    @RequestMapping(value = {"jobdetails/{jobid}"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity<String> getJobDetailsById(@PathVariable(value = "jobid") String jobId) {
        if (StringUtils.isBlank(jobId)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        String jobDetails = jobService.getJobDetailsById(jobId);
        return new ResponseEntity<>(jobDetails, HttpStatus.OK);
    }

    @RequestMapping(value = {"multiplecandidateprofilesinglejobcomparison/{jobid}"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity<String> getMultipleCandidateProfileSingleComparison(@RequestParam List<String> candidateIds, @PathVariable(value = "jobid") String jobId) {
        if (StringUtils.isBlank(jobId)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        String candidateJobComparison = jobService.getMultipleCandidateProfileSingleJobComparison(candidateIds, jobId);
        return new ResponseEntity<>(candidateJobComparison, HttpStatus.OK);
    }

    @RequestMapping(value = {"multiplejobsinglecandidateprofilecomparison/{candidateid}"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity<String> getMultipleJobSingleCandidateProfileComparison(@RequestParam List<String> jobIds, @PathVariable(value = "candidateid") String candidateId) {
        if (StringUtils.isBlank(candidateId)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        String candidateMultipleJobComparison = jobService.getMultipleJobSingleCandidateProfileComparison(jobIds, candidateId);
        return new ResponseEntity<>(candidateMultipleJobComparison, HttpStatus.OK);
    }

    @RequestMapping(value = {"/getcandidatesforreqstatus/{requisitionId}/{requisitionStatus}"}, method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<RequisitionStatusResponse> getCandidatesForRequisitionStatus(
            @PathVariable("requisitionId") String requisitionId,
            @PathVariable("requisitionStatus") String requisitionStatus) {
        RequisitionStatusResponse response = jobService
                .getCandidatesForRequisitionStatus(requisitionId,
                        requisitionStatus);
        if (response != null) {
            return new ResponseEntity<RequisitionStatusResponse>(response,
                    HttpStatus.OK);
        } else {
            return new ResponseEntity<RequisitionStatusResponse>(
                    HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = {
            "getcandidatesforcurrentstatus/{requisitionId}/{requisitionStatus}"}, method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<List<JobOpening>> getCandidatesForCurrentState(
            @PathVariable("requisitionId") String requisitionId,
            @PathVariable("requisitionStatus") String requisitionStatus) {
        List<JobOpening> response = jobService.getCandidatesForCurrentState(requisitionId, requisitionStatus);
        if (response != null) {
            return new ResponseEntity<List<JobOpening>>(response, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<JobOpening>>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/activitytypes", method = RequestMethod.GET)
    public ResponseEntity<String> getActivitysTypes() {
        String response = jobService.getActivityTypes();
        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/enabledsteps/{jobid}", method = RequestMethod.GET)
    public String getEnabledSteps(@PathVariable("jobid") String jobId) {
        if (StringUtils.isEmpty(jobId)) {
            throw new IllegalArgumentException("Job id not found");
        }
        return jobService.getEnabledSteps(jobId);
    }

    @RequestMapping(value = {"/dashboardreqcount/{userid}/{companyid}"}, method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<String> getDashboardCountByJobState(@PathVariable String companyid,
                                                       @PathVariable String userid) {
        String statusCount = jobService.getDashboardCountByJobState(companyid, userid);
        return new ResponseEntity<>(statusCount, HttpStatus.OK);
    }

    @RequestMapping(value = "/jobsbyactivities/{userid}/{companyid}", method = RequestMethod.GET)
    public ResponseEntity<List<JobMinDetailsDto>> getJobsByActivities(@PathVariable String companyid,
                                                               @PathVariable String userid) {
        List<JobMinDetailsDto> response = jobService.getJobsByActivities(companyid, userid);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/updatefourdotfiveintelligencescore", method = RequestMethod.PUT)
    public String updateFourDotFiveIntelligenceScore(@RequestBody JobScoreDTO jobScoreDTO) {
        if (jobScoreDTO == null)
            throw new IllegalArgumentException("Nothing to update");
        return jobService.updateFourDotFiveIntelligenceScore(jobScoreDTO);
    }

    @RequestMapping(value = "/updatelocation", method = RequestMethod.POST)
    public String updateLocation(@RequestBody JobLocationUpdateDTO jobLocationUpdateDTO) {
        if (jobLocationUpdateDTO == null)
            throw new IllegalArgumentException("Nothing to update");
        return jobService.updateLocation(jobLocationUpdateDTO);
    }

    @RequestMapping(value = {"/delete"}, method = RequestMethod.DELETE)
    public String deleteCandidate(@RequestBody DeleteDTO jobDeleteDTO) {
        if (jobDeleteDTO == null) {
            throw new PlatformException("Can not delete the job.");
        }
        if (StringUtils.isEmpty(jobDeleteDTO.getId())) {
            throw new PlatformException("Job id can not be null.");
        }
        return jobService.deleteJob(jobDeleteDTO);
    }

    @RequestMapping(value = "getsearchinterface", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<FourDotFiveResponse<SearchInterfaceResult>> getSearchInterface(@RequestBody SearchInterfaceRequest searchInterfaceRequest) {
        FourDotFiveResponse<SearchInterfaceResult> result = jobService.getSearchInterface(searchInterfaceRequest);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/savedaxtraparameters/{jobId}", method = RequestMethod.POST)
    public ResponseEntity<Job> saveDaxtraParameters(@PathVariable(value = "jobId") String jobId
            , @RequestBody Map<String, String> daxtraParameters) {
        Job result = jobService.saveDaxtraParameters(jobId, daxtraParameters);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/removedaxtraparameters/{jobId}", method = RequestMethod.GET)
    public ResponseEntity<Job> removeDaxtraParameters(@PathVariable(value = "jobId") String jobId) {
        Job result = jobService.removeDaxtraParameters(jobId);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/getcandidatecountbyreqid/{jobid}", method = RequestMethod.GET)
    public ResponseEntity<CandidateCount> getCandidateCountByRequisitionId(@PathVariable(value = "jobid") String jobId
            , @RequestParam(value = "jbiTransactionId", required = false) String jbiTransactionId) {
        CandidateCount response = jobService.getCandidateCountByRequisitionId(jobId, jbiTransactionId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/isgetmoreresumesinprogress/{jobid}", method = RequestMethod.GET)
    public ResponseEntity<Boolean> isGetMoreResumesInProgress(@PathVariable(value = "jobid") String jobId) {
        Boolean response = jobService.isGetMoreResumesInProgress(jobId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/getcandidatejobmatchinfo/{jbiTransactionId}", method = RequestMethod.GET)
    public ResponseEntity<List<CandidateRunScoreResponseDto>> getCandidateJobMatchInfoByJbiTransactionId(
            @PathVariable(value = "jbiTransactionId") String jbiTransactionId) {
        List<CandidateRunScoreResponseDto> response = jobService.getCandidateJobMatchInfoByJbiTransactionId(jbiTransactionId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/getjobboardsearchhistory/{jobid}", method = RequestMethod.GET)
    public ResponseEntity<List<JobBoardSearchHistory>> getJobBoardSearchHistory(@PathVariable(value = "jobid") String jobId,
                                                                                @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                                                                @RequestParam(value = "size", required = false, defaultValue = "5") Integer size,
                                                                                @RequestParam(value = "sort", required = false) String column,
                                                                                @RequestParam(value = "sortDir", required = false) String sortDirection) {
        List<JobBoardSearchHistory> response = jobService.getJobBoardSearchHistory(jobId, page, size, column, sortDirection);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/addjobboardcredentials", method = RequestMethod.POST)
    public ResponseEntity<JobBoardCredentialDto> addJobBoardCredentials(@RequestBody JobBoardCredentialModel jobBoardCredentialModel) {
        if (jobBoardCredentialModel == null) {
            throw new PlatformException("Job board credentials cannot be null");
        }
        JobBoardCredentialDto jobBoardCredentialDto = jobService.addJobBoardCredentials(jobBoardCredentialModel);
        if (jobBoardCredentialDto != null && StringUtils.isNotEmpty(jobBoardCredentialDto.getKey())
                && StringUtils.isEmpty(jobBoardCredentialDto.getError())) {
            return new ResponseEntity<>(jobBoardCredentialDto, HttpStatus.OK);
        } else {
            String errorMessage = "Job Board credentials could not be added!";
            if (jobBoardCredentialDto != null && StringUtils.isNotEmpty(jobBoardCredentialDto.getError()))
                errorMessage = jobBoardCredentialDto.getError();
            throw new PlatformException(errorMessage);
        }
    }

    @RequestMapping(value = "/deletejobboardcredentials/{companyId}", method = RequestMethod.GET)
    public ResponseEntity<DeleteAccountResponse> deleteJobBoardCredentials(@PathVariable(value = "companyId") String companyId,
                                                                           @RequestParam(value = "accountToken") String accountToken) {
        if (accountToken == null || accountToken.trim().isEmpty())
            throw new PlatformException("Key cannot be null.");
        if (companyId == null) {
            throw new PlatformException("Company cannot be null");
        }

        DeleteAccountResponse deleteAccountResponse = jobService.deleteJobBoardCredentials(accountToken, companyId);
        if (deleteAccountResponse != null && deleteAccountResponse.isStatus())
            return new ResponseEntity<>(deleteAccountResponse, HttpStatus.OK);
        else {
            String errorMessage = "Job Board credentials could not be deleted!";
            if (deleteAccountResponse != null && StringUtils.isNotEmpty(deleteAccountResponse.getError()))
                errorMessage = deleteAccountResponse.getError();
            throw new PlatformException(errorMessage);
        }
    }

    @RequestMapping(value = "/getroles/{userId}/{companyId}/{searchtext}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<String>> searchRoles(
            @PathVariable(value = "searchtext") String searchText,
            @PathVariable(value = "userId") String userId,
            @PathVariable(value = "companyId") String companyId) {
        List<String> response = jobService.getRoles(searchText, companyId, userId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @RequestMapping(value = "/jobboardsearchavailabilitycheck", method = RequestMethod.GET)
    public ResponseEntity<Boolean> jobBoardSearchAvailabilityCheck() {
        Boolean response = jobService.jobBoardSearchAvailabilityCheck();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = {"issharedrequisition/{requisitionId}"}, method = RequestMethod.GET)
    public @ResponseBody
    String isSharedRequisition(@PathVariable("requisitionId") String requisitionId) {
        return jobService.isSharedRequisition(requisitionId);
    }

    @RequestMapping(value = "/assign/vendor/recruiters", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String assignVendorRecruiters(@RequestBody VendorRecruiterUpdateDto vendorRecruiterUpdate) {
        if (StringUtils.isBlank(vendorRecruiterUpdate.getVendorId()))
            throw new PlatformException("Vendor id can't be null");

        return jobService.assignVendorRecruiters(vendorRecruiterUpdate);

    }

    @RequestMapping(value = "/copy/requisition/{requisitionId}/{companyId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String copyRequisition(@PathVariable("requisitionId") String requisitionId,
                                  @PathVariable(value = "companyId") String companyId) {
        if (StringUtils.isBlank(requisitionId))
            throw new PlatformException("No requisition found to copy.");
        if (StringUtils.isBlank(companyId))
            throw new PlatformException("Company id can't be empty");
        return jobService.copyRequisition(requisitionId, companyId);
    }

    @RequestMapping(value = "/getreleasedjobdetails/{candidateid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getReleasedJobDetails(@PathVariable("candidateid") String candidateId) {
        return jobService.getReleasedJobDetails(candidateId);
    }

    @RequestMapping(value = "/getlastaddedcandidatesourcetypeid/{jobId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getLastAddedCandidateSourceTypeId(@PathVariable("jobId") String jobId) {
        return jobService.getLastAddedCandidateSourceTypeId(jobId);
    }

    @RequestMapping(value = {"getpointofcontact/{jobId}"}, method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<String> getPointOfContact(@PathVariable("jobId") String jobId) {
        if (StringUtils.isBlank(jobId)) {
            throw new PlatformException("JobId can not be null");
        }
        String pointOfContact = jobService.getPointOfContact(jobId);
        return new ResponseEntity<>(pointOfContact, HttpStatus.OK);
    }

    @RequestMapping(value = "getsentassessmentdetailsbyassessmentid/{assessmentId}/{jobId}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<String> getSentAssessmentDetailsByAssessmentId(@PathVariable("assessmentId") String assessmentId,
                                                                  @PathVariable("jobId") String jobId) {
        String sentAssessmentDetails = jobService.getSentAssessmentDetailsByAssessmentId(assessmentId, jobId);
        return new ResponseEntity<>(sentAssessmentDetails, HttpStatus.OK);
    }

    @RequestMapping(value = "getrequisitionsbyassement", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<List<JobDetailsResponseDTO>> getRequisitionsByAssement(@RequestBody JobAssessmentRequestDTO jobAssessmentRequestDTO) {
        List<JobDetailsResponseDTO> jobDetailsDTOS = jobService.getRequisitionsByAssement(jobAssessmentRequestDTO);
        return new ResponseEntity<>(jobDetailsDTOS, HttpStatus.OK);
    }

    @RequestMapping(value = "createcandidatescorewithvendorjobs/{jobId}", method = RequestMethod.POST)
    public @ResponseBody
    void getRequisitionsByAssement(@RequestBody List<String> vendorIds,
                                   @PathVariable(value = "jobId") String jobId) {

        if (CollectionUtils.isEmpty(vendorIds))
            throw new PlatformException("Vendor ids can't be null");

        if (StringUtils.isEmpty(jobId))
            throw new PlatformException("Job id can't be null");

        jobService.saveCandidateScoreOnVendorAssociation(vendorIds, jobId);
    }

    @RequestMapping(value = {"/getsharedjobworkflowdetails/{jobId}"}, method = RequestMethod.GET)
    public String getSharedClientDetails(@PathVariable(value = "jobId") String jobId) {
        if (StringUtils.isEmpty(jobId))
            throw new PlatformException("job id can't be null");
        return jobService.getSharedJobWorkflowDetails(jobId);
    }
}