package com.fourdotfive.missioncontrol.job;

import com.fourdotfive.missioncontrol.dtos.candidate.DeleteDTO;
import com.fourdotfive.missioncontrol.dtos.job.*;
import com.fourdotfive.missioncontrol.dtos.requisition.VendorRecruiterUpdateDto;
import com.fourdotfive.missioncontrol.pojo.FourDotFiveResponse;
import com.fourdotfive.missioncontrol.pojo.job.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface JobService {
    /**
     * To get Jobs by company Id.
     */
    List<NewJobDto> getJobsBasedOnLoggedInUser(JobRequestDto jobRequestDto, String companyId, String userId);

    Job updateJobStatus(JobState jobState);

    String getStatusTypes();

    FilterResponse getClientAndRecruitersByRoles(JobRequestDto jobRequestDto, String companyId, String userId);

    FilterResponse getClientAndRoleByRecruiters(JobRequestDto jobRequestDto, String companyId, String userId);

    FilterResponse getRoleAndRecruitersByClients(JobRequestDto jobRequestDto, String companyId, String userId);

    List<String> searchByReqNumber(String searchText, String companyId, String userId);

    String searchJobByText(JobRequestDto jobRequestDto, String companyId, String userId);

    List<JobMatchCandidateDto> jobMatchByCandidate(CandidateJobMatchRequestDTO candidateJobMatchRequestDTO, Integer page, Integer size, String column, String sortDirection);

    FourDotFiveResponse<JobResponseDto> createJob(JobCreationModel jobCreationModel, String companyId, String clientOrBuid);

    FourDotFiveResponse<JobResponseDto> updateJob(JobCreationModel jobCreationModel, String companyId, String userId, String clientOrBuid);

    FourDotFiveResponse<HashMap<String, Object>> getMoreJobBoardCandidatesForJob(GetJBCandidatesModel getJBCandiatesModel);

    String getCandidateJobComparisonByJobMatchId(String jobMatchId);

    String getJobDetailsById(String jobId);

    String getMultipleCandidateProfileSingleJobComparison(List<String> candidateIds, String jobId);

    String getMultipleJobSingleCandidateProfileComparison(List<String> jobIds, String candidateId);

    RequisitionStatusResponse getCandidatesForRequisitionStatus(String requisitionId, String requisitionStatus);

    List<JobOpening> getCandidatesForCurrentState(String requisitionId, String requisitionStatus);
    
    String getActivityTypes();

    String getEnabledSteps(String jobId);

    String getDashboardCountByJobState(String companyid, String userid);

    List<JobMinDetailsDto> getJobsByActivities(String companyId, String userId);

    String updateFourDotFiveIntelligenceScore(JobScoreDTO jobScoreDTO);

    String updateLocation(JobLocationUpdateDTO jobLocationUpdateDTO);

    String deleteJob(DeleteDTO jobDeleteDTO);

    FourDotFiveResponse<SearchInterfaceResult> getSearchInterface(SearchInterfaceRequest searchInterfaceRequest);

    Job saveDaxtraParameters(String jobId, Map<String, String> daxtraParameters);

    CandidateCount getCandidateCountByRequisitionId(String jobId, String jbiTransactionId);

    Boolean isGetMoreResumesInProgress(String jobId);

    List<CandidateRunScoreResponseDto> getCandidateJobMatchInfoByJbiTransactionId(String jbiTransactionId);

    List<JobBoardSearchHistory> getJobBoardSearchHistory(String jobId, int page, int size, String sort, String sortDir);

    JobBoardCredentialDto addJobBoardCredentials(JobBoardCredentialModel jobBoardCredentialModel);

    DeleteAccountResponse deleteJobBoardCredentials(String accountToken, String companyId);

    Job removeDaxtraParameters(String jobId);

    List<String> getRoles(String searchText, String companyId, String userId);

    Boolean jobBoardSearchAvailabilityCheck();

    String isSharedRequisition(String requisitionId);

    String assignVendorRecruiters(VendorRecruiterUpdateDto vendorRecruiterUpdate);

    String copyRequisition(String requisitionId, String companyId);

    String getReleasedJobDetails(String candidateId);

    String getLastAddedCandidateSourceTypeId(String jobId);

    Job findJobById(String jobId);

    String getPointOfContact(String jobId);

    String getSentAssessmentDetailsByAssessmentId(String assessmentId, String jobId);

    List<JobDetailsResponseDTO> getRequisitionsByAssement(JobAssessmentRequestDTO jobAssessmentRequestDTO);

    void saveCandidateScoreOnVendorAssociation(List<String> vendorIds, String jobId);

    String getSharedJobWorkflowDetails(String jobId);

}
