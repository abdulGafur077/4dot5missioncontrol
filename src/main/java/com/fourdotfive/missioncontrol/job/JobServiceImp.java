package com.fourdotfive.missioncontrol.job;

import com.fourdotfive.missioncontrol.common.AccessUtil;
import com.fourdotfive.missioncontrol.dtos.candidate.DeleteDTO;
import com.fourdotfive.missioncontrol.dtos.job.*;
import com.fourdotfive.missioncontrol.dtos.requisition.VendorRecruiterUpdateDto;
import com.fourdotfive.missioncontrol.dtos.user.UserManagerDTO;
import com.fourdotfive.missioncontrol.dtos.workflowstep.WorkflowStepDTO;
import com.fourdotfive.missioncontrol.platformexcecutor.PlatformApiExecutorImp;
import com.fourdotfive.missioncontrol.pojo.FourDotFiveResponse;
import com.fourdotfive.missioncontrol.pojo.job.*;
import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.requisition.RequisitionService;
import com.fourdotfive.missioncontrol.user.UserService;
import com.fourdotfive.missioncontrol.workflowstep.WorkflowStepService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class JobServiceImp implements JobService {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(JobServiceImp.class);

    @Autowired
    private PlatformApiExecutorImp platformApiExecutorImp;

    @Autowired
    private JobApiExecutor jobApiExecutor;

    @Autowired
    private UserService userService;

    @Autowired
    private AccessUtil accessUtil;

    @Autowired
    private RequisitionService requisitionService;

    @Autowired
    private WorkflowStepService workflowStepService;


    private List<NewJobDto> getFilteredJobs(String companyId, String userId, JobRequestDto jobRequestDto) {
        List<NewJobDto> response = jobApiExecutor.getFilteredJobs(companyId, userId, jobRequestDto);
        return response;
    }

    private List<NewJobDto> getJobsByCompanyId(JobRequestDto jobRequestDto, String companyId, String userId) {
        List<NewJobDto> response = jobApiExecutor.getJobsByCompanyId(jobRequestDto, companyId, userId);
        return response;
    }

    @Override
    public List<NewJobDto> getJobsBasedOnLoggedInUser(JobRequestDto jobRequestDto, String companyId, String userId) {
        //User userInfo = userService.getUserById(userId);
        List<NewJobDto> response = null;
        User user = userService.getUserById(userId);
        if (user.getRole() != null) {
            if (accessUtil.isSuperUser(user.getRole().getId())
                    || accessUtil.isFourDot5Admin(user.getRole())
                    || accessUtil.isCompanyAdmin(user.getRole())) {
                response = getJobsByCompanyId(jobRequestDto, companyId, userId);
            } else {

                //JobRequestDto jobRequestDto = new JobRequestDto();
                List<String> userList = new ArrayList<>();
                userList.add(userId);
                jobRequestDto.setScopeUsers(userList);
                response = getFilteredJobs(companyId, userId, jobRequestDto);
            }
        }
        if (response != null) {
            requisitionService.setAutoMatchedLicencePreference(response, companyId);
        }
        return response;

    }

    @Override
    public Job updateJobStatus(JobState jobState) {
        String loggedInUserId = userService.getCurrentLoggedInUser().getId();
        jobState.setModifiedBy(loggedInUserId);
        Job response = jobApiExecutor.updateJobStatus(jobState);
        return response;
    }

    @Override
    public String getStatusTypes() {
        String response = jobApiExecutor.getStatusTypes();
        return response;
    }

    @Override
    public FilterResponse getClientAndRecruitersByRoles(JobRequestDto jobRequestDto, String companyId, String userId) {
        FilterResponse response = null;
        User user = userService.getUserById(userId);
        if (user.getRole() != null) {
            if (accessUtil.isSuperUser(user.getRole().getId())
                    || accessUtil.isFourDot5Admin(user.getRole())
                    || accessUtil.isCompanyAdmin(user.getRole())) {
                response = jobApiExecutor.getClientAndRecruitersOfCompanyByRoles(jobRequestDto, companyId);
            } else {

                //JobRequestDto jobRequestDto = new JobRequestDto();
                List<String> userList = new ArrayList<>();
                userList.add(userId);
                jobRequestDto.setScopeUsers(userList);
                response = jobApiExecutor.getClientAndRecruitersByRoles(jobRequestDto);

            }
        } else {
            response = null;
        }

        return response;
    }

    @Override
    public FilterResponse getClientAndRoleByRecruiters(JobRequestDto jobRequestDto, String companyId, String userId) {
        FilterResponse response = null;
        User user = userService.getUserById(userId);
        if (user.getRole() != null) {
            if (accessUtil.isSuperUser(user.getRole().getId())
                    || accessUtil.isFourDot5Admin(user.getRole())
                    || accessUtil.isCompanyAdmin(user.getRole())) {
                response = jobApiExecutor.getClientAndRoleOfCompanyByRecruiters(jobRequestDto, companyId);
            } else {

                //JobRequestDto jobRequestDto = new JobRequestDto();
                List<String> userList = new ArrayList<>();
                userList.add(userId);
                jobRequestDto.setScopeUsers(userList);
                response = jobApiExecutor.getClientAndRoleByRecruiters(jobRequestDto);

            }
        } else {
            response = null;
        }
        return response;
    }

    @Override
    public FilterResponse getRoleAndRecruitersByClients(JobRequestDto jobRequestDto, String companyId, String userId) {
        FilterResponse response = null;
        User user = userService.getUserById(userId);
        if (user.getRole() != null) {
            if (accessUtil.isSuperUser(user.getRole().getId())
                    || accessUtil.isFourDot5Admin(user.getRole())
                    || accessUtil.isCompanyAdmin(user.getRole())) {
                response = jobApiExecutor.getRoleAndRecruitersOfCompanyByClients(jobRequestDto, companyId);
            } else {

                //JobRequestDto jobRequestDto = new JobRequestDto();
                List<String> userList = new ArrayList<>();
                userList.add(userId);
                jobRequestDto.setScopeUsers(userList);
                response = jobApiExecutor.getRoleAndRecruitersByClients(jobRequestDto);

            }
        } else {
            response = null;
        }
        //FilterResponse response = jobApiExecutor.getRoleAndRecruitersByClients(jobRequestDto);
        return response;
    }

    @Override
    public List<String> searchByReqNumber(String searchText, String companyId, String userId) {
        List<String> response = null;
        User user = userService.getUserById(userId);
        if (user.getRole() != null) {
            if (accessUtil.isSuperUser(user.getRole().getId())
                    || accessUtil.isFourDot5Admin(user.getRole())
                    || accessUtil.isCompanyAdmin(user.getRole())) {
                response = jobApiExecutor.searchByReqNumberCompany(searchText, companyId);
            } else {
                List<String> userList = new ArrayList<>();
                userList.add(userId);
                response = jobApiExecutor.searchByReqNumber(searchText, companyId, userList);
            }
        } else {
            response = null;
        }
        return response;

    }

    @Override
    public String searchJobByText(JobRequestDto jobRequestDto, String companyId, String userId) {
        String response;
        User user = userService.getUserById(userId);
        if (user.getRole() != null) {
            if (accessUtil.isSuperUser(user.getRole().getId())
                    || accessUtil.isFourDot5Admin(user.getRole())
                    || accessUtil.isCompanyAdmin(user.getRole())) {
                response = jobApiExecutor.searchJobInCompanyByText(jobRequestDto, companyId, userId);
            } else {
                List<String> userList = new ArrayList<>();
                userList.add(userId);
                jobRequestDto.setScopeUsers(userList);
                response = jobApiExecutor.searchJobByText(jobRequestDto, companyId, userId);
            }
        } else {
            response = null;
        }
        return response;
    }

    @Override
    public List<JobMatchCandidateDto> jobMatchByCandidate(CandidateJobMatchRequestDTO candidateJobMatchRequestDTO, Integer page, Integer size, String column, String sortDirection) {
        User user = userService.getCurrentLoggedInUser();
        String userId = user.getId();
        candidateJobMatchRequestDTO.setUserId(userId);
        return jobApiExecutor.jobMatchByCandidate(candidateJobMatchRequestDTO, page, size, column, sortDirection);
    }

    public FourDotFiveResponse<JobResponseDto> createJob(JobCreationModel jobCreationModel, String companyId, String clientOrBuid) {
        String loggedInUserId = userService.getCurrentLoggedInUser().getId();
        jobCreationModel.setUserId(loggedInUserId);
        return jobApiExecutor.createJob(jobCreationModel, companyId, clientOrBuid);
    }

    public FourDotFiveResponse<JobResponseDto> updateJob(JobCreationModel jobCreationModel, String companyId, String userId, String clientOrBuid) {
        String loggedInUserId = userService.getCurrentLoggedInUser().getId();
        jobCreationModel.setUserId(loggedInUserId);
        Job oldJob = findJobById(jobCreationModel.getJobId());
        FourDotFiveResponse<JobResponseDto> jobResponseDtoFourDotFiveResponse = jobApiExecutor.updateJob(jobCreationModel, companyId, userId, clientOrBuid);
        if (jobCreationModel.getWorkflowStepsOrder() != null) {
            WorkflowStepDTO workflowStepDTO = new WorkflowStepDTO();
            workflowStepDTO.setCompanyId(companyId);
            workflowStepDTO.setJobId(jobCreationModel.getJobId());
            workflowStepDTO.setWorkflowStepsOrder(jobCreationModel.getWorkflowStepsOrder());
            workflowStepDTO.setUserId(loggedInUserId);
            if (oldJob != null && oldJob.getLicensePreferences() != null) {
                workflowStepDTO.setOldJobLicensePreferences(oldJob.getLicensePreferences());
            }
            workflowStepService.saveWorkflowStepsByRequisitionId(workflowStepDTO);
        }
        return jobResponseDtoFourDotFiveResponse;
    }

    @Override
    public FourDotFiveResponse<HashMap<String, Object>> getMoreJobBoardCandidatesForJob(GetJBCandidatesModel getJBCandidatesModel) {
        return jobApiExecutor.getMoreJobBoardCandidatesForJob(getJBCandidatesModel);
    }

    public String getCandidateJobComparisonByJobMatchId(String jobMatchId) {
        return jobApiExecutor.getCandidateJobComparisonByJobMatchId(jobMatchId);
    }

    public String getJobDetailsById(String jobId) {
        return jobApiExecutor.getJobDetailsById(jobId);
    }

    public String getMultipleCandidateProfileSingleJobComparison(List<String> candidateIds, String jobId) {
        return jobApiExecutor.getMultipleCandidateProfileSingleJobComparison(candidateIds, jobId);
    }

    public String getMultipleJobSingleCandidateProfileComparison(List<String> jobIds, String candidateId) {
        return jobApiExecutor.getMultipleJobSingleCandidateProfileComparison(jobIds, candidateId);
    }

    @Override
    public RequisitionStatusResponse getCandidatesForRequisitionStatus(String requisitionId, String requisitionStatus) {
        return jobApiExecutor.getCandidatesForRequisitionStatus(requisitionId, requisitionStatus);
    }

    @Override
    public List<JobOpening> getCandidatesForCurrentState(String requisitionId, String requisitionStatus) {
        return jobApiExecutor.getCandidatesForCurrentState(requisitionId, requisitionStatus);
    }

    @Override
    public String getActivityTypes() {
        String response = jobApiExecutor.getActivityTypes();
        return response;
    }

    @Override
    public String getEnabledSteps(String jobId) {
        return jobApiExecutor.getEnabledSteps(jobId);
    }

    @Override
    public String getDashboardCountByJobState(String companyId, String userId) {
        User user = userService.getUserById(userId);
        String response;
        if (user.getRole() != null) {
            if (accessUtil.isSuperUser(user.getRole().getId())
                    || accessUtil.isFourDot5Admin(user.getRole())
                    || accessUtil.isCompanyAdmin(user.getRole())) {
                response = jobApiExecutor.getDashboardCountByJobStateForCompany(companyId);
            } else {
                List<String> userList = new ArrayList<>();
                userList.add(userId);
                response = jobApiExecutor.getDashboardCountByJobStateForUser(userId);
            }
        } else {
            response = null;
        }
        return response;
    }

    @Override
    public List<JobMinDetailsDto> getJobsByActivities(String companyId, String userId) {
        List<JobMinDetailsDto> response = null;
        User user = userService.getUserById(userId);
        if (user.getRole() != null) {
            if (accessUtil.isSuperUser(user.getRole().getId())
                    || accessUtil.isFourDot5Admin(user.getRole())
                    || accessUtil.isCompanyAdmin(user.getRole())) {
                response = jobApiExecutor.getJobsByActivitiesForCompany(companyId, userId);
            } else {
                response = jobApiExecutor.getJobsByActivitiesForUser(companyId, userId);

            }
        }
        return response;
    }

    @Override
    public String updateFourDotFiveIntelligenceScore(JobScoreDTO jobScoreDTO) {
        return jobApiExecutor.updateFourDotFiveIntelligenceScore(jobScoreDTO);
    }

    @Override
    public String updateLocation(JobLocationUpdateDTO jobLocationUpdateDTO) {
        String userId = userService.getCurrentLoggedInUser().getId();
        return jobApiExecutor.updateLocation(jobLocationUpdateDTO, userId);
    }

    @Override
    public String deleteJob(DeleteDTO jobDeleteDTO) {
        return jobApiExecutor.deleteJob(jobDeleteDTO);
    }

    @Override
    public FourDotFiveResponse<SearchInterfaceResult> getSearchInterface(SearchInterfaceRequest searchInterfaceRequest) {
        return jobApiExecutor.getSearchInterface(searchInterfaceRequest);
    }

    @Override
    public Job saveDaxtraParameters(String jobId, Map<String, String> daxtraParameters) {
        return jobApiExecutor.saveDaxtraParameters(jobId, daxtraParameters);
    }

    @Override
    public CandidateCount getCandidateCountByRequisitionId(String jobId, String jbiTransactionId) {
        return jobApiExecutor.getCandidateCountByRequisitionId(jobId, jbiTransactionId);
    }

    @Override
    public Boolean isGetMoreResumesInProgress(String jobId) {
        return jobApiExecutor.isGetMoreResumesInProgress(jobId);
    }

    @Override
    public List<CandidateRunScoreResponseDto> getCandidateJobMatchInfoByJbiTransactionId(String jbiTransactionId) {
        return jobApiExecutor.getCandidateJobMatchInfoByJbiTransactionId(jbiTransactionId);
    }

    @Override
    public List<JobBoardSearchHistory> getJobBoardSearchHistory(String jobId, int page, int size, String sort, String sortDir) {
        return jobApiExecutor.getJobBoardSearchHistory(jobId, page, size, sort, sortDir);
    }

    @Override
    public JobBoardCredentialDto addJobBoardCredentials(JobBoardCredentialModel jobBoardCredentialModel) {
        return jobApiExecutor.addJobBoardCredentials(jobBoardCredentialModel);
    }

    @Override
    public DeleteAccountResponse deleteJobBoardCredentials(String accountToken, String companyId) {
        return jobApiExecutor.deleteJobBoardCredentials(accountToken, companyId);
    }

    @Override
    public Job removeDaxtraParameters(String jobId) {
        return jobApiExecutor.removeDaxtraParameters(jobId);
    }

    @Override
    public List<String> getRoles(String searchText, String companyId, String userId) {
        List<String> jobNames = null;
        User user = userService.getUserById(userId);
        if (user.getRole() != null) {
            if (accessUtil.isSuperUser(user.getRole().getId())
                    || accessUtil.isFourDot5Admin(user.getRole())
                    || accessUtil.isCompanyAdmin(user.getRole())) {
                jobNames = jobApiExecutor.getJobNamesByCompany(searchText, companyId);
            } else {
                jobNames = jobApiExecutor.getJobNamesByUser(searchText, userId, companyId);
            }
        }
        return jobNames;
    }

    @Override
    public Boolean jobBoardSearchAvailabilityCheck() {
        return jobApiExecutor.jobBoardSearchAvailabilityCheck();
    }

    @Override
    public String isSharedRequisition(String requisitionId) {
        return jobApiExecutor.isSharedRequisition(requisitionId);
    }

    @Override
    public String assignVendorRecruiters(VendorRecruiterUpdateDto vendorRecruiterUpdate) {
        User user = userService.getCurrentLoggedInUser();
        vendorRecruiterUpdate.setLoggedInUserId(user.getId());
        return jobApiExecutor.assignVendorRecruiters(vendorRecruiterUpdate);
    }

    @Override
    public String copyRequisition(String requisitionId, String companyId) {
        String userId = userService.getCurrentLoggedInUser().getId();
        return jobApiExecutor.copyRequisition(requisitionId, companyId, userId);
    }

    @Override
    public String getReleasedJobDetails(String candidateId) {
        return jobApiExecutor.getReleasedJobDetails(candidateId);
    }

    @Override
    public String getLastAddedCandidateSourceTypeId(String jobId) {
        return jobApiExecutor.getLastAddedCandidateSourceTypeId(jobId);
    }

    @Override
    public Job findJobById(String jobId) {
        return jobApiExecutor.findJobById(jobId);
    }

    @Override
    public String getPointOfContact(String jobId) {
        return jobApiExecutor.getPointOfContact(jobId);
    }

    @Override
    public String getSentAssessmentDetailsByAssessmentId(String assessmentId, String jobId) {
        return jobApiExecutor.getSentAssessmentDetailsByAssessmentId(assessmentId, jobId);
    }

    @Override
    public List<JobDetailsResponseDTO> getRequisitionsByAssement(JobAssessmentRequestDTO jobAssessmentRequestDTO) {

        List<JobDetailsDTO> jobDetailsDTOS = jobApiExecutor.getRequisitionsByAssement(jobAssessmentRequestDTO);
        List<JobDetailsResponseDTO> jobDetailsResponseDTOS = new ArrayList<>();

        for (JobDetailsDTO jobDetailsDTO : jobDetailsDTOS) {
            List<UserManagerDTO> recruiters = new ArrayList<>();
            List<UserManagerDTO> hiringManagers = new ArrayList<>();
            JobDetailsResponseDTO jobDetailsResponseDTO = new JobDetailsResponseDTO(jobDetailsDTO);
            if (!CollectionUtils.isEmpty(jobDetailsDTO.getRecruiters())) {
                for(User user : jobDetailsDTO.getRecruiters()){
                    UserManagerDTO userManagerDTO = new UserManagerDTO(user);
                    recruiters.add(userManagerDTO);
                }
            }
            if (!CollectionUtils.isEmpty(jobDetailsDTO.getHiringManagers())) {
                for(User user : jobDetailsDTO.getHiringManagers()){
                    UserManagerDTO userManagerDTO = new UserManagerDTO(user);
                    hiringManagers.add(userManagerDTO);
                }
            }
            jobDetailsResponseDTO.setHiringManagers(hiringManagers);
            jobDetailsResponseDTO.setRecruiters(recruiters);
            jobDetailsResponseDTOS.add(jobDetailsResponseDTO);
        }

        return jobDetailsResponseDTOS;
    }

    @Override
    public void saveCandidateScoreOnVendorAssociation(List<String> vendorIds, String jobId) {
        jobApiExecutor.saveCandidateScoreOnVendorAssociation(vendorIds, jobId);
    }

    @Override
    public String getSharedJobWorkflowDetails(String jobId) {
        return jobApiExecutor.getSharedJobWorkflowDetails(jobId);
    }
}
