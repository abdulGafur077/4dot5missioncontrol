package com.fourdotfive.missioncontrol.jobmatch;

import com.fourdotfive.missioncontrol.dtos.candidate.AssociateCandidateRuleInfoRequest;
import com.fourdotfive.missioncontrol.dtos.candidate.CandidateRequisitionInfoDto;
import com.fourdotfive.missioncontrol.dtos.candidate.CandidateWorkflowCardDto;
import com.fourdotfive.missioncontrol.dtos.jobmatch.*;
import com.fourdotfive.missioncontrol.dtos.pdf.FileDto;
import com.fourdotfive.missioncontrol.exception.PlatformException;
import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.user.UserService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/jobmatch")
public class JobMatchController {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(JobMatchController.class);
    @Autowired
    private JobMatchService jobMatchService;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/jobmatchcardview", method = RequestMethod.POST)
    public ResponseEntity<JobMatchWorkflowDto> getJobMatchesUser(
            @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
            @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
            @RequestParam(value = "requisitionId", required = false, defaultValue = "") String requisitionId,
            @RequestParam("userId") String userId, @RequestParam("companyId") String companyId,
            @RequestBody JobMatchFilterDto jobMatchFilter) {

        LOGGER.info("user id :" + userId + " requisition id :" + requisitionId + " company id :" + companyId);
        JobMatchWorkflowDto jobMatchViewWorkflowDto = jobMatchService.getWorkFlowJobMatches(page, size, companyId, userId, requisitionId, jobMatchFilter);
        return new ResponseEntity<>(jobMatchViewWorkflowDto, HttpStatus.OK);

    }

    @RequestMapping(value = "/candidateworkflowcard/{jobmatchid}/{companyid}", method = RequestMethod.GET)
    public String getJobMatchesCompany(
            @PathVariable("jobmatchid") String jobMatchId,
            @PathVariable("companyid") String companyId) {
        return jobMatchService.setCandidateWorkflowCard(jobMatchId, companyId);
    }

    @RequestMapping(value = "/contactandnotes/{jobmatchid}", method = RequestMethod.GET)
    public ResponseEntity<List<ContactsNotesDto>> getContactNotes(@PathVariable("jobmatchid") String jobmatchid,
                                                                  @RequestParam(value = "companyId") String companyId) {
        List<ContactsNotesDto> contactNotes = jobMatchService.getContactNoteDetails(jobmatchid, companyId);
        LOGGER.info("contact and notes [] : " + contactNotes.toString() + "size : " + contactNotes.size());
        return new ResponseEntity<>(contactNotes, HttpStatus.OK);
    }

    @RequestMapping(value = "/notesandcommunications/save/{jobmatchid}", method = RequestMethod.POST)
    public ResponseEntity<List<ContactsNotesDto>> saveNote(
            @RequestBody ContactsNotesDto contactNote) {
        List<ContactsNotesDto> contactNotes = jobMatchService.saveNote(contactNote);
        return new ResponseEntity<>(contactNotes, HttpStatus.OK);
    }

    @RequestMapping(value = "/notesandcommunications/delete/{jobmatchId}/{noteid}", method = RequestMethod.DELETE)
    public ResponseEntity<List<ContactsNotesDto>> deleteNote(@PathVariable("noteid") String noteid,
                                                             @PathVariable("jobmatchId") String jobmatchId,
                                                             @RequestParam(value = "companyId") String companyId) {
        User user = userService.getCurrentLoggedInUser();
        DeleteNoteDto deleteNoteDto = new DeleteNoteDto(noteid, jobmatchId, user.getId(), companyId);
        List<ContactsNotesDto> contactNotes = jobMatchService.deleteNote(deleteNoteDto);

        return new ResponseEntity<>(contactNotes, HttpStatus.OK);
    }

    @RequestMapping(value = "/getphonescreeners/{jobmatchid}/{companyId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getPhoneScreeners(@PathVariable(value = "jobmatchid") String jobMatchId,
                                    @PathVariable(value = "companyId") String companyId,
                                    @RequestParam(value = "isCancelledMeetingScheduleRequired", required = false, defaultValue = "false") boolean isCancelledMeetingScheduleRequired) {
        if (StringUtils.isEmpty(jobMatchId)) {
            throw new IllegalArgumentException("JobMatchId is not found");
        }
        return jobMatchService.getPhoneScreeners(jobMatchId, isCancelledMeetingScheduleRequired, companyId);
    }

    @RequestMapping(value = "/getinterviewers/{jobmatchid}/{companyId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getPhoneScreens(@PathVariable(value = "jobmatchid") String jobMatchId,
                                  @PathVariable(value = "companyId") String companyId,
                                  @RequestParam(value = "isCancelledMeetingScheduleRequired", required = false, defaultValue = "false") boolean isCancelledMeetingScheduleRequired) {
        if (StringUtils.isEmpty(jobMatchId)) {
            throw new IllegalArgumentException("JobMatchId is not found");
        }
        return jobMatchService.getInterviewers(jobMatchId, isCancelledMeetingScheduleRequired, companyId);
    }

    @RequestMapping(value = "/getmeetingscheduledetails/{meetingscheduleid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getMeetingScheduleDetails(@PathVariable(value = "meetingscheduleid") String meetingScheduleId) {
        if (StringUtils.isEmpty(meetingScheduleId)) {
            throw new IllegalArgumentException("meetingScheduleId is not found");
        }
        return jobMatchService.getMeetingScheduleDetails(meetingScheduleId);
    }

    @RequestMapping(value = {"/createjobmatches"}, method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<String> createJobMatches(@RequestBody CandidateRequisitionInfoDto candidateRequisitionInfoDto) {
        String candidateScoreDetails = jobMatchService.createJobMatches(candidateRequisitionInfoDto);
        return new ResponseEntity<>(candidateScoreDetails, HttpStatus.OK);
    }

    @RequestMapping(value = {"/getallcandidateworkflowcardstatus"}, method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<String> getAllCandidateJobMatchCardStatus() {
        String candidateJobMatchCardStatus = jobMatchService.getAllCandidateWorkflowCardStatus();
        return new ResponseEntity<>(candidateJobMatchCardStatus, HttpStatus.OK);
    }

    @RequestMapping(value = {"/updatejobmatchstate/{jobId}/{companyId}"}, method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<String> updateJobmatchState(@PathVariable(value = "jobId") String jobId,
                                               @PathVariable(value = "companyId") String companyId) {
        if (StringUtils.isBlank(jobId) || StringUtils.isBlank(companyId))
            return new ResponseEntity<>("", HttpStatus.INTERNAL_SERVER_ERROR);

        String status = jobMatchService.updateJobMatchState(jobId, companyId);
        return new ResponseEntity<>(status, HttpStatus.OK);
    }

    @RequestMapping(value = {"/getallcanceledmeetings/{jobMatchId}"}, method = RequestMethod.GET)
    public @ResponseBody
    String getAllCanceledMeetings(@PathVariable(value = "jobMatchId") String jobMatchId) {
        return jobMatchService.getAllCanceledMeetings(jobMatchId);
    }

    @RequestMapping(value = "getinterviewfeedbackreport/{jobMatchId}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getInterviewFeedbackReport(@PathVariable String jobMatchId) throws IOException {
        FileDto fileDto = jobMatchService.getInterviewFeedbackReport(jobMatchId);
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_PDF)
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileDto.getFileName())
                .body(fileDto.getFileData());
    }

    @RequestMapping(value = "getphonescreenfeedbackreport/{jobMatchId}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getPhoneScreenFeedbackReport(@PathVariable String jobMatchId) throws IOException {
        FileDto fileDto = jobMatchService.getPhoneScreenFeedbackReport(jobMatchId);
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_PDF)
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileDto.getFileName())
                .body(fileDto.getFileData());
    }

    @RequestMapping(value = "getscreeningfeedbackreport/{jobMatchId}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getRecruiterScreeningReport(@PathVariable String jobMatchId) throws IOException {
        FileDto fileDto = jobMatchService.getRecruiterScreeningReport(jobMatchId);
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_PDF)
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileDto.getFileName())
                .body(fileDto.getFileData());
    }

    @RequestMapping(value = "getreportavailability/{jobMatchId}", method = RequestMethod.GET)
    ResponseEntity<Map<String, Map<String, Boolean>>> getReportAvailabilityMap(@PathVariable("jobMatchId") String jobMatchId) {
        Map<String, Map<String, Boolean>> reportAvailabilityMap = jobMatchService.getReportAvailabilityMap(jobMatchId);
        return new ResponseEntity<>(reportAvailabilityMap, HttpStatus.OK);
    }

    @RequestMapping(value = {"getreasoncodes"}, method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<String> getReasonCodes(@RequestBody List<ReasonCodeAction> actionTypes) {
        if (CollectionUtils.isEmpty(actionTypes))
            throw new PlatformException("Action types can't be empty.");

        String reasons = jobMatchService.getReasonCodes(actionTypes);
        return new ResponseEntity<>(reasons, HttpStatus.OK);
    }

    @RequestMapping(value = {"savereleaseresons"}, method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<String> saveResonCodes(@RequestBody ReasonNoteDTO reasonNoteDTO) {
        String result = jobMatchService.saveReasonCodes(reasonNoteDTO);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = {"getemaildeliveryhistory/{jobMatchId}"}, method = RequestMethod.GET)
    public @ResponseBody
    String getEmailDeliveryHistory(@PathVariable("jobMatchId") String jobMatchId) {
        return jobMatchService.getEmailDeliveryHistory(jobMatchId);
    }
}
