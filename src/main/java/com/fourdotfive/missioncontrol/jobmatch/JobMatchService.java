package com.fourdotfive.missioncontrol.jobmatch;

import com.fourdotfive.missioncontrol.dtos.candidate.AssociateCandidateRuleInfoRequest;
import com.fourdotfive.missioncontrol.dtos.candidate.CandidateRequisitionInfoDto;
import com.fourdotfive.missioncontrol.dtos.jobmatch.*;
import com.fourdotfive.missioncontrol.dtos.pdf.FileDto;

import java.util.List;
import java.util.Map;

public interface JobMatchService {
    JobMatchWorkflowDto getWorkFlowJobMatches(int page, int size, String companyId, String userId, String requisitionId, JobMatchFilterDto jobMatchFilter);

    String setCandidateWorkflowCard(String jobMatchId, String companyId);

    List<ContactsNotesDto> getContactNoteDetails(String jobmatchid, String companyId);

    List<ContactsNotesDto> saveNote(ContactsNotesDto contactNote);

    List<ContactsNotesDto> deleteNote(DeleteNoteDto deleteNoteDto);

    String getPhoneScreeners(String jobMatchId, boolean isCancelledMeetingScheduleRequired, String companyId);

    String getInterviewers(String jobMatchId, boolean isCancelledMeetingScheduleRequired, String companyId);

    String getMeetingScheduleDetails(String meetingScheduleId);

    String createJobMatches(CandidateRequisitionInfoDto candidateRequisitionInfoDto);

    String getAllCandidateWorkflowCardStatus();

    String updateJobMatchState(String jobId, String companyId);

    String getAllCanceledMeetings(String jobMatchId);

    FileDto getInterviewFeedbackReport(String jobMatchId);

    FileDto getPhoneScreenFeedbackReport(String jobMatchId);

    FileDto getRecruiterScreeningReport(String jobMatchId);

    Map<String,Map<String,Boolean>> getReportAvailabilityMap(String jobMatchId);

    String getReasonCodes(List<ReasonCodeAction> reasonCodeActions);

    String saveReasonCodes(ReasonNoteDTO reasonNoteDTO);

    String getEmailDeliveryHistory(String jobMatchId);
}