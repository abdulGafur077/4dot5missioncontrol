package com.fourdotfive.missioncontrol.jobmatch;

import com.fourdotfive.missioncontrol.common.AccessUtil;
import com.fourdotfive.missioncontrol.dtos.candidate.CandidateRequisitionInfoDto;
import com.fourdotfive.missioncontrol.dtos.jobmatch.*;
import com.fourdotfive.missioncontrol.dtos.pdf.FileDto;
import com.fourdotfive.missioncontrol.platformexcecutor.PlatformApiExecutorImp;
import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class JobMatchServiceImp implements JobMatchService {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(JobMatchServiceImp.class);

    @Autowired
    private PlatformApiExecutorImp platformApiExecutorImp;

    @Autowired
    private JobMatchApiExecutor jobMatchApiExecutor;

    @Autowired
    private UserService userService;

    @Autowired
    private AccessUtil accessUtil;

    @Override
    public JobMatchWorkflowDto getWorkFlowJobMatches(int page, int size, String companyId, String userId, String requisitionId, JobMatchFilterDto jobMatchFilter) {
        User user = userService.getUserById(userId);
        JobMatchWorkflowDto response;
        if (user.getRole() != null) {
            if (accessUtil.isSuperUser(user.getRole().getId())
                    || accessUtil.isFourDot5Admin(user.getRole())
                    || accessUtil.isCompanyAdmin(user.getRole())) {
                response = jobMatchApiExecutor.getWorkFlowJobMatchesForCompany(page, size, companyId, userId, requisitionId, jobMatchFilter);
            } else {

                response = jobMatchApiExecutor.getWorkFlowJobMatchesForUser(page, size, companyId, userId, requisitionId, jobMatchFilter);

            }
        } else {
            response = null;
        }

        return response;

    }

    @Override
    public String setCandidateWorkflowCard(String jobMatchId, String companyId) {
        User user = userService.getCurrentLoggedInUser();
        String loggedInUserId = user.getId();
        return jobMatchApiExecutor.setCandidateWorkflowCard(jobMatchId, loggedInUserId, companyId);
    }

    @Override
    public List<ContactsNotesDto> getContactNoteDetails(String jobmatchId, String companyId) {
        User user = userService.getCurrentLoggedInUser();
        return jobMatchApiExecutor.getContactNoteDetails(jobmatchId, companyId, user.getId());
    }

    @Override
    public List<ContactsNotesDto> saveNote(ContactsNotesDto contactNote) {
        return jobMatchApiExecutor.saveNote(contactNote);
    }

    @Override
    public List<ContactsNotesDto> deleteNote(DeleteNoteDto deleteNoteDto) {
        return jobMatchApiExecutor.deleteNote(deleteNoteDto);

    }

    @Override
    public String getInterviewers(String jobMatchId, boolean isCancelledMeetingScheduleRequired, String companyId) {
        return jobMatchApiExecutor.getInterviewers(jobMatchId, isCancelledMeetingScheduleRequired,companyId);
    }

    @Override
    public String getPhoneScreeners(String jobMatchId, boolean isCancelledMeetingScheduleRequired, String companyId) {
        return jobMatchApiExecutor.getPhoneScreeners(jobMatchId, isCancelledMeetingScheduleRequired, companyId);
    }

    @Override
    public String getMeetingScheduleDetails(String meetingScheduleId) {
        return jobMatchApiExecutor.getMeetingScheduleDetails(meetingScheduleId);
    }

    @Override
    public String createJobMatches(CandidateRequisitionInfoDto candidateRequisitionInfoDto) {
        User user = userService.getCurrentLoggedInUser();
        candidateRequisitionInfoDto.setLoggedInUserId(user.getId());
        return jobMatchApiExecutor.createJobMatches(candidateRequisitionInfoDto);
    }

    @Override
    public String getAllCandidateWorkflowCardStatus() {
        return jobMatchApiExecutor.getAllCandidateWorkflowCardStatus();

    }

    @Override
    public String updateJobMatchState(String jobId, String companyId) {
        return jobMatchApiExecutor.updateJobMatchState(jobId, companyId);
    }

    @Override
    public String getAllCanceledMeetings(String jobMatchId) {
        return jobMatchApiExecutor.getAllCanceledMeetings(jobMatchId);
    }

    @Override
    public FileDto getInterviewFeedbackReport(String jobMatchId) {
        return jobMatchApiExecutor.getInterviewFeedbackReport(jobMatchId);
    }

    @Override
    public FileDto getPhoneScreenFeedbackReport(String jobMatchId) {
        return jobMatchApiExecutor.getPhoneScreenFeedbackReport(jobMatchId);
    }

    @Override
    public FileDto getRecruiterScreeningReport(String jobMatchId) {
        return jobMatchApiExecutor.getRecruiterScreeningReport(jobMatchId);
    }

    @Override
    public Map<String, Map<String, Boolean>> getReportAvailabilityMap(String jobMatchId) {
        User user = userService.getCurrentLoggedInUser();
        return jobMatchApiExecutor.getReportAvailabilityMap(jobMatchId, user.getId());
    }

    @Override
    public String getReasonCodes(List<ReasonCodeAction> actions) {
        List<String> actionTypes =  new ArrayList<>();
        for (ReasonCodeAction reasonCodeAction : actions) {
            actionTypes.add(reasonCodeAction.getText());
        }
        return jobMatchApiExecutor.getReasonCodes(actionTypes);
    }

    @Override
    public String saveReasonCodes(ReasonNoteDTO reasonNoteDTO) {
        return jobMatchApiExecutor.saveReasonCodes(reasonNoteDTO);
    }

    @Override
    public String getEmailDeliveryHistory(String jobMatchId) {
        return jobMatchApiExecutor.getEmailDeliveryHistory(jobMatchId);
    }
}




