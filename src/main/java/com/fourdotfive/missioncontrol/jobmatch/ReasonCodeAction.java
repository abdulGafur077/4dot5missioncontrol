package com.fourdotfive.missioncontrol.jobmatch;

public enum ReasonCodeAction {

    RECRUITER_SCREENING("Recruiter Screening"),
    TECH_ASSESSMENT("Tech Assessment"),
    VALUE_ASSESSMENT("Value Assessment"),
    PHONE_SCREEN("Phone Screen"),
    INTERVIEW("Interview"),
    REMOVE_FILL("Remove Fill"),
    REMOVE_JOIN("Remove Join"),
    JOB_LEFT("Job Left"),
    OFFER_REASONS("Offer Reasons"),
    NOT_INTERESTED("Not Interested"),
    RELEASED("Released");

    private String text;

    ReasonCodeAction(String text) {
        this.text = text;
    }

    public String getText() {
        return this.text;
    }
}
