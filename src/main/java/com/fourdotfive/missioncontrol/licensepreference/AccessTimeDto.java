package com.fourdotfive.missioncontrol.licensepreference;

public class AccessTimeDto {
    private String accessTimeLabel;
    private boolean accessTimeEnabled;
    private boolean accessTimeValue;
    private String accessTimeMessage;

    public String getAccessTimeLabel() {
        return accessTimeLabel;
    }

    public void setAccessTimeLabel(String accessTimeLabel) {
        this.accessTimeLabel = accessTimeLabel;
    }

    public boolean isAccessTimeEnabled() {
        return accessTimeEnabled;
    }

    public void setAccessTimeEnabled(boolean accessTimeEnabled) {
        this.accessTimeEnabled = accessTimeEnabled;
    }

    public boolean isAccessTimeValue() {
        return accessTimeValue;
    }

    public void setAccessTimeValue(boolean accessTimeValue) {
        this.accessTimeValue = accessTimeValue;
    }

    public String getAccessTimeMessage() {
        return accessTimeMessage;
    }

    public void setAccessTimeMessage(String accessTimeMessage) {
        this.accessTimeMessage = accessTimeMessage;
    }
}
