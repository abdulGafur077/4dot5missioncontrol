package com.fourdotfive.missioncontrol.licensepreference;


import java.util.Arrays;
import java.util.List;

public class AssessmentCredential {

    private String userName;
    private String key;
    private boolean use4Dot5Account;
    private boolean use4Dot5Credential;
    private String assessmentType;



    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public boolean getUse4Dot5Account() {
        return use4Dot5Account;
    }

    public void setUse4Dot5Account(boolean use4Dot5Account) {
        this.use4Dot5Account = use4Dot5Account;
    }

    public boolean getUse4Dot5Credential() {
        return use4Dot5Credential;
    }

    public void setUse4Dot5Credential(boolean use4Dot5Credential) {
        this.use4Dot5Credential = use4Dot5Credential;
    }

   public String getAssessmentType() {
        return assessmentType;
    }

    public void setAssessmentType(String assessmentType) {
        this.assessmentType = assessmentType;
    }


}
