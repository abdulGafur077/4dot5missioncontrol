package com.fourdotfive.missioncontrol.licensepreference;

public class JobBoardCredential {

    private String jobBoardId;
    private String sourceType;
    private String userName;
    private String key;
    private boolean useParentUserName;
    private String password;
    private boolean isDelete;
    private boolean credentialsAddedWithKey;

    public String getJobBoardId() {
        return jobBoardId;
    }

    public void setJobBoardId(String jobBoardId) {
        this.jobBoardId = jobBoardId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public boolean isUseParentUserName() {
        return useParentUserName;
    }

    public void setUseParentUserName(boolean useParentUserName) {
        this.useParentUserName = useParentUserName;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    public boolean isCredentialsAddedWithKey() {
        return credentialsAddedWithKey;
    }

    public void setCredentialsAddedWithKey(boolean credentialsAddedWithKey) {
        this.credentialsAddedWithKey = credentialsAddedWithKey;
    }
}

