package com.fourdotfive.missioncontrol.licensepreference;

import com.fourdotfive.missioncontrol.pojo.JobBoardDetails;

import java.util.List;

public class JobLicensePreferences {

    private Boolean fourDotFiveIntelligence = true;

    private Boolean techAssessment;

    private Boolean valueAssessment;

    private Boolean educationVerification;

    private Boolean experienceVerification;

    private Boolean phoneScreen;

    private Boolean interview;

    private Boolean useCorporateCredential = true;

    private String testLinkName;

    private List<String> accessTime;

    private Boolean inviteCandidateOnly = true;

    private Boolean imageProctoring;

    private List<AssessmentCredential> assessmentCredentials;

    private List<JobBoardDetails> jobBoardDetails;

    private Boolean autoMatch;

    private Integer matchCount;

    private Boolean recruiterScreening;

    private boolean addProctoringImagesToReport;

    private Integer resumesToPullPerJobBoardCount;

    public Boolean getFourDotFiveIntelligence() {
        return fourDotFiveIntelligence;
    }

    public void setFourDotFiveIntelligence(Boolean fourDotFiveIntelligence) {
        this.fourDotFiveIntelligence = fourDotFiveIntelligence;
    }

    public Boolean getTechAssessment() {
        return techAssessment;
    }

    public void setTechAssessment(Boolean techAssessment) {
        this.techAssessment = techAssessment;
    }

    public Boolean getValueAssessment() {
        return valueAssessment;
    }

    public void setValueAssessment(Boolean valueAssessment) {
        this.valueAssessment = valueAssessment;
    }

    public Boolean getEducationVerification() {
        return educationVerification;
    }

    public void setEducationVerification(Boolean educationVerification) {
        this.educationVerification = educationVerification;
    }

    public Boolean getExperienceVerification() {
        return experienceVerification;
    }

    public void setExperienceVerification(Boolean experienceVerification) {
        this.experienceVerification = experienceVerification;
    }

    public Boolean getPhoneScreen() {
        return phoneScreen;
    }

    public void setPhoneScreen(Boolean phoneScreen) {
        this.phoneScreen = phoneScreen;
    }

    public Boolean getInterview() {
        return interview;
    }

    public void setInterview(Boolean interview) {
        this.interview = interview;
    }

    public Boolean getUseCorporateCredential() {
        return useCorporateCredential;
    }

    public void setUseCorporateCredential(Boolean useCorporateCredential) {
        this.useCorporateCredential = useCorporateCredential;
    }

    public String getTestLinkName() {
        return testLinkName;
    }

    public void setTestLinkName(String testLinkName) {
        this.testLinkName = testLinkName;
    }

    public List<String> getAccessTime() {
        return accessTime;
    }

    public void setAccessTime(List<String> accessTime) {
        this.accessTime = accessTime;
    }

    public Boolean getInviteCandidateOnly() {
        return inviteCandidateOnly;
    }

    public void setInviteCandidateOnly(Boolean inviteCandidateOnly) {
        this.inviteCandidateOnly = inviteCandidateOnly;
    }

    public Boolean getImageProctoring() {
        return imageProctoring;
    }

    public void setImageProctoring(Boolean imageProctoring) {
        this.imageProctoring = imageProctoring;
    }

    public List<AssessmentCredential> getAssessmentCredentials() {
        return assessmentCredentials;
    }

    public void setAssessmentCredentials(List<AssessmentCredential> assessmentCredentials) {
        this.assessmentCredentials = assessmentCredentials;
    }

    public List<JobBoardDetails> getJobBoardDetails() {
        return jobBoardDetails;
    }

    public void setJobBoardDetails(List<JobBoardDetails> jobBoardDetails) {
        this.jobBoardDetails = jobBoardDetails;
    }

    public Boolean getAutoMatch() {
        return autoMatch;
    }

    public void setAutoMatch(Boolean autoMatch) {
        this.autoMatch = autoMatch;
    }

    public Integer getMatchCount() {
        return matchCount;
    }

    public void setMatchCount(Integer matchCount) {
        this.matchCount = matchCount;
    }

    public Boolean getRecruiterScreening() {
        return recruiterScreening;
    }

    public void setRecruiterScreening(Boolean recruiterScreening) {
        this.recruiterScreening = recruiterScreening;
    }

    public boolean isAddProctoringImagesToReport() {
        return addProctoringImagesToReport;
    }

    public void setAddProctoringImagesToReport(boolean addProctoringImagesToReport) {
        this.addProctoringImagesToReport = addProctoringImagesToReport;
    }

    public Integer getResumesToPullPerJobBoardCount() {
        return resumesToPullPerJobBoardCount;
    }

    public void setResumesToPullPerJobBoardCount(Integer resumesToPullPerJobBoardCount) {
        this.resumesToPullPerJobBoardCount = resumesToPullPerJobBoardCount;
    }
}
