package com.fourdotfive.missioncontrol.licensepreference;

import com.fourdotfive.missioncontrol.dtos.pdf.QuestionType;

import java.util.Arrays;
import java.util.List;

public class LicenseAssessmentPreferences {
    private String testLinkName;
    private List<String> accessTime;
    private List<String> accessTimeDisabledValues;
    private boolean inviteCandidateOnly  = true;
    private boolean inviteCandidateOnlyEnabled;
    private String inviteCandidateOnlyMessage;
    private int inviteCandidateOnlyMsgVal;
    private boolean imageProctoring;
    private boolean imageProctoringEnabled;
    private String imageProctoringMessage;
    private int imageProctoringMsgVal;
    private  List<AssessmentCredential> assessmentCredentials;
    private boolean addProctoringImagesToReport;
    private boolean addProctoringImagesToReportEnabled;
    private String addProctoringImagesToReportMessage;
    private List<AccessTimeDto> accessTimes;
    private List<String> accessTimeOptions =  Arrays.asList("0","1","2");
    private QuestionType assessmentReportQuestionType;


    public String getTestLinkName() {
        return testLinkName;
    }

    public void setTestLinkName(String testLinkName) {
        this.testLinkName = testLinkName;
    }

    public List<String> getAccessTime() {
        return accessTime;
    }

    public void setAccessTime(List<String>  accessTime) {
        this.accessTime = accessTime;
    }

    public boolean isInviteCandidateOnly() {
        return inviteCandidateOnly;
    }

    public void setInviteCandidateOnly(boolean inviteCandidateOnly) {
        this.inviteCandidateOnly = inviteCandidateOnly;
    }

    public boolean isImageProctoring() {
        return imageProctoring;
    }

    public void setImageProctoring(boolean imageProctoring) {
        this.imageProctoring = imageProctoring;
    }

    public  List<AssessmentCredential> getAssessmentCredentials() {
        return assessmentCredentials;
    }

    public void setAssessmentCredentials( List<AssessmentCredential> assessmentCredentials) {
        this.assessmentCredentials = assessmentCredentials;
    }

    public boolean isInviteCandidateOnlyEnabled() {
        return inviteCandidateOnlyEnabled;
    }

    public void setInviteCandidateOnlyEnabled(boolean inviteCandidateOnlyEnabled) {
        this.inviteCandidateOnlyEnabled = inviteCandidateOnlyEnabled;
    }

    public boolean isImageProctoringEnabled() {
        return imageProctoringEnabled;
    }

    public void setImageProctoringEnabled(boolean imageProctoringEnabled) {
        this.imageProctoringEnabled = imageProctoringEnabled;
    }

    public String getInviteCandidateOnlyMessage() {
        return inviteCandidateOnlyMessage;
    }

    public void setInviteCandidateOnlyMessage(String inviteCandidateOnlyMessage) {
        this.inviteCandidateOnlyMessage = inviteCandidateOnlyMessage;
    }

    public String getImageProctoringMessage() {
        return imageProctoringMessage;
    }

    public void setImageProctoringMessage(String imageProctoringMessage) {
        this.imageProctoringMessage = imageProctoringMessage;
    }

    public int getInviteCandidateOnlyMsgVal() {
        return inviteCandidateOnlyMsgVal;
    }

    public void setInviteCandidateOnlyMsgVal(int inviteCandidateOnlyMsgVal) {
        this.inviteCandidateOnlyMsgVal = inviteCandidateOnlyMsgVal;
    }

    public int getImageProctoringMsgVal() {
        return imageProctoringMsgVal;
    }

    public void setImageProctoringMsgVal(int imageProctoringMsgVal) {
        this.imageProctoringMsgVal = imageProctoringMsgVal;
    }

    public List<String> getAccessTimeDisabledValues() {
        return accessTimeDisabledValues;
    }

    public void setAccessTimeDisabledValues(List<String> accessTimeDisabledValues) {
        this.accessTimeDisabledValues = accessTimeDisabledValues;
    }

    public List<String> getAccessTimeOptions() {
        return accessTimeOptions;
    }

    public boolean isAddProctoringImagesToReport() {
        return addProctoringImagesToReport;
    }

    public void setAddProctoringImagesToReport(boolean addProctoringImagesToReport) {
        this.addProctoringImagesToReport = addProctoringImagesToReport;
    }

    public boolean isAddProctoringImagesToReportEnabled() {
        return addProctoringImagesToReportEnabled;
    }

    public void setAddProctoringImagesToReportEnabled(boolean addProctoringImagesToReportEnabled) {
        this.addProctoringImagesToReportEnabled = addProctoringImagesToReportEnabled;
    }

    public String getAddProctoringImagesToReportMessage() {
        return addProctoringImagesToReportMessage;
    }

    public void setAddProctoringImagesToReportMessage(String addProctoringImagesToReportMessage) {
        this.addProctoringImagesToReportMessage = addProctoringImagesToReportMessage;
    }

    public List<AccessTimeDto> getAccessTimes() {
        return accessTimes;
    }

    public void setAccessTimes(List<AccessTimeDto> accessTimes) {
        this.accessTimes = accessTimes;
    }

    public QuestionType getAssessmentReportQuestionType() {
        return assessmentReportQuestionType;
    }

    public void setAssessmentReportQuestionType(QuestionType assessmentReportQuestionType) {
        this.assessmentReportQuestionType = assessmentReportQuestionType;
    }
}
