package com.fourdotfive.missioncontrol.licensepreference;

import javax.servlet.http.HttpServletRequest;

import com.fourdotfive.missioncontrol.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import com.fourdotfive.missioncontrol.common.AppConstants;
import com.fourdotfive.missioncontrol.common.CommonUtil;
import com.fourdotfive.missioncontrol.common.Messages;
import com.fourdotfive.missioncontrol.common.TokenDetails;
import com.fourdotfive.missioncontrol.dtos.company.CompanyDto;

@RestController
@RequestMapping("/api/licensepref")
public class LicensePrefController {

	private static final Logger LOGGER = LoggerFactory.getLogger(LicensePrefController.class);

	@Autowired
	private LicensePrefService licensePrefService;
	@Autowired
	private UserService userService;

	@PreAuthorize("@accessControlImp.canSetLicensePreferences(principal)")
	@RequestMapping(value = "/setlicensepreferences/{companytype}/{companyid}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CompanyDto> setStaffingOrCorpLicensePreferences(
			@PathVariable(value = "companyid") String companyId,
			@PathVariable(value = "companytype") String companyType,
			@RequestBody LicensePrefDtoList dto,

			HttpServletRequest request) {

		final String authHeader = request.getHeader(AppConstants.AUTHORIZATION);
		TokenDetails tokenDetails = CommonUtil.getTokenDetails(authHeader);

		CompanyDto company = licensePrefService.setLicensePreferences(dto, companyId, tokenDetails, companyType);

		return new ResponseEntity<CompanyDto>(company, HttpStatus.OK);
	}

	@PreAuthorize("@accessControlImp.canSetLicensePreferences(principal)")
	@RequestMapping(value = "/setclientorbulicensepreferences/{parentCompId}/{childCompId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CompanyDto> setClientOrBuLicensePreferences(
			@PathVariable(value = "parentCompId") String parentCompId,
			@PathVariable(value = "childCompId") String childCompId, @RequestBody LicensePrefDtoList dto,
			HttpServletRequest request) {

		final String authHeader = request.getHeader(AppConstants.AUTHORIZATION);
		TokenDetails tokenDetails = CommonUtil.getTokenDetails(authHeader);

		CompanyDto company = licensePrefService.setClientOrBuLicensePref(dto, parentCompId, childCompId,
				tokenDetails.getRoleId(),userService.getCurrentLoggedInUser().getId());

		return new ResponseEntity<CompanyDto>(company, HttpStatus.OK);
	}

	@PreAuthorize("@accessControlImp.canGetLicensePreferences(principal)")
	@RequestMapping(value = "/getlicensepreferences/{companytype}/{companyid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LicensePrefDtoList> getStaffOrCorpLicensePreferences(
			@PathVariable(value = "companyid") String companyId,
			@PathVariable(value = "companytype") String companyType, HttpServletRequest request) {

		final String authHeader = request.getHeader(AppConstants.AUTHORIZATION);
		TokenDetails tokenDetails = CommonUtil.getTokenDetails(authHeader);

		LicensePrefDtoList response = licensePrefService.getLicensePreferences(companyId, tokenDetails, companyType);

		return new ResponseEntity<LicensePrefDtoList>(response, HttpStatus.OK);
	}

	@PreAuthorize("@accessControlImp.canGetClientLicensePreferences(principal)")
	@RequestMapping(value = "/getclientorbulicensepreferences/{parentcompid}/{childcompid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LicensePrefDtoList> getClientOrBuLicensePreferences(
			@PathVariable(value = "parentcompid") String parentcompid,
			@PathVariable(value = "childcompid") String childcompid, HttpServletRequest request) {

		LOGGER.debug("Fetching license preferences of client or Bu  - parent company id {} -  child company id {} ",
				parentcompid, childcompid);
		LicensePrefDtoList response = licensePrefService.getClientOrBuLicensePreference(parentcompid, childcompid);

		return new ResponseEntity<LicensePrefDtoList>(response, HttpStatus.OK);
	}

	@PreAuthorize("@accessControlImp.canGetClientLicensePreferences(principal)")
	@RequestMapping(value = "/compareclientorbulicensepreferences/{parentcompid}/{firstchildcompid}/{secondchildcompid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> compareClientOrBuLicensePreferences(
			@PathVariable(value = "parentcompid") String parentcompid,
			@PathVariable(value = "firstchildcompid") String firstchildcompid,
			@PathVariable(value = "secondchildcompid") String secondchildcompid, HttpServletRequest request) {

		LOGGER.debug(
				"Comparing license preferences of two client or Bu  - parent company id {} - first child company id {} - second child company id {}",
				parentcompid, firstchildcompid, secondchildcompid);
		boolean compareResult = licensePrefService.compareClientOrBuLicensePreferences(parentcompid, firstchildcompid,
				secondchildcompid);

		if (compareResult)
			return new ResponseEntity<String>(HttpStatus.OK);
		else
			return new ResponseEntity<String>(Messages.PREFERENCES_NOT_EQUAL,
					HttpStatus.BAD_REQUEST);
	}
}
