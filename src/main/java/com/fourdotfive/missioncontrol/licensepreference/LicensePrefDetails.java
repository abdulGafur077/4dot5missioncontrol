package com.fourdotfive.missioncontrol.licensepreference;

public class LicensePrefDetails {

	private LicensePrefEnum licensePrefEnum;
	private boolean isSelected;
	private boolean isEnabled;
	private String message;
	private int msgVal; 
	private String name;
	private boolean canHaveValue;
	private int value;
	private double licensePrefMarkerValue;
	private String markerCompanyName;
	private boolean candidatesPresentOnStep;
	private String messageForCandidatePresent;

	public double getLicensePrefMarkerValue() {
		return licensePrefMarkerValue;
	}

	public void setLicensePrefMarkerValue(double licensePrefMarkerValue) {
		this.licensePrefMarkerValue = licensePrefMarkerValue;
	}

	public String getMarkerCompanyName() {
		return markerCompanyName;
	}

	public void setMarkerCompanyName(String markerCompanyName) {
		this.markerCompanyName = markerCompanyName;
	}

	public LicensePrefEnum getLicensePrefEnum() {
		return licensePrefEnum;
	}

	public void setLicensePrefEnum(LicensePrefEnum licensePrefEnum) {
		this.licensePrefEnum = licensePrefEnum;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public boolean isEnabled() {
		return isEnabled;
	}

	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getMsgVal() {
		return msgVal;
	}

	public void setMsgVal(int msgVal) {
		this.msgVal = msgVal;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isCanHaveValue() {
		return canHaveValue;
	}

	public void setCanHaveValue(boolean canHaveValue) {
		this.canHaveValue = canHaveValue;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public boolean isCandidatesPresentOnStep() {
		return candidatesPresentOnStep;
	}

	public void setCandidatesPresentOnStep(boolean candidatesPresentOnStep) {
		this.candidatesPresentOnStep = candidatesPresentOnStep;
	}

	public String getMessageForCandidatePresent() {
		return messageForCandidatePresent;
	}

	public void setMessageForCandidatePresent(String messageForCandidatePresent) {
		this.messageForCandidatePresent = messageForCandidatePresent;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LicensePrefDetails [licensePrefEnum=");
		builder.append(licensePrefEnum);
		builder.append(", isSelected=");
		builder.append(isSelected);
		builder.append(", isEnabled=");
		builder.append(isEnabled);
		builder.append(", message=");
		builder.append(message);
		builder.append(", name=");
		builder.append(name);
		builder.append(", msgVal=");
		builder.append(msgVal);
		builder.append(", canHaveValue=");
		builder.append(canHaveValue);
		builder.append(", value=");
		builder.append(value);
		builder.append(", licensePrefMarkerValue=");
		builder.append(licensePrefMarkerValue);
		builder.append("]");
		return builder.toString();
	}

}
