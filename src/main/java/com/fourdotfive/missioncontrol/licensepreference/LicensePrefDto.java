package com.fourdotfive.missioncontrol.licensepreference;

import java.util.List;

public class LicensePrefDto {
	
	private String name;
	
	private boolean enabled;
	
	private String message;
	
	private List<LicensePrefDetails> licensePrefDetailsList;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<LicensePrefDetails> getLicensePrefDetailsList() {
		return licensePrefDetailsList;
	}

	public void setLicensePrefDetailsList(List<LicensePrefDetails> licensePrefDetailsList) {
		this.licensePrefDetailsList = licensePrefDetailsList;
	}
	
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "LicensePrefDto [name=" + name + ", enabled=" + enabled + ", message=" + message
				+ ", licensePrefDetailsList=" + licensePrefDetailsList + "]";
	}
}
