package com.fourdotfive.missioncontrol.licensepreference;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class LicensePrefDtoList {

    private LicensePrefDto fourDotFiveIntellList;

    private LicensePrefDto recruiterScreening;

    private LicensePrefDto techAssessmentList;

    private LicensePrefDto valueAssessmentList;

    private LicensePrefDto verificationList;

    private LicensePrefDto phoneScreenList;

    private LicensePrefDto interviewList;

    private LicensePrefDto autoMatch;

    private LicenseAssessmentPreferences licenseAssessmentPreferences;

    private List<JobBoardCredential> jobBoardCredentialList;

    private boolean useCorporateCredential = true;

    private List<String> useAccountCredDetailsList = Arrays.asList("4dot5", "My Own");

    private Integer matchCount;

    private Integer resumesToPullPerJobBoardCount;

    private String daxtraDbName;

    private boolean daxtraDbNamePresentAtCompany;

    private Map<String, List<String>> workflowStepsOrder;

    private boolean addCandidateOnlyWithResume;

    private int candidatesSuspendedPeriod;

    private boolean allowOverride;

    private boolean IsMultipleRolesMatchEnabled;

    private boolean enableCompanyBranding = false;

    private String frontPageLogoS3Url;

    private boolean addSmallHeaderToReport = false;

    private String smallHeaderLogoS3Url;

    private String reportColor;

    public LicensePrefDto getFourDotFiveIntellList() {
        return fourDotFiveIntellList;
    }

    public void setFourDotFiveIntellList(LicensePrefDto fourDotFiveIntellList) {
        this.fourDotFiveIntellList = fourDotFiveIntellList;
    }

    public LicensePrefDto getTechAssessmentList() {
        return techAssessmentList;
    }

    public void setTechAssessmentList(LicensePrefDto techAssessmentList) {
        this.techAssessmentList = techAssessmentList;
    }

    public LicensePrefDto getValueAssessmentList() {
        return valueAssessmentList;
    }

    public void setValueAssessmentList(LicensePrefDto valueAssessmentList) {
        this.valueAssessmentList = valueAssessmentList;
    }

    public LicensePrefDto getVerificationList() {
        return verificationList;
    }

    public void setVerificationList(LicensePrefDto verificationList) {
        this.verificationList = verificationList;
    }

    public LicenseAssessmentPreferences getLicenseAssessmentPreferences() {
        return licenseAssessmentPreferences;
    }

    public void setLicenseAssessmentPreferences(LicenseAssessmentPreferences licenseAssessmentPreferences) {
        this.licenseAssessmentPreferences = licenseAssessmentPreferences;
    }

    public List<JobBoardCredential> getJobBoardCredentialList() {
        return jobBoardCredentialList;
    }

    public void setJobBoardCredentialList(List<JobBoardCredential> jobBoardCredentialList) {
        this.jobBoardCredentialList = jobBoardCredentialList;
    }

    public LicensePrefDto getPhoneScreenList() {
        return phoneScreenList;
    }

    public void setPhoneScreenList(LicensePrefDto phoneScreenList) {
        this.phoneScreenList = phoneScreenList;
    }

    public LicensePrefDto getInterviewList() {
        return interviewList;
    }

    public void setInterviewList(LicensePrefDto interviewList) {
        this.interviewList = interviewList;
    }

    public boolean isUseCorporateCredential() {
        return useCorporateCredential;
    }

    public void setUseCorporateCredential(boolean useCorporateCredential) {
        this.useCorporateCredential = useCorporateCredential;
    }

    public List<String> getUseAccountCredDetailsList() {
        return useAccountCredDetailsList;
    }

    public void setUseAccountCredDetailsList(List<String> useAccountCredDetailsList) {
        this.useAccountCredDetailsList = useAccountCredDetailsList;
    }

    public LicensePrefDto getAutoMatch() {
        return autoMatch;
    }

    public void setAutoMatch(LicensePrefDto autoMatch) {
        this.autoMatch = autoMatch;
    }

    public Integer getMatchCount() {
        return matchCount;
    }

    public void setMatchCount(Integer matchCount) {
        this.matchCount = matchCount;
    }

    public LicensePrefDto getRecruiterScreening() {
        return recruiterScreening;
    }

    public void setRecruiterScreening(LicensePrefDto recruiterScreening) {
        this.recruiterScreening = recruiterScreening;
    }

    public Integer getResumesToPullPerJobBoardCount() {
        return resumesToPullPerJobBoardCount;
    }

    public void setResumesToPullPerJobBoardCount(Integer resumesToPullPerJobBoardCount) {
        this.resumesToPullPerJobBoardCount = resumesToPullPerJobBoardCount;
    }

    public Map<String, List<String>> getWorkflowStepsOrder() {
        return workflowStepsOrder;
    }

    public void setWorkflowStepsOrder(Map<String, List<String>> workflowStepsOrder) {
        this.workflowStepsOrder = workflowStepsOrder;
    }

    public String getDaxtraDbName() {
        return daxtraDbName;
    }

    public void setDaxtraDbName(String daxtraDbName) {
        this.daxtraDbName = daxtraDbName;
    }

    public boolean isDaxtraDbNamePresentAtCompany() {
        return daxtraDbNamePresentAtCompany;
    }

    public void setDaxtraDbNamePresentAtCompany(boolean daxtraDbNamePresentAtCompany) {
        this.daxtraDbNamePresentAtCompany = daxtraDbNamePresentAtCompany;
    }

    public boolean isAddCandidateOnlyWithResume() {
        return addCandidateOnlyWithResume;
    }

    public void setAddCandidateOnlyWithResume(boolean addCandidateOnlyWithResume) {
        this.addCandidateOnlyWithResume = addCandidateOnlyWithResume;
    }

    @Override
    public String toString() {
        return "LicensePrefDtoList{" +
                "fourDotFiveIntellList=" + fourDotFiveIntellList +
                ", recruiterScreening=" + recruiterScreening +
                ", techAssessmentList=" + techAssessmentList +
                ", valueAssessmentList=" + valueAssessmentList +
                ", verificationList=" + verificationList +
                ", phoneScreenList=" + phoneScreenList +
                ", interviewList=" + interviewList +
                ", autoMatch=" + autoMatch +
                ", licenseAssessmentPreferences=" + licenseAssessmentPreferences +
                ", jobBoardCredentialList=" + jobBoardCredentialList +
                ", useCorporateCredential=" + useCorporateCredential +
                ", useAccountCredDetailsList=" + useAccountCredDetailsList +
                ", matchCount=" + matchCount +
                ", resumesToPullPerJobBoardCount=" + resumesToPullPerJobBoardCount +
                ", daxtraDbName='" + daxtraDbName + '\'' +
                ", daxtraDbNamePresentAtCompany=" + daxtraDbNamePresentAtCompany +
                ", workflowStepsOrder=" + workflowStepsOrder +
                ", addCandidateOnlyWithResume=" + addCandidateOnlyWithResume +
                '}';
    }

    public int getCandidatesSuspendedPeriod() {
        return candidatesSuspendedPeriod;
    }

    public void setCandidatesSuspendedPeriod(int candidatesSuspendedPeriod) {
        this.candidatesSuspendedPeriod = candidatesSuspendedPeriod;
    }

    public boolean isAllowOverride() {
        return allowOverride;
    }

    public void setAllowOverride(boolean allowOverride) {
        this.allowOverride = allowOverride;
    }

    public boolean isMultipleRolesMatchEnabled() {
        return IsMultipleRolesMatchEnabled;
    }

    public void setMultipleRolesMatchEnabled(boolean multipleRolesMatchEnabled) {
        IsMultipleRolesMatchEnabled = multipleRolesMatchEnabled;
    }

    public boolean isEnableCompanyBranding() {
        return enableCompanyBranding;
    }

    public void setEnableCompanyBranding(boolean enableCompanyBranding) {
        this.enableCompanyBranding = enableCompanyBranding;
    }

    public String getFrontPageLogoS3Url() {
        return frontPageLogoS3Url;
    }

    public void setFrontPageLogoS3Url(String frontPageLogoS3Url) {
        this.frontPageLogoS3Url = frontPageLogoS3Url;
    }

    public boolean isAddSmallHeaderToReport() {
        return addSmallHeaderToReport;
    }

    public void setAddSmallHeaderToReport(boolean addSmallHeaderToReport) {
        this.addSmallHeaderToReport = addSmallHeaderToReport;
    }

    public String getSmallHeaderLogoS3Url() {
        return smallHeaderLogoS3Url;
    }

    public void setSmallHeaderLogoS3Url(String smallHeaderLogoS3Url) {
        this.smallHeaderLogoS3Url = smallHeaderLogoS3Url;
    }

    public String getReportColor() {
        return reportColor;
    }

    public void setReportColor(String reportColor) {
        this.reportColor = reportColor;
    }
}
