package com.fourdotfive.missioncontrol.licensepreference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

@Service
public class LicensePrefDtoToLicensePref implements Converter<LicensePrefDtoList, LicensePreferences> {

    private static final Logger LOGGER = LoggerFactory.getLogger(LicensePrefDtoToLicensePref.class);

    @Override
    public LicensePreferences convert(LicensePrefDtoList source) {

        LOGGER.debug("Converting LicensePrefDto to LicensePref");

        LicensePreferences licensePref = new LicensePreferences();

        boolean fourdotfiveIntel = fetchValue(source.getFourDotFiveIntellList(), LicensePrefEnum.FOUR_DOT_FIVE_INTELLIGENCE);
        licensePref.setFourDotFiveIntelligence(fourdotfiveIntel);

        boolean techAssess = fetchValue(source.getTechAssessmentList(), LicensePrefEnum.TECH_ASSESSMENT);
        licensePref.setTechAssessment(techAssess);

        boolean valueAssess = fetchValue(source.getValueAssessmentList(), LicensePrefEnum.VALUE_ASSESSMENT);
        licensePref.setValueAssessment(valueAssess);

        boolean eduVerification = fetchValue(source.getVerificationList(), LicensePrefEnum.EDUCATION_VERIFICATION);
        licensePref.setEducationVerification(eduVerification);

        boolean expVerification = fetchValue(source.getVerificationList(), LicensePrefEnum.EXPERIENCE_VERIFICATION);
        licensePref.setExperienceVerification(expVerification);

        boolean phoneScreen = fetchValue(source.getPhoneScreenList(), LicensePrefEnum.PHONE_SCREEN);
        licensePref.setPhoneScreen(phoneScreen);

        boolean interviewScreen = fetchValue(source.getInterviewList(), LicensePrefEnum.INTERVIEW);
        licensePref.setInterview(interviewScreen);
        boolean autoMatch = fetchValue(source.getAutoMatch(), LicensePrefEnum.AUTO_MATCH);
        licensePref.setAutoMatch(autoMatch);
        boolean recruiterScreen = fetchValue(source.getRecruiterScreening(), LicensePrefEnum
                .RECRUITER_SCREENING);
        licensePref.setRecruiterScreening(recruiterScreen);
        licensePref.setTestLinkName(source.getLicenseAssessmentPreferences().getTestLinkName());
        licensePref.setAccessTime(source.getLicenseAssessmentPreferences().getAccessTime());
        licensePref.setAssessmentCredentials(source.getLicenseAssessmentPreferences().getAssessmentCredentials());
        licensePref.setInviteCandidateOnly(source.getLicenseAssessmentPreferences().isInviteCandidateOnly());
        licensePref.setImageProctoring(source.getLicenseAssessmentPreferences().isImageProctoring());
        licensePref.setAddProctoringImagesToReport(source.getLicenseAssessmentPreferences().isAddProctoringImagesToReport());
        licensePref.setJobBoardCredentialList(source.getJobBoardCredentialList());
        licensePref.setUseCorporateCredential(source.isUseCorporateCredential());
        licensePref.setResumesToPullPerJobBoardCount(source.getResumesToPullPerJobBoardCount());
        licensePref.setAccessTimes(source.getLicenseAssessmentPreferences().getAccessTimes());
        licensePref.setAssessmentReportQuestionType(source.getLicenseAssessmentPreferences().getAssessmentReportQuestionType());
        licensePref.setDaxtraDbName(source.getDaxtraDbName());
        licensePref.setCandidatesSuspendedPeriod(source.getCandidatesSuspendedPeriod());
        licensePref.setAllowOverride(source.isAllowOverride());
        licensePref.setMultipleRolesMatchEnabled(source.isMultipleRolesMatchEnabled());
        licensePref.setAddCandidateOnlyWithResume(source.isAddCandidateOnlyWithResume());
        licensePref.setEnableCompanyBranding(source.isEnableCompanyBranding());
        licensePref.setFrontPageLogoS3Url(source.getFrontPageLogoS3Url());
        licensePref.setAddSmallHeaderToReport(source.isAddSmallHeaderToReport());
        licensePref.setSmallHeaderLogoS3Url(source.getSmallHeaderLogoS3Url());
        licensePref.setReportColor(source.getReportColor());
        LOGGER.debug("License pref after conversion {} ", licensePref);

        return licensePref;
    }

    public boolean fetchValue(LicensePrefDto dto, LicensePrefEnum licensePrefEnum) {

        boolean value;
        for (LicensePrefDetails details : dto.getLicensePrefDetailsList()) {

            if (details.getLicensePrefEnum() != licensePrefEnum) {
                continue;
            }
            switch (details.getLicensePrefEnum()) {
                case FOUR_DOT_FIVE_INTELLIGENCE:
                    value = details.isSelected();
                    return value;
                case TECH_ASSESSMENT:
                    value = details.isSelected();
                    return value;
                case VALUE_ASSESSMENT:
                    value = details.isSelected();
                    return value;
                case EDUCATION_VERIFICATION:
                    value = details.isSelected();
                    return value;
                case EXPERIENCE_VERIFICATION:
                    value = details.isSelected();
                    return value;
                case PHONE_SCREEN:
                    value = details.isSelected();
                    return value;
                case INTERVIEW:
                    value = details.isSelected();
                    return value;
                case AUTO_MATCH:
                    value = details.isSelected();
                    return value;
                case RECRUITER_SCREENING:
                    value = details.isSelected();
                    return value;
                default:
                    return false;
            }
        }

        return false;
    }
}
