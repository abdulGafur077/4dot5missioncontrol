package com.fourdotfive.missioncontrol.licensepreference;

public interface LicensePrefEvaluator {

	LicensePrefDtoList evaluate(LicensePrefDtoList parent, LicensePrefDtoList child);

	boolean validate(LicensePrefDtoList curLicPref, LicensePrefDtoList newLicPref);
	
	boolean compareLicensePreferences(LicensePrefDtoList firstLicPref, LicensePrefDtoList secondLicPref);
}
