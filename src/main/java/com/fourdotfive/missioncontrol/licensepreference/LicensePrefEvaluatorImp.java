package com.fourdotfive.missioncontrol.licensepreference;

import com.fourdotfive.missioncontrol.common.CommonUtil;
import com.fourdotfive.missioncontrol.common.LicensePreferenceUtil;
import com.fourdotfive.missioncontrol.common.Messages;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class LicensePrefEvaluatorImp implements LicensePrefEvaluator {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(LicensePrefEvaluatorImp.class);

    @Autowired
    public LicensePreferenceUtil licensePrefUtil;

    @Override
    public LicensePrefDtoList evaluate(LicensePrefDtoList parent,
                                       LicensePrefDtoList child) {

        LOGGER.debug("Evaluating license preferences ");
        LicensePrefDetails pIntelDetails = licensePrefUtil.getLicensePrefDetails(parent,
                LicensePrefEnum.FOUR_DOT_FIVE_INTELLIGENCE);

        LicensePrefDto cDto = child.getFourDotFiveIntellList();
        LicensePrefDetails cIntelDetails = licensePrefUtil.getLicensePrefDetails(cDto,
                LicensePrefEnum.FOUR_DOT_FIVE_INTELLIGENCE);

        if (pIntelDetails.isSelected()) {
            cIntelDetails.setSelected(true);
            cIntelDetails.setEnabled(false);
        }
        cDto.getLicensePrefDetailsList().set(
                getIndexOf(cDto.getLicensePrefDetailsList(),
                        LicensePrefEnum.FOUR_DOT_FIVE_INTELLIGENCE),
                cIntelDetails);
        child.setFourDotFiveIntellList(cDto);

        child.setTechAssessmentList(assignLicensePref(
                parent.getTechAssessmentList(), child.getTechAssessmentList(),
                LicensePrefEnum.TECH_ASSESSMENT));

        child.setValueAssessmentList(assignLicensePref(
                parent.getValueAssessmentList(),
                child.getValueAssessmentList(),
                LicensePrefEnum.VALUE_ASSESSMENT));

        child.setVerificationList(assignLicensePref(
                parent.getVerificationList(), child.getVerificationList(),
                LicensePrefEnum.EDUCATION_VERIFICATION));

        child.setVerificationList(assignLicensePref(
                parent.getVerificationList(), child.getVerificationList(),
                LicensePrefEnum.EXPERIENCE_VERIFICATION));

        child.setPhoneScreenList(assignLicensePref(
                parent.getPhoneScreenList(), child.getPhoneScreenList(),
                LicensePrefEnum.PHONE_SCREEN));


        child.setInterviewList(assignLicensePref(
                parent.getInterviewList(), child.getInterviewList(),
                LicensePrefEnum.INTERVIEW));

        child.setAutoMatch(assignLicensePref(
                parent.getAutoMatch(), child.getAutoMatch(),
                LicensePrefEnum.AUTO_MATCH));

        child.setRecruiterScreening(assignLicensePref(
                parent.getRecruiterScreening(), child.getRecruiterScreening(),
                LicensePrefEnum.RECRUITER_SCREENING));

        if (parent.getAutoMatch() != null && !CollectionUtils.isEmpty(parent.getAutoMatch().getLicensePrefDetailsList())
                && parent.getAutoMatch().getLicensePrefDetailsList().get(0).isEnabled()
                && parent.getAutoMatch().getLicensePrefDetailsList().get(0).isSelected()
                && child.getMatchCount() == null) {
            child.setMatchCount(parent.getMatchCount());
        }

// Set based on parent for Assesment

        assignInviteCandidateOnly(parent.getLicenseAssessmentPreferences(), child.getLicenseAssessmentPreferences());
        assignImageProctoring(parent.getLicenseAssessmentPreferences(), child.getLicenseAssessmentPreferences());
        assignAccessTime(parent.getLicenseAssessmentPreferences(), child.getLicenseAssessmentPreferences());
        child.setResumesToPullPerJobBoardCount(assignResumesToPullPerJobBoardCount
                (parent.getResumesToPullPerJobBoardCount(), child.getResumesToPullPerJobBoardCount()));

        LOGGER.debug("Evaluated license preference {}", child);

        return child;
    }

    private void assignAccessTime(LicenseAssessmentPreferences pLicenseAssesmentPreferences, LicenseAssessmentPreferences cLicenseAssessmentPreferences) {

        List<String> parentAccessTime = pLicenseAssesmentPreferences.getAccessTime();
        List<String> accessTimeOptions = pLicenseAssesmentPreferences.getAccessTimeOptions();

        if (parentAccessTime != null) {
            Set<String> set1 = new HashSet<>();
            set1.addAll(parentAccessTime);

            Set<String> set2 = new HashSet<>();
            set2.addAll(accessTimeOptions);
            set2.removeAll(set1);
            cLicenseAssessmentPreferences.setAccessTimeDisabledValues(new ArrayList<>(set2));
        }
        if (cLicenseAssessmentPreferences.getAccessTime() == null) {
            cLicenseAssessmentPreferences.setAccessTime(parentAccessTime);
        }

        if (!CollectionUtils.isEmpty(pLicenseAssesmentPreferences.getAccessTimes())) {
            for (AccessTimeDto accessTimeDto : pLicenseAssesmentPreferences.getAccessTimes()) {
                if (!CollectionUtils.isEmpty(cLicenseAssessmentPreferences.getAccessTimes())) {
                    for (AccessTimeDto cAccessTimeDto : cLicenseAssessmentPreferences.getAccessTimes()) {
                        if (StringUtils.equals(cAccessTimeDto.getAccessTimeLabel(), accessTimeDto.getAccessTimeLabel())) {
                            if (accessTimeDto.isAccessTimeEnabled() && accessTimeDto.isAccessTimeValue()) {
                                cAccessTimeDto.setAccessTimeEnabled(true);
                            } else {
                                cAccessTimeDto.setAccessTimeEnabled(false);
                                String msg;
                                if (cAccessTimeDto.isAccessTimeValue()) {
                                    msg = cAccessTimeDto.getAccessTimeLabel() + Messages.LICENSE_PREF_NOT_AVAILABLE_NOW;
                                } else {
                                    msg = cAccessTimeDto.getAccessTimeLabel() + Messages.LICENSE_PREF_NOT_AVAILABLE;
                                }
                                cAccessTimeDto.setAccessTimeMessage(msg);
                            }
                        }
                    }
                }
            }
        }
    }

    private void assignInviteCandidateOnly(LicenseAssessmentPreferences pLicenseAssesmentPreferences, LicenseAssessmentPreferences cLicenseAssessmentPreferences) {
        if (pLicenseAssesmentPreferences.isInviteCandidateOnlyEnabled() && pLicenseAssesmentPreferences.isInviteCandidateOnly())
            cLicenseAssessmentPreferences.setInviteCandidateOnlyEnabled(true);
        else {
            String msg;
            if (pLicenseAssesmentPreferences.isInviteCandidateOnlyEnabled() && cLicenseAssessmentPreferences.isInviteCandidateOnly()) {
                cLicenseAssessmentPreferences.setInviteCandidateOnlyEnabled(true);
            }
            if (cLicenseAssessmentPreferences.isInviteCandidateOnlyEnabled()) {
                msg = Messages.LICENSE_PREF_NOT_AVAILABLE_NOW;
                cLicenseAssessmentPreferences.setInviteCandidateOnlyMsgVal(Messages.LICENSE_PREF_NOT_AVAILABLE_AT_THE_MOMENT_VALUE);
            } else {
                msg = Messages.LICENSE_PREF_NOT_AVAILABLE;
                cLicenseAssessmentPreferences.setInviteCandidateOnlyMsgVal(Messages.LICENSE_PREF_NOT_AVAILABLE_VALUE);
            }
            String name = "Invite Candidate Only";
            cLicenseAssessmentPreferences.setInviteCandidateOnlyMessage(String.format("%s %s", name, msg));
            cLicenseAssessmentPreferences.setInviteCandidateOnlyEnabled(false);
        }
    }


    private void assignImageProctoring(LicenseAssessmentPreferences parentLicenseAssesmentPreferences, LicenseAssessmentPreferences cLicenseAssessmentPreferences) {
        if (parentLicenseAssesmentPreferences.isImageProctoring() && parentLicenseAssesmentPreferences.isImageProctoringEnabled()) {
            cLicenseAssessmentPreferences.setImageProctoringEnabled(true);
        } else {
            String msg;
            if (parentLicenseAssesmentPreferences.isImageProctoringEnabled() && cLicenseAssessmentPreferences.isImageProctoring()) {
                cLicenseAssessmentPreferences.setImageProctoringEnabled(true);
            }
            if (cLicenseAssessmentPreferences.isImageProctoringEnabled()) {
                msg = Messages.LICENSE_PREF_NOT_AVAILABLE_NOW;
                cLicenseAssessmentPreferences.setImageProctoringMsgVal(Messages.LICENSE_PREF_NOT_AVAILABLE_AT_THE_MOMENT_VALUE);
            } else {
                msg = Messages.LICENSE_PREF_NOT_AVAILABLE;
                cLicenseAssessmentPreferences.setImageProctoringMsgVal(Messages.LICENSE_PREF_NOT_AVAILABLE_VALUE);
            }

            String name = "Image Proctoring";
            cLicenseAssessmentPreferences.setImageProctoringMessage(String.format("%s %s", name, msg));
            cLicenseAssessmentPreferences.setImageProctoringEnabled(false);
        }

        if (parentLicenseAssesmentPreferences.isAddProctoringImagesToReport() && parentLicenseAssesmentPreferences.isAddProctoringImagesToReportEnabled()) {
            cLicenseAssessmentPreferences.setAddProctoringImagesToReportEnabled(true);
        } else {
            String addProctoringImagesToReportMessage;
            if (parentLicenseAssesmentPreferences.isAddProctoringImagesToReportEnabled() && cLicenseAssessmentPreferences.isAddProctoringImagesToReport()) {
                cLicenseAssessmentPreferences.setAddProctoringImagesToReportEnabled(true);
            }
            if (cLicenseAssessmentPreferences.isAddProctoringImagesToReportEnabled()) {
                addProctoringImagesToReportMessage = Messages.LICENSE_PREF_NOT_AVAILABLE_NOW;
            } else {
                addProctoringImagesToReportMessage = Messages.LICENSE_PREF_NOT_AVAILABLE;
            }
            String addProctoringImagesToReport = "add proctoring images to report";
            cLicenseAssessmentPreferences.setAddProctoringImagesToReportMessage(String.format("%s %s", addProctoringImagesToReport, addProctoringImagesToReportMessage));
            cLicenseAssessmentPreferences.setAddProctoringImagesToReportEnabled(false);
        }
    }


    private int getIndexOf(List<LicensePrefDetails> list,
                           LicensePrefEnum licensePrefEnum) {

        int index = -1;
        for (LicensePrefDetails details : list) {
            if (details.getLicensePrefEnum() == licensePrefEnum) {
                index = list.indexOf(details);
            }
        }

        return index;
    }

    private LicensePrefDto assignLicensePref(LicensePrefDto pList,
                                             LicensePrefDto cList, LicensePrefEnum prefEnum) {

        LicensePrefDetails pDetails = licensePrefUtil.getLicensePrefDetails(pList, prefEnum);

        LicensePrefDetails cDetails = licensePrefUtil.getLicensePrefDetails(cList, prefEnum);

        if (pDetails.isSelected() && pDetails.isEnabled()) {
            cDetails.setEnabled(true);
        } else {
            String msg;
            if (cDetails.isSelected()) {
                msg = Messages.LICENSE_PREF_NOT_AVAILABLE_NOW;
                cDetails.setMsgVal(Messages.LICENSE_PREF_NOT_AVAILABLE_AT_THE_MOMENT_VALUE);
            } else {
                msg = Messages.LICENSE_PREF_NOT_AVAILABLE;
                cDetails.setMsgVal(Messages.LICENSE_PREF_NOT_AVAILABLE_VALUE);
            }
            String name = CommonUtil.formatLicensePrefName(cDetails
                    .getLicensePrefEnum());
            cDetails.setMessage(String.format("%s %s", name, msg));
            cDetails.setEnabled(false);
        }

        cList.getLicensePrefDetailsList().set(
                getIndexOf(cList.getLicensePrefDetailsList(), prefEnum),
                cDetails);

        return cList;
    }

    @Override
    public boolean validate(LicensePrefDtoList curLicPref,
                            LicensePrefDtoList newLicPref) {

        LOGGER.debug("Validating license preferences ");

        LOGGER.debug("Current license preferences- {}, new License pref {} ",
                curLicPref, newLicPref);

        LicensePrefDetails cTechAssessDetails = licensePrefUtil.getLicensePrefDetails(
                curLicPref.getTechAssessmentList(),
                LicensePrefEnum.TECH_ASSESSMENT);
        LicensePrefDetails nTechAssesDetails = licensePrefUtil.getLicensePrefDetails(
                newLicPref.getTechAssessmentList(),
                LicensePrefEnum.TECH_ASSESSMENT);
        if (!isSelectionValid(cTechAssessDetails, nTechAssesDetails)) {
            return false;
        }

        LicensePrefDetails cValueAssessDetails = licensePrefUtil.getLicensePrefDetails(
                curLicPref.getValueAssessmentList(),
                LicensePrefEnum.VALUE_ASSESSMENT);
        LicensePrefDetails nValueAssesDetails = licensePrefUtil.getLicensePrefDetails(
                newLicPref.getValueAssessmentList(),
                LicensePrefEnum.VALUE_ASSESSMENT);
        if (!isSelectionValid(cValueAssessDetails, nValueAssesDetails)) {
            return false;
        }

        LicensePrefDetails cEduVeriDetails = licensePrefUtil.getLicensePrefDetails(
                curLicPref.getVerificationList(),
                LicensePrefEnum.EDUCATION_VERIFICATION);
        LicensePrefDetails nEduVeriDetails = licensePrefUtil.getLicensePrefDetails(
                newLicPref.getVerificationList(),
                LicensePrefEnum.EDUCATION_VERIFICATION);
        if (!isSelectionValid(cEduVeriDetails, nEduVeriDetails)) {
            return false;
        }

        System.out.println("licensePrefUtil" + licensePrefUtil);
        System.out.println("hjjh" + licensePrefUtil.getLicensePrefDetails(
                curLicPref, LicensePrefEnum.EXPERIENCE_VERIFICATION));

        LicensePrefDetails cExpVeriDetails = licensePrefUtil.getLicensePrefDetails(
                curLicPref, LicensePrefEnum.EXPERIENCE_VERIFICATION);
        LicensePrefDetails nExpVeriDetails = licensePrefUtil.getLicensePrefDetails(
                newLicPref, LicensePrefEnum.EXPERIENCE_VERIFICATION);
        System.out.println("cExpVeriDetails " + cExpVeriDetails);
        System.out.println("nExpVeriDetails " + nExpVeriDetails);
        if (!isSelectionValid(cExpVeriDetails, nExpVeriDetails)) {
            return false;
        }

        return true;
    }

    private boolean isSelectionValid(LicensePrefDetails curLicPref,
                                     LicensePrefDetails newLicPef) {

        if (!curLicPref.isEnabled() && !curLicPref.isSelected()
                && newLicPef.isSelected()) {
            return false;
        }
        return true;
    }


    @Override
    public boolean compareLicensePreferences(LicensePrefDtoList firstLicPref,
                                             LicensePrefDtoList secondLicPref) {

        LicensePrefDetails firstTech = licensePrefUtil.getLicensePrefDetails(
                firstLicPref, LicensePrefEnum.TECH_ASSESSMENT);
        LicensePrefDetails secondTech = licensePrefUtil.getLicensePrefDetails(
                secondLicPref, LicensePrefEnum.TECH_ASSESSMENT);
        if (firstTech.isSelected() != secondTech.isSelected()) {
            return false;
        }

        LicensePrefDetails firstValue = licensePrefUtil.getLicensePrefDetails(
                firstLicPref, LicensePrefEnum.VALUE_ASSESSMENT);
        LicensePrefDetails secondValue = licensePrefUtil.getLicensePrefDetails(
                secondLicPref, LicensePrefEnum.VALUE_ASSESSMENT);
        if (firstValue.isSelected() != secondValue.isSelected()) {
            return false;
        }

        LicensePrefDetails firstEducation = licensePrefUtil.getLicensePrefDetails(
                firstLicPref, LicensePrefEnum.EDUCATION_VERIFICATION);
        LicensePrefDetails secondEducation = licensePrefUtil.getLicensePrefDetails(
                secondLicPref, LicensePrefEnum.EDUCATION_VERIFICATION);
        if (firstEducation.isSelected() != secondEducation.isSelected()) {
            return false;
        }

        LicensePrefDetails firstExperience = licensePrefUtil.getLicensePrefDetails(
                firstLicPref, LicensePrefEnum.EXPERIENCE_VERIFICATION);
        LicensePrefDetails secondExperience = licensePrefUtil.getLicensePrefDetails(
                secondLicPref, LicensePrefEnum.EXPERIENCE_VERIFICATION);
        if (firstExperience.isSelected() != secondExperience.isSelected()) {
            return false;
        }

        return true;
    }

    private Integer assignResumesToPullPerJobBoardCount(Integer parentResumeToPullPerJobBoardCount
            , Integer childResumesToPullPerJobBoardCount) {
        if (childResumesToPullPerJobBoardCount != null) {
            return childResumesToPullPerJobBoardCount;
        } else return parentResumeToPullPerJobBoardCount;
    }

}
