package com.fourdotfive.missioncontrol.licensepreference;

public class LicensePrefMarkerDto {

    private Double fourDotFiveIntelligenceMarkerValue;
    private Double techAssessmentMarkerValue;
    private String markerValueOfCompnany;

    /**
     * @return the fourDotFiveIntelligenceMarkerValue
     */
    public Double getFourDotFiveIntelligenceMarkerValue() {
        return fourDotFiveIntelligenceMarkerValue;
    }
    /**
     * @param fourDotFiveIntelligenceMarkerValue the fourDotFiveIntelligenceMarkerValue to set
     */
    public void setFourDotFiveIntelligenceMarkerValue(
            Double fourDotFiveIntelligenceMarkerValue) {
        this.fourDotFiveIntelligenceMarkerValue = fourDotFiveIntelligenceMarkerValue;
    }
    /**
     * @return the techAssessmentMarkerValue
     */
    public Double getTechAssessmentMarkerValue() {
        return techAssessmentMarkerValue;
    }
    /**
     * @param techAssessmentMarkerValue the techAssessmentMarkerValue to set
     */
    public void setTechAssessmentMarkerValue(Double techAssessmentMarkerValue) {
        this.techAssessmentMarkerValue = techAssessmentMarkerValue;
    }
    /**
     * @return the markerValueOfCompnany
     */
    public String getMarkerValueOfCompnany() {
        return markerValueOfCompnany;
    }
    /**
     * @param markerValueOfCompnany the markerValueOfCompnany to set
     */
    public void setMarkerValueOfCompnany(String markerValueOfCompnany) {
        this.markerValueOfCompnany = markerValueOfCompnany;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "LicensePreferenceDto [fourDotFiveIntelligenceMarkerValue="
                + fourDotFiveIntelligenceMarkerValue
                + ", techAssessmentMarkerValue=" + techAssessmentMarkerValue
                + ", markerValueOfCompnany=" + markerValueOfCompnany + "]";
    }



}

