package com.fourdotfive.missioncontrol.licensepreference;

import com.fourdotfive.missioncontrol.pojo.company.Company;

public interface LicensePrefRetriever {

	LicensePreferences getLicensePref(String compId);

	LicensePreferences getHostCompanyLicensePref();

	LicensePreferences getLicensePref(Company company);

	LicensePreferences getChildLicensePref(String staffOrCorpCompId, String clientOrBuCompId);
}
