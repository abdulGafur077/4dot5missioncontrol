package com.fourdotfive.missioncontrol.licensepreference;

import com.fourdotfive.missioncontrol.company.CompanyService;
import com.fourdotfive.missioncontrol.dtos.vendor.CorporateVendorMinDto;
import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.company.CompanyType;
import com.fourdotfive.missioncontrol.workflowstep.WorkflowStepService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class LicensePrefRetrieverImp implements LicensePrefRetriever {

    private static final Logger LOGGER = LoggerFactory.getLogger(LicensePrefRetrieverImp.class);
    @Autowired
    private CompanyService companyService;

    @Autowired
    private WorkflowStepService workflowStepService;

    @Override
    public LicensePreferences getLicensePref(String compId) {
        Company company = companyService.getCompany(compId);
        LicensePreferences companyLicensePreferences = company.getLicensePreferences();
        Map<String, List<String>> companySteps =  workflowStepService.getWorkflowStepsByCompanyId(compId);
        companyLicensePreferences.setWorkflowStepsOrder(companySteps);
        return companyLicensePreferences;
    }

    @Override
    public LicensePreferences getHostCompanyLicensePref() {

        Company company = companyService.getHostCompany();

        LicensePreferences licensePref = company.getLicensePreferences();
        Map<String, List<String>> companySteps =  workflowStepService.getWorkflowStepsByCompanyId(company.getId());
        licensePref.setWorkflowStepsOrder(companySteps);
        return licensePref;
    }

    @Override
    public LicensePreferences getLicensePref(Company company) {

        LOGGER.debug("Retriving license preference of company {}", company.getName());
        LicensePreferences licensePref = company.getLicensePreferences();

        return licensePref;
    }

    @Override
    public LicensePreferences getChildLicensePref(String staffOrCorpCompId, String clientOrBuCompId) {
        Company childCompany = companyService.getCompany(clientOrBuCompId);
        LicensePreferences licensePref = null;
        if (childCompany != null) {
            if (childCompany.getCompanyType().name().equals(CompanyType.Corporation.name())) {
                CorporateVendorMinDto corporateVendorMinDto = companyService.getCorporateVendorRelationship(clientOrBuCompId, staffOrCorpCompId);
                if (corporateVendorMinDto != null) {
                    licensePref = corporateVendorMinDto.getClientLicensePreferences();
                    if (licensePref == null) {
                        licensePref = getLicensePref(staffOrCorpCompId);
                    }else {
                        licensePref.setAddCandidateOnlyWithResume(childCompany.getLicensePreferences().isAddCandidateOnlyWithResume());
                    }
                }
            } else {
                licensePref = childCompany.getLicensePreferences();
            }
            Map<String, List<String>> childCompanySteps = workflowStepService.getWorkflowStepsByCompanyId(childCompany.getId());
            if (childCompanySteps != null)
                licensePref.setWorkflowStepsOrder(childCompanySteps);
        }
        return licensePref;
    }
}
