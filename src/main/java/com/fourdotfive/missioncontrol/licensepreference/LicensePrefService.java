package com.fourdotfive.missioncontrol.licensepreference;

import com.fourdotfive.missioncontrol.common.TokenDetails;
import com.fourdotfive.missioncontrol.dtos.company.CompanyDto;
import com.fourdotfive.missioncontrol.dtos.job.JobRequestDto;
import com.fourdotfive.missioncontrol.dtos.job.NewJobDto;

import java.util.List;

public interface LicensePrefService {

	CompanyDto setHostLicensePref(LicensePrefDtoList newLicPrefDtoList, String companyId, String roleId);
	
	CompanyDto setStaffingOrCorpLicensePref(LicensePrefDtoList newLicPrefDtoList, String companyId, String roleId, String companyType);

	CompanyDto setClientOrBuLicensePref(LicensePrefDtoList newLicPrefDtoList, String pCompId, String cCompId,
			String roleId, String loggedInUserId);

	LicensePrefDtoList getHostLicensePreferences();

	LicensePrefDtoList getStaffOrCorpLicensePref(String staffOrCorpCompId);
	
	LicensePrefDtoList getClientOrBuLicensePref(String staffOrCorpCompId, String clientOrBuCompId);
	
	boolean compareClientOrBuLicensePreferences(String staffOrCorpCompId, String firstClientOrBuCompId,String secondClientOrBuCompId);
	
	LicensePrefDtoList enableLicensePref(LicensePrefDtoList dto);

	CompanyDto setLicensePreferences(LicensePrefDtoList newLicPrefDtoList, String companyId, TokenDetails tokenDetails, String companyType);

	LicensePrefDtoList getLicensePreferences(String companyId, TokenDetails tokenDetails, String companyType);

	void evaluateWithPlan(String companyId, LicensePrefDtoList licensePrefDtoList);

	LicensePrefDtoList getStaffOrCorpLicensePreference(String companyId);

	LicensePrefDtoList getClientOrBuLicensePreference(String staffOrCorpCompId, String clientOrBuCompId);

	List<NewJobDto> getAllJobsByClientOrBuId(JobRequestDto jobRequestDto, String companyId, String userId);

	List<NewJobDto> getAllJobsByClientOrBuIdAndCompanyId(JobRequestDto jobRequestDto, String companyId, String clientOrBuId, String userId);
}
