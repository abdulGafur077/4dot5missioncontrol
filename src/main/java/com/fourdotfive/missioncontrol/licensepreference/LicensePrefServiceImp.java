package com.fourdotfive.missioncontrol.licensepreference;

import com.fourdotfive.missioncontrol.candidate.CandidateService;
import com.fourdotfive.missioncontrol.common.AccessUtil;
import com.fourdotfive.missioncontrol.common.AppConstants;
import com.fourdotfive.missioncontrol.common.Messages;
import com.fourdotfive.missioncontrol.common.TokenDetails;
import com.fourdotfive.missioncontrol.company.CompanyApiExecutor;
import com.fourdotfive.missioncontrol.company.CompanyService;
import com.fourdotfive.missioncontrol.dtos.candidate.CandidatesEnabledStepDto;
import com.fourdotfive.missioncontrol.dtos.company.CompanyDto;
import com.fourdotfive.missioncontrol.dtos.job.JobRequestDto;
import com.fourdotfive.missioncontrol.dtos.job.NewJobDto;
import com.fourdotfive.missioncontrol.dtos.vendor.CorporateVendorDto;
import com.fourdotfive.missioncontrol.dtos.vendor.CorporateVendorMinDto;
import com.fourdotfive.missioncontrol.dtos.workflowstep.WorkflowStepDTO;
import com.fourdotfive.missioncontrol.exception.LicensePrefException;
import com.fourdotfive.missioncontrol.job.JobApiExecutor;
import com.fourdotfive.missioncontrol.plan.PlanApiExecutor;
import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.company.CompanyType;
import com.fourdotfive.missioncontrol.pojo.job.ResumeSourceType;
import com.fourdotfive.missioncontrol.pojo.plan.Feature;
import com.fourdotfive.missioncontrol.pojo.plan.PlanFeaturesCapability;
import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.resume.ResumeApiExecutor;
import com.fourdotfive.missioncontrol.user.UserService;
import com.fourdotfive.missioncontrol.workflowstep.WorkflowStepService;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class LicensePrefServiceImp implements LicensePrefService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LicensePrefServiceImp.class);
    @Autowired
    UserService userService;
    @Autowired
    private LicensePrefDtoToLicensePref dtoToLicensePref;
    @Autowired
    private LicensePrefToLicensePrefDto licensePrefToDto;
    @Autowired
    private CompanyApiExecutor apiExecutor;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private LicensePrefRetriever licensePrefRetriever;
    @Autowired
    private LicensePrefEvaluator licensePrefEvaluator;
    @Autowired
    private AccessUtil accessUtil;
    @Autowired
    private PlanApiExecutor planApiExecutor;
    @Autowired
    private ResumeApiExecutor resumeApiExecutor;
    @Autowired
    private JobApiExecutor jobApiExecutor;
    @Autowired
    private CandidateService candidateService;
    @Autowired
    private WorkflowStepService workflowStepService;

    @Override
    public CompanyDto setLicensePreferences(LicensePrefDtoList newLicPrefDtoList, String companyId,
                                            TokenDetails tokenDetails, String companyType) {

        CompanyDto companyDto = null;
        if (accessUtil.isSuperUser(tokenDetails.getRoleId()) && accessUtil.isHostCompany(companyType)) {
            companyDto = setHostLicensePref(newLicPrefDtoList, companyId, tokenDetails.getRoleId());
        } else {
            companyDto = setStaffingOrCorpLicensePref(newLicPrefDtoList, companyId, tokenDetails.getRoleId(), companyType);
        }
        if (newLicPrefDtoList.getWorkflowStepsOrder() != null) {
            WorkflowStepDTO workflowStepDTO = new WorkflowStepDTO();
            workflowStepDTO.setCompanyId(companyId);
            workflowStepDTO.setWorkflowStepsOrder(newLicPrefDtoList.getWorkflowStepsOrder());
            workflowStepService.saveWorkflowSteps(workflowStepDTO);
        }
        return companyDto;

    }

    @Override
    public CompanyDto setHostLicensePref(LicensePrefDtoList newLicPrefDtoList, String compId, String roleId) {

        LOGGER.info("Setting license preferences of host company");

        LicensePreferences licensePreferences = dtoToLicensePref.convert(newLicPrefDtoList);
        boolean autoMatch = dtoToLicensePref.fetchValue(newLicPrefDtoList.getAutoMatch(), LicensePrefEnum.AUTO_MATCH);
        licensePreferences.setAutoMatch(autoMatch);
        if (autoMatch) {
            licensePreferences.setMatchCount(newLicPrefDtoList.getMatchCount());
        }
        Company returnedComp = apiExecutor.setLicensePreferences(licensePreferences, compId);
        CompanyDto companyDto = companyService.composeCompanyDto(returnedComp, roleId);
        companyDto.setLicensePreferences(newLicPrefDtoList);
        if (returnedComp != null && returnedComp.getLicensePreferences() != null
                && returnedComp.getLicensePreferences().getJobBoardCredentialList() != null) {
            companyDto.getLicensePreferences().setJobBoardCredentialList(setResumeSourceTypeJobBoardCredentialList
                    (returnedComp.getLicensePreferences().getJobBoardCredentialList()));
        }
        return companyDto;
    }

    @Override
    public CompanyDto setStaffingOrCorpLicensePref(LicensePrefDtoList newLicPrefDtoList, String companyId, String roleId, String companyType) {

        LOGGER.info(new DateTime() + " Setting license preferences of staffing or corp company {}", companyId);
        LicensePrefDtoList currLicPrefDtoList = getStaffOrCorpLicensePref(companyId);
        boolean flag = licensePrefEvaluator.validate(currLicPrefDtoList, newLicPrefDtoList);
        if (!flag) {
            throw new LicensePrefException(Messages.LICENSE_PREF_MODIFIED);
        }
        LicensePreferences licensePreferences = dtoToLicensePref.convert(newLicPrefDtoList);
        if (licensePreferences.isAutoMatch()) {
            licensePreferences.setMatchCount(newLicPrefDtoList.getMatchCount());
        }
        if (StringUtils.equals(companyType, String.valueOf(CompanyType.Corporation))) {
            licensePreferences.setAddCandidateOnlyWithResume(newLicPrefDtoList.isAddCandidateOnlyWithResume());
        } else {
            newLicPrefDtoList.setAddCandidateOnlyWithResume(false);
            licensePreferences.setMultipleRolesMatchEnabled(true);
        }

        setAssessmentLinkNameForStaffingCompanyOrCorporation(licensePreferences, companyId, newLicPrefDtoList);
        Company returnedComp = apiExecutor.setLicensePreferences(licensePreferences, companyId);
        CompanyDto companyDto = companyService.composeCompanyDto(returnedComp, roleId);
        companyDto.setLicensePreferences(newLicPrefDtoList);
        if (returnedComp != null && returnedComp.getLicensePreferences() != null) {
            LicensePreferences savedLicensePreferences = returnedComp.getLicensePreferences();
            if (returnedComp.getLicensePreferences().getJobBoardCredentialList() != null) {
                companyDto.getLicensePreferences().setJobBoardCredentialList(setResumeSourceTypeJobBoardCredentialList
                        (savedLicensePreferences.getJobBoardCredentialList()));
            }
            companyDto.getLicensePreferences().setEnableCompanyBranding(savedLicensePreferences.isEnableCompanyBranding());
            companyDto.getLicensePreferences().setFrontPageLogoS3Url(savedLicensePreferences.getFrontPageLogoS3Url());
            companyDto.getLicensePreferences().setAddSmallHeaderToReport(savedLicensePreferences.isAddSmallHeaderToReport());
            companyDto.getLicensePreferences().setSmallHeaderLogoS3Url(savedLicensePreferences.getSmallHeaderLogoS3Url());
            companyDto.getLicensePreferences().setReportColor(savedLicensePreferences.getReportColor());
        }
        return companyDto;
    }

    @Override
    public CompanyDto setClientOrBuLicensePref(LicensePrefDtoList newLicPrefDtoList, String pCompId, String cCompId, String roleId, String loggedInUserId) {

        LOGGER.info("Setting license preferences of Client or Bu {}", cCompId);

        LOGGER.debug("Fetching current license preference");
        LicensePrefDtoList currLicPrefDtoList = getClientOrBuLicensePref(pCompId, cCompId);

        boolean flag = licensePrefEvaluator.validate(currLicPrefDtoList, newLicPrefDtoList);
        if (!flag) {
            throw new LicensePrefException(Messages.LICENSE_PREF_MODIFIED);
        }
        LicensePreferences licensePreferences = dtoToLicensePref.convert(newLicPrefDtoList);
        Company clientCompany = companyService.getCompany(cCompId);
        Company parentCompany = companyService.getCompany(pCompId);
        if (clientCompany != null && clientCompany.getCompanyType() == CompanyType.Client) {
            licensePreferences.setAddCandidateOnlyWithResume(newLicPrefDtoList.isAddCandidateOnlyWithResume());
        }
        setAssessmentLinkNameForClientOrBu(licensePreferences, clientCompany, parentCompany, newLicPrefDtoList);
        Company returnedComp = clientCompany;
        if (clientCompany != null && accessUtil.isCorporateCompany(clientCompany.getCompanyType().name())) {
            CorporateVendorMinDto corporateVendorMinDto = companyService.getCorporateVendorRelationship(cCompId, pCompId);
            corporateVendorMinDto.setClientLicensePreferences(licensePreferences);
            CorporateVendorDto returnedCorporateVendorDto = companyService.saveCorporateVendorRelationship(new CorporateVendorDto(corporateVendorMinDto), loggedInUserId);
            returnedComp.setLicensePreferences(returnedCorporateVendorDto.getClientLicensePreferences());
            CompanyDto companyDto = companyService.composeCompanyDto(returnedComp, roleId);
            companyDto.setLicensePreferences(newLicPrefDtoList);
            setWorkflowStepsOrder(newLicPrefDtoList, companyDto.getCompany().getId());
            return companyDto;
        } else {
            returnedComp = apiExecutor.setLicensePreferences(licensePreferences, cCompId);
            CompanyDto companyDto = companyService.composeCompanyDto(returnedComp, roleId);
            companyDto.setLicensePreferences(newLicPrefDtoList);
            if (returnedComp != null && returnedComp.getLicensePreferences() != null
                    && returnedComp.getLicensePreferences().getJobBoardCredentialList() != null) {
                companyDto.getLicensePreferences().setJobBoardCredentialList(setResumeSourceTypeJobBoardCredentialList
                        (returnedComp.getLicensePreferences().getJobBoardCredentialList()));
            }
            setWorkflowStepsOrder(newLicPrefDtoList, companyDto.getCompany().getId());
            return companyDto;
        }
    }

    private void setWorkflowStepsOrder(LicensePrefDtoList licensePrefDtoList, String companyId) {
        WorkflowStepDTO workflowStepDTO = new WorkflowStepDTO();
        workflowStepDTO.setCompanyId(companyId);
        workflowStepDTO.setWorkflowStepsOrder(licensePrefDtoList.getWorkflowStepsOrder());
        workflowStepService.saveWorkflowSteps(workflowStepDTO);
    }

    private void setAssessmentLinkNameForClientOrBu(LicensePreferences licensePreferences, Company clientOrBu, Company parentCompany, LicensePrefDtoList licensePrefDtoList) {
        if (StringUtils.isNotBlank(licensePrefDtoList.getLicenseAssessmentPreferences().getTestLinkName())) {
            licensePreferences.setTestLinkName(licensePrefDtoList.getLicenseAssessmentPreferences().getTestLinkName());
            boolean isTestLinkNameEditedDeliberately = clientOrBu.getLicensePreferences().isTestLinkNameEditedDeliberately();
            String currentTestLinkName = parentCompany.getShortName() + "-" + clientOrBu.getShortName();
            LOGGER.info("setAssessmentLinkName :: current short name : " + currentTestLinkName + " test link name : " + licensePrefDtoList.getLicenseAssessmentPreferences().getTestLinkName());
            if (!isTestLinkNameEditedDeliberately && !StringUtils.equals(currentTestLinkName, licensePrefDtoList.getLicenseAssessmentPreferences().getTestLinkName())) {
                licensePreferences.setTestLinkNameEditedDeliberately(true);
            } else if (isTestLinkNameEditedDeliberately) {
                licensePreferences.setTestLinkNameEditedDeliberately(true);
            } else {
                licensePreferences.setTestLinkNameEditedDeliberately(false);
            }

            List<String> testLinkNames = clientOrBu.getLicensePreferences().getTestLinkNames();
            if (!testLinkNames.contains(licensePrefDtoList.getLicenseAssessmentPreferences().getTestLinkName())) {
                testLinkNames.add(licensePrefDtoList.getLicenseAssessmentPreferences().getTestLinkName());
            }
            licensePreferences.setTestLinkNames(testLinkNames);
        }
    }

    private void setAssessmentLinkNameForStaffingCompanyOrCorporation(LicensePreferences licensePreferences, String companyId, LicensePrefDtoList licensePrefDtoList) {
        if (StringUtils.isNotBlank(licensePrefDtoList.getLicenseAssessmentPreferences().getTestLinkName())) {
            licensePreferences.setTestLinkName(licensePrefDtoList.getLicenseAssessmentPreferences().getTestLinkName());
            Company company = companyService.getCompany(companyId);
            boolean isTestLinkNameEditedDeliberately = company.getLicensePreferences().isTestLinkNameEditedDeliberately();
            LOGGER.info("setAssessmentLinkName :: current short name : " + company.getShortName() + " test link name : " + licensePrefDtoList.getLicenseAssessmentPreferences().getTestLinkName());
            if (!isTestLinkNameEditedDeliberately && !StringUtils.equals(company.getShortName(), licensePrefDtoList.getLicenseAssessmentPreferences().getTestLinkName())) {
                licensePreferences.setTestLinkNameEditedDeliberately(true);
            } else if (isTestLinkNameEditedDeliberately) {
                licensePreferences.setTestLinkNameEditedDeliberately(true);
            } else {
                licensePreferences.setTestLinkNameEditedDeliberately(false);
            }

            List<String> testLinkNames = company.getLicensePreferences().getTestLinkNames();
            if (!testLinkNames.contains(licensePrefDtoList.getLicenseAssessmentPreferences().getTestLinkName())) {
                testLinkNames.add(licensePrefDtoList.getLicenseAssessmentPreferences().getTestLinkName());
            }
            licensePreferences.setTestLinkNames(testLinkNames);
        }
    }

    @Override
    public LicensePrefDtoList enableLicensePref(LicensePrefDtoList dto) {
        dto.setFourDotFiveIntellList(enableLicensePrefDetails(dto.getFourDotFiveIntellList()));
        dto.setTechAssessmentList(enableLicensePrefDetails(dto.getTechAssessmentList()));
        dto.setValueAssessmentList(enableLicensePrefDetails(dto.getValueAssessmentList()));
        dto.setVerificationList(enableLicensePrefDetails(dto.getVerificationList()));
        dto.setPhoneScreenList(enableLicensePrefDetails(dto.getPhoneScreenList()));
        dto.setInterviewList(enableLicensePrefDetails(dto.getInterviewList()));
        dto.setAutoMatch(enableLicensePrefDetails(dto.getAutoMatch()));
        dto.setRecruiterScreening(enableLicensePrefDetails(dto.getRecruiterScreening()));
        dto.setLicenseAssessmentPreferences(enableLicenseAssesmentPreferences(dto.getLicenseAssessmentPreferences()));
        return dto;
    }

    private LicenseAssessmentPreferences enableLicenseAssesmentPreferences(LicenseAssessmentPreferences dtoLicenseAssesmentPreferences) {

        dtoLicenseAssesmentPreferences.setInviteCandidateOnlyEnabled(true);
        dtoLicenseAssesmentPreferences.setImageProctoringEnabled(true);
        dtoLicenseAssesmentPreferences.setAddProctoringImagesToReportEnabled(true);
        return dtoLicenseAssesmentPreferences;
    }

    private LicensePrefDto enableLicensePrefDetails(LicensePrefDto dto) {

        for (LicensePrefDetails licensePrefDetails : dto.getLicensePrefDetailsList()) {
            licensePrefDetails.setEnabled(true);
        }

        return dto;
    }

    @Override
    public LicensePrefDtoList getHostLicensePreferences() {

        LicensePreferences licPref = licensePrefRetriever.getHostCompanyLicensePref();
        LicensePrefDtoList dto = licensePrefToDto.convert(licPref);
        dto = enableLicensePref(dto);

        return dto;
    }

    @Override
    public LicensePrefDtoList getLicensePreferences(String companyId, TokenDetails tokenDetails, String companyType) {
        LicensePrefDtoList dto = null;
        if (accessUtil.isSuperUser(tokenDetails.getRoleId()) && accessUtil.isHostCompany(companyType)) {
            dto = getHostLicensePreferences();
        } else {
            dto = getStaffOrCorpLicensePreference(companyId);
        }
        Company company = companyService.getCompany(companyId);
        if (company.getCompanyType() == CompanyType.StaffingCompany || company.getCompanyType() == CompanyType.Corporation) {
            evaluateWithPlan(companyId, dto);
        }
        /*if (!company.getCompanyType().equals(CompanyType.Corporation)) {
            dto.setMultipleRolesMatchEnabled(true);
        }*/
        return dto;
    }

    public void evaluateWithPlan(String companyId, LicensePrefDtoList licensePrefDtoList) {

        Boolean isActivePlan = planApiExecutor.isPlanActive(companyId);
        PlanFeaturesCapability planFeaturesCapability = planApiExecutor.getPlanDetailsByCompanyId(companyId);
        if (planFeaturesCapability != null && !CollectionUtils.isEmpty(planFeaturesCapability.getFeatures())) {
            for (Feature feature : planFeaturesCapability.getFeatures()) {
                String featureName = feature.getName();
                if (featureName.contains("Customizable "))
                    featureName = featureName.replace("Customizable ", "");
                String message = featureName + " is not part of your subscribed plan.";
                boolean isPlanAvailable = feature.isAvailable();
                if (StringUtils.equals(feature.getName(), AppConstants.FEATURE_MATCHING)) {
                    if (!isPlanAvailable || BooleanUtils.isFalse(isActivePlan)) {
                        licensePrefDtoList.getAutoMatch().getLicensePrefDetailsList().get(0).setEnabled(false);
                    }
                }

                if (StringUtils.equals(feature.getName(), AppConstants.FEATURE_TECH_ASSESSMENT)) {
                    if (!isPlanAvailable || BooleanUtils.isFalse(isActivePlan)) {
                        licensePrefDtoList.getTechAssessmentList().getLicensePrefDetailsList().get(0).setEnabled(false);
                        if (BooleanUtils.isFalse(isActivePlan))
                            message = AppConstants.PLAN_STEP_EXPIRY_VALIDATION_MESSAGE;

                        licensePrefDtoList.getTechAssessmentList().getLicensePrefDetailsList().get(0).setMessage(message);
                    }
                }

                if (StringUtils.equals(feature.getName(), AppConstants.FEATURE_VALUE_ASSESSMENT)) {
                    if (!isPlanAvailable || BooleanUtils.isFalse(isActivePlan)) {
                        licensePrefDtoList.getValueAssessmentList().getLicensePrefDetailsList().get(0).setEnabled(false);
                        if (BooleanUtils.isFalse(isActivePlan))
                            message = AppConstants.PLAN_STEP_EXPIRY_VALIDATION_MESSAGE;

                        licensePrefDtoList.getValueAssessmentList().getLicensePrefDetailsList().get(0).setMessage(message);
                    }
                }

                if (StringUtils.equals(feature.getName(), AppConstants.FEATURE_CUSTOMIZABLE_PHONE_FEEDBACK)) {
                    if (!isPlanAvailable || BooleanUtils.isFalse(isActivePlan)) {
                        licensePrefDtoList.getPhoneScreenList().getLicensePrefDetailsList().get(0).setEnabled(false);

                        if (BooleanUtils.isFalse(isActivePlan))
                            message = AppConstants.PLAN_STEP_EXPIRY_VALIDATION_MESSAGE;

                        licensePrefDtoList.getPhoneScreenList().getLicensePrefDetailsList().get(0).setMessage(message);

                    }
                }

                if (StringUtils.equals(feature.getName(), AppConstants.FEATURE_CUSTOMIZABLE_INTERVIEW_FEEDBACK)) {
                    if (!isPlanAvailable || BooleanUtils.isFalse(isActivePlan)) {
                        licensePrefDtoList.getInterviewList().getLicensePrefDetailsList().get(0).setEnabled(false);
                        if (BooleanUtils.isFalse(isActivePlan))
                            message = AppConstants.PLAN_STEP_EXPIRY_VALIDATION_MESSAGE;

                        licensePrefDtoList.getInterviewList().getLicensePrefDetailsList().get(0).setMessage(message);
                    }
                }

                if (StringUtils.equals(feature.getName(), AppConstants.FEATURE_CUSTOMIZABLE_RECRUITER_SCREENING)) {
                    if (!isPlanAvailable || BooleanUtils.isFalse(isActivePlan)) {
                        licensePrefDtoList.getRecruiterScreening().getLicensePrefDetailsList().get(0).setEnabled(false);
                        if (BooleanUtils.isFalse(isActivePlan))
                            message = AppConstants.PLAN_STEP_EXPIRY_VALIDATION_MESSAGE;

                        licensePrefDtoList.getRecruiterScreening().getLicensePrefDetailsList().get(0).setMessage(message);
                    }
                }
            }
        }
    }

    @Override
    public LicensePrefDtoList getStaffOrCorpLicensePref(String staffOrCorpCompId) {
        LOGGER.debug("Fetching license preferences of staffing or corporate company  - company id {} ", staffOrCorpCompId);
        //Retrieve host company license preferences
        LicensePreferences hostLicensePref = licensePrefRetriever.getHostCompanyLicensePref();
        LicensePrefDtoList hostPrefDto = licensePrefToDto.convert(hostLicensePref);
        hostPrefDto = enableLicensePref(hostPrefDto);
        //Retrieve staffing or corporate company license preferences
        LicensePreferences staffOrCorporateLicensePreferences = licensePrefRetriever.getLicensePref(staffOrCorpCompId);
        if (staffOrCorporateLicensePreferences == null) {
            staffOrCorporateLicensePreferences = new LicensePreferences();
        }
        Company company = companyService.getCompany(staffOrCorpCompId);
        String testLinkName = getCompanyTestLinkName(company);
        staffOrCorporateLicensePreferences.setTestLinkName(testLinkName);
        LicensePrefDtoList staffOrCorporateLicensePreferencesDTO = licensePrefToDto.convert(staffOrCorporateLicensePreferences);
        LicensePrefDtoList licensePrefDtoList = licensePrefEvaluator.evaluate(hostPrefDto, staffOrCorporateLicensePreferencesDTO);
        licensePrefDtoList.setDaxtraDbName(staffOrCorporateLicensePreferencesDTO.getDaxtraDbName());
        if (staffOrCorporateLicensePreferences.isFourDotFiveIntelligence()
                && ((staffOrCorporateLicensePreferences.isValueAssessment())
                || (staffOrCorporateLicensePreferences.isTechAssessment()))) {
            licensePrefDtoList.getLicenseAssessmentPreferences().setAccessTimes(staffOrCorporateLicensePreferences.getAccessTimes());
        } else {
            List<AccessTimeDto> companyAccessTimes = setCompanyAccessTimes(hostLicensePref.getAccessTimes());
            licensePrefDtoList.getLicenseAssessmentPreferences().setAccessTimes(companyAccessTimes);
        }

        if (StringUtils.isNotBlank(licensePrefDtoList.getDaxtraDbName())) {
            licensePrefDtoList.setDaxtraDbNamePresentAtCompany(true);
        }

        if (company.getCompanyType() == CompanyType.Corporation) {
            licensePrefDtoList.setAddCandidateOnlyWithResume(staffOrCorporateLicensePreferences.isAddCandidateOnlyWithResume());
            licensePrefDtoList.setMultipleRolesMatchEnabled(staffOrCorporateLicensePreferences.isMultipleRolesMatchEnabled());
        } else {
            licensePrefDtoList.setMultipleRolesMatchEnabled(true);
        }

        return licensePrefDtoList;
    }

    @Override
    public LicensePrefDtoList getStaffOrCorpLicensePreference(String companyId) {
        LicensePrefDtoList licensePrefDtoList = getStaffOrCorpLicensePref(companyId);
        setMessagesForWorkflowStepsByCompany(licensePrefDtoList, companyId);
        return licensePrefDtoList;
    }

    @Override
    public LicensePrefDtoList getClientOrBuLicensePreference(String staffOrCorpCompId, String clientOrBuCompId) {
        LicensePrefDtoList licensePrefDtoList = getClientOrBuLicensePref(staffOrCorpCompId, clientOrBuCompId);
        setMessagesForWorkflowStepsByClientOrBu(licensePrefDtoList, staffOrCorpCompId, clientOrBuCompId);
        return licensePrefDtoList;
    }

    private String getCompanyTestLinkName(Company company) {
        LicensePreferences licensePreference = company.getLicensePreferences();
        if (licensePreference == null)
            return null;

        if (licensePreference.isTestLinkNameEditedDeliberately()) {
            return licensePreference.getTestLinkName();
        }

        companyService.updateCompany(company.getId(), company.getShortName());
        return company.getShortName();
    }


    private String getTestLinkNameOfClientOrBu(Company clientOrBU, String parentTestLinkName) {
        String clientTestLinkName = parentTestLinkName + "-" + clientOrBU.getShortName();
        LicensePreferences licensePreference = clientOrBU.getLicensePreferences();
        if (licensePreference == null)
            return null;

        if (licensePreference.isTestLinkNameEditedDeliberately()) {
            return licensePreference.getTestLinkName();
        }

        companyService.updateCompany(clientOrBU.getId(), clientTestLinkName);
        return clientTestLinkName;
    }

    @Override
    public LicensePrefDtoList getClientOrBuLicensePref(String staffOrCorpCompId, String clientOrBuCompId) {


        LOGGER.debug("Fetching license preferences of client or Bu  - parent company id {} -  child company id {} ", staffOrCorpCompId, clientOrBuCompId);
        LicensePrefDtoList staffOrCorporateLicensePreferencesDTO = getStaffOrCorpLicensePref(staffOrCorpCompId);
        LicensePreferences clientOrBuLicensePreferences = null;
        if (StringUtils.isNotBlank(clientOrBuCompId)) {
            clientOrBuLicensePreferences = licensePrefRetriever.getChildLicensePref(staffOrCorpCompId, clientOrBuCompId);
            if (clientOrBuLicensePreferences.getAccessTimes() == null || clientOrBuLicensePreferences.getAccessTimes().size() < 2) {
                clientOrBuLicensePreferences.setAccessTimes(staffOrCorporateLicensePreferencesDTO.getLicenseAssessmentPreferences().getAccessTimes());
            }
            if (clientOrBuLicensePreferences.getCandidatesSuspendedPeriod() == 0 && staffOrCorporateLicensePreferencesDTO.getCandidatesSuspendedPeriod() != 0) {
                clientOrBuLicensePreferences.setCandidatesSuspendedPeriod(staffOrCorporateLicensePreferencesDTO.getCandidatesSuspendedPeriod());
            }
            clientOrBuLicensePreferences.setAllowOverride(staffOrCorporateLicensePreferencesDTO.isAllowOverride());
        }

        if (clientOrBuLicensePreferences == null) {
            clientOrBuLicensePreferences = new LicensePreferences();
        }

        Company company = companyService.getCompany(staffOrCorpCompId);
        LicensePrefDtoList licensePreferencesDTO;
        Company clientOrBu = companyService.getCompany(clientOrBuCompId);

        if (clientOrBu != null) {
            String companyShortName = company.getShortName();
            String testLinkName = getTestLinkNameOfClientOrBu(clientOrBu, companyShortName);
            clientOrBuLicensePreferences.setTestLinkName(testLinkName);
            LicensePrefDtoList clientOrBuLicensePreferencesDTO = licensePrefToDto.convert(clientOrBuLicensePreferences);
            licensePreferencesDTO = licensePrefEvaluator.evaluate(staffOrCorporateLicensePreferencesDTO, clientOrBuLicensePreferencesDTO);
        } else {
            licensePreferencesDTO = staffOrCorporateLicensePreferencesDTO;
        }

        if (clientOrBuLicensePreferences.isFourDotFiveIntelligence()
                && ((clientOrBuLicensePreferences.isValueAssessment())
                || (clientOrBuLicensePreferences.isTechAssessment()))) {
            licensePreferencesDTO.getLicenseAssessmentPreferences().setAccessTimes(clientOrBuLicensePreferences.getAccessTimes());
        } else {
            List<AccessTimeDto> companyAccessTimes = setCompanyAccessTimes(staffOrCorporateLicensePreferencesDTO.getLicenseAssessmentPreferences().getAccessTimes());
            licensePreferencesDTO.getLicenseAssessmentPreferences().setAccessTimes(companyAccessTimes);
        }
        if (staffOrCorporateLicensePreferencesDTO.getAutoMatch() != null && !CollectionUtils.isEmpty(staffOrCorporateLicensePreferencesDTO.getAutoMatch().getLicensePrefDetailsList())) {
            licensePreferencesDTO.getAutoMatch().getLicensePrefDetailsList().get(0).setEnabled(staffOrCorporateLicensePreferencesDTO.getAutoMatch().getLicensePrefDetailsList().get(0).isEnabled());
            licensePreferencesDTO.getAutoMatch().getLicensePrefDetailsList().get(0).setSelected(staffOrCorporateLicensePreferencesDTO.getAutoMatch().getLicensePrefDetailsList().get(0).isSelected());
        }
        if (StringUtils.isNotBlank(staffOrCorporateLicensePreferencesDTO.getDaxtraDbName())) {
            licensePreferencesDTO.setDaxtraDbNamePresentAtCompany(true);
        }

        if (company.getCompanyType() == CompanyType.StaffingCompany || company.getCompanyType() == CompanyType.Corporation) {
            evaluateWithPlan(staffOrCorpCompId, licensePreferencesDTO);
        }

        if (clientOrBu != null && clientOrBu.getCompanyType() == CompanyType.Client) {
            LicensePreferences clientLicensePref = clientOrBu.getLicensePreferences();
            if (clientLicensePref != null) {
                licensePreferencesDTO.setAddCandidateOnlyWithResume(clientLicensePref.isAddCandidateOnlyWithResume());
            }
        }

        return licensePreferencesDTO;
    }

    @Override
    public boolean compareClientOrBuLicensePreferences(String staffOrCorpCompId,
                                                       String firstClientOrBuCompId, String secondClientOrBuCompId) {

        LicensePrefDtoList firstPrefDto = getClientOrBuLicensePref(staffOrCorpCompId, firstClientOrBuCompId);

        LicensePrefDtoList secondPrefDto = getClientOrBuLicensePref(staffOrCorpCompId, secondClientOrBuCompId);

        return licensePrefEvaluator.compareLicensePreferences(firstPrefDto, secondPrefDto);
    }

    public List<AccessTimeDto> setCompanyAccessTimes(List<AccessTimeDto> companyAccessTimes) {
        List<AccessTimeDto> accessTimeDtos = new ArrayList<>();
        for (AccessTimeDto accessTimeDto : companyAccessTimes) {
            if (!accessTimeDto.isAccessTimeValue()) {
                accessTimeDto.setAccessTimeEnabled(false);
            }
            if (!accessTimeDto.isAccessTimeEnabled() || !accessTimeDto.isAccessTimeValue()) {
                accessTimeDto.setAccessTimeValue(false);
                accessTimeDto.setAccessTimeEnabled(false);
            }
            accessTimeDtos.add(accessTimeDto);
        }
        return accessTimeDtos;
    }

    private List<JobBoardCredential> setResumeSourceTypeJobBoardCredentialList(List<JobBoardCredential> jobBoardCredentialList) {
        for (JobBoardCredential jobBoardCredential : jobBoardCredentialList) {
            ResumeSourceType resumeSourceType = resumeApiExecutor.getResumeSourceTypeById(jobBoardCredential.getJobBoardId());
            if (resumeSourceType != null)
                jobBoardCredential.setSourceType(resumeSourceType.getSourceType());
        }
        return jobBoardCredentialList;
    }

    private void setMessagesForWorkflowStepsByClientOrBu(LicensePrefDtoList licensePrefDtoList, String companyId, String clientOrBuId) {
        User user = userService.getCurrentLoggedInUser();
        JobRequestDto jobRequestDto = new JobRequestDto();
        jobRequestDto.setIsAllRequisition(true);
        jobRequestDto.setAassignedToMe(true);
        jobRequestDto.setAssignedToOthers(true);
        jobRequestDto.setIsSharedWithMe(true);
        jobRequestDto.setUnassigned(true);
        List<NewJobDto> newJobDtos = getAllJobsByClientOrBuIdAndCompanyId(jobRequestDto, companyId, clientOrBuId, user.getId());
        if (!CollectionUtils.isEmpty(newJobDtos)) {
            CandidatesEnabledStepDto candidatesEnabledStepDto = candidateService.getCandidatesPresentOnStepByClientOrBuId(companyId, user.getId(), newJobDtos);

            if (candidatesEnabledStepDto.isCandidatesPresentOnRecruiterScreening()
                    && licensePrefDtoList.getRecruiterScreening().getLicensePrefDetailsList().get(0).isEnabled()) {
                licensePrefDtoList.getRecruiterScreening().getLicensePrefDetailsList().get(0).setCandidatesPresentOnStep(true);
                licensePrefDtoList.getRecruiterScreening().getLicensePrefDetailsList()
                        .get(0).setMessageForCandidatePresent(candidatesEnabledStepDto.getRequisitionCountOnRecruiterScreening() + " Requisition(s) that contain this step. Please move all the candidate cards out of this step in all the requisitions before performing this action.");
            }
            if (candidatesEnabledStepDto.isCandidatesPresentOnTechAssessment()
                    && licensePrefDtoList.getTechAssessmentList().getLicensePrefDetailsList().get(0).isEnabled()) {
                licensePrefDtoList.getTechAssessmentList().getLicensePrefDetailsList().get(0).setCandidatesPresentOnStep(true);
                licensePrefDtoList.getTechAssessmentList().getLicensePrefDetailsList()
                        .get(0).setMessageForCandidatePresent(candidatesEnabledStepDto.getRequisitionCountOnTechAssessment() + " Requisition(s) that contain this step. Please move all the candidate cards out of this step in all the requisitions before performing this action.");
            }
            if (candidatesEnabledStepDto.isCandidatesPresentOnValueAssessment()
                    && licensePrefDtoList.getValueAssessmentList().getLicensePrefDetailsList().get(0).isEnabled()) {
                licensePrefDtoList.getValueAssessmentList().getLicensePrefDetailsList().get(0).setCandidatesPresentOnStep(true);
                licensePrefDtoList.getValueAssessmentList().getLicensePrefDetailsList()
                        .get(0).setMessageForCandidatePresent(candidatesEnabledStepDto.getRequisitionCountOnValueAssessment() + " Requisition(s) that contain this step. Please move all the candidate cards out of this step in all the requisitions before performing this action.");
            }
            if (candidatesEnabledStepDto.isCandidatesPresentOnPhoneScreen()
                    && licensePrefDtoList.getPhoneScreenList().getLicensePrefDetailsList().get(0).isEnabled()) {
                licensePrefDtoList.getPhoneScreenList().getLicensePrefDetailsList().get(0).setCandidatesPresentOnStep(true);
                licensePrefDtoList.getPhoneScreenList().getLicensePrefDetailsList()
                        .get(0).setMessageForCandidatePresent(candidatesEnabledStepDto.getRequisitionCountOnPhoneScreen() + " Requisition(s) that contain this step. Please move all the candidate cards out of this step in all the requisitions before performing this action.");
            }
            if (candidatesEnabledStepDto.isCandidatesPresentOnInterview()
                    && licensePrefDtoList.getInterviewList().getLicensePrefDetailsList().get(0).isEnabled()) {
                licensePrefDtoList.getInterviewList().getLicensePrefDetailsList().get(0).setCandidatesPresentOnStep(true);
                licensePrefDtoList.getInterviewList().getLicensePrefDetailsList()
                        .get(0).setMessageForCandidatePresent(candidatesEnabledStepDto.getRequisitionCountOnInterview() + " Requisition(s) that contain this step. Please move all the candidate cards out of this step in all the requisitions before performing this action.");
            }
            if (candidatesEnabledStepDto.isCandidatesPresentOnVerification()
                    && licensePrefDtoList.getVerificationList().getLicensePrefDetailsList().get(0).isEnabled()) {
                licensePrefDtoList.getVerificationList().getLicensePrefDetailsList().get(0).setCandidatesPresentOnStep(true);
                licensePrefDtoList.getVerificationList().getLicensePrefDetailsList()
                        .get(0).setMessageForCandidatePresent(candidatesEnabledStepDto.getRequisitionCountOnVerification() + " Requisition(s) that contain this step. Please move all the candidate cards out of this step in all the requisitions before performing this action.");
            }
        }


    }

    private void setMessagesForWorkflowStepsByCompany(LicensePrefDtoList licensePrefDtoList, String companyId) {
        Company company = companyService.getCompany(companyId);
        User user = userService.getCurrentLoggedInUser();
        String companyType;
        if (CompanyType.Corporation.equals(company.getCompanyType()))
            companyType = CompanyType.BusinessUnit.name();
        else
            companyType = CompanyType.Client.name();
        List<CompanyDto> allClientsOrBu = companyService.getClientOrBuList(companyId);
        List<String> allClientOrBuIds = new ArrayList<>();
        for (CompanyDto companyDto : allClientsOrBu) {
            allClientOrBuIds.add(companyDto.getCompany().getId());
        }
        if (!CollectionUtils.isEmpty(allClientsOrBu)) {
            CandidatesEnabledStepDto candidatesEnabledStepDto = candidateService.getCandidatesPresentOnStepByCompanyId(companyId, user.getId(), allClientOrBuIds);

            if (candidatesEnabledStepDto.isCandidatesPresentOnRecruiterScreening()
                    && licensePrefDtoList.getRecruiterScreening().getLicensePrefDetailsList().get(0).isEnabled()) {
                licensePrefDtoList.getRecruiterScreening().getLicensePrefDetailsList().get(0).setCandidatesPresentOnStep(true);
                licensePrefDtoList.getRecruiterScreening().getLicensePrefDetailsList()
                        .get(0).setMessageForCandidatePresent(candidatesEnabledStepDto.getClientOrBuCountOnRecruiterScreening() + " " + companyType + "s that have Requisition(s) that contain this step. Please move all the candidate cards out of this step in all of the requisitions for each of the " + companyType + " before performing this step");
            }
            if (candidatesEnabledStepDto.isCandidatesPresentOnTechAssessment()
                    && licensePrefDtoList.getTechAssessmentList().getLicensePrefDetailsList().get(0).isEnabled()) {
                licensePrefDtoList.getTechAssessmentList().getLicensePrefDetailsList().get(0).setCandidatesPresentOnStep(true);
                licensePrefDtoList.getTechAssessmentList().getLicensePrefDetailsList()
                        .get(0).setMessageForCandidatePresent(candidatesEnabledStepDto.getClientOrBuCountOnTechAssessment() + " " + companyType + "s that have Requisition(s) that contain this step. Please move all the candidate cards out of this step in all of the requisitions for each of the " + companyType + " before performing this step");
            }
            if (candidatesEnabledStepDto.isCandidatesPresentOnValueAssessment()
                    && licensePrefDtoList.getValueAssessmentList().getLicensePrefDetailsList().get(0).isEnabled()) {
                licensePrefDtoList.getValueAssessmentList().getLicensePrefDetailsList().get(0).setCandidatesPresentOnStep(true);
                licensePrefDtoList.getValueAssessmentList().getLicensePrefDetailsList()
                        .get(0).setMessageForCandidatePresent(candidatesEnabledStepDto.getClientOrBuCountOnValueAssessment() + " " + companyType + "s that have Requisition(s) that contain this step. Please move all the candidate cards out of this step in all of the requisitions for each of the " + companyType + " before performing this step");
            }
            if (candidatesEnabledStepDto.isCandidatesPresentOnInterview()
                    && licensePrefDtoList.getInterviewList().getLicensePrefDetailsList().get(0).isEnabled()) {
                licensePrefDtoList.getInterviewList().getLicensePrefDetailsList().get(0).setCandidatesPresentOnStep(true);
                licensePrefDtoList.getInterviewList().getLicensePrefDetailsList()
                        .get(0).setMessageForCandidatePresent(candidatesEnabledStepDto.getClientOrBuCountOnInterview() + " " + companyType + "s that have Requisition(s) that contain this step. Please move all the candidate cards out of this step in all of the requisitions for each of the " + companyType + " before performing this step");
            }
            if (candidatesEnabledStepDto.isCandidatesPresentOnPhoneScreen()
                    && licensePrefDtoList.getPhoneScreenList().getLicensePrefDetailsList().get(0).isEnabled()) {
                licensePrefDtoList.getPhoneScreenList().getLicensePrefDetailsList().get(0).setCandidatesPresentOnStep(true);
                licensePrefDtoList.getPhoneScreenList().getLicensePrefDetailsList()
                        .get(0).setMessageForCandidatePresent(candidatesEnabledStepDto.getClientOrBuCountOnPhoneScreen() + " " + companyType + "s that have Requisition(s) that contain this step. Please move all the candidate cards out of this step in all of the requisitions for each of the " + companyType + " before performing this step");
            }
            if (candidatesEnabledStepDto.isCandidatesPresentOnVerification()
                    && licensePrefDtoList.getVerificationList().getLicensePrefDetailsList().get(0).isEnabled()) {
                licensePrefDtoList.getVerificationList().getLicensePrefDetailsList().get(0).setCandidatesPresentOnStep(true);
                licensePrefDtoList.getVerificationList().getLicensePrefDetailsList()
                        .get(0).setMessageForCandidatePresent(candidatesEnabledStepDto.getClientOrBuCountOnVerification() + " " + companyType + "s that have Requisition(s) that contain this step. Please move all the candidate cards out of this step in all of the requisitions for each of the " + companyType + " before performing this step");
            }
        }
    }

    @Override
    public List<NewJobDto> getAllJobsByClientOrBuId(JobRequestDto jobRequestDto, String companyId, String userId) {
        return jobApiExecutor.getAllJobsByClientOrBuId(jobRequestDto, companyId, userId);
    }

    @Override
    public List<NewJobDto> getAllJobsByClientOrBuIdAndCompanyId(JobRequestDto jobRequestDto, String companyId, String clientOrBuId, String userId) {
        return jobApiExecutor.getAllJobsByClientOrBuIdAndCompanyId(jobRequestDto, companyId, clientOrBuId, userId);
    }
}
