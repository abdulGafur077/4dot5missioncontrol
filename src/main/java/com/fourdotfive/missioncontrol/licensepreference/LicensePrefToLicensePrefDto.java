package com.fourdotfive.missioncontrol.licensepreference;

import com.fourdotfive.missioncontrol.common.AppConstants;
import com.fourdotfive.missioncontrol.common.CommonUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Shalini
 */
@Service
public class LicensePrefToLicensePrefDto implements Converter<LicensePreferences, LicensePrefDtoList> {

    private static final Logger LOGGER = LoggerFactory.getLogger(LicensePrefToLicensePrefDto.class);

    @Override
    public LicensePrefDtoList convert(LicensePreferences source) {

        LOGGER.debug("Converting LicensePref to LicensePrefDto");

        LicensePrefDtoList dto = new LicensePrefDtoList();

        LicenseAssessmentPreferences licenseAssessmentPreferences = new LicenseAssessmentPreferences();

        //set four dot five intelligence
        LicensePrefDetails fourdotfiveIntell = createLicensePrefDetails(source.isFourDotFiveIntelligence(),
                LicensePrefEnum.FOUR_DOT_FIVE_INTELLIGENCE);
        dto.setFourDotFiveIntellList(getLicensePrefDetailsList(LicensePrefEnum.FOUR_DOT_FIVE_INTELLIGENCE, fourdotfiveIntell));

        //set technical assessment
        LicensePrefDetails techAssess = createLicensePrefDetails(source.isTechAssessment(),
                LicensePrefEnum.TECH_ASSESSMENT);
        dto.setTechAssessmentList(getLicensePrefDetailsList(LicensePrefEnum.TECH_ASSESSMENT, techAssess));

        //set value assessment
        LicensePrefDetails valueAssess = createLicensePrefDetails(source.isValueAssessment(),
                LicensePrefEnum.VALUE_ASSESSMENT);
        dto.setValueAssessmentList(getLicensePrefDetailsList(LicensePrefEnum.VALUE_ASSESSMENT, valueAssess));

        //set verification - education and experience
        LicensePrefDetails eduVerification = createLicensePrefDetails(source.isEducationVerification(),
                LicensePrefEnum.EDUCATION_VERIFICATION);
        LicensePrefDetails expVerification = createLicensePrefDetails(source.isExperienceVerification(),
                LicensePrefEnum.EXPERIENCE_VERIFICATION);


        LicensePrefDetails phoneScreen = createLicensePrefDetails(source.isPhoneScreen(),
                LicensePrefEnum.PHONE_SCREEN);

        dto.setPhoneScreenList(getLicensePrefDetailsList(LicensePrefEnum.PHONE_SCREEN, phoneScreen));

        LicensePrefDetails interview = createLicensePrefDetails(source.isInterview(),
                LicensePrefEnum.INTERVIEW);

        LicensePrefDetails autoMatch = createLicensePrefDetails(source.isAutoMatch(),
                LicensePrefEnum.AUTO_MATCH);

        LicensePrefDetails softQualified = createLicensePrefDetails(source.isRecruiterScreening(),
                LicensePrefEnum.RECRUITER_SCREENING);

        dto.setRecruiterScreening(getLicensePrefDetailsList(LicensePrefEnum.RECRUITER_SCREENING, softQualified));

        dto.setAutoMatch(getLicensePrefDetailsList(LicensePrefEnum.AUTO_MATCH, autoMatch));

        dto.setPhoneScreenList(getLicensePrefDetailsList(LicensePrefEnum.PHONE_SCREEN, phoneScreen));

        dto.setInterviewList(getLicensePrefDetailsList(LicensePrefEnum.INTERVIEW, interview));

        dto.setVerificationList(getLicensePrefDetailsList(LicensePrefEnum.VERIFICATION, eduVerification, expVerification));

        if (autoMatch.isSelected()) {
            dto.setMatchCount(source.getMatchCount());
        }

        licenseAssessmentPreferences.setAccessTime(source.getAccessTime());
        licenseAssessmentPreferences.setAssessmentCredentials(source.getAssessmentCredentials());
        licenseAssessmentPreferences.setImageProctoring(source.isImageProctoring());
        licenseAssessmentPreferences.setImageProctoringEnabled(false);
        licenseAssessmentPreferences.setTestLinkName(source.getTestLinkName());
        licenseAssessmentPreferences.setInviteCandidateOnly(source.isInviteCandidateOnly());
        licenseAssessmentPreferences.setInviteCandidateOnlyEnabled(false);
        licenseAssessmentPreferences.setAddProctoringImagesToReport(source.isAddProctoringImagesToReport());
        licenseAssessmentPreferences.setAddProctoringImagesToReportEnabled(false);
        licenseAssessmentPreferences.setTestLinkName(source.getTestLinkName());
        licenseAssessmentPreferences.setAccessTimes(source.getAccessTimes());
        licenseAssessmentPreferences.setAssessmentReportQuestionType(source.getAssessmentReportQuestionType());
        dto.setJobBoardCredentialList(source.getJobBoardCredentialList());
        dto.setLicenseAssessmentPreferences(licenseAssessmentPreferences);
        dto.setUseCorporateCredential(source.isUseCorporateCredential());
        dto.setResumesToPullPerJobBoardCount(source.getResumesToPullPerJobBoardCount());
        dto.setDaxtraDbName(source.getDaxtraDbName());
        if (StringUtils.isNotBlank(source.getDaxtraDbName())) {
            dto.setDaxtraDbNamePresentAtCompany(true);
        }
        dto.setWorkflowStepsOrder(source.getWorkflowStepsOrder());
        dto.setCandidatesSuspendedPeriod(source.getCandidatesSuspendedPeriod());
        dto.setAllowOverride(source.isAllowOverride());
        dto.setMultipleRolesMatchEnabled(source.isMultipleRolesMatchEnabled());
        dto.setAddCandidateOnlyWithResume(source.isAddCandidateOnlyWithResume());
        dto.setEnableCompanyBranding(source.isEnableCompanyBranding());
        dto.setFrontPageLogoS3Url(source.getFrontPageLogoS3Url());
        dto.setAddSmallHeaderToReport(source.isAddSmallHeaderToReport());
        dto.setSmallHeaderLogoS3Url(source.getSmallHeaderLogoS3Url());
        dto.setReportColor(source.getReportColor());
        LOGGER.debug("License preference dto after conversion {}", dto);

        return dto;
    }

    private LicensePrefDetails createLicensePrefDetails(boolean isSelected, LicensePrefEnum licensePrefEnum) {

        LicensePrefDetails prefDetail = new LicensePrefDetails();
        prefDetail.setSelected(isSelected);
        prefDetail.setEnabled(false);
        prefDetail.setLicensePrefEnum(licensePrefEnum);

        String name = licensePrefEnum.toString();
        if (licensePrefEnum.equals(LicensePrefEnum.FOUR_DOT_FIVE_INTELLIGENCE)) {
            name = AppConstants.FOUR_DOT_FIVE_INTELLIGENCE_SCORE;

        } else if (licensePrefEnum.equals(LicensePrefEnum.AUTO_MATCH)) {
            name = AppConstants.AUTOMATED_MATCH;
        } else {
            name = CommonUtil.formatLicensePrefName(licensePrefEnum);
        }
        prefDetail.setName(name);

        return prefDetail;
    }

    private LicensePrefDto getLicensePrefDetailsList(LicensePrefEnum licensePrefEnum, Object... args) {

        LicensePrefDto dto = new LicensePrefDto();
        List<LicensePrefDetails> list = new ArrayList<LicensePrefDetails>();

        for (Object obj : args) {
            LicensePrefDetails details = (LicensePrefDetails) obj;
            list.add(details);
        }

        String name = licensePrefEnum.toString();
        name = CommonUtil.formatLicensePrefName(licensePrefEnum);

        dto.setName(name);
        dto.setLicensePrefDetailsList(list);

        return dto;
    }

}
