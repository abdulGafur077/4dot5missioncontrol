package com.fourdotfive.missioncontrol.licensepreference;


import com.fourdotfive.missioncontrol.dtos.pdf.QuestionType;
import com.fourdotfive.missioncontrol.pojo.job.JobType;

import java.util.List;
import java.util.Map;

public class LicensePreferences {

    private boolean fourDotFiveIntelligence = true;

    private boolean techAssessment;

    private boolean valueAssessment;

    private boolean educationVerification;

    private boolean experienceVerification;

    private boolean phoneScreen;

    private boolean interview;

    private boolean autoMatch;

    private Integer matchCount;

    private boolean recruiterScreening;

    private String testLinkName;
    private List<String> accessTime;
    private boolean inviteCandidateOnly = true;
    private boolean imageProctoring;
    private List<AssessmentCredential> assessmentCredentials;
    private List<JobBoardCredential> jobBoardCredentialList;

    private boolean useCorporateCredential = true;
    private boolean addProctoringImagesToReport;
    private Integer resumesToPullPerJobBoardCount;
    private List<AccessTimeDto> accessTimes;
    private boolean isTestLinkNameEditedDeliberately;
    private List<String> testLinkNames;
    private QuestionType assessmentReportQuestionType = QuestionType.SOME;
    private Map<String, List<String>> workflowStepsOrder;
    private String daxtraDbName;
    private boolean addCandidateOnlyWithResume;
    private int candidatesSuspendedPeriod;
    private boolean allowOverride;
    private boolean IsMultipleRolesMatchEnabled;
    private boolean enableCompanyBranding = false;
    private String frontPageLogoS3Url;
    private boolean addSmallHeaderToReport = false;
    private String smallHeaderLogoS3Url;
    private String reportColor;

    public boolean isFourDotFiveIntelligence() {
        return fourDotFiveIntelligence;
    }

    public void setFourDotFiveIntelligence(boolean fourDotFiveIntelligence) {
        this.fourDotFiveIntelligence = fourDotFiveIntelligence;
    }

    public boolean isTechAssessment() {
        return techAssessment;
    }

    public void setTechAssessment(boolean techAssessment) {
        this.techAssessment = techAssessment;
    }

    public boolean isValueAssessment() {
        return valueAssessment;
    }

    public void setValueAssessment(boolean valueAssessment) {
        this.valueAssessment = valueAssessment;
    }

    public boolean isEducationVerification() {
        return educationVerification;
    }

    public void setEducationVerification(boolean educationVerification) {
        this.educationVerification = educationVerification;
    }

    public boolean isExperienceVerification() {
        return experienceVerification;
    }

    public void setExperienceVerification(boolean experienceVerification) {
        this.experienceVerification = experienceVerification;
    }

    public boolean isPhoneScreen() {
        return phoneScreen;
    }

    public void setPhoneScreen(boolean phoneScreen) {
        this.phoneScreen = phoneScreen;
    }

    public boolean isInterview() {
        return interview;
    }

    public void setInterview(boolean interview) {
        this.interview = interview;
    }

    public String getTestLinkName() {
        return testLinkName;
    }

    public void setTestLinkName(String testLinkName) {
        this.testLinkName = testLinkName;
    }

    public List<String> getAccessTime() {
        return accessTime;
    }

    public void setAccessTime(List<String> accessTime) {
        this.accessTime = accessTime;
    }

    public boolean isInviteCandidateOnly() {
        return inviteCandidateOnly;
    }

    public void setInviteCandidateOnly(boolean inviteCandidateOnly) {
        this.inviteCandidateOnly = inviteCandidateOnly;
    }

    public boolean isImageProctoring() {
        return imageProctoring;
    }

    public void setImageProctoring(boolean imageProctoring) {
        this.imageProctoring = imageProctoring;
    }

    public List<AssessmentCredential> getAssessmentCredentials() {
        return assessmentCredentials;
    }

    public void setAssessmentCredentials(List<AssessmentCredential> assessmentCredentials) {
        this.assessmentCredentials = assessmentCredentials;
    }

    public List<JobBoardCredential> getJobBoardCredentialList() {
        return jobBoardCredentialList;
    }

    public void setJobBoardCredentialList(List<JobBoardCredential> jobBoardCredentialList) {
        this.jobBoardCredentialList = jobBoardCredentialList;
    }

    public boolean isUseCorporateCredential() {
        return useCorporateCredential;
    }

    public void setUseCorporateCredential(boolean useCorporateCredential) {
        this.useCorporateCredential = useCorporateCredential;
    }

    public boolean isAutoMatch() {
        return autoMatch;
    }

    public void setAutoMatch(boolean autoMatch) {
        this.autoMatch = autoMatch;
    }

    public Integer getMatchCount() {
        return matchCount;
    }

    public void setMatchCount(Integer matchCount) {
        this.matchCount = matchCount;
    }

    public boolean isRecruiterScreening() {
        return recruiterScreening;
    }

    public void setRecruiterScreening(boolean recruiterScreening) {
        this.recruiterScreening = recruiterScreening;
    }

    public boolean isAddProctoringImagesToReport() {
        return addProctoringImagesToReport;
    }

    public void setAddProctoringImagesToReport(boolean addProctoringImagesToReport) {
        this.addProctoringImagesToReport = addProctoringImagesToReport;
    }

    public Integer getResumesToPullPerJobBoardCount() {
        return resumesToPullPerJobBoardCount;
    }

    public void setResumesToPullPerJobBoardCount(Integer resumesToPullPerJobBoardCount) {
        this.resumesToPullPerJobBoardCount = resumesToPullPerJobBoardCount;
    }

    public List<AccessTimeDto> getAccessTimes() {
        return accessTimes;
    }

    public void setAccessTimes(List<AccessTimeDto> accessTimes) {
        this.accessTimes = accessTimes;
    }

    public boolean isTestLinkNameEditedDeliberately() {
        return isTestLinkNameEditedDeliberately;
    }

    public void setTestLinkNameEditedDeliberately(boolean testLinkNameEditedDeliberately) {
        isTestLinkNameEditedDeliberately = testLinkNameEditedDeliberately;
    }

    public List<String> getTestLinkNames() {
        return testLinkNames;
    }

    public void setTestLinkNames(List<String> testLinkNames) {
        this.testLinkNames = testLinkNames;
    }

    public QuestionType getAssessmentReportQuestionType() {
        return assessmentReportQuestionType;
    }

    public void setAssessmentReportQuestionType(QuestionType assessmentReportQuestionType) {
        this.assessmentReportQuestionType = assessmentReportQuestionType;
    }

    public Map<String, List<String>> getWorkflowStepsOrder() {
        return workflowStepsOrder;
    }

    public void setWorkflowStepsOrder(Map<String, List<String>> workflowStepsOrder) {
        this.workflowStepsOrder = workflowStepsOrder;
    }

    public String getDaxtraDbName() {
        return daxtraDbName;
    }

    public void setDaxtraDbName(String daxtraDbName) {
        this.daxtraDbName = daxtraDbName;
    }

    public boolean isAddCandidateOnlyWithResume() {
        return addCandidateOnlyWithResume;
    }

    public void setAddCandidateOnlyWithResume(boolean addCandidateOnlyWithResume) {
        this.addCandidateOnlyWithResume = addCandidateOnlyWithResume;
    }

    public int getCandidatesSuspendedPeriod() {
        return candidatesSuspendedPeriod;
    }

    public void setCandidatesSuspendedPeriod(int candidatesSuspendedPeriod) {
        this.candidatesSuspendedPeriod = candidatesSuspendedPeriod;
    }

    public boolean isAllowOverride() {
        return allowOverride;
    }

    public void setAllowOverride(boolean allowOverride) {
        this.allowOverride = allowOverride;
    }

    public boolean isMultipleRolesMatchEnabled() {
        return IsMultipleRolesMatchEnabled;
    }

    public void setMultipleRolesMatchEnabled(boolean multipleRolesMatchEnabled) {
        IsMultipleRolesMatchEnabled = multipleRolesMatchEnabled;
    }

    public boolean isEnableCompanyBranding() {
        return enableCompanyBranding;
    }

    public void setEnableCompanyBranding(boolean enableCompanyBranding) {
        this.enableCompanyBranding = enableCompanyBranding;
    }

    public String getFrontPageLogoS3Url() {
        return frontPageLogoS3Url;
    }

    public void setFrontPageLogoS3Url(String frontPageLogoS3Url) {
        this.frontPageLogoS3Url = frontPageLogoS3Url;
    }

    public boolean isAddSmallHeaderToReport() {
        return addSmallHeaderToReport;
    }

    public void setAddSmallHeaderToReport(boolean addSmallHeaderToReport) {
        this.addSmallHeaderToReport = addSmallHeaderToReport;
    }

    public String getSmallHeaderLogoS3Url() {
        return smallHeaderLogoS3Url;
    }

    public void setSmallHeaderLogoS3Url(String smallHeaderLogoS3Url) {
        this.smallHeaderLogoS3Url = smallHeaderLogoS3Url;
    }

    public String getReportColor() {
        return reportColor;
    }

    public void setReportColor(String reportColor) {
        this.reportColor = reportColor;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("LicensePreferences [fourDotFiveIntelligence=")
                .append(fourDotFiveIntelligence).append(", techAssessment=")
                .append(techAssessment).append(", valueAssessment=")
                .append(valueAssessment).append(", educationVerification=")
                .append(educationVerification)
                .append(", experienceVerification=")
                .append(experienceVerification)
                .append(", phoneScreen=")
                .append(phoneScreen)
                .append(", interviewScreen=")
                .append(interview)
                .append(", testLinkName=")
                .append(testLinkName)
                .append(", accessTime=")
                .append(accessTime)
                .append(", assessmentCredentials=")
                .append(assessmentCredentials)
                .append(", inviteCandidateOnly=")
                .append(inviteCandidateOnly)
                .append(", imageProctoring=")
                .append(imageProctoring)
                .append(", useCorporateCredential=")
                .append(useCorporateCredential)
                .append(",jobBoardCredentialList")
                .append(jobBoardCredentialList)
                .append(", resumesToPullPerJobBoardCount=")
                .append(resumesToPullPerJobBoardCount)
                .append(", daxtraDbName=")
                .append(daxtraDbName)
                .append("]");
        return builder.toString();
    }


}
