package com.fourdotfive.missioncontrol.login;

import java.security.NoSuchAlgorithmException;
import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fourdotfive.missioncontrol.common.AppConstants;
import com.fourdotfive.missioncontrol.common.Message;
import com.fourdotfive.missioncontrol.common.Messages;
import com.fourdotfive.missioncontrol.dtos.user.UserMinInfoDto;
import com.fourdotfive.missioncontrol.pojo.ResetPwdDetails;

/**
 * 
 * 
 * @author shalini,chakravarthy
 *
 */
@RestController
@RequestMapping("/api/access")
public class AutoLoginController {

	@Autowired
	private LoginAndRegistrationService accessControlService;

	private static final Logger LOGGER = LoggerFactory
			.getLogger(AutoLoginController.class);

	/**
	 * For Remember me (Autologin) this method is called. User will be obtained
	 * from the Refresh token by getting email from Refresh token and calling
	 * getUserByUsername from Platform.
	 * 
	 * @param principal
	 *            -Refresh Token
	 * @return UserMinInfoDto
	 */
	@RequestMapping(value = "/rememberme", method = RequestMethod.GET)
	public ResponseEntity<UserMinInfoDto> rememberMe(Principal user,
			HttpServletRequest request) {

		final String authHeader = request.getHeader(AppConstants.AUTHORIZATION);
		UserMinInfoDto userObj = accessControlService.verifyUsername(user
				.getName());

		userObj.setRefreshToken(authHeader);
		return new ResponseEntity<UserMinInfoDto>(userObj, HttpStatus.OK);
	}

	@RequestMapping(value = "/changepassword", method = RequestMethod.POST)
	public ResponseEntity<Message> changePassword(
			@RequestBody @Valid ResetPwdDetails pwdDetails)
			throws NoSuchAlgorithmException {

		LOGGER.info("changepassword : " + pwdDetails);
		accessControlService.resetPassword(pwdDetails);
		Message msg = Message.statusCode(HttpStatus.OK)
				.message(Messages.CHANGE_PWD_SUCCESS).build();

		return new ResponseEntity<Message>(msg, HttpStatus.OK);
	}

}
