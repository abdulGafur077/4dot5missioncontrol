package com.fourdotfive.missioncontrol.login;

import com.fourdotfive.missioncontrol.dtos.plan.PlanMinDto;
import com.fourdotfive.missioncontrol.dtos.user.UserMinInfoDto;
import com.fourdotfive.missioncontrol.pojo.LoginDetails;
import com.fourdotfive.missioncontrol.pojo.NewPasswordDetails;
import com.fourdotfive.missioncontrol.pojo.ResetPwdDetails;
import com.fourdotfive.missioncontrol.pojo.TokenLoginDetails;
import com.fourdotfive.missioncontrol.pojo.user.RoleScreenXRef;

import java.security.NoSuchAlgorithmException;

/**
 * @author shalini, chakravarthy
 */
public interface LoginAndRegistrationService {

    /**
     * To check valid user is logging in Validation of user fields are checked
     * from @LoginDetails
     *
     * @param loginDetails
     * @return User
     * @throws NoSuchAlgorithmException
     */
    UserMinInfoDto verifyPassword(LoginDetails loginDetails) throws NoSuchAlgorithmException;

    /**
     * To get to which email the forgot password link should be sent
     *
     * @param emailId
     * @return
     */
    void forgotPassword(String emailId);

    /**
     * Validate the password fields in forgot password flow
     *
     * @param pwdDetails
     * @throws NoSuchAlgorithmException
     */
    void resetPassword(ResetPwdDetails pwdDetails) throws NoSuchAlgorithmException;

    /**
     * Validate email while doing Remember me (Auto login)
     *
     * @param email
     * @return User
     */
    UserMinInfoDto verifyUsername(String email);

    RoleScreenXRef getRoleScreenReference(String roleId);

    UserMinInfoDto verifyToken(TokenLoginDetails tokenLoginDetails);

    String setPassword(NewPasswordDetails newPasswordDetails, String token);

    PlanMinDto findCurrentPlanMinDtoByCompanyId(String companyId);

    UserMinInfoDto verifyRefreshToken(String token);

    String getUserIdByToken(String token);
}
