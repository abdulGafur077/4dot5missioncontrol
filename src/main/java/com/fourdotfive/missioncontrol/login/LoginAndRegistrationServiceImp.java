package com.fourdotfive.missioncontrol.login;

import com.fourdotfive.missioncontrol.common.AccessUtil;
import com.fourdotfive.missioncontrol.common.AppConstants;
import com.fourdotfive.missioncontrol.common.CommonUtil;
import com.fourdotfive.missioncontrol.common.Messages;
import com.fourdotfive.missioncontrol.dtos.login.PasswordDetailsDto;
import com.fourdotfive.missioncontrol.dtos.plan.PlanHeaderDto;
import com.fourdotfive.missioncontrol.dtos.plan.PlanMinDto;
import com.fourdotfive.missioncontrol.dtos.user.UserMinInfoDto;
import com.fourdotfive.missioncontrol.exception.InvalidCredentialsException;
import com.fourdotfive.missioncontrol.exception.PlatformException;
import com.fourdotfive.missioncontrol.pojo.LoginDetails;
import com.fourdotfive.missioncontrol.pojo.NewPasswordDetails;
import com.fourdotfive.missioncontrol.pojo.ResetPwdDetails;
import com.fourdotfive.missioncontrol.pojo.TokenLoginDetails;
import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.company.CompanyState;
import com.fourdotfive.missioncontrol.pojo.company.CompanyType;
import com.fourdotfive.missioncontrol.pojo.user.Role;
import com.fourdotfive.missioncontrol.pojo.user.RoleScreenXRef;
import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.pojo.user.VerifyPassword;
import com.fourdotfive.missioncontrol.security.FourDotFiveUser;
import com.fourdotfive.missioncontrol.security.JwtUtil;
import com.fourdotfive.missioncontrol.user.UserService;
import com.fourdotfive.missioncontrol.userentityscope.UserScopeOfManagerServiceImp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 * @author shalini, chakravarthy
 */
@Service
public class LoginAndRegistrationServiceImp implements LoginAndRegistrationService {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(LoginAndRegistrationServiceImp.class);
    @Autowired
    private AccessUtil accessUtil;
    @Autowired
    private LoginApiExecutor loginApiExecutor;
    @Autowired
    private UserScopeOfManagerServiceImp managerService;
    @Autowired
    UserService userService;

    @Override
    public UserMinInfoDto verifyPassword(LoginDetails loginDetails)
            throws NoSuchAlgorithmException {
        loginDetails.setPassword(CommonUtil.sha256Password(loginDetails
                .getPassword()));
        VerifyPassword verifyPassword = loginApiExecutor
                .verifyPassword(loginDetails);
        LOGGER.info("verifyPassword account state"+verifyPassword.getAccountState());
        if (verifyPassword != null) {
            LOGGER.info(LOGGER.getClass()+" Method verifyPassword "+verifyPassword.getFirstname());


            // Calling verify username to get the firstname and lastname
            return verifyUsername(loginDetails.getEmail());
        }

        return null;
    }

    @Override
    public UserMinInfoDto verifyToken(TokenLoginDetails tokenLoginDetails) {
        String verifyToken = loginApiExecutor.verifyToken(tokenLoginDetails);
        if (!StringUtils.isEmpty(verifyToken)) {
            return verifyUsername(tokenLoginDetails.getEmail());
        }
        return null;
    }

    @Override
    public String setPassword(NewPasswordDetails newPasswordDetails, String token) {
        PasswordDetailsDto passwordDetailsDto = new PasswordDetailsDto();
        passwordDetailsDto.setToken(token);
        passwordDetailsDto.setNewPassword(newPasswordDetails.getNewPassword());
        passwordDetailsDto.setConfirmPassword(newPasswordDetails.getConfirmPassword());
        passwordDetailsDto.setActionType(newPasswordDetails.getSetPasswordEmailType());
        return loginApiExecutor.setPassword(passwordDetailsDto);
    }

    @Override
    public void forgotPassword(String emailId) {
        loginApiExecutor.forgotPassword(emailId);

    }

    @Override
    public void resetPassword(ResetPwdDetails pwdDetails)
            throws NoSuchAlgorithmException {

        // converting to SHA 256
        pwdDetails.setCurrentPassword(CommonUtil.sha256Password(pwdDetails
                .getCurrentPassword()));
        loginApiExecutor.resetPassword(pwdDetails);

    }

    @Override
    public UserMinInfoDto verifyUsername(String email) {

        User user = loginApiExecutor.verifyUsername(email);
        if (user != null) {
            LOGGER.info("verifyUser Name "+ user.getFirstname());
            return checkCompanyStatus(user);
        } else {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG,
                    Messages.INTERNAL_ERROR_MSG);
        }
    }

    private UserMinInfoDto checkCompanyStatus(User user) {
        Company company = user.getEmployer();
        if (user.getAccessControlList().getAccountState()
                .equals(AppConstants.ACCOUNT_STATE_ARCHIVED)
                || company.getCompanyState().equals(
                CompanyState.Archived.toString())) {
            LOGGER.info("User - {} has been archived", user.getId());
            throw new InvalidCredentialsException();
        }

        LOGGER.info(LOGGER.getClass()+" - Method checkCompanyStatus company "+company.getName());
        RoleScreenXRef screenRef = getRoleScreenReference(user.getRole().getId());
        PlanMinDto planMinDto = findCurrentPlanMinDtoByCompanyId(company.getId());
        LOGGER.info(LOGGER.getClass()+" - Method checkCompanyStatus planMinDto "+planMinDto.getCompanyId());
        UserMinInfoDto minInfo;
        if (screenRef != null) {
            LOGGER.info(LOGGER.getClass()+" - Method checkCompanyStatus screenRef "+ screenRef.getRole().getId());
            minInfo = UserMinInfoDto.getMinInfo(user, screenRef, planMinDto);
        } else {
            minInfo = UserMinInfoDto.getUserMinInfo(user, planMinDto);
        }

        LOGGER.info(LOGGER.getClass()+" - Method checkCompanyStatus minInfo "+minInfo.getAccountState());
        Role role = user.getRole();
        minInfo.setRole(role);
        LOGGER.info(LOGGER.getClass()+" - Method checkCompanyStatus role "+role.getName());
        if (accessUtil.is4Dot5CompanyRole(role)) {
            LOGGER.info(LOGGER.getClass()+" - Method checkCompanyStatus is4Dot5CompanyRole ");
            minInfo.setShouldPopupBeDisplayed(true);
        } else {
            LOGGER.info(LOGGER.getClass()+" - Method checkCompanyStatus isNot4Dot5CompanyRole ");
            minInfo.setShouldPopupBeDisplayed(false);
        }

        checkClientList(minInfo, user, company);
        LOGGER.info(LOGGER.getClass()+" - Method checkCompanyStatus checkClientList ");
        LOGGER.debug("User min info dto {}", minInfo);
        return JwtUtil.createToken(minInfo, user.getEmail());
    }

    private void checkClientList(UserMinInfoDto minInfoDto, User user,
                                 Company company) {
        if ((accessUtil.isSeniorRec(user.getRole())
                || accessUtil.isRecruiter(user.getRole()) || accessUtil
                .isRecruitingManager(user.getRole()))
                && company.getCompanyType().equals(CompanyType.StaffingCompany)) {

            if (user.getAccessControlList().isAllclientOrBU()) {
                List<Company> clientList = managerService.getClientFromManager(
                        user.getId(), company.getId());
                if (checkUserHasNoClientList(clientList)) {
                    minInfoDto.setUserHasNoClients(true);
                }
            } else {
                List<Company> clientList = user.getAccessControlList().getClientOrBUList();
                if (checkUserHasNoClientList(clientList)) {
                    minInfoDto.setUserHasNoClients(true);
                }
            }

        }
    }

    private boolean checkUserHasNoClientList(List<Company> clientList) {
        if (clientList == null || clientList.isEmpty()) {
            return true;
        }
        return false;
    }

    @Override
    public RoleScreenXRef getRoleScreenReference(String roleId) {

        return loginApiExecutor.getRoleScreenReference(roleId);
    }

    @Override
    public PlanMinDto findCurrentPlanMinDtoByCompanyId(String companyId) {
        return loginApiExecutor.findCurrentPlanMinDtoByCompanyId(companyId);

    }

    @Override
    public UserMinInfoDto verifyRefreshToken(String token) {
        FourDotFiveUser fourDotFiveUser = JwtUtil.parseToken(token);
        User user = userService.getUserById(fourDotFiveUser.getId());
        if (user != null && org.apache.commons.lang3.StringUtils.isNotEmpty(user.getEmail())) {
            return verifyUsername(user.getEmail());
        }
        return null;
    }

    @Override
    public String getUserIdByToken(String token) {
        return userService.getUserByToken(token);
    }
}
