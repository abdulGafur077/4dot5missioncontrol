package com.fourdotfive.missioncontrol.login;

import com.fourdotfive.missioncontrol.dtos.login.PasswordDetailsDto;
import com.fourdotfive.missioncontrol.dtos.plan.PlanMinDto;
import com.fourdotfive.missioncontrol.pojo.LoginDetails;
import com.fourdotfive.missioncontrol.pojo.ResetPwdDetails;
import com.fourdotfive.missioncontrol.pojo.TokenLoginDetails;
import com.fourdotfive.missioncontrol.pojo.user.RoleScreenXRef;
import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.pojo.user.VerifyPassword;

import java.security.NoSuchAlgorithmException;

public interface LoginApiExecutor {

    /**
     * To check valid user is logging in Validation of user fields are checked
     * from @LoginDetails
     *
     * @param loginDetails
     * @return User
     */
    VerifyPassword verifyPassword(LoginDetails loginDetails);

    String verifyToken(TokenLoginDetails tokenLoginDetails);

    /**
     * To get to which email the forgot password link should be sent
     *
     * @param emailId
     * @return
     */
    void forgotPassword(String emailId);

    /**
     * Validate the password fields in forgot password flow
     *
     * @param pwdDetails
     * @throws NoSuchAlgorithmException
     */
    void resetPassword(ResetPwdDetails pwdDetails)
            throws NoSuchAlgorithmException;

    /**
     * Validate email while doing Remember me (Auto login)
     *
     * @param email
     * @return User
     */
    User verifyUsername(String email);

    RoleScreenXRef getRoleScreenReference(String roleId);

    String setPassword(PasswordDetailsDto passwordDetailsDto);

    PlanMinDto findCurrentPlanMinDtoByCompanyId(String companyId);
}
