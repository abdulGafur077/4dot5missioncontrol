package com.fourdotfive.missioncontrol.login;

import com.fourdotfive.missioncontrol.common.Message;
import com.fourdotfive.missioncontrol.common.Messages;
import com.fourdotfive.missioncontrol.dtos.user.UserMinInfoDto;
import com.fourdotfive.missioncontrol.pojo.*;
import com.fourdotfive.missioncontrol.pojo.company.CompanyState;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.NoSuchAlgorithmException;

/**
 * @author shalini, chakravarthy
 */
@RestController
@RequestMapping("/auth")
public class LoginController {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(LoginController.class);
    @Autowired
    private LoginAndRegistrationService accessControlService;

    /**
     * This method is called for authorizing users who are trying to login into
     * the app
     *
     * @param loginDetails
     * @return User
     * @throws NoSuchAlgorithmException
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserMinInfoDto> login(
            @RequestBody @Valid LoginDetails loginDetails)
            throws NoSuchAlgorithmException {
        LOGGER.info("login controller "+loginDetails.getEmail());
        UserMinInfoDto user = accessControlService.verifyPassword(loginDetails);
        LOGGER.info(LOGGER.getClass() + " : login - response :  "+ user.toString());
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @RequestMapping(value = "/tokenlogin", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserMinInfoDto> tokenLogin(
            @RequestBody @Valid TokenLoginDetails tokenLoginDetails) {
        UserMinInfoDto user = accessControlService.verifyToken(tokenLoginDetails);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    /**
     * For Forgotten Password, this method is called
     *
     * @param emailId
     * @return String
     */
    @RequestMapping(value = "/forgotpassword", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> forgotPassword(
            @RequestParam("emailId") String emailId) {

        accessControlService.forgotPassword(emailId);

        return new ResponseEntity<String>(HttpStatus.OK);
    }

    /**
     * To change the password from forgot password flow
     *
     * @param pwdDetails
     * @return Message
     * @throws NoSuchAlgorithmException
     */
    @RequestMapping(value = "/changepassword", method = RequestMethod.POST)
    public ResponseEntity<Message> changePassword(
            @RequestBody @Valid ResetPwdDetails pwdDetails)
            throws NoSuchAlgorithmException {

        accessControlService.resetPassword(pwdDetails);

        Message msg = Message.statusCode(HttpStatus.OK)
                .message(Messages.CHANGE_PWD_SUCCESS).build();

        return new ResponseEntity<Message>(msg, HttpStatus.OK);
    }

    @RequestMapping(value = "/setpassword", method = RequestMethod.POST)
    public ResponseEntity<String> newPassword(
            @RequestParam(value = "token", required = true) String token,
            @RequestBody @Valid NewPasswordDetails newPasswordDetails) {
        String response = accessControlService.setPassword(newPasswordDetails, token);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/refreshtoken/login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserMinInfoDto> refreshTokenLogin(
            @RequestBody RefreshTokenLogin refreshTokenLogin) {

        UserMinInfoDto user = accessControlService.verifyRefreshToken(refreshTokenLogin.getToken());
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @RequestMapping(value = "/getuseridbytoken/{token}", method = RequestMethod.GET)
    public ResponseEntity<String> getUserIdByToken(@PathVariable String token) {
        String userId = accessControlService.getUserIdByToken(token);
        return new ResponseEntity<>(userId, HttpStatus.OK);
    }
}
