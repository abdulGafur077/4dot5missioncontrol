package com.fourdotfive.missioncontrol.login;


public enum SetPasswordEmailType {
    CREATE_PASSWORD, FORGOT_PASSWORD
}
