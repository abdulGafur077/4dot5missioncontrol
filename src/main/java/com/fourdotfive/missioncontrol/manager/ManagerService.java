package com.fourdotfive.missioncontrol.manager;

import java.util.List;

import com.fourdotfive.missioncontrol.common.TokenDetails;
import com.fourdotfive.missioncontrol.dtos.company.CompanyDto;
import com.fourdotfive.missioncontrol.dtos.user.TeamMemberDto;
import com.fourdotfive.missioncontrol.dtos.user.UserDto;

public interface ManagerService {

	List<TeamMemberDto> getPossibleManagersForUpdate(String userId,
			String role, String loggedInUserId);

	CompanyDto assignCompanyFor4dot5Admin(String userId, String olduserId,
			TokenDetails tokenDetails, String companyId);

	UserDto setManager(String userId, String managerId,String companyId);

	/**
	 * To get Possible managers for a user, this method is called.
	 * 
	 * @return List<TeamMemeberDto>
	 */
	List<TeamMemberDto> getPossibleManagersForUpdatingCompanyAdmin(
			String userId, String companyId);
	
	boolean removeManager(String userId,UserDto userDto);

	List<TeamMemberDto> getPossibleManagersForHiringManager(String userId, String loggedInUserId);
}
