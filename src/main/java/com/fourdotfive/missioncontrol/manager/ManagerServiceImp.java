package com.fourdotfive.missioncontrol.manager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.fourdotfive.missioncontrol.pojo.Role;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fourdotfive.missioncontrol.common.AccessUtil;
import com.fourdotfive.missioncontrol.common.CommonUtil;
import com.fourdotfive.missioncontrol.common.Messages;
import com.fourdotfive.missioncontrol.common.TokenDetails;
import com.fourdotfive.missioncontrol.company.CompanyApiExecutor;
import com.fourdotfive.missioncontrol.company.CompanyService;
import com.fourdotfive.missioncontrol.dtos.company.CompanyDto;
import com.fourdotfive.missioncontrol.dtos.user.TeamMemberDto;
import com.fourdotfive.missioncontrol.dtos.user.UserDto;
import com.fourdotfive.missioncontrol.exception.AssignManagerPlatformException;
import com.fourdotfive.missioncontrol.exception.FetchEnityScopePlatformException;
import com.fourdotfive.missioncontrol.exception.RemoveManagerPlatformException;
import com.fourdotfive.missioncontrol.exception.Save4Dot5AdminPlatfromException;
import com.fourdotfive.missioncontrol.exception.SetScopePlatformException;
import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.user.AccessControlList;
import com.fourdotfive.missioncontrol.pojo.user.Scope;
import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.user.UserApiExecutor;
import com.fourdotfive.missioncontrol.user.UserException;
import com.fourdotfive.missioncontrol.user.UserService;
import com.fourdotfive.missioncontrol.userentityscope.UserScopeServiceImp;

import static com.fourdotfive.missioncontrol.dtos.user.TeamMemberDto.teamMemberNameComparator;

/**
 * @author Chakravarthy
 */
@Service
public class ManagerServiceImp implements ManagerService {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(ManagerServiceImp.class);

    @Autowired
    private UserService userService;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private AccessUtil accessUtil;

    @Autowired
    private CompanyApiExecutor companyApiExeceutor;

    @Autowired
    private UserApiExecutor userApiExecutor;

    @Autowired
    private UserScopeServiceImp userScopeServiceImp;

    @Override
    public List<TeamMemberDto> getPossibleManagersForUpdate(String userId,
                                                            String role, String loggedInUserId) {
        List<TeamMemberDto> managerList = new ArrayList<TeamMemberDto>();
        User loggedInUser = null;

        if (!accessUtil.isManagerApplicable(role)) {
            return managerList;
        }

        // Get Manger
        User manager = userService.getManager(userId);

        if (manager != null) {
            managerList.add(new TeamMemberDto(manager.getId(), manager
                    .getFirstname(), manager.getLastname()));

            /** The logged in person cannot updated his manager **/
            if (loggedInUserId.equals(userId)) {
                return managerList;
            }

            // Get Manager's reportees
            List<User> managerReportees = userService.getReporteeList(manager.getId());
            for (User user : managerReportees) {
                if (!user.getId().equals(userId) && accessUtil.canRoleHaveReportees(user.getRole().getId())) {
                    if (accessUtil.isHiringManagerRole(role)) {
                        if (accessUtil.isHiringManagerRole(user.getRole()))
                            managerList.add(new TeamMemberDto(user.getId(), user.getFirstname(), user.getLastname()));
                    } else {
                        if (!accessUtil.isHiringManagerRole(user.getRole()))
                            managerList.add(new TeamMemberDto(user.getId(), user.getFirstname(), user.getLastname()));

                    }
                }
            }

            if (accessUtil.isCompanyAdmin(manager.getRole())) {
                loggedInUser = userService.getUserById(loggedInUserId);
                if (accessUtil.isRoleAtCompanyAdminLevel(loggedInUser.getRole()
                        .getId())) {
                    List<User> companyAdmins = userService
                            .getCompanyAdmins(manager.getEmployer());
                    for (User user : companyAdmins) {
                        if (user.getId().equals(manager.getId())) {
                            continue;
                        }
                        managerList.add(new TeamMemberDto(user.getId(), user
                                .getFirstname(), user.getLastname()));
                    }
                }
            }

            /**
             * If the logged in person is updating his reportees, then his peers
             * cannot be his reportee's manager
             **/
            if (manager.getId().equals(loggedInUserId)) {
                return sortManagerList(managerList);
            }

            // Get Manager's manager
            User managersManager = userService.getManager(manager.getId());
            if (managersManager != null) {
                managerList.add(new TeamMemberDto(managersManager.getId(),
                        managersManager.getFirstname(), managersManager
                        .getLastname()));

                // Get Manager's managers reportees
                List<User> managersManagerReportees = userService.getReporteeList(managersManager.getId());
                for (User user : managersManagerReportees) {
                    if (user.getId().equals(manager.getId())) {
                        continue;
                    }
                    if (accessUtil.canRoleHaveReportees(user.getRole().getId())) {
                        if (accessUtil.isHiringManagerRole(role)) {
                            if (accessUtil.isHiringManagerRole(user.getRole()))
                                managerList.add(new TeamMemberDto(user.getId(), user.getFirstname(), user.getLastname()));
                        } else {
                            if (!accessUtil.isHiringManagerRole(user.getRole()))
                                managerList.add(new TeamMemberDto(user.getId(), user.getFirstname(), user.getLastname()));

                        }
                    }
                }

                if (accessUtil.isCompanyAdmin(managersManager.getRole())) {
                    loggedInUser = userService.getUserById(loggedInUserId);
                    if (accessUtil.isRoleAtCompanyAdminLevel(loggedInUser
                            .getRole().getId())) {
                        List<User> companyAdmins = userService
                                .getCompanyAdmins(manager.getEmployer());
                        for (User user : companyAdmins) {
                            if (user.getId().equals(managersManager.getId())) {
                                continue;
                            }
                            managerList.add(new TeamMemberDto(user.getId(),
                                    user.getFirstname(), user.getLastname()));
                        }
                    }
                }
            }
        }

        return sortManagerList(managerList);
    }

    private List<TeamMemberDto> sortManagerList(List<TeamMemberDto> managerList) {
        List<TeamMemberDto> managers = managerList;
        if (managers.size() > 1) {
            Collections.sort(managers, teamMemberNameComparator);
        }
        return managers;
    }

    @Override
    public CompanyDto assignCompanyFor4dot5Admin(String userId,
                                                 String olduserId, TokenDetails tokenDetails, String companyId) {

        LOGGER.info("Length " + olduserId.length());
        if (olduserId.length() != 4) {
            LOGGER.info("Removing old 4dot5 admin ");
            removeCompanyFromOld4dot5admin(olduserId, userId, companyId);
        }
        assignCompanyForNew4DotAdmin(userId, companyId, olduserId);
        Company company = null;
        try {
            company = companyApiExeceutor.saveFourDotFiveAdmin(companyId,
                    userId);
        } catch (Save4Dot5AdminPlatfromException e) {
            rollBackWhileSavingCompanyWith4Dot5Admin(userId, olduserId,
                    companyId);
        }

        CompanyDto companyDto = CompanyDto.mapCompanytoCompanyDto(company);

        User user = companyApiExeceutor.getFourDotFiveAdmin(companyId);

        if (user != null) {
            TeamMemberDto dto = new TeamMemberDto();
            dto.setFirstName(user.getFirstname());
            dto.setLastName(user.getLastname());
            dto.setUserId(user.getId());
            companyDto.setFourDotFiveAdmin(dto);
        }

        LOGGER.info("Four dot five admin with company {} ", companyDto);
        return companyDto;
    }

    private void assignCompanyForNew4DotAdmin(String userId, String companyId,
                                              String olduserId) {
        try {
            AccessControlList accessControlList = userService.getEnityScope(userId);
            Scope scope = Scope.getScope(accessControlList);

            List<String> companyList = scope.getCompanyIDList();
            if (companyList == null) {
                companyList = new ArrayList<String>();
            }
            if (!companyList.contains(companyId)) {
                companyList.add(companyId);
            }
            scope.setCompanyIDList(companyList);
            LOGGER.debug("Setting scope for new user {}", scope);

            userService.setScope(scope, userId);
        } catch (SetScopePlatformException | FetchEnityScopePlatformException e) {
            rollBackWhileAssigningNew4Dot5Admin(userId, olduserId, companyId);
        }

    }

    private void rollBackWhileAssigningNew4Dot5Admin(String userId,
                                                     String olduserId, String companyId) {
        try {
            AccessControlList accessControlList = userService
                    .getEnityScope(olduserId);
            Scope scope = Scope.getScope(accessControlList);

            List<String> companyList = scope.getCompanyIDList();
            if (companyList == null) {
                companyList = new ArrayList<String>();
            }
            companyList.add(companyId);
            scope.setCompanyIDList(companyList);
            LOGGER.debug("Setting scope for new user {}", scope);

            userService.setScope(scope, olduserId);
        } catch (SetScopePlatformException | FetchEnityScopePlatformException e) {
            throwExceptionWhileAssigning(userId);
        }
        throwExceptionWhileAssigning(userId);
    }

    private void rollBackWhileSavingCompanyWith4Dot5Admin(String userId,
                                                          String olduserId, String companyId) {
        addOld4Dot5AdminToCompany(userId, olduserId, companyId);
        removeCompanyFromNew4dot5admin(userId, companyId);
        throwExceptionWhileAssigning(userId);
    }

    private void addOld4Dot5AdminToCompany(String userId, String olduserId,
                                           String companyId) {
        try {
            AccessControlList accessControlList = userService
                    .getEnityScope(olduserId);
            Scope scope = Scope.getScope(accessControlList);

            List<String> companyList = scope.getCompanyIDList();
            if (companyList == null) {
                companyList = new ArrayList<String>();
            }
            if (!companyList.contains(companyId)) {
                companyList.add(companyId);
            }
            scope.setCompanyIDList(companyList);
            LOGGER.debug("Setting scope for new user {}", scope);

            userService.setScope(scope, olduserId);
        } catch (SetScopePlatformException | FetchEnityScopePlatformException e) {
            throwExceptionWhileAssigning(userId);
            User user = userService.getUserById(userId);
            String message = CommonUtil.getUserName(user);
            throw new UserException(
                    Messages.EXCEPTION_WHILE_SAVING_4_DOT_5_ADMIN_COMPANY,
                    message);

        }
    }

    private void removeCompanyFromNew4dot5admin(String userId, String companyId) {
        try {
            AccessControlList accessControlList = userService.getEnityScope(userId);

            Scope scope = Scope.getScope(accessControlList);

            List<String> companyList = scope.getCompanyIDList();

            Iterator<String> iterator = companyList.iterator();
            while (iterator.hasNext()) {
                String id = iterator.next();
                if (id.equals(companyId)) {
                    LOGGER.debug("Company Id removed from scope");
                    iterator.remove();
                }
            }
            scope.setCompanyIDList(companyList);

            userService.setScope(scope, userId);
        } catch (SetScopePlatformException | FetchEnityScopePlatformException e) {
            throwExceptionWhileSavingCompany(userId);
        }

    }

    private void throwExceptionWhileAssigning(String userId) {
        User user = userService.getUserById(userId);
        String message = user.getFirstname();
        if (user.getLastname() != null) {
            message = message + " " + user.getLastname();
        }
        throw new UserException(Messages.EXCEPTION_WHILE_ASSIGNING_4_DOT_ADMIN,
                message);
    }

    private void throwExceptionWhileSavingCompany(String userId) {
        User user = userService.getUserById(userId);
        String message = user.getFirstname();
        if (user.getLastname() != null) {
            message = message + " " + user.getLastname();
        }
        throw new UserException(
                Messages.EXCEPTION_WHILE_SAVING_4_DOT_5_ADMIN_COMPANY, message);
    }

    private void removeCompanyFromOld4dot5admin(String olduserId,
                                                String userId, String companyId) {
        try {
            AccessControlList accessControlList = userService
                    .getEnityScope(olduserId);

            Scope scope = Scope.getScope(accessControlList);

            List<String> companyList = scope.getCompanyIDList();

            Iterator<String> iterator = companyList.iterator();
            while (iterator.hasNext()) {
                String id = iterator.next();
                if (id.equals(companyId)) {
                    LOGGER.debug("Company Id removed from scope");
                    iterator.remove();
                }
            }
            scope.setCompanyIDList(companyList);

            userService.setScope(scope, olduserId);
        } catch (SetScopePlatformException | FetchEnityScopePlatformException e) {
            // get user details for the exception message
            User user = userService.getUserById(userId);
            String message = CommonUtil.getUserName(user);
            throw new UserException(
                    Messages.EXCEPTION_WHILE_ASSIGNING_4_DOT_ADMIN, message);
        }

    }

    @Override
    public UserDto setManager(String userId, String managerId, String companyId) {
        try {
            User user = userService.getUserById(userId);
            // to check wether assigning manger to company admin
            if (accessUtil.isCompanyAdmin(user.getRole())) {
                return userService.getUserDto(user);
            }
            // when updating to same manager
            User manager = userService.getManager(userId);
            if (manager != null && manager.getId().equals(managerId)) {
                return userService.getUserDto(user);
            }
            userApiExecutor.setManager(userId, managerId);
            if (!accessUtil.isFourDot5Admin(user.getRole())
                    && !accessUtil.isSuperUser(user.getRole().getId())) {
                userScopeServiceImp.updateClientListWhenRoleOrManagerUpdate(
                        user, userService.getManager(userId), companyId, false);
            }
            return userService.getUserDto(user);
        } catch (AssignManagerPlatformException e) {
            throw new AssignManagerPlatformException(Messages.INTERNAL_ERROR_MSG);
        }
    }

    @Override
    public List<TeamMemberDto> getPossibleManagersForUpdatingCompanyAdmin(
            String userId, String companyId) {
        Company company = companyService.getCompany(companyId);
        List<User> userList = userService.getCompanyAdmins(company);
        List<TeamMemberDto> managerList = new ArrayList<>();
        for (Iterator<User> iterator = userList.iterator(); iterator.hasNext(); ) {
            User obj = iterator.next();
            if (!obj.getId().equals(userId)) {
                TeamMemberDto dto = TeamMemberDto.mapUserToTeamMember(obj);
                managerList.add(dto);
            }
        }

        return managerList;
    }

    @Override
    public boolean removeManager(String userId, UserDto userDto) {
        try {
            User user = userService.getUserById(userId);
            if (accessUtil.isCompanyAdmin(userDto.getUserDetails().getRole())
                    && !accessUtil.isCompanyAdmin(user.getRole())) {
                userApiExecutor.removeManager(userId);
                return true;
            }
            return false;
        } catch (RemoveManagerPlatformException e) {
            throw new RemoveManagerPlatformException(Messages.INTERNAL_ERROR_MSG);
        }
    }

    @Override
    public List<TeamMemberDto> getPossibleManagersForHiringManager(String companyId, String userId) {

        List<TeamMemberDto> managerList = new ArrayList<TeamMemberDto>();

        List<User> managerUserList = userApiExecutor.getPossibleManagersForHiringManager(companyId,userId);
        for (User user : managerUserList) {
            managerList.add(new TeamMemberDto(user.getId(), user.getFirstname(), user.getLastname()));
        }

        if (managerList.size() > 1) {
            Collections.sort(managerList, teamMemberNameComparator);
        }
        return managerList;
    }
}
