package com.fourdotfive.missioncontrol.notificatinpreference;

import java.util.ArrayList;
import java.util.List;

import com.fourdotfive.missioncontrol.dtos.notificationpreference.NotificationPrefsDetailsDto;
import com.fourdotfive.missioncontrol.pojo.notificationpreference.NotificationPreferenceType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import com.fourdotfive.missioncontrol.dtos.notificationpreference.NotificationPreferenceGroupDto;
import com.fourdotfive.missioncontrol.dtos.notificationpreference.NotificationPreferencesListDto;
import com.fourdotfive.missioncontrol.pojo.notificationpreference.NotificationPreferenceDetails;
import com.fourdotfive.missioncontrol.pojo.notificationpreference.NotificationPreferenceGroup;

@Service
public class DtoToNotificationPrefs
		implements
		Converter<List<NotificationPreferenceGroupDto>, List<NotificationPreferenceGroup>> {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(DtoToNotificationPrefs.class);

	@Override
	public List<NotificationPreferenceGroup> convert(
			List<NotificationPreferenceGroupDto> source) {
		List<NotificationPreferenceGroup> notificationPreferencesGroups = new ArrayList<NotificationPreferenceGroup>();

		for (NotificationPreferenceGroupDto groupDto : source) {
			NotificationPreferenceGroup notificationPreferencesGroup = new NotificationPreferenceGroup();
			notificationPreferencesGroup.setName(groupDto.getName());
			notificationPreferencesGroup.setNotificationGroupEnum(groupDto
					.getNotificationGroupEnum());
			notificationPreferencesGroup
					.setNotificationPreferenceList(getNotificationPrefs(groupDto
							.getNotificationPreferencesDtos()));
			notificationPreferencesGroups.add(notificationPreferencesGroup);
		}
		return notificationPreferencesGroups;
	}


	List<NotificationPreferenceGroup> convertOrderedNotificationPreferences(
			List<NotificationPreferenceGroupDto> source) {
		List<NotificationPreferenceGroup> notificationPreferencesGroups = new ArrayList<>();

		for (NotificationPreferenceGroupDto groupDto : source) {
			NotificationPreferenceGroup notificationPreferencesGroup = new NotificationPreferenceGroup();
			notificationPreferencesGroup.setName(
					groupDto.getName());
			notificationPreferencesGroup.setNotificationGroupEnum(
					groupDto.getNotificationGroupEnum());
			notificationPreferencesGroup.setNotificationPreferenceList(
					getOrderedNotificationPrefs(groupDto.getOrderedNotificationPreferences()));
			notificationPreferencesGroups.add(notificationPreferencesGroup);
		}
		return notificationPreferencesGroups;
	}

	private List<NotificationPreferenceDetails> getNotificationPrefs(
			List<NotificationPreferencesListDto> notificationPreferencesDtos) {

		List<NotificationPreferenceDetails> notificationPreferences = new ArrayList<NotificationPreferenceDetails>();
		for (NotificationPreferencesListDto dto : notificationPreferencesDtos) {
			NotificationPreferenceDetails notificationPreference = new NotificationPreferenceDetails();
			notificationPreference.setEnabledForApp(dto.isEnabledForApp());
			notificationPreference.setNotificationPreferencEnum(NotificationPreferenceType.fromText(dto.getNotificationPreferenceType()));
			notificationPreference.setEnabledForEmail(dto.isEnabledForEmail());
			notificationPreference.setEnabledForText(dto.isEnabledForText());
			notificationPreference.setName(dto.getName());
			notificationPreference.setOnForApp(dto.isOnForApp());
			notificationPreference.setOnForEmail(dto.isOnForEmail());
			notificationPreference.setOnForText(dto.isOnForText());
			notificationPreference.setEnabled(notificationPreference.isEnabled());
			notificationPreferences.add(notificationPreference);
		}
		return notificationPreferences;

	}

	private List<NotificationPreferenceDetails> getOrderedNotificationPrefs(
			List<NotificationPrefsDetailsDto> notificationPrefsDetailsDtos) {
		List<NotificationPreferenceDetails> notificationPreferenceDetailsList = new ArrayList<>();
		for (NotificationPrefsDetailsDto orderedNotificationPreferences :
				notificationPrefsDetailsDtos) {
			for (NotificationPreferencesListDto notificationPreferencesDto :
					orderedNotificationPreferences.getNotificationPreferencesListDtos()) {
				NotificationPreferenceDetails notificationPreference = new NotificationPreferenceDetails();
				notificationPreference.setEnabledForApp(notificationPreferencesDto.isEnabledForApp());
				notificationPreference.setNotificationPreferencEnum(NotificationPreferenceType.fromText(notificationPreferencesDto.getNotificationPreferenceType()));
				notificationPreference.setEnabledForEmail(notificationPreferencesDto.isEnabledForEmail());
				notificationPreference.setEnabledForText(notificationPreferencesDto.isEnabledForText());
				notificationPreference.setName(notificationPreferencesDto.getName());
				notificationPreference.setOnForApp(notificationPreferencesDto.isOnForApp());
				notificationPreference.setOnForEmail(notificationPreferencesDto.isOnForEmail());
				notificationPreference.setOnForText(notificationPreferencesDto.isOnForText());
				notificationPreference.setEnabled(notificationPreference.isEnabled());

				notificationPreferenceDetailsList.add(notificationPreference);

			}
		}

		return notificationPreferenceDetailsList;

	}
}
