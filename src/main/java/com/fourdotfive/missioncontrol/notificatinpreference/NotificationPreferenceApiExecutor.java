package com.fourdotfive.missioncontrol.notificatinpreference;

import com.fourdotfive.missioncontrol.pojo.notificationpreference.NotificationPreference;

public interface NotificationPreferenceApiExecutor {

	NotificationPreference getNotificationPrefs(String userId);

	NotificationPreference saveNotificationPrefs(NotificationPreference notificationPreference,
			String userId);

}
