package com.fourdotfive.missioncontrol.notificatinpreference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fourdotfive.missioncontrol.dtos.notificationpreference.NotificationPrefsDto;

/**
 * @author abhishek,chakravarthy
 */
@RestController
@RequestMapping("/api/notification")
public class NotificationPreferenceController {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(NotificationPreferenceController.class);

	@Autowired
	private NotificationPreferenceService notificationPreferenceService;

	/**
	 * To Get Roles based on role, this method is called.
	 * 
	 * @return List<NotificationPreferenceGroupDto>
	 */
	@RequestMapping(value = "/getnotificationprefs/{userid}/{companyid}", method = RequestMethod.GET)
	public ResponseEntity<NotificationPrefsDto> getNotificationPrefs(
			@PathVariable(value = "userid") String userId,
			@PathVariable(value = "companyid") String companyId) {
		NotificationPrefsDto response = notificationPreferenceService
				.getNotificationPrefs(companyId, userId);
		return new ResponseEntity<NotificationPrefsDto>(response, HttpStatus.OK);
	}

	/**
	 * To Save notification prefs, this method is called.
	 * 
	 * @return String
	 */
	@RequestMapping(value = "/savenotificationprefs/{userid}/{companyid}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<NotificationPrefsDto> saveNotificationPrefs(
			@RequestBody NotificationPrefsDto notificationDto,
			@PathVariable(value = "userid") String userId,
			@PathVariable(value = "companyid") String companyId) {
		NotificationPrefsDto response = notificationPreferenceService.saveNotificationPrefs(notificationDto, userId,companyId);
		return new ResponseEntity<NotificationPrefsDto>(response, HttpStatus.OK);
	}

}
