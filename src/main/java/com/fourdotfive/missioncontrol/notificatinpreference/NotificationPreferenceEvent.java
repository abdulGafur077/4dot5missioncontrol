package com.fourdotfive.missioncontrol.notificatinpreference;

import java.util.HashMap;
import java.util.Map;

class NotificationPreferenceEvent {
    private static final Map<String, String> allNotifyEvents = new HashMap<>();

    static {
        allNotifyEvents.put("TECH_ASSESSMENT_COMPLETED", "Technical Assessment Completed");
        allNotifyEvents.put("TECH_ASSESSMENT_SENT", "Technical Assessment Sent");
        allNotifyEvents.put("TECH_ASSESSMENT_CANCELED", "Technical Assessment Canceled");
        allNotifyEvents.put("TECH_ASSESSMENT_EXPIRED", "Technical Assessment Expired");
        allNotifyEvents.put("TECH_ASSESSMENT_SUSPENDED", "Technical Assessment Suspended");
        allNotifyEvents.put("TECH_ASSESSMENT_LEFT", "Technical Assessment Left");
        allNotifyEvents.put("TECH_ASSESSMENT_IN_PROGRESS", "Technical Assessment In Progress");
        allNotifyEvents.put("TECH_ASSESSMENT_TERMINATED", "Technical Assessment Terminated");
        allNotifyEvents.put("TECH_ASSESSMENT_REMINDER", "Technical Assessment Reminder");
        allNotifyEvents.put("PHONE_SCREEN_SCHEDULED", "Phone Screen Scheduled");
        allNotifyEvents.put("PHONE_SCREEN_RE_SCHEDULED", "Phone Screen Re-scheduled");
        allNotifyEvents.put("PHONE_SCREEN_CANCELED", "Phone Screen Canceled");
        allNotifyEvents.put("PHONE_SCREEN_COMPLETED", "Phone Screen Completed");
        allNotifyEvents.put("PHONE_SCREEN_OVERDUE", "Phone Screen Overdue");
        allNotifyEvents.put("INTERVIEW_SCHEDULED", "Interview Scheduled");
        allNotifyEvents.put("INTERVIEW_RE_SCHEDULED", "Interview Re-scheduled");
        allNotifyEvents.put("INTERVIEW_CANCELED", "Interview Canceled");
        allNotifyEvents.put("INTERVIEW_COMPLETED", "Interview Completed");
        allNotifyEvents.put("INTERVIEW_OVERDUE", "Interview Overdue");
        allNotifyEvents.put("VALUE_ASSESSMENT_COMPLETED", "Value Assessment Completed");
        allNotifyEvents.put("VALUE_ASSESSMENT_SENT", "Value Assessment Sent");
        allNotifyEvents.put("VALUE_ASSESSMENT_REMINDER", "Value Assessment Reminder");
        allNotifyEvents.put("VALUE_ASSESSMENT_IN_PROGRESS", "Value Assessment In Progress");
        allNotifyEvents.put("VALUE_ASSESSMENT_CANCELED", "Value Assessment Canceled");
        allNotifyEvents.put("VALUE_ASSESSMENT_EXPIRED", "Value Assessment Expired");
        allNotifyEvents.put("VALUE_ASSESSMENT_LEFT", "Value Assessment Left");
        allNotifyEvents.put("VERIFICATION_COMPLETED", "Candidate Verification completed");
        allNotifyEvents.put("VERIFICATION_OVERDUE", "Candidate Verification overdue");
    }

    static String getNotifyEvent(String name) {
        return allNotifyEvents.get(name);
    }
}
