package com.fourdotfive.missioncontrol.notificatinpreference;

public class NotificationPreferenceException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3426579929773860273L;

	private String message;

	public String getMessage() {
		return message;
	}
	
	public NotificationPreferenceException() {
	}

	public NotificationPreferenceException(String message, Object... args) {
		this.message = String.format(message, args); 
	}
	
	@Override
	public String toString() {
		return "NotificationPreferenceException [message=" + message + "]";
	}
}
