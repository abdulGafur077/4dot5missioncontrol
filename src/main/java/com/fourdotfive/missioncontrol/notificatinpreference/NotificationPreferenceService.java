package com.fourdotfive.missioncontrol.notificatinpreference;

import com.fourdotfive.missioncontrol.dtos.notificationpreference.NotificationPrefsDto;

public interface NotificationPreferenceService {

	NotificationPrefsDto getNotificationPrefs(String compId,
			String userId);

	NotificationPrefsDto saveNotificationPrefs(NotificationPrefsDto notificationPreference,
			String userId, String companyId);
}
