package com.fourdotfive.missioncontrol.notificatinpreference;

import com.fourdotfive.missioncontrol.dtos.notificationpreference.*;
import com.fourdotfive.missioncontrol.dtos.user.UserDto;
import com.fourdotfive.missioncontrol.dtos.user.UserPersonalDetailsDto;
import com.fourdotfive.missioncontrol.licensepreference.LicensePrefDtoList;
import com.fourdotfive.missioncontrol.licensepreference.LicensePrefEnum;
import com.fourdotfive.missioncontrol.licensepreference.LicensePrefService;
import com.fourdotfive.missioncontrol.pojo.notificationpreference.*;
import com.fourdotfive.missioncontrol.pojo.setting.LicensePreferenceSetting;
import com.fourdotfive.missioncontrol.pojo.setting.NotifyEvent;
import com.fourdotfive.missioncontrol.pojo.setting.NotifyEventEnum;
import com.fourdotfive.missioncontrol.pojo.setting.Setting;
import com.fourdotfive.missioncontrol.setting.SettingApiExecutor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class NotificationPreferenceServiceImp implements NotificationPreferenceService {

	@Autowired
	private LicensePrefService licensePrefService;

	@Autowired
	private NotificationPreferenceToDto notificationPreferenceToDto;

	@Autowired
	private DtoToNotificationPrefs dtoToNotificationPrefs;

	@Autowired
	private NotificationPreferenceApiExecutor notificationApiExecutor;

	@Autowired
	private SettingApiExecutor apiExecutor;

	public static final Logger LOGGER = LoggerFactory.getLogger(NotificationPreferenceServiceImp.class);

	private static final List<NotificationPreferenceEnum> TECH_ASSESSMENT_NOTIFY_EVENTS = Arrays.asList( NotificationPreferenceEnum.TECH_ASSESSMENT_SENT,  NotificationPreferenceEnum.TECH_ASSESSMENT_REMINDER,
			NotificationPreferenceEnum.TECH_ASSESSMENT_IN_PROGRESS, NotificationPreferenceEnum.TECH_ASSESSMENT_COMPLETED, NotificationPreferenceEnum.TECH_ASSESSMENT_EXPIRED, NotificationPreferenceEnum.TECH_ASSESSMENT_CANCELED,
			NotificationPreferenceEnum.TECH_ASSESSMENT_LEFT, NotificationPreferenceEnum.TECH_ASSESSMENT_SUSPENDED, NotificationPreferenceEnum.TECH_ASSESSMENT_TERMINATED);

	private static final List<NotificationPreferenceEnum> PHONE_SCREEN_NOTIFY_EVENTS = Arrays.asList( NotificationPreferenceEnum.PHONE_SCREEN_SCHEDULED,  NotificationPreferenceEnum.PHONE_SCREEN_IN_PROGRESS,
			NotificationPreferenceEnum.PHONE_SCREEN_COMPLETED, NotificationPreferenceEnum.PHONE_SCREEN_OVERDUE, NotificationPreferenceEnum.PHONE_SCREEN_RE_SCHEDULED, NotificationPreferenceEnum.PHONE_SCREEN_CANCELED);

	private static final List<NotificationPreferenceEnum> INTERVIEW_NOTIFY_EVENTS = Arrays.asList( NotificationPreferenceEnum.INTERVIEW_SCHEDULED, NotificationPreferenceEnum.INTERVIEW_IN_PROGRESS, NotificationPreferenceEnum.INTERVIEW_COMPLETED,
			NotificationPreferenceEnum.INTERVIEW_OVERDUE, NotificationPreferenceEnum.INTERVIEW_RE_SCHEDULED, NotificationPreferenceEnum.INTERVIEW_CANCELED);

	private static final List<NotificationPreferenceEnum> VALUE_ASSESSMENT_NOTIFY_EVENTS = Arrays.asList(NotificationPreferenceEnum.VALUE_ASSESSMENT_SENT, NotificationPreferenceEnum.VALUE_ASSESSMENT_REMINDER,
			NotificationPreferenceEnum.VALUE_ASSESSMENT_IN_PROGRESS, NotificationPreferenceEnum.VALUE_ASSESSMENT_COMPLETED, NotificationPreferenceEnum.VALUE_ASSESSMENT_EXPIRED, NotificationPreferenceEnum.VALUE_ASSESSMENT_CANCELED,
			NotificationPreferenceEnum.VALUE_ASSESSMENT_LEFT, NotificationPreferenceEnum.VALUE_ASSESSMENT_SUSPENDED, NotificationPreferenceEnum.VALUE_ASSESSMENT_TERMINATED);

	@Override
	public NotificationPrefsDto getNotificationPrefs(String compId, String userId) {
		NotificationPreference notificationPrefs = notificationApiExecutor.getNotificationPrefs(userId);
		LicensePrefDtoList dtoList = licensePrefService.getStaffOrCorpLicensePref(compId);
		List<NotificationPreferenceGroupDto> groupDtos = notificationPreferenceToDto
				.convert(new NotificationPrefsWithLicensePrefs(notificationPrefs.getNotificationList(), dtoList));

		Setting setting = apiExecutor.getStaffingOrCorpSettings(compId, true);
		if (setting != null) {
			checkSettings(setting, groupDtos);
		}
		NotificationPrefsDto notificationPrefsDto = new NotificationPrefsDto();
		notificationPrefsDto.setId(notificationPrefs.getId());
		notificationPrefsDto.setGroups(groupDtos);
		setOrderedNotificationPreferences(groupDtos);

		UserDto dto = new UserDto();
		UserPersonalDetailsDto userDetails = new UserPersonalDetailsDto();
		userDetails.setFirstName(notificationPrefs.getUser().getFirstname());
		userDetails.setLastName(notificationPrefs.getUser().getLastname());
		userDetails.setUserId(notificationPrefs.getUser().getId());
		dto.setUserDetails(userDetails);
		notificationPrefsDto.setUser(dto);
		
		return notificationPrefsDto;
	}

	@Override
	public NotificationPrefsDto saveNotificationPrefs(NotificationPrefsDto notificationPreferenceDto, String userId,
			String companyId) {
		LOGGER.debug("saving notification prefs for userid {}", userId);

		NotificationPreference notificationPreference = new NotificationPreference();
		notificationPreference.setId(notificationPreferenceDto.getId());
		List<NotificationPreferenceGroup> groups = dtoToNotificationPrefs
				.convertOrderedNotificationPreferences(notificationPreferenceDto.getGroups());
		notificationPreference.setNotificationList(groups);
		NotificationPreference notificationPrefs = notificationApiExecutor.saveNotificationPrefs(notificationPreference,
				userId);

		LicensePrefDtoList dtoList = licensePrefService.getStaffOrCorpLicensePref(companyId);
		List<NotificationPreferenceGroupDto> groupDtos = notificationPreferenceToDto
				.convert(new NotificationPrefsWithLicensePrefs(notificationPrefs.getNotificationList(), dtoList));

		NotificationPrefsDto notificationPrefsDto = new NotificationPrefsDto();
		notificationPrefsDto.setId(notificationPrefs.getId());
		notificationPrefsDto.setGroups(groupDtos);
		setOrderedNotificationPreferences(groupDtos);


		UserDto dto = new UserDto();
		UserPersonalDetailsDto userDetails = new UserPersonalDetailsDto();
		userDetails.setFirstName(notificationPrefs.getUser().getFirstname());
		userDetails.setLastName(notificationPrefs.getUser().getLastname());
		userDetails.setUserId(notificationPrefs.getUser().getId());
		dto.setUserDetails(userDetails);
		notificationPrefsDto.setUser(dto);

		return notificationPrefsDto;
	}

	private void checkSettings(Setting setting, List<NotificationPreferenceGroupDto> groupDtos) {
		Map<LicensePrefEnum, LicensePreferenceSetting> licensePreferenceSetting = setting.getLicensePreferenceSetting();
		for (Map.Entry<LicensePrefEnum, LicensePreferenceSetting> licensePreferenceSettingEntry : licensePreferenceSetting.entrySet()) {
			LinkedHashMap<NotifyEventEnum, NotifyEvent> notifyEvents = licensePreferenceSettingEntry.getValue().getNotifyEvents();
			for (Map.Entry<NotifyEventEnum, NotifyEvent> notifyEventEntry : notifyEvents.entrySet()) {
				if (!notifyEventEntry.getValue().isDetectOn()) {
					updateNotificationPreferences(groupDtos, String.valueOf(notifyEventEntry.getKey()));
				}
			}
		}
	}

	private void updateNotificationPreferences(List<NotificationPreferenceGroupDto> groupDtos, String notifyEventName) {
		for (NotificationPreferenceGroupDto notificationPreferenceGroupDto : groupDtos) {
			if (notificationPreferenceGroupDto.getNotificationGroupEnum() == NotificationGroupEnum.OCCURANCE_AND_NON_OCCURANCE_ALERT) {
				for (NotificationPreferencesListDto notificationPreferencesListDto : notificationPreferenceGroupDto.getNotificationPreferencesDtos()) {
					String notificationPreferenceName = NotificationPreferenceEvent.getNotifyEvent(notifyEventName);
					if (StringUtils.equals(notificationPreferencesListDto.getName(), notificationPreferenceName)) {
						notificationPreferencesListDto.setEnabled(false);
						return;
					}
				}
			}
		}
	}

	private void setOrderedNotificationPreferences(List<NotificationPreferenceGroupDto> groupDtos) {
		Map<String, List<NotificationPreferencesListDto>> occurrenceOrderedNotificationPreferences = new LinkedHashMap<>();
		occurrenceOrderedNotificationPreferences.put(NotificationPreferenceType.TECH_ASSESSMENT.getText(), new ArrayList<NotificationPreferencesListDto>());
		occurrenceOrderedNotificationPreferences.put(NotificationPreferenceType.VALUE_ASSESSMENT.getText(), new ArrayList<NotificationPreferencesListDto>());
		occurrenceOrderedNotificationPreferences.put(NotificationPreferenceType.VERIFICATION.getText(), new ArrayList<NotificationPreferencesListDto>());
		occurrenceOrderedNotificationPreferences.put(NotificationPreferenceType.PHONE_SCREEN.getText(), new ArrayList<NotificationPreferencesListDto>());
		occurrenceOrderedNotificationPreferences.put(NotificationPreferenceType.INTERVIEW.getText(), new ArrayList<NotificationPreferencesListDto>());

		Map<String, List<NotificationPreferencesListDto>> requisitionAlertOrderedNotificationPreferences = new LinkedHashMap<>();

		Map<String, List<NotificationPreferencesListDto>> candidateAlertOrderedNotificationPreferences = new LinkedHashMap<>();

		for (NotificationPreferenceGroupDto notificationPreferenceGroupDto : groupDtos) {
			if (notificationPreferenceGroupDto.getNotificationGroupEnum() == NotificationGroupEnum.OCCURANCE_AND_NON_OCCURANCE_ALERT) {
				setNotificationPreferenceOrder(notificationPreferenceGroupDto, occurrenceOrderedNotificationPreferences);
			}
			if (notificationPreferenceGroupDto.getNotificationGroupEnum() == NotificationGroupEnum.REQUISITION_ALERT) {
				setNotificationPreferenceOrder(notificationPreferenceGroupDto, requisitionAlertOrderedNotificationPreferences);
			}
			if (notificationPreferenceGroupDto.getNotificationGroupEnum() == NotificationGroupEnum.CANDIDATE_ALERT) {
				setNotificationPreferenceOrder(notificationPreferenceGroupDto, candidateAlertOrderedNotificationPreferences);
			}
		}
	}

	private void setNotificationPreferenceOrder(NotificationPreferenceGroupDto notificationPreferenceGroupDto, Map<String, List<NotificationPreferencesListDto>> orderedNotificationPreferences) {
		for (NotificationPreferencesListDto notificationPreferencesListDto : notificationPreferenceGroupDto.getNotificationPreferencesDtos()) {
			if (orderedNotificationPreferences.containsKey(notificationPreferencesListDto.getNotificationPreferenceType()))
				orderedNotificationPreferences.get(notificationPreferencesListDto.getNotificationPreferenceType()).add(notificationPreferencesListDto);
		}

		Map<String, List<NotificationPreferencesListDto>> newOrderedNotificationPreference = new LinkedHashMap<>();
		newOrderedNotificationPreference.put(NotificationPreferenceType.TECH_ASSESSMENT.getText(), new ArrayList<NotificationPreferencesListDto>());
		newOrderedNotificationPreference.put(NotificationPreferenceType.VALUE_ASSESSMENT.getText(), new ArrayList<NotificationPreferencesListDto>());
		newOrderedNotificationPreference.put(NotificationPreferenceType.VERIFICATION.getText(), new ArrayList<NotificationPreferencesListDto>());
		newOrderedNotificationPreference.put(NotificationPreferenceType.PHONE_SCREEN.getText(), new ArrayList<NotificationPreferencesListDto>());
		newOrderedNotificationPreference.put(NotificationPreferenceType.INTERVIEW.getText(), new ArrayList<NotificationPreferencesListDto>());
		for (Map.Entry<String, List<NotificationPreferencesListDto>> entry : orderedNotificationPreferences.entrySet()) {
			if (entry.getKey().equals(NotificationPreferenceEnum.TECH_ASSESSMENT.getDescription())) {
				for (NotificationPreferenceEnum notificationPreferenceEnum : TECH_ASSESSMENT_NOTIFY_EVENTS) {
					for (NotificationPreferencesListDto notificationPreferencesListDto : notificationPreferenceGroupDto.getNotificationPreferencesDtos()) {
						if (notificationPreferencesListDto.getName().equals(notificationPreferenceEnum.getDescription())) {
							newOrderedNotificationPreference.get(notificationPreferencesListDto.getNotificationPreferenceType()).add(notificationPreferencesListDto);
						}
					}
				}
			}
			if (entry.getKey().equals(NotificationPreferenceEnum.VALUE_ASSESSMENT.getDescription())) {
				for (NotificationPreferenceEnum notificationPreferenceEnum : VALUE_ASSESSMENT_NOTIFY_EVENTS) {
					for (NotificationPreferencesListDto notificationPreferencesListDto : notificationPreferenceGroupDto.getNotificationPreferencesDtos()) {
						if (notificationPreferencesListDto.getName().equals(notificationPreferenceEnum.getDescription())) {
							newOrderedNotificationPreference.get(notificationPreferencesListDto.getNotificationPreferenceType()).add(notificationPreferencesListDto);
						}
					}
				}
			}
			if (entry.getKey().equals(NotificationPreferenceEnum.VERIFICATION.getDescription())) {
				for (NotificationPreferencesListDto notificationPreferencesListDto : entry.getValue()) {
					newOrderedNotificationPreference.get(notificationPreferencesListDto.getNotificationPreferenceType()).add(notificationPreferencesListDto);
				}
			}
			if (entry.getKey().equals(NotificationPreferenceEnum.PHONE_SCREEN.getDescription())) {
				List<NotificationPreferencesListDto> notificationPreferencesListDtos = new ArrayList<>();
				Map<String, List<NotificationPreferencesListDto>> phoneScreenNotificationPreference = new LinkedHashMap<>();
				phoneScreenNotificationPreference.put(NotificationPreferenceType.PHONE_SCREEN.getText(), new ArrayList<NotificationPreferencesListDto>());
				for (NotificationPreferenceEnum notificationPreferenceEnum : PHONE_SCREEN_NOTIFY_EVENTS) {
					for (NotificationPreferencesListDto notificationPreferencesListDto : notificationPreferenceGroupDto.getNotificationPreferencesDtos()) {
						if (notificationPreferencesListDto.getName().equals(notificationPreferenceEnum.getDescription())) {
							notificationPreferencesListDtos.add(notificationPreferencesListDto);
							phoneScreenNotificationPreference.get(notificationPreferencesListDto.getNotificationPreferenceType()).add(notificationPreferencesListDto);
						}
					}
				}
				if (phoneScreenNotificationPreference.get(NotificationPreferenceEnum.PHONE_SCREEN.getDescription()).size() < PHONE_SCREEN_NOTIFY_EVENTS.size()) {
					for (NotificationPreferenceEnum notificationPreferenceEnum : PHONE_SCREEN_NOTIFY_EVENTS) {
						for (NotificationPreferencesListDto notificationPreferencesListDto : phoneScreenNotificationPreference.get(NotificationPreferenceEnum.PHONE_SCREEN.getDescription())) {
							if (StringUtils.equals(notificationPreferencesListDto.getName(), notificationPreferenceEnum.getDescription())) {
								newOrderedNotificationPreference.get(notificationPreferencesListDto.getNotificationPreferenceType()).add(notificationPreferencesListDto);
								break;
							} else {
								setNotificationPreference(notificationPreferenceEnum, notificationPreferencesListDto, newOrderedNotificationPreference);
								break;
							}
						}
					}
				}else{
					newOrderedNotificationPreference.get(NotificationPreferenceEnum.PHONE_SCREEN.getDescription()).addAll(notificationPreferencesListDtos);
				}
			}
			if (entry.getKey().equals(NotificationPreferenceEnum.INTERVIEW.getDescription())) {
				List<NotificationPreferencesListDto> notificationPreferencesListDtos = new ArrayList<>();
				Map<String, List<NotificationPreferencesListDto>> interviewNotificationPreference = new LinkedHashMap<>();
				interviewNotificationPreference.put(NotificationPreferenceType.INTERVIEW.getText(), new ArrayList<NotificationPreferencesListDto>());
				for (NotificationPreferenceEnum notificationPreferenceEnum : INTERVIEW_NOTIFY_EVENTS) {
					for (NotificationPreferencesListDto notificationPreferencesListDto : notificationPreferenceGroupDto.getNotificationPreferencesDtos()) {
						if (notificationPreferencesListDto.getName().equals(notificationPreferenceEnum.getDescription())) {
							notificationPreferencesListDtos.add(notificationPreferencesListDto);
							interviewNotificationPreference.get(notificationPreferencesListDto.getNotificationPreferenceType()).add(notificationPreferencesListDto);
						}
					}
				}
				if (interviewNotificationPreference.get(NotificationPreferenceEnum.INTERVIEW.getDescription()).size() < INTERVIEW_NOTIFY_EVENTS.size()) {
					for (NotificationPreferenceEnum notificationPreferenceEnum : INTERVIEW_NOTIFY_EVENTS) {
						for (NotificationPreferencesListDto notificationPreferencesListDto : interviewNotificationPreference.get(NotificationPreferenceEnum.INTERVIEW.getDescription())) {
							if (StringUtils.equals(notificationPreferencesListDto.getName(), notificationPreferenceEnum.getDescription())) {
								newOrderedNotificationPreference.get(notificationPreferencesListDto.getNotificationPreferenceType()).add(notificationPreferencesListDto);
								break;
							} else {
								setNotificationPreference(notificationPreferenceEnum, notificationPreferencesListDto, newOrderedNotificationPreference);
								break;
							}
						}
					}
				}else{
					newOrderedNotificationPreference.get(NotificationPreferenceEnum.INTERVIEW.getDescription()).addAll(notificationPreferencesListDtos);
				}
			}
		}

		List<NotificationPrefsDetailsDto> orderedNotificationPreferencesList = new ArrayList<>();
		for (Map.Entry<String, List<NotificationPreferencesListDto>> entry : newOrderedNotificationPreference.entrySet()) {
			NotificationPrefsDetailsDto notificationPrefsDetailsDto = new NotificationPrefsDetailsDto();
			notificationPrefsDetailsDto.setNotificationPreferenceType(entry.getKey());
			notificationPrefsDetailsDto.setNotificationPreferencesListDtos(entry.getValue());
			orderedNotificationPreferencesList.add(notificationPrefsDetailsDto);
		}
		notificationPreferenceGroupDto.setOrderedNotificationPreferences(orderedNotificationPreferencesList);
	}

	private void setNotificationPreference(NotificationPreferenceEnum notificationPreferenceEnum, NotificationPreferencesListDto notificationPreferencesListDto,
										   Map<String, List<NotificationPreferencesListDto>> newOrderedNotificationPreference) {

		NotificationPreferencesListDto notificationPreference = new NotificationPreferencesListDto();
		notificationPreference.setEnabled(true);
		notificationPreference.setName(notificationPreferenceEnum.getDescription());
		notificationPreference.setOnForApp(true);
		notificationPreference.setEnabledForEmail(true);
		notificationPreference.setEnabledForText(true);
		notificationPreference.setOnForEmail(true);
		notificationPreference.setOnForText(true);
		notificationPreference.setNotificationPreferenceType(notificationPreferencesListDto.getNotificationPreferenceType());
		newOrderedNotificationPreference.get(notificationPreferencesListDto.getNotificationPreferenceType()).add(notificationPreference);
	}
}
