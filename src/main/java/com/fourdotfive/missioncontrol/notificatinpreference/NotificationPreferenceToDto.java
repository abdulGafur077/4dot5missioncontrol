package com.fourdotfive.missioncontrol.notificatinpreference;

import com.fourdotfive.missioncontrol.common.Messages;
import com.fourdotfive.missioncontrol.dtos.notificationpreference.NotificationPreferenceGroupDto;
import com.fourdotfive.missioncontrol.dtos.notificationpreference.NotificationPreferencesListDto;
import com.fourdotfive.missioncontrol.dtos.notificationpreference.NotificationPrefsWithLicensePrefs;
import com.fourdotfive.missioncontrol.licensepreference.LicensePrefDetails;
import com.fourdotfive.missioncontrol.licensepreference.LicensePrefDto;
import com.fourdotfive.missioncontrol.licensepreference.LicensePrefDtoList;
import com.fourdotfive.missioncontrol.pojo.notificationpreference.NotificationPreferenceDetails;
import com.fourdotfive.missioncontrol.pojo.notificationpreference.NotificationPreferenceGroup;
import com.fourdotfive.missioncontrol.pojo.notificationpreference.NotificationPreferenceType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class NotificationPreferenceToDto
		implements Converter<NotificationPrefsWithLicensePrefs, List<NotificationPreferenceGroupDto>> {

	private static final Logger LOGGER = LoggerFactory.getLogger(NotificationPreferenceToDto.class);

	@Override
	public List<NotificationPreferenceGroupDto> convert(NotificationPrefsWithLicensePrefs source) {
		List<NotificationPreferenceGroupDto> groupDtos = new ArrayList<NotificationPreferenceGroupDto>();
		for (NotificationPreferenceGroup group : source.getGroups()) {
			NotificationPreferenceGroupDto groupDto = new NotificationPreferenceGroupDto();
			groupDto.setName(group.getName());
			groupDto.setNotificationGroupEnum(group.getNotificationGroupEnum());
			List<NotificationPreferencesListDto> listDtos = getNotificationPrefsDtoFromNotificationPrefs(
					group.getNotificationPreferenceList(), source.getLicensePrefDtoList());
			groupDto.setNotificationPreferencesDtos(listDtos);
			groupDtos.add(groupDto);
		}
		return groupDtos;
	}

	private List<NotificationPreferencesListDto> getNotificationPrefsDtoFromNotificationPrefs(
			List<NotificationPreferenceDetails> notificationPreferences, LicensePrefDtoList licensePrefDtoList) {

		List<NotificationPreferencesListDto> dtos = new ArrayList<NotificationPreferencesListDto>();

		for (NotificationPreferenceDetails np : notificationPreferences) {

			NotificationPreferencesListDto dto = new NotificationPreferencesListDto();
			dto.setName(np.getName());
			dto.setEnabledForApp(np.isEnabledForApp());
			dto.setEnabledForEmail(np.isEnabledForEmail());
			dto.setEnabledForText(np.isEnabledForText());
			dto.setOnForApp(np.isOnForApp());
			dto.setOnForEmail(np.isOnForEmail());
			dto.setOnForText(np.isOnForText());
			dto.setNotificationPreferenceType(np.getNotificationPreferencEnum().getText());
			dtos.add(dto);
			dto.setEnabled(true);

		}
		return dtos;

	}

	private class Util {

		boolean flag;
		String message;

		public boolean isFlag() {
			return flag;
		}

		public void setFlag(boolean flag) {
			this.flag = flag;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		@Override
		public String toString() {
			return "Util [flag=" + flag + ", message=" + message + "]";
		}
	}

	private Util compareLicensePrefs(NotificationPreferenceDetails notificationPreference,
			LicensePrefDtoList licensePrefDtoList) {

		Util util = new Util();
		util.setFlag(true);

		if (notificationPreference.getNotificationPreferencEnum()
				.equals(NotificationPreferenceType.FOURDOTFIVE_INTELLIGENCE)) {
			LicensePrefDto licensePrefDto = licensePrefDtoList.getFourDotFiveIntellList();
			for (LicensePrefDetails details : licensePrefDto.getLicensePrefDetailsList()) {
				if (!(details.isEnabled() && details.isSelected())) {
					util.setFlag(false);
					util.setMessage(details.getMessage());
				}
			}
		} else if (notificationPreference.getNotificationPreferencEnum()
				.equals(NotificationPreferenceType.TECH_ASSESSMENT)) {
			LicensePrefDto licensePrefDto = licensePrefDtoList.getTechAssessmentList();
			for (LicensePrefDetails details : licensePrefDto.getLicensePrefDetailsList()) {
				if (!(details.isEnabled() && details.isSelected())) {
					util.setFlag(false);
					util.setMessage(details.getMessage());
				}
			}
		} else if (notificationPreference.getNotificationPreferencEnum()
				.equals(NotificationPreferenceType.VALUE_ASSESSMENT)) {
			LicensePrefDto licensePrefDto = licensePrefDtoList.getValueAssessmentList();
			for (LicensePrefDetails details : licensePrefDto.getLicensePrefDetailsList()) {
				if (!(details.isEnabled() && details.isSelected())) {
					util.setFlag(false);
					util.setMessage(details.getMessage());
				}
			}
		} else if (notificationPreference.getNotificationPreferencEnum()
				.equals(NotificationPreferenceType.PHONE_SCREEN)) {
			LicensePrefDto licensePrefDto = licensePrefDtoList.getPhoneScreenList();
			for (LicensePrefDetails details : licensePrefDto.getLicensePrefDetailsList()) {
				if (!(details.isEnabled() && details.isSelected())) {
					util.setFlag(false);
					util.setMessage(details.getMessage());
				}
			}
		} else if (notificationPreference.getNotificationPreferencEnum()
				.equals(NotificationPreferenceType.INTERVIEW)) {
			LicensePrefDto licensePrefDto = licensePrefDtoList.getInterviewList();
			for (LicensePrefDetails details : licensePrefDto.getLicensePrefDetailsList()) {
				if (!(details.isEnabled() && details.isSelected())) {
					util.setFlag(false);
					util.setMessage(details.getMessage());
				}
			}
		} else if (notificationPreference.getNotificationPreferencEnum()
				.equals(NotificationPreferenceType.VERIFICATION)) {
			LicensePrefDto licensePrefDto = licensePrefDtoList.getVerificationList();
			util.setFlag(false);
			int notAvailableCount = 0;
			int notAvailableAtTheMomentCount = 0;
			StringBuilder message = new StringBuilder();
			for (LicensePrefDetails details : licensePrefDto.getLicensePrefDetailsList()) {
				if (!(details.isEnabled() && details.isSelected())) {
					if (util.isFlag() != true) {
						util.setFlag(false);
					}
					if (details.getMessage() != null) {
						if (details.getMsgVal() == 1) {
							notAvailableCount++;
						} else if (details.getMsgVal() == 2) {
							notAvailableAtTheMomentCount++;
						}
						message.append(details.getMessage()).append(" ");
						util.setMessage(message.toString());
					}
				} else {
					util.setFlag(true);
				}
			}

			if(notAvailableCount == 2){
				util.setMessage(Messages.BOTH_NOT_AVAILABLE);
			}else if(notAvailableAtTheMomentCount == 2){
				util.setMessage(Messages.BOTH_NOT_AVAILABLE_AT_THE_MOMENT);
			}
		}

		return util;
	}

}
