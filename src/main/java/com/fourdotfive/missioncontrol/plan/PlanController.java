package com.fourdotfive.missioncontrol.plan;

import com.fourdotfive.missioncontrol.dtos.plan.PlanDetailDto;
import com.fourdotfive.missioncontrol.dtos.plan.PlanDto;
import com.fourdotfive.missioncontrol.dtos.plan.PlanHeaderDto;
import com.fourdotfive.missioncontrol.exception.PlatformException;
import com.fourdotfive.missioncontrol.pojo.company.CompanyType;
import com.fourdotfive.missioncontrol.pojo.plan.PlanFeaturesCapability;
import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.user.UserService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/api/plan")
public class PlanController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PlanController.class);

    private final PlanService planService;

    @Autowired
    private UserService userService;

    @Autowired
    public PlanController(PlanService planService) {
        this.planService = planService;
    }


    @RequestMapping(value = {"getallplanfeatures"}, method = RequestMethod.GET)
    public List<PlanFeaturesCapability> getAllPlanFeatures() {
        return planService.getAllPlanFeatures();
    }

    @RequestMapping(value = {"getallplanfeatures/{companyType}"}, method = RequestMethod.GET)
    public List<PlanFeaturesCapability> getAllPlanFeatures(@PathVariable(value = "companyType") String companyType,
                                                           @RequestParam(value = "companyId") String companyId) {
        if (StringUtils.isEmpty(companyId)) {
            throw new IllegalArgumentException("Company Id is not found");
        }
        return planService.getAllPlanFeatures(companyType, companyId);
    }

    @RequestMapping(value = "findplanfeaturescapabilitybyid/{id}", method = {RequestMethod.GET})
    public @ResponseBody
    ResponseEntity<PlanFeaturesCapability> findPlanFeaturesCapabilityById(@PathVariable(value = "id") String id) {
        if (StringUtils.isEmpty(id)) {
            throw new IllegalArgumentException("PlanFeaturesCapabilityId is not found");
        }
        PlanFeaturesCapability planFeaturesCapability = planService.findPlanFeaturesCapabilityById(id);
        if (planFeaturesCapability == null)
            return new ResponseEntity<>(planFeaturesCapability, HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(planFeaturesCapability, HttpStatus.OK);
    }

    @RequestMapping(value = "findplanfeaturescapabilitybyplan", method = {RequestMethod.GET})
    public @ResponseBody
    ResponseEntity<PlanFeaturesCapability> findPlanFeaturesCapabilityByPlan(@RequestParam(value = "plan") String plan) {
        if (StringUtils.isEmpty(plan)) {
            throw new IllegalArgumentException("plan is not found");
        }
        PlanFeaturesCapability planFeaturesCapability = planService.findPlanFeaturesCapabilityByPlan(plan);
        if (planFeaturesCapability == null)
            return new ResponseEntity<>(planFeaturesCapability, HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(planFeaturesCapability, HttpStatus.OK);
    }

    @RequestMapping(value = "findplanheaderbyid/{planHeaderId}", method = {RequestMethod.GET})
    public @ResponseBody
    ResponseEntity<PlanHeaderDto> findPlanHeaderById(@PathVariable String planHeaderId) {
        if (StringUtils.isEmpty(planHeaderId)) {
            throw new IllegalArgumentException("planHeaderId is not found");
        }
        PlanHeaderDto response = planService.findPlanHeaderById(planHeaderId);
        if (response == null)
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "findplanheaderbycompanyid/{companyId}", method = {RequestMethod.GET})
    public @ResponseBody
    ResponseEntity<PlanHeaderDto> findPlanHeaderByCompanyId(@PathVariable String companyId) {
        if (StringUtils.isEmpty(companyId)) {
            throw new IllegalArgumentException("companyId is not found");
        }
        PlanHeaderDto response = planService.findPlanHeaderByCompanyId(companyId);
        if (response == null)
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @RequestMapping(value = "findplandetailbyid/{planDetailId}", method = {RequestMethod.GET})
    public @ResponseBody
    ResponseEntity<PlanDetailDto> findPlanDetailById(@PathVariable String planDetailId) {
        if (StringUtils.isEmpty(planDetailId)) {
            throw new IllegalArgumentException("planDetailId is not found");
        }
        PlanDetailDto response = planService.findPlanDetailById(planDetailId);
        if (response == null)
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "findplandetailbyplanheaderid/{planHeaderId}", method = {RequestMethod.GET})
    public @ResponseBody
    ResponseEntity<PlanDetailDto> findPlanDetailByPlanHeaderId(@PathVariable String planHeaderId) {
        if (StringUtils.isEmpty(planHeaderId)) {
            throw new IllegalArgumentException("planHeaderId is not found");
        }
        PlanDetailDto response = planService.findPlanDetailByPlanHeaderId(planHeaderId);
        if (response == null)
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "saveplan", method = {RequestMethod.POST})
    public @ResponseBody
    ResponseEntity<PlanDto> savePlan(@RequestBody PlanDto planDto) {
        if (planDto.getPlanHeaderDto() == null || StringUtils.isEmpty(planDto.getPlanHeaderDto().getTimeZoneName())) {
            throw new IllegalArgumentException("Time zone name is not found");
        }
        User user = userService.getCurrentLoggedInUser();
        if (user != null)
            planDto.setLoggedInUserId(user.getId());
        PlanDto response = planService.savePlan(planDto);
        if (response == null)
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "findplanbyplanheaderid/{planHeaderId}", method = {RequestMethod.GET})
    public @ResponseBody
    ResponseEntity<PlanDto> findPlanByPlanHeaderId(@PathVariable String planHeaderId) {
        if (StringUtils.isEmpty(planHeaderId)) {
            throw new IllegalArgumentException("planHeaderId is not found");
        }
        PlanDto response = planService.findPlanByPlanHeaderId(planHeaderId);
        if (response == null)
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "findplanbycompanyid/{companyId}", method = {RequestMethod.GET})
    public @ResponseBody
    ResponseEntity<PlanDto> findPlanByCompanyId(@PathVariable String companyId) {
        if (StringUtils.isEmpty(companyId)) {
            throw new IllegalArgumentException("companyId is not found");
        }
        PlanDto response = planService.findPlanByCompanyId(companyId);
        if (response == null)
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = {"/getplandetails/{companyId}"}, method = RequestMethod.GET)
    public String getPlanDetials(@PathVariable(value = "companyId") String companyId) {
        if (StringUtils.isEmpty(companyId)) {
            throw new PlatformException("Company id can not be null.");
        }
        return planService.getPlanDetails(companyId);
    }

    @RequestMapping(value = {"findcurrentplanbycompanyid/{companyId}"}, method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<PlanDto> findCurrentPlanByCompanyId(@PathVariable String companyId) {
        PlanDto response = planService.findCurrentPlanByCompanyId(companyId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = {"findfutureplanbycompanyid/{companyId}"}, method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<PlanDto> findFuturePlanByCompanyId(@PathVariable String companyId) {
        PlanDto response = planService.findFuturePlanByCompanyId(companyId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = {"findcurrentplanheaderbycompanyid/{companyId}"}, method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<PlanHeaderDto> findCurrentPlanHeaderByCompanyId(@PathVariable String companyId) {
        PlanHeaderDto response = planService.findCurrentPlanHeaderByCompanyId(companyId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = {"findfutureplanheaderbycompanyid/{companyId}"}, method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<PlanHeaderDto> findFuturePlanHeaderByCompanyId(@PathVariable String companyId) {
        PlanHeaderDto response = planService.findFuturePlanHeaderByCompanyId(companyId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = {"getavailabletechassessmentscount/{companyId}"}, method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<Map<String, Integer>> getAvailableTechAssessmentsCount(@PathVariable String companyId,
                                                                          @RequestParam(value = "jobId", required = false) String jobId) {
        if (StringUtils.isEmpty(companyId)) {
            throw new PlatformException("Company id can not be null.");
        }
//        if (StringUtils.isEmpty(jobId)) {
//            throw new PlatformException("Job id can not be null.");
//        }
        Map<String, Integer> response = planService.getAvailableTechAssessmentsCount(companyId, jobId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = {"getavailablejobboardresumescount/{companyId}"}, method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<Map<String, Integer>> getAvailableJobBoardResumesCount(@PathVariable String companyId) {
        if (StringUtils.isEmpty(companyId)) {
            throw new PlatformException("Company id can not be null.");
        }
        Map<String, Integer> response = planService.getAvailableJobBoardResumesCount(companyId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = {"findexpiredplanbycompanyid/{companyId}"}, method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<PlanDto> findExpiredPlanByCompanyId(@PathVariable String companyId) {
        PlanDto response = planService.findExpiredPlanByCompanyId(companyId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
