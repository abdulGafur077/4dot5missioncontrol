package com.fourdotfive.missioncontrol.plan;

import com.fourdotfive.missioncontrol.dtos.plan.PlanDetailDto;
import com.fourdotfive.missioncontrol.dtos.plan.PlanDto;
import com.fourdotfive.missioncontrol.dtos.plan.PlanHeaderDto;
import com.fourdotfive.missioncontrol.pojo.plan.PlanFeaturesCapability;

import java.util.List;
import java.util.Map;

public interface PlanService {


    List<PlanFeaturesCapability> getAllPlanFeatures();

    String getPlanDetails(String companyId);

    PlanFeaturesCapability findPlanFeaturesCapabilityById(String id);

    PlanFeaturesCapability findPlanFeaturesCapabilityByPlan(String plan);

    PlanHeaderDto findPlanHeaderById(String planHeaderId);

    PlanHeaderDto findPlanHeaderByCompanyId(String companyId);

    PlanDetailDto findPlanDetailById(String planDetailId);

    PlanDetailDto findPlanDetailByPlanHeaderId(String planHeaderId);

    PlanDto savePlan(PlanDto planDto);

    PlanDto findPlanByPlanHeaderId(String planHeaderId);

    PlanDto findPlanByCompanyId(String companyId);

    PlanHeaderDto findCurrentPlanHeaderByCompanyId(String companyId);

    PlanHeaderDto findFuturePlanHeaderByCompanyId(String companyId);

    PlanDto findCurrentPlanByCompanyId(String companyId);

    PlanDto findFuturePlanByCompanyId(String companyId);

    Map<String, Integer> getAvailableTechAssessmentsCount(String companyId, String jobId);

    Map<String,Integer> getAvailableJobBoardResumesCount(String companyId);

    List<PlanFeaturesCapability> getAllPlanFeatures(String companyType, String type);

    PlanDto findExpiredPlanByCompanyId(String companyId);
}
