package com.fourdotfive.missioncontrol.plan;

import com.fourdotfive.missioncontrol.dtos.plan.PlanDetailDto;
import com.fourdotfive.missioncontrol.dtos.plan.PlanDto;
import com.fourdotfive.missioncontrol.dtos.plan.PlanHeaderDto;
import com.fourdotfive.missioncontrol.pojo.plan.PlanFeaturesCapability;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author Shalini
 */
@Service
public class PlanServiceImp implements PlanService {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(PlanServiceImp.class);

    @Autowired
    private PlanApiExecutor planApiExecutor;

    @Override
    public List<PlanFeaturesCapability> getAllPlanFeatures() {
        return planApiExecutor.getAllPlanFeatures();
    }

    @Override
    public PlanFeaturesCapability findPlanFeaturesCapabilityById(String id) {
        return planApiExecutor.findPlanFeaturesCapabilityById(id);
    }

    @Override
    public PlanFeaturesCapability findPlanFeaturesCapabilityByPlan(String plan) {
        return planApiExecutor.findPlanFeaturesCapabilityByPlan(plan);
    }

    @Override
    public PlanHeaderDto findPlanHeaderById(String planHeaderId) {
        return planApiExecutor.findPlanHeaderById(planHeaderId);
    }

    @Override
    public PlanHeaderDto findPlanHeaderByCompanyId(String companyId) {
        return planApiExecutor.findPlanHeaderByCompanyId(companyId);
    }

    @Override
    public PlanDetailDto findPlanDetailById(String planDetailId) {
        return planApiExecutor.findPlanDetailById(planDetailId);
    }

    @Override
    public PlanDetailDto findPlanDetailByPlanHeaderId(String planHeaderId) {
        return planApiExecutor.findPlanDetailByPlanHeaderId(planHeaderId);
    }

    @Override
    public PlanDto savePlan(PlanDto planDto) {
        return planApiExecutor.savePlan(planDto);
    }


    @Override
    public PlanDto findPlanByPlanHeaderId(String planHeaderId) {
        return planApiExecutor.findPlanByPlanHeaderId(planHeaderId);
    }


    @Override
    public PlanDto findPlanByCompanyId(String companyId) {
        return planApiExecutor.findPlanByCompanyId(companyId);
    }

    @Override
    public String getPlanDetails(String companyId) {
        return planApiExecutor.getPlanDetails(companyId);
    }

    @Override
    public PlanDto findCurrentPlanByCompanyId(String companyId){
        return planApiExecutor.findCurrentPlanByCompanyId(companyId);
    }

    @Override
    public PlanDto findFuturePlanByCompanyId(String companyId){
        return planApiExecutor.findFuturePlanByCompanyId(companyId);
    }

    @Override
    public PlanHeaderDto findCurrentPlanHeaderByCompanyId(String companyId){
        return planApiExecutor.findCurrentPlanHeaderByCompanyId(companyId);
    }

    @Override
    public PlanHeaderDto findFuturePlanHeaderByCompanyId(String companyId){
        return planApiExecutor.findFuturePlanHeaderByCompanyId(companyId);
    }

    @Override
    public Map<String, Integer> getAvailableTechAssessmentsCount(String companyId, String jobId) {
        return planApiExecutor.getAvailableTechAssessmentsCount(companyId, jobId);
    }

    @Override
    public Map<String, Integer> getAvailableJobBoardResumesCount(String companyId) {
        return planApiExecutor.getAvailableJobBoardResumesCount(companyId);
    }

    @Override
    public List<PlanFeaturesCapability> getAllPlanFeatures(String companyType, String companyId) {
        return planApiExecutor.getAllPlanFeatures(companyType, companyId);
    }

    @Override
    public PlanDto findExpiredPlanByCompanyId(String companyId) {
        return planApiExecutor.findExpiredPlanByCompanyId(companyId);
    }


}
