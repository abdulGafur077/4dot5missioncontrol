package com.fourdotfive.missioncontrol.plan;

public enum Plans {
    TRIAL("Trial"), THE_ASSESSOR("The Assessor"), THE_REVIEWER("The Reviewer"),
    THE_EXPLORER("The Explorer"), VENDOR_ONLY("Vendor Only");

    private String text;

    Plans(String text) {
        this.text = text;
    }

    public String getText() {
        return this.text;
    }
}
