package com.fourdotfive.missioncontrol.platformexcecutor;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fourdotfive.missioncontrol.candidate.CandidateMockApiExecutor;
import com.fourdotfive.missioncontrol.common.ApiConstants;
import com.fourdotfive.missioncontrol.common.Messages;
import com.fourdotfive.missioncontrol.dtos.candidate.ActiveCandidateMinDto;
import com.fourdotfive.missioncontrol.dtos.candidate.CandidateDetailsDto;
import com.fourdotfive.missioncontrol.dtos.candidate.CandidateMinInfoDto;
import com.fourdotfive.missioncontrol.dtos.candidate.PassiveCandidateMinDto;
import com.fourdotfive.missioncontrol.dtos.job.MockJobProfileDto;
import com.fourdotfive.missioncontrol.dtos.requisition.CountDto;
import com.fourdotfive.missioncontrol.dtos.requisition.RequisitionDetailedDto;
import com.fourdotfive.missioncontrol.dtos.requisition.RequisitionMinDto;
import com.fourdotfive.missioncontrol.dtos.user.DashBoardDto;
import com.fourdotfive.missioncontrol.exception.PlatformException;
import com.fourdotfive.missioncontrol.pojo.candidate.CandidateState;
import com.fourdotfive.missioncontrol.requisition.RequisitionMockApiExecutor;
import com.fourdotfive.missioncontrol.user.UserMockApiExceutor;

@Service
public class MockApiExecutor implements RequisitionMockApiExecutor,
		CandidateMockApiExecutor, UserMockApiExceutor {

	@Autowired
	private RestTemplate restTemplate;

	public static final Logger LOGGER = LoggerFactory
			.getLogger(MockApiExecutor.class);

	@Override
	public List<RequisitionMinDto> getAllReqs() {
		String url = new StringBuilder().append(ApiConstants.MOCK_BASE_URL)
				.append(ApiConstants.GET_ALL_JOB).toString();
		LOGGER.info("Url {} ", url);
		ResponseEntity<List<RequisitionMinDto>> response = null;
		try {
			response = restTemplate.exchange(url, HttpMethod.GET, null,
					new ParameterizedTypeReference<List<RequisitionMinDto>>() {
					});
		} catch (HttpClientErrorException e) {
			LOGGER.error("Error occured while fetching open requisitions",
					e.getStatusCode());
		}

		if (response == null) {
			throw new PlatformException(Messages.INTERNAL_ERROR_MSG,
					Messages.INTERNAL_ERROR_MSG);
		}
		return response.getBody();
	}

	@Override
	public RequisitionDetailedDto getMockJob(String jobId) {
		String url = new StringBuilder().append(ApiConstants.MOCK_BASE_URL)
				.append(ApiConstants.GET_JOB).append("/").append(jobId)
				.toString();
		LOGGER.info("Url {} ", url);
		ResponseEntity<RequisitionDetailedDto> response = null;
		try {
			response = restTemplate.exchange(url, HttpMethod.GET, null,
					new ParameterizedTypeReference<RequisitionDetailedDto>() {
					});
		} catch (HttpClientErrorException e) {
			LOGGER.error("Error occured while fetching  requisition",
					e.getStatusCode());
		}

		if (response == null) {
			throw new PlatformException(Messages.INTERNAL_ERROR_MSG,
					Messages.INTERNAL_ERROR_MSG);
		}
		return response.getBody();
	}

	@Override
	public List<ActiveCandidateMinDto> getAciveCandidates(
			CandidateState candidateState) {
		String url = new StringBuilder().append(ApiConstants.MOCK_BASE_URL)
				.append(ApiConstants.GET_ALL_CANDIDATES).append("/")
				.append(CandidateState.ACTIVE).toString();
		LOGGER.info("Url {} ", url);
		ResponseEntity<List<ActiveCandidateMinDto>> response = null;
		try {
			response = restTemplate
					.exchange(
							url,
							HttpMethod.GET,
							null,
							new ParameterizedTypeReference<List<ActiveCandidateMinDto>>() {
							});
		} catch (HttpClientErrorException e) {
			LOGGER.error("Error occured while fetching Active candidate",
					e.getStatusCode());
		}

		if (response == null) {
			throw new PlatformException(Messages.INTERNAL_ERROR_MSG,
					Messages.INTERNAL_ERROR_MSG);
		}
		return response.getBody();
	}

	@Override
	public List<PassiveCandidateMinDto> getPassiveCandidates(
			CandidateState candidateState) {
		String url = new StringBuilder().append(ApiConstants.MOCK_BASE_URL)
				.append(ApiConstants.GET_ALL_CANDIDATES).append("/")
				.append(CandidateState.PASSIVE).toString();
		LOGGER.info("Url {} ", url);
		ResponseEntity<List<PassiveCandidateMinDto>> response = null;
		try {
			response = restTemplate
					.exchange(
							url,
							HttpMethod.GET,
							null,
							new ParameterizedTypeReference<List<PassiveCandidateMinDto>>() {
							});
		} catch (HttpClientErrorException e) {
			LOGGER.error("Error occured while fetching Active candidate",
					e.getStatusCode());
		}

		if (response == null) {
			throw new PlatformException(Messages.INTERNAL_ERROR_MSG,
					Messages.INTERNAL_ERROR_MSG);
		}
		return response.getBody();
	}

	@Override
	public CandidateDetailsDto getCandidateDetails(String candidateId) {
		String url = new StringBuilder().append(ApiConstants.MOCK_BASE_URL)
				.append(ApiConstants.GET_CANDIDATE).append("/")
				.append(candidateId).toString();
		LOGGER.info("Url {} ", url);
		ResponseEntity<CandidateDetailsDto> response = null;
		try {
			response = restTemplate.exchange(url, HttpMethod.GET, null,
					new ParameterizedTypeReference<CandidateDetailsDto>() {
					});
		} catch (HttpClientErrorException e) {
			LOGGER.error(
					"Error occured while fetching  Candidate complete details {}",
					e.getMessage());
		}

		if (response == null) {
			throw new PlatformException(Messages.INTERNAL_ERROR_MSG,
					Messages.INTERNAL_ERROR_MSG);
		}
		return response.getBody();
	}

	@Override
	public List<CandidateMinInfoDto> getAllCandidates() {
		String url = new StringBuilder().append(ApiConstants.MOCK_BASE_URL)
				.append(ApiConstants.GET_ALL_CANDIDATES).append("/")
				.append("findall").toString();
		LOGGER.info("Url {} ", url);
		ResponseEntity<List<CandidateMinInfoDto>> response = null;
		try {
			response = restTemplate
					.exchange(
							url,
							HttpMethod.GET,
							null,
							new ParameterizedTypeReference<List<CandidateMinInfoDto>>() {
							});
		} catch (HttpClientErrorException e) {
			LOGGER.error("Error occured while fetching passive candidate",
					e.getStatusCode());
		}

		if (response == null) {
			throw new PlatformException(Messages.INTERNAL_ERROR_MSG,
					Messages.INTERNAL_ERROR_MSG);
		}
		return response.getBody();
	}

	@Override
	public CountDto getRequisitionCount(String reqId) {
		String url = new StringBuilder().append(ApiConstants.MOCK_BASE_URL)
				.append(ApiConstants.GET_REQ_TRANSACTION_CANDIDATES_COUNT).append("/").append(reqId)
				.toString();
		LOGGER.info("Url {} ", url);
		ResponseEntity<CountDto> response = null;
		try {
			response = restTemplate.exchange(url, HttpMethod.GET, null,
					new ParameterizedTypeReference<CountDto>() {
					});
		} catch (HttpClientErrorException e) {
			LOGGER.error(
					"Error occured while fetching  Candidate complete details {}",
					e.getMessage());
		}

		if (response == null) {
			throw new PlatformException(Messages.INTERNAL_ERROR_MSG,
					Messages.INTERNAL_ERROR_MSG);
		}
		return response.getBody();
	}

	@Override
	public MockJobProfileDto getMockJobProfile() {
		String url = new StringBuilder().append(ApiConstants.MOCK_BASE_URL)
				.append(ApiConstants.GET_JOB_PROFILE).toString();
		LOGGER.info("Url {} ", url);
		ResponseEntity<MockJobProfileDto> response = null;
		try {
			response = restTemplate.exchange(url, HttpMethod.GET, null,
					new ParameterizedTypeReference<MockJobProfileDto>() {
					});
		} catch (HttpClientErrorException e) {
			LOGGER.error("Error occured while fetching jobprofile",
					e.getStatusCode());
		}

		if (response == null) {
			throw new PlatformException(Messages.INTERNAL_ERROR_MSG,
					Messages.INTERNAL_ERROR_MSG);
		}
		return response.getBody();
	}

	@Override
	public DashBoardDto getUserDashBoardData(String userId) {
		String url = new StringBuilder().append(ApiConstants.MOCK_BASE_URL)
				.append(ApiConstants.GET_DASHBOARD_DATA).toString();
		LOGGER.info("Url {} ", url);
		ResponseEntity<DashBoardDto> response = null;
		try {
			response = restTemplate.exchange(url, HttpMethod.GET, null,
					new ParameterizedTypeReference<DashBoardDto>() {
					});
		} catch (HttpClientErrorException e) {
			LOGGER.error("Error occured while fetching Dashboard",
					e.getStatusCode());
		}

		if (response == null) {
			throw new PlatformException(Messages.INTERNAL_ERROR_MSG,
					Messages.INTERNAL_ERROR_MSG);
		}
		return response.getBody();
	}

}
