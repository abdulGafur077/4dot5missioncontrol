package com.fourdotfive.missioncontrol.platformexcecutor;

import com.fourdotfive.missioncontrol.acl.FeatureAccessControlApiExecutor;
import com.fourdotfive.missioncontrol.alert.AlertNotificationApiExecutor;
import com.fourdotfive.missioncontrol.assessment.AssessmentApiExecutor;
import com.fourdotfive.missioncontrol.candidate.CandidateApiExecutor;
import com.fourdotfive.missioncontrol.candidate.CandidateContactDetailDTO;
import com.fourdotfive.missioncontrol.candidate.CandidateDetailsResponseDTO;
import com.fourdotfive.missioncontrol.chat.ChatApiExecutor;
import com.fourdotfive.missioncontrol.chat.ChatFilterDTO;
import com.fourdotfive.missioncontrol.chat.ChatMessageDTO;
import com.fourdotfive.missioncontrol.common.ApiConstants;
import com.fourdotfive.missioncontrol.common.AppConstants;
import com.fourdotfive.missioncontrol.common.Messages;
import com.fourdotfive.missioncontrol.company.CompanyApiExecutor;
import com.fourdotfive.missioncontrol.dto.assessment.AccessTimeRequestDTO;
import com.fourdotfive.missioncontrol.dtos.alert.NotificationFilterParamsDto;
import com.fourdotfive.missioncontrol.dtos.alert.NotificationStateChangeDTO;
import com.fourdotfive.missioncontrol.dtos.candidate.*;
import com.fourdotfive.missioncontrol.dtos.company.*;
import com.fourdotfive.missioncontrol.dtos.employee.*;
import com.fourdotfive.missioncontrol.dtos.external.callback.IMCandidateReportCallbackDto;
import com.fourdotfive.missioncontrol.dtos.job.*;
import com.fourdotfive.missioncontrol.dtos.jobmatch.*;
import com.fourdotfive.missioncontrol.dtos.login.PasswordDetailsDto;
import com.fourdotfive.missioncontrol.dtos.pdf.*;
import com.fourdotfive.missioncontrol.dtos.plan.PlanDetailDto;
import com.fourdotfive.missioncontrol.dtos.plan.PlanDto;
import com.fourdotfive.missioncontrol.dtos.plan.PlanHeaderDto;
import com.fourdotfive.missioncontrol.dtos.plan.PlanMinDto;
import com.fourdotfive.missioncontrol.dtos.requisition.*;
import com.fourdotfive.missioncontrol.dtos.user.DashBoardDto;
import com.fourdotfive.missioncontrol.dtos.user.UserFilterDTO;
import com.fourdotfive.missioncontrol.dtos.user.VendorRecruiterRequestDto;
import com.fourdotfive.missioncontrol.dtos.user.VendorRecruitersCollectionRequestDto;
import com.fourdotfive.missioncontrol.dtos.vendor.*;
import com.fourdotfive.missioncontrol.dtos.workflowstep.WorkflowStepDTO;
import com.fourdotfive.missioncontrol.dtos.workflowstep.WorkflowStepRequestDTO;
import com.fourdotfive.missioncontrol.employee.EmployeeApiExecutor;
import com.fourdotfive.missioncontrol.exception.*;
import com.fourdotfive.missioncontrol.external.callback.ExternalCallbackApiExecutor;
import com.fourdotfive.missioncontrol.feedback.FeedbackAnswersApiExecutor;
import com.fourdotfive.missioncontrol.feedback.FeedbackQuestionsApiExecutor;
import com.fourdotfive.missioncontrol.job.JobApiExecutor;
import com.fourdotfive.missioncontrol.jobmatch.JobMatchApiExecutor;
import com.fourdotfive.missioncontrol.licensepreference.LicensePrefMarkerDto;
import com.fourdotfive.missioncontrol.licensepreference.LicensePreferences;
import com.fourdotfive.missioncontrol.login.LoginApiExecutor;
import com.fourdotfive.missioncontrol.notificatinpreference.NotificationPreferenceApiExecutor;
import com.fourdotfive.missioncontrol.plan.PlanApiExecutor;
import com.fourdotfive.missioncontrol.pojo.FourDotFiveResponse;
import com.fourdotfive.missioncontrol.pojo.LoginDetails;
import com.fourdotfive.missioncontrol.pojo.ResetPwdDetails;
import com.fourdotfive.missioncontrol.pojo.TokenLoginDetails;
import com.fourdotfive.missioncontrol.pojo.bigfivequestions.BigFiveQuestions;
import com.fourdotfive.missioncontrol.pojo.bigfivequestions.CandidateBigFiveResponses;
import com.fourdotfive.missioncontrol.pojo.candidate.*;
import com.fourdotfive.missioncontrol.pojo.candidate.Contact;
import com.fourdotfive.missioncontrol.pojo.company.*;
import com.fourdotfive.missioncontrol.pojo.employee.EmployeeDTO;
import com.fourdotfive.missioncontrol.pojo.feedback.FeedbackExecutionResult;
import com.fourdotfive.missioncontrol.pojo.feedback.FeedbackQuestions;
import com.fourdotfive.missioncontrol.pojo.feedback.FileRetrieve;
import com.fourdotfive.missioncontrol.pojo.job.*;
import com.fourdotfive.missioncontrol.pojo.notificationpreference.NotificationPreference;
import com.fourdotfive.missioncontrol.pojo.plan.PlanFeaturesCapability;
import com.fourdotfive.missioncontrol.pojo.setting.Setting;
import com.fourdotfive.missioncontrol.pojo.user.*;
import com.fourdotfive.missioncontrol.requisition.RequisitionApiExecutor;
import com.fourdotfive.missioncontrol.resume.ResumeApiExecutor;
import com.fourdotfive.missioncontrol.setting.SettingApiExecutor;
import com.fourdotfive.missioncontrol.user.UserApiExecutor;
import com.fourdotfive.missioncontrol.utility.UtilityApiExecutor;
import com.fourdotfive.missioncontrol.valueassessment.ValueAssesmentApiExecutor;
import com.fourdotfive.missioncontrol.workflowstep.WorkflowStepApiExecutor;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.*;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@Service
public class PlatformApiExecutorImp implements CompanyApiExecutor, UserApiExecutor, RequisitionApiExecutor,
        CandidateApiExecutor, LoginApiExecutor, SettingApiExecutor, NotificationPreferenceApiExecutor, JobApiExecutor
        , ResumeApiExecutor, JobMatchApiExecutor, AssessmentApiExecutor, FeedbackQuestionsApiExecutor
        , FeedbackAnswersApiExecutor, ValueAssesmentApiExecutor, UtilityApiExecutor, ExternalCallbackApiExecutor
        , AlertNotificationApiExecutor, PlanApiExecutor, FeatureAccessControlApiExecutor, EmployeeApiExecutor, ChatApiExecutor,
        WorkflowStepApiExecutor {

    public static final Logger LOGGER = LoggerFactory.getLogger(PlatformApiExecutorImp.class);
    @Autowired
    private RestTemplate restTemplate;

    @Override
    public Company saveFourDotFiveAdmin(String companyId, String userId) {

        String url = ApiConstants.SAVE_FOURDOTFIVE_ADMIN + companyId + "/" + userId;

        LOGGER.debug("Executing url: Save 4.5 admin - {}", url);
        ResponseEntity<Company> response;
        try {
            response = restTemplate.exchange(url, HttpMethod.POST, null, new ParameterizedTypeReference<Company>() {
            });
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            LOGGER.error("An error occured while saving 4dot5 admin to company ", e.getMessage());
            throw new Save4Dot5AdminPlatfromException(Messages.INTERNAL_ERROR_MSG);
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }
        LOGGER.debug("Response: Save 4.5 admin - {}", response);

        if (response == null) {
            throw new Save4Dot5AdminPlatfromException(Messages.INTERNAL_ERROR_MSG);
        }
        return response.getBody();
    }

    @Override
    public User getFourDotFiveAdmin(String companyId) {

        String url = ApiConstants.GET_FOURDOTFIVE_ADMIN + companyId;

        LOGGER.debug("Executing url: Get 4.5 admin - {}", url);
        ResponseEntity<User> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<User>() {
            });
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.EXCEPTION_WHILE_FETCHING_FOURDOTFIVE_ADMIN, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }
        LOGGER.debug("Response: Get 4.5 admin - {}", response);

        if (response != null) {
            return response.getBody();
        }
        return null;
    }

    @Override
    public Company getCompany(String companyId) {

        String url = ApiConstants.GET_COMPANY_BY_ID + "/" + companyId;

        LOGGER.debug("Executing url: Get company - {}", url);
        ResponseEntity<Company> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<Company>() {
            });
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG);
        }
        LOGGER.debug("Response: Get company -  {}", response);
        return response.getBody();
    }

    @Override
    public Company getHostCompany(String name) {

        String url = ApiConstants.GET_COMPANY_BY_NAME + "/" + name;

        LOGGER.debug("Executing url: Get host company - {}", url);
        ResponseEntity<Company> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<Company>() {
            });
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }
        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG);
        }
        LOGGER.debug("Response: Get host company - {}", response);
        return response.getBody();
    }

    @Override
    public Company saveCompany(Company company) {
        ResponseEntity<Company> response;
        LOGGER.debug("Executing url: Save company {}", company);
        try {
            response = restTemplate.exchange(ApiConstants.SAVE_COMPANY, HttpMethod.PUT,
                    new HttpEntity<>(company), new ParameterizedTypeReference<Company>() {
                    });
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            LOGGER.error(e.getMessage());
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                throw new DuplicateCompanyException(Messages.DUPLICATE_COMPANY, company.getShortName(),
                        company.getName());
            } else {
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }
        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG);
        }
        LOGGER.debug("Response: Save company {}", response);
        return response.getBody();
    }

    @Override
    public boolean isDuplicateCompanyByName(String name, String id) {
        CompanyDuplicateCheckRequestDTO companyDuplicateCheckRequestDTO = new CompanyDuplicateCheckRequestDTO(name, id);
        String url = ApiConstants.IS_DUPLICATE_COMPANY_BY_NAME;
        ResponseEntity<Boolean> response = restTemplate.exchange(
                url,
                HttpMethod.POST,
                new HttpEntity<>(companyDuplicateCheckRequestDTO),
                new ParameterizedTypeReference<Boolean>() {
                });
        return response.getBody();
    }


    @Override
    public Company addClientOrBU(String parentCompanyId, String childCompanyId) {
        String url = ApiConstants.ADD_CLIENT_OR_BU + "/" + parentCompanyId +
                "/" + childCompanyId;

        LOGGER.debug("Executing url: Add client/BU - {}", url);
        ResponseEntity<Company> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.PUT, null, new ParameterizedTypeReference<Company>() {
            });
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG);
        }
        LOGGER.debug("Response: Add client/BU - {}", response);
        return response.getBody();
    }


    @Override
    public List<Company> getClientOrBUs(String companyId) {
        String url = ApiConstants.GET_CLIENT_OR_BU + "/" + companyId;
        LOGGER.debug("Executing url: Get client/Bu - {}", url);

        ResponseEntity<List<Company>> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null,
                    new ParameterizedTypeReference<List<Company>>() {
                    });
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG);
        }
        LOGGER.debug("Response: Get client/Bu - {}", response);
        return response.getBody();
    }


    @Override
    public void deleteClientOrBU(String parentCompanyId, String childCompanyId) {
        String url = ApiConstants.DELETE_CLIENT_OR_BU + "/" + parentCompanyId + "/" + childCompanyId;
        LOGGER.debug("Executing url: Delete client/BU- {}", url);
        try {
            restTemplate.exchange(url, HttpMethod.PUT, null, String.class);
        } catch (HttpClientErrorException | HttpServerErrorException | ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());

        }
    }

    @Override
    public void deleteCompany(String parentCompanyId) {
        String url = ApiConstants.DELETE_COMPANY + "/" + parentCompanyId;

        LOGGER.debug("Executing url: Delete company- {}", url);
        try {
            restTemplate.exchange(url, HttpMethod.DELETE, null, String.class);
        } catch (HttpClientErrorException | HttpServerErrorException | ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());

        }
    }


    @Override
    public List<Company> getAllCompany(int page, int size, String userId, String sort, String sortDir) {

        String url = ApiConstants.GET_ALL_COMPANIES + "?" + "page=" + page +
                "&" + "size=" + size + "&" + "sort=" + sort + "&" +
                "sortDir=" + sortDir;

        LOGGER.debug("Executing url: Get all company - {}", url);
        ResponseEntity<CompanyList> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<CompanyList>() {
            });
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG);
        }
        LOGGER.debug("Response: Get all company - {}", response);

        CompanyList companyList = response.getBody();
        return companyList.getContent();
    }

    @Override
    public Company setLicensePreferences(LicensePreferences licensePreferences, String companyId) {

        String url = ApiConstants.SAVE_LICENSE_PREFERENCES + "/" + companyId;

        LOGGER.debug("Executing url: Set license preferences - {}", url);
        ResponseEntity<SetLicensePreferencesResponse> response;
        try {
            response = restTemplate.exchange(url, HttpMethod.POST,
                    new HttpEntity<>(licensePreferences), new ParameterizedTypeReference<SetLicensePreferencesResponse>() {
                    });
        } catch (HttpClientErrorException | HttpServerErrorException | ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG);
        }

        if (StringUtils.isNotEmpty(response.getBody().getErrorMessage())) {
            throw new PlatformException(response.getBody().getErrorMessage());
        }

        LOGGER.debug("Response: Set license preferences - {}", response);
        return response.getBody().getCompany();
    }


    @Override
    public List<Role> getAllRoles() {

        LOGGER.debug("Executing url: Get all roles - {}", ApiConstants.GET_ROLES);
        ResponseEntity<List<Role>> response;
        try {
            response = restTemplate.exchange(ApiConstants.GET_ROLES, HttpMethod.GET, null,
                    new ParameterizedTypeReference<List<Role>>() {
                    });
        } catch (HttpClientErrorException | HttpServerErrorException | ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG);
        }
        LOGGER.debug("Response: Get all roles - {} ", response);
        return response.getBody();
    }

    @Override
    public HttpStatus setManager(String userId, String managerId) {

        String url = ApiConstants.ASSIGN_MANAGER + "/" + userId + "/" +
                managerId;
        LOGGER.debug("Executing url: Set manager - {} ", url);
        ResponseEntity<User> response;
        try {
            response = restTemplate.exchange(url, HttpMethod.POST, null, new ParameterizedTypeReference<User>() {
            });
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            LOGGER.error(e.getMessage());
            throw new AssignManagerPlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }
        LOGGER.debug("Response: Set manager - {} ", response);
        if (response == null) {
            throw new AssignManagerPlatformException(Messages.INTERNAL_ERROR_MSG);
        }
        //TODO why are we returning status here
        return HttpStatus.OK;
    }

    @Override
    public User saveUser(User user, String roleId, String companyId) {
        String url = ApiConstants.SAVE_USER + "/" + companyId + "/" + roleId;
        LOGGER.debug("Executing url: Save user - {}", url);
        ResponseEntity<User> response;
        try {
            response = restTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<>(user),
                    new ParameterizedTypeReference<User>() {
                    });
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            LOGGER.error(e.getMessage());
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                throw new DuplicateUserException(Messages.DUPLICATE_USER, user.getEmail());
            } else {
                throw new SaveUserPlatformException(Messages.EXCEPTION_WHILE_UPDATING_USER, user.getEmail());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        if (response == null) {
            throw new SaveUserPlatformException(Messages.INTERNAL_ERROR_MSG);
        }
        LOGGER.debug("Response: Save user - {}", response);
        return response.getBody();
    }

    @Override
    public boolean deleteUser(String userId) {
        String url = ApiConstants.DELETE_USER + "/" + userId;
        LOGGER.debug("Executing url: Delete user - {}", url);

        ResponseEntity<Boolean> response;
        try {
            response = restTemplate.exchange(url, HttpMethod.DELETE, null, new ParameterizedTypeReference<Boolean>() {
            });
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            LOGGER.error(e.getMessage());
            throw new DeleteUserPlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        if (response == null) {
            throw new DeleteUserPlatformException(Messages.INTERNAL_ERROR_MSG);
        } else {
            if (!response.getBody()) {
                throw new DeleteUserPlatformException(Messages.INTERNAL_ERROR_MSG);
            }
        }

        LOGGER.debug("Response: Delete user - {}", response);
        return response.getBody();
    }

    @Override
    public String getOneTimePassword(String userId) {
        String url = ApiConstants.GET_OTP + "/" + userId;
        return getResponse(url, HttpMethod.GET, new HttpEntity<>(null));
    }

    @Override
    public AccessControlList getEnityScope(String userId) {
        String url = ApiConstants.GET_ENTITY_SCOPE + "/" + userId;

        LOGGER.debug("Executing url: Get entity scope - {}", url);
        ResponseEntity<AccessControlList> response;
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null,
                    new ParameterizedTypeReference<AccessControlList>() {
                    });
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            LOGGER.error(e.getMessage());
            throw new FetchEnityScopePlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        if (response == null) {
            throw new FetchEnityScopePlatformException(Messages.INTERNAL_ERROR_MSG);
        }
        LOGGER.debug("Response: Get entity scope - {}", response);
        return response.getBody();
    }

    @Override
    public User setScope(UserScope scope, String userId) {

        String url = ApiConstants.SET_ENTITY_SCOPE + "/" + userId;
        LOGGER.debug("Executing url: Set user scope - {}", url);
        ResponseEntity<User> response;
        try {
            response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(scope),
                    new ParameterizedTypeReference<User>() {
                    });
        } catch (HttpClientErrorException | HttpServerErrorException | ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }
        LOGGER.debug("Response {}", response);
        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG);
        }

        LOGGER.debug("Response: Set user scope - {}", response);
        return response.getBody();
    }

    @Override
    public AccessControlList setScope(Scope scope, String userId) {

        String url = ApiConstants.SET_ENTITY_SCOPE + "/" + userId;
        LOGGER.debug("Executing url: Set user scope {}", url);
        ResponseEntity<User> response;
        try {
            response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(scope),
                    new ParameterizedTypeReference<User>() {
                    });
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            LOGGER.error(e.getMessage());
            throw new SetScopePlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        LOGGER.debug("Response: Set user scope {}", response);
        if (response == null) {
            throw new SetScopePlatformException(Messages.INTERNAL_ERROR_MSG);
        }
        return response.getBody().getAccessControlList();
    }


    @Override
    public List<User> getRecruitersByCompany(String companyId) {

        String url = ApiConstants.GET_RECRUITER_LIST + "/" + companyId;
        LOGGER.debug("Executing url: Get recruiters by company {}", url);
        ResponseEntity<List<User>> response;
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null,
                    new ParameterizedTypeReference<List<User>>() {
                    });
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            LOGGER.error(e.getMessage());
            throw new SetScopePlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        LOGGER.debug("Response: Get recruiters by company {}", response);
        if (response == null) {
            throw new SetScopePlatformException(Messages.INTERNAL_ERROR_MSG);
        }
        return response.getBody();
    }

    @Override
    public List<User> getReporteeList(String userId) {
        String url = ApiConstants.GET_REPORTEES + "/" + userId;
        LOGGER.debug("Executing url: Get Reportee list - {}", url);
        ResponseEntity<List<User>> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<List<User>>() {
            });
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }
        LOGGER.debug("Response: Get reportee list {}", response);
        if (response != null && response.getBody() != null) {
            return response.getBody();
        }
        return null;
    }

    @Override
    public User getUserById(String userId) {
        String url = ApiConstants.GET_USER + "/" + userId;

        LOGGER.debug("Executing url: Get user by id - {} ", url);
        ResponseEntity<User> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<User>() {
            });

        } catch (HttpClientErrorException | HttpServerErrorException e) {
            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG);
        }
        LOGGER.debug("Response: Get user by id - {} ", response);
        return response.getBody();
    }

    @Override
    public User getManager(String userId) {
        String url = ApiConstants.GET_MANAGER + "/" + userId;
        LOGGER.debug("Executing url: Get manager - {} ", url);
        ResponseEntity<User> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<User>() {
            });
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        LOGGER.debug("Response: Get manager - {} ", response);
        if (response != null) {
            return response.getBody();
        }
        return null;
    }

    @Override
    public User getRecipientUser(String userId) {
        String url = ApiConstants.GET_RECIPIENT_USER + "/" + userId;

        LOGGER.debug("Executing url: Get recipient - {}", url);
        ResponseEntity<User> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<User>() {
            });
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }
        if (response != null) {
            LOGGER.debug("Response: Get recipient - {}", response);
            return response.getBody();
        }
        return null;
    }

    @Override
    public List<User> getCompanyAdmins(String companyId) {
        String url = ApiConstants.GET_ALL_COMPANY_ADMIN + "/" + companyId;

        LOGGER.debug("Executing url: Get all admins - {}", url);
        ResponseEntity<List<User>> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<List<User>>() {
            });
        } catch (HttpClientErrorException | HttpServerErrorException e) {

            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }
        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG);
        }
        LOGGER.debug("Response: Get all admins - {}", response);
        return response.getBody();
    }

    //TODO refactor code below

    /*******************************************Start******************************************/
    @Override
    public void setReqLicensePreferences(LicensePreferences licensePreferences, String jobId) {
        String url = ApiConstants.SAVE_LICENSE_PREFERENCES + "/" + jobId;
        LOGGER.debug("Executing url: Set license preferences - {}", url);
        try {
            restTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<>(licensePreferences),
                    new ParameterizedTypeReference<Job>() {
                    });
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }
    }

    @Override
    public LicensePrefMarkerDto getLicensePreferenceMarkerValues(String clientOrBuId, String jobId) {
        String url = ApiConstants.GET_MARKER_VALUES + "/" + jobId + "?staffingorcorpcompid=" + clientOrBuId;
        ResponseEntity<LicensePrefMarkerDto> response = null;
        LOGGER.debug("Executing url: get marker license preferences for job - {}", url);
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null,
                    new ParameterizedTypeReference<LicensePrefMarkerDto>() {
                    });
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }
        return response.getBody();
    }

    @Override
    public String uploadRequistionDoc(List<MultipartFile> reqFiles, String userId, String companyId, String buId,
                                      String sharedClientBuOrCorpId, boolean isSharedClientRequisition) throws IOException {
        System.out.println("In api executor - companyId - " + companyId + " userId - " + userId + " buId - " + buId);
        //String url = new StringBuilder().append(ApiConstants.UPLOAD_REQ_DOC).toString();
        String url = ApiConstants.UPLOAD_REQ_DOC + '/' + companyId + '/' + userId + "?buId=" + buId +
                "&sharedClientBuOrCorpId=" + sharedClientBuOrCorpId + "&isSharedClientRequisition=" + isSharedClientRequisition;
        System.out.println("URL generated - " + url);
        MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        System.out.println("req files length - " + reqFiles.size());
        for (MultipartFile file : reqFiles) {
            String fileName = file.getOriginalFilename();
            if (StringUtils.isNotBlank(fileName)) {
                fileName = fileName.replace("\u00A0", " ");
            }
            final String name = fileName;
            ByteArrayResource resource = new ByteArrayResource(file.getBytes()) {
                @Override
                public String getFilename() {
                    return name;
                }
            };
            map.add("files", resource);
        }

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(map, header);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
                new ParameterizedTypeReference<String>() {
                });
        //System.out.println("Response entity - " + response.getBody());
        return response.getBody();
    }

    @Override
    public String getJobTypes() {
        String url = ApiConstants.GET_JOB_TYPES;
        //ObjectMapper objectMapper = new ObjectMapper();
        //objectMapper.setVisibility(PropertyAccessor., Visibility.ANY);
        //ObjectMapper mapper = new ObjectMapper();
        //mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<String>() {
                });
        return response.getBody();
    }

    @Override
    public String getSponsorshipTypes() {
        String url = ApiConstants.GET_SPONSORSHIP_TYPES;
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<String>() {
                });
        return response.getBody();
    }

    //@Override
    public String processJob(JSONObject fileInfo) {
        System.out.println("In api executor -" + fileInfo);
        String url = ApiConstants.PROCESS_REQ_JOB;
        System.out.println("URL generated - " + url);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> requestEntity = new HttpEntity<>(fileInfo.toString(), headers);

        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
                new ParameterizedTypeReference<String>() {
                });

        System.out.println("Process Job Response - " + response.getBody());
        return response.getBody();

    }

    //@Override
    public JobResponseDto saveAndParseJob(String userId, String hrxmlFileInfoJsonString) {
        System.out.println("In api executor - " + hrxmlFileInfoJsonString);
        String url = ApiConstants.SAVE_AND_PARSE_HRXML_JOB + "/" + userId;
        System.out.println("URL generated - " + url);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> requestEntity = new HttpEntity<>(hrxmlFileInfoJsonString, headers);

        ResponseEntity<JobResponseDto> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
                new ParameterizedTypeReference<JobResponseDto>() {
                });

        System.out.println("Save and Parse Response - " + response.getBody());
        return response.getBody();

    }

    @Override
    public CountDto getJobCandidateCount(String jobId, Float score, String reqTransactionId) {
        String url = ApiConstants.GET_REQ_TRANSACTION_CANDIDATES_COUNT + "/" + jobId + "?" +
                "score=" + score + "&requisitionTransactionId=" + reqTransactionId;
        ResponseEntity<CountDto> response = null;
        LOGGER.debug("Executing url: get candidates count for job requisition transaction - {}", url);
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null,
                    new ParameterizedTypeReference<CountDto>() {
                    });
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }
        return response.getBody();
    }

    @Override
    public List<CandidateCardDto> getCandidatesByRequisitionTransaction(String jobId, String companyId, String requisitionTransactionId, String resumeTransactionId, String countType, Double score, String userId) {
        String url = ApiConstants.GET_REQ_TRANSACTION_CANDIDATES + "/" + jobId + "/" + countType + "/" + companyId +
                "?" + "score=" + score + "&requisitionTransactionId=" + requisitionTransactionId +
                "&userId=" + userId;
        ResponseEntity<List<CandidateCardDto>> response = null;
        LOGGER.debug("Executing url: get candidates for job requisition transaction - {}", url);
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null,
                    new ParameterizedTypeReference<List<CandidateCardDto>>() {
                    });
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }
        return response.getBody();
    }

    @Override
    public Job saveJob(Job job) {
        String url = ApiConstants.SAVE_JOB;

        ResponseEntity<Job> response = restTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<>(job),
                new ParameterizedTypeReference<Job>() {
                });

        return response.getBody();
    }


    @Override
    public List<JobOpeningDto> saveJobOpenings(List<JobOpeningDto> jobOpeningDtos) {
        String url = ApiConstants.SAVE_JOB_OPENINGS;

        ResponseEntity<List<JobOpeningDto>> response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(jobOpeningDtos),
                new ParameterizedTypeReference<List<JobOpeningDto>>() {
                });

        System.out.println("Job Openings Save response - " + response);

        return response.getBody();
    }

    @Override
    public Job setJobStatus(JobState jobState) {
        String url = ApiConstants.SET_JOB_STATUS;
        ResponseEntity<Job> response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(jobState),
                new ParameterizedTypeReference<Job>() {
                });

        return response.getBody();
    }

    @Override
    public List<String> runJobForScoring(String companyId, String jobId, String transactionType, String transactionId, String reqTransactionId) {
        String url = ApiConstants.GET_CANDIDATES_COUNT +
                "/" + companyId +
                "/" + jobId +
                "/" + transactionType +
                "/" + transactionId +
                "/" + reqTransactionId;

        ResponseEntity<List<String>> response = restTemplate.exchange(url, HttpMethod.POST, null,
                new ParameterizedTypeReference<List<String>>() {
                });
        if (response != null && response.getBody() != null) {
            return response.getBody();
        }
        return null;
    }

    @Override
    public Job getJob(String jobId) {
        String url = ApiConstants.GET_JOB + "/" + jobId;
        ResponseEntity<Job> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<Job>() {
                });
        return response.getBody();
    }

    @Override
    public List<Job> getJobAll(String userId) {
        String url = ApiConstants.GET_OPEN_REQUISITIONS + "/" + userId;
        ResponseEntity<List<Job>> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.PUT, null, new ParameterizedTypeReference<List<Job>>() {
            });
        } catch (HttpClientErrorException e) {
            LOGGER.error("Error occured while fetching open requisitions");
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {

            }
        }

        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, Messages.NULL_RESPONSE);
        }
        return response.getBody();
    }

    @Override
    public List<Candidate> getCandidatesByRequisitionId(String id) {

        String url = ApiConstants.GET_CANDIDATES_BY_REQID;

        ResponseEntity<List<Candidate>> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null,
                    new ParameterizedTypeReference<List<Candidate>>() {
                    });
        } catch (HttpClientErrorException e) {
            LOGGER.error("An error occurred while fetching active candidates by requisition id {}", e.getMessage());
        }

        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, Messages.NULL_RESPONSE);
        }

        return response.getBody();
    }

    @Override
    public List<Company> getClientOrBuList(String userId, String companyId) {
        String url = ApiConstants.GET_CLIENT_OR_BU + "/" + companyId;

        ResponseEntity<List<Company>> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<Company>>() {
                });
        if (response != null && response.getBody() != null) {
            return response.getBody();
        }
        return null;
    }

    @Override
    public DashBoardDto getCandidateCount(String userId) {
        String url = ApiConstants.GET_CANDIDATES_COUNT + "/" + userId;

        ResponseEntity<DashBoardDto> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<DashBoardDto>() {
                });
        if (response != null && response.getBody() != null) {
            return response.getBody();
        }
        return null;
    }

    @Override
    public List<Candidate> getAllCandidates(CandidateRequestDto requestDto) {

        String url = ApiConstants.GET_ALL_CANDIDATES;

        ResponseEntity<List<Candidate>> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null,
                    new ParameterizedTypeReference<List<Candidate>>() {
                    });
        } catch (HttpClientErrorException e) {
            LOGGER.error("An error ocurred while fetching all candidates {}", e.getMessage());
        }

        if (response != null && response.getBody() != null) {
            return response.getBody();
        }

        return null;
    }

    @Override
    public List<Industry> getIndustries(String companyId) {
        String url = ApiConstants.GET_INDUSTIRES + "/" + companyId;
        ResponseEntity<List<Industry>> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<Industry>>() {
                });
        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, Messages.NULL_RESPONSE);
        }
        LOGGER.debug("Response {}", response);
        return response.getBody();
    }

    @Override
    public List<JobRequisitionRole> getJobRequisitionRoles(String companyId) {
        String url = ApiConstants.GET_REQUISITION_ROLE + "/" + companyId;
        ResponseEntity<List<JobRequisitionRole>> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<JobRequisitionRole>>() {
                });

        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, Messages.NULL_RESPONSE);
        }
        LOGGER.debug("Response {}", response);
        return response.getBody();
    }

    @Override
    public List<String> getJobRequisitionCategories(String companyId) {
        String url = ApiConstants.GET_REQUISITION_CATEGORY + "/" + companyId;
        ResponseEntity<List<String>> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<String>>() {
                });
        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, Messages.NULL_RESPONSE);
        }
        LOGGER.debug("Response {}", response);
        return response.getBody();
    }

    @Override
    public List<RoleEntityScope> getBasicEntityScope(String companyId) {
        String url = ApiConstants.GET_ROLE_ENTITY_SCOPE + "/" + companyId;
        LOGGER.debug("url {}", url);
        ParameterizedTypeReference<List<RoleEntityScope>> responseType = new ParameterizedTypeReference<List<RoleEntityScope>>() {
        };
        ResponseEntity<List<RoleEntityScope>> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null, responseType);
        } catch (HttpClientErrorException | HttpServerErrorException e) {

            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, Messages.NULL_RESPONSE);
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, Messages.NULL_RESPONSE);
        }

        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, Messages.NULL_RESPONSE);
        }

        LOGGER.debug("Response {}", response);

        return response.getBody();
    }

    @Override
    public List<Job> getrequisitionBasedOnClients(List<CompanyMinInfoDto> clients) {
        // TODO Auto-generated method stub
        return null;
    }

    /***************************************End************************************/

    @Override
    public Company createRoleEnityScope(String companyId, List<RoleEntityScope> roleEntityScopeList) {

        String url = ApiConstants.CREATE_ROLE_ENTITY_SCOPE + "/" + companyId;
        LOGGER.debug("Executing url: create role entity scope - {}", url);
        HttpEntity<List<RoleEntityScope>> httpEntity = new HttpEntity<>(roleEntityScopeList);
        ParameterizedTypeReference<Company> responseType = new ParameterizedTypeReference<Company>() {
        };
        ResponseEntity<Company> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.PUT, httpEntity, responseType);
        } catch (HttpClientErrorException | HttpServerErrorException e) {

            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG);
        }
        LOGGER.debug("Response: create role entity scope - {}", response.getBody());

        return response.getBody();
    }

    @Override
    public Company updateRoleEntityScope(String companyId, List<RoleEntityScope> roleEntityScopeList) {

        String url = ApiConstants.UPDATE_ROLE_ENTITY_SCOPE + "/" + companyId;
        LOGGER.debug("Executing url: Update role entity scope - {}", url);
        HttpEntity<List<RoleEntityScope>> httpEntity = new HttpEntity<>(roleEntityScopeList);
        ParameterizedTypeReference<Company> responseType = new ParameterizedTypeReference<Company>() {
        };
        ResponseEntity<Company> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.PUT, httpEntity, responseType);
        } catch (HttpClientErrorException | HttpServerErrorException e) {

            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG);
        }
        LOGGER.debug("Response: update role entity scope - {}", response.getBody());

        return response.getBody();
    }

    @Override
    public VerifyPassword verifyPassword(LoginDetails loginDetails) {
        String url = ApiConstants.VERIFY_PASSWORD + "/" +
                loginDetails.getEmail() + "/" + loginDetails.getPassword();

        ParameterizedTypeReference<VerifyPassword> responseType = new ParameterizedTypeReference<VerifyPassword>() {
        };
        LOGGER.info("verifyPassword URL " + url);
        ResponseEntity<VerifyPassword> response = null;
        try {
            LOGGER.debug("Executing url: verify password - {}", url);
            response = restTemplate.exchange(url, HttpMethod.GET, null, responseType);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            LOGGER.error(e.getMessage());
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                throw new InvalidCredentialsException(Messages.INVALID_EMAIL_OR_PASSWORD);
            } else if (e.getStatusCode().equals(HttpStatus.FORBIDDEN)) {
                throw new InvalidCredentialsException("This account is on 'Hold'. Please consult your administrator to resolve this issue.");
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }
        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG);
        }
        LOGGER.debug("Response: verify password - {}", response.getBody());
        return response.getBody();
    }

    @Override
    public String verifyToken(TokenLoginDetails tokenLoginDetails) {
        String url = ApiConstants.VERIFY_TOKEN + "/" +
                tokenLoginDetails.getEmail() + "/" + tokenLoginDetails.getToken();
        ResponseEntity<String> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(tokenLoginDetails), String.class);
        } catch (PlatformException e) {
            throw new InvalidCredentialsException(Messages.INVALID_EMAIL, e.getMessage());
        }
        return response.getBody();
    }

    @Override
    public void forgotPassword(String emailId) {
        String url = ApiConstants.FORGOT_PASSWORD + "/" + emailId + "/";

        LOGGER.debug("Executing url: Verify password - {}", url);
        ResponseEntity<String> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null, String.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                throw new InvalidCredentialsException(Messages.INVALID_EMAIL);
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        if (response != null && response.getBody().equals("false")) {
            throw new PlatformException(Messages.INVALID_EMAIL);
        }

        LOGGER.debug("Response: Verify password - {}", response);
    }

    @Override
    public void resetPassword(ResetPwdDetails pwdDetails) {
        HttpEntity<ResetPwdDetails> request = new HttpEntity<>(pwdDetails);

        LOGGER.debug("Executing url: Change password - {}", ApiConstants.CHANGE_PASSWORD);
        ResponseEntity<String> response;
        try {
            response = restTemplate.exchange(ApiConstants.CHANGE_PASSWORD, HttpMethod.POST, request, String.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw new InvalidResetPwdCodeException(Messages.INVALID_ORIG_PWD);
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        LOGGER.debug("Response: Change password - {}", response);
    }

    @Override
    public User verifyUsername(String email) {
        String url = ApiConstants.VERIFY_USERNAME + "/" + email + "/";
        LOGGER.debug("Executing url: Verify username - {}", url);
        ResponseEntity<User> response = null;
        try {
            LOGGER.info("PlatformApiExecutorImpl VerifyUserName URL" + url);
            response = restTemplate.exchange(url, HttpMethod.GET, null, User.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {

            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }
        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG);
        }
        LOGGER.debug("Response: Verify username - {}", response);
        return response.getBody();
    }

    @Override
    public String sendValueAssessment(SendAssessmentDto sendAssessmentDto) {
        String url = ApiConstants.SEND_VALUE_ASSESSMENT;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(sendAssessmentDto));
    }

    @Override
    public String sendTechAssessment(SendAssessmentDto sendAssessmentDto) {
        String url = ApiConstants.SEND_TECH_ASSESSMENT;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(sendAssessmentDto));
    }

    @Override
    public String cancelValueAssessment(CancelAssessmentDto cancelAssessmentDto) {
        String url = ApiConstants.CANCEL_VALUE_ASSESSMENT;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(cancelAssessmentDto));
    }

    @Override
    public String cancelTechAssessment(CancelAssessmentDto cancelAssessmentDto) {
        String url = ApiConstants.CANCEL_TECH_ASSESSMENT;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(cancelAssessmentDto));
    }

    @Override
    public String moveForward(ChangeStepDto changeStep) {
        String url = ApiConstants.MOVE_FORWARD;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(changeStep));
    }

    @Override
    public RoleScreenXRef getRoleScreenReference(String roleId) {
        String url = ApiConstants.GET_ROLES_SCREEN_REFERENCE + "/" + roleId + "/";

        LOGGER.debug("Executing url {}: Get role screen reference -", url);
        ResponseEntity<RoleScreenXRef> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null,
                    new ParameterizedTypeReference<RoleScreenXRef>() {
                    });
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }
        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG);
        }
        LOGGER.debug("Response: Get role screen reference - {}", response);
        return response.getBody();
    }

    @Override
    public String setPassword(PasswordDetailsDto passwordDetailsDto) {
        String url = ApiConstants.SET_PASSWORD;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(passwordDetailsDto));
    }

    @Override
    public PlanMinDto findCurrentPlanMinDtoByCompanyId(String companyId) {
        String url = ApiConstants.GET_CURRENT_PLAN_MIN_DTO_BY_COMPANY_ID + "/" + companyId;
        ResponseEntity<PlanMinDto> response = restTemplate.exchange(url, HttpMethod.GET, null
                , new ParameterizedTypeReference<PlanMinDto>() {
                });

        return response.getBody();
    }

    @Override
    public boolean removeManager(String userId) {
        String url = ApiConstants.DELETE_MANAGER + "/" + userId;
        LOGGER.debug("Executing url: Remove manager - {}", url);
        ResponseEntity<Boolean> response;
        try {
            response = restTemplate.exchange(url, HttpMethod.POST, null, new ParameterizedTypeReference<Boolean>() {
            });
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            LOGGER.error(e.getMessage());
            throw new RemoveManagerPlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        if (response == null) {
            LOGGER.error("error while removing manager, response from platform was null");
            throw new RemoveManagerPlatformException(Messages.INTERNAL_ERROR_MSG);
        } else {
            if (!response.getBody()) {
                LOGGER.error("error while removing manager, response from platform was false");
                throw new RemoveManagerPlatformException(Messages.INTERNAL_ERROR_MSG);
            }
        }
        LOGGER.debug("Response: Remove manager - {}", response);
        return response.getBody();
    }

    @Override
    public List<UserForwardedReference> getForwardedFromRecipients(String userId) {
        String url = ApiConstants.GET_FORWARDED_FROM_RECIPIENTS + "/" + userId;
        ResponseEntity<List<UserForwardedReference>> response = null;
        LOGGER.debug("Executing url: get forwarded from recipients - {}", url);
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null,
                    new ParameterizedTypeReference<List<UserForwardedReference>>() {
                    });
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }
        LOGGER.debug("Response: get forwarded from recipients - {}", response);
        if (response != null && response.getBody() != null) {
            return response.getBody();
        }
        return null;
    }

    @Override
    public Setting getFourDotFiveSettings() {
        String url = ApiConstants.GET_FOURDOTFIVE_SETTINGS;

        ResponseEntity<Setting> response = null;
        LOGGER.debug("Executing url: Get 4.5 settings - {}", url);
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<Setting>() {
            });
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        LOGGER.debug("Response: Get 4.5 settings - {}", response);
        if (response != null) {
            return response.getBody();
        }
        return null;
    }

    @Override
    public Setting getStaffingOrCorpSettings(String compId, boolean escalateIfNotFound) {
        String url = ApiConstants.GET_STAFFINGORCORP_SETTINGS + "/" + compId +
                "?" + "escalateIfNotFound=" + escalateIfNotFound;

        LOGGER.debug("Executing url: Get staffing or corp settings - {}", url);
        ResponseEntity<Setting> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<Setting>() {
            });
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        LOGGER.debug("Response: Get staffing or corp settings - {}", response);
        if (response != null) {
            return response.getBody();
        }
        return null;
    }

    @Override
    public Setting getClientOrBuSettings(String parentCompId, String childCompId, boolean escalateIfNotFound) {
        String url = ApiConstants.GET_CLIENTORBU_SETTINGS + "/" + parentCompId +
                "/" + childCompId + "?" + "escalateIfNotFound=" + escalateIfNotFound;

        LOGGER.debug("Executing url: get client or BU settings - {}", url);
        ResponseEntity<Setting> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<Setting>() {
            });
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        if (response != null) {
            return response.getBody();
        }
        LOGGER.debug("Response get client or BU settings - {}", response);
        return null;
    }

    @Override
    public Setting getParentSettings(String compId, boolean escalateIfNotFound) {
        String url = ApiConstants.GET_STAFFINGORCORP_SETTINGS + "/" + compId + "/" + escalateIfNotFound;

        LOGGER.debug("Executing url: Get staffing/Corporate settings - {}", url);
        ResponseEntity<Setting> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<Setting>() {
            });
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        if (response != null) {
            return response.getBody();
        }
        LOGGER.debug("Response: Get staffing/Corporate settings - {}", response);
        return null;
    }

    @Override
    public Setting saveSettings(Setting setting, String compId) {
        String url = ApiConstants.SAVE_SETTINGS + "/" + compId;

        LOGGER.debug("Executing url: Save settings - {}", url);
        ResponseEntity<Setting> response;
        try {
            response = restTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<>(setting),
                    new ParameterizedTypeReference<Setting>() {
                    });
        } catch (HttpClientErrorException | HttpServerErrorException | ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }
        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG);
        }
        LOGGER.debug("Response: Save settings - {}", response);
        return response.getBody();
    }

    @Override
    public void deleteSettings(String compId) {
        String url = ApiConstants.DELETE_SETTINGS + "/" + compId;
        LOGGER.debug("Executing url: Delete settings - {}", url);

        try {
            restTemplate.exchange(url, HttpMethod.DELETE, null, String.class);
        } catch (HttpClientErrorException | HttpServerErrorException | ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }
    }

    @Override
    public List<User> getUserListBasedOnRole(String roleId, String companyId) {
        String url = ApiConstants.GET_USERS_BASED_ON_ROLE + "/" + companyId +
                "/" + roleId;

        LOGGER.debug("Executing url: Get users based on role - {}", url);
        ResponseEntity<List<User>> response;
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<List<User>>() {
            });
        } catch (HttpClientErrorException | HttpServerErrorException | ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        LOGGER.debug("Response: Get users based on role - {}", response);
        if (response != null && response.getBody() != null) {
            return response.getBody();
        }
        return null;
    }

    @Override
    public NotificationPreference getNotificationPrefs(String userId) {
        ResponseEntity<NotificationPreference> response;

        String url = ApiConstants.GET_NOTIFICATION_PREFS + "/" + userId;

        LOGGER.debug("Executing url: Find notification preference by user - {}", url);
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null,
                    new ParameterizedTypeReference<NotificationPreference>() {
                    });
        } catch (HttpClientErrorException | HttpServerErrorException | ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG);
        }
        LOGGER.debug("Response: Find notification preference by user - {} ", response);
        return response.getBody();
    }

    @Override
    public NotificationPreference saveNotificationPrefs(NotificationPreference notificationPreference, String userId) {
        ResponseEntity<NotificationPreference> response;
        String url = ApiConstants.SAVE_NOTIFICATION_PREFS + "/" + userId;

        LOGGER.debug("Executing url: Save notification preference - {}", url);
        try {
            response = restTemplate.exchange(url, HttpMethod.PUT,
                    new HttpEntity<>(notificationPreference),
                    new ParameterizedTypeReference<NotificationPreference>() {
                    });
        } catch (HttpClientErrorException | HttpServerErrorException | ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        LOGGER.debug("Response: Save notification preference - {} ", response);
        if (response != null) {
            return response.getBody();
        }
        return null;
    }

    @Override
    public boolean removeForwardedFromRecipient(UserRecipient userRecipient) {

        String url = ApiConstants.REMOVE_FORWARDED_FROM_RECIPIENT + "/" +
                userRecipient.getFromUserID() + "/" + userRecipient.getToUserID();

        LOGGER.debug("Executing url: Remove forwarded from recipient - {}", url);
        ResponseEntity<Boolean> response;
        try {
            response = restTemplate.exchange(url, HttpMethod.POST, null, new ParameterizedTypeReference<Boolean>() {
            });
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            LOGGER.error(e.getMessage());
            throw new RemoveFRPlatformException(Messages.EXCEPTION_WHILE_REMOVING_FORWARD_RECIPIENT);
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG);
        }

        LOGGER.debug("Response: Remove forwarded from recipient - {} ", response);
        if (response == null) {
            throw new RemoveFRPlatformException(Messages.EXCEPTION_WHILE_REMOVING_FORWARD_RECIPIENT);
        } else {
            if (!response.getBody()) {
                throw new RemoveFRPlatformException(Messages.EXCEPTION_WHILE_REMOVING_FORWARD_RECIPIENT);
            }
        }
        return response.getBody();
    }

    @Override
    public boolean addForwardedFromRecipient(UserRecipient userRecipient) {

        ResponseEntity<Boolean> response;
        String url = ApiConstants.ADD_FORWARDED_FROM_RECIPIENT;

        LOGGER.debug("Executing url: Add forwarded from recipient - {}", url);
        try {
            response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(userRecipient),
                    new ParameterizedTypeReference<Boolean>() {
                    });
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            LOGGER.error("An error occured while adding forward from recipient ", e.getMessage());
            throw new AddFRPlaformException(Messages.EXCEPTION_WHILE_ADDING_FORWARD_RECIPIENT);
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, Messages.NULL_RESPONSE);
        }

        LOGGER.debug("Response Add forwarded from recipient - {} ", response);
        if (response != null) {
            if (!response.getBody()) {
                LOGGER.error("An error occured while adding forward from recipient ");
                throw new AddFRPlaformException(Messages.EXCEPTION_WHILE_ADDING_FORWARD_RECIPIENT);
            }
            return response.getBody();
        }

        return false;
    }


    @Override
    public Company saveQuestionList(String companyId, List<String> questionList) {
        String url = ApiConstants.SAVE_RECRUITER_QUESTIONLIST + companyId;
        LOGGER.debug("Executing url: Set RecruiterQuestionList - {}", url);
        ResponseEntity<Company> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(questionList),
                    new ParameterizedTypeReference<Company>() {
                    });

        } catch (HttpClientErrorException | HttpServerErrorException e) {
            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        return response.getBody();
    }

    @Override
    public List<String> getQuestionList(String companyId) {
        String url = ApiConstants.GET_RECRUITER_QUESTIONLIST + companyId;
        LOGGER.debug("Executing url: Get companyQuestionList - {}", url);

        ResponseEntity<List<String>> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null,
                    new ParameterizedTypeReference<List<String>>() {
                    });
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG);
        }
        LOGGER.debug("Response: Get companyQuestionList - {}", response);
        return response.getBody();
    }

    @Override
    public void addSettingsToCompany(String parentCompanyId, String companyId) {
        String url = ApiConstants.ADD_SETTINGS + "/" + parentCompanyId + "/" + companyId;
        restTemplate.exchange(url, HttpMethod.PUT, null, new ParameterizedTypeReference<Company>() {
        });
    }

    public List<NewJobDto> getFilteredJobs(String companyId, String userId, JobRequestDto jobRequestDto) {
        String url = ApiConstants.GET_JOBS_BY_FILTER + "/" + companyId + "?" + "page=" + jobRequestDto.getPageNum() +
                "&" + "size=" + jobRequestDto.getSize();
        ;
        ResponseEntity<List<NewJobDto>> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.POST,
                    new HttpEntity<>(jobRequestDto),
                    new ParameterizedTypeReference<List<NewJobDto>>() {
                    });
        } catch (HttpClientErrorException e) {
            LOGGER.error("Error occured while fetching open jobs");
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {

            }
        }

        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, Messages.NULL_RESPONSE);
        }
        System.out.println("========================================");
        System.out.println(response.getBody());
        System.out.println("========================================");
        return response.getBody();
    }


    public List<NewJobDto> getJobsByCompanyId(JobRequestDto jobRequestDto, String companyId, String userId) {
        String url = ApiConstants.GET_JOBS_BY_COMPANYID +
                "/" + companyId +
                "/" + userId +
                "?" + "page=" + jobRequestDto.getPageNum() +
                "&" + "size=" + jobRequestDto.getSize();
        ResponseEntity<List<NewJobDto>> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.POST,
                    new HttpEntity<>(jobRequestDto),
                    new ParameterizedTypeReference<List<NewJobDto>>() {
                    });
        } catch (HttpClientErrorException e) {
            LOGGER.error("Error occured while fetching open jobs");
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {

            }
        }

        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, Messages.NULL_RESPONSE);
        }
        System.out.println("========================================");
        System.out.println(response.getBody());
        System.out.println("========================================");
        return response.getBody();
    }

    public Job updateJobStatus(JobState jobState) {
        String url = ApiConstants.UPDATE_REQUISITION_STATUS;
        ResponseEntity<Job> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.POST,
                    new HttpEntity<>(jobState),
                    new ParameterizedTypeReference<Job>() {
                    });
        } catch (HttpClientErrorException e) {
            LOGGER.error("Error occured while fetching open jobs");
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {

            }
        }

        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, Messages.NULL_RESPONSE);
        }
        return response.getBody();
    }

    @Override
    public String getStatusTypes() {
        String url = ApiConstants.GET_STATUS_TYPES;
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<String>() {
                });
        return response.getBody();
    }

    @Override
    public FilterResponse getClientAndRecruitersByRoles(JobRequestDto jobRequestDto) {
        String url = ApiConstants.GET_CLIENT_RECRUITER_BY_ROLE;
        ResponseEntity<FilterResponse> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.POST,
                    new HttpEntity<>(jobRequestDto),
                    new ParameterizedTypeReference<FilterResponse>() {
                    });
        } catch (HttpClientErrorException e) {
            LOGGER.error("Error occured while fetching open jobs");
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {

            }
        }

        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, Messages.NULL_RESPONSE);
        }
        System.out.println("========================================");
        System.out.println(response.getBody());
        System.out.println("========================================");
        return response.getBody();
    }

    @Override
    public FilterResponse getClientAndRecruitersOfCompanyByRoles(JobRequestDto jobRequestDto, String companyId) {
        String url = ApiConstants.GET_CLIENT_RECRUITER_COMPANY_BY_ROLE + "/" + companyId;
        ResponseEntity<FilterResponse> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.POST,
                    new HttpEntity<>(jobRequestDto),
                    new ParameterizedTypeReference<FilterResponse>() {
                    });
        } catch (HttpClientErrorException e) {
            LOGGER.error("Error occured while fetching open jobs");
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {

            }
        }

        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, Messages.NULL_RESPONSE);
        }
        System.out.println("========================================");
        System.out.println(response.getBody());
        System.out.println("========================================");
        return response.getBody();
    }

    @Override
    public FilterResponse getClientAndRoleByRecruiters(JobRequestDto jobRequestDto) {
        String url = ApiConstants.GET_CLIENT_ROLE_BY_RECRUITER;
        ResponseEntity<FilterResponse> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.POST,
                    new HttpEntity<>(jobRequestDto),
                    new ParameterizedTypeReference<FilterResponse>() {
                    });
        } catch (HttpClientErrorException e) {
            LOGGER.error("Error occured while fetching open jobs");
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {

            }
        }

        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, Messages.NULL_RESPONSE);
        }
        System.out.println("========================================");
        System.out.println(response.getBody());
        System.out.println("========================================");
        return response.getBody();
    }

    @Override
    public FilterResponse getClientAndRoleOfCompanyByRecruiters(JobRequestDto jobRequestDto, String companyId) {
        String url = ApiConstants.GET_CLIENT_ROLE_COMPANY_BY_RECRUITER + "/" + companyId;
        ResponseEntity<FilterResponse> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.POST,
                    new HttpEntity<>(jobRequestDto),
                    new ParameterizedTypeReference<FilterResponse>() {
                    });
        } catch (HttpClientErrorException e) {
            LOGGER.error("Error occured while fetching open jobs");
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {

            }
        }

        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, Messages.NULL_RESPONSE);
        }
        System.out.println("========================================");
        System.out.println(response.getBody());
        System.out.println("========================================");
        return response.getBody();
    }

    @Override
    public FilterResponse getRoleAndRecruitersByClients(JobRequestDto jobRequestDto) {
        String url = ApiConstants.GET_ROLE_RECRUITER_BY_CLIENT;
        ResponseEntity<FilterResponse> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.POST,
                    new HttpEntity<>(jobRequestDto),
                    new ParameterizedTypeReference<FilterResponse>() {
                    });
        } catch (HttpClientErrorException e) {
            LOGGER.error("Error occured while fetching open jobs");
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {

            }
        }

        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, Messages.NULL_RESPONSE);
        }
        System.out.println("========================================");
        System.out.println(response.getBody());
        System.out.println("========================================");
        return response.getBody();
    }

    @Override
    public FilterResponse getRoleAndRecruitersOfCompanyByClients(JobRequestDto jobRequestDto, String companyId) {
        String url = ApiConstants.GET_ROLE_RECRUITER_COMPANY_BY_CLIENT + "/" + companyId;
        ResponseEntity<FilterResponse> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.POST,
                    new HttpEntity<>(jobRequestDto),
                    new ParameterizedTypeReference<FilterResponse>() {
                    });
        } catch (HttpClientErrorException e) {
            LOGGER.error("Error occured while fetching open jobs");
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {

            }
        }

        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, Messages.NULL_RESPONSE);
        }
        System.out.println("========================================");
        System.out.println(response.getBody());
        System.out.println("========================================");
        return response.getBody();
    }

    @Override
    public List<String> searchByReqNumber(String searchText, String companyId, List<String> userList) {
        String url = ApiConstants.SEARCH_BY_REQ_NUM + "/" + searchText + "/" + companyId;
        ResponseEntity<List<String>> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.POST,
                    new HttpEntity<>(userList),
                    new ParameterizedTypeReference<List<String>>() {
                    });
        } catch (HttpClientErrorException e) {
            LOGGER.error("Error occured while fetching open jobs");
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {

            }
        }

        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, Messages.NULL_RESPONSE);
        }
        System.out.println("========================================");
        System.out.println(response.getBody());
        System.out.println("========================================");
        return response.getBody();
    }

    @Override
    public List<String> searchByReqNumberCompany(String searchText, String companyId) {
        String url = ApiConstants.SEARCH_BY_REQ_NUM_COMPANY + "/" + searchText + "/" + companyId;
        ResponseEntity<List<String>> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.POST,
                    null,
                    new ParameterizedTypeReference<List<String>>() {
                    });
        } catch (HttpClientErrorException e) {
            LOGGER.error("Error occured while fetching open jobs");
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {

            }
        }

        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, Messages.NULL_RESPONSE);
        }
        System.out.println("========================================");
        System.out.println(response.getBody());
        System.out.println("========================================");
        return response.getBody();
    }

    @Override
    public String searchJobByText(JobRequestDto jobRequestDto, String companyId, String userId) {
        Integer pageNumber = null;
        if (jobRequestDto.getPageNum() != null)
            pageNumber = Integer.parseInt(jobRequestDto.getPageNum());
        Integer pageSize = null;
        if (jobRequestDto.getSize() != null)
            pageSize = Integer.parseInt(jobRequestDto.getSize());
        String url = ApiConstants.SEARCH_JOB_BY_TEXT +
                "/" + companyId +
                "/" + userId + "?" + "page=" + pageNumber + "&" + "size=" + pageSize;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(jobRequestDto));
    }

    @Override
    public String searchJobInCompanyByText(JobRequestDto jobRequestDto, String companyId, String userId) {
        Integer pageNumber = null;
        if (jobRequestDto.getPageNum() != null)
            pageNumber = Integer.parseInt(jobRequestDto.getPageNum());
        Integer pageSize = null;
        if (jobRequestDto.getSize() != null)
            pageSize = Integer.parseInt(jobRequestDto.getSize());

        String url = ApiConstants.SEARCH_JOB_COMPANY_BY_TEXT +
                "/" + companyId +
                "/" + userId + "?" + "page=" + pageNumber + "&" + "size=" + pageSize;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(jobRequestDto));
    }

    @Override
    public List<ResumeSourceType> getResumeSourceTypes(String companyId, String buId) {
        String url = ApiConstants.GET_RESUME_SOURCE_TYPES;
        if (StringUtils.isNotBlank(companyId)) {
            url = url + "?companyId=" + companyId;
            if (StringUtils.isNotBlank(buId)) {
                url = url + "&buId=" + buId;
            }
        }
        ResponseEntity<List<ResumeSourceType>> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<ResumeSourceType>>() {
                });
        return response.getBody();
    }

    @Override
    public ResumeSourceType getResumeSourceTypeById(String id) {
        String url = ApiConstants.GET_RESUME_SOURCE_TYPE_BY_ID + "/" + id;

        ResponseEntity<ResumeSourceType> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<ResumeSourceType>() {
                });
        return response.getBody();
    }

    @Override
    public String uploadResumes(List<MultipartFile> reqFiles, ResumeUpload resumeUpload) throws IOException {
        System.out.println("In api executor - companyId - " + resumeUpload.getCompanyId() + " userId - " + resumeUpload.getUserId() + " buId - " + resumeUpload.getBuIdList());
        //String url = new StringBuilder().append(ApiConstants.UPLOAD_REQ_DOC).toString();
        String url = ApiConstants.UPLOAD_RESUME;
        System.out.println("URL generated - " + url);
        MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        System.out.println("req files length - " + reqFiles.size());
        //List<ByteArrayResource> allResources = new ArrayList<ByteArrayResource>();
        for (MultipartFile file : reqFiles) {
            String fileName = file.getOriginalFilename();
            if (StringUtils.isNotBlank(fileName)) {
                fileName = fileName.replace("\u00A0", " ");
            }
            final String name = fileName;
            ByteArrayResource resource = new ByteArrayResource(file.getBytes()) {
                @Override
                public String getFilename() {
                    return name;
                }
            };
            map.add("files", resource);
            //allResources.add(resource);
        }
        //map.add("files", reqFiles);
        map.add("resumeUpload", resumeUpload);

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(map, header);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
                new ParameterizedTypeReference<String>() {
                });
        //System.out.println("Response entity - " + response.getBody());
        return response.getBody();
    }

    //@Override
    public String processResume(JSONObject fileInfo) {
        String url = ApiConstants.PROCESS_RESUME;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> requestEntity = new HttpEntity<>(fileInfo.toString(), headers);

        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
                new ParameterizedTypeReference<String>() {
                });

        System.out.println("Process Job Response - " + response.getBody());
        return response.getBody();
    }

    @Override
    public String runResumesForScoring(ParsingOutputObject parsingOutputObject, String transactionType,
                                       String clientorbuid, String reqtransactionId, String jobId, String match, String loggedInUserId, boolean hasToOverride) {
        String url = ApiConstants.RUN_RESUME_FOR_SCORING +
                "/" + transactionType +
                "?" + "clientorbuid=" + clientorbuid +
                "&" + "reqtransactionId=" + reqtransactionId +
                "&" + "jobId=" + jobId +
                "&" + "match=" + match +
                "&" + "loggedinuserid=" + loggedInUserId +
                "&" + "hasToOverride=" + hasToOverride;
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST,
                new HttpEntity<>(parsingOutputObject),
                new ParameterizedTypeReference<String>() {
                });
        return response.getBody();
    }

    @Override
    public String runResumesForAssociate(CandidateResumeOutput parsingOutputObject, String transactionType, String clientorbuid, String reqtransactionId, String jobId, boolean forceToMatch, String loggedInUserId, String candidateId) {
        String url = ApiConstants.RUN_RESUME_FOR_ASSOCIATE +
                "/" + transactionType
                + "/" + candidateId
                + "?" + "clientorbuid=" + clientorbuid
                + "&" + "reqtransactionId=" + reqtransactionId
                + "&" + "jobId=" + jobId
                + "&" + "forceToMatch=" + forceToMatch
                + "&" + "loggedinuserid=" + loggedInUserId;
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST,
                new HttpEntity<>(parsingOutputObject),
                new ParameterizedTypeReference<String>() {
                });
        return response.getBody();
    }

    @Override
    public UploadResumeCandidateDto getCandidateCountByResumeTransaction(String transactionId) {
        String url = ApiConstants.GET_CANDIDATE_COUNT_BY_RESUME_TRANSACTION_ID + "/" + transactionId;
        ResponseEntity<UploadResumeCandidateDto> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<UploadResumeCandidateDto>() {
                });
        return response.getBody();
    }

    @Override
    public List<CandidateReqResumeMinInfoDto> getCandidatesByResumeTransaction(String transactionId, String countType) {
        String url = ApiConstants.GET_CANDIDATES_BY_RESUME_TRANSACTION_ID + "/" + transactionId + "/" + countType;
        ResponseEntity<List<CandidateReqResumeMinInfoDto>> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<CandidateReqResumeMinInfoDto>>() {
                });
        return response.getBody();
    }

    @Override
    public String updateScoreBasedOnPreviousResume(String resumeId) {
        String url = ApiConstants.UPDATE_SCORE_BASED_ON_RECENT_RESUME + "/" + resumeId;
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(null),
                new ParameterizedTypeReference<String>() {
                });
        return response.getBody();
    }

    @Override
    public String processResumesWithoutScoring(ParsingOutputObject parsingOutputObject, String transactionType, String clientorbuid, String reqtransactionId, String jobId, String match, String loggedInUserId) {
        String url = ApiConstants.RUN_RESUME_WITHOUT_SCORING +
                "/" + transactionType +
                "?" + "clientorbuid=" + clientorbuid +
                "&" + "reqtransactionId=" + reqtransactionId +
                "&" + "jobId=" + jobId +
                "&" + "match=" + match +
                "&" + "loggedinuserid=" + loggedInUserId;

        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST,
                new HttpEntity<>(parsingOutputObject),
                new ParameterizedTypeReference<String>() {
                });
        return response.getBody();
    }

    @Override
    public byte[] getResume(String candidateId) {
        String url = ApiConstants.GET_RESUME_BY_ID + "/" + candidateId;
        ResponseEntity<byte[]> response = restTemplate.exchange(url,
                HttpMethod.GET, null,
                new ParameterizedTypeReference<byte[]>() {
                });
        return response.getBody();
    }

    @Override
    public byte[] getResumeHrXml(String candidateId) {
        String url = ApiConstants.GET_RESUME_HRXML_BY_ID + "/" + candidateId;
        ResponseEntity<byte[]> response = restTemplate.exchange(url,
                HttpMethod.GET, null,
                new ParameterizedTypeReference<byte[]>() {
                });
        return response.getBody();
    }

    @Override
    public String getCandidateLatestResume(String candidateId) {
        String url = ApiConstants.GET_CANDIDATE_LATEST_RESUME + "/" + candidateId;
        ResponseEntity<String> response = restTemplate.exchange(url,
                HttpMethod.GET, null,
                new ParameterizedTypeReference<String>() {
                });
        return response.getBody();
    }

    @Override
    public List<JobMatchCandidateDto> jobMatchByCandidate(CandidateJobMatchRequestDTO candidateJobMatchRequestDTO, Integer page, Integer size, String sort, String sortDir) {
        String url = ApiConstants.GET_JOB_MATCH_BY_CANDIDATE
                + "?" + "page=" + page
                + "&" + "size=" + size
                + "&" + "sort=" + sort
                + "&" + "sortDir=" + sortDir;
        ResponseEntity<List<JobMatchCandidateDto>> response = restTemplate.exchange(url,
                HttpMethod.POST, new HttpEntity<>(candidateJobMatchRequestDTO),
                new ParameterizedTypeReference<List<JobMatchCandidateDto>>() {
                });
        return response.getBody();
    }

    @Override
    public String getCandidateIdNamesByUser(CandidateFilterParams filterParams) {
        String url = ApiConstants.GET_CANDIDATE_ID_NAME_BY_USER;
        System.out.println("url======" + url);
        ResponseEntity<String> response = null;
        response = restTemplate.exchange(url, HttpMethod.POST,
                new HttpEntity<>(filterParams),
                new ParameterizedTypeReference<String>() {
                });

        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, Messages.NULL_RESPONSE);
        }
        System.out.println("========================================");
        //System.out.println(response.getBody());
        System.out.println("========================================");
        return response.getBody();
    }

    @Override
    public String getCandidateIdNamesByCompany(CandidateFilterParams filterParams) {
        String url = ApiConstants.GET_CANDIDATE_ID_NAME_BY_COMPANY;
        ResponseEntity<String> response = null;

        System.out.println("url======" + url);

        try {
            response = restTemplate.exchange(url, HttpMethod.POST,
                    new HttpEntity<>(filterParams),
                    new ParameterizedTypeReference<String>() {
                    });
        } catch (HttpClientErrorException e) {
            LOGGER.error("Error occured while fetching candidate name and ids");
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {

            }
        }

        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, Messages.NULL_RESPONSE);
        }
        System.out.println("========================================");
        //System.out.println(response.getBody());
        System.out.println("========================================");
        return response.getBody();
    }

    @Override
    public CandidateCardDto getCandidateCard(String candidateId, String loggedInUserId, CandidateCardFilterParams filterParams) {
        String url = ApiConstants.GET_CANDIDATE_CARD_INFO +
                "/" + candidateId +
                "/" + loggedInUserId;
        ResponseEntity<CandidateCardDto> response = restTemplate.exchange(url, HttpMethod.POST,
                new HttpEntity<>(filterParams),
                new ParameterizedTypeReference<CandidateCardDto>() {
                });
        return response.getBody();
    }

    @Override
    public FourDotFiveResponse<JobResponseDto> createJob(JobCreationModel jobCreationModel,
                                                         String companyId, String clientOrBuid) {
        System.out.println("In api executor -" + companyId);

        String url = ApiConstants.CREATE_JOB +
                "/" + companyId +
                "?" + "clientOrBuid=" + clientOrBuid;

        System.out.println("URL generated - " + url);

        ResponseEntity<FourDotFiveResponse<JobResponseDto>> response = restTemplate.exchange(url, HttpMethod.POST,
                new HttpEntity<>(jobCreationModel),
                new ParameterizedTypeReference<FourDotFiveResponse<JobResponseDto>>() {
                });

        System.out.println("Save and Parse Response - " + response.getBody());
        return response.getBody();

    }

    @Override
    public FourDotFiveResponse<JobResponseDto> updateJob(JobCreationModel jobCreationModel,
                                                         String companyId, String userId, String clientOrBuid) {
        System.out.println("In api executor -" + companyId);

        String url = ApiConstants.UPDATE_JOB +
                "/" + companyId +
                "/" + userId +
                "?" + "clientOrBuid=" + clientOrBuid;

        System.out.println("URL generated - " + url);

        ResponseEntity<FourDotFiveResponse<JobResponseDto>> response = restTemplate.exchange(url, HttpMethod.POST,
                new HttpEntity<>(jobCreationModel),
                new ParameterizedTypeReference<FourDotFiveResponse<JobResponseDto>>() {
                });

        System.out.println("Save and Parse Response - " + response.getBody());
        return response.getBody();

    }

    @Override
    public FourDotFiveResponse<HashMap<String, Object>> getMoreJobBoardCandidatesForJob(GetJBCandidatesModel getJBCandidatesModel) {

        System.out.println("In api executor get more jb candidates -" + getJBCandidatesModel.getJobId());

        String url = ApiConstants.GET_MORE_JB_CANDIDATES;

        System.out.println("URL generated - " + url);

        ResponseEntity<FourDotFiveResponse<HashMap<String, Object>>> response = restTemplate.exchange(url, HttpMethod.POST,
                new HttpEntity<>(getJBCandidatesModel),
                new ParameterizedTypeReference<FourDotFiveResponse<HashMap<String, Object>>>() {
                });

        FourDotFiveResponse<HashMap<String, Object>> fourDotFiveResponse = response.getBody();
        System.out.println("Save and Parse Response - " + fourDotFiveResponse);
        if (fourDotFiveResponse.isError()) {
            throw new PlatformException(fourDotFiveResponse.getMessage(), Messages.INTERNAL_ERROR_MSG);
        }

        return response.getBody();
    }

    @Override
    public Candidate setStatus(JobStateChange jobStateChange) {
        System.out.println("In api executor -" + jobStateChange.toString());

        String url = ApiConstants.CANDIDATE_SET_STATUS;

        System.out.println("URL generated - " + url);

        ResponseEntity<Candidate> response = restTemplate.exchange(url, HttpMethod.POST,
                new HttpEntity<>(jobStateChange),
                new ParameterizedTypeReference<Candidate>() {
                });

        System.out.println("Save and Parse Response - " + response.getBody());
        return response.getBody();
    }


    @Override
    public List<ActivityDto> getRequisitionActivity(RequisitionActivityFilterDto requisitionActivityFilterDto) {
        requisitionActivityFilterDto.setActivityMode("requisition");
        String url = ApiConstants.GET_ACTIVITY;

        ResponseEntity<List<ActivityDto>> response = restTemplate.exchange(url, HttpMethod.POST,
                new HttpEntity<>(requisitionActivityFilterDto), new ParameterizedTypeReference<List<ActivityDto>>() {
                });

        return response.getBody();
    }


    public List<String> getAllRolesInACompany(String companyId) {
        String url = ApiConstants.GET_ALL_ROLES_COMPANY + "/" + companyId;
        ResponseEntity<List<String>> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<String>>() {
                });
        return response.getBody();
    }

    @Override
    public JobMatchWorkflowDto getWorkFlowJobMatchesForCompany(int page, int size, String companyId, String userId, String requisitionId, JobMatchFilterDto jobMatchFilter) {
        System.out.println("In api executor -" + jobMatchFilter.toString());

        String url = ApiConstants.CANDIDATE_CARD_WORKFLOW_COMPANY +
                "?" + "page=" + page +
                "&" + "size=" + size + "&" + "requisitionId=" + requisitionId + "&" +
                "userId=" + userId + "&" + "companyId=" + companyId;

        System.out.println("URL generated - " + url);

        ResponseEntity<JobMatchWorkflowDto> response = restTemplate.exchange(url, HttpMethod.POST,
                new HttpEntity<>(jobMatchFilter),
                new ParameterizedTypeReference<JobMatchWorkflowDto>() {
                });

        System.out.println("Save and Parse Response - " + response.getBody());
        return response.getBody();
    }


    @Override
    public JobMatchWorkflowDto getWorkFlowJobMatchesForUser(int page, int size, String companyId, String userId, String requisitionId, JobMatchFilterDto jobMatchFilter) {
        System.out.println("In api executor -" + jobMatchFilter.toString());

        String url = ApiConstants.CANDIDATE_CARD_WORKFLOW_USER +
                "?" + "page=" + page +
                "&" + "size=" + size + "&" + "requisitionId=" + requisitionId + "&" +
                "userId=" + userId + "&" + "companyId=" + companyId;

        System.out.println("URL generated - " + url);

        ResponseEntity<JobMatchWorkflowDto> response = restTemplate.exchange(url, HttpMethod.POST,
                new HttpEntity<>(jobMatchFilter),
                new ParameterizedTypeReference<JobMatchWorkflowDto>() {
                });

        System.out.println("Save and Parse Response - " + response.getBody());
        return response.getBody();
    }

    @Override
    public String setCandidateWorkflowCard(String jobMatchId, String loggedInUserId, String companyId) {
        String url = ApiConstants.CANDIDATE_CARD_INFO +
                "/" + jobMatchId +
                "/" + loggedInUserId +
                "/" + companyId;
        return getResponse(url, HttpMethod.GET, new HttpEntity<>(null));
    }

    @Override
    public List<ContactsNotesDto> getContactNoteDetails(String jobmatchid, String companyId, String userId) {
        String url = ApiConstants.GET_CONTACT_NOTES_BY_JOBMATCH + "/" + jobmatchid;
        if (StringUtils.isNotBlank(companyId) && StringUtils.isNotBlank(userId)) {
            url = url + "?companyId=" + companyId + "&userId=" + userId;
        }
        ResponseEntity<List<ContactsNotesDto>> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<ContactsNotesDto>>() {
                });
        return response.getBody();
    }

    @Override
    public String getCandidateJobComparisonByJobMatchId(String jobMatchId) {
        String url = ApiConstants.GET_JOB_COMPARISON + "/" + jobMatchId;
        return getResponse(url, HttpMethod.GET, null);
    }

    @Override
    public String getJobDetailsById(String jobId) {
        String url = ApiConstants.GET_JOB_DETAILS_BY_ID + "/" + jobId;
        return getResponse(url, HttpMethod.GET, null);
    }

    @Override
    public String getCandidateProfile(String candidateId) {
        String url = ApiConstants.GET_CANDIDATE_PROFILE_BY_ID + "/" + candidateId;
        return getResponse(url, HttpMethod.GET, null);
    }

    @Override
    public String getMultipleCandidateProfileSingleJobComparison(List<String> candidateIds, String jobId) {
        String url = ApiConstants.GET_MULTIPLE_CANDIDATE_SINGLE_JOB_COMPARISON + "/" + jobId + "?candidateIds=" + StringUtils.join(candidateIds, ",");
        return getResponse(url, HttpMethod.GET, null);
    }

    @Override
    public String getMultipleJobSingleCandidateProfileComparison(List<String> jobIds, String candidateId) {
        String url = ApiConstants.GET_MULTIPLE_JOB_SINGLE_CANDIDATE_COMPARISON + "/" + candidateId + "?jobIds=" + StringUtils.join(jobIds, ",");
        return getResponse(url, HttpMethod.GET, null);
    }

    @Override
    public String syncAssessments(String companyId) {
        String url = ApiConstants.SYNC_ASSESSMENT;
        if (StringUtils.isNotEmpty(companyId)) {
            url = url + "?companyId=" + companyId;
        }
        return getResponse(url, HttpMethod.PUT, null);
    }

    @Override
    public String schedulePhoneScreen(CandidateMeetingScheduleDto candidateMeetingSchedule) {
        String url = ApiConstants.SCHEDULE_PHONE_SCREEN;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(candidateMeetingSchedule));
    }

    @Override
    public String scheduleInterview(CandidateMeetingScheduleDto candidateMeetingSchedule) {
        String url = ApiConstants.SCHEDULE_INTERVIEW;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(candidateMeetingSchedule));
    }

    @Override
    public String assignAssessment(Map<String, List<String>> assessments) {
        String url = ApiConstants.ASSIGN_ASSESSMENT;
        return getResponse(url, HttpMethod.PUT, new HttpEntity<>(assessments));
    }

    @Override
    public String getAllValueAssessmentsByCompanyId(String companyId, String clientOrBuId) {
        String url = ApiConstants.GET_ALL_VALUE_ASSESSMENTS_BY_COMPANY_ID + "/" + companyId + "?clientOrBuId=" + clientOrBuId;
        return getResponse(url, HttpMethod.GET, null);
    }

    @Override
    public String getAllTechAssessmentsByCompanyId(String companyId, String clientOrBuId) {
        String url = ApiConstants.GET_ALL_TECH_ASSESSMENTS_BY_COMPANY_ID + "/" + companyId + "?clientOrBuId=" + clientOrBuId;
        return getResponse(url, HttpMethod.GET, null);
    }

    @Override
    public String getAllUnassignedAssessmentsByCompanyId(String companyId, String clientOrBuId) {
        String url = ApiConstants.GET_ALL_UNASSIGNED_ASSESSMENTS_BY_COMPANY_ID + "/" + companyId + "?clientOrBuId =" + clientOrBuId;
        return getResponse(url, HttpMethod.GET, null);
    }

    private String getResponse(String url, HttpMethod httpMethod, HttpEntity httpEntity) {
        ResponseEntity<String> response = restTemplate.exchange(url, httpMethod, httpEntity, String.class);
        return response.getBody();
    }

    @Override
    public RequisitionStatusResponse getCandidatesForRequisitionStatus(String requisitionId, String requisitionStatus) {
        String url = ApiConstants.GET_CANDIDATE_FOR_REQ_STATUS +
                "/" + requisitionId +
                "/" + requisitionStatus;
        ResponseEntity<RequisitionStatusResponse> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<RequisitionStatusResponse>() {
                });
        return response.getBody();
    }

    @Override
    public List<ContactsNotesDto> saveNote(ContactsNotesDto contactNote) {
        String url = ApiConstants.SAVE_CONTACT_NOTES + "/" + contactNote.getJobMatchId();
        //return getResponse(url, HttpMethod.POST, new HttpEntity<>(meetingSchedule));

        ResponseEntity<List<ContactsNotesDto>> response = restTemplate.exchange(url, HttpMethod.POST,
                new HttpEntity<>(contactNote),
                new ParameterizedTypeReference<List<ContactsNotesDto>>() {
                });

        System.out.println("Save and Parse Response - " + response.getBody());
        return response.getBody();
    }

    @Override
    public List<ContactsNotesDto> deleteNote(DeleteNoteDto deleteNoteDto) {
        String url = ApiConstants.SAVE_NOTE +
                "/" + deleteNoteDto.getJobMatchId() +
                "/" + deleteNoteDto.getNoteId() +
                "?userId=" + deleteNoteDto.getUserId() +
                "&companyId=" + deleteNoteDto.getCompanyId();

        ResponseEntity<List<ContactsNotesDto>> response = restTemplate.exchange(url, HttpMethod.DELETE,
                null,
                new ParameterizedTypeReference<List<ContactsNotesDto>>() {
                });

        System.out.println("Save and Parse Response - " + response.getBody());
        return response.getBody();
    }


    @Override

    public String saveFeedBack(FeedbackQuestions feedback) {
        String url = ApiConstants.SAVE_FEEDBACK_FORM;
        LOGGER.debug("Executing url: Save FeedBack - {}", url);
        ResponseEntity<String> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<>(feedback),
                    new ParameterizedTypeReference<String>() {
                    });

        } catch (HttpClientErrorException | HttpServerErrorException e) {
            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        return response.getBody();
    }


    @Override

    public FeedbackQuestions getFeedBack(String companyId, String questionType) {
        String url = ApiConstants.GET_FEEDBACK_FORM + "/" + companyId + "/" + questionType;

        LOGGER.debug("Executing url: Get FeedBack - {}", url);
        ResponseEntity<FeedbackQuestions> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null,
                    new ParameterizedTypeReference<FeedbackQuestions>() {
                    });

        } catch (HttpClientErrorException | HttpServerErrorException e) {
            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }
        return response.getBody();
    }

    @Override

    public FeedbackAnswersDto getAllRecruiterQuestions(String meetingScheduleId, String jobMatchId, String userId, String companyId, String loggedInUserId) {
        String url = ApiConstants.GET_ALL_RECRUITER_QUESTIONS + jobMatchId + "/" + userId + "?companyId=" + companyId
                + "&loggedInUserId=" + loggedInUserId + "&meetingScheduleId=" + meetingScheduleId;
        LOGGER.debug("Executing url: Set RecruiterQuestionList - {}", url);
        ResponseEntity<FeedbackAnswersDto> response = null;
        response = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<FeedbackAnswersDto>() {
        });
        return response.getBody();
    }

    public String moveBackward(ChangeStepDto changeStep) {
        String url = ApiConstants.MOVE_BACKWARD;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(changeStep));
    }

    @Override
    public FeedbackAnswersDto save(FeedbackExecutionResult feedbackExecutionResult, String jobMatchId) {
        String url = ApiConstants.SAVE_RECRUITER_SCREENING_QUESTION + jobMatchId;

        LOGGER.debug("Executing url: save RecruiterSceeningQuestions - {}", url);
        ResponseEntity<FeedbackAnswersDto> response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(feedbackExecutionResult),
                new ParameterizedTypeReference<FeedbackAnswersDto>() {
                });
        return response.getBody();
    }

    @Override
    public String setReleased(List<CandidateJobMatchInfoDto> candidateJobMatchDetails) {
        String url = ApiConstants.SET_RELEASED;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(candidateJobMatchDetails));
    }

    @Override
    public String setNotInterested(List<CandidateJobMatchInfoDto> candidateJobMatchDetails) {
        String url = ApiConstants.SET_NOT_INTERESTED;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(candidateJobMatchDetails));
    }

    @Override
    public List<JobOpening> getCandidatesForCurrentState(String requisitionId, String requisitionStatus) {
        String url = ApiConstants.GET_CANDIDATE_FOR_CURRENT_STATUS +
                "/" + requisitionId +
                "/" + requisitionStatus;
        ResponseEntity<List<JobOpening>> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<JobOpening>>() {
                });

        return response.getBody();
    }

    @Override
    public FeedbackAnswersDto saveJobMatchState(String statusOfSave, List<JobDetailsRecruiterScreening> jobDetailsRecruiterScreeningList) {
        String url = ApiConstants.SAVE_JOB_MATCH_STATE + statusOfSave;

        LOGGER.debug("Executing url: save RecruiterSceeningQuestions - {}", url);
        ResponseEntity<FeedbackAnswersDto> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(jobDetailsRecruiterScreeningList),
                    new ParameterizedTypeReference<FeedbackAnswersDto>() {
                    });

        } catch (HttpClientErrorException | HttpServerErrorException e) {
            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        return response.getBody();
    }

    @Override
    public void retrieveFile(FileRetrieve fileRetrieve) {
        String url = ApiConstants.GET_FILE;
        restTemplate.getMessageConverters().add(new ByteArrayHttpMessageConverter());
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_OCTET_STREAM));
        HttpEntity<String> entity = new HttpEntity<>(headers);
        LOGGER.debug("Executing url: save RecruiterSceeningQuestions - {}", url);


        try {
            ResponseEntity<byte[]> response = restTemplate.exchange(url, HttpMethod.POST, entity, byte[].class, "1");
            if (response.getStatusCode().equals(HttpStatus.OK)) {
                /*FileOutputStream output = new FileOutputStream(new File("filename.jar"));
                IOUtils.write(response.getBody(), output);*/


            }

        } catch (HttpClientErrorException | HttpServerErrorException e) {
            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new
                        PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }


    }

    @Override
    public List<BigFiveQuestions> getBigFiveQuestions() {
        String url = ApiConstants.GET_ALL_BIGFIVE_QUESTIONS;

        LOGGER.debug("Executing url: Set RecruiterQuestionList - {}", url);
        ResponseEntity<List<BigFiveQuestions>> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null,
                    new ParameterizedTypeReference<List<BigFiveQuestions>>() {
                    });

        } catch (HttpClientErrorException | HttpServerErrorException e) {
            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        return response.getBody();
    }

    @Override
    public CandidateBigFiveResponsesDto getCandidateBigFiveResponses(String jobMatchId) {
        String url = ApiConstants.GET_ALL_CANDIDATE_BIGFIVE_RESPONSES + jobMatchId;

        LOGGER.debug("Executing url: Get all candidate responses - {}", url);
        ResponseEntity<CandidateBigFiveResponsesDto> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null,
                    new ParameterizedTypeReference<CandidateBigFiveResponsesDto>() {
                    });

        } catch (HttpClientErrorException | HttpServerErrorException e) {
            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        return response.getBody();
    }

    @Override
    public void saveCandidateBigFiveResponses(CandidateBigFiveResponses candidateBigFiveResponses) {
        String url = ApiConstants.SAVE_CANDIDATE_BIGFIVE_RESPONSES;

        LOGGER.debug("Executing url: Save candidate response - {}", url);
        ResponseEntity<String> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(candidateBigFiveResponses),
                    new ParameterizedTypeReference<String>() {
                    });

        } catch (HttpClientErrorException | HttpServerErrorException e) {
            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        LOGGER.debug("Response: Save Candidate Big Five Response - {}", response);
    }


    public String getActivityTypes() {
        String url = ApiConstants.GET_ACTIVITY_TYPES;
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<String>() {
                });
        return response.getBody();
    }

    public String getEnabledSteps(String jobId) {
        String url = ApiConstants.GET_ENABLED_STEPS + jobId;
        return getResponse(url, HttpMethod.GET, null);
    }

    public String getTypes(String typeName) {
        String url = ApiConstants.GET_TYPES + typeName;
        return getResponse(url, HttpMethod.GET, null);
    }

    public String getTypesWithText(String typeName) {
        String url = ApiConstants.GET_TYPES_WITH_TEXT + typeName;
        return getResponse(url, HttpMethod.GET, null);
    }

    public String moveToAny(CandidateMoveStepDto candidateMoveStep) {
        String url = ApiConstants.MOVE_TO_ANY;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(candidateMoveStep));
    }

    @Override
    public String imCandidateReportCallback(IMCandidateReportCallbackDto callback) {
        String url = ApiConstants.IM_CANDIDATE_REPORT_CALLBACK;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(callback));
    }

    @Override
    public CandidateStatusCountDto getCandidateStatusCountByCompany(String companyId, String visibility, String userId) {
        String url = ApiConstants.GET_CANDIDATE_STATUS_COUNT_COMPANY + companyId + '/' + userId + "?visibility=" + visibility;
        ResponseEntity<CandidateStatusCountDto> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<CandidateStatusCountDto>() {
                });
        return response.getBody();
    }

    @Override
    public CandidateStatusCountDto getCandidateStatusCountByUser(String userId, String companyId, String visibility) {
        String url = ApiConstants.GET_CANDIDATE_STATUS_COUNT_USER + userId + '/' + companyId + "?visibility=" + visibility;
        ResponseEntity<CandidateStatusCountDto> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<CandidateStatusCountDto>() {
                });
        return response.getBody();
    }

    @Override
    public String getDashboardCountByJobStateForCompany(String companyId) {
        String url = ApiConstants.GET_DASHBOARD_REQ_COUNT_COMPANY + companyId;
        return getResponse(url, HttpMethod.GET, null);
    }

    @Override
    public String getDashboardCountByJobStateForUser(String userId) {
        String url = ApiConstants.GET_DASHBOARD_REQ_COUNT_USER + userId;
        return getResponse(url, HttpMethod.GET, null);
    }

    public List<JobMinDetailsDto> getJobsByActivitiesForCompany(String companyId, String userId) {
        String url = ApiConstants.JOBS_BY_ACTIVITY_COMPANY + companyId + '/' + userId;
        ResponseEntity<List<JobMinDetailsDto>> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<JobMinDetailsDto>>() {
                });
        return response.getBody();
    }

    public List<JobMinDetailsDto> getJobsByActivitiesForUser(String companyId, String userId) {
        String url = ApiConstants.JOBS_BY_ACTIVITY_USER + companyId + '/' + userId;
        ResponseEntity<List<JobMinDetailsDto>> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<JobMinDetailsDto>>() {
                });
        return response.getBody();
    }

    @Override
    public String updateFourDotFiveIntelligenceScore(JobScoreDTO jobScoreDTO) {
        String url = ApiConstants.UPDATE_INTELLIGENCE_SCORE;
        return getResponse(url, HttpMethod.PUT, new HttpEntity<>(jobScoreDTO));
    }

    @Override
    public String updateLocation(JobLocationUpdateDTO jobLocationUpdateDTO, String userId) {
        String url = ApiConstants.UPDATE_LOCATION + "/" + userId;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(jobLocationUpdateDTO));
    }

    @Override
    public String deleteJob(DeleteDTO jobDeleteDTO) {
        String url = ApiConstants.DELETE_JOB;
        return getResponse(url, HttpMethod.DELETE, new HttpEntity<>(jobDeleteDTO));
    }

    @Override
    public FourDotFiveResponse<SearchInterfaceResult> getSearchInterface(SearchInterfaceRequest searchInterfaceRequest) {
        System.out.println("In api executor get search interface -" + searchInterfaceRequest);

        String url = ApiConstants.SEARCH_INTERFACE;

        System.out.println("URL generated - " + url);

        ResponseEntity<FourDotFiveResponse<SearchInterfaceResult>> response = restTemplate.exchange(url, HttpMethod.POST,
                new HttpEntity<>(searchInterfaceRequest),
                new ParameterizedTypeReference<FourDotFiveResponse<SearchInterfaceResult>>() {
                });

        System.out.println("Save and Parse Response - " + response.getBody());
        return response.getBody();

    }

    @Override
    public Job saveDaxtraParameters(String jobId, Map<String, String> daxtraParameters) {
        String url = ApiConstants.SAVE_DAXTRA_PARAMETERS + "/" + jobId;
        ResponseEntity<Job> response = restTemplate.exchange(url, HttpMethod.POST
                , new HttpEntity<>(daxtraParameters), new ParameterizedTypeReference<Job>() {
                });
        return response.getBody();
    }

    @Override
    public String getPhoneScreeners(String jobMatchId, boolean isCancelledMeetingScheduleRequired, String companyId) {
        String url = ApiConstants.GET_PHONE_SCREENERS + "/" + jobMatchId + "/" + companyId
                + "?isCancelledMeetingScheduleRequired=" + isCancelledMeetingScheduleRequired;
        return getResponse(url, HttpMethod.GET, null);
    }

    @Override
    public String getInterviewers(String jobMatchId, boolean isCancelledMeetingScheduleRequired, String companyId) {
        String url = ApiConstants.GET_INTERVIEWERS + "/" + jobMatchId + "/" + companyId
                + "?isCancelledMeetingScheduleRequired=" + isCancelledMeetingScheduleRequired;
        return getResponse(url, HttpMethod.GET, null);
    }

    @Override
    public String getMeetingScheduleDetails(String meetingScheduleId) {
        String url = ApiConstants.GET_MEETING_SCHEDULE_DETAIL + "/" + meetingScheduleId;
        return getResponse(url, HttpMethod.GET, null);
    }

    @Override
    public String getTokenPayload(String userId, String token) {
        String url = ApiConstants.GET_TOKEN_PAYLOAD + "/" + userId + "/" + token;
        return getResponse(url, HttpMethod.GET, null);
    }

    @Override
    public List<User> getUserList(String jobMatchId, String userId) {

        String url = ApiConstants.GET_USER_LIST + "/" + jobMatchId + "/" + userId;

        LOGGER.debug("Executing url: Get UserList - {}", url);
        ResponseEntity<List<User>> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<List<User>>() {
            });
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG);
        }
        LOGGER.debug("Response: Get company -  {}", response);
        return response.getBody();
    }

    @Override
    public Contact getCandidateDetails(String jobMatchId) {

        String url = ApiConstants.GET_CANDIDATE_NAME + "/" + jobMatchId;

        LOGGER.debug("Executing url: Get UserList - {}", url);
        ResponseEntity<Contact> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<Contact>() {
            });
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            if (!e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                LOGGER.error(e.getMessage());
                throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
            }
        } catch (ResourceAccessException e) {
            LOGGER.error(e.getMessage());
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, e.getMessage());
        }

        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG);
        }
        LOGGER.debug("Response: Get company -  {}", response);
        return response.getBody();
    }

    @Override
    public RecruitersScreeningDto checkRecruiterScreeningStatus(String jobMatchId, String userId, String companyId) {
        String url = ApiConstants.CHECK_RECRUITER_SCREENING_STATUS + jobMatchId + "/" + userId + "?companyId=" + companyId;
        LOGGER.debug("Executing url: Set RecruiterQuestionList - {}", url);
        ResponseEntity<RecruitersScreeningDto> response = null;
        response = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<RecruitersScreeningDto>() {
        });
        return response.getBody();
    }


    @Override
    public String rescheduleMeeting(String meetingScheduleId, MeetingScheduleUpdateDto meetingScheduleUpdateDto) {
        String url = ApiConstants.RESCHEDULE_MEETING + "/" + meetingScheduleId;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(meetingScheduleUpdateDto));
    }

    @Override
    public String removeAssignedAssessmentByCompanyId(String companyId, String assessmentId) {
        String url = ApiConstants.REMOVE_ASSIGNED_ASSESSMENT + "/" + companyId + "/" + assessmentId;
        return getResponse(url, HttpMethod.DELETE, null);
    }

    @Override
    public String getAccessTime(AccessTimeRequestDTO accessTimeRequestDTO) {
        String url = ApiConstants.GET_ACCESS_TIME;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(accessTimeRequestDTO));
    }

    @Override
    public String getAssessmentDetailsByAssessmentId(String assessmentId) {
        String url = ApiConstants.GET_ASSESSMENT_DETAILS_BY_ASSESSMENT_ID + "/" + assessmentId;
        return getResponse(url, HttpMethod.GET, null);
    }

    @Override
    public String cancelScheduledMeeting(String meetingScheduleId, CancelMeetingDto cancelMeetingDto) {
        String url = ApiConstants.CANCEL_SCHEDULED_MEETING + "/" + meetingScheduleId;
        return getResponse(url, HttpMethod.DELETE, new HttpEntity<>(cancelMeetingDto));
    }

    @Override
    public byte[] getCandidatePdfReport(Long testInvitationId, boolean sectional) {
        String url = ApiConstants.GET_CANDIDATE_PDF_REPORT + "?testinvitationid=" + testInvitationId + "&sectional=" + sectional;
        ResponseEntity<byte[]> response = restTemplate.exchange(url, HttpMethod.GET, null, byte[].class);
        return response.getBody();
    }

    @Override
    public String getRecruiters(List<String> jobMatchIds, String companyId) {
        String url = ApiConstants.GET_RECRUITERS + "/" + companyId;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(jobMatchIds));
    }

    @Override
    public String setRecruiter(AssignRecruiterDto assignRecruiterDto) {
        String url = ApiConstants.SET_RECRUITER;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(assignRecruiterDto));
    }

    @Override
    public String addCandidates(List<CandidateContactDetailDTO> candidateDetails) {
        String url = ApiConstants.ADD_CANDIDATES;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(candidateDetails));
    }

    @Override
    public String findCandidatesForRequisition(CandidateRequisitionDto candidateRequisitionDto) {
        String url = ApiConstants.FIND_CANDIDATES_FOR_REQUISITION;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(candidateRequisitionDto));
    }

    @Override
    public String getFilteredUserDetails(UserFilterDTO userFilter) {
        String url = ApiConstants.FILTER_USERS;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(userFilter));
    }

    @Override
    public String deleteUser(DeleteDTO deleteDTO) {
        String url = ApiConstants.SOFT_DELETE_USER;
        return getResponse(url, HttpMethod.DELETE, new HttpEntity<>(deleteDTO));
    }

    @Override
    public String getUserDeleteInfo(String userId) {
        String url = ApiConstants.GET_USER_DELETE_INFO + "/" + userId;
        return getResponse(url, HttpMethod.GET, new HttpEntity<>(null));
    }

    @Override
    public String resendInvite(String userId) {
        String url = ApiConstants.RESEND_INVITE + "/" + userId;
        return getResponse(url, HttpMethod.GET, new HttpEntity<>(null));
    }

    @Override
    public String getUsersOfSharedRequisition(String requisitionId) {
        String url = ApiConstants.GET_USER_DETAIL_BY_SCOPE + "/" + requisitionId;
        return getResponse(url, HttpMethod.GET, new HttpEntity<>(null));
    }

    @Override
    public String getNotifyUsersOfRequisition(NotifyUserDTO notifyUserDTO) {
        String url = ApiConstants.GET_USERS_OF_SHARED_REQUISITION;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(notifyUserDTO));
    }

    @Override
    public String getVendorRecruiters(VendorRecruiterRequestDto request) {
        String url = ApiConstants.GET_RECRUITERS_OF_VENDOR;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(request));
    }

    @Override
    public String getCompanyDeleteInfo(String companyId) {
        String url = ApiConstants.GET_COMPANY_DELETE_INFO + "/" + companyId;
        return getResponse(url, HttpMethod.GET, new HttpEntity<>(null));
    }

    @Override
    public String deleteCompany(DeleteDTO deleteDTO) {
        String url = ApiConstants.SOFT_DELETE_COMPANY;
        return getResponse(url, HttpMethod.DELETE, new HttpEntity<>(deleteDTO));
    }

    @Override
    public Company changeCompanyState(ChangeCompanyStateDto companyStateDto) {
        String url = ApiConstants.CHANGE_COMPANY_STATE;

        ResponseEntity<Company> response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(companyStateDto)
                , new ParameterizedTypeReference<Company>() {
                });
        return response.getBody();

    }

    @Override
    public List<StaffingCompanyDto> getAllStaffingCompanies(String name, int page, int size, String sort, String sortDir) {
        String url = ApiConstants.GET_ALL_STAFFING_COMPANIES + "?" + "searchString=" + name + "&" + "page=" + page +
                "&" + "size=" + size + "&" + "sort=" + sort + "&" +
                "sortDir=" + sortDir;

        LOGGER.debug("Executing url: Get all staffing company - {}", url);
        ResponseEntity<List<StaffingCompanyDto>> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<StaffingCompanyDto>>() {
                });
        return response.getBody();
    }

    @Override
    public String getPlanDetails(String companyId) {
        String url = ApiConstants.GET_PLAN_DETAILS + "/" + companyId;
        return getResponse(url, HttpMethod.GET, new HttpEntity<>(null));
    }

    @Override
    public PlanFeaturesCapability getPlanDetailsByCompanyId(String companyId) {
        String url = ApiConstants.GET_PLAN_DETAILS_BY_COMPANY_ID + "/" + companyId;
        ResponseEntity<PlanFeaturesCapability> response = restTemplate.exchange(url,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<PlanFeaturesCapability>() {
                });
        return response.getBody();
    }

    @Override
    public Boolean isPlanActive(String companyId) {
        String url = ApiConstants.IS_PLAN_ACTIVE + "/" + companyId;
        ResponseEntity<Boolean> response = restTemplate.exchange(url,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<Boolean>() {
                });
        return response.getBody();
    }


    @Override
    public PlanFeaturesCapability getFeaturesPlanDetails(String companyId) {
        String url = ApiConstants.GET_PLAN_DETAILS + "/" + companyId;
        ResponseEntity<PlanFeaturesCapability> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<PlanFeaturesCapability>() {
                });
        return response.getBody();
    }

    @Override
    public String createJobMatches(CandidateRequisitionInfoDto candidateRequisitionInfoDto) {
        String url = ApiConstants.CREATE_JOB_MATCHES;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(candidateRequisitionInfoDto));
    }

    @Override
    public String resendAssessment(SendAssessmentDto sendAssessmentDto) {
        String url = ApiConstants.RESEND_ASSESSMENT;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(sendAssessmentDto));
    }

    @Override
    public String getCandidateAssessmentDetail(CandidateAssessmentDetailRequestDto candidateAssessmentDetailRequestDto) {
        String url = ApiConstants.GET_CANDIDATE_ASSESSMENT_DETAIL;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(candidateAssessmentDetailRequestDto));
    }

    @Override
    public String changeNotificationsState(NotificationStateChangeDTO notificationStateChange) {
        String url = ApiConstants.CHANGE_NOTIFICATION_STATE;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(notificationStateChange));
    }

    @Override
    public String findAll(NotificationFilterParamsDto notificationFilterParamsDto) {
        String url = ApiConstants.GET_ALL_NOTIFICATIONS;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(notificationFilterParamsDto));
    }

    @Override
    public String changeStateForAllNotificationsOfLoggedInUser(String userId, String state) {
        String url = ApiConstants.CHANGE_NOTIFICATION_STATE_USER + "/" + userId + "?" + "state=" + state;
        return getResponse(url, HttpMethod.GET, new HttpEntity<>(null));
    }

    @Override
    public String setAssessmentLeft(AssessmentLeftDto assessmentLeftDto) {
        String url = ApiConstants.SET_ASSESSMENT_LEFT;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(assessmentLeftDto));
    }


    @Override
    public String getAssessmentSentDetails(String jobMatchId, CandidateAssessmentTestType testType) {
        String url = ApiConstants.GET_ASSESSMENT_SENT_DETAILS + "/" + jobMatchId + "?" + "assessmentType=" + testType;
        return getResponse(url, HttpMethod.GET, new HttpEntity<>(null));
    }

    @Override
    public String getAvailableTimeZones() {
        String url = ApiConstants.GET_ALL_TIME_ZONES;
        return getResponse(url, HttpMethod.GET, new HttpEntity<>(null));
    }

    @Override
    public List<CandidateActivityDto> getCandidateActivity(FilterCandidateActivityDto filterCandidateActivityDto) {
        String url = ApiConstants.GET_CANDIDATE_ACTIVITY;
        ResponseEntity<List<CandidateActivityDto>> response = restTemplate.exchange(url, HttpMethod.POST,
                new HttpEntity<>(filterCandidateActivityDto), new ParameterizedTypeReference<List<CandidateActivityDto>>() {
                });

        return response.getBody();
    }

    @Override
    public String getCandidateClientOrBUs(String candidateId) {
        String url = ApiConstants.GET_ALL_CLIET_OR_BUS_OF_CANDIDATE + "/" + candidateId;
        return getResponse(url, HttpMethod.GET, new HttpEntity<>(null));
    }

    @Override
    public String getCandidateJobAndRole(String candidateId) {
        String url = ApiConstants.GET_JOBS_AND_ROLE_OF_CANDIDATE + "/" + candidateId;
        return getResponse(url, HttpMethod.GET, new HttpEntity<>(null));
    }

    @Override
    public String getAllCandidateWorkflowCardStatus() {
        String url = ApiConstants.GET_ALL_CANDIDATE_WORKFLOW_CARD_STATUS;
        return getResponse(url, HttpMethod.GET, new HttpEntity<>(null));
    }

    @Override
    public String updateJobMatchState(String jobId, String companyId) {
        String url = ApiConstants.UPDATE_JOB_MATCH_STATE + "/" + jobId + "/" + companyId;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(null));
    }

    @Override
    public String getAllCanceledMeetings(String jobMatchId) {
        String url = ApiConstants.GET_ALL_CANCELED_MEETINGS + "/" + jobMatchId;
        return getResponse(url, HttpMethod.GET, new HttpEntity<>(null));
    }

    @Override
    public FileDto getInterviewFeedbackReport(String jobMatchId) {
        String url = ApiConstants.INTERVIEW_FEEDBACK_REPORT + "/" + jobMatchId;
        ResponseEntity<FileDto> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<FileDto>() {
                });
        return response.getBody();
    }

    @Override
    public FileDto getPhoneScreenFeedbackReport(String jobMatchId) {
        String url = ApiConstants.PHONE_SCREEN_FEEDBACK_REPORT + "/" + jobMatchId;
        ResponseEntity<FileDto> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<FileDto>() {
                });
        return response.getBody();
    }

    @Override
    public FileDto getRecruiterScreeningReport(String jobMatchId) {
        String url = ApiConstants.SCREENING_FEEDBACK_REPORT + "/" + jobMatchId;
        ResponseEntity<FileDto> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<FileDto>() {
                });
        return response.getBody();
    }

    @Override
    public Map<String, Map<String, Boolean>> getReportAvailabilityMap(String jobMatchId, String userId) {
        String url = ApiConstants.FEEDBACK_REPORT_AVAILABILITY + "/" + jobMatchId + "/" + userId;
        ResponseEntity<Map<String, Map<String, Boolean>>> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<Map<String, Map<String, Boolean>>>() {
                });
        return response.getBody();
    }

    @Override
    public String getReasonCodes(List<String> actions) {
        String url = ApiConstants.GET_REASON_CODES;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(actions));
    }

    @Override
    public String saveReasonCodes(ReasonNoteDTO reasonNoteDTO) {
        String url = ApiConstants.SAVE_REASON_CODES;
        return getResponse(url, HttpMethod.POST, new HttpEntity(reasonNoteDTO));
    }

    @Override
    public String getEmailDeliveryHistory(String jobMatchId) {
        String url = ApiConstants.GET_EMAIL_DELIVERY_HISTORY + "/" + jobMatchId;
        return getResponse(url, HttpMethod.GET, null);
    }

    @Override
    public String setCandidateAvailability(CandidateAvailabilityDTO candidateAvailabilityDTO) {
        String url = ApiConstants.SET_CANDIDATE_AVAILABILITY;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(candidateAvailabilityDTO));
    }

    @Override
    public String getDaxtraAddedCandidatesWithLessScore(String jobId, String daxtraRequestId) {
        String url = ApiConstants.GET_DAXTRA_ADDED_CANDIDATES_WITH_LESS_SCORE + "/" + jobId + "/" + daxtraRequestId;
        return getResponse(url, HttpMethod.GET, new HttpEntity<>(null));
    }

    @Override
    public String deleteCandidate(DeleteDTO deleteDTO) {
        String url = ApiConstants.DELETE_CANDIDATE;
        return getResponse(url, HttpMethod.DELETE, new HttpEntity<>(deleteDTO));
    }

    @Override
    public String deleteCandidateOnResumeRemove(String resumeId, String loggedInUserId) {
        String url = ApiConstants.DELETE_CANDIDATE_ON_RESUME_REMOVE + "/" + resumeId + "/" + loggedInUserId;
        return getResponse(url, HttpMethod.DELETE, new HttpEntity<>(null));
    }

    @Override
    public String addOrUpdateContactEmail(ContactDetailsDTO contactDetailsDTO) {
        String url = ApiConstants.ADD_OR_UPDATE_CANDIDATE_EMAIL;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(contactDetailsDTO));
    }

    @Override
    public String addOrUpdateContactPhoneNumber(ContactDetailsDTO contactDetailsDTO) {
        String url = ApiConstants.ADD_OR_UPDATE_CANDIDATE_PHONE;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(contactDetailsDTO));
    }

    @Override
    public String addOrUpdateContactAddress(ContactDetailsDTO contactDetailsDTO) {
        String url = ApiConstants.ADD_OR_UPDATE_CANDIDATE_ADDRESS;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(contactDetailsDTO));
    }

    public CandidateAssessmentDto getCandidateAssessmentDetails(Long testInvitationId) {
        String url = ApiConstants.GET_CANDIDATE_AND_ASSESSMENT_DETAIL + testInvitationId;
        ResponseEntity<CandidateAssessmentDto> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<CandidateAssessmentDto>() {
                });
        return response.getBody();
    }

    @Override
    public CandidatesEnabledStepDto getCandidatesPresentOnStep(String companyId, String userId, String jobId) {
        String url = ApiConstants.GET_CANDIDATE_PRESENT_ON_STEP + "/" + companyId + "/" + userId + "/" + jobId;
        ResponseEntity<CandidatesEnabledStepDto> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<CandidatesEnabledStepDto>() {
                });
        return response.getBody();
    }

    @Override
    public String getAllPendingMeetingsAndAssessments(List<String> jobMatchIds) {
        String url = ApiConstants.PENDING_CANDIDATE_JOB_CARD_ACTIVITES;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(jobMatchIds));
    }

    @Override
    public String updatePendingMeetingsAndAssessments(List<String> jobMatchIds, String loggedInUserId) {
        String url = ApiConstants.UPDATE_PENDING_CANDIDATE_JOB_CARD_ACTIVITES + "/" + loggedInUserId;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(jobMatchIds));
    }

    @Override
    public String sendAssessments(SendAssessmentListDto sendAssessmentDto) {
        String url = ApiConstants.SEND_ASSESSMENTS;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(sendAssessmentDto));
    }

    @Override
    public String cancelAssessments(CancelAssessmentListDto cancelAssessmentListDto) {
        String url = ApiConstants.CANCEL_ASSESSMENTS;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(cancelAssessmentListDto));
    }

    @Override
    public CandidateAssessmentReportDetails updateCandidateReportDetails(CandidateAssessmentReportDetails candidateAssessmentReportDetails, String userId) {
        String url = ApiConstants.UPDATE_CANDIDATE_REPORT_DETAILS + "/" + userId;
        ResponseEntity<CandidateAssessmentReportDetails> response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(candidateAssessmentReportDetails),
                new ParameterizedTypeReference<CandidateAssessmentReportDetails>() {
                });
        return response.getBody();
    }

    @Override
    public String updateCandidateReportInfo(String candidateAssessmentDetailId, AssessmentReportInfoDto assessmentReportInfoDto) {
        String url = ApiConstants.UPDATE_REPORT_INFO + "/" + candidateAssessmentDetailId;
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(assessmentReportInfoDto),
                new ParameterizedTypeReference<String>() {
                });

        if (response == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, Messages.EXCEPTION_WHILE_REGENERATING_REPORT);
        }

        return response.getBody();
    }

    @Override
    public String cancelAssessmentsOnAssessmentUpdate(List<String> candidateAssessmentIds, String currentCompanyId, String userId) {
        String url = ApiConstants.CANCEL_ASSESSMENTS_ON_ASSESSMENT_UPDATE + "/" + userId + "/" + currentCompanyId;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(candidateAssessmentIds));
    }

    @Override
    public String validateMeetingStartDateAndEndDate(String meetingScheduleId) {
        String url = ApiConstants.VALIDATE_MEETING_START_DATE_AND_END_DATE + "/" + meetingScheduleId;
        return getResponse(url, HttpMethod.GET, null);
    }

    @Override
    public CandidateDetailsResponseDTO getCandidateDetailsById(String candidateId) {
        String url = ApiConstants.GET_CANDIDATE + "/" + candidateId;
        ResponseEntity<CandidateDetailsResponseDTO> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<CandidateDetailsResponseDTO>() {
                });
        return response.getBody();
    }

    @Override
    public CandidateAssessmentReportDetails deleteCommentFromCandidateReportDetails(String candidateAssessmentDetailId, String commentId, String userId) {
        String url = ApiConstants.DELETE_COMMENT_CANDIDATE_REPORT_DETAILS + "/" + candidateAssessmentDetailId + "?userId=" + userId;
        if (StringUtils.isNotEmpty(commentId))
            url += "&commentId=" + commentId;
        ResponseEntity<CandidateAssessmentReportDetails> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<CandidateAssessmentReportDetails>() {
                });
        return response.getBody();
    }

    @Override
    public CandidatesEnabledStepDto getCandidatesPresentOnStepByClientOrBuId(String companyId, String userId, List<NewJobDto> newJobDtos) {
        String url = ApiConstants.GET_CANDIDATE_PRESENT_ON_STEP_BY_CLIENT_OR_BU_ID + "/" + companyId + "/" + userId;
        ResponseEntity<CandidatesEnabledStepDto> response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<Object>(newJobDtos),
                new ParameterizedTypeReference<CandidatesEnabledStepDto>() {
                });
        return response.getBody();
    }

    @Override
    public CandidatesEnabledStepDto getCandidatesPresentOnStepByCompanyId(String companyId, String userId, List<String> allClientsOrBu) {
        String url = ApiConstants.GET_CANDIDATE_PRESENT_ON_STEP_BY_COMPANY_ID + "/" + companyId + "/" + userId;
        ResponseEntity<CandidatesEnabledStepDto> response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<Object>(allClientsOrBu),
                new ParameterizedTypeReference<CandidatesEnabledStepDto>() {
                });
        return response.getBody();
    }

    @Override
    public String addOrUpdateCandidateAdditionalDetails(UpdateCandidateAdditionalDetailsDto candidateAdditionalDetailsDto) {
        String url = ApiConstants.UPDATE_REPORT_INFO;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(candidateAdditionalDetailsDto));
    }

    @Override
    public String allowOverride(String candidateId) {
        String url = ApiConstants.ALLOW_OVERRIDE + "/ " + candidateId;
        return getResponse(url, HttpMethod.GET, null);
    }

    @Override
    public NewJobDto getRequisitionCard(String jobId, String companyId, String userId) {
        String url = ApiConstants.GET_REQUISITION_CARD + "/" + jobId + "/" + companyId + "/" + userId;
        ResponseEntity<NewJobDto> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<NewJobDto>() {
                });
        return response.getBody();
    }

    @Override
    public JobBoardParameters getJobBoardParameterList() {
        String url = ApiConstants.JOB_BOARD_PARAMETERS;
        ResponseEntity<JobBoardParameters> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<JobBoardParameters>() {
                });
        return response.getBody();
    }

    @Override
    public Map<String, List<String>> getRadiusAndRecencyOptionsList() {
        String url = ApiConstants.RADIUS_AND_RECENCY_OPTIONS_LIST;
        ResponseEntity<Map<String, List<String>>> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<Map<String, List<String>>>() {
                });
        return response.getBody();
    }

    @Override
    public JobResponseDto getJobById(String jobId) {
        String url = ApiConstants.GET_JOB_BY_ID + "/" + jobId;
        ResponseEntity<JobResponseDto> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<JobResponseDto>() {
                });
        return response.getBody();
    }

    @Override
    public String getRequisitionById(String requisitionId) {
        String url = ApiConstants.GET_REQUISITION_BY_ID + "/" + requisitionId;
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<String>() {
                });
        return response.getBody();
    }

    @Override
    public byte[] getJobRequisitionFile(String requisitionId) {
        String url = ApiConstants.GET_JOB_REQUISITION_FILE + "/" + requisitionId;
        ResponseEntity<byte[]> response = restTemplate.exchange(url,
                HttpMethod.GET, null,
                new ParameterizedTypeReference<byte[]>() {
                });
        return response.getBody();
    }

    @Override
    public List<VendorMinDto> getVendorList(String companyId, String buId) {
        String url = ApiConstants.GET_VENDOR_LIST + "/" + companyId + "?buId=" + buId;
        ResponseEntity<List<VendorMinDto>> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<VendorMinDto>>() {
                });
        return response.getBody();
    }

    @Override
    public List<VendorMinDto> getAssignedVendors(String jobId) {
        String url = ApiConstants.GET_ASSIGNED_VENDORS + "/" + jobId;
        ResponseEntity<List<VendorMinDto>> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<VendorMinDto>>() {
                });
        return response.getBody();
    }

    @Override
    public List<JobVendorDetailDTO> getVendorsOfJob(String requisitionId) {
        String url = ApiConstants.GET_VENDORS_OF_JOB + "/" + requisitionId;
        ResponseEntity<List<JobVendorDetailDTO>> response = restTemplate.exchange(
                url, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<JobVendorDetailDTO>>() {
                });
        return response.getBody();
    }

    @Override
    public CandidateCount getCandidateCountByRequisitionId(String jobId, String jbiTransactionId) {
        String url = ApiConstants.CANDIDATE_COUNT + "/" + jobId;
        if (StringUtils.isNotEmpty(jbiTransactionId))
            url += "?jbiTransactionId=" + jbiTransactionId;
        ResponseEntity<CandidateCount> response = restTemplate.exchange(url, HttpMethod.GET, null
                , new ParameterizedTypeReference<CandidateCount>() {
                });
        return response.getBody();
    }

    @Override
    public Boolean isGetMoreResumesInProgress(String jobId) {
        String url = ApiConstants.GET_MORE_RESUMES_STATUS + "/" + jobId;
        ResponseEntity<Boolean> response = restTemplate.exchange(url, HttpMethod.GET, null
                , new ParameterizedTypeReference<Boolean>() {
                });
        return response.getBody();
    }

    @Override
    public List<CandidateRunScoreResponseDto> getCandidateJobMatchInfoByJbiTransactionId(String jbiTransactionId) {
        String url = ApiConstants.CANDIDATE_JOBMATCH_INFO + "/" + jbiTransactionId;

        ResponseEntity<List<CandidateRunScoreResponseDto>> response = restTemplate.exchange(url, HttpMethod.GET, null
                , new ParameterizedTypeReference<List<CandidateRunScoreResponseDto>>() {
                });

        System.out.println("Save and Parse Response - " + response.getBody());
        return response.getBody();

    }

    @Override
    public List<JobBoardSearchHistory> getJobBoardSearchHistory(String jobId, int page, int size, String sort, String sortDir) {
        String url = ApiConstants.JOB_BOARD_SEARCH_HISTORY + "/" + jobId + "?" + "page=" + page +
                "&" + "size=" + size + "&" + "sort=" + sort + "&" + "sortDir=" + sortDir;

        ResponseEntity<List<JobBoardSearchHistory>> response = restTemplate.exchange(url, HttpMethod.GET, null
                , new ParameterizedTypeReference<List<JobBoardSearchHistory>>() {
                });
        return response.getBody();
    }

    @Override
    public JobBoardCredentialDto addJobBoardCredentials(JobBoardCredentialModel jobBoardCredentialModel) {
        String url = ApiConstants.ADD_JOB_BOARD_CREDENTIAL;

        ResponseEntity<JobBoardCredentialDto> response = restTemplate.exchange(url, HttpMethod.POST,
                new HttpEntity<>(jobBoardCredentialModel), new ParameterizedTypeReference<JobBoardCredentialDto>() {
                });
        return response.getBody();
    }

    @Override
    public DeleteAccountResponse deleteJobBoardCredentials(String accountToken, String companyId) {
        String url = ApiConstants.DELETE_JOB_BOARD_CREDENTIAL + "/" + companyId + "?" + "accountToken=" + accountToken;

        ResponseEntity<DeleteAccountResponse> response = restTemplate.exchange(url, HttpMethod.GET,
                null, new ParameterizedTypeReference<DeleteAccountResponse>() {
                });
        return response.getBody();
    }

    @Override
    public Job removeDaxtraParameters(String jobId) {
        String url = ApiConstants.REMOVE_DAXTRA_PARAMETERS + "/" + jobId;
        ResponseEntity<Job> response = restTemplate.exchange(url, HttpMethod.GET
                , null, new ParameterizedTypeReference<Job>() {
                });
        return response.getBody();

    }

    @Override
    public Boolean jobBoardSearchAvailabilityCheck() {
        String url = ApiConstants.JOB_BOARD_SEARCH_AVAILABILITY_CHECK;
        ResponseEntity<Boolean> response = restTemplate.exchange(url, HttpMethod.GET
                , null, new ParameterizedTypeReference<Boolean>() {
                });
        return response.getBody();
    }

    @Override
    public String isSharedRequisition(String requisitionId) {
        String url = ApiConstants.CHECK_SHARED_REQUISITION + "/" + requisitionId;
        return getResponse(url, HttpMethod.GET, null);
    }

    @Override
    public List<PlanFeaturesCapability> getAllPlanFeatures() {
        String url = ApiConstants.GET_ALL_PLAN_FEATURES;

        ResponseEntity<List<PlanFeaturesCapability>> response = restTemplate.exchange(url, HttpMethod.GET, null
                , new ParameterizedTypeReference<List<PlanFeaturesCapability>>() {
                });

        return response.getBody();
    }

    @Override
    public PlanFeaturesCapability findPlanFeaturesCapabilityById(String id) {
        String url = ApiConstants.GET_PLAN_FEATURE_BY_ID + "/" + id;

        ResponseEntity<PlanFeaturesCapability> response = restTemplate.exchange(url, HttpMethod.GET, null
                , new ParameterizedTypeReference<PlanFeaturesCapability>() {
                });

        return response.getBody();
    }

    @Override
    public PlanFeaturesCapability findPlanFeaturesCapabilityByPlan(String plan) {
        String url = ApiConstants.GET_PLAN_FEATURE_BY_PLAN + "?plan=" + plan;

        ResponseEntity<PlanFeaturesCapability> response = restTemplate.exchange(url, HttpMethod.GET, null
                , new ParameterizedTypeReference<PlanFeaturesCapability>() {
                });

        return response.getBody();
    }

    @Override
    public PlanHeaderDto findPlanHeaderById(String planHeaderId) {
        String url = ApiConstants.GET_PLAN_HEADER_BY_ID + "/" + planHeaderId;

        ResponseEntity<PlanHeaderDto> response = restTemplate.exchange(url, HttpMethod.GET, null
                , new ParameterizedTypeReference<PlanHeaderDto>() {
                });

        return response.getBody();
    }

    @Override
    public PlanHeaderDto findPlanHeaderByCompanyId(String companyId) {
        String url = ApiConstants.GET_PLAN_HEADER_BY_COMPANY_ID + "/" + companyId;

        ResponseEntity<PlanHeaderDto> response = restTemplate.exchange(url, HttpMethod.GET, null
                , new ParameterizedTypeReference<PlanHeaderDto>() {
                });

        return response.getBody();
    }

    @Override
    public PlanDetailDto findPlanDetailById(String planDetailId) {
        String url = ApiConstants.GET_PLAN_DETAIL_BY_ID + "/" + planDetailId;

        ResponseEntity<PlanDetailDto> response = restTemplate.exchange(url, HttpMethod.GET, null
                , new ParameterizedTypeReference<PlanDetailDto>() {
                });

        return response.getBody();
    }

    @Override
    public PlanDetailDto findPlanDetailByPlanHeaderId(String planHeaderId) {
        String url = ApiConstants.GET_PLAN_DETAIL_BY_PLAN_HEADER_ID + "/" + planHeaderId;

        ResponseEntity<PlanDetailDto> response = restTemplate.exchange(url, HttpMethod.GET, null
                , new ParameterizedTypeReference<PlanDetailDto>() {
                });

        return response.getBody();
    }

    @Override
    public PlanDto savePlan(PlanDto planDto) {
        String url = ApiConstants.SAVE_PLAN;

        ResponseEntity<PlanDto> response = restTemplate.exchange(url, HttpMethod.POST,
                new HttpEntity<>(planDto), new ParameterizedTypeReference<PlanDto>() {
                });

        return response.getBody();
    }

    @Override
    public PlanDto findPlanByPlanHeaderId(String planHeaderId) {
        String url = ApiConstants.GET_PLAN_BY_PLAN_HEADER_ID + "/" + planHeaderId;

        ResponseEntity<PlanDto> response = restTemplate.exchange(url, HttpMethod.GET, null
                , new ParameterizedTypeReference<PlanDto>() {
                });

        return response.getBody();
    }

    @Override
    public PlanDto findPlanByCompanyId(String companyId) {
        String url = ApiConstants.GET_PLAN_BY_COMPANY_ID + "/" + companyId;

        ResponseEntity<PlanDto> response = restTemplate.exchange(url, HttpMethod.GET, null
                , new ParameterizedTypeReference<PlanDto>() {
                });

        return response.getBody();
    }

    @Override
    public boolean isAddUserFeatureAvailable(String userId) {
        String url = ApiConstants.CHECK_FEATURE_IS_ENABLED_OR_NOT + "/" + userId + "?feature=" + AppConstants.FEATURE_USER;
        return isFeatureEnabled(url);
    }

    @Override
    public boolean isAddCandidateFeatureAvailable(String userId) {
        String url = ApiConstants.CHECK_FEATURE_IS_ENABLED_OR_NOT + "/" + userId + "?feature=" + AppConstants.FEATURE_CANDIDATES;
        return isFeatureEnabled(url);
    }

    @Override
    public boolean isAddRequisitionFeatureAvailable(String userId) {
        String url = ApiConstants.CHECK_FEATURE_IS_ENABLED_OR_NOT + "/" + userId + "?feature=" + AppConstants.FEATURE_REQUISITIONS;
        return isFeatureEnabled(url);
    }

    @Override
    public boolean isMatchingFeatureAvailable(String userId) {
        String url = ApiConstants.CHECK_FEATURE_IS_ENABLED_OR_NOT + "/" + userId + "?feature=" + AppConstants.FEATURE_MATCHING;
        return isFeatureEnabled(url);
    }

    @Override
    public boolean isTechAssessmentFeatureAvailable(String userId) {
        String url = ApiConstants.CHECK_FEATURE_IS_ENABLED_OR_NOT + "/" + userId + "?feature=" + AppConstants.FEATURE_TECH_ASSESSMENT;
        return isFeatureEnabled(url);
    }

    @Override
    public boolean isValueAssessmentFeatureAvailable(String userId) {
        String url = ApiConstants.CHECK_FEATURE_IS_ENABLED_OR_NOT + "/" + userId + "?feature=" + AppConstants.FEATURE_VALUE_ASSESSMENT;
        return isFeatureEnabled(url);
    }

    @Override
    public boolean isCustomizableRecruiterScreeningFeatureAvailable(String userId) {
        String url = ApiConstants.CHECK_FEATURE_IS_ENABLED_OR_NOT + "/" + userId + "?feature=" + AppConstants.FEATURE_CUSTOMIZABLE_RECRUITER_SCREENING;
        return isFeatureEnabled(url);
    }

    @Override
    public boolean isCustomizablePhoneFeedbackFeatureAvailable(String userId) {
        String url = ApiConstants.CHECK_FEATURE_IS_ENABLED_OR_NOT + "/" + userId + "?feature=" + AppConstants.FEATURE_CUSTOMIZABLE_PHONE_FEEDBACK;
        return isFeatureEnabled(url);
    }

    @Override
    public boolean isCustomizableInterviewFeedbackFeatureAvailable(String userId) {
        String url = ApiConstants.CHECK_FEATURE_IS_ENABLED_OR_NOT + "/" + userId + "?feature=" + AppConstants.FEATURE_CUSTOMIZABLE_INTERVIEW_FEEDBACK;
        return isFeatureEnabled(url);
    }

    @Override
    public boolean isJobBoardIntegrationFeatureAvailable(String userId) {
        String url = ApiConstants.CHECK_FEATURE_IS_ENABLED_OR_NOT + "/" + userId + "?feature=" + AppConstants.FEATURE_JOB_BOARD_INTEGRATION;
        return isFeatureEnabled(url);
    }

    private boolean isFeatureEnabled(String url) {
        ResponseEntity<Boolean> response = null;
        boolean isEnabled = false;
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<Boolean>() {
            });
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        if (response != null) {
            isEnabled = response.getBody();
        }
        return isEnabled;
    }

    @Override
    public PlanDto findCurrentPlanByCompanyId(String companyId) {
        String url = ApiConstants.GET_CURRENT_PLAN_BY_COMPANY_ID + "/" + companyId;
        ResponseEntity<PlanDto> response = restTemplate.exchange(url, HttpMethod.GET, null
                , new ParameterizedTypeReference<PlanDto>() {
                });

        return response.getBody();

    }

    @Override
    public PlanDto findFuturePlanByCompanyId(String companyId) {
        String url = ApiConstants.GET_FUTURE_PLAN_BY_COMPANY_ID + "/" + companyId;
        ResponseEntity<PlanDto> response = restTemplate.exchange(url, HttpMethod.GET, null
                , new ParameterizedTypeReference<PlanDto>() {
                });

        return response.getBody();
    }

    @Override
    public PlanHeaderDto findCurrentPlanHeaderByCompanyId(String companyId) {
        String url = ApiConstants.GET_CURRENT_PLAN_HEADER_BY_COMPANY_ID + "/" + companyId;
        ResponseEntity<PlanHeaderDto> response = restTemplate.exchange(url, HttpMethod.GET, null
                , new ParameterizedTypeReference<PlanHeaderDto>() {
                });

        return response.getBody();

    }

    @Override
    public PlanHeaderDto findFuturePlanHeaderByCompanyId(String companyId) {
        String url = ApiConstants.GET_FUTURE_PLAN_HEADER_BY_COMPANY_ID + "/" + companyId;
        ResponseEntity<PlanHeaderDto> response = restTemplate.exchange(url, HttpMethod.GET, null
                , new ParameterizedTypeReference<PlanHeaderDto>() {
                });

        return response.getBody();
    }

    @Override
    public Map<String, Integer> getAvailableTechAssessmentsCount(String companyId, String jobId) {
        String url = ApiConstants.TECH_ASSESSMENTS_COUNT + "/" + companyId + "?jobId=" + jobId;

        ResponseEntity<Map<String, Integer>> response = restTemplate.exchange(url, HttpMethod.GET, null
                , new ParameterizedTypeReference<Map<String, Integer>>() {
                });

        return response.getBody();
    }

    @Override
    public Map<String, Integer> getAvailableJobBoardResumesCount(String companyId) {
        String url = ApiConstants.JOB_BOARD_RESUMES_COUNT + "/" + companyId;

        ResponseEntity<Map<String, Integer>> response = restTemplate.exchange(url, HttpMethod.GET, null
                , new ParameterizedTypeReference<Map<String, Integer>>() {
                });

        return response.getBody();

    }

    @Override
    public List<PlanFeaturesCapability> getAllPlanFeatures(String companyType, String companyId) {
        String url = ApiConstants.GET_ALL_PLAN_FEATURES + "/" + companyType + "?companyId=" + companyId;

        ResponseEntity<List<PlanFeaturesCapability>> response = restTemplate.exchange(url, HttpMethod.GET, null
                , new ParameterizedTypeReference<List<PlanFeaturesCapability>>() {
                });

        return response.getBody();
    }

    @Override
    public PlanDto findExpiredPlanByCompanyId(String companyId) {
        String url = ApiConstants.GET_EXPIRED_PLAN_BY_COMPANY_ID + "/" + companyId;
        ResponseEntity<PlanDto> response = restTemplate.exchange(url, HttpMethod.GET, null
                , new ParameterizedTypeReference<PlanDto>() {
                });

        return response.getBody();
    }

    @Override
    public List<String> getJobNamesByCompany(String searchText, String companyId) {
        String url = ApiConstants.SEARCH_ROLES_FOR_COMPANY + "/" + searchText + "/" + companyId;
        ResponseEntity<List<String>> jobsNames = null;
        try {
            jobsNames = restTemplate.exchange(url, HttpMethod.POST,
                    null,
                    new ParameterizedTypeReference<List<String>>() {
                    });
        } catch (HttpClientErrorException e) {
            LOGGER.error("Error occured while fetching roles " + e.getMessage(), e);
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
            }
        }
        if (jobsNames == null) {
            throw new PlatformException(Messages.INTERNAL_ERROR_MSG, Messages.NULL_RESPONSE);
        }
        return jobsNames.getBody();
    }

    @Override
    public List<String> getJobNamesByUser(String searchText, String userId, String companyId) {
        String url = ApiConstants.SEARCH_ROLES_BY_USER + "/" + searchText + "/" + userId + "/" + companyId;
        ResponseEntity<List<String>> jobsNames = null;
        try {
            jobsNames = restTemplate.exchange(url, HttpMethod.POST,
                    null,
                    new ParameterizedTypeReference<List<String>>() {
                    });
        } catch (HttpClientErrorException e) {
            LOGGER.error("Error occured while fetching roles " + e.getMessage(), e);
        }
        return jobsNames.getBody();
    }

    @Override
    public CorporateVendorDto saveCorporateVendorRelationship(CorporateVendorDto corporateVendorDto, String loggedInUserId) {
        String url = ApiConstants.SAVE_CORPORATE_VENDOR_RELN + "?loggedInUserId=" + loggedInUserId;
        ResponseEntity<CorporateVendorDto> response = restTemplate.exchange(url, HttpMethod.POST,
                new HttpEntity<>(corporateVendorDto), new ParameterizedTypeReference<CorporateVendorDto>() {
                });

        return response.getBody();

    }

    @Override
    public List<CorporateVendorDto> saveCorporateVendors(AssociateVendorsDto associateVendorsDto, String loggedInUserId) {

        String url = ApiConstants.SAVE_CORPORATE_VENDORS + "?loggedInUserId=" + loggedInUserId;
        ResponseEntity<List<CorporateVendorDto>> response = restTemplate.exchange(url, HttpMethod.POST,
                new HttpEntity<>(associateVendorsDto), new ParameterizedTypeReference<List<CorporateVendorDto>>() {
                });

        return response.getBody();

    }

    @Override
    public List<CorporateVendorDto> findAllVendorsOfCorporateCompany(String companyId) {
        String url = ApiConstants.FIND_ALL_CORPORATE_VENDORS + "/" + companyId;

        ResponseEntity<List<CorporateVendorDto>> response = restTemplate.exchange(url, HttpMethod.GET, null
                , new ParameterizedTypeReference<List<CorporateVendorDto>>() {
                });

        return response.getBody();
    }

    @Override
    public BuVendorDto getAllBuVendors(String corporateCompanyId, String buId) {
        String url = ApiConstants.FIND_ALL_BU_VENDORS + "/" + corporateCompanyId + "/" + buId;

        ResponseEntity<BuVendorDto> response = restTemplate.exchange(url, HttpMethod.GET, null
                , new ParameterizedTypeReference<BuVendorDto>() {
                });

        return response.getBody();

    }

    @Override
    public BuVendorDto saveBuVendors(BuVendorDto buVendorDto, String loggedInUserId) {
        String url = ApiConstants.SAVE_BU_VENDORS + "?loggedInUserId=" + loggedInUserId;
        ResponseEntity<BuVendorDto> response = restTemplate.exchange(url, HttpMethod.POST,
                new HttpEntity<>(buVendorDto), new ParameterizedTypeReference<BuVendorDto>() {
                });

        return response.getBody();
    }

    @Override
    public Map<String, List<Company>> getAllBusOfClient(String companyId) {
        String url = ApiConstants.GET_ALL_BUS_OF_CLIENT + "/" + companyId;

        ResponseEntity<Map<String, List<Company>>> response = restTemplate.exchange(url, HttpMethod.GET, null
                , new ParameterizedTypeReference<Map<String, List<Company>>>() {
                });

        return response.getBody();
    }

    @Override
    public CorporateVendorMinDto updateCorporateCompanyContactListInCorporateVendor(CorporateVendorDto corporateVendorDto) {
        String url = ApiConstants.UPDATE_CORPORATE_CONTACT_LIST;
        ResponseEntity<CorporateVendorDto> response = restTemplate.exchange(url, HttpMethod.POST,
                new HttpEntity<>(corporateVendorDto), new ParameterizedTypeReference<CorporateVendorDto>() {
                });
        CorporateVendorDto savedCorporateVendorDto = response.getBody();
        CorporateVendorMinDto corporateVendorMinDto = null;
        if (savedCorporateVendorDto != null) {
            corporateVendorMinDto = new CorporateVendorMinDto(savedCorporateVendorDto);
        }
        return corporateVendorMinDto;
    }

    @Override
    public List<ClientBuMinDto> getClientBuList(String corporateCompanyId, String vendorId, String loggedInUserId) {
        String url = ApiConstants.GET_CLIENT_BU_LIST + "?vendorId=" + vendorId + "&corporateCompanyId=" + corporateCompanyId +
                "&loggedInUserId=" + loggedInUserId;
        ResponseEntity<List<ClientBuDto>> response = restTemplate.exchange(url, HttpMethod.GET,
                null, new ParameterizedTypeReference<List<ClientBuDto>>() {
                });
        List<ClientBuDto> clientBuDtoList = response.getBody();
        List<ClientBuMinDto> clientBuMinDtoList = new ArrayList<>();
        if (clientBuDtoList != null && !clientBuDtoList.isEmpty()) {
            for (ClientBuDto clientBuDto : clientBuDtoList) {
                clientBuMinDtoList.add(new ClientBuMinDto(clientBuDto));
            }
        }

        return clientBuMinDtoList;
    }

    @Override
    public List<ContactDto> getCorporateCompanyContactList(String corporateCompanyId, String vendorId) {
        String url = ApiConstants.GET_CORPORATE_CONTACT_LIST + "?vendorId=" + vendorId + "&corporateCompanyId=" + corporateCompanyId;
        ResponseEntity<CorporateVendorDto> response = restTemplate.exchange(url, HttpMethod.GET, null
                , new ParameterizedTypeReference<CorporateVendorDto>() {
                });
        return ContactDto.getContactListDto(response.getBody().getCorporateCompanyContactList());
    }

    @Override
    public CorporateVendorMinDto getCorporateVendorRelationship(String corporateCompanyId, String vendorId) {
        String url = ApiConstants.GET_CORPORATE_VENDOR_RELN + "/" + corporateCompanyId + "/" + vendorId;
        ResponseEntity<CorporateVendorDto> response = restTemplate.exchange(url, HttpMethod.GET, null
                , new ParameterizedTypeReference<CorporateVendorDto>() {
                });
        CorporateVendorDto savedCorporateVendorDto = response.getBody();
        CorporateVendorMinDto corporateVendorMinDto = null;
        if (savedCorporateVendorDto != null) {
            corporateVendorMinDto = new CorporateVendorMinDto(savedCorporateVendorDto);
        }
        return corporateVendorMinDto;
    }

    @Override
    public String assignVendorRecruiters(VendorRecruiterUpdateDto vendorRecruiterUpdate) {
        String url = ApiConstants.ASSIGN_VENDOR_RECRUITERS;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(vendorRecruiterUpdate));
    }

    @Override
    public String copyRequisition(String requisitionId, String companyId, String userId) {
        String url = ApiConstants.COPY_REQUISITION + "/" + requisitionId + "/" + companyId + "/" + userId;
        return getResponse(url, HttpMethod.GET, new HttpEntity<>(null));
    }

    @Override
    public List<NewJobDto> getAllJobsByClientOrBuId(JobRequestDto jobRequestDto, String companyId, String userId) {
        String url = ApiConstants.GET_ALL_JOBS_BY_CLIENT_OR_BU_ID + "/" + companyId + "/" + userId;
        ResponseEntity<List<NewJobDto>> response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(jobRequestDto)
                , new ParameterizedTypeReference<List<NewJobDto>>() {
                });
        return response.getBody();
    }

    @Override
    public String getReleasedJobDetails(String candidateId) {
        String url = ApiConstants.GET_RELEASED_JOB_DETAILS + "/" + candidateId;
        return getResponse(url, HttpMethod.GET, new HttpEntity<>(null));
    }

    @Override
    public List<NewJobDto> getAllJobsByClientOrBuIdAndCompanyId(JobRequestDto jobRequestDto, String companyId, String clientOrBuId, String userId) {
        String url = ApiConstants.GET_ALL_JOBS_BY_CLIENT_OR_BU_ID_AND_COMPANY_ID + "/" + companyId + "/" + clientOrBuId + "/" + userId;
        ResponseEntity<List<NewJobDto>> response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(jobRequestDto)
                , new ParameterizedTypeReference<List<NewJobDto>>() {
                });
        return response.getBody();
    }

    @Override
    public String getLastAddedCandidateSourceTypeId(String jobId) {
        String url = ApiConstants.GET_LAST_ADDED_CANDIDATE_SOURCE_TYPE_ID + "/" + jobId;
        return getResponse(url, HttpMethod.GET, new HttpEntity<>(null));

    }

    @Override
    public Job findJobById(String jobId) {
        String url = ApiConstants.FIND_JOB_BY_ID + "/" + jobId;
        ResponseEntity<Job> response = restTemplate.exchange(url, HttpMethod.GET, null
                , new ParameterizedTypeReference<Job>() {
                });
        return response.getBody();
    }

    @Override
    public String getPointOfContact(String jobId) {
        String url = ApiConstants.GET_POC + "/" + jobId;
        return getResponse(url, HttpMethod.GET, null);
    }

    @Override
    public String getSentAssessmentDetailsByAssessmentId(String assessmentId, String jobId) {
        String url = ApiConstants.GET_SENT_ASSESSMENT_DETAILS_BY_ASSESSMENT_ID_AND_JOB_ID + "/" + assessmentId + "/" + jobId;
        return getResponse(url, HttpMethod.GET, null);
    }

    @Override
    public List<JobDetailsDTO> getRequisitionsByAssement(JobAssessmentRequestDTO jobAssessmentRequestDTO) {
        String url = ApiConstants.GET_REQUISITIONS_BY_ASSESSMENT;
        ResponseEntity<List<JobDetailsDTO>> response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(jobAssessmentRequestDTO)
                , new ParameterizedTypeReference<List<JobDetailsDTO>>() {
                });
        return response.getBody();
    }

    @Override
    public void saveCandidateScoreOnVendorAssociation(List<String> vendorIds, String jobId) {
        String url = ApiConstants.CREATE_CANDIDATE_SCORE_WITH_VENDOR_JOBS + "/" + jobId;
        getResponse(url, HttpMethod.POST, new HttpEntity<>(vendorIds));
    }

    @Override
    public String getSharedJobWorkflowDetails(String jobId) {
        String url = ApiConstants.GET_SHARED_JOB_WORKFLOW_DETAIL + "/" + jobId;
        return getResponse(url, HttpMethod.GET, null);
    }

    @Override
    public void checkForDuplicateHiringManagers(Company company) {
        String url = ApiConstants.CHECK_DUPLICATE_HIRING_MANAGERS;
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(company)
                , new ParameterizedTypeReference<String>() {
                });
        if (response.getBody() != null) {
            throw new PlatformException(response.getBody());
        }
    }

    @Override
    public String updateCompany(String companyId, String testLinkName) {
        String url = ApiConstants.UPDATE_COMPANY + "?companyId=" + companyId + "&testLinkName=" + testLinkName;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(null));
    }

    @Override
    public List<CandidateAssessmentReportDetails> getCompanyCandidateReportDetails(AssessmentReportFilterDto assessmentReportFilterDto) {
        String url = ApiConstants.COMPANY_CANDIDATE_REPORTS;
        ResponseEntity<List<CandidateAssessmentReportDetails>> response = restTemplate.exchange(url, HttpMethod.POST
                , new HttpEntity<Object>(assessmentReportFilterDto),
                new ParameterizedTypeReference<List<CandidateAssessmentReportDetails>>() {
                });
        return response.getBody();
    }

    @Override
    public String deleteCorporateVendorRelationship(DeleteDTO deleteDTO, String userId) {
        String url = ApiConstants.SOFT_DELETE_CORPORATE_VENDOR_RELN + "?loggedInUserId=" + userId;
        return getResponse(url, HttpMethod.DELETE, new HttpEntity<>(deleteDTO));
    }

    @Override
    public String deleteBuVendorRelationship(DeleteBuVendorDto deleteBuVendorDto) {
        String url = ApiConstants.SOFT_DELETE_BU_VENDOR_RELN;
        return getResponse(url, HttpMethod.DELETE, new HttpEntity<>(deleteBuVendorDto));
    }

    @Override
    public DeleteCountDto getSharedRequisitionAndBuCounts(DeleteBuVendorDto deleteBuVendorDto) {
        String url = ApiConstants.GET_SHARED_REQ_AND_BU_COUNT;
        ResponseEntity<DeleteCountDto> response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(deleteBuVendorDto)
                , new ParameterizedTypeReference<DeleteCountDto>() {
                });
        return response.getBody();
    }

    @Override
    public List<ClientOrBuCountDTO> getClientOrBuDetails(ClientOrBuDetailsDTO clientOrBuDetailsDTO) {
        String url = ApiConstants.GET_CLIENT_OR_BU_DETAILS;
        ResponseEntity<List<ClientOrBuCountDTO>> response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(clientOrBuDetailsDTO)
                , new ParameterizedTypeReference<List<ClientOrBuCountDTO>>() {
                });
        return response.getBody();
    }

    @Override
    public List<CompanyMinDto> searchForCompanies(String name, Integer page, Integer size, String sort, String sortDir) {
        String url = ApiConstants.SEARCH_COMPANIES_BY_NAME + "?" + "searchString=" + name + "&" + "page=" + page +
                "&" + "size=" + size + "&" + "sort=" + sort + "&" +
                "sortDir=" + sortDir;

        LOGGER.debug("Executing url: Search for company by name - {}", url);
        ResponseEntity<List<CompanyMinDto>> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<CompanyMinDto>>() {
                });
        return response.getBody();
    }

    @Override
    public String getAssessmentReportQuestionType(String companyId) {
        String url = ApiConstants.GET_ASSESSMENT_REPORT_QUESTION_TYPE + "/" + companyId;
        return getResponse(url, HttpMethod.GET, new HttpEntity<>(null));
    }

    @Override
    public String updateAssessmentReportQuestionType(String companyId, QuestionType questionType) {
        String url = ApiConstants.UPDATE_ASSESSMENT_REPORT_QUESTION_TYPE + "/" + companyId + "?questionType=" + questionType;
        return getResponse(url, HttpMethod.GET, new HttpEntity<>(null));
    }

    @Override
    public String hasAccessToAddCandidateWithOrWithoutResume(String companyId, String clientOrBuId) {
        String url = ApiConstants.HAS_ACCESS_TO_ADD_CANDIDATE_WITHOUT_RESUME + "/" + companyId;
        if (StringUtils.isNotEmpty(clientOrBuId)) {
            url = url + "?clientOrBuId=" + clientOrBuId;
        }
        return getResponse(url, HttpMethod.GET, new HttpEntity<>(null));
    }

    @Override
    public String getSharedClientDetails(String companyId) {
        String url = ApiConstants.GET_SHARED_CLIENT_DETAILS + "/" + companyId;
        return getResponse(url, HttpMethod.GET, new HttpEntity<>(null));
    }

    @Override
    public String getVendorsByUserScope(String companyId, String userId) {
        String url = ApiConstants.GET_VENDORS_BY_USER_SCOPE + "/" + companyId + "/" + userId;
        return getResponse(url, HttpMethod.GET, new HttpEntity<>(null));
    }

    @Override
    public String getClientOrBuPointOfContacts(String corporateId, String clientOrBuId) {
        String url = ApiConstants.GET_CLIENT_OR_BU_POINT_OF_CONTACTS + "?corporateId=" + corporateId
                + "&clientOrBuId=" + clientOrBuId;
        return getResponse(url, HttpMethod.GET, new HttpEntity<>(null));
    }

    @Override
    public CorporateVendorMinDto setCorporateVendorRelationship(String corporateCompanyId, String vendorId) {
        String url = ApiConstants.SET_CORPORATE_VENDOR_RELN + "/" + corporateCompanyId + "/" + vendorId;
        ResponseEntity<CorporateVendorDto> response = restTemplate.exchange(url, HttpMethod.GET, null
                , new ParameterizedTypeReference<CorporateVendorDto>() {
                });
        CorporateVendorDto savedCorporateVendorDto = response.getBody();
        CorporateVendorMinDto corporateVendorMinDto = null;
        if (savedCorporateVendorDto != null) {
            corporateVendorMinDto = new CorporateVendorMinDto(savedCorporateVendorDto);
        }
        return corporateVendorMinDto;
    }

    @Override
    public String getVendorRecruitersOfSharedRequisition(String requisitionId, String vendorId) {
        String url = ApiConstants.GET_ASSIGNED_VENDOR_RECRUITERS_OF_SHARED_REQUISITION + "/" + requisitionId + "/" + vendorId;
        return getResponse(url, HttpMethod.GET, new HttpEntity<>(null));
    }

    @Override
    public List<User> getPossibleManagersForHiringManager(String companyId, String userId) {
        String url = ApiConstants.GET_POSSIBLE_MANAGERS_FOR_HIRING_MANAGER + "/" + companyId;
        if (StringUtils.isNotEmpty(userId)) {
            url = url + "?userId=" + userId;
        }

        ResponseEntity<List<User>> response = restTemplate.exchange(url, HttpMethod.GET, null
                , new ParameterizedTypeReference<List<User>>() {
                });

        return response.getBody();
    }

    @Override
    public UserLoggedInStatus getUserLoggedInStatus(String userId) {
        String url = ApiConstants.GET_USER_LOGGED_IN_STATUS + "/" + userId;

        ResponseEntity<UserLoggedInStatus> response = restTemplate.exchange(url, HttpMethod.GET, null
                , new ParameterizedTypeReference<UserLoggedInStatus>() {
                });

        return response.getBody();
    }

    @Override
    public String createOrUpdateEmployee(EmployeeDTO employeeDTO) {
        String url = ApiConstants.CREATE_OR_UPDATE_EMPLOYEE;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(employeeDTO));
    }

    @Override
    public String searchEmployee(EmployeeSearchDTO employeeSearchDTO) {
        String url = ApiConstants.SEARCH_EMPLOYEES;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(employeeSearchDTO));
    }

    @Override
    public String getDepartments(DepartmentFilterDTO departmentFilterDTO) {
        String url = ApiConstants.GET_DEPARTMENTS;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(departmentFilterDTO));
    }

    @Override
    public String convertUserToEmployee(EmployeeToUserRequestDTO employeeToUserRequestDTO) {
        String url = ApiConstants.SET_USER_TO_EMPLOYEE;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(employeeToUserRequestDTO));
    }

    @Override
    public String convertEmployeeToUser(EmployeeUserCovertDTO employeeUserCovertDTO) {
        String url = ApiConstants.SET_EMPLOYEE_TO_USER;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(employeeUserCovertDTO));
    }

    @Override
    public String getEmployee(String employeeId) {
        String url = ApiConstants.GET_EMPLOYEE + "/" + employeeId;
        return getResponse(url, HttpMethod.GET, new HttpEntity<>(null));
    }

    @Override
    public String getBusByUserScope(BUFilterDTO buFilterDTO) {
        String url = ApiConstants.GET_BUS_BY_USER_SCOPE;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(buFilterDTO));
    }

    @Override
    public String getOrganizationalManager(OrganizationManagerRequestDTO organizationManagerRequestDTO) {
        String url = ApiConstants.GET_ORGANIZATIONAL_MANAGER;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(organizationManagerRequestDTO));
    }

    @Override
    public DepartmentDto getDepartmentById(String departmentId) {
        String url = ApiConstants.GET_DEPARTMENT_BY_ID + "/" + departmentId;
        ResponseEntity<DepartmentDto> response = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<DepartmentDto>() {
        });
        return response.getBody();
    }

    @Override
    public String getPermissibleManagers(EmployeeManagerRequestDto employeeManagerRequestDto) {
        String url = ApiConstants.GET_PERMISSIBLE_MANAGERS;
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(employeeManagerRequestDto), new ParameterizedTypeReference<String>() {
        });
        return response.getBody();
    }

    @Override
    public Company getClientByRequisitionId(String requisitionId) {
        String url = ApiConstants.GET_CLIENT_BY_REQUISITION_ID + "/" + requisitionId;
        ResponseEntity<Company> response = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<Company>() {
        });
        return response.getBody();
    }

    @Override
    public List<RequisitionDetailsResponseDTO> getRequisitionDetailsByClientOrBuId(RequisitionDetailsDTO requisitionDetailsDTO) {
        String url = ApiConstants.GET_REQUISITION_DETAILS_BY_CLIENT_OR_BU_ID;
        ResponseEntity<List<RequisitionDetailsResponseDTO>> response
                = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<Object>(requisitionDetailsDTO), new ParameterizedTypeReference<List<RequisitionDetailsResponseDTO>>() {
        });
        return response.getBody();
    }

    @Override
    public String saveChat(ChatMessageDTO chatMessageDTO) {
        String url = ApiConstants.SAVE_CHAT;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(chatMessageDTO));
    }

    @Override
    public String getAllChatsByUserIdAndJobMatchId(String loggedInUserId, String jobMatchId, ChatFilterDTO chatFilterDTO) {
        String url = ApiConstants.GET_CHAT_BY_USER + "/" + loggedInUserId + "/" + jobMatchId;
        return getResponse(url, HttpMethod.POST, new HttpEntity<>(chatFilterDTO));
    }

    @Override
    public String changeChatMessageState(String chatMessageId, String userId) {
        String url = ApiConstants.CHANGE_CHAT_MESSAGE_STATE + "/" + userId + "/" + chatMessageId;
        return getResponse(url, HttpMethod.GET, null);
    }

    @Override
    public Map<String, List<User>> getVendorRecruiters(VendorRecruitersCollectionRequestDto request) {
        String url = ApiConstants.GET_RECRUITERS_OF_VENDORS;
        ResponseEntity<Map<String, List<User>>> response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(request)
                , new ParameterizedTypeReference<Map<String, List<User>>>() {
                });
        return response.getBody();
    }

    @Override
    public Map<String, List<User>> getAssignedHiringManagersAndRecruiters(String requisitionId) {
        String url = ApiConstants.GET_ASSIGNED_HIRINGMANAGERS_AND_RECRUITERS_OF_JOB + "/" + requisitionId;
        ResponseEntity<Map<String, List<User>>> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<Map<String, List<User>>>() {
                });
        return response.getBody();
    }

    @Override
    public Map<String, List<User>> getAssignedVendorsAndRecruiters(String requisitionId) {

        String url = ApiConstants.GET_ASSIGNED_VENDORS_AND_RECRUITERS_OF_JOB + "/" + requisitionId;
        ResponseEntity<Map<String, List<User>>> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<Map<String, List<User>>>() {
                });
        return response.getBody();
    }

    @Override
    public List<UserDTO> getCompanyUsersForActivities(String companyId) {
        String url = ApiConstants.COMPANY_USERS_FOR_ACTIVITIES + "/" + companyId;
        ResponseEntity<List<UserDTO>> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<UserDTO>>() {
                });
        return response.getBody();
    }

    @Override
    public Map<String, ClientBUInfo> setClientBuHierarchy(String userId, String companyId) {
        String url = ApiConstants.GET_CORPORATE_BUS_OF_USER + "/" + companyId + "/" + userId;
        ResponseEntity<Map<String, ClientBUInfo>> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<Map<String, ClientBUInfo>>() {
                });
        return response.getBody();
    }

    @Override
    public List<Company> getAllClientOrBusOfUser(String userId, String companyId) {
        String url = ApiConstants.GET_ALL_CLIENT_OR_BUS_OF_USER + "/" + userId + "/" + companyId;
        ResponseEntity<List<Company>> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<Company>>() {
                });
        return response.getBody();
    }

    @Override
    public String getUserByToken(String token) {
        String url = ApiConstants.GET_USER_BY_TOKEN + "/" + token;
        return getResponse(url, HttpMethod.GET, null);
    }

    @Override
    public List<User> getAllPermissibleUsers(List<String> userIds, String companyId) {
        String url = ApiConstants.GET_ALL_PERMISSIBLE_USER + "/" + companyId;
        ResponseEntity<List<User>> permissibleUsers = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(userIds),
                new ParameterizedTypeReference<List<User>>() {
                });
        return permissibleUsers.getBody();
    }

    @Override
    public List<User> getAllRecruitersOrHiringManagers(String userId, String companyId, List<String> userIds) {
        String url = ApiConstants.GET_ALL_RECRUITERS_OR_HIRING_MANAGER + "/" + companyId + "/" + userId;
        ResponseEntity<List<User>> permissibleUsers = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(userIds),
                new ParameterizedTypeReference<List<User>>() {
                });
        return permissibleUsers.getBody();
    }

    @Override
    public boolean hasAccessToClientOrBu(String userId, String clientOrBuId) {
        String url = ApiConstants.HAS_ACCESS_TO_CLIENT_OR_BU + "/" + userId + "/" + clientOrBuId;
        ResponseEntity<Boolean> permissibleUsers = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<Boolean>() {
                });
        return permissibleUsers.getBody();
    }

    @Override
    public String getSubordinatesByBU(String userId, String buId) {
        String url = ApiConstants.GET_SUBORDINATES_BY_BU + "/" + userId + "/" + buId;
        return getResponse(url, HttpMethod.GET, new HttpEntity<>(null));
    }


    @Override
    public WorkflowStepDTO saveWorkflowStepOrder(WorkflowStepDTO workflowStepDTO) {
        String url = ApiConstants.SAVE_WORKFLOW_STEP_ORDER;
        ResponseEntity<WorkflowStepDTO> response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<Object>(workflowStepDTO),
                new ParameterizedTypeReference<WorkflowStepDTO>() {
                });
        return response.getBody();
    }

    @Override
    public WorkflowStepDTO getWorkflowStepsOrder(WorkflowStepRequestDTO workflowStepRequestDTO) {
        String url = ApiConstants.GET_WORKFLOW_STEP_ORDER;
        ResponseEntity<WorkflowStepDTO> response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<Object>(workflowStepRequestDTO),
                new ParameterizedTypeReference<WorkflowStepDTO>() {
                });
        return response.getBody();
    }

    @Override
    public Map<String, List<String>> getWorkflowStepsByRequisitionId(WorkflowStepRequestDTO workflowStepRequestDTO) {
        String url = ApiConstants.GET_WORKFLOW_STEPS_BY_REQUSITION_ID;
        ResponseEntity<Map<String, List<String>>> response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<Object>(workflowStepRequestDTO),
                new ParameterizedTypeReference<Map<String, List<String>>>() {
                });
        return response.getBody();
    }

    @Override
    public Map<String, List<String>> getWorkflowStepsByCompanyId(String companyId) {
        String url = ApiConstants.GET_WORKFLOW_STEPS_BY_COMPANY_ID + "/" + companyId;
        ResponseEntity<Map<String, List<String>>> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<Map<String, List<String>>>() {
                });
        return response.getBody();
    }

    @Override
    public void saveWorkflowSteps(WorkflowStepDTO workflowStepDTO) {
        String url = ApiConstants.SAVE_WORKFLOW_STEPS;
        restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<Object>(workflowStepDTO),
                new ParameterizedTypeReference<WorkflowStepRequestDTO>() {
                });
    }

    @Override
    public void saveWorkflowStepsByRequisitionId(WorkflowStepDTO workflowStepDTO) {
        String url = ApiConstants.SAVE_WORKFLOW_STEPS_BY_REQUISITION_ID;
        restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<Object>(workflowStepDTO),
                new ParameterizedTypeReference<WorkflowStepRequestDTO>() {
                });
    }

    @Override
    public String getSalaryDurationOptions() {
        String url = ApiConstants.GET_SALARY_DURATION_OPTIONS;
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<String>() {
                });
        return response.getBody();
    }

    @Override
    public String getCurrencyCodes() {
        String url = ApiConstants.GET_CURRENCY_CODES;
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<String>() {
                });
        return response.getBody();
    }

    @Override
    public String uploadCompanyLogo(String companyId, MultipartFile file, boolean isSmallHeaderLogo) throws IOException {
        String url = ApiConstants.UPLOAD_COMPANY_LOGO + "/" + companyId + "?isSmallHeaderLogo=" + isSmallHeaderLogo;
        MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        String fileName = file.getOriginalFilename();
        if (StringUtils.isNotBlank(fileName)) {
            fileName = fileName.replace("\u00A0", " ");
        }
        final String name = fileName;
        ByteArrayResource resource = new ByteArrayResource(file.getBytes()) {
            @Override
            public String getFilename() {
                return name;
            }
        };
        map.add("file", resource);
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(map, header);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
                new ParameterizedTypeReference<String>() {
                });
        return response.getBody();
    }
}