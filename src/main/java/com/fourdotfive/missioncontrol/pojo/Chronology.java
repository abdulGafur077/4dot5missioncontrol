package com.fourdotfive.missioncontrol.pojo;

public class Chronology {

	private Zone zone;

	public Zone getZone() {
		return zone;
	}

	public void setZone(Zone zone) {
		this.zone = zone;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Chronology [zone=").append(zone).append("]");
		return builder.toString();
	}

}

