package com.fourdotfive.missioncontrol.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Map;

public class FileInfo {

    @JsonProperty("fileName")
    private String fileName;

    @JsonProperty("cloudFileLocation")
    private String fileLocation;

    private String subDir;

    @JsonProperty("clientOrBU")
    private void unpackNameFromNestedObject(Map<String, Object> clientOrBU) {
        if (clientOrBU != null) {
            Object a = clientOrBU.get("id");
            subDir = a.toString();
        }
    }

	/*public class ClientOrBu{
	    private String id;

	    public String getId() {
            return id;
        }
        public void setId(String id) {
            this.id = id;
        }
	}*/

    public String getFileName() {
        return fileName;
    }
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    public String getFileLocation() {
        return fileLocation;
    }
    public void setFileLocation(String fileLocation) {
        this.fileLocation = fileLocation;
    }

    public String getSubDir() {
        return subDir;
    }
    public void setSubDir(String subDir) {
        this.subDir = subDir;
    }

	/*public ClientOrBu getClientOrBu() {
		return clientOrBU;
	}
	public void setClientOrBu(ClientOrBu clientOrBU) {
		this.clientOrBU = clientOrBU;
	}
	*/

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("FileInfo [subDir=").append(subDir)
                .append(", fileLocation=").append(fileLocation)
                .append(", fileName=").append(fileName)
                .append("]");
        return builder.toString();
    }

}