package com.fourdotfive.missioncontrol.pojo;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

public class FourDotFiveDate {

	private Chronology chronology;

	private int dayOfMonth;

	private int dayOfWeek;

	private int dayOfYear;

	private boolean equalNow;

	private int hourOfDay;

	private int minuteOfDay;

	private int minuteOfHour;

	private int monthOfYear;
	
	private int secondOfMinute;
	
	private int millisOfSecond;

	private int year;

	private Zone zone;
	
	private DateTime dateTime;
	
	
	public Chronology getChronology() {
		return chronology;
	}

	public void setChronology(Chronology chronology) {
		this.chronology = chronology;
	}

	public int getDayOfMonth() {
		return dayOfMonth;
	}

	public void setDayOfMonth(int dayOfMonth) {
		this.dayOfMonth = dayOfMonth;
	}

	public int getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(int dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	public int getDayOfYear() {
		return dayOfYear;
	}

	public void setDayOfYear(int dayOfYear) {
		this.dayOfYear = dayOfYear;
	}

	public boolean isEqualNow() {
		return equalNow;
	}

	public void setEqualNow(boolean equalNow) {
		this.equalNow = equalNow;
	}

	public int getHourOfDay() {
		return hourOfDay;
	}

	public void setHourOfDay(int hourOfDay) {
		this.hourOfDay = hourOfDay;
	}

	public int getMinuteOfDay() {
		return minuteOfDay;
	}

	public void setMinuteOfDay(int minuteOfDay) {
		this.minuteOfDay = minuteOfDay;
	}

	public int getMinuteOfHour() {
		return minuteOfHour;
	}

	public void setMinuteOfHour(int minuteOfHour) {
		this.minuteOfHour = minuteOfHour;
	}
	
	public int getSecondOfMinute() {
		return secondOfMinute;
	}

	public void setSecondOfMinute(int secondOfMinute) {
		this.secondOfMinute = secondOfMinute;
	}

	public int getMillisOfSecond() {
		return millisOfSecond;
	}

	public void setMillisOfSecond(int millisOfSecond) {
		this.millisOfSecond = millisOfSecond;
	}

	public int getMonthOfYear() {
		return monthOfYear;
	}

	public void setMonthOfYear(int monthOfYear) {
		this.monthOfYear = monthOfYear;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public Zone getZone() {
		return zone;
	}

	public void setZone(Zone zone) {
		this.zone = zone;
	}
	
	public DateTime getDateTime() {
		return new DateTime(dayOfYear, monthOfYear, dayOfMonth, 
				hourOfDay, minuteOfHour, DateTimeZone.getDefault()); 
	}

	public void setDateTime(DateTime dateTime) {
		this.dateTime = dateTime;
	}

	@Override
	public String toString() {
		return "FourDotFiveDate [chronology=" + chronology + ", dayOfMonth=" + dayOfMonth + ", dayOfWeek=" + dayOfWeek
				+ ", dayOfYear=" + dayOfYear + ", equalNow=" + equalNow + ", hourOfDay=" + hourOfDay + ", minuteOfDay="
				+ minuteOfDay + ", minuteOfHour=" + minuteOfHour + ", monthOfYear=" + monthOfYear + ", year=" + year
				+ ", zone=" + zone + ", dateTime=" + dateTime + "]";
	}

}
