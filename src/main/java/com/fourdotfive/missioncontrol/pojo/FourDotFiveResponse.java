package com.fourdotfive.missioncontrol.pojo;

public class FourDotFiveResponse<T> {
    private T t;
    private String message;
    private int code;
    private boolean isError;

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isError() {
        return isError;
    }

    public void setError(boolean isError) {
        this.isError = isError;
    }

    @Override
    public String toString() {
        return "FourDotFiveResponse{" +
                "t=" + t +
                ", message='" + message + '\'' +
                ", code=" + code +
                ", isError=" + isError +
                '}';
    }
}
