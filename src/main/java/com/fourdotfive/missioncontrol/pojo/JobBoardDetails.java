package com.fourdotfive.missioncontrol.pojo;

public class JobBoardDetails {

    private String boardId;

    private boolean enabled;

    public String getBoardId() {
        return boardId;
    }

    public void setBoardId(String boardId) {
        this.boardId = boardId;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
