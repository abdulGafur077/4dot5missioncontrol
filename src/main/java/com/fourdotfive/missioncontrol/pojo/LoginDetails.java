package com.fourdotfive.missioncontrol.pojo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.fourdotfive.missioncontrol.common.AppConstants;

/**
 * 
 * @author shalini
 *
 */
public class LoginDetails {

	@NotNull
	private String email;

	@NotNull
	@Size(min = AppConstants.PASSWORD_MIN_LENGTH, max = AppConstants.PASSWORD_MAX_LENGTH)
	private String password;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public static MultiValueMap<String, String> getMap(LoginDetails loginDetails) {

		MultiValueMap<String, String> loginParams = new LinkedMultiValueMap<>();
		loginParams.add(AppConstants.USERNAME, loginDetails.getEmail());
		loginParams.add(AppConstants.PASSWORD, loginDetails.getPassword());

		return loginParams;
	}

	public static MultiValueMap<String, String> getMap(String email) {

		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add(AppConstants.USERNAME, email);

		return params;
	}

	@Override
	public String toString() {
		return "LoginDetails [email=" + email + ", password=" + password + "]";
	}

}
