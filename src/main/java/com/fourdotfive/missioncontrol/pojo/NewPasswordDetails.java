package com.fourdotfive.missioncontrol.pojo;

import com.fourdotfive.missioncontrol.common.AppConstants;
import com.fourdotfive.missioncontrol.login.SetPasswordEmailType;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class NewPasswordDetails {

    @NotNull
    @Size(min = AppConstants.PASSWORD_MIN_LENGTH, max = AppConstants.PASSWORD_MAX_LENGTH)
    private String newPassword;

    @NotNull
    @Size(min = AppConstants.PASSWORD_MIN_LENGTH, max = AppConstants.PASSWORD_MAX_LENGTH)
    private String confirmPassword;

    private SetPasswordEmailType setPasswordEmailType;

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public SetPasswordEmailType getSetPasswordEmailType() {
        return setPasswordEmailType;
    }

    public void setSetPasswordEmailType(SetPasswordEmailType setPasswordEmailType) {
        this.setPasswordEmailType = setPasswordEmailType;
    }
}
