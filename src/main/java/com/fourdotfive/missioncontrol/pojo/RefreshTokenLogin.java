package com.fourdotfive.missioncontrol.pojo;

public class RefreshTokenLogin {

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
