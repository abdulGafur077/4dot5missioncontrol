package com.fourdotfive.missioncontrol.pojo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fourdotfive.missioncontrol.common.AppConstants;

public class ResetPwdDetails {

	private String email;

	@NotNull
	@Size(min = AppConstants.PASSWORD_MIN_LENGTH, max = AppConstants.PASSWORD_MAX_LENGTH)
	private String currentPassword;

	@NotNull
	@Size(min = AppConstants.PASSWORD_MIN_LENGTH, max = AppConstants.PASSWORD_MAX_LENGTH)
	private String newPassword;

	private String verifyPassword;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCurrentPassword() {
		return currentPassword;
	}

	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getVerifyPassword() {
		return verifyPassword;
	}

	public void setVerifyPassword(String verifyPassword) {
		this.verifyPassword = verifyPassword;
	}

	@Override
	public String toString() {
		return "ResetPwdDetails [email=" + email + ", currentPassword=" + currentPassword + ", newPassword="
				+ newPassword + ", verifyPassword=" + verifyPassword + "]";
	}

}
