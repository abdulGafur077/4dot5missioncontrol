package com.fourdotfive.missioncontrol.pojo;

import java.util.List;

public class RoleScreenXRef {
	
	private String id;
	
	private List<Role> roles;
	
	private List<Screen> screens;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public List<Screen> getScreens() {
		return screens;
	}

	public void setScreens(List<Screen> screens) {
		this.screens = screens;
	}

	@Override
	public String toString() {
		return "RoleScreenXRef [id=" + id + ", roles=" + roles + ", screens=" + screens + "]";
	}
	
}
