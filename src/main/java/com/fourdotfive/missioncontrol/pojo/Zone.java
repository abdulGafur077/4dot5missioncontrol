package com.fourdotfive.missioncontrol.pojo;

public class Zone {

	private boolean fixed;

	private String id;

	public boolean isFixed() {
		return fixed;
	}

	public void setFixed(boolean fixed) {
		this.fixed = fixed;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Zone [fixed=").append(fixed).append(", id=").append(id)
				.append("]");
		return builder.toString();
	}

}
