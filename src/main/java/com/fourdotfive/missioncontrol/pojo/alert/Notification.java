package com.fourdotfive.missioncontrol.pojo.alert;

import java.util.Set;

public class Notification {
    private String notificationId;

    private String companyId;

    private String clientOrBUId;

    private String jobId;

    private String candidateId;

    private String jobMatchId;

    private String eventId;

    private String alertId;

    private String recipientId;

    private String recipientType;

    private String mode;

    private String status;

    private String state;

    private String text;

    private Set<String> notifyOn;

    private String createdDate;

    private String lastModifiedDate;

    private String operation;

    private String taggedText;

    private String jbiTransactionId;

    private String eventType;

    private String assessmentType;

    private String testInvitationId;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getClientOrBUId() {
        return clientOrBUId;
    }

    public void setClientOrBUId(String clientOrBUId) {
        this.clientOrBUId = clientOrBUId;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(String candidateId) {
        this.candidateId = candidateId;
    }

    public String getJobMatchId() {
        return jobMatchId;
    }

    public void setJobMatchId(String jobMatchId) {
        this.jobMatchId = jobMatchId;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getAlertId() {
        return alertId;
    }

    public void setAlertId(String alertId) {
        this.alertId = alertId;
    }

    public String getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(String recipientId) {
        this.recipientId = recipientId;
    }

    public String getRecipientType() {
        return recipientType;
    }

    public void setRecipientType(String recipientType) {
        this.recipientType = recipientType;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Set<String> getNotifyOn() {
        return notifyOn;
    }

    public void setNotifyOn(Set<String> notifyOn) {
        this.notifyOn = notifyOn;
    }

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(String lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }
    public String getTaggedText() {
        return taggedText;
    }

    public void setTaggedText(String taggedText) {
        this.taggedText = taggedText;
    }


    public String getJbiTransactionId() {
        return jbiTransactionId;
    }

    public void setJbiTransactionId(String jbiTransactionId) {
        this.jbiTransactionId = jbiTransactionId;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getAssessmentType() {
        return assessmentType;
    }

    public void setAssessmentType(String assessmentType) {
        this.assessmentType = assessmentType;
    }

    public String getTestInvitationId() {
        return testInvitationId;
    }

    public void setTestInvitationId(String testInvitationId) {
        this.testInvitationId = testInvitationId;
    }

    @Override
    public String toString() {
        return "Notification{" +
                "notificationId='" + notificationId + '\'' +
                ", companyId='" + companyId + '\'' +
                ", clientOrBUId='" + clientOrBUId + '\'' +
                ", jobId='" + jobId + '\'' +
                ", candidateId='" + candidateId + '\'' +
                ", jobMatchId='" + jobMatchId + '\'' +
                ", eventId='" + eventId + '\'' +
                ", alertId='" + alertId + '\'' +
                ", recipientId='" + recipientId + '\'' +
                ", recipientType='" + recipientType + '\'' +
                ", mode='" + mode + '\'' +
                ", status='" + status + '\'' +
                ", state='" + state + '\'' +
                ", text='" + text + '\'' +
                ", notifyOn=" + notifyOn +
                ", createdDate='" + createdDate + '\'' +
                ", lastModifiedDate='" + lastModifiedDate + '\'' +
                ", operation='" + operation + '\'' +
                ", taggedText='" + taggedText + '\'' +
                ", jbiTransactionId='" + jbiTransactionId + '\'' +
                ", eventType='" + eventType + '\'' +
                ", assessmentType='" + assessmentType + '\'' +
                ", testInvitationId='" + testInvitationId + '\'' +
                '}';
    }
}

