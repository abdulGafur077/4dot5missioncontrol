package com.fourdotfive.missioncontrol.pojo.alert;

public class WorkflowUpdateAlertDTO {
    private String jobmatchId;

    private String reqId;

    private String candidateId;

    private String recipientId;

    private boolean refreshCard;

    private boolean refreshWorkflow;


    public String getJobmatchId() {
        return jobmatchId;
    }

    public void setJobmatchId(String jobmatchId) {
        this.jobmatchId = jobmatchId;
    }

    public String getReqId() {
        return reqId;
    }

    public void setReqId(String reqId) {
        this.reqId = reqId;
    }

    public String getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(String candidateId) {
        this.candidateId = candidateId;
    }

    public String getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(String recipientId) {
        this.recipientId = recipientId;
    }

    public boolean isRefreshCard() {
        return refreshCard;
    }

    public void setRefreshCard(boolean refreshCard) {
        this.refreshCard = refreshCard;
    }

    public boolean isRefreshWorkflow() {
        return refreshWorkflow;
    }

    public void setRefreshWorkflow(boolean refreshWorkflow) {
        this.refreshWorkflow = refreshWorkflow;
    }

    @Override
    public String toString() {
        return "WorkflowUpdateAlertDTO{" +
                "jobmatchId='" + jobmatchId + '\'' +
                ", reqId='" + reqId + '\'' +
                ", candidateId='" + candidateId + '\'' +
                ", recipientId='" + recipientId + '\'' +
                ", refreshCard=" + refreshCard +
                ", refreshWorkflow=" + refreshWorkflow +
                '}';
    }
}

