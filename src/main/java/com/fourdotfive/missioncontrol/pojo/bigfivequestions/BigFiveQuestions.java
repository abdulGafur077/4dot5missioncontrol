package com.fourdotfive.missioncontrol.pojo.bigfivequestions;

public class BigFiveQuestions {

        private String qno;

        private String id;

        private String question;

        public String getQno() {
            return qno;
        }

        public void setQno(String qno) {
            this.qno = qno;
        }

        public String getQuestion() {
            return question;
        }

        public void setQuestion(String question) {
            this.question = question;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

}
