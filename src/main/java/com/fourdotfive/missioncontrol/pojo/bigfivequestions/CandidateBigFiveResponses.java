package com.fourdotfive.missioncontrol.pojo.bigfivequestions;
import java.util.Date;
public class CandidateBigFiveResponses {



        private String candidateId;

        private String jobMatchId;

        private String id;

        private String qno;


        private String response;

        private  Date responseDateTime;

        private String companyId;

        private String clientOrBuId;

        public String getCandidateId() {
            return candidateId;
        }

        public void setCandidateId(String candidateId) {
            this.candidateId = candidateId;
        }

        public String getJobMatchId() {
            return jobMatchId;
        }

        public void setJobMatchId(String jobMatchId) {
            this.jobMatchId = jobMatchId;
        }

        public String getQno() {
            return qno;
        }

        public void setQno(String qno) {
            this.qno = qno;
        }

        public String getResponse() {
            return response;
        }

        public void setResponse(String response) {
            this.response = response;
        }

        public Date getResponseDateTime() {
            return responseDateTime;
        }

        public void setResponseDateTime(Date responseDateTime) {
            this.responseDateTime = responseDateTime;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }


        public String getCompanyId() {
            return companyId;
        }

        public void setCompanyId(String companyId) {
            this.companyId = companyId;
        }

        public String getClientOrBuId() {
            return clientOrBuId;
        }

        public void setClientOrBuId(String clientOrBuId) {
            this.clientOrBuId = clientOrBuId;
        }
}
