package com.fourdotfive.missioncontrol.pojo.candidate;

public enum ActiveCandidateJobStateType {

    QUALIFIED("Qualified"), NOT_INTERESTED("Not Interested"), RELEASED("Released"), IN_TECH_ASSESSMENT("In Tech Assessment"), SENT_TECH_ASSESSMENT("Sent Tech Assessment"),
    TECH_ASSESSMENT_COMPLETED("Tech Assessment Completed"), TECH_ASSESSMENT_SCORED("Tech Assessment Scored"), IN_VALUE_ASSESSMENT("In Value Assessment"), SENT_VALUE_ASSESSMENT("Sent Value Assessment"),
    VALUE_ASSESSMENT_COMPLETED("Value Assessment Completed"), VALUE_ASSESSMENT_SCORED("Value Assessment Scored"), IN_VEFIFICATION("In Verification"), SENT_FOR_VERIFICATION("Sent For Verification"),
    VERIFICATION_RECEIVED("Verification Received"), IN_INTERVIEW("In Interview"), READY_FOR_INTERVIEW("Ready For Interview"), IN_LIKELY("In Likely");

    private String text;

    ActiveCandidateJobStateType(String text) {
        this.text = text;
    }

    public String getText() {
        return this.text;
    }
}
