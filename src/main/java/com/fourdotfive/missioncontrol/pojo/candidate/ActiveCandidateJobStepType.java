package com.fourdotfive.missioncontrol.pojo.candidate;

public enum ActiveCandidateJobStepType {

    QUALIFIED("Qualified"), TECH_ASSESSMENT("Tech Assessment"), VALUE_ASSESSMENT("Value Assessment"), VEFIFICATION("Verification"), INTERVIEW("Interview"), LIKELY("Likely"), NOT_INTERESTED("Not Interested"), RELEASED("Released");

    private String text;

    ActiveCandidateJobStepType(String text) {
        this.text = text;
    }

    public String getText() {
        return this.text;
    }
}
