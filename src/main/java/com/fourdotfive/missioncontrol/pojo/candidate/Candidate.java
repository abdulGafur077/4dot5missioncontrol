package com.fourdotfive.missioncontrol.pojo.candidate;

import java.util.List;

import com.fourdotfive.missioncontrol.pojo.job.JobMatch;
import com.fourdotfive.missioncontrol.pojo.user.Address;

/**
 * @author Shalini
 */
public class Candidate {

	private String id;

	private Contact contact;
	
	private List<JobMatch> jobMatchList;

	private Address address;
	
	private Profile profile;

	private Boolean isDeleted;

	private boolean isSuspended;

	private String releasedDate;

	private boolean noMatchDueToRelease;

	private boolean duplicateJobMatch;

	private boolean matchedByOtherVendor;
	
	public Candidate() {
		// TODO Auto-generated constructor stub
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public List<JobMatch> getJobMatchList() {
		return jobMatchList;
	}

	public void setJobMatchList(List<JobMatch> jobMatchList) {
		this.jobMatchList = jobMatchList;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public Boolean getDeleted() {
		return isDeleted;
	}

	public void setDeleted(Boolean deleted) {
		isDeleted = deleted;
	}

	public boolean isSuspended() {
		return isSuspended;
	}

	public void setSuspended(boolean suspended) {
		isSuspended = suspended;
	}

	public String getReleasedDate() {
		return releasedDate;
	}

	public void setReleasedDate(String releasedDate) {
		this.releasedDate = releasedDate;
	}

	public boolean isNoMatchDueToRelease() {
		return noMatchDueToRelease;
	}

	public void setNoMatchDueToRelease(boolean noMatchDueToRelease) {
		this.noMatchDueToRelease = noMatchDueToRelease;
	}

	public boolean isDuplicateJobMatch() {
		return duplicateJobMatch;
	}

	public void setDuplicateJobMatch(boolean duplicateJobMatch) {
		this.duplicateJobMatch = duplicateJobMatch;
	}

	public boolean isMatchedByOtherVendor() {
		return matchedByOtherVendor;
	}

	public void setMatchedByOtherVendor(boolean matchedByOtherVendor) {
		this.matchedByOtherVendor = matchedByOtherVendor;
	}

	@Override
	public String toString() {
		return "Candidate [contact=" + contact + ", address=" + address + ", jobMatchList=" + jobMatchList + ", profile=" + profile + "]";
	}
	
}
