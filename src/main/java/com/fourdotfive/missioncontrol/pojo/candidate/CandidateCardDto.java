package com.fourdotfive.missioncontrol.pojo.candidate;

import com.fourdotfive.missioncontrol.dtos.candidate.AddressDTO;
import com.fourdotfive.missioncontrol.dtos.candidate.CandidateAdditionalDetailsCardDto;
import com.fourdotfive.missioncontrol.pojo.BestJobMatchJobLocationDto;

import java.util.List;

public class CandidateCardDto {

    //Info needed - First Name, Last Name, Telephone #, IM and Email address,
    // # of days since last contact, The Best Match Job's Title,
    // Best Match Job's Match Score, The Best Match Job's Primary Recruiter,
    // Best Match Job's Company, Current State wrto that Job Requisition.

    /**
     *
     */

    private static final long serialVersionUID = 1L;
    private String id;
    private String firstName;
    private String lastName;
    private String workphone;
    private String mobilephone;
    private String email;
    private String profileUrl;
    private String profileType;
    private String im;
    private String bestMatchId;
    private String bestMatchJobId;
    private String bestMatchJobTitle;
    private String bestMatchJobRequisitionNum;
    private Double bestMatchJobScore;
    //    private String bestMatchRecruiterFirstName;
//    private String bestMatchRecruiterLastName;
    private List<String> recruitersList;
    private String bestMatchClientName;
    private String bestMatchJobCurrentState;
    //private DateTime lastUploadedDate;
    private int noOfDaysSinceLastUpload;
    private int jobMatchesCount;
    private String bestMatchJobDate;
    private String candidateStatus;
    private String currentJobTitle;
    private String currentJobCompanyName;
    private String currentJobLocation;
    private String currentJobDuration;
    private boolean isBestJobMatch;
    private String filledOrJoinedMatchId;
    private String filledOrJoinedMatchJobId;
    private String filledOrJoinedMatchJobTitle;
    private String filledOrJoinedMatchJobRequisitionNum;
    private Double filledOrJoinedMatchJobScore;
    private String filledOrJoinedMatchClientName;
    private String filledOrJoinedMatchJobCurrentState;
    private String filledOrJoinedMatchJobDate;
    private String filledOrJoinedJobState;
    private String filledDate;
    private String joinedDate;
    private Boolean isConflictPhoneNumber;
    private Boolean isConflictEmail;
    private Boolean isInCompletePhoneNumber;
    private Boolean isInCompleteEmail;
    private Integer jobMatchRank;
    private BestJobMatchJobLocationDto bestJobmatchJobLocation;
    private String lastModifiedDate;
    private Double currentJobScore;
    private boolean isAnyJobMatchFilledOrJoined;
    private AddressDTO bestJobMatchClientLocation;
    private AddressDTO joinedOrFilledJobLocation;
    private Integer joinedOrFilledJobMatchRank;
    private String availabilityDate;
    private boolean isJobLeftActionAvailable;
    private String jobLeftDate;
    private String otherSourceTypeNotes;
    private String referrerName;
    private String referrerEmail;
    private boolean isFromOtherSource;
    private boolean isReferred;
    private CandidateAdditionalDetailsCardDto candidateAdditionalDetailsCard;
    private int totalJobMatchCount;
    private boolean mobilePhoneCountryCodePresent;
    private String assignedRecruiterOfBestJobMatch;
    private boolean joinedInOtherJob;
    private boolean filledInOtherJob;
    private String vendorName;
    private boolean isCandidateFilledOrJoinedToOwnJob;
    private String bestJobMatchSharedClientName;
    private String filledOrJoinedMatchSharedClientName;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public List<String> getRecruitersList() {
        return recruitersList;
    }

    //    public String getBestMatchRecruiterFirstName() {
//        return bestMatchRecruiterFirstName;
//    }
//
//    public void setBestMatchRecruiterFirstName(String bestMatchRecruiterFirstName) {
//        this.bestMatchRecruiterFirstName = bestMatchRecruiterFirstName;
//    }
//
//    public String getBestMatchRecruiterLastName() {
//        return bestMatchRecruiterLastName;
//    }
//
//    public void setBestMatchRecruiterLastName(String bestMatchRecruiterLastName) {
//        this.bestMatchRecruiterLastName = bestMatchRecruiterLastName;
//    }

    public void setRecruitersList(List<String> recruitersList) {
        this.recruitersList = recruitersList;
    }

    public String getBestMatchClientName() {
        return bestMatchClientName;
    }

    public void setBestMatchClientName(String bestMatchClientName) {
        this.bestMatchClientName = bestMatchClientName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getWorkphone() {
        return workphone;
    }

    public void setWorkphone(String workphone) {
        this.workphone = workphone;
    }

    public String getMobilephone() {
        return mobilephone;
    }

    public void setMobilephone(String mobilephone) {
        this.mobilephone = mobilephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIm() {
        return im;
    }

    public void setIm(String im) {
        this.im = im;
    }

    public String getBestMatchId() {
        return bestMatchId;
    }

    public void setBestMatchId(String bestMatchId) {
        this.bestMatchId = bestMatchId;
    }

    public String getBestMatchJobId() {
        return bestMatchJobId;
    }

    public void setBestMatchJobId(String bestMatchJobId) {
        this.bestMatchJobId = bestMatchJobId;
    }

    public String getBestMatchJobTitle() {
        return bestMatchJobTitle;
    }

    public void setBestMatchJobTitle(String bestMatchJobTitle) {
        this.bestMatchJobTitle = bestMatchJobTitle;
    }

    public Double getBestMatchJobScore() {
        return bestMatchJobScore;
    }

    public void setBestMatchJobScore(Double bestMatchJobScore) {
        this.bestMatchJobScore = bestMatchJobScore;
    }

    public String getBestMatchJobCurrentState() {
        return bestMatchJobCurrentState;
    }

    public void setBestMatchJobCurrentState(String bestMatchJobCurrentState) {
        this.bestMatchJobCurrentState = bestMatchJobCurrentState;
    }

    public String getBestMatchJobRequisitionNum() {
        return bestMatchJobRequisitionNum;
    }

    public void setBestMatchJobRequisitionNum(String bestMatchJobRequisitionNum) {
        this.bestMatchJobRequisitionNum = bestMatchJobRequisitionNum;
    }

    //    public DateTime getLastUploadedDate() {
//        return lastUploadedDate;
//    }
//
//    public void setLastUploadedDate(DateTime lastUploadedDate) {
//        this.lastUploadedDate = lastUploadedDate;
//    }
//
    public String getBestMatchJobDate() {
        return bestMatchJobDate;
    }

    public void setBestMatchJobDate(String bestMatchJobDate) {
        this.bestMatchJobDate = bestMatchJobDate;
    }


    public int getJobMatchesCount() {
        return jobMatchesCount;
    }

    public void setJobMatchesCount(int jobMatchesCount) {
        this.jobMatchesCount = jobMatchesCount;
    }

    public String getCandidateStatus() {
        return candidateStatus;
    }

    public void setCandidateStatus(String candidateStatus) {
        this.candidateStatus = candidateStatus;
    }

    public int getNoOfDaysSinceLastUpload() {
        return noOfDaysSinceLastUpload;
    }

    public void setNoOfDaysSinceLastUpload(int noOfDaysSinceLastUpload) {
        this.noOfDaysSinceLastUpload = noOfDaysSinceLastUpload;
    }

    public String getCurrentJobTitle() {
        return currentJobTitle;
    }

    public void setCurrentJobTitle(String currentJobTitle) {
        this.currentJobTitle = currentJobTitle;
    }

    public String getCurrentJobCompanyName() {
        return currentJobCompanyName;
    }

    public void setCurrentJobCompanyName(String currentJobCompanyName) {
        this.currentJobCompanyName = currentJobCompanyName;
    }

    public String getCurrentJobLocation() {
        return currentJobLocation;
    }

    public void setCurrentJobLocation(String currentJobLocation) {
        this.currentJobLocation = currentJobLocation;
    }

    public String getCurrentJobDuration() {
        return currentJobDuration;
    }

    public void setCurrentJobDuration(String currentJobDuration) {
        this.currentJobDuration = currentJobDuration;
    }

    public boolean isBestJobMatch() {
        return isBestJobMatch;
    }

    public void setBestJobMatch(boolean bestJobMatch) {
        isBestJobMatch = bestJobMatch;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public String getFilledOrJoinedMatchId() {
        return filledOrJoinedMatchId;
    }

    public void setFilledOrJoinedMatchId(String filledOrJoinedMatchId) {
        this.filledOrJoinedMatchId = filledOrJoinedMatchId;
    }

    public String getFilledOrJoinedMatchJobId() {
        return filledOrJoinedMatchJobId;
    }

    public void setFilledOrJoinedMatchJobId(String filledOrJoinedMatchJobId) {
        this.filledOrJoinedMatchJobId = filledOrJoinedMatchJobId;
    }

    public String getFilledOrJoinedMatchJobTitle() {
        return filledOrJoinedMatchJobTitle;
    }

    public void setFilledOrJoinedMatchJobTitle(String filledOrJoinedMatchJobTitle) {
        this.filledOrJoinedMatchJobTitle = filledOrJoinedMatchJobTitle;
    }

    public String getFilledOrJoinedMatchJobRequisitionNum() {
        return filledOrJoinedMatchJobRequisitionNum;
    }

    public void setFilledOrJoinedMatchJobRequisitionNum(String filledOrJoinedMatchJobRequisitionNum) {
        this.filledOrJoinedMatchJobRequisitionNum = filledOrJoinedMatchJobRequisitionNum;
    }

    public Double getFilledOrJoinedMatchJobScore() {
        return filledOrJoinedMatchJobScore;
    }

    public void setFilledOrJoinedMatchJobScore(Double filledOrJoinedMatchJobScore) {
        this.filledOrJoinedMatchJobScore = filledOrJoinedMatchJobScore;
    }

    public String getFilledOrJoinedMatchClientName() {
        return filledOrJoinedMatchClientName;
    }

    public void setFilledOrJoinedMatchClientName(String filledOrJoinedMatchClientName) {
        this.filledOrJoinedMatchClientName = filledOrJoinedMatchClientName;
    }

    public String getFilledOrJoinedMatchJobCurrentState() {
        return filledOrJoinedMatchJobCurrentState;
    }

    public void setFilledOrJoinedMatchJobCurrentState(String filledOrJoinedMatchJobCurrentState) {
        this.filledOrJoinedMatchJobCurrentState = filledOrJoinedMatchJobCurrentState;
    }

    public String getFilledOrJoinedMatchJobDate() {
        return filledOrJoinedMatchJobDate;
    }

    public void setFilledOrJoinedMatchJobDate(String filledOrJoinedMatchJobDate) {
        this.filledOrJoinedMatchJobDate = filledOrJoinedMatchJobDate;
    }

    public String getFilledOrJoinedJobState() {
        return filledOrJoinedJobState;
    }

    public void setFilledOrJoinedJobState(String filledOrJoinedJobState) {
        this.filledOrJoinedJobState = filledOrJoinedJobState;
    }

    public String getFilledDate() {
        return filledDate;
    }

    public void setFilledDate(String filledDate) {
        this.filledDate = filledDate;
    }

    public String getJoinedDate() {
        return joinedDate;
    }

    public void setJoinedDate(String joinedDate) {
        this.joinedDate = joinedDate;
    }

    public Boolean getConflictPhoneNumber() {
        return isConflictPhoneNumber;
    }

    public void setConflictPhoneNumber(Boolean conflictPhoneNumber) {
        isConflictPhoneNumber = conflictPhoneNumber;
    }

    public Boolean getConflictEmail() {
        return isConflictEmail;
    }

    public void setConflictEmail(Boolean conflictEmail) {
        isConflictEmail = conflictEmail;
    }

    public Boolean getInCompletePhoneNumber() {
        return isInCompletePhoneNumber;
    }

    public void setInCompletePhoneNumber(Boolean inCompletePhoneNumber) {
        isInCompletePhoneNumber = inCompletePhoneNumber;
    }

    public Boolean getInCompleteEmail() {
        return isInCompleteEmail;
    }

    public void setInCompleteEmail(Boolean inCompleteEmail) {
        isInCompleteEmail = inCompleteEmail;
    }

    public String getProfileType() {
        return profileType;
    }

    public void setProfileType(String profileType) {
        this.profileType = profileType;
    }

    public Integer getJobMatchRank() {
        return jobMatchRank;
    }

    public void setJobMatchRank(Integer jobMatchRank) {
        this.jobMatchRank = jobMatchRank;
    }

    public BestJobMatchJobLocationDto getBestJobmatchJobLocation() {
        return bestJobmatchJobLocation;
    }

    public void setBestJobmatchJobLocation(BestJobMatchJobLocationDto bestJobmatchJobLocation) {
        this.bestJobmatchJobLocation = bestJobmatchJobLocation;
    }

    public String getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(String lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Double getCurrentJobScore() {
        return currentJobScore;
    }

    public void setCurrentJobScore(Double currentJobScore) {
        this.currentJobScore = currentJobScore;
    }

    public boolean isAnyJobMatchFilledOrJoined() {
        return isAnyJobMatchFilledOrJoined;
    }

    public void setAnyJobMatchFilledOrJoined(boolean anyJobMatchFilledOrJoined) {
        isAnyJobMatchFilledOrJoined = anyJobMatchFilledOrJoined;
    }

    public AddressDTO getBestJobMatchClientLocation() {
        return bestJobMatchClientLocation;
    }

    public void setBestJobMatchClientLocation(AddressDTO bestJobMatchClientLocation) {
        this.bestJobMatchClientLocation = bestJobMatchClientLocation;
    }

    public AddressDTO getJoinedOrFilledJobLocation() {
        return joinedOrFilledJobLocation;
    }

    public void setJoinedOrFilledJobLocation(AddressDTO joinedOrFilledJobLocation) {
        this.joinedOrFilledJobLocation = joinedOrFilledJobLocation;
    }

    public Integer getJoinedOrFilledJobMatchRank() {
        return joinedOrFilledJobMatchRank;
    }

    public void setJoinedOrFilledJobMatchRank(Integer joinedOrFilledJobMatchRank) {
        this.joinedOrFilledJobMatchRank = joinedOrFilledJobMatchRank;
    }

    public String getAvailabilityDate() {
        return availabilityDate;
    }

    public void setAvailabilityDate(String availabilityDate) {
        this.availabilityDate = availabilityDate;
    }

    public boolean isJobLeftActionAvailable() {
        return isJobLeftActionAvailable;
    }

    public void setJobLeftActionAvailable(boolean jobLeftActionAvailable) {
        isJobLeftActionAvailable = jobLeftActionAvailable;
    }

    public String getJobLeftDate() {
        return jobLeftDate;
    }

    public void setJobLeftDate(String jobLeftDate) {
        this.jobLeftDate = jobLeftDate;
    }

    public String getOtherSourceTypeNotes() {
        return otherSourceTypeNotes;
    }

    public void setOtherSourceTypeNotes(String otherSourceTypeNotes) {
        this.otherSourceTypeNotes = otherSourceTypeNotes;
    }

    public String getReferrerName() {
        return referrerName;
    }

    public void setReferrerName(String referrerName) {
        this.referrerName = referrerName;
    }

    public String getReferrerEmail() {
        return referrerEmail;
    }

    public void setReferrerEmail(String referrerEmail) {
        this.referrerEmail = referrerEmail;
    }

    public boolean isFromOtherSource() {
        return isFromOtherSource;
    }

    public void setFromOtherSource(boolean fromOtherSource) {
        isFromOtherSource = fromOtherSource;
    }

    public boolean isReferred() {
        return isReferred;
    }

    public void setReferred(boolean referred) {
        isReferred = referred;
    }

    public CandidateAdditionalDetailsCardDto getCandidateAdditionalDetailsCard() {
        return candidateAdditionalDetailsCard;
    }

    public void setCandidateAdditionalDetailsCard(CandidateAdditionalDetailsCardDto candidateAdditionalDetailsCard) {
        this.candidateAdditionalDetailsCard = candidateAdditionalDetailsCard;
    }

    public int getTotalJobMatchCount() {
        return totalJobMatchCount;
    }

    public void setTotalJobMatchCount(int totalJobMatchCount) {
        this.totalJobMatchCount = totalJobMatchCount;
    }

    public boolean getMobilePhoneCountryCodePresent() {
        return mobilePhoneCountryCodePresent;
    }

    public void setMobilePhoneCountryCodePresent(boolean mobilePhoneCountryCodePresent) {
        this.mobilePhoneCountryCodePresent = mobilePhoneCountryCodePresent;
    }

    public String getAssignedRecruiterOfBestJobMatch() {
        return assignedRecruiterOfBestJobMatch;
    }

    public void setAssignedRecruiterOfBestJobMatch(String assignedRecruiterOfBestJobMatch) {
        this.assignedRecruiterOfBestJobMatch = assignedRecruiterOfBestJobMatch;
    }

    public boolean isJoinedInOtherJob() {
        return joinedInOtherJob;
    }

    public void setJoinedInOtherJob(boolean joinedInOtherJob) {
        this.joinedInOtherJob = joinedInOtherJob;
    }

    public boolean isFilledInOtherJob() {
        return filledInOtherJob;
    }

    public void setFilledInOtherJob(boolean filledInOtherJob) {
        this.filledInOtherJob = filledInOtherJob;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public boolean isCandidateFilledOrJoinedToOwnJob() {
        return isCandidateFilledOrJoinedToOwnJob;
    }

    public void setCandidateFilledOrJoinedToOwnJob(boolean candidateFilledOrJoinedToOwnJob) {
        isCandidateFilledOrJoinedToOwnJob = candidateFilledOrJoinedToOwnJob;
    }

    public String getBestJobMatchSharedClientName() {
        return bestJobMatchSharedClientName;
    }

    public void setBestJobMatchSharedClientName(String bestJobMatchSharedClientName) {
        this.bestJobMatchSharedClientName = bestJobMatchSharedClientName;
    }

    public String getFilledOrJoinedMatchSharedClientName() {
        return filledOrJoinedMatchSharedClientName;
    }

    public void setFilledOrJoinedMatchSharedClientName(String filledOrJoinedMatchSharedClientName) {
        this.filledOrJoinedMatchSharedClientName = filledOrJoinedMatchSharedClientName;
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "CandidateCardDto{" +
                "id='" + id + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", workphone='" + workphone + '\'' +
                ", mobilephone='" + mobilephone + '\'' +
                ", email='" + email + '\'' +
                ", im='" + im + '\'' +

                ", bestMatchJobTitle='" + bestMatchJobTitle + '\'' +
                ", bestMatchJobScore='" + bestMatchJobScore + '\'' +
//                ", bestMatchRecruiterFirstName='" + bestMatchRecruiterFirstName + '\'' +
//                ", bestMatchRecruiterLastName='" + bestMatchRecruiterLastName + '\'' +
//                ", companyName='" + companyName + '\'' +
                ", bestMatchJobCurrentState='" + bestMatchJobCurrentState + '\'' +
                ", jobMatchRank='" + jobMatchRank + '\'' +
                ", lastModifiedDate='" + lastModifiedDate + '\'' +
                ", currentJobScore='" + currentJobScore + '\'' +
                '}';
    }
}
