package com.fourdotfive.missioncontrol.pojo.candidate;

import java.util.List;

import com.fourdotfive.missioncontrol.pojo.job.JobStateType;

public class CandidateCardFilterParams {
    private List<String> status;
    private String[] clientOrBuList;
    private String[] recruiterList;
    private String[] roleList;
    private List<String> requisitionStatus;
    private List<String> workflowStates;
    private String[] requisitionNumbers;
    private String parentCompanyId;
    private String loggedInUserId;
    private List<String> userIds;
    private CandidateType candidateType;
    private List<String> vendorIds;


    public String getLoggedInUserId() {
        return loggedInUserId;
    }

    public void setLoggedInUserId(String loggedInUserId) {
        this.loggedInUserId = loggedInUserId;
    }

    public String getParentCompanyId() {
        return parentCompanyId;
    }

    public void setParentCompanyId(String parentCompanyId) {
        this.parentCompanyId = parentCompanyId;
    }

    public List<String> getStatus() {
        return status;
    }

    public void setStatus(List<String> status) {
        this.status = status;
    }

    public String[] getClientOrBuList() {
        return clientOrBuList;
    }

    public void setClientOrBuList(String[] clientOrBuList) {
        this.clientOrBuList = clientOrBuList;
    }

    public String[] getRecruiterList() {
        return recruiterList;
    }

    public void setRecruiterList(String[] recruiterList) {
        this.recruiterList = recruiterList;
    }

    public String[] getRoleList() {
        return roleList;
    }

    public void setRoleList(String[] roleList) {
        this.roleList = roleList;
    }

    public List<String> getRequisitionStatus() {
        return requisitionStatus;
    }

    public void setRequisitionStatus(List<String> requisitionStatus) {
        this.requisitionStatus = requisitionStatus;
    }

    public String[] getRequisitionNumbers() {
        return requisitionNumbers;
    }

    public void setRequisitionNumbers(String[] requisitionNumbers) {
        this.requisitionNumbers = requisitionNumbers;
    }

    public List<String> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<String> userIds) {
        this.userIds = userIds;
    }

    public CandidateType getCandidateType() {
        return candidateType;
    }

    public void setCandidateType(CandidateType candidateType) {
        this.candidateType = candidateType;
    }

    public List<String> getVendorIds() {
        return vendorIds;
    }

    public void setVendorIds(List<String> vendorIds) {
        this.vendorIds = vendorIds;
    }

    public List<String> getWorkflowStates() {
        return workflowStates;
    }

    public void setWorkflowStates(List<String> workflowStates) {
        this.workflowStates = workflowStates;
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "CandidateCardFilterParams{" +
                "status=" + status +
                ", clientOrBuList=" + java.util.Arrays.toString(clientOrBuList) +
                ", recruiterList=" + java.util.Arrays.toString(recruiterList) +
                ", roleList=" + java.util.Arrays.toString(roleList) +
                ", requisitionStatus=" + requisitionStatus +
                ", requisitionNumbers=" + java.util.Arrays.toString(requisitionNumbers) +
                ", parentCompanyId='" + parentCompanyId + '\'' +
                ", loggedInUserId='" + loggedInUserId + '\'' +
                '}';
    }
}