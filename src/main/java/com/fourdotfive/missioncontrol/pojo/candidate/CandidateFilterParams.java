package com.fourdotfive.missioncontrol.pojo.candidate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CandidateFilterParams {
	private List<CandidateSearchStatus> status;
	private String[] clientOrBuList;
	private String[] recruiterList;
	private String[] roleList;
	private List<String> requisitionStatus;
	private String[] requisitionNumbers;
	private String firstName;
	private String lastName;
	private String phone;
	private String email;
	private String workflowState;
	private String fromDate;
	private String toDate;
	private String searchString;
	private int pageNum = 1;
	private int pageSize = 20;
	private String column;
	private String sortDirection;
	private List<String> users;
	private String parentCompanyId;
	private String loggedInUserId;
	private ArrayList<String> visibility = new ArrayList<String>();
	private ArrayList<String> workflowStates = new ArrayList<>();
	private ArrayList<String> workflowSteps = new ArrayList<>();
    private CandidateType candidateType;
    private List<String> vendors;


    public String getLoggedInUserId() {
		return loggedInUserId;
	}

	public void setLoggedInUserId(String loggedInUserId) {
		this.loggedInUserId = loggedInUserId;
	}

	public String getParentCompanyId() {
		return parentCompanyId;
	}

	public void setParentCompanyId(String parentCompanyId) {
		this.parentCompanyId = parentCompanyId;
	}

	public List<String> getUsers() {
		return users;
	}

	public void setUsers(List<String> users) {
		this.users = users;
	}

	public List<CandidateSearchStatus> getStatus() {
		return status;
	}

	public void setStatus(List<CandidateSearchStatus> status) {
		this.status = status;
	}

	public String[] getClientOrBuList() {
		return clientOrBuList;
	}

	public void setClientOrBuList(String[] clientOrBuList) {
		this.clientOrBuList = clientOrBuList;
	}

	public String[] getRecruiterList() {
		return recruiterList;
	}

	public void setRecruiterList(String[] recruiterList) {
		this.recruiterList = recruiterList;
	}

	public String[] getRoleList() {
		return roleList;
	}

	public void setRoleList(String[] roleList) {
		this.roleList = roleList;
	}

	public List<String> getRequisitionStatus() {
		return requisitionStatus;
	}

	public void setRequisitionStatus(List<String> requisitionStatus) {
		this.requisitionStatus = requisitionStatus;
	}

	public String[] getRequisitionNumbers() {
		return requisitionNumbers;
	}

	public void setRequisitionNumbers(String[] requisitionNumbers) {
		this.requisitionNumbers = requisitionNumbers;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWorkflowState() {
		return workflowState;
	}

	public void setWorkflowState(String workflowState) {
		this.workflowState = workflowState;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}

	public String getSortDirection() {
		return sortDirection;
	}

	public void setSortDirection(String sortDirection) {
		this.sortDirection = sortDirection;
	}

	public ArrayList<String> getVisibility() {
		return visibility;
	}

	public void setVisibility(ArrayList<String> visibility) {
		this.visibility = visibility;
	}

	public ArrayList<String> getWorkflowStates() {
		return workflowStates;
	}

	public void setWorkflowStates(ArrayList<String> workflowStates) {
		this.workflowStates = workflowStates;
	}

	public ArrayList<String> getWorkflowSteps() {
		return workflowSteps;
	}

	public void setWorkflowSteps(ArrayList<String> workflowSteps) {
		this.workflowSteps = workflowSteps;
	}

	public CandidateType getCandidateType() {
		return candidateType;
	}

	public void setCandidateType(CandidateType candidateType) {
		this.candidateType = candidateType;
	}

	public List<String> getVendors() {
		return vendors;
	}

	public void setVendors(List<String> vendors) {
		this.vendors = vendors;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CandidateAndJobFilterParams [status=");

		builder.append(status);
		builder.append(", clientOrBuList=");
		builder.append(Arrays.toString(clientOrBuList));
		builder.append(", recruiterList=");
		builder.append(Arrays.toString(recruiterList));
		builder.append(", roleList=");
		builder.append(Arrays.toString(roleList));
		builder.append(", requisitionStatus=");
		builder.append(requisitionStatus);
		builder.append(", requisitionNumbers=");
		builder.append(Arrays.toString(requisitionNumbers));
		builder.append(", firstName=");
		builder.append(firstName);
		builder.append(", lastName=");
		builder.append(lastName);
		builder.append(", phone=");
		builder.append(phone);
		builder.append(", email=");
		builder.append(email);
		builder.append(", workflowState=");
		builder.append(workflowState);
		builder.append(", fromDate=");
		builder.append(fromDate);
		builder.append(", toDate=");
		builder.append(toDate);
		builder.append(", searchString=");
		builder.append(searchString);
		builder.append("]");
		return builder.toString();
	}
}