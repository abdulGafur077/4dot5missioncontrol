package com.fourdotfive.missioncontrol.pojo.candidate;


public enum CandidateSearchStatus {

	Active ("ACTIVE"),Passive("PASSIVE"),Inert("INERT");
	
	private String text;

	CandidateSearchStatus(String text) {
		this.text = text;
	}

	public String getText() {
		return this.text;
	}

	public static CandidateSearchStatus fromString(String text) {
		for (CandidateSearchStatus b : CandidateSearchStatus.values()) {
			if (b.text.equalsIgnoreCase(text)) {
				return b;
			}
		}
		return null;
	}
}