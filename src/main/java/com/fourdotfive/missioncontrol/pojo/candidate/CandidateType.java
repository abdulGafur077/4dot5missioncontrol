package com.fourdotfive.missioncontrol.pojo.candidate;

public enum CandidateType {

    VENDOR_CANDIDATES, OWN_CANDIDATES
}
