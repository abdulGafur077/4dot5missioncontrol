package com.fourdotfive.missioncontrol.pojo.candidate;

/**
 * @author Shalini
 */
public class Contact {
	
	private String email;
	
	private String firstname;
	
	private String im;
	
	private boolean isPrimary;
	
	private String lastname;
	
	private String mobilephone;
	
	private String workphone;
	
	public Contact() {
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getIm() {
		return im;
	}

	public void setIm(String im) {
		this.im = im;
	}

	public boolean isPrimary() {
		return isPrimary;
	}

	public void setPrimary(boolean isPrimary) {
		this.isPrimary = isPrimary;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getMobilephone() {
		return mobilephone;
	}

	public void setMobilephone(String mobilephone) {
		this.mobilephone = mobilephone;
	}

	public String getWorkphone() {
		return workphone;
	}

	public void setWorkphone(String workphone) {
		this.workphone = workphone;
	}

	@Override
	public String toString() {
		return "Contact [email=" + email + ", firstname=" + firstname + ", im=" + im + ", isPrimary=" + isPrimary
				+ ", lastname=" + lastname + ", mobilephone=" + mobilephone + ", workphone=" + workphone + "]";
	}
	
}
