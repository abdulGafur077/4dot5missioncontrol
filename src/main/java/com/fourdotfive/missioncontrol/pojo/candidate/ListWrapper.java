package com.fourdotfive.missioncontrol.pojo.candidate;

import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;

public class ListWrapper {

	private List<String> jobRequistionIds;

	private List<String> recruiterIds;

	private List<String> clientOrBuIds;

	private List<String> candidateActiveStateTypes;

	private List<String> candidatePassiveStateTypes;

	private List<String> candidateIds;

	private Date fromDate;

	private Date toDate;

	private DateTime fDate;

	private DateTime tDate;

	private String fName;

	private String lName;

	private String email;

	private String phoneNumber;

	private String requistionNumber;

	private List<String> reqStateType;

	public List<String> getJobRequistionIds() {
		return jobRequistionIds;
	}

	public void setJobRequistionIds(List<String> jobRequistionIds) {
		this.jobRequistionIds = jobRequistionIds;
	}

	public List<String> getRecruiterIds() {
		return recruiterIds;
	}

	public void setRecruiterIds(List<String> recruiterIds) {
		this.recruiterIds = recruiterIds;
	}

	public List<String> getClientOrBuIds() {
		return clientOrBuIds;
	}

	public void setClientOrBuIds(List<String> clientOrBuIds) {
		this.clientOrBuIds = clientOrBuIds;
	}

	public List<String> getCandidateActiveStateTypes() {
		return candidateActiveStateTypes;
	}

	public void setCandidateActiveStateTypes(
			List<String> candidateActiveStateTypes) {
		this.candidateActiveStateTypes = candidateActiveStateTypes;
	}

	public List<String> getCandidatePassiveStateTypes() {
		return candidatePassiveStateTypes;
	}

	public void setCandidatePassiveStateTypes(
			List<String> candidatePassiveStateTypes) {
		this.candidatePassiveStateTypes = candidatePassiveStateTypes;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public DateTime getfDate() {
		return fDate;
	}

	public void setfDate(DateTime fDate) {
		this.fDate = fDate;
	}

	public DateTime gettDate() {
		return tDate;
	}

	public void settDate(DateTime tDate) {
		this.tDate = tDate;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getRequistionNumber() {
		return requistionNumber;
	}

	public void setRequistionNumber(String requistionNumber) {
		this.requistionNumber = requistionNumber;
	}

	public List<String> getCandidateIds() {
		return candidateIds;
	}

	public void setCandidateIds(List<String> candidateIds) {
		this.candidateIds = candidateIds;
	}

	public List<String> getReqStateType() {
		return reqStateType;
	}

	public void setReqStateType(List<String> reqStateType) {
		this.reqStateType = reqStateType;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ListWrapper [jobRequistionIds=")
				.append(jobRequistionIds).append(", recruiterIds=")
				.append(recruiterIds).append(", clientOrBuIds=")
				.append(clientOrBuIds).append(", candidateActiveStateTypes=")
				.append(candidateActiveStateTypes)
				.append(", candidatePassiveStateTypes=")
				.append(candidatePassiveStateTypes).append(", candidateIds=")
				.append(candidateIds).append(", fromDate=").append(fromDate)
				.append(", toDate=").append(toDate).append(", fDate=")
				.append(fDate).append(", tDate=").append(tDate)
				.append(", fName=").append(fName).append(", lName=")
				.append(lName).append(", email=").append(email)
				.append(", phoneNumber=").append(phoneNumber)
				.append(", requistionNumber=").append(requistionNumber)
				.append(", reqStateType=").append(reqStateType).append("]");
		return builder.toString();
	}

}
