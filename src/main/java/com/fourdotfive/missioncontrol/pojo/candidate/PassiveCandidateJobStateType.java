package com.fourdotfive.missioncontrol.pojo.candidate;

public enum PassiveCandidateJobStateType {
	SOFT_QUALIFIED, IN_OUTREACH, CONTACTED, RESPONDED, ACTIVATED 
}
