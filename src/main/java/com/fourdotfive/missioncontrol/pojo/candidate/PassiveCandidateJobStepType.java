package com.fourdotfive.missioncontrol.pojo.candidate;

public enum PassiveCandidateJobStepType {

SOFT_QUALIFIED, IN_OUTREACH, CONTACTED, RESPONDED, NOT_INTERESTED, ACTIVATED, RELEASED

}
