package com.fourdotfive.missioncontrol.pojo.candidate;

import com.fourdotfive.missioncontrol.pojo.FourDotFiveDate;
import com.fourdotfive.missioncontrol.pojo.job.Certifications;
import com.fourdotfive.missioncontrol.pojo.job.Education;
import com.fourdotfive.missioncontrol.pojo.job.Experience;
import com.fourdotfive.missioncontrol.pojo.job.Skill;

import java.util.List;

public class Profile {

	private List<Certifications> certifications;

	private String createdBy;

	private FourDotFiveDate createdDate;

	private List<Education> educationList;

	private List<Experience> experienceList;

	private String id;

	private String lastModifiedBy;

	private FourDotFiveDate lastModifiedDate;

	private boolean isNew;

	private List<Skill> operationalSkills;

	private List<Skill> softSkills;

	private List<Skill> technicalSkills;

	private int version;

	public List<Certifications> getCertifications() {
		return certifications;
	}

	public void setCertifications(List<Certifications> certifications) {
		this.certifications = certifications;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public FourDotFiveDate getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(FourDotFiveDate createdDate) {
		this.createdDate = createdDate;
	}

	public List<Education> getEducationList() {
		return educationList;
	}

	public void setEducationList(List<Education> educationList) {
		this.educationList = educationList;
	}

	public List<Experience> getExperienceList() {
		return experienceList;
	}

	public void setExperienceList(List<Experience> experienceList) {
		this.experienceList = experienceList;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public FourDotFiveDate getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(FourDotFiveDate lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

	public List<Skill> getOperationalSkills() {
		return operationalSkills;
	}

	public void setOperationalSkills(List<Skill> operationalSkills) {
		this.operationalSkills = operationalSkills;
	}

	public List<Skill> getSoftSkills() {
		return softSkills;
	}

	public void setSoftSkills(List<Skill> softSkills) {
		this.softSkills = softSkills;
	}

	public List<Skill> getTechnicalSkills() {
		return technicalSkills;
	}

	public void setTechnicalSkills(List<Skill> technicalSkills) {
		this.technicalSkills = technicalSkills;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Profile [certifications=").append(certifications)
				.append(", createdBy=").append(createdBy)
				.append(", createdDate=").append(createdDate)
				.append(", educationList=").append(educationList)
				.append(", experienceList=").append(experienceList)
				.append(", id=").append(id).append(", lastModifiedBy=")
				.append(lastModifiedBy).append(", lastModifiedDate=")
				.append(lastModifiedDate).append(", isNew=").append(isNew)
				.append(", operationalSkills=").append(operationalSkills)
				.append(", softSkills=").append(softSkills)
				.append(", technicalSkills=").append(technicalSkills)
				.append(", version=").append(version).append("]");
		return builder.toString();
	}
}
