package com.fourdotfive.missioncontrol.pojo.company;

import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fourdotfive.missioncontrol.licensepreference.LicensePreferences;
import com.fourdotfive.missioncontrol.pojo.FourDotFiveDate;
import com.fourdotfive.missioncontrol.pojo.user.Address;
import com.fourdotfive.missioncontrol.pojo.user.Contact;
import com.fourdotfive.missioncontrol.pojo.user.EmployerRanking;
import com.fourdotfive.missioncontrol.pojo.user.Location;
import com.fourdotfive.missioncontrol.pojo.user.User;

public class Company {

	private Address address;

	private String companyLogoURL;

	private CompanyType companyType;

	private String description;

	private String id;

	private String name;

	@JsonProperty("new")
	private boolean isNew;

	private String shortName;

	private List<Contact> contactList;

	private List<EmployerRanking> employerRankingList;

	private LicensePreferences licensePreferences;

	private List<Location> locationList;

	private List<User> recruiterList;

	private List<Industry> industryList;

	private List<Industry> servingIndustryList;

	private List<RoleEntityScope> scopeList;

	private String companyState;

	private User fourdotfiveadmin;

	private String createdUser;

	private FourDotFiveDate createdDate;

	private DateTime createdDateTime;

	private String lastModifiedUser;

	private FourDotFiveDate lastModifiedDate;

	private DateTime modifiedDateTime;

	private List<String> questionList;

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public String getLastModifiedUser() {
		return lastModifiedUser;
	}

	public void setLastModifiedUser(String lastModifiedUser) {
		this.lastModifiedUser = lastModifiedUser;
	}

	public String getCompanyState() {
		return companyState;
	}

	public void setCompanyState(String companyState) {
		this.companyState = companyState;
	}

	public User getFourdotfiveadmin() {
		return fourdotfiveadmin;
	}

	public void setFourdotfiveadmin(User fourdotfiveadmin) {
		this.fourdotfiveadmin = fourdotfiveadmin;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getCompanyLogoURL() {
		return companyLogoURL;
	}

	public void setCompanyLogoURL(String companyLogoURL) {
		this.companyLogoURL = companyLogoURL;
	}

	public CompanyType getCompanyType() {
		return companyType;
	}

	public void setCompanyType(CompanyType companyType) {
		this.companyType = companyType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<String> getQuestionList() {
		return questionList;
	}

	public void setQuestionList(List<String> questionList) {
		this.questionList = questionList;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public List<Contact> getContactList() {
		return contactList;
	}

	public void setContactList(List<Contact> contactList) {
		this.contactList = contactList;
	}

	public List<EmployerRanking> getEmployerRankingList() {
		return employerRankingList;
	}

	public void setEmployerRankingList(List<EmployerRanking> employerRankingList) {
		this.employerRankingList = employerRankingList;
	}

	public LicensePreferences getLicensePreferences() {
		return licensePreferences;
	}

	public void setLicensePreferences(LicensePreferences licensePreferences) {
		this.licensePreferences = licensePreferences;
	}

	public List<Location> getLocationList() {
		return locationList;
	}

	public void setLocationList(List<Location> locationList) {
		this.locationList = locationList;
	}

	public List<User> getRecruiterList() {
		return recruiterList;
	}

	public void setRecruiterList(List<User> recruiterList) {
		this.recruiterList = recruiterList;
	}

	public List<Industry> getServingIndustryList() {
		return servingIndustryList;
	}

	public void setServingIndustryList(List<Industry> servingIndustryList) {
		this.servingIndustryList = servingIndustryList;
	}

	public List<RoleEntityScope> getScopeList() {
		return scopeList;
	}

	public void setScopeList(List<RoleEntityScope> scopeList) {
		this.scopeList = scopeList;
	}

	public List<Industry> getIndustryList() {
		return industryList;
	}

	public void setIndustryList(List<Industry> industryList) {
		this.industryList = industryList;
	}

	public void setCreatedDate(FourDotFiveDate date) {
		this.createdDate = date;
		if (date != null) {
			DateTime dateTime = new DateTime(date.getYear(),
					date.getMonthOfYear(), date.getDayOfMonth(),
					date.getHourOfDay(), date.getMinuteOfHour(),
					date.getSecondOfMinute(), date.getMillisOfSecond(),
					DateTimeZone.UTC);
			setCreatedDateTime(dateTime);
		}
	}

	public void setLastModifiedDate(FourDotFiveDate date) {
		this.lastModifiedDate = date;
		if (date != null) {
			DateTime dateTime = new DateTime(date.getYear(),
					date.getMonthOfYear(), date.getDayOfMonth(),
					date.getHourOfDay(), date.getMinuteOfHour(),
					date.getSecondOfMinute(), date.getMillisOfSecond(),
					DateTimeZone.UTC);
			setModifiedDateTime(dateTime);
		}
	}

	public DateTime getCreatedDateTime() {
		return createdDateTime;
	}

	public FourDotFiveDate getCreatedDate() {
		return createdDate;
	}

	public FourDotFiveDate getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setCreatedDateTime(DateTime createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public DateTime getModifiedDateTime() {
		return modifiedDateTime;
	}

	public void setModifiedDateTime(DateTime modifiedDateTime) {
		this.modifiedDateTime = modifiedDateTime;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Company [address=").append(address)
				.append(", companyLogoURL=").append(companyLogoURL)
				.append(", companyType=").append(companyType)
				.append(", description=").append(description).append(", id=")
				.append(id).append(", name=").append(name).append(", isNew=")
				.append(isNew).append(", shortName=").append(shortName)
				.append(", contactList=").append(contactList)
				.append(", employerRankingList=").append(employerRankingList)
				.append(", licensePreferences=").append(licensePreferences)
				.append(", locationList=").append(locationList)
				.append(", recruiterList=").append(recruiterList)
				.append(", industryList=").append(industryList)
				.append(", servingIndustryList=").append(servingIndustryList)
				.append(", scopeList=").append(scopeList)
				.append(", companyState=").append(companyState)
				.append(", fourdotfiveadmin=").append(fourdotfiveadmin)
				.append(", createdUser=").append(createdUser)
				.append(", lastModifiedUser=").append(lastModifiedUser)
				.append("]").append(", createdDate=").append(createdDate)
				.append(", lastModifiedDate=").append(lastModifiedDate)
				.append(", questionList=").append(questionList);
		return builder.toString();
	}

}
