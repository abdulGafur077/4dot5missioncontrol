package com.fourdotfive.missioncontrol.pojo.company;

import java.util.List;

public class CompanyList {

	private List<Company> content;

	private boolean last;

	private int totalPages;

	private int totalElements;

	private int numberOfElements;

	private boolean first;

	private String direction;

	private int size;

	private int number;

	public List<Company> getContent() {
		return content;
	}

	public void setContent(List<Company> content) {
		this.content = content;
	}

	public boolean isLast() {
		return last;
	}

	public void setLast(boolean last) {
		this.last = last;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public int getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(int totalElements) {
		this.totalElements = totalElements;
	}

	public int getNumberOfElements() {
		return numberOfElements;
	}

	public void setNumberOfElements(int numberOfElements) {
		this.numberOfElements = numberOfElements;
	}

	public boolean isFirst() {
		return first;
	}

	public void setFirst(boolean first) {
		this.first = first;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CompanyList [content=").append(content)
				.append(", last=").append(last).append(", totalPages=")
				.append(totalPages).append(", totalElements=")
				.append(totalElements).append(", numberOfElements=")
				.append(numberOfElements).append(", first=").append(first)
				.append(", sort=").append(direction).append(", size=").append(size)
				.append(", number=").append(number).append("]");
		return builder.toString();
	}

}
