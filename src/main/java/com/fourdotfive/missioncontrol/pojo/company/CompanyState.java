package com.fourdotfive.missioncontrol.pojo.company;

public enum CompanyState {

    Active, Archived, Draft, InActive, Hold, SoftHold


}
