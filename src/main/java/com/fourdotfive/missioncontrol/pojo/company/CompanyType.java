package com.fourdotfive.missioncontrol.pojo.company;

public enum CompanyType {

    StaffingCompany, Client, Corporation, BusinessUnit, Host, ClientOrBU, StaffingOrCorporate

}
