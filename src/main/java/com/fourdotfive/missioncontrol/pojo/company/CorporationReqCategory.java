package com.fourdotfive.missioncontrol.pojo.company;

import java.util.ArrayList;
import java.util.List;

public class CorporationReqCategory {

	public static final String FULLTIME = "Full Time";
	public static final String CONTRACTING = "Contracting";
	public static final String PART_TIME = "Part Time";
	public static final String HOURLY = "Hourly";

	public List<String> addCorporationReqCategory() {
		List<String> category = new ArrayList<String>();
		category.add(FULLTIME);
		category.add(CONTRACTING);
		category.add(PART_TIME);
		category.add(HOURLY);
		return category;

	}
}
