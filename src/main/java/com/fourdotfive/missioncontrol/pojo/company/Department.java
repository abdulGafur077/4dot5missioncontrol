package com.fourdotfive.missioncontrol.pojo.company;

public class Department {

    private String id;
    private String name;
    private String staffingCompanyId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStaffingCompanyId() {
        return staffingCompanyId;
    }

    public void setStaffingCompanyId(String staffingCompanyId) {
        this.staffingCompanyId = staffingCompanyId;
    }
}
