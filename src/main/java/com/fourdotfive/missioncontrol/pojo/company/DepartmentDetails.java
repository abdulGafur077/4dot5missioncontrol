package com.fourdotfive.missioncontrol.pojo.company;

import com.fourdotfive.missioncontrol.dtos.company.CompanyDto;

public class DepartmentDetails {

    private String name;
    private CompanyDto staffingCompany;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CompanyDto getStaffingCompany() {
        return staffingCompany;
    }

    public void setStaffingCompany(CompanyDto staffingCompany) {
        this.staffingCompany = staffingCompany;
    }
}
