package com.fourdotfive.missioncontrol.pojo.company;

public class JobRequisitionRole {

	private String id;

	private String name;

	private String description;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("JobRequisitionRole [id=").append(id).append(", name=")
				.append(name).append(", description=").append(description)
				.append("]");
		return builder.toString();
	}

}
