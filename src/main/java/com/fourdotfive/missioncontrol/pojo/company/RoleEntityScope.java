package com.fourdotfive.missioncontrol.pojo.company;

import java.util.ArrayList;
import java.util.List;

import com.fourdotfive.missioncontrol.dtos.company.RoleScopeDto;
import com.fourdotfive.missioncontrol.pojo.user.Role;

public class RoleEntityScope {

    private boolean allClients;
    private boolean allIndustries;
    private boolean allJobRequisitionTitles;
    private boolean allRequisitionCategories;
    private boolean allBUsOfClients;
    private Role role;

    public RoleEntityScope() {
        super();
    }

    public RoleEntityScope(boolean allClients, boolean allIndustries,
                           boolean allJobRequisitionTitles, boolean allRequisitionCategories,
                           Role role, boolean allBUsOfClients) {
        super();
        this.allClients = allClients;
        this.allIndustries = allIndustries;
        this.allJobRequisitionTitles = allJobRequisitionTitles;
        this.allRequisitionCategories = allRequisitionCategories;
        this.role = role;
        this.allBUsOfClients = allBUsOfClients;
    }

    public static List<RoleEntityScope> mapDtotoRoleEntityScope(
            List<RoleScopeDto> roleScopeDtos) {

        List<RoleEntityScope> entityScopes = new ArrayList<RoleEntityScope>();
        for (RoleScopeDto roleScopeDto : roleScopeDtos) {
            RoleEntityScope entityScope = new RoleEntityScope(
                    roleScopeDto.isAllClientsOrBU(),
                    roleScopeDto.isAllIndustries(),
                    roleScopeDto.isAllJobRequisitionTitles(),
                    roleScopeDto.isAllRequisitionCategories(),
                    roleScopeDto.getRole(),
                    roleScopeDto.isAllBUsOfClients());
            entityScopes.add(entityScope);
        }
        return entityScopes;

    }

    public boolean isAllClients() {
        return allClients;
    }

    public void setAllClients(boolean allClients) {
        this.allClients = allClients;
    }

    public boolean isAllIndustries() {
        return allIndustries;
    }

    public void setAllIndustries(boolean allIndustries) {
        this.allIndustries = allIndustries;
    }

    public boolean isAllJobRequisitionTitles() {
        return allJobRequisitionTitles;
    }

    public void setAllJobRequisitionTitles(boolean allJobRequisitionTitles) {
        this.allJobRequisitionTitles = allJobRequisitionTitles;
    }

    public boolean isAllRequisitionCategories() {
        return allRequisitionCategories;
    }

    public void setAllRequisitionCategories(boolean allRequisitionCategories) {
        this.allRequisitionCategories = allRequisitionCategories;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public boolean isAllBUsOfClients() {
        return allBUsOfClients;
    }

    public void setAllBUsOfClients(boolean allBUsOfClients) {
        this.allBUsOfClients = allBUsOfClients;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("RoleEntityScope [isDisabled=");
        builder.append(", allClients=");
        builder.append(allClients);
        builder.append(", allIndustries=");
        builder.append(allIndustries);
        builder.append(", allJobRequisitionTitles=");
        builder.append(allJobRequisitionTitles);
        builder.append(", allRequisitionCategories=");
        builder.append(allRequisitionCategories);
        builder.append(", role=");
        builder.append(role);
        builder.append(", allBUsOfClients=");
        builder.append(allBUsOfClients);
        builder.append("]");
        return builder.toString();
    }
}