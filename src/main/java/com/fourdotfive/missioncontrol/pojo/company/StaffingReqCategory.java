package com.fourdotfive.missioncontrol.pojo.company;

import java.util.ArrayList;
import java.util.List;

public class StaffingReqCategory {
	public static final String RETAINED = "Retained";
	public static final String CONTINGENT = "Contingent";
	public static final String BOTH = "Both";

	public List<String> addStaffingReqCategory() {
		List<String> category = new ArrayList<String>();
		category.add(RETAINED);
		category.add(CONTINGENT);
		category.add(BOTH);
		return category;

	}
}
