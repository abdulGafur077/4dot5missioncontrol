package com.fourdotfive.missioncontrol.pojo.employee;

import com.fourdotfive.missioncontrol.dtos.employee.EmployeeDetails;
import com.fourdotfive.missioncontrol.dtos.employee.OrganizationManagerDto;


public class Employee {

    private String id;
    private String firstName;
    private String lastName;
    private String email;
    private String title;
    private String phoneNumber;
    private OrganizationManagerDto organizationManager;
    private String departmentId;
    private String departmentName;
    private String buId;
    private String buName;

    public Employee(){
    }

    public Employee(EmployeeDetails employeeDetails){
        this.id = employeeDetails.getId();
        this.firstName = employeeDetails.getFirstName();
        this.lastName = employeeDetails.getLastName();
        this.email = employeeDetails.getEmail();
        this.title = employeeDetails.getTitle();
        this.phoneNumber = employeeDetails.getPhoneNumber();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public OrganizationManagerDto getOrganizationManager() {
        return organizationManager;
    }

    public void setOrganizationManager(OrganizationManagerDto organizationManager) {
        this.organizationManager = organizationManager;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getBuId() {
        return buId;
    }

    public void setBuId(String buId) {
        this.buId = buId;
    }

    public String getBuName() {
        return buName;
    }

    public void setBuName(String buName) {
        this.buName = buName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
