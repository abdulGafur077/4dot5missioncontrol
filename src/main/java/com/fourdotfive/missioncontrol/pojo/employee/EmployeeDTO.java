package com.fourdotfive.missioncontrol.pojo.employee;

import com.fourdotfive.missioncontrol.common.Messages;
import com.fourdotfive.missioncontrol.dtos.employee.DepartmentDto;
import com.fourdotfive.missioncontrol.pojo.company.Department;
import org.apache.commons.lang3.Validate;

public class EmployeeDTO {

    private String id;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private String title;
    private String organizationManagerId;
    private Department department;
    private String buId;
    private String companyId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOrganizationManagerId() {
        return organizationManagerId;
    }

    public void setOrganizationManagerId(String organizationManagerId) {
        this.organizationManagerId = organizationManagerId;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public String getBuId() {
        return buId;
    }

    public void setBuId(String buId) {
        this.buId = buId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public static void validateFields(EmployeeDTO employeeDto) {
        Validate.notNull(employeeDto.getEmail(), Messages.EMAIL_MANDATORY);
        Validate.notNull(employeeDto.getFirstName(), Messages.FIRST_NAME_MANDATORY);
        Validate.notNull(employeeDto.getPhoneNumber(),Messages.MOBILE_MANDATORY);

        validateEmptyFields(employeeDto);
    }

    private static void validateEmptyFields(EmployeeDTO employeeDto) {

        Validate.notBlank(employeeDto.getEmail(), Messages.EMAIL_MANDATORY);
        Validate.notBlank(employeeDto.getFirstName(),Messages.FIRST_NAME_MANDATORY);
        Validate.notBlank(employeeDto.getPhoneNumber(), Messages.MOBILE_MANDATORY);
    }
}
