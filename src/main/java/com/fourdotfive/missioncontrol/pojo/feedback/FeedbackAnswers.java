package com.fourdotfive.missioncontrol.pojo.feedback;

public class FeedbackAnswers {

    private String candidateId;

    private String companyId;

    private String clientorbuId;

    private String jobMatchId;

    private String question;

    private int questionOrder;

    private int rating;

    private String recruiterComment;

    private String questionareDateTime;


    private String userId;

    private String id;

    private String recruiterName;

    private String questionType;

    private String lastModifiedBy;

    private String participantId;

    private String externalComment;

     public String getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(String candidateId) {
        this.candidateId = candidateId;
    }


    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getClientorbuId() {
        return clientorbuId;
    }

    public void setClientorbuId(String clientorbuId) {
        this.clientorbuId = clientorbuId;
    }

    public String getJobmatchId() {
        return jobMatchId;
    }

    public void setJobmatchId(String jobMatchId) {
        this.jobMatchId = jobMatchId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getQuestionOrder() {
        return questionOrder;
    }

    public void setQuestionOrder(int questionOrder) {
        this.questionOrder = questionOrder;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getRecruiterComment() {
        return recruiterComment;
    }

    public void setRecruiterComment(String recruiterComment) {
        this.recruiterComment = recruiterComment;
    }

    public String getQuestionareDateTime() {
        return questionareDateTime;
    }

    public void setQuestionareDateTime(String questionareDateTime) {
        this.questionareDateTime = questionareDateTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRecruiterName() {
        return recruiterName;
    }

    public void setRecruiterName(String recruiterName) {
        this.recruiterName = recruiterName;
    }

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getParticipantId() {
        return participantId;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }

    public String getExternalComment() {
        return externalComment;
    }

    public void setExternalComment(String externalComment) {
        this.externalComment = externalComment;
    }
}