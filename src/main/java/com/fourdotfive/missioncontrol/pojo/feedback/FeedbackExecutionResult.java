package com.fourdotfive.missioncontrol.pojo.feedback;

import com.fourdotfive.missioncontrol.dtos.company.JobDetailsRecruiterScreening;

import java.util.List;

public class FeedbackExecutionResult {

    private List<JobDetailsRecruiterScreening> jobDetailsRecruiterScreeningList;

    private List<FeedbackAnswers> feedbackAnswersList;

    private String companyAndOverallComplete;

    private String fromButton;

    private String loggedInUser;

    private String companyId;

    private String meetingScheduleId;

    public List<JobDetailsRecruiterScreening> getJobDetailsRecruiterScreeningList() {
        return jobDetailsRecruiterScreeningList;
    }

    public void setJobDetailsRecruiterScreeningList(List<JobDetailsRecruiterScreening> jobDetailsRecruiterScreeningList) {
        this.jobDetailsRecruiterScreeningList = jobDetailsRecruiterScreeningList;
    }

    public List<FeedbackAnswers> getFeedbackAnswersList() {
        return feedbackAnswersList;
    }

    public void setFeedbackAnswersList(List<FeedbackAnswers> feedbackAnswersList) {
        this.feedbackAnswersList = feedbackAnswersList;
    }

    public String getCompanyAndOverallComplete() {
        return companyAndOverallComplete;
    }

    public void setCompanyAndOverallComplete(String companyAndOverallComplete) {
        this.companyAndOverallComplete = companyAndOverallComplete;
    }

    public String getFromButton() {
        return fromButton;
    }

    public void setFromButton(String fromButton) {
        this.fromButton = fromButton;
    }


    public String getLoggedInUser() {
        return loggedInUser;
    }

    public void setLoggedInUser(String loggedInUser) {
        this.loggedInUser = loggedInUser;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getMeetingScheduleId() {
        return meetingScheduleId;
    }

    public void setMeetingScheduleId(String meetingScheduleId) {
        this.meetingScheduleId = meetingScheduleId;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("FeedbackExecutionResult [companyAndOverallComplete=");
        builder.append(companyAndOverallComplete);
        builder.append(", feedbackAnswersList=");
        builder.append(feedbackAnswersList);
        builder.append(", jobDetailsRecruiterScreeningList=");
        builder.append(jobDetailsRecruiterScreeningList);
        builder.append(", fromButton=");
        builder.append(fromButton);
        builder.append("]");
        return builder.toString();
    }
}
