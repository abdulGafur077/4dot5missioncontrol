package com.fourdotfive.missioncontrol.pojo.feedback;



import com.fourdotfive.missioncontrol.pojo.company.Company;

import java.util.ArrayList;
import java.util.List;

public class FeedbackQuestions {

    private String companyId;

    private String jobId;

    private List<String> questionList = new ArrayList<>();

    private String questionType;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public List<String> getQuestionList() {
        return questionList;
    }

    public void setQuestionList(List<String> questionList) {
        this.questionList = questionList;
    }

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }
}
