package com.fourdotfive.missioncontrol.pojo.feedback;

public enum FeedbackType {

    INTERVIEW("I"), PHONE("P"), RECRUITER("R");

    private final String feedbackType;

   FeedbackType(String feedbackType) {
        this.feedbackType = feedbackType;
    }


    public String getfeedbackType(){
        return feedbackType;
    }
}
