package com.fourdotfive.missioncontrol.pojo.feedback;

public class FileRetrieve {

    private String filelocation;

    private String subdir;

    private String filename;


    /**
     * @return the filelocation
     */
    public String getFilelocation() {
        return filelocation;
    }

    /**
     * @param filelocation the filelocation to set
     */
    public void setFilelocation(String filelocation) {
        this.filelocation = filelocation;
    }

    /**
     * @return the filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * @param the filename to set
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }

    /**
     * @return the subdir
     */
    public String getSubdir() {
        return subdir;
    }

    /**
     * @param the subdir to set
     */
    public void setSubdir(String subdir) {
        this.subdir = subdir;
    }
}
