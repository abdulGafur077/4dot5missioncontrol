package com.fourdotfive.missioncontrol.pojo.job;

import java.util.List;

import com.fourdotfive.missioncontrol.pojo.candidate.Profile;
import com.fourdotfive.missioncontrol.pojo.user.Address;
import com.fourdotfive.missioncontrol.pojo.user.Contact;

public class AcceptedCandidate {

	private Address address;

	private Contact contact;

	private List<ContactMethod> contactMethods;

	private String createdBy;

	private String createdDate;

	private String id;

	private List<JobMatch> jobMatches;

	private String lastModifiedBy;

	private String lastModifiedDate;

	private boolean isNew;

	private Profile profile;

	private List<Resume> resumes;

	private List<SocialProfile> socialProfiles;

	private int version;

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public List<ContactMethod> getContactMethods() {
		return contactMethods;
	}

	public void setContactMethods(List<ContactMethod> contactMethods) {
		this.contactMethods = contactMethods;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<JobMatch> getJobMatches() {
		return jobMatches;
	}

	public void setJobMatches(List<JobMatch> jobMatches) {
		this.jobMatches = jobMatches;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	
	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(String lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public List<Resume> getResumes() {
		return resumes;
	}

	public void setResumes(List<Resume> resumes) {
		this.resumes = resumes;
	}

	public List<SocialProfile> getSocialProfiles() {
		return socialProfiles;
	}

	public void setSocialProfiles(List<SocialProfile> socialProfiles) {
		this.socialProfiles = socialProfiles;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AcceptedCandidate [address=").append(address)
				.append(", contact=").append(contact)
				.append(", contactMethods=").append(contactMethods)
				.append(", createdBy=").append(createdBy)
				.append(", createdDate=").append(createdDate).append(", id=")
				.append(id).append(", jobMatches=").append(jobMatches)
				.append(", lastModifiedBy=").append(lastModifiedBy)
				.append(", lastModifiedDate=").append(lastModifiedDate)
				.append(", isNew=").append(isNew).append(", profile=")
				.append(profile).append(", resumes=").append(resumes)
				.append(", socialProfiles=").append(socialProfiles)
				.append(", version=").append(version).append("]");
		return builder.toString();
	}

}
