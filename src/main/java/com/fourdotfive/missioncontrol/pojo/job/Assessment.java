package com.fourdotfive.missioncontrol.pojo.job;

import com.fourdotfive.missioncontrol.pojo.candidate.Candidate;

public class Assessment {

	private Candidate candidate;

	private String createdBy;

	private String createdDate;

	private String id;

	private String inviteURL;

	private Job job;

	private String lastModifiedBy;

	private String lastModifiedDate;

	private boolean isNew;

	private String resultURL;

	private String sessionURL;

	private AssessmentMaster techAssessmentMaster;

	private AssessmentResult techAssessmentResult;

	private String testTaken;

	private int version;

	public Candidate getCandidate() {
		return candidate;
	}

	public void setCandidate(Candidate candidate) {
		this.candidate = candidate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getInviteURL() {
		return inviteURL;
	}

	public void setInviteURL(String inviteURL) {
		this.inviteURL = inviteURL;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

	public String getResultURL() {
		return resultURL;
	}

	public void setResultURL(String resultURL) {
		this.resultURL = resultURL;
	}

	public String getSessionURL() {
		return sessionURL;
	}

	public void setSessionURL(String sessionURL) {
		this.sessionURL = sessionURL;
	}

	public AssessmentMaster getTechAssessmentMaster() {
		return techAssessmentMaster;
	}

	public void setTechAssessmentMaster(
			AssessmentMaster techAssessmentMaster) {
		this.techAssessmentMaster = techAssessmentMaster;
	}

	public AssessmentResult getTechAssessmentResult() {
		return techAssessmentResult;
	}

	public void setTechAssessmentResult(
			AssessmentResult techAssessmentResult) {
		this.techAssessmentResult = techAssessmentResult;
	}

	public String getTestTaken() {
		return testTaken;
	}

	public void setTestTaken(String testTaken) {
		this.testTaken = testTaken;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(String lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TechAssessment [candidate=").append(candidate)
				.append(", createdBy=").append(createdBy)
				.append(", createdDate=").append(createdDate).append(", id=")
				.append(id).append(", inviteURL=").append(inviteURL)
				.append(", job=").append(job).append(", lastModifiedBy=")
				.append(lastModifiedBy).append(", lastModifiedDate=")
				.append(lastModifiedDate).append(", isNew=").append(isNew)
				.append(", resultURL=").append(resultURL)
				.append(", sessionURL=").append(sessionURL)
				.append(", techAssessmentMaster=").append(techAssessmentMaster)
				.append(", techAssessmentResult=").append(techAssessmentResult)
				.append(", testTaken=").append(testTaken).append(", version=")
				.append(version).append("]");
		return builder.toString();
	}

}
