package com.fourdotfive.missioncontrol.pojo.job;

import com.fourdotfive.missioncontrol.pojo.company.Company;

public class AssessmentMaster {

	private String assessmentName;

	private Company company;

	private String createdBy;

	private String createdDate;

	private String id;

	private String lastModifiedBy;

	private String lastModifiedDate;

	private boolean isNew;

	private String testURL;

	private int version;

	public String getAssessmentName() {
		return assessmentName;
	}

	public void setAssessmentName(String assessmentName) {
		this.assessmentName = assessmentName;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(String lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

	public String getTestURL() {
		return testURL;
	}

	public void setTestURL(String testURL) {
		this.testURL = testURL;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TechAssessmentMaster [assessmentName=")
				.append(assessmentName).append(", company=").append(company)
				.append(", createdBy=").append(createdBy)
				.append(", createdDate=").append(createdDate).append(", id=")
				.append(id).append(", lastModifiedBy=").append(lastModifiedBy)
				.append(", lastModifiedDate=").append(lastModifiedDate)
				.append(", isNew=").append(isNew).append(", testURL=")
				.append(testURL).append(", version=").append(version)
				.append("]");
		return builder.toString();
	}

}
