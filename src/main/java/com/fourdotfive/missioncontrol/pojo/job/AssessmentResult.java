package com.fourdotfive.missioncontrol.pojo.job;

import java.util.List;

public class AssessmentResult {

	private String campaignURL;

	private CandidateInfo candidateInfo;

	private List<Evaluation> evaluationList;

	private String feedbackURL;

	private int maxScore;

	private String pdfURL;

	private String reoprtURL;

	private String resultURL;

	private int score;

	private String startDate;

	private String testCreateDate;

	public String getCampaignURL() {
		return campaignURL;
	}

	public void setCampaignURL(String campaignURL) {
		this.campaignURL = campaignURL;
	}

	public CandidateInfo getCandidateInfo() {
		return candidateInfo;
	}

	public void setCandidateInfo(CandidateInfo candidateInfo) {
		this.candidateInfo = candidateInfo;
	}

	public List<Evaluation> getEvaluationList() {
		return evaluationList;
	}

	public void setEvaluationList(List<Evaluation> evaluationList) {
		this.evaluationList = evaluationList;
	}

	public String getFeedbackURL() {
		return feedbackURL;
	}

	public void setFeedbackURL(String feedbackURL) {
		this.feedbackURL = feedbackURL;
	}

	public int getMaxScore() {
		return maxScore;
	}

	public void setMaxScore(int maxScore) {
		this.maxScore = maxScore;
	}

	public String getPdfURL() {
		return pdfURL;
	}

	public void setPdfURL(String pdfURL) {
		this.pdfURL = pdfURL;
	}

	public String getReoprtURL() {
		return reoprtURL;
	}

	public void setReoprtURL(String reoprtURL) {
		this.reoprtURL = reoprtURL;
	}

	public String getResultURL() {
		return resultURL;
	}

	public void setResultURL(String resultURL) {
		this.resultURL = resultURL;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getTestCreateDate() {
		return testCreateDate;
	}

	public void setTestCreateDate(String testCreateDate) {
		this.testCreateDate = testCreateDate;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TechAssessmentResult [campaignURL=")
				.append(campaignURL).append(", candidateInfo=")
				.append(candidateInfo).append(", evaluationList=")
				.append(evaluationList).append(", feedbackURL=")
				.append(feedbackURL).append(", maxScore=").append(maxScore)
				.append(", pdfURL=").append(pdfURL).append(", reoprtURL=")
				.append(reoprtURL).append(", resultURL=").append(resultURL)
				.append(", score=").append(score).append(", startDate=")
				.append(startDate).append(", testCreateDate=")
				.append(testCreateDate).append("]");
		return builder.toString();
	}

}
