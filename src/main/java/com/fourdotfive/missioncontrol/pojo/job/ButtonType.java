package com.fourdotfive.missioncontrol.pojo.job;


public enum ButtonType {
    SAVE, NEXT, CANCEL, COMPLETED
}
