package com.fourdotfive.missioncontrol.pojo.job;

public class CandidateCount {

    private int uploadedCandidateCount;
    private int notProcessedCandidateCount;
    private int autoMatchedCandidateCount;
    private int unmatchedCandidateCount;
    private int manuallyMatchedCandidateCount;

    public int getUploadedCandidateCount() {
        return uploadedCandidateCount;
    }

    public void setUploadedCandidateCount(int uploadedCandidateCount) {
        this.uploadedCandidateCount = uploadedCandidateCount;
    }

    public int getNotProcessedCandidateCount() {
        return notProcessedCandidateCount;
    }

    public void setNotProcessedCandidateCount(int notProcessedCandidateCount) {
        this.notProcessedCandidateCount = notProcessedCandidateCount;
    }

    public int getAutoMatchedCandidateCount() {
        return autoMatchedCandidateCount;
    }

    public void setAutoMatchedCandidateCount(int autoMatchedCandidateCount) {
        this.autoMatchedCandidateCount = autoMatchedCandidateCount;
    }

    public int getUnmatchedCandidateCount() {
        return unmatchedCandidateCount;
    }

    public void setUnmatchedCandidateCount(int unmatchedCandidateCount) {
        this.unmatchedCandidateCount = unmatchedCandidateCount;
    }

    public int getManuallyMatchedCandidateCount() {
        return manuallyMatchedCandidateCount;
    }

    public void setManuallyMatchedCandidateCount(int manuallyMatchedCandidateCount) {
        this.manuallyMatchedCandidateCount = manuallyMatchedCandidateCount;
    }
}
