package com.fourdotfive.missioncontrol.pojo.job;

public class CandidateInfo {

	private String academicDegree;

	private String fieldOfStudy;

	private String profileURL;

	private String programmingExperience;

	private String school;

	public String getAcademicDegree() {
		return academicDegree;
	}

	public void setAcademicDegree(String academicDegree) {
		this.academicDegree = academicDegree;
	}

	public String getFieldOfStudy() {
		return fieldOfStudy;
	}

	public void setFieldOfStudy(String fieldOfStudy) {
		this.fieldOfStudy = fieldOfStudy;
	}

	public String getProfileURL() {
		return profileURL;
	}

	public void setProfileURL(String profileURL) {
		this.profileURL = profileURL;
	}

	public String getProgrammingExperience() {
		return programmingExperience;
	}

	public void setProgrammingExperience(String programmingExperience) {
		this.programmingExperience = programmingExperience;
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CandidateInfo [academicDegree=").append(academicDegree)
				.append(", fieldOfStudy=").append(fieldOfStudy)
				.append(", profileURL=").append(profileURL)
				.append(", programmingExperience=")
				.append(programmingExperience).append(", school=")
				.append(school).append("]");
		return builder.toString();
	}

}
