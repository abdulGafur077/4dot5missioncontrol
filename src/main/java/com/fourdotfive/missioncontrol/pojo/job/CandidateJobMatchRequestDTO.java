package com.fourdotfive.missioncontrol.pojo.job;

import com.fourdotfive.missioncontrol.pojo.candidate.CandidateType;

import java.util.List;

public class CandidateJobMatchRequestDTO {

    private String candidateId;
    private String userId;
    private boolean top5Flag;
    private CandidateType candidateType;
    private String searchText;
    private String companyId;
    private List<String> vendorIds;

    public String getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(String candidateId) {
        this.candidateId = candidateId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean isTop5Flag() {
        return top5Flag;
    }

    public void setTop5Flag(boolean top5Flag) {
        this.top5Flag = top5Flag;
    }

    public CandidateType getCandidateType() {
        return candidateType;
    }

    public void setCandidateType(CandidateType candidateType) {
        this.candidateType = candidateType;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public List<String> getVendorIds() {
        return vendorIds;
    }

    public void setVendorIds(List<String> vendorIds) {
        this.vendorIds = vendorIds;
    }
}
