package com.fourdotfive.missioncontrol.pojo.job;

public enum CandidateJobStateType {
    SOFT_QUALIFIED("Soft Qualified"), IN_OUTREACH("OutReach"), CONTACTED(
            "Contacted"), ACTIVATED("Activated"), RESPONDED("Responded"),

    QUALIFIED("Qualified"), NOT_INTERESTED("Not Interested"), RELEASED("Released"), IN_TECH_ASSESSMENT("In Tech Assessment"), SENT_TECH_ASSESSMENT("Sent for Tech Assessment"),
    TECH_ASSESSMENT_COMPLETED("Completed Tech Assessment"), TECH_ASSESSMENT_SCORED("Scored Tech Assessment"), IN_VALUE_ASSESSMENT("In Value Assessment"),
    SENT_VALUE_ASSESSMENT("Sent for Value Assessment"), VALUE_ASSESSMENT_COMPLETED("Completed Value Assessment"), VALUE_ASSESSMENT_SCORED("Scored Value Assessment"),
    IN_VERIFICATION("In Verification"), IN_VERIFICATION_INFORMATION_COLLECTION("In Verification Information Collection"),
    VERIFICATION_APPROVED("Candidate approved Verification"), VERIFICATION_REJECTED("Candidate Rejected Verification"),
    SENT_FOR_VERIFICATION("Sent for Verification"), VERIFICATION_RECEIVED("Received Verification"),
    IN_INTERVIEW("In Interview"), READY_FOR_INTERVIEW("Ready for Interview"), IN_LIKELY("In Likley");

    private String text;

    CandidateJobStateType(String text) {
        this.text = text;
    }

    public String getText() {
        return this.text;
    }
}
