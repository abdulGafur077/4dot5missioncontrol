package com.fourdotfive.missioncontrol.pojo.job;

public class CandidateResumeOutput {

    private String transactionID;
    private HrXmlFileInfo file;
    private long successCount;
    private long failureCount;

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public HrXmlFileInfo getFile() {
        return file;
    }

    public void setFile(HrXmlFileInfo file) {
        this.file = file;
    }

    public long getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(long successCount) {
        this.successCount = successCount;
    }

    public long getFailureCount() {
        return failureCount;
    }

    public void setFailureCount(long failureCount) {
        this.failureCount = failureCount;
    }
}
