package com.fourdotfive.missioncontrol.pojo.job;

import java.util.List;

public class Certifications {

	private String description;

	private int durationInMonths;

	private Institution institution;

	private String name;

	private List<Skill> relatedOperationalSkill;

	private List<Skill> relatedSoftSkillList;

	private List<Skill> relatedTechnicalSkillList;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getDurationInMonths() {
		return durationInMonths;
	}

	public void setDurationInMonths(int durationInMonths) {
		this.durationInMonths = durationInMonths;
	}

	public Institution getInstitution() {
		return institution;
	}

	public void setInstitution(Institution institution) {
		this.institution = institution;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Skill> getRelatedOperationalSkill() {
		return relatedOperationalSkill;
	}

	public void setRelatedOperationalSkill(List<Skill> relatedOperationalSkill) {
		this.relatedOperationalSkill = relatedOperationalSkill;
	}

	public List<Skill> getRelatedSoftSkillList() {
		return relatedSoftSkillList;
	}

	public void setRelatedSoftSkillList(List<Skill> relatedSoftSkillList) {
		this.relatedSoftSkillList = relatedSoftSkillList;
	}

	public List<Skill> getRelatedTechnicalSkillList() {
		return relatedTechnicalSkillList;
	}

	public void setRelatedTechnicalSkillList(
			List<Skill> relatedTechnicalSkillList) {
		this.relatedTechnicalSkillList = relatedTechnicalSkillList;
	}

	@Override
	public String toString() {
		return "Certifications{" +
				"description='" + description + '\'' +
				", durationInMonths=" + durationInMonths +
				", institution=" + institution +
				", name='" + name + '\'' +
				", relatedOperationalSkill=" + relatedOperationalSkill +
				", relatedSoftSkillList=" + relatedSoftSkillList +
				", relatedTechnicalSkillList=" + relatedTechnicalSkillList +
				'}';
	}
}
