package com.fourdotfive.missioncontrol.pojo.job;

public class ContactMethod {

	private String contactMethodTime;

	private String countryCode;

	private String email;

	private String municipality;

	private String phone;

	private String portalCode;

	private String region;

	public String getContactMethodTime() {
		return contactMethodTime;
	}

	public void setContactMethodTime(String contactMethodTime) {
		this.contactMethodTime = contactMethodTime;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMunicipality() {
		return municipality;
	}

	public void setMunicipality(String municipality) {
		this.municipality = municipality;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPortalCode() {
		return portalCode;
	}

	public void setPortalCode(String portalCode) {
		this.portalCode = portalCode;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ContactMethod [contactMethodTime=")
				.append(contactMethodTime).append(", countryCode=")
				.append(countryCode).append(", email=").append(email)
				.append(", municipality=").append(municipality)
				.append(", phone=").append(phone).append(", portalCode=")
				.append(portalCode).append(", region=").append(region)
				.append("]");
		return builder.toString();
	}

}
