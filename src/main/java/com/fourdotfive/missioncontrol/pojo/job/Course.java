package com.fourdotfive.missioncontrol.pojo.job;

public class Course {

	private String courseName;

	private int gpa;

	private int score;

	private int totalScore;

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public int getGpa() {
		return gpa;
	}

	public void setGpa(int gpa) {
		this.gpa = gpa;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getTotalScore() {
		return totalScore;
	}

	public void setTotalScore(int totalScore) {
		this.totalScore = totalScore;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Course [courseName=").append(courseName)
				.append(", gpa=").append(gpa).append(", score=").append(score)
				.append(", totalScore=").append(totalScore).append("]");
		return builder.toString();
	}

}
