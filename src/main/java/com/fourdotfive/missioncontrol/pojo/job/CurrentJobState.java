package com.fourdotfive.missioncontrol.pojo.job;

import com.fourdotfive.missioncontrol.pojo.FourDotFiveDate;

public class CurrentJobState {

	private AcceptedCandidate acceptedCandidate;

	private String createdBy;

	//private FourDotFiveDate createdDate;

	//private FourDotFiveDate date;

	private String id;

	private String jobStateType;

	private String lastModifiedBy;

	//private FourDotFiveDate lastModifiedDate;

	private boolean isNew;

	private String note;

	private int version;

	public AcceptedCandidate getAcceptedCandidate() {
		return acceptedCandidate;
	}

	public void setAcceptedCandidate(AcceptedCandidate acceptedCandidate) {
		this.acceptedCandidate = acceptedCandidate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getJobStateType() {
		return jobStateType;
	}

	public void setJobStateType(String jobStateType) {
		this.jobStateType = jobStateType;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	/*
	public FourDotFiveDate getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(FourDotFiveDate createdDate) {
		this.createdDate = createdDate;
	}

	public FourDotFiveDate getDate() {
		return date;
	}

	public void setDate(FourDotFiveDate date) {
		this.date = date;
	}

	public FourDotFiveDate getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(FourDotFiveDate lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	*/

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CurrentJobState [acceptedCandidate=")
				.append(acceptedCandidate).append(", createdBy=")
			//	.append(createdBy).append(", createdDate=").append(createdDate)
			//	.append(", date=").append(date).append(", id=").append(id)
				.append(", jobStateType=").append(jobStateType)
				.append(", lastModifiedBy=").append(lastModifiedBy)
			//	.append(", lastModifiedDate=").append(lastModifiedDate)
				.append(", isNew=").append(isNew).append(", note=")
				.append(note).append(", version=").append(version).append("]");
		return builder.toString();
	}

}
