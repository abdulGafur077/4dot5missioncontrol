package com.fourdotfive.missioncontrol.pojo.job;

import java.util.List;

public class Education {

	private List<Course> courseList;

	private String degreeName;

	private String degreeType;

	private int gpa;

	private Institution institution;

	private List<Skill> operationalSkills;

	private int score;

	private List<Skill> softSkills;

	private List<Skill> technicalSkills;

	private int totalScore;

	public List<Course> getCourseList() {
		return courseList;
	}

	public void setCourseList(List<Course> courseList) {
		this.courseList = courseList;
	}

	public String getDegreeName() {
		return degreeName;
	}

	public void setDegreeName(String degreeName) {
		this.degreeName = degreeName;
	}

	public String getDegreeType() {
		return degreeType;
	}

	public void setDegreeType(String degreeType) {
		this.degreeType = degreeType;
	}

	public int getGpa() {
		return gpa;
	}

	public void setGpa(int gpa) {
		this.gpa = gpa;
	}

	public Institution getInstitution() {
		return institution;
	}

	public void setInstitution(Institution institution) {
		this.institution = institution;
	}

	public List<Skill> getOperationalSkills() {
		return operationalSkills;
	}

	public void setOperationalSkills(List<Skill> operationalSkills) {
		this.operationalSkills = operationalSkills;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public List<Skill> getSoftSkills() {
		return softSkills;
	}

	public void setSoftSkills(List<Skill> softSkills) {
		this.softSkills = softSkills;
	}

	public int getTotalScore() {
		return totalScore;
	}

	public void setTotalScore(int totalScore) {
		this.totalScore = totalScore;
	}

	public List<Skill> getTechnicalSkills() {
		return technicalSkills;
	}

	public void setTechnicalSkills(List<Skill> technicalSkills) {
		this.technicalSkills = technicalSkills;
	}

	@Override
	public String toString() {
		return "Education{" +
				"courseList=" + courseList +
				", degreeName='" + degreeName + '\'' +
				", degreeType='" + degreeType + '\'' +
				", gpa=" + gpa +
				", institution=" + institution +
				", operationalSkills=" + operationalSkills +
				", score=" + score +
				", softSkills=" + softSkills +
				", technicalSkills=" + technicalSkills +
				", totalScore=" + totalScore +
				'}';
	}
}
