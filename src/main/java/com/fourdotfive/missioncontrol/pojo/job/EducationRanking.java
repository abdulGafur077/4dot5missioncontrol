package com.fourdotfive.missioncontrol.pojo.job;

public class EducationRanking {

	private String degreeName;

	private String degreeType;

	private int rank;

	private String year;

	public String getDegreeName() {
		return degreeName;
	}

	public void setDegreeName(String degreeName) {
		this.degreeName = degreeName;
	}

	public String getDegreeType() {
		return degreeType;
	}

	public void setDegreeType(String degreeType) {
		this.degreeType = degreeType;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EducationRanking [degreeName=").append(degreeName)
				.append(", degreeType=").append(degreeType).append(", rank=")
				.append(rank).append(", year=").append(year).append("]");
		return builder.toString();
	}

}
