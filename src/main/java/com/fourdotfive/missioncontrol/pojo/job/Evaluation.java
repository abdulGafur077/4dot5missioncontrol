package com.fourdotfive.missioncontrol.pojo.job;

public class Evaluation {

	private int maxResults;

	private String name;

	private String programmingLanguage;

	private int result;

	private String taskName;

	public int getMaxResults() {
		return maxResults;
	}

	public void setMaxResults(int maxResults) {
		this.maxResults = maxResults;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProgrammingLanguage() {
		return programmingLanguage;
	}

	public void setProgrammingLanguage(String programmingLanguage) {
		this.programmingLanguage = programmingLanguage;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Evaluation [maxResults=").append(maxResults)
				.append(", name=").append(name)
				.append(", programmingLanguage=").append(programmingLanguage)
				.append(", result=").append(result).append(", taskName=")
				.append(taskName).append("]");
		return builder.toString();
	}

}
