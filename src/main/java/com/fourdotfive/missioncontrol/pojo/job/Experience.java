package com.fourdotfive.missioncontrol.pojo.job;

import com.fourdotfive.missioncontrol.pojo.company.Company;

import java.util.List;

public class Experience {

	private String comments;

	private boolean currentEmployer;

	private Company employer;

	private String jobDescription;

	private List<Skill> operationalSkills;

	private String positionLocation;

	private String positionType;

	private List<Skill> softSkills;

	private List<Skill> technicalSkills;

	private String title;

	public boolean isCurrentEmployer() {
		return currentEmployer;
	}

	public void setCurrentEmployer(boolean currentEmployer) {
		this.currentEmployer = currentEmployer;
	}

	public Company getEmployer() {
		return employer;
	}

	public void setEmployer(Company employer) {
		this.employer = employer;
	}

	public String getJobDescription() {
		return jobDescription;
	}

	public void setJobDescription(String jobDescription) {
		this.jobDescription = jobDescription;
	}

	public List<Skill> getOperationalSkills() {
		return operationalSkills;
	}

	public void setOperationalSkills(List<Skill> operationalSkills) {
		this.operationalSkills = operationalSkills;
	}

	public String getPositionLocation() {
		return positionLocation;
	}

	public void setPositionLocation(String positionLocation) {
		this.positionLocation = positionLocation;
	}

	public String getPositionType() {
		return positionType;
	}

	public void setPositionType(String positionType) {
		this.positionType = positionType;
	}

	public List<Skill> getSoftSkills() {
		return softSkills;
	}

	public void setSoftSkills(List<Skill> softSkills) {
		this.softSkills = softSkills;
	}

	public List<Skill> getTechnicalSkills() {
		return technicalSkills;
	}

	public void setTechnicalSkills(List<Skill> technicalSkills) {
		this.technicalSkills = technicalSkills;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
        return "Experience{" +
                "comments='" + comments + '\'' +
                ", currentEmployer=" + currentEmployer +
                ", employer=" + employer +
                ", jobDescription='" + jobDescription + '\'' +
                ", operationalSkills=" + operationalSkills +
                ", positionLocation='" + positionLocation + '\'' +
                ", positionType='" + positionType + '\'' +
                ", softSkills=" + softSkills +
                ", technicalSkills=" + technicalSkills +
                ", title='" + title + '\'' +
                '}';
	}
}
