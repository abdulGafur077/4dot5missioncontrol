package com.fourdotfive.missioncontrol.pojo.job;

import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.user.User;

import java.util.List;

public class FilterResponse {

	private List<User> recruiters;
	private List<JobStateType> requisitionStates;
	private List<String> roleList;
	private List<Company> clientOrBU;
	private List<CandidateJobStateType> activeCandidateStateList;
	private List<CandidateJobStateType> passiveCandidateStateList;

	public List<User> getRecruiters() {
		return recruiters;
	}

	public void setRecruiters(List<User> recruiters) {
		this.recruiters = recruiters;
	}

	public List<JobStateType> getRequisitionStates() {
		return requisitionStates;
	}

	public void setRequisitionStates(List<JobStateType> requisitionStates) {
		this.requisitionStates = requisitionStates;
	}

	public List<String> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<String> roleList) {
		this.roleList = roleList;
	}

	public List<Company> getClientOrBU() {
		return clientOrBU;
	}

	public void setClientOrBU(List<Company> clientOrBU) {
		this.clientOrBU = clientOrBU;
	}

	public List<CandidateJobStateType> getActiveCandidateStateList() {
		return activeCandidateStateList;
	}

	public void setActiveCandidateStateList(List<CandidateJobStateType> activeCandidateStateList) {
		this.activeCandidateStateList = activeCandidateStateList;
	}

	public List<CandidateJobStateType> getPassiveCandidateStateList() {
		return passiveCandidateStateList;
	}

	public void setPassiveCandidateStateList(List<CandidateJobStateType> passiveCandidateStateList) {
		this.passiveCandidateStateList = passiveCandidateStateList;
	}
}
