package com.fourdotfive.missioncontrol.pojo.job;

import com.fourdotfive.missioncontrol.pojo.JobBoardDetails;
import org.joda.time.Period;

import java.util.List;

public class GetJBCandidatesModel {
    private String jobId;
    private String userName;
    private Integer resumesToPullPerJobBoardCount = null;
    private List<JobBoardDetails> jobBoardDetails;
    private String radius;
    private String recency;
    private String currentCompanyId;


    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<JobBoardDetails> getJobBoardDetails() {
        return jobBoardDetails;
    }

    public void setJobBoardDetails(List<JobBoardDetails> jobBoardDetails) {
        this.jobBoardDetails = jobBoardDetails;
    }

    public Integer getResumesToPullPerJobBoardCount() {
        return resumesToPullPerJobBoardCount;
    }

    public void setResumesToPullPerJobBoardCount(Integer resumesToPullPerJobBoardCount) {
        this.resumesToPullPerJobBoardCount = resumesToPullPerJobBoardCount;
    }

    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }

    public String getRecency() {
        return recency;
    }

    public void setRecency(String recency) {
        this.recency = recency;
    }

    public String getCurrentCompanyId() {
        return currentCompanyId;
    }

    public void setCurrentCompanyId(String currentCompanyId) {
        this.currentCompanyId = currentCompanyId;
    }
}
