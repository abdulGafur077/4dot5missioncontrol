package com.fourdotfive.missioncontrol.pojo.job;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fourdotfive.missioncontrol.pojo.user.User;

public class HiringManagerInfo {
    private User user;

    @JsonProperty("isPrimary")
    private boolean isPrimary;


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isPrimary() {
        return isPrimary;
    }

    public void setPrimary(boolean primary) {
        isPrimary = primary;
    }

}
