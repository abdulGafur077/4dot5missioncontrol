package com.fourdotfive.missioncontrol.pojo.job;

public class HrXmlFileInfo {
	private String hrXmlFileName;
	private String hrXmlFileLocation;
	private String rawFileName;
	private String rawFileLocation;
	private String subDir;
	private String resumeId;
	private boolean isSuccess;
	private String errorCode;
	public String getHrXmlFileName() {
		return hrXmlFileName;
	}
	public void setHrXmlFileName(String hrXmlFileName) {
		this.hrXmlFileName = hrXmlFileName;
	}
	public String getHrXmlFileLocation() {
		return hrXmlFileLocation;
	}
	public void setHrXmlFileLocation(String hrXmlFileLocation) {
		this.hrXmlFileLocation = hrXmlFileLocation;
	}
	public String getRawFileName() {
		return rawFileName;
	}
	public void setRawFileName(String rawFileName) {
		this.rawFileName = rawFileName;
	}
	public String getRawFileLocation() {
		return rawFileLocation;
	}
	public void setRawFileLocation(String rawFileLocation) {
		this.rawFileLocation = rawFileLocation;
	}
	public boolean getIsSuccess() {
		return isSuccess;
	}
	public void setIsSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getSubDir() {
		return subDir;
	}
	public void setSubDir(String subDir) {
		this.subDir = subDir;
	}
	public String getResumeId() {
		return resumeId;
	}
	public void setResumeId(String resumeId) {
		this.resumeId = resumeId;
	}

}
