package com.fourdotfive.missioncontrol.pojo.job;

import com.fourdotfive.missioncontrol.pojo.user.Address;

import java.util.List;

public class Institution {

	private List<Address> addressList;

	private String createdBy;

	private List<EducationRanking> educationRankingList;

	private String email;

	private String id;

	private String institutionType;

	private String lastModifiedBy;

	private String name;

	private boolean isNew;

	private String phone;

	private int version;

	public List<Address> getAddressList() {
		return addressList;
	}

	public void setAddressList(List<Address> addressList) {
		this.addressList = addressList;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public List<EducationRanking> getEducationRankingList() {
		return educationRankingList;
	}

	public void setEducationRankingList(
			List<EducationRanking> educationRankingList) {
		this.educationRankingList = educationRankingList;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getInstitutionType() {
		return institutionType;
	}

	public void setInstitutionType(String institutionType) {
		this.institutionType = institutionType;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Institution [addressList=").append(addressList)
				.append(", createdBy=").append(createdBy)
				.append(", educationRankingList=").append(educationRankingList)
				.append(", email=").append(email).append(", id=").append(id)
				.append(", institutionType=").append(institutionType)
				.append(", lastModifiedBy=").append(lastModifiedBy)
				.append(", name=").append(name).append(", isNew=")
				.append(isNew).append(", phone=").append(phone)
				.append(", version=").append(version).append("]");
		return builder.toString();
	}
}
