package com.fourdotfive.missioncontrol.pojo.job;

import com.fourdotfive.missioncontrol.licensepreference.JobLicensePreferences;
import com.fourdotfive.missioncontrol.pojo.company.CompanyCopy;
import com.fourdotfive.missioncontrol.pojo.user.Address;
import com.fourdotfive.missioncontrol.pojo.user.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Job {

    private String id;

    private String title;

    private Address location;

    private String description;

    private JobType jobType;

    private String jobCurrentState;

    private JobCandidatesAvailable jobCandidatesAvailable;
    private JobOpeningsAvailable jobOpeningsAvailable;

    private Integer numberOfOpenings;

    private Double fourDotFiveIntelligenceScore;
    private Double techAssessmentScore;
    private String typeOfSource;
    private String sourceText;
    private String executiveType;
    //private Title mainJobTitle;
    private int minYears, maxYears;
    //private Degree requiredDegree;

    private String requisitionNumber;
    private JobLicensePreferences licensePreferences;
    private String techAssessmentId;
    private String valueAssessmentId;

    private CompanyCopy company;

    private CompanyCopy clientorbu;

    private List<User> recruiters = new ArrayList<>();

    //private TechAssessmentMaster techAssessmentMaster;
    private List<CurrentJobState> currentJobStateList;
    private List<JobRequisition> jobRequisitionList;
    private List<JobOpening> jobOpenings;


    private List<Education> educationList;

    private List<Experience> experienceList;

    private List<Certifications> certifications;
    //private List<Compensation> compensationList = new ArrayList<>();

    private List<Skill> technicalSkills;
    private List<Skill> softSkills;
    private List<Skill> operationalSkills;

    //private final List<Title> jobTitles = new ArrayList<>();
    private List<String> requiredSkills = new ArrayList<>();
    private List<String> otherSkills = new ArrayList<>();
    private List<Skill> extraSkills;
    private List<Sponsorship> sponsorships;

    private String lastModifiedBy;
    //private DateTime lastModifiedDate;
    //private DateTime createdDate;
    private String createdBy;
    private List<String> vendorIds;

    private Map<String, String> daxtraParameters;
    private String advancedParametersImage;
    private String radius;
    private String recency;
    private List<User> hiringManager = new ArrayList<>();
    private List<HiringManagerInfo> HiringManagersInfos;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Address getLocation() {
        return location;
    }

    public void setLocation(Address location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public JobType getJobType() {
        return jobType;
    }

    public void setJobType(JobType jobType) {
        this.jobType = jobType;
    }

    public String getJobCurrentState() {
        return jobCurrentState;
    }

    public void setJobCurrentState(String jobCurrentState) {
        this.jobCurrentState = jobCurrentState;
    }

    public JobCandidatesAvailable getJobCandidatesAvailable() {
        return jobCandidatesAvailable;
    }

    public void setJobCandidatesAvailable(JobCandidatesAvailable jobCandidatesAvailable) {
        this.jobCandidatesAvailable = jobCandidatesAvailable;
    }

    public JobOpeningsAvailable getJobOpeningsAvailable() {
        return jobOpeningsAvailable;
    }

    public void setJobOpeningsAvailable(JobOpeningsAvailable jobOpeningsAvailable) {
        this.jobOpeningsAvailable = jobOpeningsAvailable;
    }

    public Integer getNumberOfOpenings() {
        return numberOfOpenings;
    }

    public void setNumberOfOpenings(Integer numberOfOpenings) {
        this.numberOfOpenings = numberOfOpenings;
    }

    public Double getFourDotFiveIntelligenceScore() {
        return fourDotFiveIntelligenceScore;
    }

    public void setFourDotFiveIntelligenceScore(Double fourDotFiveIntelligenceScore) {
        this.fourDotFiveIntelligenceScore = fourDotFiveIntelligenceScore;
    }

    public Double getTechAssessmentScore() {
        return techAssessmentScore;
    }

    public void setTechAssessmentScore(Double techAssessmentScore) {
        this.techAssessmentScore = techAssessmentScore;
    }

    public String getTypeOfSource() {
        return typeOfSource;
    }

    public void setTypeOfSource(String typeOfSource) {
        this.typeOfSource = typeOfSource;
    }

    public String getSourceText() {
        return sourceText;
    }

    public void setSourceText(String sourceText) {
        this.sourceText = sourceText;
    }

    public String getExecutiveType() {
        return executiveType;
    }

    public void setExecutiveType(String executiveType) {
        this.executiveType = executiveType;
    }

    public int getMinYears() {
        return minYears;
    }

    public void setMinYears(int minYears) {
        this.minYears = minYears;
    }

    public int getMaxYears() {
        return maxYears;
    }

    public void setMaxYears(int maxYears) {
        this.maxYears = maxYears;
    }

    public String getRequisitionNumber() {
        return requisitionNumber;
    }

    public void setRequisitionNumber(String requisitionNumber) {
        this.requisitionNumber = requisitionNumber;
    }

    public JobLicensePreferences getLicensePreferences() {
        return licensePreferences;
    }

    public void setLicensePreferences(JobLicensePreferences licensePreferences) {
        this.licensePreferences = licensePreferences;
    }

    public String getTechAssessmentId() {
        return techAssessmentId;
    }

    public void setTechAssessmentId(String techAssessmentId) {
        this.techAssessmentId = techAssessmentId;
    }

    public String getValueAssessmentId() {
        return valueAssessmentId;
    }

    public void setValueAssessmentId(String valueAssessmentId) {
        this.valueAssessmentId = valueAssessmentId;
    }

    public CompanyCopy getCompany() {
        return company;
    }

    public void setCompany(CompanyCopy company) {
        this.company = company;
    }

    public CompanyCopy getClientorbu() {
        return clientorbu;
    }

    public void setClientorbu(CompanyCopy clientorbu) {
        this.clientorbu = clientorbu;
    }

    public List<User> getRecruiters() {
        return recruiters;
    }

    public void setRecruiters(List<User> recruiters) {
        this.recruiters = recruiters;
    }

    public List<CurrentJobState> getCurrentJobStateList() {
        return currentJobStateList;
    }

    public void setCurrentJobStateList(List<CurrentJobState> currentJobStateList) {
        this.currentJobStateList = currentJobStateList;
    }

    public List<JobRequisition> getJobRequisitionList() {
        return jobRequisitionList;
    }

    public void setJobRequisitionList(List<JobRequisition> jobRequisitionList) {
        this.jobRequisitionList = jobRequisitionList;
    }

    public List<JobOpening> getJobOpenings() {
        return jobOpenings;
    }

    public void setJobOpenings(List<JobOpening> jobOpenings) {
        this.jobOpenings = jobOpenings;
    }

    public List<Education> getEducationList() {
        return educationList;
    }

    public void setEducationList(List<Education> educationList) {
        this.educationList = educationList;
    }

    public List<Experience> getExperienceList() {
        return experienceList;
    }

    public void setExperienceList(List<Experience> experienceList) {
        this.experienceList = experienceList;
    }

    public List<Certifications> getCertifications() {
        return certifications;
    }

    public void setCertifications(List<Certifications> certifications) {
        this.certifications = certifications;
    }

    public List<Skill> getTechnicalSkills() {
        return technicalSkills;
    }

    public void setTechnicalSkills(List<Skill> technicalSkills) {
        this.technicalSkills = technicalSkills;
    }

    public List<Skill> getSoftSkills() {
        return softSkills;
    }

    public void setSoftSkills(List<Skill> softSkills) {
        this.softSkills = softSkills;
    }

    public List<Skill> getOperationalSkills() {
        return operationalSkills;
    }

    public void setOperationalSkills(List<Skill> operationalSkills) {
        this.operationalSkills = operationalSkills;
    }

    public List<String> getRequiredSkills() {
        return requiredSkills;
    }

    public void setRequiredSkills(List<String> requiredSkills) {
        this.requiredSkills = requiredSkills;
    }

    public List<String> getOtherSkills() {
        return otherSkills;
    }

    public void setOtherSkills(List<String> otherSkills) {
        this.otherSkills = otherSkills;
    }

    public List<Skill> getExtraSkills() {
        return extraSkills;
    }

    public void setExtraSkills(List<Skill> extraSkills) {
        this.extraSkills = extraSkills;
    }

    public List<Sponsorship> getSponsorships() {
        return sponsorships;
    }

    public void setSponsorships(List<Sponsorship> sponsorships) {
        this.sponsorships = sponsorships;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

//	public DateTime getLastModifiedDate() {
//		return lastModifiedDate;
//	}
//
//	public void setLastModifiedDate(DateTime lastModifiedDate) {
//		this.lastModifiedDate = lastModifiedDate;
//	}
//
//	public DateTime getCreatedDate() {
//		return createdDate;
//	}
//
//	public void setCreatedDate(DateTime createdDate) {
//		this.createdDate = createdDate;
//	}

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Map<String, String> getDaxtraParameters() {
        return daxtraParameters;
    }

    public void setDaxtraParameters(Map<String, String> daxtraParameters) {
        this.daxtraParameters = daxtraParameters;
    }

    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }

    public String getRecency() {
        return recency;
    }

    public void setRecency(String recency) {
        this.recency = recency;
    }

    public String getAdvancedParametersImage() {
        return advancedParametersImage;
    }

    public void setAdvancedParametersImage(String advancedParametersImage) {
        this.advancedParametersImage = advancedParametersImage;
    }

    public List<String> getVendorIds() {
        return vendorIds;
    }

    public void setVendorIds(List<String> vendorIds) {
        this.vendorIds = vendorIds;
    }

    public List<User> getHiringManager() {
        return hiringManager;
    }

    public void setHiringManager(List<User> hiringManager) {
        this.hiringManager = hiringManager;
    }

    public List<HiringManagerInfo> getHiringManagersInfos() {
        return HiringManagersInfos;
    }

    public void setHiringManagersInfos(List<HiringManagerInfo> hiringManagersInfos) {
        HiringManagersInfos = hiringManagersInfos;
    }

    @Override
    public String toString() {
        return "Job{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", location=" + location +
                ", description='" + description + '\'' +
                ", jobType='" + jobType + '\'' +
                ", numberOfOpenings=" + numberOfOpenings +
                ", fourDotFiveIntelligenceScore=" + fourDotFiveIntelligenceScore +
                ", techAssessmentScore=" + techAssessmentScore +
                ", typeOfSource='" + typeOfSource + '\'' +
                ", sourceText='" + sourceText + '\'' +
                ", executiveType='" + executiveType + '\'' +
                ", minYears=" + minYears +
                ", maxYears=" + maxYears +
                ", requisitionNumber='" + requisitionNumber + '\'' +
                ", licensePreferences=" + licensePreferences +
                ", company=" + company +
                ", clientorbu=" + clientorbu +
                ", recruiters=" + recruiters +
                ", currentJobStateList=" + currentJobStateList +
                ", jobRequisitionList=" + jobRequisitionList +
                ", jobOpenings=" + jobOpenings +
                ", educationList=" + educationList +
                ", experienceList=" + experienceList +
                ", certifications=" + certifications +
                ", technicalSkills=" + technicalSkills +
                ", softSkills=" + softSkills +
                ", operationalSkills=" + operationalSkills +
                ", requiredSkills=" + requiredSkills +
                ", otherSkills=" + otherSkills +
                ", extraSkills=" + extraSkills +
                ", sponsorships=" + sponsorships +
                ", lastModifiedBy='" + lastModifiedBy + '\'' +
//				", lastModifiedDate=" + lastModifiedDate +
//				", createdDate=" + createdDate +
                ", createdBy='" + createdBy + '\'' +
                ", daxtraParameters=" + daxtraParameters +
                ", radius='" + radius + '\'' +
                ", recency='" + recency + '\'' +
                '}';
    }
}
