package com.fourdotfive.missioncontrol.pojo.job;

import com.fourdotfive.missioncontrol.dtos.job.HiringManagerDTO;
import com.fourdotfive.missioncontrol.dtos.job.JobSalaryDto;
import com.fourdotfive.missioncontrol.pojo.JobBoardDetails;
import com.fourdotfive.missioncontrol.pojo.user.Address;

import java.util.List;
import java.util.Map;

public class JobCreationModel {

    private String jobId;
    private String requistionNumber;
    private int openings;
    private String jobType;
    private String[] sponsorships;
    private String[] recruiters;
    private String[] vendorRecruiters;
    private Double score;
    private String techAssessmentId;
    private Double techAssessmentScore;
    private boolean techAssessment;
    private String valueAssessmentId;
    private boolean valueAssessment;
    private boolean eduVerification;
    private boolean expVeification;
    private String tab;
    private Boolean phoneScreen;
    private Boolean interview;
    private Boolean imageProctoring;
    private List<JobBoardDetails> jobBoardDetails;
    private Boolean autoMatch;
    private boolean recruiterScreening;
    private boolean addProctoringImagesToReport;
    private String title;
    private Address location;
    private String clientOrBUId;
    private Integer resumesToPullPerJobBoardCount;
    private Boolean isResumePullEnabled;
    private ButtonType buttonType;
    private String radius;
    private String recency;
    private List<String> vendorIds;
    private List<HiringManagerDTO> hiringManagers;
    private JobSalaryDto jobSalary;
    private ActionType actionType;
    private String userId;
    private Map<String, List<String>> workflowStepsOrder;
    private String pointOfContactUserId;
    private String sharedClientBuOrCorpId;
    private boolean isSharedClientRequisition;

    public String getRequistionNumber() {
        return requistionNumber;
    }

    public void setRequistionNumber(String requistionNumber) {
        this.requistionNumber = requistionNumber;
    }

    public int getOpenings() {
        return openings;
    }

    public void setOpenings(int openings) {
        this.openings = openings;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public String[] getSponsorships() {
        return sponsorships;
    }

    public void setSponsorships(String[] sponsorships) {
        this.sponsorships = sponsorships;
    }

    public String[] getRecruiters() {
        return recruiters;
    }

    public void setRecruiters(String[] recruiters) {
        this.recruiters = recruiters;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public String getTechAssessmentId() {
        return techAssessmentId;
    }

    public void setTechAssessmentId(String techAssessmentId) {
        this.techAssessmentId = techAssessmentId;
    }

    public Double getTechAssessmentScore() {
        return techAssessmentScore;
    }

    public void setTechAssessmentScore(Double techAssessmentScore) {
        this.techAssessmentScore = techAssessmentScore;
    }

    public boolean isValueAssessment() {
        return valueAssessment;
    }

    public void setValueAssessment(boolean valueAssessment) {
        this.valueAssessment = valueAssessment;
    }

    public boolean isEduVerification() {
        return eduVerification;
    }

    public void setEduVerification(boolean eduVerification) {
        this.eduVerification = eduVerification;
    }

    public boolean isExpVeification() {
        return expVeification;
    }

    public void setExpVeification(boolean expVeification) {
        this.expVeification = expVeification;
    }

    public String getTab() {
        return tab;
    }

    public void setTab(String tab) {
        this.tab = tab;
    }

    public boolean isTechAssessment() {
        return techAssessment;
    }

    public void setTechAssessment(boolean techAssessment) {
        this.techAssessment = techAssessment;
    }

    public String getValueAssessmentId() {
        return valueAssessmentId;
    }

    public void setValueAssessmentId(String valueAssessmentId) {
        this.valueAssessmentId = valueAssessmentId;
    }

    public Boolean getPhoneScreen() {
        return phoneScreen;
    }

    public void setPhoneScreen(Boolean phoneScreen) {
        this.phoneScreen = phoneScreen;
    }

    public Boolean getInterview() {
        return interview;
    }

    public void setInterview(Boolean interview) {
        this.interview = interview;
    }

    public Boolean getImageProctoring() {
        return imageProctoring;
    }

    public void setImageProctoring(Boolean imageProctoring) {
        this.imageProctoring = imageProctoring;
    }

    public List<JobBoardDetails> getJobBoardDetails() {
        return jobBoardDetails;
    }

    public void setJobBoardDetails(List<JobBoardDetails> jobBoardDetails) {
        this.jobBoardDetails = jobBoardDetails;
    }


    public boolean isRecruiterScreening() {
        return recruiterScreening;
    }

    public void setRecruiterScreening(boolean recruiterScreening) {
        this.recruiterScreening = recruiterScreening;
    }

    public Boolean getAutoMatch() {
        return autoMatch;
    }

    public void setAutoMatch(Boolean autoMatch) {
        this.autoMatch = autoMatch;
    }

    public boolean isAddProctoringImagesToReport() {
        return addProctoringImagesToReport;
    }

    public void setAddProctoringImagesToReport(boolean addProctoringImagesToReport) {
        this.addProctoringImagesToReport = addProctoringImagesToReport;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Address getLocation() {
        return location;
    }

    public void setLocation(Address location) {
        this.location = location;
    }

    public String getClientOrBUId() {
        return clientOrBUId;
    }

    public void setClientOrBUId(String clientOrBUId) {
        this.clientOrBUId = clientOrBUId;
    }

    public Integer getResumesToPullPerJobBoardCount() {
        return resumesToPullPerJobBoardCount;
    }

    public void setResumesToPullPerJobBoardCount(Integer resumesToPullPerJobBoardCount) {
        this.resumesToPullPerJobBoardCount = resumesToPullPerJobBoardCount;
    }

    public Boolean getResumePullEnabled() {
        return isResumePullEnabled;
    }

    public void setResumePullEnabled(Boolean resumePullEnabled) {
        isResumePullEnabled = resumePullEnabled;
    }

    public ButtonType getButtonType() {
        return buttonType;
    }

    public void setButtonType(ButtonType buttonType) {
        this.buttonType = buttonType;
    }

    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }

    public String getRecency() {
        return recency;
    }

    public void setRecency(String recency) {
        this.recency = recency;
    }

    public List<String> getVendorIds() {
        return vendorIds;
    }

    public void setVendorIds(List<String> vendorIds) {
        this.vendorIds = vendorIds;
    }

    public List<HiringManagerDTO> getHiringManagers() {
        return hiringManagers;
    }

    public void setHiringManagers(List<HiringManagerDTO> hiringManagers) {
        this.hiringManagers = hiringManagers;
    }

    public String[] getVendorRecruiters() {
        return vendorRecruiters;
    }

    public void setVendorRecruiters(String[] vendorRecruiters) {
        this.vendorRecruiters = vendorRecruiters;
    }

    public JobSalaryDto getJobSalary() {
        return jobSalary;
    }

    public void setJobSalary(JobSalaryDto jobSalary) {
        this.jobSalary = jobSalary;
    }

    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Map<String, List<String>> getWorkflowStepsOrder() {
        return workflowStepsOrder;
    }

    public void setWorkflowStepsOrder(Map<String, List<String>> workflowStepsOrder) {
        this.workflowStepsOrder = workflowStepsOrder;
    }

    public String getPointOfContactUserId() {
        return pointOfContactUserId;
    }

    public void setPointOfContactUserId(String pointOfContactUserId) {
        this.pointOfContactUserId = pointOfContactUserId;
    }

    public String getSharedClientBuOrCorpId() {
        return sharedClientBuOrCorpId;
    }

    public void setSharedClientBuOrCorpId(String sharedClientBuOrCorpId) {
        this.sharedClientBuOrCorpId = sharedClientBuOrCorpId;
    }

    public boolean getIsSharedClientRequisition() {
        return isSharedClientRequisition;
    }

    public void setIsSharedClientRequisition(boolean sharedClientRequisition) {
        isSharedClientRequisition = sharedClientRequisition;
    }
}
