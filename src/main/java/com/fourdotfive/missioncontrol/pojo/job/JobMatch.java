package com.fourdotfive.missioncontrol.pojo.job;

import com.fourdotfive.missioncontrol.pojo.candidate.Candidate;
import com.fourdotfive.missioncontrol.pojo.user.User;

import java.util.List;

public class JobMatch {

	private Candidate candidate;

	private User assignedRecruiter;

	private String createdBy;

	private String id;

	private Job job;

	private List<JobStateStep> jobStateSteps;

	private String lastModifiedBy;

	private boolean matched;

	private boolean isNew;

	private Boolean isDeleted;

	private JobmatchStatus jobMatchStatus;

	private Double score;

	private String currentStep;

	private String currentState;
	
	//Need more information on assessments
	//start
	private Assessment techAssessment;
	private Assessment valueAssessment;
	private Assessment verificationAssessment;

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	//end

	public Candidate getCandidate() {
		return candidate;
	}

	public void setCandidate(Candidate candidate) {
		this.candidate = candidate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public List<JobStateStep> getJobStateSteps() {
		return jobStateSteps;
	}

	public void setJobStateSteps(List<JobStateStep> jobStateSteps) {
		this.jobStateSteps = jobStateSteps;
	}

	public boolean isMatched() {
		return matched;
	}

	public void setMatched(boolean matched) {
		this.matched = matched;
	}

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	public Assessment getTechAssessment() {
		return techAssessment;
	}

	public void setTechAssessment(Assessment techAssessment) {
		this.techAssessment = techAssessment;
	}

	public Assessment getValueAssessment() {
		return valueAssessment;
	}

	public void setValueAssessment(Assessment valueAssessment) {
		this.valueAssessment = valueAssessment;
	}

	public Assessment getVerificationAssessment() {
		return verificationAssessment;
	}

	public void setVerificationAssessment(Assessment verificationAssessment) {
		this.verificationAssessment = verificationAssessment;
	}

	public String getCurrentStep() {
		return currentStep;
	}

	public void setCurrentStep(String currentStep) {
		this.currentStep = currentStep;
	}

	public String getCurrentState() {
		return currentState;
	}

	public void setCurrentState(String currentState) {
		this.currentState = currentState;
	}

	public JobmatchStatus getJobMatchStatus() {
		return jobMatchStatus;
	}

	public void setJobMatchStatus(JobmatchStatus jobMatchStatus) {
		this.jobMatchStatus = jobMatchStatus;
	}

	public User getAssignedRecruiter() {
		return assignedRecruiter;
	}

	public void setAssignedRecruiter(User assignedRecruiter) {
		this.assignedRecruiter = assignedRecruiter;
	}

	public Boolean getDeleted() {
		return isDeleted;
	}

	public void setDeleted(Boolean deleted) {
		isDeleted = deleted;
	}

	@Override
	public String toString() {
		return "JobMatch{" +
				"candidate=" + candidate +
				", assignedRecruiter=" + assignedRecruiter +
				", id='" + id + '\'' +
				", job=" + job +
				", jobStateSteps=" + jobStateSteps +
				", matched=" + matched +
				", isNew=" + isNew +
				", jobMatchStatus=" + jobMatchStatus +
				", score=" + score +
				", currentStep='" + currentStep + '\'' +
				", currentState='" + currentState + '\'' +
				", techAssessment=" + techAssessment +
				", valueAssessment=" + valueAssessment +
				", verificationAssessment=" + verificationAssessment +
				'}';
	}
}
