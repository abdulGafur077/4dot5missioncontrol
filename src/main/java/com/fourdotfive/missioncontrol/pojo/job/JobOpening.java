package com.fourdotfive.missioncontrol.pojo.job;

import java.util.Date;
import java.util.List;

import com.fourdotfive.missioncontrol.pojo.candidate.Candidate;
import com.fourdotfive.missioncontrol.pojo.user.User;

/**
 * Job Opening Entity
 *
 */
public class JobOpening {

	private Integer openingNumber;

	private String requisitionOpeningNumber;

	private List<User> assignedRecruiters;

	private Candidate assignedCandidate;

	private Date fillDate = new Date();

	private Date joinDate = new Date();

	private String note;

	private Candidate fillCandidate;

	private Candidate joinCandidate;

	/**
	 * get openingNumber
	 * 
	 * @return the openingNumber
	 *
	 */
	public Integer getOpeningNumber() {
		return openingNumber;
	}

	/**
	 * set openingNumber
	 * 
	 * @param openingNumber
	 *            the openingNumber to set
	 *
	 */
	public void setOpeningNumber(Integer openingNumber) {
		this.openingNumber = openingNumber;
	}

	/**
	 * get requisitionOpeningNumber
	 * 
	 * @return the requisitionOpeningNumber
	 *
	 */
	public String getRequisitionOpeningNumber() {
		return requisitionOpeningNumber;
	}

	/**
	 * set requisitionOpeningNumber
	 * 
	 * @param requisitionOpeningNumber
	 *            the requisitionOpeningNumber to set
	 *
	 */
	public void setRequisitionOpeningNumber(String requisitionOpeningNumber) {
		this.requisitionOpeningNumber = requisitionOpeningNumber;
	}

	/**
	 * get assignedRecruiters
	 * 
	 * @return the assignedRecruiters
	 *
	 */
	public List<User> getAssignedRecruiters() {
		return assignedRecruiters;
	}

	/**
	 * set assignedRecruiters
	 * 
	 * @param assignedRecruiters
	 *            the assignedRecruiters to set
	 *
	 */
	public void setAssignedRecruiters(List<User> assignedRecruiters) {
		this.assignedRecruiters = assignedRecruiters;
	}

	/**
	 * get assignedCandidate
	 * 
	 * @return the assignedCandidate
	 *
	 */
	public Candidate getAssignedCandidate() {
		return assignedCandidate;
	}

	/**
	 * set assignedCandidate
	 * 
	 * @param assignedCandidate
	 *            the assignedCandidate to set
	 *
	 */
	public void setAssignedCandidate(Candidate assignedCandidate) {
		this.assignedCandidate = assignedCandidate;
	}

	/**
	 * get fillDate
	 * 
	 * @return the fillDate
	 *
	 */
	public Date getFillDate() {
		return fillDate;
	}

	/**
	 * set fillDate
	 * 
	 * @param fillDate
	 *            the fillDate to set
	 *
	 */
	public void setFillDate(Date fillDate) {
		this.fillDate = fillDate;
	}

	/**
	 * get note
	 * 
	 * @return the note
	 *
	 */
	public String getNote() {
		return note;
	}

	/**
	 * set note
	 * 
	 * @param note
	 *            the note to set
	 *
	 */
	public void setNote(String note) {
		this.note = note;
	}


	public Date getJoinDate() {
		return joinDate;
	}

	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}

	public Candidate getFillCandidate() {
		return fillCandidate;
	}

	public void setFillCandidate(Candidate fillCandidate) {
		this.fillCandidate = fillCandidate;
	}

	public Candidate getJoinCandidate() {
		return joinCandidate;
	}

	public void setJoinCandidate(Candidate joinCandidate) {
		this.joinCandidate = joinCandidate;
	}
}