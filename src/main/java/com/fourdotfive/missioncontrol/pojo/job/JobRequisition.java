package com.fourdotfive.missioncontrol.pojo.job;

import com.fourdotfive.missioncontrol.dtos.company.CompanyMinInfoDto;
import com.fourdotfive.missioncontrol.pojo.user.User;

import java.util.List;

//import com.fourdotfive.missioncontrol.pojo.company.Company;

public class JobRequisition {

	private List<CompanyMinInfoDto> clientsOrBUs;

	private String cloudFileLocation;

	private CompanyMinInfoDto company;

	private String createdBy;

	//private FourDotFiveDate createdDate;

	private String fileName;

	private int fileSize;

	private String id;

	private String lastModifiedBy;

	//private FourDotFiveDate lastModifiedDate;

	private int length;

	private boolean isNew;

	private List<Resume> resumes;

	private String uploadedTime;

	private User user;

	private int version;

	private String transactionId;

	private String originalRequisitionName;

	public List<CompanyMinInfoDto> getClientsOrBUs() {
		return clientsOrBUs;
	}

	public void setClientsOrBUs(List<CompanyMinInfoDto> clientsOrBUs) {
		this.clientsOrBUs = clientsOrBUs;
	}

	public String getCloudFileLocation() {
		return cloudFileLocation;
	}

	public void setCloudFileLocation(String cloudFileLocation) {
		this.cloudFileLocation = cloudFileLocation;
	}

	public CompanyMinInfoDto getCompany() {
		return company;
	}

	public void setCompany(CompanyMinInfoDto company) {
		this.company = company;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public int getFileSize() {
		return fileSize;
	}

	public void setFileSize(int fileSize) {
		this.fileSize = fileSize;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

	public List<Resume> getResumes() {
		return resumes;
	}

	public void setResumes(List<Resume> resumes) {
		this.resumes = resumes;
	}

	public String getUploadedTime() {
		return uploadedTime;
	}

	public void setUploadedTime(String uploadedTime) {
		this.uploadedTime = uploadedTime;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
	
	/*public FourDotFiveDate getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(FourDotFiveDate createdDate) {
		this.createdDate = createdDate;
	}

	public FourDotFiveDate getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(FourDotFiveDate lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	*/

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getOriginalRequisitionName() {
		return originalRequisitionName;
	}

	public void setOriginalRequisitionName(String originalRequisitionName) {
		this.originalRequisitionName = originalRequisitionName;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("JobRequisition [clientsOrBUs=").append(clientsOrBUs)
				.append(", cloudFileLocation=").append(cloudFileLocation)
				.append(", company=").append(company).append(", createdBy=")
			//	.append(createdBy).append(", createdDate=").append(createdDate)
				.append(", fileName=").append(fileName).append(", fileSize=")
				.append(fileSize).append(", id=").append(id)
				.append(", lastModifiedBy=").append(lastModifiedBy)
			//	.append(", lastModifiedDate=").append(lastModifiedDate)
				.append(", length=").append(length).append(", isNew=")
				.append(isNew).append(", resumes=").append(resumes)
				.append(", uploadedTime=").append(uploadedTime)
				.append(", user=").append(user).append(", version=")
				.append(version).append("]");
		return builder.toString();
	}

}
