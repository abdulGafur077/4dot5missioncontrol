package com.fourdotfive.missioncontrol.pojo.job;

public class JobScoreDTO {

    private String jobId;
    private Double score;

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }
}
