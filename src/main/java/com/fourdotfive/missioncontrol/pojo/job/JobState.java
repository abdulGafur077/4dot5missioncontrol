package com.fourdotfive.missioncontrol.pojo.job;

import com.fourdotfive.missioncontrol.dtos.jobmatch.ReasonCodeDTO;

import java.util.Date;

/**
 * Job Change State Value Object
 *
 * @author rvadlam
 */
public class JobState {

    private String jobId;
    private JobStateType jobStateType;
    private String note;
    private String modifiedBy;
    private Date statusDate;
    private String requisitionOpeningNumber;
    private String candidateId;
    private String recruiterId;
    private String direction;
    private String externalNote;
    private String removeFillOrJoinNote;
    private ReasonCodeDTO reasonCodeDTO;

    /**
     * get jobId
     *
     * @return the jobId
     */
    public String getJobId() {
        return jobId;
    }

    /**
     * set jobId
     *
     * @param jobId the jobId to set
     */
    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    /**
     * get jobStateType
     *
     * @return the jobStateType
     */
    public JobStateType getJobStateType() {
        return jobStateType;
    }

    /**
     * set jobStateType
     *
     * @param jobStateType the jobStateType to set
     */
    public void setJobStateType(JobStateType jobStateType) {
        this.jobStateType = jobStateType;
    }

    /**
     * get note
     *
     * @return the note
     */
    public String getNote() {
        return note;
    }

    /**
     * set note
     *
     * @param note the note to set
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     * get modifiedBy
     *
     * @return the modifiedBy
     */
    public String getModifiedBy() {
        return modifiedBy;
    }

    /**
     * set modifiedBy
     *
     * @param modifiedBy the modifiedBy to set
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * get statusDate
     *
     * @return the statusDate
     */
    public Date getStatusDate() {
        return statusDate;
    }

    /**
     * set statusDate
     *
     * @param statusDate the statusDate to set
     */
    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    /**
     * get requisitionOpeningNumber
     *
     * @return the requisitionOpeningNumber
     */
    public String getRequisitionOpeningNumber() {
        return requisitionOpeningNumber;
    }

    /**
     * set requisitionOpeningNumber
     *
     * @param requisitionOpeningNumber the requisitionOpeningNumber to set
     */
    public void setRequisitionOpeningNumber(String requisitionOpeningNumber) {
        this.requisitionOpeningNumber = requisitionOpeningNumber;
    }

    /**
     * get candidateId
     *
     * @return the candidateId
     */
    public String getCandidateId() {
        return candidateId;
    }

    /**
     * set candidateId
     *
     * @param candidateId the candidateId to set
     */
    public void setCandidateId(String candidateId) {
        this.candidateId = candidateId;
    }

    /**
     * get recruiterId
     *
     * @return the recruiterId
     */
    public String getRecruiterId() {
        return recruiterId;
    }

    /**
     * set recruiterId
     *
     * @param recruiterId the recruiterId to set
     */
    public void setRecruiterId(String recruiterId) {
        this.recruiterId = recruiterId;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getExternalNote() {
        return externalNote;
    }

    public void setExternalNote(String externalNote) {
        this.externalNote = externalNote;
    }

    public String getRemoveFillOrJoinNote() {
        return removeFillOrJoinNote;
    }

    public void setRemoveFillOrJoinNote(String removeFillOrJoinNote) {
        this.removeFillOrJoinNote = removeFillOrJoinNote;
    }

    public ReasonCodeDTO getReasonCodeDTO() {
        return reasonCodeDTO;
    }

    public void setReasonCodeDTO(ReasonCodeDTO reasonCodeDTO) {
        this.reasonCodeDTO = reasonCodeDTO;
    }
}

