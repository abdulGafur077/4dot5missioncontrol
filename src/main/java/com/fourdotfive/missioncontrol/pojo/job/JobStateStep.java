package com.fourdotfive.missioncontrol.pojo.job;

import com.fourdotfive.missioncontrol.pojo.FourDotFiveDate;
import com.fourdotfive.missioncontrol.pojo.candidate.ActiveCandidateJobStateType;
import com.fourdotfive.missioncontrol.pojo.candidate.ActiveCandidateJobStepType;
import com.fourdotfive.missioncontrol.pojo.candidate.PassiveCandidateJobStateType;
import com.fourdotfive.missioncontrol.pojo.candidate.PassiveCandidateJobStepType;

public class JobStateStep {

	private ActiveCandidateJobStateType activeCandidateJobStateType;

	private ActiveCandidateJobStepType activeCandidateJobStepType;

	private PassiveCandidateJobStateType passiveCandidateJobStateType;

	private PassiveCandidateJobStepType passiveCandidateJobStepType;

	private String createdBy;

	private FourDotFiveDate createdDate;

	private String date;

	private String id;

	private Job job;

	private String lastModifiedBy;

	private FourDotFiveDate lastModifiedDate;

	private boolean matched;
	
	private String note;

	private boolean isNew;

	private int version;

	public ActiveCandidateJobStateType getActiveCandidateJobStateType() {
		return activeCandidateJobStateType;
	}

	public void setActiveCandidateJobStateType(ActiveCandidateJobStateType activeCandidateJobStateType) {
		this.activeCandidateJobStateType = activeCandidateJobStateType;
	}

	public ActiveCandidateJobStepType getActiveCandidateJobStepType() {
		return activeCandidateJobStepType;
	}

	public void setActiveCandidateJobStepType(ActiveCandidateJobStepType activeCandidateJobStepType) {
		this.activeCandidateJobStepType = activeCandidateJobStepType;
	}

	public PassiveCandidateJobStateType getPassiveCandidateJobStateType() {
		return passiveCandidateJobStateType;
	}

	public void setPassiveCandidateJobStateType(PassiveCandidateJobStateType passiveCandidateJobStateType) {
		this.passiveCandidateJobStateType = passiveCandidateJobStateType;
	}

	public PassiveCandidateJobStepType getPassiveCandidateJobStepType() {
		return passiveCandidateJobStepType;
	}

	public void setPassiveCandidateJobStepType(PassiveCandidateJobStepType passiveCandidateJobStepType) {
		this.passiveCandidateJobStepType = passiveCandidateJobStepType;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public FourDotFiveDate getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(FourDotFiveDate createdDate) {
		this.createdDate = createdDate;
	}

	public FourDotFiveDate getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(FourDotFiveDate lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public boolean isMatched() {
		return matched;
	}

	public void setMatched(boolean matched) {
		this.matched = matched;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("JobStateStep [candidateActiveJobStateType=")
				.append(activeCandidateJobStateType)
				.append(", candidateActiveJobStepType=")
				.append(activeCandidateJobStepType)
				.append(", candidatePassiveJobStateType=")
				.append(passiveCandidateJobStateType)
				.append(", candidatePassiveJobStepType=")
				.append(passiveCandidateJobStepType).append(", createdBy=")
				.append(createdBy).append(", createdDate=").append(createdDate)
				.append(", date=").append(date).append(", id=").append(id)
				.append(", job=").append(job).append(", lastModifiedBy=")
				.append(lastModifiedBy).append(", lastModifiedDate=")
				.append(lastModifiedDate).append(", matched=").append(matched)
				.append(", note=").append(note)
				.append(", isNew=").append(isNew).append(", version=")
				.append(version).append("]");
		return builder.toString();
	}

}
