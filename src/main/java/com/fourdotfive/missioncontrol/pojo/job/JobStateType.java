package com.fourdotfive.missioncontrol.pojo.job;

public enum JobStateType {

	DRAFT("Draft"), OPEN("Open"), SOURCED("Sourced"), FILLED("Filled"), JOINED("Joined"), COMPLETED(
			"Completed"), PAUSED("Paused"), CANCELED("Canceled"), ARCHIVED(
			"Archived");

	private String name;

	private JobStateType(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
