package com.fourdotfive.missioncontrol.pojo.job;

public enum JobType {

    FULLTIME("Full Time"), PARTTIME("Part Time"), CONTRACTTOHIRE(
            "Contract to Hire"), INTERN("Intern"), SEASONALORTEMP(
            "Seasonal/Temp"), CONTRACTOR("Contractor");

    private String name;

    private JobType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}

