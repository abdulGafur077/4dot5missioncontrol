package com.fourdotfive.missioncontrol.pojo.job;

public enum JobmatchStatus {
	
	ACTIVE("ACTIVE"),PASSIVE("PASSIVE");

	private String text;

	JobmatchStatus(String text) {
		this.text = text;
	}

	public String getText() {
		return this.text;
	}

}
