package com.fourdotfive.missioncontrol.pojo.job;

import java.util.List;

public class ParsingOutputObject {
	
	private String transactionID;
	private List<HrXmlFileInfo> files;
	private long successCount;
	private long failureCount;
	
	public String getTransactionID() {
		return transactionID;
	}
	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}
	
	public long getSuccessCount() {
		return successCount;
	}
	public void setSuccessCount(long successCount) {
		this.successCount = successCount;
	}
	public long getFailureCount() {
		return failureCount;
	}
	public void setFailureCount(long failureCount) {
		this.failureCount = failureCount;
	}
	public List<HrXmlFileInfo> getFiles() {
		return files;
	}
	public void setFiles(List<HrXmlFileInfo> files) {
		this.files = files;
	}
	
	
	

}

