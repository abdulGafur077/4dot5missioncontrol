package com.fourdotfive.missioncontrol.pojo.job;

import java.util.List;

import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.user.JobRequisition;
import com.fourdotfive.missioncontrol.pojo.user.User;

public class Resume {

	private List<Company> clientOrBUs;

	private String cloudFileLocation;

	private Company company;

	private String createdBy;

	private String createdDate;

	private String fileName;

	private int fileSize;

	private String id;

	private List<JobRequisition> jobRequisitions;

	private String lastModifiedDate;

	private int length;

	private boolean isNew;

	private String uploadedTime;

	private User user;

	private int version;

	private String otherSourceTypeNotes;

	private String referrerName;

	private String referrerEmail;

	private boolean isFromOtherSource;

	private boolean isReferred;

	public List<Company> getClientOrBUs() {
		return clientOrBUs;
	}

	public void setClientOrBUs(List<Company> clientOrBUs) {
		this.clientOrBUs = clientOrBUs;
	}

	public String getCloudFileLocation() {
		return cloudFileLocation;
	}

	public void setCloudFileLocation(String cloudFileLocation) {
		this.cloudFileLocation = cloudFileLocation;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public void setLastModifiedDate(String lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public int getFileSize() {
		return fileSize;
	}

	public void setFileSize(int fileSize) {
		this.fileSize = fileSize;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<JobRequisition> getJobRequisitions() {
		return jobRequisitions;
	}

	public void setJobRequisitions(List<JobRequisition> jobRequisitions) {
		this.jobRequisitions = jobRequisitions;
	}

	public String getLastModifiedDate() {
		return lastModifiedDate;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

	public String getUploadedTime() {
		return uploadedTime;
	}

	public void setUploadedTime(String uploadedTime) {
		this.uploadedTime = uploadedTime;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getOtherSourceTypeNotes() {
		return otherSourceTypeNotes;
	}

	public void setOtherSourceTypeNotes(String otherSourceTypeNotes) {
		this.otherSourceTypeNotes = otherSourceTypeNotes;
	}

	public String getReferrerName() {
		return referrerName;
	}

	public void setReferrerName(String referrerName) {
		this.referrerName = referrerName;
	}

	public String getReferrerEmail() {
		return referrerEmail;
	}

	public void setReferrerEmail(String referrerEmail) {
		this.referrerEmail = referrerEmail;
	}

	public boolean isFromOtherSource() {
		return isFromOtherSource;
	}

	public void setFromOtherSource(boolean fromOtherSource) {
		isFromOtherSource = fromOtherSource;
	}

	public boolean isReferred() {
		return isReferred;
	}

	public void setReferred(boolean referred) {
		isReferred = referred;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Resume [clientOrBUs=").append(clientOrBUs)
				.append(", cloudFileLocation=").append(cloudFileLocation)
				.append(", company=").append(company).append(", createdBy=")
				.append(createdBy).append(", createdDate=").append(createdDate)
				.append(", fileName=").append(fileName).append(", fileSize=")
				.append(fileSize).append(", id=").append(id)
				.append(", jobRequisitions=").append(jobRequisitions)
				.append(", lastModifiedDate=").append(lastModifiedDate)
				.append(", length=").append(length).append(", isNew=")
				.append(isNew).append(", uploadedTime=").append(uploadedTime)
				.append(", user=").append(user).append(", version=")
				.append(version).append("]");
		return builder.toString();
	}

}
