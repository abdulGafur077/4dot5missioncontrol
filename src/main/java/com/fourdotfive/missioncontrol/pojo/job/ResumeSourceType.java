
package com.fourdotfive.missioncontrol.pojo.job;

public class ResumeSourceType {

	private String id;

	private String sourceType;

	private String daxtraCode;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	public String getDaxtraCode() {
		return daxtraCode;
	}

	public void setDaxtraCode(String daxtraCode) {
		this.daxtraCode = daxtraCode;
	}
}