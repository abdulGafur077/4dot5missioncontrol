
package com.fourdotfive.missioncontrol.pojo.job;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Resume Upload
 * 
 * @author rvadlam
 *
 */
public class ResumeUpload implements Serializable {

	private static final long serialVersionUID = -5866844614831654884L;

	private String userId;

	private String companyId;

	private String resumesourcetypeId;	
	
	private List<String> buIdList = new ArrayList<>();

	private String otherSourceTypeNotes;

	private String referrerName;

	private String referrerEmail;

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the companyId
	 */
	public String getCompanyId() {
		return companyId;
	}

	/**
	 * @param companyId the companyId to set
	 */
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	/**
	 * @return the buIdList
	 */
	public List<String> getBuIdList() {
		return buIdList;
	}

	/**
	 * @param buIdList the buIdList to set
	 */
	public void setBuIdList(List<String> buIdList) {
		this.buIdList = buIdList;
	}

	/**
	 * @return the resumesourcetypeId
	 */
	public String getResumesourcetypeId() {
		return resumesourcetypeId;
	}

	/**
	 * @param resumesourcetypeId the resumesourcetypeId to set
	 */
	public void setResumesourcetypeId(String resumesourcetypeId) {
		this.resumesourcetypeId = resumesourcetypeId;
	}

	public String getOtherSourceTypeNotes() {
		return otherSourceTypeNotes;
	}

	public void setOtherSourceTypeNotes(String otherSourceTypeNotes) {
		this.otherSourceTypeNotes = otherSourceTypeNotes;
	}

	public String getReferrerName() {
		return referrerName;
	}

	public void setReferrerName(String referrerName) {
		this.referrerName = referrerName;
	}

	public String getReferrerEmail() {
		return referrerEmail;
	}

	public void setReferrerEmail(String referrerEmail) {
		this.referrerEmail = referrerEmail;
	}
}
