package com.fourdotfive.missioncontrol.pojo.job;

public class SearchInterfaceRequest {
    private String dbCode;
    private String jobBoardCodes;
    private String ajaxAutoCompleter;
    private String ajaxUpdater;
    private String jsLib;
    private String jsonp;
    private String libs;
    private String locale;
    private String locationCountries;
    private String locationCountry;
    private String postcodeCountries;
    private String postcodeCountry;
    private String relayParam;
    private String salaryCurrency;
    private String style;

    public SearchInterfaceRequest() {
    }

    public SearchInterfaceRequest(String dbCode, String jobBoardCodes, String ajaxAutoCompleter, String ajaxUpdater
            , String jsLib, String jsonp, String libs, String locale, String locationCountries, String locationCountry
            , String postcodeCountries, String postcodeCountry, String relayParam, String salaryCurrency, String style) {
        this.dbCode = dbCode;
        this.jobBoardCodes = jobBoardCodes;
        this.ajaxAutoCompleter = ajaxAutoCompleter;
        this.ajaxUpdater = ajaxUpdater;
        this.jsLib = jsLib;
        this.jsonp = jsonp;
        this.libs = libs;
        this.locale = locale;
        this.locationCountries = locationCountries;
        this.locationCountry = locationCountry;
        this.postcodeCountries = postcodeCountries;
        this.postcodeCountry = postcodeCountry;
        this.relayParam = relayParam;
        this.salaryCurrency = salaryCurrency;
        this.style = style;
    }

    public String getDbCode() {
        return dbCode;
    }

    public void setDbCode(String dbCode) {
        this.dbCode = dbCode;
    }

    public String getJobBoardCodes() {
        return jobBoardCodes;
    }

    public void setJobBoardCodes(String jobBoardCodes) {
        this.jobBoardCodes = jobBoardCodes;
    }

    public String getAjaxAutoCompleter() {
        return ajaxAutoCompleter;
    }

    public void setAjaxAutoCompleter(String ajaxAutoCompleter) {
        this.ajaxAutoCompleter = ajaxAutoCompleter;
    }

    public String getAjaxUpdater() {
        return ajaxUpdater;
    }

    public void setAjaxUpdater(String ajaxUpdater) {
        this.ajaxUpdater = ajaxUpdater;
    }

    public String getJsLib() {
        return jsLib;
    }

    public void setJsLib(String jsLib) {
        this.jsLib = jsLib;
    }

    public String getJsonp() {
        return jsonp;
    }

    public void setJsonp(String jsonp) {
        this.jsonp = jsonp;
    }

    public String getLibs() {
        return libs;
    }

    public void setLibs(String libs) {
        this.libs = libs;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getLocationCountries() {
        return locationCountries;
    }

    public void setLocationCountries(String locationCountries) {
        this.locationCountries = locationCountries;
    }

    public String getLocationCountry() {
        return locationCountry;
    }

    public void setLocationCountry(String locationCountry) {
        this.locationCountry = locationCountry;
    }

    public String getPostcodeCountries() {
        return postcodeCountries;
    }

    public void setPostcodeCountries(String postcodeCountries) {
        this.postcodeCountries = postcodeCountries;
    }

    public String getPostcodeCountry() {
        return postcodeCountry;
    }

    public void setPostcodeCountry(String postcodeCountry) {
        this.postcodeCountry = postcodeCountry;
    }

    public String getRelayParam() {
        return relayParam;
    }

    public void setRelayParam(String relayParam) {
        this.relayParam = relayParam;
    }

    public String getSalaryCurrency() {
        return salaryCurrency;
    }

    public void setSalaryCurrency(String salaryCurrency) {
        this.salaryCurrency = salaryCurrency;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    @Override
    public String toString() {
        return "SearchInterfaceRequest{" +
                "dbCode='" + dbCode + '\'' +
                ", jobBoardCodes='" + jobBoardCodes + '\'' +
                ", ajaxAutoCompleter='" + ajaxAutoCompleter + '\'' +
                ", ajaxUpdater='" + ajaxUpdater + '\'' +
                ", jsLib='" + jsLib + '\'' +
                ", jsonp='" + jsonp + '\'' +
                ", libs='" + libs + '\'' +
                ", locale='" + locale + '\'' +
                ", locationCountries='" + locationCountries + '\'' +
                ", locationCountry='" + locationCountry + '\'' +
                ", postcodeCountries='" + postcodeCountries + '\'' +
                ", postcodeCountry='" + postcodeCountry + '\'' +
                ", relayParam='" + relayParam + '\'' +
                ", salaryCurrency='" + salaryCurrency + '\'' +
                ", style='" + style + '\'' +
                '}';
    }
}
