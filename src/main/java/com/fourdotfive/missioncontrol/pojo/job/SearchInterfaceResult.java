package com.fourdotfive.missioncontrol.pojo.job;


public class SearchInterfaceResult {
    private String htmlContent;
    private String jsContent;
    private String cssContent;
    private String jbParamsJq;
    private String jQuery;
    private String jQueryUi;

    public String getHtmlContent() {
        return htmlContent;
    }

    public void setHtmlContent(String htmlContent) {
        this.htmlContent = htmlContent;
    }

    public String getJsContent() {
        return jsContent;
    }

    public void setJsContent(String jsContent) {
        this.jsContent = jsContent;
    }

    public String getCssContent() {
        return cssContent;
    }

    public void setCssContent(String cssContent) {
        this.cssContent = cssContent;
    }

    public String getJbParamsJq() {
        return jbParamsJq;
    }

    public void setJbParamsJq(String jbParamsJq) {
        this.jbParamsJq = jbParamsJq;
    }

    public String getjQuery() {
        return jQuery;
    }

    public void setjQuery(String jQuery) {
        this.jQuery = jQuery;
    }

    public String getjQueryUi() {
        return jQueryUi;
    }

    public void setjQueryUi(String jQueryUi) {
        this.jQueryUi = jQueryUi;
    }
}
