package com.fourdotfive.missioncontrol.pojo.job;


import java.util.List;

public class SharedRequisitionCheckDTO {
    private boolean sharedRequisition;
    private String requisitionId;
    private String corporateId;
    private List<String> vendorIds;
    private String buId;

    public boolean isSharedRequisition() {
        return sharedRequisition;
    }

    public void setSharedRequisition(boolean sharedRequisition) {
        this.sharedRequisition = sharedRequisition;
    }

    public String getRequisitionId() {
        return requisitionId;
    }

    public void setRequisitionId(String requisitionId) {
        this.requisitionId = requisitionId;
    }

    public String getCorporateId() {
        return corporateId;
    }

    public void setCorporateId(String corporateId) {
        this.corporateId = corporateId;
    }

    public List<String> getVendorIds() {
        return vendorIds;
    }

    public void setVendorIds(List<String> vendorIds) {
        this.vendorIds = vendorIds;
    }

    public String getBuId() {
        return buId;
    }

    public void setBuId(String buId) {
        this.buId = buId;
    }
}
