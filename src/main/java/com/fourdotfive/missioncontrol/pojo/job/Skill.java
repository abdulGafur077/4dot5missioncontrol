package com.fourdotfive.missioncontrol.pojo.job;

public class Skill {
	private String competency;

	private int experienceInMonths;

	private String skillType;

	public String getCompetency() {
		return competency;
	}

	public void setCompetency(String competency) {
		this.competency = competency;
	}

	public int getExperienceInMonths() {
		return experienceInMonths;
	}

	public void setExperienceInMonths(int experienceInMonths) {
		this.experienceInMonths = experienceInMonths;
	}

	public String getSkillType() {
		return skillType;
	}

	public void setSkillType(String skillType) {
		this.skillType = skillType;
	}

	@Override
	public String toString() {
		return "Skill{" +
				"competency='" + competency + '\'' +
				", experienceInMonths=" + experienceInMonths +
				", skillType='" + skillType + '\'' +
				'}';
	}
}
