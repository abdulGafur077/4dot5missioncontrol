package com.fourdotfive.missioncontrol.pojo.job;

public enum Sponsorship {

    CPT("CPT"), OPT("OPT"), H1("H1"), B1("B1"), L1("L1"), H1TRANSFER("H1 Transfer"),
    PERMANENTRESIDENTORGREENCARD("Permanent Resident/Green Card"),
    CITIZENONLY("Citizen"), OTHER("Other");

    private String name;

    private Sponsorship(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
