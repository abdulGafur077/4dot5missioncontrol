package com.fourdotfive.missioncontrol.pojo.job;

import java.util.List;

import com.fourdotfive.missioncontrol.dtos.candidate.CandidateUploadResumeMinInfoDto;
import com.fourdotfive.missioncontrol.pojo.candidate.Candidate;

public class UploadResumeCandidateDto {

    private int activeCandidatesCount;
    private int passiveCandidatesCount;
    private int matchedCandidateCount;
    private int unMatchedCandidateCount;
    private int justAddedCandidateCount;
    private int notProcessedCandidateCount;
    private int manuallyMatchedCandidateCount;
    private List<Candidate> candidateList;
    private List<CandidateUploadResumeMinInfoDto> matchedCandidateIdAndNames;
    private List<CandidateUploadResumeMinInfoDto> unMatchedCandidateIdAndNames;
    private int candidatesSuspendedPeriod;
    private boolean allowOverride;

    public int getActiveCandidatesCount() {
        return activeCandidatesCount;
    }

    public void setActiveCandidatesCount(int activeCandidatesCount) {
        this.activeCandidatesCount = activeCandidatesCount;
    }

    public int getPassiveCandidatesCount() {
        return passiveCandidatesCount;
    }

    public void setPassiveCandidatesCount(int passiveCandidatesCount) {
        this.passiveCandidatesCount = passiveCandidatesCount;
    }

    public int getMatchedCandidateCount() {
        return matchedCandidateCount;
    }

    public void setMatchedCandidateCount(int matchedCandidateCount) {
        this.matchedCandidateCount = matchedCandidateCount;
    }

    public int getUnMatchedCandidateCount() {
        return unMatchedCandidateCount;
    }

    public void setUnMatchedCandidateCount(int unMatchedCandidateCount) {
        this.unMatchedCandidateCount = unMatchedCandidateCount;
    }

    public int getJustAddedCandidateCount() {
        return justAddedCandidateCount;
    }

    public void setJustAddedCandidateCount(int justAddedCandidateCount) {
        this.justAddedCandidateCount = justAddedCandidateCount;
    }

    public int getNotProcessedCandidateCount() {
        return notProcessedCandidateCount;
    }

    public void setNotProcessedCandidateCount(int notProcessedCandidateCount) {
        this.notProcessedCandidateCount = notProcessedCandidateCount;
    }

    public List<Candidate> getCandidateList() {
        return candidateList;
    }

    public void setCandidateList(List<Candidate> candidateList) {
        this.candidateList = candidateList;
    }

    public List<CandidateUploadResumeMinInfoDto> getMatchedCandidateIdAndNames() {
        return matchedCandidateIdAndNames;
    }

    public void setMatchedCandidateIdAndNames(List<CandidateUploadResumeMinInfoDto> matchedCandidateIdAndNames) {
        this.matchedCandidateIdAndNames = matchedCandidateIdAndNames;
    }

    public List<CandidateUploadResumeMinInfoDto> getUnMatchedCandidateIdAndNames() {
        return unMatchedCandidateIdAndNames;
    }

    public void setUnMatchedCandidateIdAndNames(List<CandidateUploadResumeMinInfoDto> unMatchedCandidateIdAndNames) {
        this.unMatchedCandidateIdAndNames = unMatchedCandidateIdAndNames;
    }

    public int getManuallyMatchedCandidateCount() {
        return manuallyMatchedCandidateCount;
    }

    public void setManuallyMatchedCandidateCount(int manuallyMatchedCandidateCount) {
        this.manuallyMatchedCandidateCount = manuallyMatchedCandidateCount;
    }

    public int getCandidatesSuspendedPeriod() {
        return candidatesSuspendedPeriod;
    }

    public void setCandidatesSuspendedPeriod(int candidatesSuspendedPeriod) {
        this.candidatesSuspendedPeriod = candidatesSuspendedPeriod;
    }

    public boolean isAllowOverride() {
        return allowOverride;
    }

    public void setAllowOverride(boolean allowOverride) {
        this.allowOverride = allowOverride;
    }
}
