package com.fourdotfive.missioncontrol.pojo.notificationpreference;

public enum NotificationGroupEnum {
	
	OCCURANCE_AND_NON_OCCURANCE_ALERT, REQUISITION_ALERT, CANDIDATE_ALERT

}
