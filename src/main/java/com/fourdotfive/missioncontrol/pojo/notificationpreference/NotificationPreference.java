package com.fourdotfive.missioncontrol.pojo.notificationpreference;

import java.util.List;

import com.fourdotfive.missioncontrol.pojo.user.User;

public class NotificationPreference {

	private String id;
	private User user;
	private List<NotificationPreferenceGroup> notificationList;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<NotificationPreferenceGroup> getNotificationList() {
		return notificationList;
	}

	public void setNotificationList(
			List<NotificationPreferenceGroup> notificationList) {
		this.notificationList = notificationList;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("NotificationPreference [id=").append(id)
				.append(", user=").append(user).append(", notificationList=")
				.append(notificationList).append("]");
		return builder.toString();
	}

}
