package com.fourdotfive.missioncontrol.pojo.notificationpreference;

public class NotificationPreferenceDetails {

	private String name;

	private boolean enabledForApp;

	private boolean enabledForEmail;

	private boolean enabledForText;

	private boolean onForApp;

	private boolean onForEmail;

	private boolean onForText;

	private boolean enabled;

	private NotificationPreferenceType notificationPreferencEnum;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isEnabledForApp() {
		return enabledForApp;
	}

	public void setEnabledForApp(boolean enabledForApp) {
		this.enabledForApp = enabledForApp;
	}

	public boolean isEnabledForEmail() {
		return enabledForEmail;
	}

	public void setEnabledForEmail(boolean enabledForEmail) {
		this.enabledForEmail = enabledForEmail;
	}

	public boolean isEnabledForText() {
		return enabledForText;
	}

	public void setEnabledForText(boolean enabledForText) {
		this.enabledForText = enabledForText;
	}

	public boolean isOnForApp() {
		return onForApp;
	}

	public void setOnForApp(boolean onForApp) {
		this.onForApp = onForApp;
	}

	public boolean isOnForEmail() {
		return onForEmail;
	}

	public void setOnForEmail(boolean onForEmail) {
		this.onForEmail = onForEmail;
	}

	public boolean isOnForText() {
		return onForText;
	}

	public void setOnForText(boolean onForText) {
		this.onForText = onForText;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public NotificationPreferenceType getNotificationPreferencEnum() {
		return notificationPreferencEnum;
	}

	public void setNotificationPreferencEnum(
			NotificationPreferenceType notificationPreferencEnum) {
		this.notificationPreferencEnum = notificationPreferencEnum;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("NotificationPreferenceDetails [name=").append(name)
				.append(", enabledForApp=").append(enabledForApp)
				.append(", enabledForEmail=").append(enabledForEmail)
				.append(", enabledForText=").append(enabledForText)
				.append(", onForApp=").append(onForApp).append(", onForEmail=")
				.append(onForEmail).append(", onForText=").append(onForText)
				.append(", enabled=").append(enabled)
				.append(", notificationPreferencEnum=")
				.append(notificationPreferencEnum).append("]");
		return builder.toString();
	}
	
	

}
