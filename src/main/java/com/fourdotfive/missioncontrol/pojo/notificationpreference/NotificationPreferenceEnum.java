package com.fourdotfive.missioncontrol.pojo.notificationpreference;

public enum NotificationPreferenceEnum {

    TIME_TO_PLACE("Time to Place"),
    TIME_TO_CONVERT("Time to Convert"),
    TIME_TO_FILL("Time To Fill"),
    TIME_TO_SOURCE("Time to Source"),
    JOB_MATCH_FOUND("Job match found"),
    NO_JOB_MATCH_FOUND("No job match found"),
    TECH_ASSESSMENT("Tech Assessment"),
    TECH_ASSESSMENT_COMPLETED("Technical Assessment Completed"),
    TECH_ASSESSMENT_REMINDER("Technical Assessment Reminder"),
    TECH_ASSESSMENT_SENT("Technical Assessment Sent"),
    TECH_ASSESSMENT_CANCELED("Technical Assessment Canceled"),
    TECH_ASSESSMENT_EXPIRED("Technical Assessment Expired"),
    TECH_ASSESSMENT_SUSPENDED("Technical Assessment Suspended"),
    TECH_ASSESSMENT_LEFT("Technical Assessment Left"),
    TECH_ASSESSMENT_IN_PROGRESS("Technical Assessment In Progress"),
    TECH_ASSESSMENT_TERMINATED("Technical Assessment Terminated"),
    PHONE_SCREEN("Phone Screen"),
    PHONE_SCREEN_IN_PROGRESS("Phone Screen In Progress"),
    PHONE_SCREEN_SCHEDULED("Phone Screen Scheduled"),
    PHONE_SCREEN_RE_SCHEDULED("Phone Screen Rescheduled"),
    PHONE_SCREEN_CANCELED("Phone Screen Canceled"),
    PHONE_SCREEN_COMPLETED("Phone Screen Completed"),
    PHONE_SCREEN_OVERDUE("Phone Screen Overdue"),
    INTERVIEW("Interview"),
    INTERVIEW_IN_PROGRESS("Interview In Progress"),
    INTERVIEW_SCHEDULED("Interview Scheduled"),
    INTERVIEW_RE_SCHEDULED("Interview Rescheduled"),
    INTERVIEW_CANCELED("Interview Canceled"),
    INTERVIEW_COMPLETED("Interview Completed"),
    INTERVIEW_OVERDUE("Interview Overdue"),
    VALUE_ASSESSMENT("Value Assessment"),
    VALUE_ASSESSMENT_SENT("Value Assessment Sent"),
    VALUE_ASSESSMENT_REMINDER("Value Assessment Reminder"),
    VALUE_ASSESSMENT_IN_PROGRESS("Value Assessment In Progress"),
    VALUE_ASSESSMENT_CANCELED("Value Assessment Canceled"),
    VALUE_ASSESSMENT_EXPIRED("Value Assessment Expired"),
    VALUE_ASSESSMENT_LEFT("Value Assessment Left"),
    VALUE_ASSESSMENT_COMPLETED("Value Assessment Completed"),
    VALUE_ASSESSMENT_TERMINATED("Value Assessment Terminated"),
    VALUE_ASSESSMENT_SUSPENDED("Value Assessment Suspended"),
    VERIFICATION("Verification"),
    VERIFICATION_COMPLETED("Candidate Verification completed"),
    VERIFICATION_OVERDUE("Candidate Verification overdue"),
    CANDIDATE_NO_LONGER_MATCHES("Candidate(s) no longer match job requisition"),
    USER_ROLE_SCOPE("User role/scope changed"),
    USER_SCOPE_CHANGED("User scope changed"),
    USER_ROLE_CHANGED("User role changed"),
    JOB_BOARD_SEARCH_COMPLETED("Job Board Search Completed"),
    JOB_BOARD_SEARCH_FAILED("Job Board Search Failed"),
    PLAN_EXPIRATION_WARNING("Plan Expiration Warning"),
    COMPANY_STATE_CHANGED("Company State Changed"),
    CURRENT_PLAN_ACTIVATED("Current Plan Activated"),
    REQUISITION_ADDED("Requisition Created"),
    REQUISITION_UPDATED("Requisition Updated"),
    REQUISITION_STATUS_CHANGED("Requisition Status Changed"),
    SHARED_REQUISITION_CREATED("Shared Requisition Updated"),
    SHARED_REQUISITION_UPDATED("Shared Requisition Updated"),
    SHARED_REQUISITION_STATUS_CHANGED("Shared Requisition Status Changed"),
    SHARED_REQUISITION_CANDIDATE_FILLED("Candidate Filled"),
    SHARED_REQUISITION_CANDIDATE_JOINED("Candidate Joined"),
    SHARED_REQUISITION_FILLED_CANDIDATE_REMOVED("Filled Candidate Removed From Requisition"),
    SHARED_REQUISITION_JOINED_CANDIDATE_REMOVED("Joined Candidate Removed From Requisition"),
    SHARED_REQUISITION_VENDOR_ADDED("New Vendor Added To Shared Requisition"),
    SHARED_REQUISITION_VENDOR_REMOVED("Vendor Removed From Shared Requisition"),
    CORPORATE_VENDOR_ADDED("Corporate level Vendor added"),
    CORPORATE_VENDOR_REMOVED("Corporate level Vendor removed"),
    BU_VENDOR_ADDED("BU level Vendor added"),
    BU_VENDOR_REMOVED("BU level Vendor removed");

    private final String description;

    NotificationPreferenceEnum(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

}
