package com.fourdotfive.missioncontrol.pojo.notificationpreference;

import java.util.List;

public class NotificationPreferenceGroup {

	private String name;

	private NotificationGroupEnum notificationGroupEnum;

	private List<NotificationPreferenceDetails> notificationPreferenceList;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<NotificationPreferenceDetails> getNotificationPreferenceList() {
		return notificationPreferenceList;
	}

	public void setNotificationPreferenceList(
			List<NotificationPreferenceDetails> notificationPreferenceList) {
		this.notificationPreferenceList = notificationPreferenceList;
	}

	public NotificationGroupEnum getNotificationGroupEnum() {
		return notificationGroupEnum;
	}

	public void setNotificationGroupEnum(
			NotificationGroupEnum notificationGroupEnum) {
		this.notificationGroupEnum = notificationGroupEnum;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("NotificationPreferenceGroup [name=").append(name)
				.append(", notificationGroupEnum=")
				.append(notificationGroupEnum)
				.append(", notificationPreferenceList=")
				.append(notificationPreferenceList).append("]");
		return builder.toString();
	}

}
