package com.fourdotfive.missioncontrol.pojo.notificationpreference;

public enum NotificationPreferenceType {
	FOURDOTFIVE_INTELLIGENCE("4dot5 Intelligence Score"), TECH_ASSESSMENT("Tech Assessment"), VALUE_ASSESSMENT("Value Assessment"),
	VERIFICATION("Verification"), PHONE_SCREEN("Phone Screen"), INTERVIEW("Interview");

	private String text;

	NotificationPreferenceType(String text) {
		this.text = text;
	}

	public String getText() {
		return this.text;
	}

	public static NotificationPreferenceType fromText(String text) {
		for (NotificationPreferenceType notificationPreferenceType : NotificationPreferenceType.values()) {
			if (notificationPreferenceType.text.equalsIgnoreCase(text)) {
				return notificationPreferenceType;
			}
		}
		return null;
	}
}
