package com.fourdotfive.missioncontrol.pojo.plan;

public class Feature {
    private String name;
    private boolean isAvailable;
    private boolean isLimited;
    private int limitValue;
    private String additionalInfo;
    private String displayName;

    public Feature() {
    }

    public Feature(String name, boolean isAvailable, boolean isLimited, int limitValue, String additionalInfo) {
        this.name = name;
        this.isAvailable = isAvailable;
        this.isLimited = isLimited;
        this.limitValue = limitValue;
        this.additionalInfo = additionalInfo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public boolean isLimited() {
        return isLimited;
    }

    public void setLimited(boolean limited) {
        isLimited = limited;
    }

    public int getLimitValue() {
        return limitValue;
    }

    public void setLimitValue(int limitValue) {
        this.limitValue = limitValue;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
