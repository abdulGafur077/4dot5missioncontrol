package com.fourdotfive.missioncontrol.pojo.plan;

import java.util.List;

public class PlanFeaturesCapability {

    private String id;
    private String plan;
    private String planCost;
    private List<String> planTypeList;
    private List<Feature> features;
    private boolean isPlanAvailable;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getPlanCost() {
        return planCost;
    }

    public void setPlanCost(String planCost) {
        this.planCost = planCost;
    }

    public List<String> getPlanTypeList() {
        return planTypeList;
    }

    public void setPlanTypeList(List<String> planTypeList) {
        this.planTypeList = planTypeList;
    }

    public List<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(List<Feature> features) {
        this.features = features;
    }

    public boolean getIsPlanAvailable() {
        return isPlanAvailable;
    }

    public void setPlanAvailable(boolean planAvailable) {
        isPlanAvailable = planAvailable;
    }
}

