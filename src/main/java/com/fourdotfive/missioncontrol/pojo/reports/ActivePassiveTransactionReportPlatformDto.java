/**
 * 
 */
package com.fourdotfive.missioncontrol.pojo.reports;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.user.User;

 

/**
 * @author spaneos
 *
 */
 
public class ActivePassiveTransactionReportPlatformDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String jobRequistionId;
	
	 
	private List<User>recruiters=new ArrayList<>();
	
 
	private Company company;
	
	private Date weeklyReportDate;
	//Active report
	private Integer avgTimeToPlace;
	
	private Integer activeCandidateCount;
	
	private Integer placedCandidateCount;
	//Passive report
	private Integer avgTimeToConvert;
	
	private Integer passiveCandidateCount;
	
	private Integer passiveConversionCount;

	public String getJobRequistionId() {
		return jobRequistionId;
	}

	public void setJobRequistionId(String jobRequistionId) {
		this.jobRequistionId = jobRequistionId;
	}

	public List<User> getRecruiters() {
		return recruiters;
	}

	public void setRecruiters(List<User> recruiters) {
		this.recruiters = recruiters;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Date getWeeklyReportDate() {
		return weeklyReportDate;
	}

	public void setWeeklyReportDate(Date weeklyReportDate) {
		this.weeklyReportDate = weeklyReportDate;
	}

	public Integer getAvgTimeToPlace() {
		return avgTimeToPlace;
	}

	public void setAvgTimeToPlace(Integer avgTimeToPlace) {
		this.avgTimeToPlace = avgTimeToPlace;
	}

	public Integer getActiveCandidateCount() {
		return activeCandidateCount;
	}

	public void setActiveCandidateCount(Integer activeCandidateCount) {
		this.activeCandidateCount = activeCandidateCount;
	}

	public Integer getPassiveCandidateCount() {
		return passiveCandidateCount;
	}

	public void setPassiveCandidateCount(Integer passiveCandidateCount) {
		this.passiveCandidateCount = passiveCandidateCount;
	}

	public Integer getPlacedCandidateCount() {
		return placedCandidateCount;
	}

	public void setPlacedCandidateCount(Integer placedCandidateCount) {
		this.placedCandidateCount = placedCandidateCount;
	}

	public Integer getAvgTimeToConvert() {
		return avgTimeToConvert;
	}

	public void setAvgTimeToConvert(Integer avgTimeToConvert) {
		this.avgTimeToConvert = avgTimeToConvert;
	}

	public Integer getPassiveConversionCount() {
		return passiveConversionCount;
	}

	public void setPassiveConversionCount(Integer passiveConversionCount) {
		this.passiveConversionCount = passiveConversionCount;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ActivePassiveTransactionReportPlatformDto [jobRequistionId=");
		builder.append(jobRequistionId);
		builder.append(", recruiters=");
		builder.append(recruiters);
		builder.append(", company=");
		builder.append(company);
		builder.append(", weeklyReportDate=");
		builder.append(weeklyReportDate);
		builder.append(", avgTimeToPlace=");
		builder.append(avgTimeToPlace);
		builder.append(", activeCandidateCount=");
		builder.append(activeCandidateCount);
		builder.append(", passiveCandidateCount=");
		builder.append(passiveCandidateCount);
		builder.append(", placedCandidateCount=");
		builder.append(placedCandidateCount);
		builder.append(", avgTimeToConvert=");
		builder.append(avgTimeToConvert);
		builder.append(", passiveConversionCount=");
		builder.append(passiveConversionCount);
		builder.append("]");
		return builder.toString();
	}
	
	public boolean containsRecruiter(String id){
		User recruiter=new User();
		recruiter.setId(id);
		return recruiters.contains(recruiter);
		
	}
	
	

}
