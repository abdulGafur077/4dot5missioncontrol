package com.fourdotfive.missioncontrol.pojo.reports;

import java.util.List;
/**
 * <code>BarChart</code> is parent for BarChart.
 * Can be extended by child class to add new set of data.
 * 
 * @author Utpal Kant Sharma
 * @see TimeIntervalBarChart
 * @see CandidateStatusBarChart
 */
public class BarChart {
	protected List<Integer> firstLine;
	protected List<Integer> secondLine;
	protected List<Integer> timeToFill;
	
	public List<Integer> getFirstLine() {
		return firstLine;
	}
	public void setFirstLine(List<Integer> firstLine) {
		this.firstLine = firstLine;
	}
	public List<Integer> getSecondLine() {
		return secondLine;
	}
	public void setSecondLine(List<Integer> secondLine) {
		this.secondLine = secondLine;
	}
	public List<Integer> getTimeToFill() {
		return timeToFill;
	}
	public void setTimeToFill(List<Integer> timeToFill) {
		this.timeToFill = timeToFill;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BarChart [firstLine=");
		builder.append(firstLine);
		builder.append(", secondLine=");
		builder.append(secondLine);
		builder.append(", timeToFill=");
		builder.append(timeToFill);
		builder.append("]");
		return builder.toString();
	}
	
	
}
