package com.fourdotfive.missioncontrol.pojo.reports;

public class CandidateStatusBarChart extends BarChart {
	private EnumCandidateStatus candidateStatus;

	public EnumCandidateStatus getCandidateStatus() {
		return candidateStatus;
	}

	public void setCandidateStatus(EnumCandidateStatus candidateStatus) {
		this.candidateStatus = candidateStatus;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CandidateStatusBarChart [candidateStatus=");
		builder.append(candidateStatus);
		builder.append("]");
		return builder.toString();
	}
		
}
