/**
 * 
 */

package com.fourdotfive.missioncontrol.pojo.reports;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.user.User;

/**
 * @author spaneos
 *
 */
public class DailyTransactionReport implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String dailyTransactionReportId;

	private Date dailyTransactionReportDate;

	private String jobRequistionId;

	
	private List<User> recruiters = new ArrayList<>();

	
	private Company company;

	private Integer timeToPlaceAvg;

	private Integer activeCandidateCount;

	private Integer passiveCandidateCount;

	//private JobStateType jobStateType;

	private Integer TimeToConversionCount;

	public String getDailyTransactionReportId() {
		return dailyTransactionReportId;
	}

	public void setDailyTransactionReportId(String dailyTransactionReportId) {
		this.dailyTransactionReportId = dailyTransactionReportId;
	}

	public Date getDailyTransactionReportDate() {
		return dailyTransactionReportDate;
	}

	public void setDailyTransactionReportDate(Date dailyTransactionReportDate) {
		this.dailyTransactionReportDate = dailyTransactionReportDate;
	}

	public String getJobRequistionId() {
		return jobRequistionId;
	}

	public void setJobRequistionId(String jobRequistionId) {
		this.jobRequistionId = jobRequistionId;
	}

	public List<User> getRecruiters() {
		return recruiters;
	}

	public void setRecruiters(List<User> recruiters) {
		this.recruiters = recruiters;
	}

	


	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Integer getTimeToPlaceAvg() {
		return timeToPlaceAvg;
	}

	public void setTimeToPlaceAvg(Integer timeToPlaceAvg) {
		this.timeToPlaceAvg = timeToPlaceAvg;
	}

	public Integer getActiveCandidateCount() {
		return activeCandidateCount;
	}

	public void setActiveCandidateCount(Integer activeCandidateCount) {
		this.activeCandidateCount = activeCandidateCount;
	}

	public Integer getPassiveCandidateCount() {
		return passiveCandidateCount;
	}

	public void setPassiveCandidateCount(Integer passiveCandidateCount) {
		this.passiveCandidateCount = passiveCandidateCount;
	}

	 

	public Integer getTimeToConversionCount() {
		return TimeToConversionCount;
	}

	public void setTimeToConversionCount(Integer timeToConversionCount) {
		TimeToConversionCount = timeToConversionCount;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DailyTransactionReport [dailyTransactionReportId=");
		builder.append(dailyTransactionReportId);
		builder.append(", dailyTransactionReportDate=");
		builder.append(dailyTransactionReportDate);
		builder.append(", jobRequistionId=");
		builder.append(jobRequistionId);
		builder.append(", recruiters=");
		builder.append(recruiters);
		builder.append(", company=");
		builder.append(company);
		builder.append(", timeToPlaceAvg=");
		builder.append(timeToPlaceAvg);
		builder.append(", activeCandidateCount=");
		builder.append(activeCandidateCount);
		builder.append(", passiveCandidateCount=");
		builder.append(passiveCandidateCount);
		builder.append(", TimeToConversionCount=");
		builder.append(TimeToConversionCount);
		builder.append("]");
		return builder.toString();
	}
	
	

}
