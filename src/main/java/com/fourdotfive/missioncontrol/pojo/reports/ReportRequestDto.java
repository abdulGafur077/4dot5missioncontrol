package com.fourdotfive.missioncontrol.pojo.reports;

import java.util.Date;
import java.util.List;

public class ReportRequestDto {
	
	private List<String> clientOrBuIds;
	private List<String> recruiterIds;
	private List<String> requisitions;
	private EnumReportType enumReportType;
	//It could be set of five week,month or quarter
	//Example-- suppose if today is 26-10-2016, so previousFromDate could be 01-05-2016 and previousToDate could be 01-09-2016
	//currentFromDate could be 01-10-2016 and currentToDate could be 26-10-2016
	private Date previousFromDate;
	private Date previousToDate;
	private Date currentFromDate;
	private Date currentToDate;
	
	public List<String> getClientOrBuIds() {
		return clientOrBuIds;
	}
	public void setClientOrBuIds(List<String> clientOrBuIds) {
		this.clientOrBuIds = clientOrBuIds;
	}
	public List<String> getRecruiterIds() {
		return recruiterIds;
	}
	public void setRecruiterIds(List<String> recruiterIds) {
		this.recruiterIds = recruiterIds;
	}
	public List<String> getRequisitions() {
		return requisitions;
	}
	public void setRequisitions(List<String> requisitions) {
		this.requisitions = requisitions;
	}
	public EnumReportType getEnumReportType() {
		return enumReportType;
	}
	public void setEnumReportType(EnumReportType enumReportType) {
		this.enumReportType = enumReportType;
	}
	public Date getPreviousFromDate() {
		return previousFromDate;
	}
	public void setPreviousFromDate(Date previousFromDate) {
		this.previousFromDate = previousFromDate;
	}
	public Date getPreviousToDate() {
		return previousToDate;
	}
	public void setPreviousToDate(Date previousToDate) {
		this.previousToDate = previousToDate;
	}
	public Date getCurrentFromDate() {
		return currentFromDate;
	}
	public void setCurrentFromDate(Date currentFromDate) {
		this.currentFromDate = currentFromDate;
	}
	public Date getCurrentToDate() {
		return currentToDate;
	}
	public void setCurrentToDate(Date currentToDate) {
		this.currentToDate = currentToDate;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ReportRequestDto [clientOrBuIds=");
		builder.append(clientOrBuIds);
		builder.append(", recruiterIds=");
		builder.append(recruiterIds);
		builder.append(", requisitions=");
		builder.append(requisitions);
		builder.append(", enumReportType=");
		builder.append(enumReportType);
		builder.append(", previousFromDate=");
		builder.append(previousFromDate);
		builder.append(", previousToDate=");
		builder.append(previousToDate);
		builder.append(", currentFromDate=");
		builder.append(currentFromDate);
		builder.append(", currentToDate=");
		builder.append(currentToDate);
		builder.append("]");
		return builder.toString();
	}
	
	
}
