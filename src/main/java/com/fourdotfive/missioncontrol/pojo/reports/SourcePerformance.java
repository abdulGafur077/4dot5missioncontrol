/**
 * 
 */
package com.fourdotfive.missioncontrol.pojo.reports;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fourdotfive.missioncontrol.pojo.job.Resume;



/**
 * @author spaneos
 *
 */
 
public class SourcePerformance implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String sourceName;
	
 
	private List<Resume> resume =new ArrayList<>();
	
	 
	private List<SourcePerformanceBarChart> sourcePerformanceBarChart =new ArrayList<>();

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public List<SourcePerformanceBarChart> getSourcePerformanceBarChart() {
		return sourcePerformanceBarChart;
	}

	public void setSourcePerformanceBarChart(List<SourcePerformanceBarChart> sourcePerformanceBarChart) {
		this.sourcePerformanceBarChart = sourcePerformanceBarChart;
	}

	public List<Resume> getResume() {
		return resume;
	}

	public void setResume(List<Resume> resume) {
		this.resume = resume;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SourcePerformance [sourceName=");
		builder.append(sourceName);
		builder.append(", resume=");
		builder.append(resume);
		builder.append(", sourcePerformanceBarChart=");
		builder.append(sourcePerformanceBarChart);
		builder.append("]");
		return builder.toString();
	}

	
	
	
	
}
