/**
 * 
 */
package com.fourdotfive.missioncontrol.pojo.reports;

import java.io.Serializable;



/**
 * @author spaneos
 *
 */
 
public class SourcePerformanceBarChart implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private Integer sourceCount;
	
	private Integer qualifiedCount;
	
	private Integer techAssessmentCount;
	
	private Integer goldStandard;
	
	private Integer hiredCount;

	public Integer getSourceCount() {
		return sourceCount;
	}

	public void setSourceCount(Integer sourceCount) {
		this.sourceCount = sourceCount;
	}

	public Integer getQualifiedCount() {
		return qualifiedCount;
	}

	public void setQualifiedCount(Integer qualifiedCount) {
		this.qualifiedCount = qualifiedCount;
	}

	public Integer getTechAssessmentCount() {
		return techAssessmentCount;
	}

	public void setTechAssessmentCount(Integer techAssessmentCount) {
		this.techAssessmentCount = techAssessmentCount;
	}

	public Integer getGoldStandard() {
		return goldStandard;
	}

	public void setGoldStandard(Integer goldStandard) {
		this.goldStandard = goldStandard;
	}

	public Integer getHiredCount() {
		return hiredCount;
	}

	public void setHiredCount(Integer hiredCount) {
		this.hiredCount = hiredCount;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SourcePerformanceBarChart [sourceCount=");
		builder.append(sourceCount);
		builder.append(", qualifiedCount=");
		builder.append(qualifiedCount);
		builder.append(", techAssessmentCount=");
		builder.append(techAssessmentCount);
		builder.append(", goldStandard=");
		builder.append(goldStandard);
		builder.append(", hiredCount=");
		builder.append(hiredCount);
		builder.append("]");
		return builder.toString();
	}
	

}
