package com.fourdotfive.missioncontrol.pojo.reports;

/**
 * Populates performance data for 6 months.
 * 
 * @author Utpal Kant
 *
 */
public class SourcePerformanceData {
	private int sourcedCount;
	private float goldCandidatesPercentage;
	private float hiredPercentage;
	private int hiredCount;
	public int getSourcedCount() {
		return sourcedCount;
	}
	public void setSourcedCount(int sourcedCount) {
		this.sourcedCount = sourcedCount;
	}
	public float getGoldCandidatesPercentage() {
		return goldCandidatesPercentage;
	}
	public void setGoldCandidatesPercentage(float goldCandidatesPercentage) {
		this.goldCandidatesPercentage = goldCandidatesPercentage;
	}
	public float getHiredPercentage() {
		return hiredPercentage;
	}
	public void setHiredPercentage(float hiredPercentage) {
		this.hiredPercentage = hiredPercentage;
	}
	public int getHiredCount() {
		return hiredCount;
	}
	public void setHiredCount(int hiredCount) {
		this.hiredCount = hiredCount;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SourcePerformanceData [sourcedCount=");
		builder.append(sourcedCount);
		builder.append(", goldCandidatesPercentage=");
		builder.append(goldCandidatesPercentage);
		builder.append(", hiredPercentage=");
		builder.append(hiredPercentage);
		builder.append(", hiredCount=");
		builder.append(hiredCount);
		builder.append("]");
		return builder.toString();
	}
	
}
