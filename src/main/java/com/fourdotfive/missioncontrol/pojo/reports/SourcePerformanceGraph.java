package com.fourdotfive.missioncontrol.pojo.reports;

import java.util.List;
/**
 * Populates performance data for 6 weeks.
 * @author Utpal Kant
 *
 */
public class SourcePerformanceGraph {

	private List<Integer> sourced;
	private List<Integer> qualified;
	private List<Integer> techAssessment;
	private List<Integer> goldStandard;
	private List<Integer> hired;
	
	
	public List<Integer> getSourced() {
		return sourced;
	}
	public void setSourced(List<Integer> sourced) {
		this.sourced = sourced;
	}
	public List<Integer> getQualified() {
		return qualified;
	}
	public void setQualified(List<Integer> qualified) {
		this.qualified = qualified;
	}
	public List<Integer> getTechAssessment() {
		return techAssessment;
	}
	public void setTechAssessment(List<Integer> techAssessment) {
		this.techAssessment = techAssessment;
	}
	public List<Integer> getGoldStandard() {
		return goldStandard;
	}
	public void setGoldStandard(List<Integer> goldStandard) {
		this.goldStandard = goldStandard;
	}
	public List<Integer> getHired() {
		return hired;
	}
	public void setHired(List<Integer> hired) {
		this.hired = hired;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SourcePerformanceGraph [sourced=");
		builder.append(sourced);
		builder.append(", qualified=");
		builder.append(qualified);
		builder.append(", techAssessment=");
		builder.append(techAssessment);
		builder.append(", goldStandard=");
		builder.append(goldStandard);
		builder.append(", hired=");
		builder.append(hired);
		builder.append("]");
		return builder.toString();
	}
	
	
	
}
