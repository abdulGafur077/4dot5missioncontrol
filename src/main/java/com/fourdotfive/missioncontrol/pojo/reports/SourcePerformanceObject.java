package com.fourdotfive.missioncontrol.pojo.reports;
/**
 * Contain SourcePerformanceGraph and SourcePerformanceData. <br>
 * SourcePerformanceGraph display 6 weeks data
 * where as SourcePerformanceData display 6 months.
 * @author Utpal Kant
 *
 */
public class SourcePerformanceObject {
	private SourcePerformanceGraph sourcePerformanceGraph;
	private SourcePerformanceData sourcePerformanceData;
	private String companyName;
	
	public SourcePerformanceGraph getSourcePerformanceGraph() {
		return sourcePerformanceGraph;
	}
	public void setSourcePerformanceGraph(
			SourcePerformanceGraph sourcePerformanceGraph) {
		this.sourcePerformanceGraph = sourcePerformanceGraph;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	public SourcePerformanceData getSourcePerformanceData() {
		return sourcePerformanceData;
	}
	public void setSourcePerformanceData(SourcePerformanceData sourcePerformanceData) {
		this.sourcePerformanceData = sourcePerformanceData;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SourcePerformanceObject [sourcePerformanceGraph=");
		builder.append(sourcePerformanceGraph);
		builder.append(", companyName=");
		builder.append(companyName);
		builder.append("]");
		return builder.toString();
	}
	

}
