package com.fourdotfive.missioncontrol.pojo.reports;

import java.util.List;

public class TeamPerformanceByMeasure {
	
    private List<Integer> points;
	private EnumReportType enumReportType;
	private String memberName;
	private String memberId;
	public List<Integer> getPoints() {
		return points;
	}
	public void setPoints(List<Integer> points) {
		this.points = points;
	}
	public EnumReportType getEnumReportType() {
		return enumReportType;
	}
	public void setEnumReportType(EnumReportType enumReportType) {
		this.enumReportType = enumReportType;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TeamPerformanceByMesure [points=");
		builder.append(points);
		builder.append(", enumReportType=");
		builder.append(enumReportType);
		builder.append(", memberName=");
		builder.append(memberName);
		builder.append(", memberId=");
		builder.append(memberId);
		builder.append("]");
		return builder.toString();
	}
	

}
