package com.fourdotfive.missioncontrol.pojo.reports;

import java.util.List;

public class TeamPerformanceChart {
	
	private List<Integer> points;

	public List<Integer> getPoints() {
		return points;
	}

	public void setPoints(List<Integer> points) {
		this.points = points;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TeamPerformanceChart [points=");
		builder.append(points);
		builder.append("]");
		return builder.toString();
	}
	
	

}
