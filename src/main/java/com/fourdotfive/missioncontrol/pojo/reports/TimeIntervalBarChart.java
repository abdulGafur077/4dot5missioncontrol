package com.fourdotfive.missioncontrol.pojo.reports;

public class TimeIntervalBarChart extends BarChart {
	// Type of report
	private EnumReportType reportType;

	public EnumReportType getReportType() {
		return reportType;
	}

	public void setReportType(EnumReportType reportType) {
		this.reportType = reportType;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TimeIntervalBarChart [reportType=");
		builder.append(reportType);
		builder.append("]");
		return builder.toString();
	}

}
