/**
 * 
 */
package com.fourdotfive.missioncontrol.pojo.reports;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.user.User;

 
/**
 * @author spaneos
 *
 */
 
public class WeeklyRequistionTransactionPerformanceReport implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private String jobRequistionId;
	
	 
	private List<User>recruiters=new ArrayList<>();
	
	 
	private Company company;
	
	private Integer avgTimeToFill;
	
	private Integer filledCount;
	
	private Integer newRequistionCount;
	
	private Date weeklyReqDate;

	public String getJobRequistionId() {
		return jobRequistionId;
	}

	public void setJobRequistionId(String jobRequistionId) {
		this.jobRequistionId = jobRequistionId;
	}

	public List<User> getRecruiters() {
		return recruiters;
	}

	public void setRecruiters(List<User> recruiters) {
		this.recruiters = recruiters;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Integer getAvgTimeToFill() {
		return avgTimeToFill;
	}

	public void setAvgTimeToFill(Integer avgTimeToFill) {
		this.avgTimeToFill = avgTimeToFill;
	}

	public Integer getFilledCount() {
		return filledCount;
	}

	public void setFilledCount(Integer filledCount) {
		this.filledCount = filledCount;
	}

	public Integer getNewRequistionCount() {
		return newRequistionCount;
	}

	public void setNewRequistionCount(Integer newRequistionCount) {
		this.newRequistionCount = newRequistionCount;
	}

	public Date getWeeklyReqDate() {
		return weeklyReqDate;
	}

	public void setWeeklyReqDate(Date weeklyReqDate) {
		this.weeklyReqDate = weeklyReqDate;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("WeeklyRequistionTransactionPerformanceReport [jobRequistionId=");
		builder.append(jobRequistionId);
		builder.append(", recruiters=");
		builder.append(recruiters);
		builder.append(", company=");
		builder.append(company);
		builder.append(", avgTimeToFill=");
		builder.append(avgTimeToFill);
		builder.append(", filledCount=");
		builder.append(filledCount);
		builder.append(", newRequistionCount=");
		builder.append(newRequistionCount);
		builder.append(", weeklyReqDate=");
		builder.append(weeklyReqDate);
		builder.append("]");
		return builder.toString();
	}
	
	
}
