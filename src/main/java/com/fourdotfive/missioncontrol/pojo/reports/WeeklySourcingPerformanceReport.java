/**
 * 
 */
package com.fourdotfive.missioncontrol.pojo.reports;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.user.User;

 

/**
 * @author spaneos
 *
 */
 
public class WeeklySourcingPerformanceReport implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private String requistionId;
	
	 
	private List<User>recruiters=new ArrayList<>();
	
	 
	private Company company;
	
	 
	List<SourcePerformance>sourcePerformances=new ArrayList<>();

	public String getRequistionId() {
		return requistionId;
	}

	public void setRequistionId(String requistionId) {
		this.requistionId = requistionId;
	}

	public List<User> getRecruiters() {
		return recruiters;
	}

	public void setRecruiters(List<User> recruiters) {
		this.recruiters = recruiters;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public List<SourcePerformance> getSourcePerformances() {
		return sourcePerformances;
	}

	public void setSourcePerformances(List<SourcePerformance> sourcePerformances) {
		this.sourcePerformances = sourcePerformances;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("WeeklySourcingPerformanceReport [requistionId=");
		builder.append(requistionId);
		builder.append(", recruiters=");
		builder.append(recruiters);
		builder.append(", company=");
		builder.append(company);
		builder.append(", sourcePerformances=");
		builder.append(sourcePerformances);
		builder.append("]");
		return builder.toString();
	}
	
	
}
