package com.fourdotfive.missioncontrol.pojo.setting;

import java.util.LinkedHashMap;

import com.fourdotfive.missioncontrol.licensepreference.LicensePrefEnum;

public class LicensePreferenceSetting {

	private String id;

	private LicensePrefEnum licensePreferenceEnum;

	private Double score;

	private Double timeToComplete;

	private Integer reminderInterval;

	private Integer year;

	private Integer month;

	private Integer day;

	private Integer maxLimitForTimeToComplete;

	private LinkedHashMap<NotifyEventEnum, NotifyEvent> notifyEvents;

	public LicensePreferenceSetting() {
	}

	public LicensePreferenceSetting(LicensePrefEnum licensePreferenceEnum, Double score, Double timeToComplete,
			Integer reminderInterval, Integer year, Integer month, Integer day, Integer maxLimitForTimeToComplete,
			LinkedHashMap<NotifyEventEnum, NotifyEvent> notificationList) {
		super();
		this.licensePreferenceEnum = licensePreferenceEnum;
		this.score = score;
		this.timeToComplete = timeToComplete;
		this.reminderInterval = reminderInterval;
		this.year = year;
		this.month = month;
		this.day = day;
		this.maxLimitForTimeToComplete = maxLimitForTimeToComplete;
		this.notifyEvents = notificationList;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public LicensePrefEnum getLicensePreferenceEnum() {
		return licensePreferenceEnum;
	}

	public void setLicensePreferenceEnum(LicensePrefEnum licensePreferenceEnum) {
		this.licensePreferenceEnum = licensePreferenceEnum;
	}

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	public Double getTimeToComplete() {
		return timeToComplete;
	}

	public void setTimeToComplete(Double timeToComplete) {
		this.timeToComplete = timeToComplete;
	}

	public Integer getReminderInterval() {
		return reminderInterval;
	}

	public void setReminderInterval(Integer reminderInterval) {
		this.reminderInterval = reminderInterval;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Integer getDay() {
		return day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public Integer getMaxLimitForTimeToComplete() {
		return maxLimitForTimeToComplete;
	}

	public void setMaxLimitForTimeToComplete(Integer maxLimitForTimeToComplete) {
		this.maxLimitForTimeToComplete = maxLimitForTimeToComplete;
	}

	public LinkedHashMap<NotifyEventEnum, NotifyEvent> getNotifyEvents() {
		return notifyEvents;
	}

	public void setNotifyEvents(LinkedHashMap<NotifyEventEnum, NotifyEvent> notifyEvents) {
		this.notifyEvents = notifyEvents;
	}

	@Override
	public String toString() {
		return "LicensePreferenceSetting [id=" + id + ", licensePreferenceEnum=" + licensePreferenceEnum + ", score="
				+ score + ", timeToComplete=" + timeToComplete + ", reminderInterval=" + reminderInterval + ", year="
				+ year + ", month=" + month + ", day=" + day + ", maxLimitForTimeToComplete="
				+ maxLimitForTimeToComplete + ", notifyEvents=" + notifyEvents + "]";
	}

}
