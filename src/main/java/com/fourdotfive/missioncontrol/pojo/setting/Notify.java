package com.fourdotfive.missioncontrol.pojo.setting;

public class Notify {

	private NotifyEnum notifyEnum;
	
	private String name;

	private boolean isEnabled;
	
	private boolean isReadOnly;//Not part of platform pojo

	public Notify() {
	}
	
	public NotifyEnum getNotifyEnum() {
		return notifyEnum;
	}

	public void setNotifyEnum(NotifyEnum notifyEnum) {
		this.notifyEnum = notifyEnum;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isEnabled() {
		return isEnabled;
	}

	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}
	
	public boolean isReadOnly() {
		return isReadOnly;
	}

	public void setReadOnly(boolean isReadOnly) {
		this.isReadOnly = isReadOnly;
	}

	public Notify(NotifyEnum notifyEnum, boolean isEnabled) {
		super();
		this.notifyEnum = notifyEnum;
		this.isEnabled = isEnabled;
	}

	@Override
	public String toString() {
		return "Notify [notifyEnum=" + notifyEnum + ", name=" + name + ", isEnabled=" + isEnabled + ", isReadOnly="
				+ isReadOnly + "]";
	}

}
