package com.fourdotfive.missioncontrol.pojo.setting;

public enum NotifyEnum {
	CANDIDATE_CARD, CANDIDATE_JOB_CARD, REQUISITION_CARD, USER_CARD
}
