package com.fourdotfive.missioncontrol.pojo.setting;

import java.util.LinkedHashMap;

public class NotifyEvent {
	
	private NotifyEventEnum notifyEventEnum;
	
	private String name;
	
	private LinkedHashMap<NotifyEnum,Notify> notifyOn;

	private boolean isDetectOn;

	private boolean isDetectOnReadOnly;//Not part of platform pojo

	public NotifyEvent() {
		// TODO Auto-generated constructor stub
	}
	
	public NotifyEvent(NotifyEventEnum notifyEventEnum, String name, LinkedHashMap<NotifyEnum, Notify> notifyOn, boolean isDetectOn) {
		super();
		this.notifyEventEnum = notifyEventEnum;
		this.name = name;
		this.notifyOn = notifyOn;
		this.isDetectOn = isDetectOn;
	}

	public NotifyEventEnum getNotifyEventEnum() {
		return notifyEventEnum;
	}

	public void setNotifyEventEnum(NotifyEventEnum notifyEventEnum) {
		this.notifyEventEnum = notifyEventEnum;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LinkedHashMap<NotifyEnum, Notify> getNotifyOn() {
		return notifyOn;
	}

	public void setNotifyOn(LinkedHashMap<NotifyEnum, Notify> notifyOn) {
		this.notifyOn = notifyOn;
	}

	@Override
	public String toString() {
		return "NotifyEvent [notifyEventEnum=" + notifyEventEnum + ", name=" + name + ", notifyOn=" + notifyOn + "]";
	}

	public boolean isDetectOn() {
		return isDetectOn;
	}

	public void setDetectOn(boolean detectOn) {
		isDetectOn = detectOn;
	}

	public boolean isDetectOnReadOnly() {
		return isDetectOnReadOnly;
	}

	public void setDetectOnReadOnly(boolean detectOnReadOnly) {
		isDetectOnReadOnly = detectOnReadOnly;
	}

}
