package com.fourdotfive.missioncontrol.pojo.setting;

public enum PerformanceEnum {
	TIME_TO_PLACE, TIME_TO_CONVERT, TIME_TO_SOURCE, TIME_TO_FILL
}
