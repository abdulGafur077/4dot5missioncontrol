package com.fourdotfive.missioncontrol.pojo.setting;

import java.util.HashMap;
import java.util.Map;

public class PerformanceSetting {

	private String id;

	private PerformanceEnum performanceEnum;
	
	private String name;

	private double minValue;

	private double maxValue;

	private double maxLimit;

	private Map<NotifyEventEnum,NotifyEvent> notifyEvents;

	public PerformanceSetting() {
	}
	
	public PerformanceSetting(PerformanceEnum performanceEnum, int minValue, int maxValue, int maxLimit,
			HashMap<NotifyEventEnum,NotifyEvent> notificationsFor) {
		super();
		this.performanceEnum = performanceEnum;
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.maxLimit = maxLimit;
		this.notifyEvents = notificationsFor;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public PerformanceEnum getPerformanceEnum() {
		return performanceEnum;
	}

	public void setPerformanceEnum(PerformanceEnum performanceEnum) {
		this.performanceEnum = performanceEnum;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getMinValue() {
		return minValue;
	}

	public void setMinValue(double minValue) {
		this.minValue = minValue;
	}

	public double getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(double maxValue) {
		this.maxValue = maxValue;
	}

	public double getMaxLimit() {
		return maxLimit;
	}

	public void setMaxLimit(double maxLimit) {
		this.maxLimit = maxLimit;
	}

	public Map<NotifyEventEnum, NotifyEvent> getNotifyEvents() {
		return notifyEvents;
	}

	public void setNotifyEvents(Map<NotifyEventEnum, NotifyEvent> notifyEvents) {
		this.notifyEvents = notifyEvents;
	}

	@Override
	public String toString() {
		return "PerformanceSetting [id=" + id + ", performanceEnum=" + performanceEnum + ", name=" + name
				+ ", minValue=" + minValue + ", maxValue=" + maxValue + ", maxLimit=" + maxLimit + ", notifyEvent="
				+ notifyEvents + "]";
	}

}
