package com.fourdotfive.missioncontrol.pojo.setting;

import java.util.LinkedHashMap;

import com.fourdotfive.missioncontrol.licensepreference.LicensePrefEnum;
import com.fourdotfive.missioncontrol.pojo.company.Company;

public class Setting {

	private String id;

	private Company company;
	
	private boolean global;
	
	private String createdBy;
	
	private String lastModifiedBy;
	
	private LinkedHashMap<LicensePrefEnum,LicensePreferenceSetting> licensePreferenceSetting;

	private LinkedHashMap<PerformanceEnum,PerformanceSetting> performanceSetting;

	private LinkedHashMap<UserRoleEnum,UserRoleSetting> userRoleSetting;


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public boolean isGlobal() {
		return global;
	}

	public void setGlobal(boolean global) {
		this.global = global;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public LinkedHashMap<LicensePrefEnum, LicensePreferenceSetting> getLicensePreferenceSetting() {
		return licensePreferenceSetting;
	}

	public LinkedHashMap<PerformanceEnum, PerformanceSetting> getPerformanceSetting() {
		return performanceSetting;
	}

	public void setPerformanceSetting(LinkedHashMap<PerformanceEnum, PerformanceSetting> performanceSetting) {
		this.performanceSetting = performanceSetting;
	}

	public void setLicensePreferenceSetting(
			LinkedHashMap<LicensePrefEnum, LicensePreferenceSetting> licensePreferenceSetting) {
		this.licensePreferenceSetting = licensePreferenceSetting;
	}

	public LinkedHashMap<UserRoleEnum, UserRoleSetting> getUserRoleSetting() {
		return userRoleSetting;
	}

	public void setUserRoleSetting(LinkedHashMap<UserRoleEnum, UserRoleSetting> userRoleSetting) {
		this.userRoleSetting = userRoleSetting;
	}



	@Override
	public String toString() {
		return "Setting [id=" + id + ", company=" + company + ", global=" + global + ", createdBy=" + createdBy
				+ ", lastModifiedBy=" + lastModifiedBy + ", licensePreferenceSetting=" + licensePreferenceSetting
				+ ", performanceSetting=" + performanceSetting + ", userRoleSetting=" + userRoleSetting +"]";
	}

}
