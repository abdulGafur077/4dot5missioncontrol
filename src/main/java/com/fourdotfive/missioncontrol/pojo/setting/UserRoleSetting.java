package com.fourdotfive.missioncontrol.pojo.setting;

import java.util.HashMap;
import java.util.Map;

public class UserRoleSetting {

    private String id;

    private String name;

    private UserRoleEnum  userRoleEnum;

    private Map<NotifyEventEnum,NotifyEvent> notifyEvents;

    public UserRoleSetting() {

    }

    public UserRoleSetting(UserRoleEnum userRoleEnum,
                           Map<NotifyEventEnum,NotifyEvent> notifyEvents) {
        super();
        this.userRoleEnum = userRoleEnum;
        this.notifyEvents = notifyEvents;
    }

    public UserRoleEnum getUserRoleEnum() {
        return userRoleEnum;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public void setUserRoleEnum(UserRoleEnum userRoleEnum) {
        this.userRoleEnum = userRoleEnum;
    }


    public Map<NotifyEventEnum, NotifyEvent> getNotifyEvents() {
        return notifyEvents;
    }

    public void setNotifyEvents(Map<NotifyEventEnum, NotifyEvent> notifyEvents) {
        this.notifyEvents = notifyEvents;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "UserRoleSetting [id=" + id + "name = " +  name + "userRoleEnum=" + userRoleEnum  + ", notifyEvents=" + notifyEvents + "]";
    }
}
