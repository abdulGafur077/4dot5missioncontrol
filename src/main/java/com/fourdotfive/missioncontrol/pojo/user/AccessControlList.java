package com.fourdotfive.missioncontrol.pojo.user;

import java.util.List;
import java.util.Map;

import com.fourdotfive.missioncontrol.dtos.company.AllBusOfClientList;
import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.company.Industry;
import com.fourdotfive.missioncontrol.pojo.company.JobRequisitionRole;

public class AccessControlList {

	private String accountState;

	private boolean allIndustries;

	private boolean allJobRequisitionTitleList;

	private boolean allclientOrBU;
	
	private boolean requisitionCategory;

	private boolean authenticationError;

	private List<Company> clientOrBUList;

	private List<Company> companyList;

	private String createdBy;

	private String id;

	private List<Industry> industryList;

	private List<JobRequisitionRole> jobRequisitionTitleList;

	private String lastModifiedBy;

	private String isNew;

	private List<String> requisitionCategoryList;

	private RoleScreenXRef roleScreenXRef;

	private int version;

	private boolean allBusOfClient;

	private Map<String, AllBusOfClientList> allBusOfClientList;

	public String getAccountState() {
		return accountState;
	}

	public void setAccountState(String accountState) {
		this.accountState = accountState;
	}

	public boolean isAllIndustries() {
		return allIndustries;
	}

	public void setAllIndustries(boolean allIndustries) {
		this.allIndustries = allIndustries;
	}

	public boolean isAllJobRequisitionTitleList() {
		return allJobRequisitionTitleList;
	}

	public void setAllJobRequisitionTitleList(boolean allJobRequisitionTitleList) {
		this.allJobRequisitionTitleList = allJobRequisitionTitleList;
	}

	public boolean isAllclientOrBU() {
		return allclientOrBU;
	}

	public void setAllclientOrBU(boolean allclientOrBU) {
		this.allclientOrBU = allclientOrBU;
	}
	
	public boolean isRequisitionCategory() {
		return requisitionCategory;
	}

	public void setRequisitionCategory(boolean requisitionCategory) {
		this.requisitionCategory = requisitionCategory;
	}

	public boolean isAuthenticationError() {
		return authenticationError;
	}

	public void setAuthenticationError(boolean authenticationError) {
		this.authenticationError = authenticationError;
	}

	public List<Company> getClientOrBUList() {
		return clientOrBUList;
	}

	public void setClientOrBUList(List<Company> clientOrBUList) {
		this.clientOrBUList = clientOrBUList;
	}

	public List<Company> getCompanyList() {
		return companyList;
	}

	public void setCompanyList(List<Company> companyList) {
		this.companyList = companyList;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Industry> getIndustryList() {
		return industryList;
	}

	public void setIndustryList(List<Industry> industryList) {
		this.industryList = industryList;
	}

	public List<JobRequisitionRole> getJobRequisitionTitleList() {
		return jobRequisitionTitleList;
	}

	public void setJobRequisitionTitleList(List<JobRequisitionRole> jobRequisitionTitleList) {
		this.jobRequisitionTitleList = jobRequisitionTitleList;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getIsNew() {
		return isNew;
	}

	public void setIsNew(String isNew) {
		this.isNew = isNew;
	}

	public RoleScreenXRef getRoleScreenXRef() {
		return roleScreenXRef;
	}

	public void setRoleScreenXRef(RoleScreenXRef roleScreenXRef) {
		this.roleScreenXRef = roleScreenXRef;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public List<String> getRequisitionCategoryList() {
		return requisitionCategoryList;
	}

	public void setRequisitionCategoryList(List<String> requisitionCategoryList) {
		this.requisitionCategoryList = requisitionCategoryList;
	}

	public boolean isAllBusOfClient() {
		return allBusOfClient;
	}

	public void setAllBusOfClient(boolean allBusOfClient) {
		this.allBusOfClient = allBusOfClient;
	}

	public Map<String, AllBusOfClientList> getAllBusOfClientList() {
		return allBusOfClientList;
	}

	public void setAllBusOfClientList(Map<String, AllBusOfClientList> allBusOfClientList) {
		this.allBusOfClientList = allBusOfClientList;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AccessControlList [accountState=").append(accountState)
				.append(", allIndustries=").append(allIndustries)
				.append(", allJobRequisitionTitleList=")
				.append(allJobRequisitionTitleList).append(", allclientOrBU=")
				.append(allclientOrBU).append(", requisitionCategory=")
				.append(requisitionCategory).append(", authenticationError=")
				.append(authenticationError).append(", clientOrBUList=")
				.append(clientOrBUList).append(", companyList=")
				.append(companyList).append(", createdBy=").append(createdBy)
				.append(", id=").append(id).append(", industryList=")
				.append(industryList).append(", jobRequisitionTitleList=")
				.append(jobRequisitionTitleList).append(", lastModifiedBy=")
				.append(lastModifiedBy).append(", isNew=").append(isNew)
				.append(", requisitionCategoryList=")
				.append(requisitionCategoryList).append(", roleScreenXRef=")
				.append(roleScreenXRef).append(", version=").append(version)
				.append("]");
		return builder.toString();
	}


}
