package com.fourdotfive.missioncontrol.pojo.user;

public enum AccountState {
	
	UNVERIFIED,ACTIVE,INACTIVE,BLOCKED,PASSWORDRESET,ARCHIVED 
	
}
