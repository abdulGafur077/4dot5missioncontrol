package com.fourdotfive.missioncontrol.pojo.user;

import java.util.List;

public class CommonScope {

private boolean all;
	
	private List<String> list;

	public boolean isAll() {
		return all;
	}

	public void setAll(boolean all) {
		this.all = all;
	}

	public List<String> getList() {
		return list;
	}

	public void setList(List<String> list) {
		this.list = list;
	}

	public CommonScope(boolean all, List<String> list) {
		super();
		this.all = all;
		this.list = list;
	}
	
	
}
