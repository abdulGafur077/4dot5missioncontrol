package com.fourdotfive.missioncontrol.pojo.user;

public class Contact {

    private String email;
    private String firstname;
    private String im;
    private String lastname;
    private String mobilephone;
    private boolean isPrimary;
    private String workphone;
    private boolean isHiringManager;
    private String userId;
    private String managerId;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getIm() {
        return im;
    }

    public void setIm(String im) {
        this.im = im;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMobilephone() {
        return mobilephone;
    }

    public void setMobilephone(String mobilephone) {
        this.mobilephone = mobilephone;
    }

    public boolean getIsPrimary() {
        return isPrimary;
    }

    public void setPrimary(boolean isPrimary) {
        this.isPrimary = isPrimary;
    }

    public String getWorkphone() {
        return workphone;
    }

    public void setWorkphone(String workphone) {
        this.workphone = workphone;
    }

    public boolean getIsHiringManager() {
        return isHiringManager;
    }

    public void setHiringManager(boolean hiringManager) {
        isHiringManager = hiringManager;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    @Override
    public String toString() {
        return "Contact [email=" + email + ", firstname=" + firstname + ", im=" + im + ", lastname=" + lastname
                + ", mobilephone=" + mobilephone + ", isPrimary=" + isPrimary + ", workphone=" + workphone +
                ", isHiringManager=" + isHiringManager + ", userId='" + userId + ", managerId=" + managerId + "]";
    }
}
