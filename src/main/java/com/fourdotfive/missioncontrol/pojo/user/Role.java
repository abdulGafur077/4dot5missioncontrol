package com.fourdotfive.missioncontrol.pojo.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.util.StringUtils;

import java.util.Comparator;

public class Role {

	private String id;

	private String name;
	
	private String description;
	
	private String createdBy; 
	
	private String lastModifiedBy; 

	@JsonProperty("new")
	private boolean isNew;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

	public static Comparator<Role> roleNameComparator = new Comparator<Role>() {
		public int compare(Role role, Role nextRole) {
			String roleName = role.getName();
			String nextRoleName = nextRole.getName();
			if (StringUtils.isEmpty(roleName)) {
				return (StringUtils.isEmpty(nextRoleName)) ? 0 : -1;
			}
			if (StringUtils.isEmpty(nextRoleName)) {
				return 1;
			}
			return roleName.compareTo(nextRoleName);
		}
	};

	@Override
	public String toString() {
		return "Role [id=" + id + ", name=" + name + ", description=" + description + ", createdBy=" + createdBy
				+ ", lastModifiedBy=" + lastModifiedBy + ", isNew=" + isNew + "]";
	}

}