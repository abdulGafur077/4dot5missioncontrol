package com.fourdotfive.missioncontrol.pojo.user;

import java.util.List;

public class RoleScreenXRef {
	
	private String id;
	
	private Role role;
	
	private List<Screen> screen;
	
	public RoleScreenXRef() {
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public List<Screen> getScreen() {
		return screen;
	}

	public void setScreen(List<Screen> screen) {
		this.screen = screen;
	}

	@Override
	public String toString() {
		return "RoleScreenXRef [id=" + id + ", role=" + role + ", screen=" + screen + "]";
	}

}
