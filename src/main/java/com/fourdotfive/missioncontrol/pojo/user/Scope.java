package com.fourdotfive.missioncontrol.pojo.user;

import java.util.ArrayList;
import java.util.List;

import com.fourdotfive.missioncontrol.dtos.company.AccessControlDto;
import com.fourdotfive.missioncontrol.dtos.company.ReqCategoryDto;
import com.fourdotfive.missioncontrol.dtos.company.SelectedClientOrBuDto;
import com.fourdotfive.missioncontrol.dtos.company.SubIndustryDto;
import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.company.Industry;

public class Scope {

	private boolean allClients;

	private boolean allIndustries;

	private boolean allJobRequisitionTitles;

	private boolean allRequisitionCategories;

	private List<String> clientList;

	private List<String> companyIDList;

	private List<Industry> industryList;

	private List<String> jobRequisitionTitleList = new ArrayList<String>();

	private List<String> requisitionCategoryList = new ArrayList<String>();

	public Scope() {
	}

	public boolean isAllClients() {
		return allClients;
	}

	public void setAllClients(boolean allClients) {
		this.allClients = allClients;
	}

	public boolean isAllIndustries() {
		return allIndustries;
	}

	public void setAllIndustries(boolean allIndustries) {
		this.allIndustries = allIndustries;
	}

	public boolean isAllJobRequisitionTitles() {
		return allJobRequisitionTitles;
	}

	public void setAllJobRequisitionTitles(boolean allJobRequisitionTitles) {
		this.allJobRequisitionTitles = allJobRequisitionTitles;
	}

	public boolean isAllRequisitionCategories() {
		return allRequisitionCategories;
	}

	public void setAllRequisitionCategories(boolean allRequisitionCategories) {
		this.allRequisitionCategories = allRequisitionCategories;
	}

	public List<String> getClientList() {
		return clientList;
	}

	public void setClientList(List<String> clientList) {
		this.clientList = clientList;
	}

	public List<String> getCompanyIDList() {
		return companyIDList;
	}

	public void setCompanyIDList(List<String> companyIDList) {
		this.companyIDList = companyIDList;
	}

	public List<Industry> getIndustryList() {
		return industryList;
	}

	public void setIndustryList(List<Industry> industryList) {
		this.industryList = industryList;
	}

	public List<String> getJobRequisitionTitleList() {
		return jobRequisitionTitleList;
	}

	public void setJobRequisitionTitleList(List<String> jobRequisitionTitleList) {
		this.jobRequisitionTitleList = jobRequisitionTitleList;
	}

	public List<String> getRequisitionCategoryList() {
		return requisitionCategoryList;
	}

	public void setRequisitionCategoryList(List<String> requisitionCategoryList) {
		this.requisitionCategoryList = requisitionCategoryList;
	}

	@Override
	public String toString() {
		return "Scope [allClients=" + allClients + ", allIndustries="
				+ allIndustries + ", allJobRequisitionTitles="
				+ allJobRequisitionTitles + ", allRequisitionCategories="
				+ allRequisitionCategories + ", clientList=" + clientList
				+ ", companyIDList=" + companyIDList + ", industryList="
				+ industryList + ", jobRequisitionTitleList="
				+ jobRequisitionTitleList + ", requisitionCategoryList="
				+ requisitionCategoryList + "]";
	}

	public static Scope getScope(AccessControlList accessControlList) {

		Scope scope = new Scope();
		scope.setAllClients(accessControlList.isAllclientOrBU());
		scope.setAllIndustries(accessControlList.isAllIndustries());
		scope.setAllJobRequisitionTitles(accessControlList
				.isAllJobRequisitionTitleList());
		scope.setAllRequisitionCategories(accessControlList
				.isAllJobRequisitionTitleList());

		List<String> clientOrBUIdList = new ArrayList<String>();
		for (Company comp : accessControlList.getClientOrBUList()) {
			clientOrBUIdList.add(comp.getId());
		}
		scope.setClientList(clientOrBUIdList);

		scope.setIndustryList(accessControlList.getIndustryList());

		List<String> companyIdList = new ArrayList<String>();
		for (Company company : accessControlList.getCompanyList()) {
			companyIdList.add(company.getId());
		}
		scope.setCompanyIDList(companyIdList);
		return scope;
	}

	public static Scope mapAccessControlDtoToScope(
			AccessControlDto accessControlDto) {
		Scope scope = new Scope();

		scope.setAllClients(accessControlDto.isAllclientOrBU());
		scope.setAllIndustries(accessControlDto.isAllIndustries());
		scope.setAllJobRequisitionTitles(accessControlDto
				.isAllJobRequisitionTitleList());
		scope.setAllRequisitionCategories(accessControlDto
				.isRequisitionCategory());

		if (!accessControlDto.isAllclientOrBU()) {
			List<String> clientList = new ArrayList<>();
			for (SelectedClientOrBuDto buorClient : accessControlDto
					.getClientOrBUList()) {
				if (buorClient.isSelected()) {
					clientList.add(buorClient.getCompanyId());
				}
			}

			scope.setClientList(clientList);
		}
		if (!accessControlDto.isAllIndustries()) {
			List<Industry> industryList = new ArrayList<>();
			for (SubIndustryDto selectedIndustry : accessControlDto
					.getIndustryList()) {
				if (selectedIndustry.isSelected()) {
					Industry industry = new Industry(selectedIndustry.getId(),
							selectedIndustry.getDescription(),
							selectedIndustry.getName());
					industryList.add(industry);
				}
			}

			scope.setIndustryList(industryList);
		}
		if (!accessControlDto.isAllJobRequisitionTitleList()) {
			List<String> reqTitle = new ArrayList<>();
			scope.setJobRequisitionTitleList(reqTitle);

		}
		if (!accessControlDto.isRequisitionCategory()) {
			List<String> reqCategories = new ArrayList<>();
			for (ReqCategoryDto reqCategory : accessControlDto
					.getRequisitionCategoryList()) {
				if (reqCategory.getIsSelected()) {
					reqCategories.add(reqCategory.getName());
				}
			}

			scope.setRequisitionCategoryList(reqCategories);
		}
		return scope;

	}

	public static Scope mapAccessControlToScope(
			AccessControlList accessControlList) {
		Scope scope = new Scope();

		scope.setAllClients(accessControlList.isAllclientOrBU());
		scope.setAllIndustries(accessControlList.isAllIndustries());
		scope.setAllJobRequisitionTitles(accessControlList
				.isAllJobRequisitionTitleList());
		scope.setAllRequisitionCategories(accessControlList
				.isRequisitionCategory());

		if (!accessControlList.isAllclientOrBU()) {
			List<String> clientList = new ArrayList<>();
			for (Company buorClient : accessControlList.getClientOrBUList()) {
				clientList.add(buorClient.getId());
			}

			scope.setClientList(clientList);
		}
		if (!accessControlList.isAllIndustries()
				&& accessControlList.getIndustryList() != null) {
			List<Industry> industryList = new ArrayList<>();
			for (Industry selectedIndustry : accessControlList
					.getIndustryList()) {
				Industry industry = new Industry(selectedIndustry.getId(),
						selectedIndustry.getDescription(),
						selectedIndustry.getName());
				industryList.add(industry);
			}

			scope.setIndustryList(industryList);
		}
		if (!accessControlList.isAllJobRequisitionTitleList()) {
			List<String> reqTitle = new ArrayList<>();
			scope.setJobRequisitionTitleList(reqTitle);

		}
		if (!accessControlList.isRequisitionCategory()
				&& accessControlList.getRequisitionCategoryList() != null) {
			List<String> reqCategories = new ArrayList<>();
			for (String reqCategory : accessControlList
					.getRequisitionCategoryList()) {
				reqCategories.add(reqCategory);
			}

			scope.setRequisitionCategoryList(reqCategories);
		}
		return scope;

	}

}
