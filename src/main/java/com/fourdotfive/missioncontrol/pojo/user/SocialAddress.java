package com.fourdotfive.missioncontrol.pojo.user;

public class SocialAddress {

	private String address;

	private String handler;

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getHandler() {
		return handler;
	}

	public void setHandler(String handler) {
		this.handler = handler;
	}

	@Override
	public String toString() {
		return "SocialAddress [address=" + address + ", handler=" + handler
				+ "]";
	}

}
