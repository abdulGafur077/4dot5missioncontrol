package com.fourdotfive.missioncontrol.pojo.user;

import java.util.Comparator;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fourdotfive.missioncontrol.pojo.company.Company;
import org.springframework.util.StringUtils;

public class User {

    private boolean acceptTermsAndCondition = true;

    private AccessControlList accessControlList;

    private Address address;

    private Authorities authorities;

    private List<User> directreports;

    private String email;

    private boolean emailNotification;

    private Company employer;

    private String firstname;

    private String forwardFromStartDate;

    private String forwardToEndDate;

    private String id;

    private String im;

    private String lastname;

    private User manager;

    private String mobilephone;

    @JsonProperty("new")
    private boolean isNew;

    private int numberOfFailedAttempts;

    private String oneTimePassword;

    private String password;

    private User recipientUser;

    private Role role;

    private boolean smsNotification;

    private String workphone;

    private String createdUser;

    private String lastModifiedUser;

    private boolean isTokenExpired;

    private String buId;

    private String department;

    private String title;

    private Long version;

    private boolean isPointOfContact;

    public boolean isAcceptTermsAndCondition() {
        return acceptTermsAndCondition;
    }

    public void setAcceptTermsAndCondition(boolean acceptTermsAndCondition) {
        this.acceptTermsAndCondition = acceptTermsAndCondition;
    }

    public AccessControlList getAccessControlList() {
        return accessControlList;
    }

    public void setAccessControlList(AccessControlList accessControlList) {
        this.accessControlList = accessControlList;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Authorities getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Authorities authorities) {
        this.authorities = authorities;
    }

    public List<User> getDirectreports() {
        return directreports;
    }

    public void setDirectreports(List<User> directreports) {
        this.directreports = directreports;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEmailNotification() {
        return emailNotification;
    }

    public void setEmailNotification(boolean emailNotification) {
        this.emailNotification = emailNotification;
    }

    public Company getEmployer() {
        return employer;
    }

    public void setEmployer(Company employer) {
        this.employer = employer;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getForwardFromStartDate() {
        return forwardFromStartDate;
    }

    public void setForwardFromStartDate(String forwardFromStartDate) {
        this.forwardFromStartDate = forwardFromStartDate;
    }

    public String getForwardToEndDate() {
        return forwardToEndDate;
    }

    public void setForwardToEndDate(String forwardToEndDate) {
        this.forwardToEndDate = forwardToEndDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIm() {
        return im;
    }

    public void setIm(String im) {
        this.im = im;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public User getManager() {
        return manager;
    }

    public void setManager(User manager) {
        this.manager = manager;
    }

    public String getMobilephone() {
        return mobilephone;
    }

    public void setMobilephone(String mobilephone) {
        this.mobilephone = mobilephone;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean isNew) {
        this.isNew = isNew;
    }

    public int getNumberOfFailedAttempts() {
        return numberOfFailedAttempts;
    }

    public void setNumberOfFailedAttempts(int numberOfFailedAttempts) {
        this.numberOfFailedAttempts = numberOfFailedAttempts;
    }

    public String getOneTimePassword() {
        return oneTimePassword;
    }

    public void setOneTimePassword(String oneTimePassword) {
        this.oneTimePassword = oneTimePassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User getRecipientUser() {
        return recipientUser;
    }

    public void setRecipientUser(User recipientUser) {
        this.recipientUser = recipientUser;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public boolean isSmsNotification() {
        return smsNotification;
    }

    public void setSmsNotification(boolean smsNotification) {
        this.smsNotification = smsNotification;
    }

    public String getWorkphone() {
        return workphone;
    }

    public void setWorkphone(String workphone) {
        this.workphone = workphone;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public String getLastModifiedUser() {
        return lastModifiedUser;
    }

    public void setLastModifiedUser(String lastModifiedUser) {
        this.lastModifiedUser = lastModifiedUser;
    }

    public boolean isTokenExpired() {
        return isTokenExpired;
    }

    public void setTokenExpired(boolean tokenExpired) {
        isTokenExpired = tokenExpired;
    }

    public String getBuId() {
        return buId;
    }

    public void setBuId(String buId) {
        this.buId = buId;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public boolean getIsPointOfContact() {
        return isPointOfContact;
    }

    public void setIsPointOfContact(boolean pointOfContact) {
        isPointOfContact = pointOfContact;
    }

    public static Comparator<User> userNameComparator = new Comparator<User>() {
        public int compare(User user, User nextUser) {
            String userName = user.getFirstname();
            String nextUserName = nextUser.getFirstname();
            if (StringUtils.isEmpty(userName)) {
                return (StringUtils.isEmpty(nextUserName)) ? 0 : -1;
            }
            if (StringUtils.isEmpty(nextUserName)) {
                return 1;
            }
            return userName.toUpperCase().compareTo(nextUserName.toUpperCase());
        }
    };


    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("StubUser [acceptTermsAndCondition=")
                .append(acceptTermsAndCondition).append(", accessControlList=")
                .append(accessControlList).append(", address=").append(address)
                .append(", authorities=").append(authorities)
                .append(", directreports=").append(directreports)
                .append(", email=").append(email)
                .append(", emailNotification=").append(emailNotification)
                .append(", employer=").append(employer).append(", firstname=")
                .append(firstname).append(", forwardFromStartDate=")
                .append(forwardFromStartDate).append(", forwardToEndDate=")
                .append(forwardToEndDate).append(", id=").append(id)
                .append(", im=").append(im).append(", lastname=")
                .append(lastname).append(", manager=").append(manager)
                .append(", mobilephone=").append(mobilephone)
                .append(", isNew=").append(isNew)
                .append(", numberOfFailedAttempts=")
                .append(numberOfFailedAttempts).append(", oneTimePassword=")
                .append(oneTimePassword).append(", password=").append(password)
                .append(", recipientUser=").append(recipientUser)
                .append(", role=").append(role).append(", smsNotification=")
                .append(smsNotification).append(", workphone=")
                .append(workphone).append(", createdUser=").append(createdUser)
                .append(", lastModifiedByUser=").append(lastModifiedUser).append("]");
        return builder.toString();
    }


}