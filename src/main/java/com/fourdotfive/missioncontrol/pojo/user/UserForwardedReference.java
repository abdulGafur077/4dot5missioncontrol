package com.fourdotfive.missioncontrol.pojo.user;

import com.fourdotfive.missioncontrol.pojo.FourDotFiveDate;

public class UserForwardedReference {

	private String createdBy;
	private FourDotFiveDate createdDate;
	private String createdUser;
	private FourDotFiveDate forwardFromStartDate;
	private FourDotFiveDate forwardToEndDate;
	private String id;
	private String lastModifiedBy;
	private FourDotFiveDate lastModifiedDate;
	private String lastModifiedUser;
	private boolean isNew;
	private User user;
	private int version;

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public FourDotFiveDate getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(FourDotFiveDate createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public FourDotFiveDate getForwardFromStartDate() {
		return forwardFromStartDate;
	}

	public void setForwardFromStartDate(FourDotFiveDate forwardFromStartDate) {
		this.forwardFromStartDate = forwardFromStartDate;
	}

	public FourDotFiveDate getForwardToEndDate() {
		return forwardToEndDate;
	}

	public void setForwardToEndDate(FourDotFiveDate forwardToEndDate) {
		this.forwardToEndDate = forwardToEndDate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public FourDotFiveDate getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(FourDotFiveDate lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getLastModifiedUser() {
		return lastModifiedUser;
	}

	public void setLastModifiedUser(String lastModifiedUser) {
		this.lastModifiedUser = lastModifiedUser;
	}

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UserForwardedReference [createdBy=").append(createdBy)
				.append(", createdDate=").append(createdDate)
				.append(", createdUser=").append(createdUser)
				.append(", forwardFromStartDate=").append(forwardFromStartDate)
				.append(", forwardToEndDate=").append(forwardToEndDate)
				.append(", id=").append(id).append(", lastModifiedBy=")
				.append(lastModifiedBy).append(", lastModifiedDate=")
				.append(lastModifiedDate).append(", lastModifiedUser=")
				.append(lastModifiedUser).append(", isNew=").append(isNew)
				.append(", user=").append(user).append(", version=")
				.append(version).append("]");
		return builder.toString();
	}

}
