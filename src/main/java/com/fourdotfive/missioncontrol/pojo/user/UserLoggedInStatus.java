package com.fourdotfive.missioncontrol.pojo.user;

public class UserLoggedInStatus {

    private String userId;

    private String roleId;

    private boolean isBUOrClientListEmptyForUser;

    private boolean isBUsOrClientsAvailable;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public boolean getIsBUOrClientListEmptyForUser() {
        return isBUOrClientListEmptyForUser;
    }

    public void setIsBUOrClientListEmptyForUser(boolean BUOrClientListEmptyForUser) {
        isBUOrClientListEmptyForUser = BUOrClientListEmptyForUser;
    }

    public boolean getIsBUsOrClientsAvailable() {
        return isBUsOrClientsAvailable;
    }

    public void setIsBUsOrClientsAvailable(boolean BUsOrClientsAvailable) {
        isBUsOrClientsAvailable = BUsOrClientsAvailable;
    }
}
