package com.fourdotfive.missioncontrol.pojo.user;

public class UserRecipient {

	private String fromEndDateTime;
	private String fromStartDateTime;
	private String fromUserID;
	private String toUserID;

	public UserRecipient(String fromStartDateTime, String fromEndDateTime,
			String fromUserID, String toUserID) {
		super();
		this.fromEndDateTime = fromEndDateTime;
		this.fromStartDateTime = fromStartDateTime;
		this.fromUserID = fromUserID;
		this.toUserID = toUserID;
	}

	public UserRecipient() {
		super();
	}

	public UserRecipient(String fromUserID, String toUserID) {
		super();
		this.fromUserID = fromUserID;
		this.toUserID = toUserID;
	}

	public String getFromEndDateTime() {
		return fromEndDateTime;
	}

	public void setFromEndDateTime(String fromEndDateTime) {
		this.fromEndDateTime = fromEndDateTime;
	}

	public String getFromStartDateTime() {
		return fromStartDateTime;
	}

	public void setFromStartDateTime(String fromStartDateTime) {
		this.fromStartDateTime = fromStartDateTime;
	}

	public String getFromUserID() {
		return fromUserID;
	}

	public void setFromUserID(String fromUserID) {
		this.fromUserID = fromUserID;
	}

	public String getToUserID() {
		return toUserID;
	}

	public void setToUserID(String toUserID) {
		this.toUserID = toUserID;
	}

	@Override
	public String toString() {
		return "UserRecipient [fromEndDateTime=" + fromEndDateTime
				+ ", fromStartDateTime=" + fromStartDateTime + ", fromUserID="
				+ fromUserID + ", toUserID=" + toUserID + "]";
	}

}
