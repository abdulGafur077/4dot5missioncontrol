package com.fourdotfive.missioncontrol.pojo.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fourdotfive.missioncontrol.dtos.company.*;
import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.company.Industry;
import com.fourdotfive.missioncontrol.pojo.company.RoleEntityScope;
import org.springframework.util.CollectionUtils;

public class UserScope {
	private boolean allClients;

	private boolean allIndustries;

	private boolean allJobRequisitionTitles;

	private boolean allRequisitionCategories;

	private List<String> clientList;

	private List<Industry> industryList;

	private List<String> jobRequisitionTitleList = new ArrayList<String>();

	private List<String> requisitionCategoryList = new ArrayList<String>();

	private boolean allBusOfClient;

	private Map<String, List<String>> allBusOfClientList;

	public boolean isAllClients() {
		return allClients;
	}

	public void setAllClients(boolean allClients) {
		this.allClients = allClients;
	}

	public boolean isAllIndustries() {
		return allIndustries;
	}

	public void setAllIndustries(boolean allIndustries) {
		this.allIndustries = allIndustries;
	}

	public boolean isAllJobRequisitionTitles() {
		return allJobRequisitionTitles;
	}

	public void setAllJobRequisitionTitles(boolean allJobRequisitionTitles) {
		this.allJobRequisitionTitles = allJobRequisitionTitles;
	}

	public boolean isAllRequisitionCategories() {
		return allRequisitionCategories;
	}

	public void setAllRequisitionCategories(boolean allRequisitionCategories) {
		this.allRequisitionCategories = allRequisitionCategories;
	}

	public List<String> getClientList() {
		return clientList;
	}

	public void setClientList(List<String> clientList) {
		this.clientList = clientList;
	}

	public List<Industry> getIndustryList() {
		return industryList;
	}

	public void setIndustryList(List<Industry> industryList) {
		this.industryList = industryList;
	}

	public List<String> getJobRequisitionTitleList() {
		return jobRequisitionTitleList;
	}

	public void setJobRequisitionTitleList(List<String> jobRequisitionTitleList) {
		this.jobRequisitionTitleList = jobRequisitionTitleList;
	}

	public List<String> getRequisitionCategoryList() {
		return requisitionCategoryList;
	}

	public void setRequisitionCategoryList(List<String> requisitionCategoryList) {
		this.requisitionCategoryList = requisitionCategoryList;
	}

	public boolean isAllBusOfClient() {
		return allBusOfClient;
	}

	public void setAllBusOfClient(boolean allBusOfClient) {
		this.allBusOfClient = allBusOfClient;
	}

	public Map<String, List<String>> getAllBusOfClientList() {
		return allBusOfClientList;
	}

	public void setAllBusOfClientList(Map<String, List<String>> allBusOfClientList) {
		this.allBusOfClientList = allBusOfClientList;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UserScope [allClients=");
		builder.append(allClients);
		builder.append(", allIndustries=");
		builder.append(allIndustries);
		builder.append(", allJobRequisitionTitles=");
		builder.append(allJobRequisitionTitles);
		builder.append(", allRequisitionCategories=");
		builder.append(allRequisitionCategories);
		builder.append(", clientList=");
		builder.append(clientList);
		builder.append(", industryList=");
		builder.append(industryList);
		builder.append(", jobRequisitionTitleList=");
		builder.append(jobRequisitionTitleList);
		builder.append(", requisitionCategoryList=");
		builder.append(requisitionCategoryList);
		builder.append("]");
		return builder.toString();
	}

	public static Scope getScope(AccessControlList accessControlList) {

		Scope scope = new Scope();
		scope.setAllClients(accessControlList.isAllclientOrBU());
		scope.setAllIndustries(accessControlList.isAllIndustries());
		scope.setAllJobRequisitionTitles(accessControlList
				.isAllJobRequisitionTitleList());
		scope.setAllRequisitionCategories(accessControlList
				.isAllJobRequisitionTitleList());

		List<String> clientOrBUIdList = new ArrayList<String>();
		for (Company comp : accessControlList.getClientOrBUList()) {
			clientOrBUIdList.add(comp.getId());
		}
		scope.setClientList(clientOrBUIdList);

		scope.setIndustryList(accessControlList.getIndustryList());

		List<String> companyIdList = new ArrayList<String>();
		for (Company company : accessControlList.getCompanyList()) {
			companyIdList.add(company.getId());
		}
		scope.setCompanyIDList(companyIdList);
		return scope;
	}

	public static UserScope mapAccessControlToScope(
			AccessControlDto accessControlDto) {
		UserScope scope = new UserScope();

		scope.setAllClients(accessControlDto.isAllclientOrBU());
		scope.setAllIndustries(accessControlDto.isAllIndustries());
		scope.setAllBusOfClient(accessControlDto.isAllBusOfClient());
		scope.setAllJobRequisitionTitles(accessControlDto
				.isAllJobRequisitionTitleList());
		scope.setAllRequisitionCategories(accessControlDto
				.isRequisitionCategory());


		if (accessControlDto.getUserClientBUinfo() != null) {
			UserClientBUinfo userClientBUinfo = accessControlDto.getUserClientBUinfo();
			Map<String, ClientBUInfo>  clientBUInfo = userClientBUinfo.getClientBUInfo();

			List<String> clientsOrBus = new ArrayList<>();
			Map<String, List<String>> allBusOfClients = new HashMap<>();
			if (clientBUInfo != null) {
				for (Map.Entry<String, ClientBUInfo> clientBUDeatails : clientBUInfo.entrySet()) {
					if (clientBUDeatails == null)
						continue;

					ClientBUInfo info = clientBUDeatails.getValue();
					if (info.isAvailable() && info.isEnabled()) {
						clientsOrBus.add(clientBUDeatails.getKey());
					}

					if (CollectionUtils.isEmpty(info.getBuInfos()))
						continue;

					List<String> busOfClients = new ArrayList<>();
					for (UserCorporateClientBUInfo corporateClientBUInfo : info.getBuInfos()) {
						if (corporateClientBUInfo.isAvailable() && corporateClientBUInfo.isEnabled()) {
							busOfClients.add(corporateClientBUInfo.getBuId());
						}
					}

					allBusOfClients.put(clientBUDeatails.getKey(), busOfClients);
				}
			}
			scope.setClientList(clientsOrBus);
			scope.setAllBusOfClientList(allBusOfClients);

		}

		if (accessControlDto.getIndustryList() != null
				&& !accessControlDto.isAllIndustries()) {
			List<Industry> industryList = new ArrayList<>();
			for (SubIndustryDto selectedIndustry : accessControlDto
					.getIndustryList()) {
				if (selectedIndustry.isSelected()) {
					Industry industry = new Industry(selectedIndustry.getId(),
							selectedIndustry.getDescription(),
							selectedIndustry.getName());
					industryList.add(industry);
				}
			}

			scope.setIndustryList(industryList);
		}
		if (!accessControlDto.isAllJobRequisitionTitleList()) {
			List<String> reqTitle = new ArrayList<>();
			scope.setJobRequisitionTitleList(reqTitle);

		}
		if (accessControlDto.getRequisitionCategoryList() != null
				&& !accessControlDto.isRequisitionCategory()) {
			List<String> reqCategories = new ArrayList<>();
			for (ReqCategoryDto reqCategory : accessControlDto
					.getRequisitionCategoryList()) {
				if (reqCategory.getIsSelected()) {
					reqCategories.add(reqCategory.getName());
				}
			}

			scope.setRequisitionCategoryList(reqCategories);
		}
		return scope;

	}

	public static UserScope mapAccessControlToScope(
			AccessControlList accessControlList) {
		UserScope scope = new UserScope();

		scope.setAllClients(accessControlList.isAllclientOrBU());
		scope.setAllIndustries(accessControlList.isAllIndustries());
		scope.setAllJobRequisitionTitles(accessControlList
				.isAllJobRequisitionTitleList());
		scope.setAllRequisitionCategories(accessControlList
				.isRequisitionCategory());

		if (!accessControlList.isAllclientOrBU()) {
			List<String> clientList = new ArrayList<>();
			for (Company buorClient : accessControlList.getClientOrBUList()) {
				clientList.add(buorClient.getId());
			}

			scope.setClientList(clientList);
		}
		if (!accessControlList.isAllIndustries()
				&& accessControlList.getIndustryList() != null) {
			List<Industry> industryList = new ArrayList<>();
			for (Industry selectedIndustry : accessControlList
					.getIndustryList()) {
				Industry industry = new Industry(selectedIndustry.getId(),
						selectedIndustry.getDescription(),
						selectedIndustry.getName());
				industryList.add(industry);
			}

			scope.setIndustryList(industryList);
		}
		if (!accessControlList.isAllJobRequisitionTitleList()) {
			List<String> reqTitle = new ArrayList<>();
			scope.setJobRequisitionTitleList(reqTitle);

		}
		if (!accessControlList.isRequisitionCategory()
				&& accessControlList.getRequisitionCategoryList() != null) {
			List<String> reqCategories = new ArrayList<>();
			for (String reqCategory : accessControlList
					.getRequisitionCategoryList()) {
				reqCategories.add(reqCategory);
			}

			scope.setRequisitionCategoryList(reqCategories);
		}
		return scope;

	}

	public static UserScope setScopeFromRoleEntityScope(
			RoleEntityScope roleEntityScope, List<Company> clientList) {

		UserScope scope = setScopeForCompanyAdmin();
		scope.setAllClients(roleEntityScope.isAllClients());
		scope.setAllBusOfClient(roleEntityScope.isAllBUsOfClients());
		if (!roleEntityScope.isAllClients()) {
			if (clientList != null) {
				List<String> newClientList = new ArrayList<>();
				for (Company buorClient : clientList) {
					newClientList.add(buorClient.getId());
				}

				scope.setClientList(newClientList);
			} else {
				scope.setClientList(new ArrayList<String>());
			}
		}
		return scope;

	}

	public static UserScope setScopeForCompanyAdmin() {

		UserScope scope = new UserScope();
		scope.setAllClients(true);
		scope.setAllIndustries(true);
		scope.setAllJobRequisitionTitles(true);
		scope.setAllRequisitionCategories(true);
		scope.setClientList(new ArrayList<String>());
		scope.setIndustryList(new ArrayList<Industry>());
		scope.setJobRequisitionTitleList(new ArrayList<String>());
		scope.setRequisitionCategoryList(new ArrayList<String>());
		scope.setAllBusOfClient(true);
		scope.setAllBusOfClientList(new HashMap<String, List<String>>());
		return scope;

	}
}
