package com.fourdotfive.missioncontrol.pojo.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fourdotfive.missioncontrol.dtos.company.CompanyMinInfoDto;

public class VerifyPassword {

	private String id;

	private String firstname;

	private String lastname;

	private String emailId;

	private RoleScreenXRef roleScreenXRef;

	private String accountState;

	private String industry;

	private String clientOrBU;

	private boolean authenticationError;

	private CompanyMinInfoDto company;

	@JsonProperty("new")
	private boolean isNew;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public RoleScreenXRef getRoleScreenXRef() {
		return roleScreenXRef;
	}

	public void setRoleScreenXRef(RoleScreenXRef roleScreenXRef) {
		this.roleScreenXRef = roleScreenXRef;
	}

	public String getAccountState() {
		return accountState;
	}

	public void setAccountState(String accountState) {
		this.accountState = accountState;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getClientOrBU() {
		return clientOrBU;
	}

	public void setClientOrBU(String clientOrBU) {
		this.clientOrBU = clientOrBU;
	}

	public boolean isAuthenticationError() {
		return authenticationError;
	}

	public void setAuthenticationError(boolean authenticationError) {
		this.authenticationError = authenticationError;
	}

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public CompanyMinInfoDto getCompany() {
		return company;
	}

	public void setCompany(CompanyMinInfoDto company) {
		this.company = company;
	}

}
