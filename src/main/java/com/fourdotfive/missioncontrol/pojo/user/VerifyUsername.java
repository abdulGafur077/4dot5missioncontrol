package com.fourdotfive.missioncontrol.pojo.user;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VerifyUsername {

	private String id;

	private String name;

	private String emailId;

	private String companyId;

	private AccessControlList accessControlList;

	private String industry;

	private String clientOrBU;

	private boolean authenticationError;

	@JsonProperty("new")
	private boolean isNew;

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public AccessControlList getAccessControlList() {
		return accessControlList;
	}

	public void setAccessControlList(AccessControlList accessControlList) {
		this.accessControlList = accessControlList;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getClientOrBU() {
		return clientOrBU;
	}

	public void setClientOrBU(String clientOrBU) {
		this.clientOrBU = clientOrBU;
	}

	public boolean isAuthenticationError() {
		return authenticationError;
	}

	public void setAuthenticationError(boolean authenticationError) {
		this.authenticationError = authenticationError;
	}

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

	@Override
	public String toString() {
		return "VerifyUsername [id=" + id + ", name=" + name + ", emailId=" + emailId + ", companyId=" + companyId
				+ ", accessControlList=" + accessControlList + ", industry=" + industry + ", clientOrBU=" + clientOrBU
				+ ", authenticationError=" + authenticationError + ", isNew=" + isNew + "]";
	}

}
