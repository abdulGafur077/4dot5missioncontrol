package com.fourdotfive.missioncontrol.reports;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import TeamPerformanceByMesure.TeamPerformanceByMember;

import com.fourdotfive.missioncontrol.common.AppConstants;
import com.fourdotfive.missioncontrol.common.CommonUtil;
import com.fourdotfive.missioncontrol.common.TokenDetails;
import com.fourdotfive.missioncontrol.pojo.reports.ReportRequestDto;
import com.fourdotfive.missioncontrol.pojo.reports.SourcePerformanceObject;
import com.fourdotfive.missioncontrol.pojo.reports.TeamPerformanceByMeasure;
import com.fourdotfive.missioncontrol.pojo.reports.TimeIntervalBarChart;
/**
 * Controller to generate reports
 * @author Utpal Kant Sharma
 *
 */
@RestController
@RequestMapping("/api/report")
public class ReportController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ReportController.class);

	@Autowired
	private ReportsService reportsService;
	
	@RequestMapping(value = "/getactive_candidates_performance_report", method = RequestMethod.POST)
	public ResponseEntity<?> getActiveCandiatesPerformanceRepot(@RequestBody ReportRequestDto reportRequest,HttpServletRequest request) {
		TokenDetails tokenDetails = getTokenDetailsFromRequest(request);
		LOGGER.debug("Invoked API getactive_candidates_performance_repor with parameters, Query parameter"+reportRequest+" ,, userId - "+tokenDetails.getUserId());
		TimeIntervalBarChart response = reportsService.getActiveCandidatesPerformanceReport(reportRequest,tokenDetails.getUserId());

		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getpassive_candidates_performance_report/{graphType}", method = RequestMethod.POST)
	public ResponseEntity<?> getPassiveCandiatesPerformanceRepot(@RequestBody ReportRequestDto reportRequest,HttpServletRequest request) {
		TokenDetails tokenDetails = getTokenDetailsFromRequest(request);
		LOGGER.debug("Invoked API getpassive_candidates_performance_repor with parameters, Query parameter"+reportRequest+" ,, userId - "+tokenDetails.getUserId());
		TimeIntervalBarChart response = reportsService.getPassiveCandidatesPerformanceReport(reportRequest,tokenDetails.getUserId());

		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getrequisition_performance_report/{graphType}", method = RequestMethod.POST)
	public ResponseEntity<?> getRequisitionPerformanceRepot(@RequestBody ReportRequestDto reportRequest,HttpServletRequest request) {
		TokenDetails tokenDetails = getTokenDetailsFromRequest(request);
		LOGGER.debug("Invoked API getpassive_candidates_performance_repor with parameters, Query parameter"+reportRequest+" ,, userId - "+tokenDetails.getUserId());
		TimeIntervalBarChart response = reportsService.getPassiveCandidatesPerformanceReport(reportRequest,tokenDetails.getUserId());

		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/get_sourcing_performance_report", method = RequestMethod.POST)
	public ResponseEntity<?> getSourcingPerformance(@RequestBody ReportRequestDto reportRequest,HttpServletRequest request) {
		TokenDetails tokenDetails = getTokenDetailsFromRequest(request);
		LOGGER.debug("Invoked API get_sourcing_performance_report with parameters, Query parameter"+reportRequest+" ,, userId - "+tokenDetails.getUserId());
		List<SourcePerformanceObject> performanceObjects=reportsService.getSourcingPerformance(reportRequest,tokenDetails.getUserId());
		
		return new ResponseEntity<>(performanceObjects, HttpStatus.OK);
	}
	
	
	
	//API for Team Member Performance by Measure.
	@RequestMapping(value = "/get_team_member_performance_by_measure_timetoconvert_report", method = RequestMethod.POST)
	public ResponseEntity<?> getTeamMemberPerformanceByTeamMembersTimeToConvert(@RequestBody ReportRequestDto reportRequest,HttpServletRequest request) {
		TokenDetails tokenDetails = getTokenDetailsFromRequest(request);
		LOGGER.debug("Invoked API get_team_member_performance_by_measure_timetoconvert_report with parameters, Query parameter"+reportRequest+" ,, userId - "+tokenDetails.getUserId());
		List<TeamPerformanceByMeasure> performanceObjects=reportsService.getTeamMemberPerformanceByMeasureTimeToConvert(reportRequest,tokenDetails.getUserId());
		
		return new ResponseEntity<>(performanceObjects, HttpStatus.OK);
	}
	@RequestMapping(value = "/get_team_member_performance_by_measure_active_report", method = RequestMethod.POST)
	public ResponseEntity<?> getTeamMemberPerformanceByTeamMembersActive(@RequestBody ReportRequestDto reportRequest,HttpServletRequest request) {
		TokenDetails tokenDetails = getTokenDetailsFromRequest(request);
		LOGGER.debug("Invoked API get_team_member_performance_by_measure_active_report with parameters, Query parameter"+reportRequest+" ,, userId - "+tokenDetails.getUserId());
		List<TeamPerformanceByMeasure> performanceObjects=reportsService.getTeamMemberPerformanceByMeasureActive(reportRequest,tokenDetails.getUserId());
		
		return new ResponseEntity<>(performanceObjects, HttpStatus.OK);
	}
	@RequestMapping(value = "/get_team_member_performance_by_measure_passive_report", method = RequestMethod.POST)
	public ResponseEntity<?> getTeamMemberPerformanceByTeamMembersPassive(@RequestBody ReportRequestDto reportRequest,HttpServletRequest request) {
		TokenDetails tokenDetails = getTokenDetailsFromRequest(request);
		LOGGER.debug("Invoked API get_team_member_performance_by_measure_passive_report with parameters, Query parameter"+reportRequest+" ,, userId - "+tokenDetails.getUserId());
		List<TeamPerformanceByMeasure> performanceObjects=reportsService.getTeamMemberPerformanceByMeasurePassive(reportRequest,tokenDetails.getUserId());
		
		return new ResponseEntity<>(performanceObjects, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/get_team_member_performance_by_team_member_report", method = RequestMethod.POST)
	public ResponseEntity<?> getTeamMemberPerformanceByTeamMember(@RequestBody ReportRequestDto reportRequest,HttpServletRequest request) {
		TokenDetails tokenDetails = getTokenDetailsFromRequest(request);
		LOGGER.debug("Invoked API get_team_member_performance_by_team_member_report with parameters, Query parameter"+reportRequest+" ,, userId - "+tokenDetails.getUserId());
		List<TeamPerformanceByMember>  performanceObjects=reportsService.getTeamMemberPerformanceByTeamMember(reportRequest,tokenDetails.getUserId());
		
		return new ResponseEntity<>(performanceObjects, HttpStatus.OK);
	}
	
	
	/**
	 * Common method to fetch <code>TokenDetails</code> from request parameter.
	 * @param request
	 * @return TokenDetails object
	 */
	private TokenDetails getTokenDetailsFromRequest(HttpServletRequest request){
		final String authHeader = request.getHeader(AppConstants.AUTHORIZATION);
		return CommonUtil.getTokenDetails(authHeader);
	}
}
