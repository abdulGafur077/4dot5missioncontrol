package com.fourdotfive.missioncontrol.reports;

import java.util.List;

import TeamPerformanceByMesure.TeamPerformanceByMember;

import com.fourdotfive.missioncontrol.pojo.reports.ReportRequestDto;
import com.fourdotfive.missioncontrol.pojo.reports.SourcePerformanceObject;
import com.fourdotfive.missioncontrol.pojo.reports.TeamPerformanceByMeasure;
import com.fourdotfive.missioncontrol.pojo.reports.TimeIntervalBarChart;

public interface ReportsService {
	/**
	 * Method to get report for active candidates
	 * @param reportRequest
	 * @param userId
	 * @param reportType
	 * @return TimeIntervalBarChart
	 */
	TimeIntervalBarChart getActiveCandidatesPerformanceReport(ReportRequestDto reportRequest,String userId);
	
	/**
	 * Method to get report for passive candidates
	 * @param reportRequest
	 * @param userId
	 * @param reportType
	 * @return TimeIntervalBarChart
	 * @return TimeIntervalBarChart
	 */
	TimeIntervalBarChart getPassiveCandidatesPerformanceReport(ReportRequestDto reportRequest,String userId);
	
	/**
	 * Method to get report for requisition performance
	 * @param reportRequest
	 * @param userId
	 * @param reportType
	 * @return TimeIntervalBarChart
	 * @return TimeIntervalBarChart
	 */
	TimeIntervalBarChart getRequisitionPerformanceRepot(ReportRequestDto reportRequest,String userId);
	
	/**
	 * Method to get report for team member performance by team member.<br>This is a sub type of member performance report
	 * @param reportRequest
	 * @param userId
	 * @param reportType
	 * @return TimeIntervalBarChart
	 * @return TimeIntervalBarChart
	 */
	List<TeamPerformanceByMeasure> getTeamMemberPerformanceByMeasureTimeToConvert(ReportRequestDto reportRequest,String userId);
	
	/**
	 * Method to get report for team member performance by team member.<br>This is a sub type of member performance report
	 * @param reportRequest
	 * @param userId
	 * @param reportType
	 * @return TimeIntervalBarChart
	 * @return TimeIntervalBarChart
	 */
	List<TeamPerformanceByMeasure> getTeamMemberPerformanceByMeasureActive(ReportRequestDto reportRequest,String userId);
	/**
	 * Method to get report for team member performance by team member.<br>This is a sub type of member performance report
	 * @param reportRequest
	 * @param userId
	 * @param reportType
	 * @return TimeIntervalBarChart
	 * @return TimeIntervalBarChart
	 */
	List<TeamPerformanceByMeasure> getTeamMemberPerformanceByMeasurePassive(ReportRequestDto reportRequest,String userId);
	/**
	 * Method to get report for team member performance by team member.<br>This is a sub type of member performance report
	 * @param reportRequest
	 * @param userId
	 * @param reportType
	 * @return TimeIntervalBarChart
	 * @return TimeIntervalBarChart
	 */
	List<TeamPerformanceByMember> getTeamMemberPerformanceByTeamMember(ReportRequestDto reportRequest,String userId);
	/**
	 * Method to build source performance graph object, which include <li>Source Performance Graph data</li>
	 * <li>Source Performance data</li>
	 * @param userId
	 * @return
	 */
	List<SourcePerformanceObject> getSourcingPerformance(ReportRequestDto reportRequest,String userId);

}
