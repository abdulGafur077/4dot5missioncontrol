package com.fourdotfive.missioncontrol.reports;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import TeamPerformanceByMesure.TeamPerformanceByMember;

import com.fourdotfive.missioncontrol.common.ApiConstants;
import com.fourdotfive.missioncontrol.pojo.reports.ActivePassiveTransactionReportPlatformDto;
import com.fourdotfive.missioncontrol.pojo.reports.EnumCandidateStatus;
import com.fourdotfive.missioncontrol.pojo.reports.ReportRequestDto;
import com.fourdotfive.missioncontrol.pojo.reports.RequistionPerformanceReportPlatformDto;
import com.fourdotfive.missioncontrol.pojo.reports.SourcePerformance;
import com.fourdotfive.missioncontrol.pojo.reports.SourcePerformanceBarChart;
import com.fourdotfive.missioncontrol.pojo.reports.SourcePerformanceData;
import com.fourdotfive.missioncontrol.pojo.reports.SourcePerformanceGraph;
import com.fourdotfive.missioncontrol.pojo.reports.SourcePerformanceObject;
import com.fourdotfive.missioncontrol.pojo.reports.SourcingPerformanceReportData;
import com.fourdotfive.missioncontrol.pojo.reports.TeamPerformanceByMeasure;
import com.fourdotfive.missioncontrol.pojo.reports.TimeIntervalBarChart;
/**
 * 
 * @author Utpal Kant Sharma
 *
 */
@Service
public class ReportsServiceImp implements ReportsService{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ReportsServiceImp.class);
	private final int GRAPH_SIZE=6;
	
	@Autowired
	private RestTemplate restTemplate;

	@Override
	public TimeIntervalBarChart getActiveCandidatesPerformanceReport(ReportRequestDto reportRequest,String userId) {
		return getActiveCandidatesPerformanceReportFromPlatform(reportRequest,userId);
	}

	private TimeIntervalBarChart getMonthlyActiveCandidatesPerformanceReport(ReportRequestDto reportRequest,String userId){
		TimeIntervalBarChart timeIntervalBarChart=new TimeIntervalBarChart();
		timeIntervalBarChart.setReportType(reportRequest.getEnumReportType());
		timeIntervalBarChart.setFirstLine(java.util.Arrays.asList(new Integer[]{12,14,34,45,67,34}));
		timeIntervalBarChart.setSecondLine(java.util.Arrays.asList(new Integer[]{12,14,34,45,67,34}));
		timeIntervalBarChart.setTimeToFill(java.util.Arrays.asList(new Integer[]{12,14,34,45,67,34}));
		LOGGER.debug("Returning data - "+timeIntervalBarChart);
		return timeIntervalBarChart;
	}
	
	private TimeIntervalBarChart getActiveCandidatesPerformanceReportFromPlatform(ReportRequestDto reportRequest,String userId){

		String url = new StringBuilder().append(ApiConstants.BASE_URL).append("/").append("getActiveCandidatesPerformanceReport").toString();
		
		ResponseEntity<List<ActivePassiveTransactionReportPlatformDto>> response = restTemplate.exchange(url,HttpMethod.POST, new HttpEntity<ReportRequestDto>(reportRequest),new ParameterizedTypeReference<List<ActivePassiveTransactionReportPlatformDto>>() {
				});
		
		return convertToChar(response.getBody(),reportRequest,EnumCandidateStatus.ACTIVE);
	
	}
	
	private TimeIntervalBarChart convertToChar(List<ActivePassiveTransactionReportPlatformDto> list,ReportRequestDto requestDto,EnumCandidateStatus graphFor){
		if(list==null || list.isEmpty())
			return null;
		TimeIntervalBarChart chart =new TimeIntervalBarChart();
		chart.setReportType(requestDto.getEnumReportType());
		//At any point of time graph will contain 6 records
		List<Integer> time=new ArrayList<Integer>(GRAPH_SIZE);
		List<Integer> count1=new ArrayList<Integer>(GRAPH_SIZE);
		List<Integer> count2=new ArrayList<Integer>(GRAPH_SIZE);
		for(ActivePassiveTransactionReportPlatformDto dto:list){
		
			switch (graphFor) {
			case ACTIVE:
				time.add(dto.getAvgTimeToPlace());
				count1.add(dto.getActiveCandidateCount());
				count2.add(dto.getPlacedCandidateCount());
				break;

			case PASSIVE:
				time.add(dto.getAvgTimeToConvert());
				count1.add(dto.getPassiveCandidateCount());
				count2.add(dto.getPassiveConversionCount());
				break;
			}
		}
		chart.setTimeToFill(time);
		chart.setFirstLine(count1);
		chart.setSecondLine(count2);
		return chart;
	}
	
	
	@Override
	public TimeIntervalBarChart getPassiveCandidatesPerformanceReport(ReportRequestDto reportRequest, String userId) {
		String url = new StringBuilder().append(ApiConstants.BASE_URL).append("/").append("getPassiveCandidatesPerformanceReport").toString();
		
		ResponseEntity<List<ActivePassiveTransactionReportPlatformDto>> response = restTemplate.exchange(url,HttpMethod.POST, new HttpEntity<ReportRequestDto>(reportRequest),new ParameterizedTypeReference<List<ActivePassiveTransactionReportPlatformDto>>() {
				});
		
		return convertToChar(response.getBody(),reportRequest,EnumCandidateStatus.PASSIVE);
	}
	


	@Override
	public TimeIntervalBarChart getRequisitionPerformanceRepot(ReportRequestDto reportRequest, String userId) {
		String url = new StringBuilder().append(ApiConstants.BASE_URL).append("/").append("getRequisitionPerformanceRepot").toString();
		
		ResponseEntity<List<RequistionPerformanceReportPlatformDto>> response = restTemplate.exchange(url,HttpMethod.POST, new HttpEntity<ReportRequestDto>(reportRequest),new ParameterizedTypeReference<List<RequistionPerformanceReportPlatformDto>>() {
				});
		
		return convertToReqChart(response.getBody(),reportRequest);
	}
	
	private TimeIntervalBarChart convertToReqChart(List<RequistionPerformanceReportPlatformDto> list,ReportRequestDto requestDto){
		if(list==null || list.isEmpty())
			return null;
		TimeIntervalBarChart chart =new TimeIntervalBarChart();
		chart.setReportType(requestDto.getEnumReportType());
		List<Integer> time=new ArrayList<Integer>(GRAPH_SIZE);
		List<Integer> red=new ArrayList<Integer>(GRAPH_SIZE);
		List<Integer> green=new ArrayList<Integer>(GRAPH_SIZE);
		for(RequistionPerformanceReportPlatformDto dto:list){
			time.add(dto.getAvgTimeToFill());
			red.add(dto.getNewRequistionCount());
			green.add(dto.getFilledCount());
		}
		chart.setTimeToFill(time);
		chart.setFirstLine(red);
		chart.setSecondLine(green);
		return chart;
	}

	@Override
	public List<TeamPerformanceByMeasure> getTeamMemberPerformanceByMeasureTimeToConvert(ReportRequestDto reportRequest, String userId) {
		String url = new StringBuilder().append(ApiConstants.BASE_URL).append("/").append("getTeamMemberPerformanceByMeasureTimeToConvert").toString();
		
		ResponseEntity<List<ActivePassiveTransactionReportPlatformDto>> response = restTemplate.exchange(url,HttpMethod.POST, new HttpEntity<ReportRequestDto>(reportRequest),new ParameterizedTypeReference<List<ActivePassiveTransactionReportPlatformDto>>() {
				});
		
		return convertToTeamMeasureChart(response.getBody(),reportRequest,EnumMemberPerformanceByMeasure.TIME_TO_CONVERT_PERFORMANCE);
	}
	
	/**
	 * 
	 * @param list
	 * @param reportRequest
	 * @return
	 */
	private List<TeamPerformanceByMeasure> convertToTeamMeasureChart(List<ActivePassiveTransactionReportPlatformDto> list,ReportRequestDto reportRequest,EnumMemberPerformanceByMeasure measureType){
		//Make sure rec list is not empty
		if(list==null || list.isEmpty()|| reportRequest.getRecruiterIds()==null ||reportRequest.getRecruiterIds().isEmpty())
			return null;
		List<TeamPerformanceByMeasure> performanceByMeasures=new ArrayList<TeamPerformanceByMeasure>();
		
		
		//Creating rec objects
		for(String recId:reportRequest.getRecruiterIds()){
			TeamPerformanceByMeasure performanceByMeasure=new TeamPerformanceByMeasure();
			//Basic settings
			performanceByMeasure.setEnumReportType(reportRequest.getEnumReportType());
			performanceByMeasure.setMemberId(recId);
			
			List<Integer> points=null;
			//Loop response and look for recruiter match
			for(ActivePassiveTransactionReportPlatformDto report:list){
				 points=new ArrayList<Integer>(GRAPH_SIZE);
				//Check if rec exist
				if(report.containsRecruiter(recId)){
					points.add(getMeasureType(measureType,report));
				}else{
					points.add(0);
				}
			}
			performanceByMeasure.setPoints(points);
			performanceByMeasures.add(performanceByMeasure);
		}
		return performanceByMeasures;
	}
	
	private int getMeasureType(EnumMemberPerformanceByMeasure measureType,ActivePassiveTransactionReportPlatformDto report){
		switch(measureType){
		case TIME_TO_CONVERT_PERFORMANCE:return report.getAvgTimeToConvert();
		case ACTIVE_PERFORMANCE:return report.getActiveCandidateCount();
		case PASSIVE_CONVERSION_PERFORMANCE: return report.getPassiveConversionCount();
		}
		return 0;
	}
	

	@Override
	public List<TeamPerformanceByMeasure> getTeamMemberPerformanceByMeasureActive(ReportRequestDto reportRequest, String userId) {
		String url = new StringBuilder().append(ApiConstants.BASE_URL).append("/").append("getTeamMemberPerformanceByMeasureActive").toString();
		
		ResponseEntity<List<ActivePassiveTransactionReportPlatformDto>> response = restTemplate.exchange(url,HttpMethod.POST, new HttpEntity<ReportRequestDto>(reportRequest),new ParameterizedTypeReference<List<ActivePassiveTransactionReportPlatformDto>>() {
				});
		
		return convertToTeamMeasureChart(response.getBody(),reportRequest,EnumMemberPerformanceByMeasure.ACTIVE_PERFORMANCE);
	}

	@Override
	public List<TeamPerformanceByMeasure> getTeamMemberPerformanceByMeasurePassive(ReportRequestDto reportRequest, String userId) {
		String url = new StringBuilder().append(ApiConstants.BASE_URL).append("/").append("getTeamMemberPerformanceByMeasurePassive").toString();
		
		ResponseEntity<List<ActivePassiveTransactionReportPlatformDto>> response = restTemplate.exchange(url,HttpMethod.POST, new HttpEntity<ReportRequestDto>(reportRequest),new ParameterizedTypeReference<List<ActivePassiveTransactionReportPlatformDto>>() {
				});
		
		return convertToTeamMeasureChart(response.getBody(),reportRequest,EnumMemberPerformanceByMeasure.PASSIVE_CONVERSION_PERFORMANCE);
	}

	@Override
	public List<TeamPerformanceByMember>  getTeamMemberPerformanceByTeamMember(ReportRequestDto reportRequest, String userId) {
		String url = new StringBuilder().append(ApiConstants.BASE_URL).append("/").append("getTeamMemberPerformanceByTeamMember").toString();
		
		ResponseEntity<List<ActivePassiveTransactionReportPlatformDto>> response = restTemplate.exchange(url,HttpMethod.POST, new HttpEntity<ReportRequestDto>(reportRequest),new ParameterizedTypeReference<List<ActivePassiveTransactionReportPlatformDto>>() {
				});
		
		return convertToTeamMemberChart(response.getBody(),reportRequest);
	}
	
	private List<TeamPerformanceByMember> convertToTeamMemberChart(List<ActivePassiveTransactionReportPlatformDto> list,ReportRequestDto reportRequest){
		//Make sure rec list is not empty
		if(list==null || list.isEmpty()|| reportRequest.getRecruiterIds()==null ||reportRequest.getRecruiterIds().isEmpty())
			return null;
		List<TeamPerformanceByMember> performanceByMembers=new ArrayList<TeamPerformanceByMember>();
		
		
		//Creating rec objects
		for(String recId:reportRequest.getRecruiterIds()){
			TeamPerformanceByMember performanceByMember=new TeamPerformanceByMember();
			//Basic settings
			performanceByMember.setReportType(reportRequest.getEnumReportType());
			performanceByMember.setMemberId(recId);
			
			
			List<Integer> red=null;
			List<Integer> green=null;
			List<Integer> timeToFill=null;
			//Loop response and look for recruiter match
			for(ActivePassiveTransactionReportPlatformDto report:list){
				 red=new ArrayList<Integer>(GRAPH_SIZE);
				 green=new ArrayList<Integer>(GRAPH_SIZE);
				 timeToFill=new ArrayList<Integer>(GRAPH_SIZE);
				//Check if rec exist
				if(report.containsRecruiter(recId)){
					red.add(report.getActiveCandidateCount());
					green.add(report.getPassiveConversionCount());
					timeToFill.add(report.getAvgTimeToConvert());
				}else{
					red.add(0);
					green.add(0);
					timeToFill.add(0);
				}
			}
			performanceByMember.setRed(red);
			performanceByMember.setGreen(green);
			performanceByMember.setTimeToFill(timeToFill);
			performanceByMembers.add(performanceByMember);
		}
		return performanceByMembers;
	}
	private List<TeamPerformanceByMember> createTeamMemberObject(List<String> ids){
		
		List<TeamPerformanceByMember> performanceByMembers=new ArrayList<TeamPerformanceByMember>();
		for(String id:ids){
			TeamPerformanceByMember byMember=new TeamPerformanceByMember();
			byMember.setMemberId(id);
			performanceByMembers.add(byMember);
		}
		return performanceByMembers;
	}

	@Override
	public List<SourcePerformanceObject> getSourcingPerformance(ReportRequestDto reportRequest,String userId) {
		String url = new StringBuilder().append(ApiConstants.BASE_URL).append("/").append("getSourcingPerformance").toString();
		
		ResponseEntity<SourcingPerformanceReportData> response = restTemplate.exchange(url,HttpMethod.POST, new HttpEntity<ReportRequestDto>(reportRequest),new ParameterizedTypeReference<SourcingPerformanceReportData>() {
				});
		
		return convertToSourcePerformance(response.getBody(),reportRequest);
	}
	
	private List<SourcePerformanceObject> convertToSourcePerformance(SourcingPerformanceReportData list,ReportRequestDto reportRequest){
		if(list==null || list.getSourcePerformances()==null)
			return null;
		List<SourcePerformanceObject> performanceObjects=new ArrayList<SourcePerformanceObject>();
		for(SourcePerformance data:list.getSourcePerformances()){
			SourcePerformanceObject performanceObject=new SourcePerformanceObject();
			performanceObject.setCompanyName(data.getSourceName());
			performanceObject.setSourcePerformanceGraph(convertToSourcePerformance(data.getSourcePerformanceBarChart()));
			
			performanceObjects.add(performanceObject);
		}
		return performanceObjects;
	}
	private SourcePerformanceGraph convertToSourcePerformance(List<SourcePerformanceBarChart> charDatas){
		  List<Integer> sourced=new ArrayList<Integer>(GRAPH_SIZE);
		  List<Integer> qualified=new ArrayList<Integer>(GRAPH_SIZE);
		  List<Integer> techAssessment=new ArrayList<Integer>(GRAPH_SIZE);
		  List<Integer> goldStandard=new ArrayList<Integer>(GRAPH_SIZE);
		  List<Integer> hired=new ArrayList<Integer>(GRAPH_SIZE);
		  for(SourcePerformanceBarChart charData:charDatas){
			  sourced.add(charData.getSourceCount());
			  qualified.add(charData.getQualifiedCount());
			  techAssessment.add(charData.getTechAssessmentCount());
			  goldStandard.add(charData.getGoldStandard());
			  hired.add(charData.getHiredCount());
		  }
		  SourcePerformanceGraph performanceGraph=new SourcePerformanceGraph();
		  performanceGraph.setGoldStandard(goldStandard);
		  performanceGraph.setHired(hired);
		  performanceGraph.setQualified(qualified);
		  performanceGraph.setSourced(sourced);
		  performanceGraph.setTechAssessment(techAssessment);
		  
		  return performanceGraph;
	}
	
	private List<SourcePerformanceObject> getTestData(ReportRequestDto reportRequest,String userId){
		SourcePerformanceGraph performanceGraph=new SourcePerformanceGraph();
		performanceGraph.setGoldStandard(java.util.Arrays.asList(new Integer[]{12,14,34,45,67,34}));
		performanceGraph.setHired(java.util.Arrays.asList(new Integer[]{12,14,34,45,67,34}));
		performanceGraph.setQualified(java.util.Arrays.asList(new Integer[]{12,14,34,45,67,34}));
		performanceGraph.setTechAssessment(java.util.Arrays.asList(new Integer[]{12,14,34,45,67,34}));
		performanceGraph.setSourced(java.util.Arrays.asList(new Integer[]{12,14,34,45,67,34}));
		
		SourcePerformanceData performanceData=new SourcePerformanceData();
		performanceData.setGoldCandidatesPercentage(12.33f);
		performanceData.setHiredCount(44);
		performanceData.setHiredPercentage(56.55f);
		performanceData.setSourcedCount(99);
		
		List<SourcePerformanceObject> performanceObjects=new ArrayList<SourcePerformanceObject>();
		//Initialize source object
		SourcePerformanceObject performanceObject1=new SourcePerformanceObject();
		performanceObject1.setCompanyName("Naukari");
		performanceObject1.setSourcePerformanceData(performanceData);
		performanceObject1.setSourcePerformanceGraph(performanceGraph);
		performanceObjects.add(performanceObject1);
		
		SourcePerformanceObject performanceObject2=new SourcePerformanceObject();
		performanceObject2.setCompanyName("4dot4");
		performanceObject2.setSourcePerformanceData(performanceData);
		performanceObject2.setSourcePerformanceGraph(performanceGraph);
		performanceObjects.add(performanceObject2);
		return performanceObjects;
	}
}

enum EnumMemberPerformanceByMeasure{
	TIME_TO_CONVERT_PERFORMANCE,ACTIVE_PERFORMANCE,PASSIVE_CONVERSION_PERFORMANCE
}
