package com.fourdotfive.missioncontrol.requisition;

import java.util.List;
import java.io.IOException;
import java.util.Map;

import com.fourdotfive.missioncontrol.dtos.candidate.JobBoardParameters;
import com.fourdotfive.missioncontrol.dtos.job.JobOpeningDto;
import com.fourdotfive.missioncontrol.dtos.job.JobResponseDto;
import com.fourdotfive.missioncontrol.dtos.job.NewJobDto;
import com.fourdotfive.missioncontrol.dtos.job.RequisitionActivityFilterDto;
import com.fourdotfive.missioncontrol.dtos.requisition.CountDto;
import com.fourdotfive.missioncontrol.dtos.requisition.RequisitionDetailsDTO;
import com.fourdotfive.missioncontrol.dtos.requisition.RequisitionDetailsResponseDTO;
import com.fourdotfive.missioncontrol.dtos.vendor.VendorMinDto;
import com.fourdotfive.missioncontrol.licensepreference.LicensePrefMarkerDto;
import com.fourdotfive.missioncontrol.pojo.candidate.CandidateCardDto;
import com.fourdotfive.missioncontrol.pojo.job.JobVendorDetailDTO;
import org.json.JSONObject;

import org.springframework.web.multipart.MultipartFile;

import com.fourdotfive.missioncontrol.dtos.company.CompanyMinInfoDto;
import com.fourdotfive.missioncontrol.dtos.requisition.ActivityDto;
import com.fourdotfive.missioncontrol.licensepreference.LicensePreferences;
import com.fourdotfive.missioncontrol.pojo.candidate.Candidate;
import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.job.Job;
import com.fourdotfive.missioncontrol.pojo.job.JobState;

public interface RequisitionApiExecutor {

	/**
	 * To Get client or bu Companies for a parent company, this method is
	 * called.
	 *
	 * @return response from platform
	 */
	List<Company> getClientOrBuList(String userId, String companyId);

	/**
	 * To Set LicensePreferences for the Job, this method is called.
	 *
	 */
	void setReqLicensePreferences(LicensePreferences licensePreferences,
								  String jobId);

	/**
	 * To upload RequistionDoc for the Job, this method is called.
	 *
	 * @return response from platform
	 */
	String uploadRequistionDoc(List<MultipartFile> reqFiles, String jobId,
							   String companyId, String buId, String sharedClientBuOrCorpId, boolean isSharedClientRequisition) throws IOException;

	/**
	 * To process the job, this method is called.
	 *
	 * @return response from platform
	 */
	String processJob(JSONObject fileInfo);

	/**
	 * To save and parse the job, this method is called.
	 *
	 * @return response from platform
	 */
	JobResponseDto saveAndParseJob(String userId, String hrxmlFileInfoJsonString);

	/**
	 * To Save Job, this method is called.
	 *
	 * @return response from platform
	 */
	Job saveJob(Job job);

	/**
	 * Save Job Openings.
	 *
	 * @return response from platform
	 */
	List<JobOpeningDto> saveJobOpenings(List<JobOpeningDto> jobOpeningDtos);

	/**
	 * To set the job status, this method is called.
	 *
	 * @return response from platform
	 */

	Job setJobStatus(JobState jobState);

	/**
	 * To get all candidates based on scoring, this method is called.
	 *
	 * @return response from platform
	 */

	List<String> runJobForScoring(String CompanyId, String JobId, String TransactionType, String TransactionId, String ReqTransactionId);

	/**
	 * To Get Job details, this method is called.
	 *
	 * @return response from platform
	 */
	Job getJob(String jobId);

	/**
	 * To Get Job Types, this method is called.
	 *
	 *
	 */
	String getJobTypes();

	/**
	 * To Get Sponsorship Types, this method is called.
	 *
	 */
	String getSponsorshipTypes();

	/**
	 * To get marker values for license preferences, this method is called.
	 *
	 */
	LicensePrefMarkerDto getLicensePreferenceMarkerValues(String clientOrBuId, String jobId);

	/**
	 * To Get all Jobs , this method is called.
	 *
	 * @return response from platform
	 */
	List<Job> getJobAll(String userId);

	/**
	 * To get Candidates based on jobId
	 *
	 * @param jobId
	 * @return
	 */
	List<Candidate> getCandidatesByRequisitionId(String id);

	/**
	 * To get Candidate count based on jobId, score and reqTransactionId
	 *
	 * @param jobId
	 * @param score
	 * @param reqTransactionId
	 * @return CountDto
	 */
	CountDto getJobCandidateCount(String jobId, Float score, String reqTransactionId);

	/**
	 *
	 * @param jobId
	 * @param requisitionTransactionId
	 * @param resumeTransactionId
	 * @param countType
	 * @param score
	 * @return
	 */
	List<CandidateCardDto> getCandidatesByRequisitionTransaction(String jobId, String companyId, String requisitionTransactionId, String resumeTransactionId, String countType, Double score, String userId);

	/**
	 * To get Candidate count based on jobId
	 *
	 * @param jobId
	 * @return
	 */
	List<Job> getrequisitionBasedOnClients(List<CompanyMinInfoDto> clients);

	List<ActivityDto> getRequisitionActivity(RequisitionActivityFilterDto requisitionActivityFilterDto);

	NewJobDto getRequisitionCard(String jobId, String companyId, String userId);

	JobBoardParameters getJobBoardParameterList();

	Map<String, List<String>> getRadiusAndRecencyOptionsList();

	JobResponseDto getJobById(String jobId);

	String getRequisitionById(String requisitionId);

	byte[] getJobRequisitionFile(String requisitionId);

    List<VendorMinDto> getVendorList(String companyId, String buId);

	List<VendorMinDto> getAssignedVendors(String jobId);

	List<JobVendorDetailDTO> getVendorsOfJob(String requisitionId);

    Company getClientByRequisitionId(String requisitionId);

	List<RequisitionDetailsResponseDTO> getRequisitionDetailsByClientOrBuId(RequisitionDetailsDTO requisitionDetailsDTO);

	String getSalaryDurationOptions();

	String getCurrencyCodes();
}
