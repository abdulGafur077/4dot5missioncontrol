package com.fourdotfive.missioncontrol.requisition;

import com.fourdotfive.missioncontrol.common.CommonUtil;
import com.fourdotfive.missioncontrol.dtos.candidate.JobBoardParameters;
import com.fourdotfive.missioncontrol.dtos.company.CompanyMinDto;
import com.fourdotfive.missioncontrol.dtos.company.CompanyMinInfoDto;
import com.fourdotfive.missioncontrol.dtos.job.JobDto;
import com.fourdotfive.missioncontrol.dtos.job.JobOpeningDto;
import com.fourdotfive.missioncontrol.dtos.job.JobResponseDto;
import com.fourdotfive.missioncontrol.dtos.job.NewJobDto;
import com.fourdotfive.missioncontrol.dtos.requisition.*;
import com.fourdotfive.missioncontrol.dtos.user.TeamMemberCollectionDTO;
import com.fourdotfive.missioncontrol.dtos.vendor.VendorMinDto;
import com.fourdotfive.missioncontrol.exception.FileUploadException;
import com.fourdotfive.missioncontrol.exception.PlatformException;
import com.fourdotfive.missioncontrol.licensepreference.LicensePrefDtoList;
import com.fourdotfive.missioncontrol.licensepreference.LicensePreferences;
import com.fourdotfive.missioncontrol.pojo.candidate.CandidateCardDto;
import com.fourdotfive.missioncontrol.pojo.job.Job;
import com.fourdotfive.missioncontrol.pojo.job.JobState;
import com.fourdotfive.missioncontrol.pojo.job.JobVendorDetailDTO;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author shalini, chakravarthy
 */
@RestController
@RequestMapping("/api/requisition/")
public class RequisitionController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RequisitionController.class);
    @Autowired
    private RequisitionService requisitionService;

    /**
     * To get Client or Bu for user, this method is called.
     *
     * @throws List <CompanyListDto> {{working with actual api}}
     */
    @RequestMapping(value = "/getclientorbulist/{userid}/{companyid}", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyMinDto>> getClientOrBuList(
            @PathVariable(value = "userid") String userId,
            @PathVariable(value = "companyid") String companyId,
            @RequestParam(value = "requisitionId", required = false, defaultValue = "") String requisitionId) {
        List<CompanyMinDto> response = requisitionService.getClientOrBuList(userId, companyId, requisitionId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     * To Upload req file, this method is called.
     *
     * @throws FileUploadException
     */
    @PreAuthorize("@featureAccessControlServiceImpl.isAddRequisitionFeatureAvailable(principal)")
    @RequestMapping(value = "/uploadrequisitiondoc", method = RequestMethod.POST)
    public ResponseEntity<JobResponseDto> uploadRequisitionDocument(
            @RequestParam("file") List<MultipartFile> files,
            @RequestParam("companyId") String companyId,
            @RequestParam("userId") String userId,
            @RequestParam(value = "buId", required = false, defaultValue = "") String buId,
            @RequestParam(value = "sharedClientBuOrCorpId", required = false, defaultValue = "") String sharedClientBuOrCorpId,
            @RequestParam(value = "isSharedClientRequisition", required = false, defaultValue = "false") Boolean isSharedClientRequisition)
            throws FileUploadException, IOException {
        boolean flag = CommonUtil.checkFileWhileCreatingReq(files);
        if (flag) {
            JobResponseDto response = requisitionService.uploadRequistionDoc(files, userId, companyId, buId,
                    sharedClientBuOrCorpId, isSharedClientRequisition);
            // process the job now
            return new ResponseEntity<>(response, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Set the job status
     *
     * @param jobState
     */
    @RequestMapping(value = "/setjobstatus", method = RequestMethod.POST)
    public ResponseEntity<Job> updateRequisitionStatus(
            @RequestBody JobState jobState) {

        Job response = requisitionService.setJobStatus(jobState);
        return new ResponseEntity<Job>(response, HttpStatus.OK);
        //return new ResponseEntity<String>(HttpStatus.OK);
    }

    /**
     * To get all candidates based on scoring, this method is called.
     */

    @PreAuthorize("@featureAccessControlServiceImpl.isMatchingFeatureAvailable(principal)")
    @RequestMapping(value = "/runjobforscoring/{companyId}/{jobId}/{transactionType}/{transactionId}/{reqTransactionId}", method = RequestMethod.POST)
    public ResponseEntity<List<String>> runJobForScoring(
            @PathVariable String companyId,
            @PathVariable String jobId,
            @PathVariable String transactionType,
            @PathVariable String transactionId,
            @PathVariable String reqTransactionId) {
        List<String> result = requisitionService.runJobForScoring(companyId, jobId, transactionType, transactionId, reqTransactionId);
        return new ResponseEntity<List<String>>(result, HttpStatus.OK);
    }

    /**
     * To save job openings with recruiter details on the requisition.
     */
    @RequestMapping(value = "/jobOpeningsAndRecruiter", method = RequestMethod.PUT)
    public ResponseEntity<JobDto> saveJobOpenings(
            //@PathVariable(value = "jobid") String jobId,
            @RequestBody JobDto jobDto) {
        List<JobOpeningDto> jobOpenings = jobDto.getJobOpenings();
        // save the job openings
        // List<JobOpeningDto> jobOpeningsResponse = requisitionService.saveJobOpenings(jobOpenings);
        // jobDto.setJobOpenings(jobOpeningsResponse);
        // JobDto response = requisitionService.saveJob(jobDto, "");
        JobDto response = new JobDto();
        return new ResponseEntity<JobDto>(response, HttpStatus.OK);
    }

    /**
     * To get Req for user, this method is called.
     *
     * @throws List <CompanyListDto>
     *              <p>
     *              {{TODO}}
     */
    @RequestMapping(value = "/getrequisitionsbasedonuser/{userid}/{companyid}", method = RequestMethod.GET)
    public ResponseEntity<List<JobDto>> getRequisitionsBasedONRecruiterId(
            @PathVariable(value = "userid") String userId,
            @PathVariable(value = "companyid") String companyId) {
        List<JobDto> response = requisitionService.getJobAll(userId);
        return new ResponseEntity<List<JobDto>>(response, HttpStatus.OK);

    }

    /**
     * To get Recruiter list, this method is called. {{working with actual api}}
     */
    @RequestMapping(value = "/getrecruiterlist/{userid}/{companyId}", method = RequestMethod.GET)
    public ResponseEntity<TeamMemberCollectionDTO> getRecruiterList(
            @PathVariable(value = "userid") String userId,
            @PathVariable(value = "companyId") String companyId,
            @RequestParam(value = "clientId", required = false, defaultValue = "") String clientId,
            @RequestParam(value = "requisitionId", required = false, defaultValue = "") String requisitionId) {
        LOGGER.info(getClass() + "getrecruiterlist");
        TeamMemberCollectionDTO teamMemberCollectionDTO = requisitionService.getRecruiterList(userId, companyId, clientId, requisitionId);
        return new ResponseEntity<>(teamMemberCollectionDTO, HttpStatus.OK);

    }

    /**
     * To get license Preferences, this method is called. {{working with hard
     * coded values}}
     */
    @RequestMapping(value = "/getlicensepreferences/{companyid}/{jobid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LicensePrefDtoList> getLicensePreferences(
            @PathVariable(value = "companyid") String companyId,
            @RequestParam(value = "clientorbuid", required = false, defaultValue = "") String clientOrBuId,
            @PathVariable(value = "jobid") String jobId) {

        LicensePrefDtoList response = requisitionService.getReqLicensePreferences(companyId, clientOrBuId, jobId);

        return new ResponseEntity<LicensePrefDtoList>(response, HttpStatus.OK);

    }

    /**
     * To set license Preferences for client for a job, this method is called.
     *
     * @return {{TODO}}
     */
    @RequestMapping(value = "/setlicensepreferences/{jobid}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> setLicensePreferences(
            @PathVariable(value = "jobid") String jobId,
            @RequestBody LicensePreferences licensePreferences) {

        //requisitionService.setLicensePreferences(licensePreferences, jobId);

        return new ResponseEntity<String>(HttpStatus.OK);
    }

    /**
     * To Save job, this method is called.
     *
     * @return {{TODO}}
     */
    @RequestMapping(value = "/savejob", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Job> saveJob(
            @RequestBody Job job) {
        Job jobResponse = requisitionService.saveJob(job, "");
        return new ResponseEntity<Job>(jobResponse, HttpStatus.OK);
    }

    /**
     * This function saves the requisition number, recruiter details, job Type and sponsorship details of a job.
     *
     * @return Job
     */
    @RequestMapping(value = "/saveRecruiterDetails", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Job> saveRecruiterDetails(@RequestBody Job job) {
        String jobId = job.getId();
        Job fullJob = requisitionService.getCompleteJob(jobId);
        LOGGER.info("Job get - " + fullJob.toString());
        // set the requisition number, recruiters, job type and sponsorships on the full job.
        fullJob.setRequisitionNumber(job.getRequisitionNumber());
        fullJob.setRecruiters(job.getRecruiters());
        fullJob.setJobType(job.getJobType());
        //fullJob.setSponsorships(job.getSponsorships());
        LOGGER.info("Job before save - " + fullJob.toString());
        // save the full job back to the platform
        return saveJob(fullJob);
    }

    /**
     * To Get Job details, this method is called.
     *
     * @return {{working with mock}}
     */
    @RequestMapping(value = "/getjob/{jobid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RequisitionDetailedDto> getMockJob(
            @PathVariable(value = "jobid") String jobId) {
        RequisitionDetailedDto response = requisitionService.getMockJob(jobId);
        return new ResponseEntity<RequisitionDetailedDto>(response,
                HttpStatus.OK);
    }

    @RequestMapping(value = "/findjob/{jobid}", method = RequestMethod.GET)
    public ResponseEntity<JobResponseDto> getJob(
            @PathVariable(value = "jobid") String jobId) {
        JobResponseDto response = requisitionService.getJobById(jobId);
        return new ResponseEntity<>(response,
                HttpStatus.OK);
    }

    @RequestMapping(value = "/getRequisitionCard/{jobId}/{companyId}", method = RequestMethod.GET)
    public ResponseEntity<NewJobDto> getRequisitionCard(@PathVariable(value = "jobId") String jobId,
                                                        @PathVariable(value = "companyId") String companyId) {
        NewJobDto requisitionCard = requisitionService.getRequisitionCard(jobId, companyId);
        return new ResponseEntity<NewJobDto>(requisitionCard, HttpStatus.OK);
    }

    @RequestMapping(value = "/getopenrequisitions", method = RequestMethod.GET)
    public ResponseEntity<OpenRequisitionsResponseDto> getOpenRequisitions(
            @RequestBody OpenRequisitionRequestDto dto) {

        return new ResponseEntity<OpenRequisitionsResponseDto>(HttpStatus.OK);
    }

    /**
     * To Get Job Types, this method is called.
     */
    @RequestMapping(value = "/jobtypes", method = RequestMethod.GET)
    public ResponseEntity<String> getJobTypes() {
        String response = requisitionService.getJobTypes();
        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    /**
     * To Get Sponsorship Types, this method is called.
     */
    @RequestMapping(value = "/sponsorshiptypes", method = RequestMethod.GET)
    public ResponseEntity<String> getSponsorshipTypes() {
        String response = requisitionService.getSponsorshipTypes();
        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    /**
     * To Upload resumes, this method is called.
     *
     * @throws FileUploadException {{working with mock}}
     */
    @RequestMapping(value = "/uploadresumedoc/{jobid}/{companyid}", method = RequestMethod.POST)
    public ResponseEntity<String> uploadResumeDocument(
            @RequestParam("resumes") List<MultipartFile> files,
            @PathVariable(value = "jobid") String jobId,
            @PathVariable(value = "companyid") String companyId)
            throws FileUploadException {
        boolean flag = CommonUtil.checkFileWhileCreatingReq(files);
        if (flag) {
            return new ResponseEntity<String>("123456789", HttpStatus.OK);
        } else {
            return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * To get all mocked reqs, this method is called. {{working with mock}}
     */
    @RequestMapping(value = "/getallrequisition", method = RequestMethod.GET)
    public ResponseEntity<List<RequisitionMinDto>> getAllRequisition() {
        List<RequisitionMinDto> response = requisitionService.findAllJobs();
        return new ResponseEntity<List<RequisitionMinDto>>(response,
                HttpStatus.OK);
    }

    /**
     * To get all req based on clients, this method is called. {{working with
     * mock}}
     */
    @RequestMapping(value = "/getrequisitionforclients", method = RequestMethod.GET)
    public ResponseEntity<List<RequisitionMinDto>> getRequisitionBasedOnClients(
            @RequestBody List<CompanyMinInfoDto> clients) {
        List<RequisitionMinDto> response = requisitionService
                .getRequisistionBasedOnClient(clients);
        return new ResponseEntity<List<RequisitionMinDto>>(response,
                HttpStatus.OK);
    }

    /**
     * To get all candidate count, this method is called. {{working with mock}}
     */
    @RequestMapping(value = "/getcandidatecountbyreqtransaction/{jobid}", method = RequestMethod.GET)
    public ResponseEntity<CountDto> getCandidateCountByRequisitionTransaction(
            @PathVariable(value = "jobid") String jobId,
            @RequestParam(value = "score") Float score,
            @RequestParam(value = "requisitionTransactionId") String reqTransactionId) {
        CountDto response = requisitionService.getJobCandidateCount(jobId, score, reqTransactionId);
        return new ResponseEntity<CountDto>(response, HttpStatus.OK);
    }

    /**
     * Get all candidates by requisition transaction
     */

    @RequestMapping(value = "/getcandidatesbyreqtransaction/{jobId}/{counttype}/{companyId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CandidateCardDto>> getCandidatesByRequisitionTransaction(@PathVariable String jobId,
                                                                                        @PathVariable("counttype") String countType,
                                                                                        @PathVariable("companyId") String companyId,
                                                                                        @RequestParam("score") Double score,
                                                                                        @RequestParam("requisitionTransactionId") String requisitionTransactionId,
                                                                                        @RequestParam(value = "resumeTransactionId", required = false) String resumeTransactionId) {
        List<CandidateCardDto> candidates = requisitionService.getCandidatesByRequisitionTransaction
                (jobId, companyId, requisitionTransactionId, resumeTransactionId, countType, score);
        return new ResponseEntity<>(candidates, HttpStatus.OK);
    }

    /**
     * To get activities of a particular requisition
     *
     * @param jobId
     * @return
     */
    @RequestMapping(value = "/requisitionActivity/{jobId}", method = RequestMethod.POST)
    public ResponseEntity getRequisitionActivityById(
            @PathVariable(value = "jobId") String jobId,
            @RequestParam(value = "searchText", required = false, defaultValue = "") String searchText,
            @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
            @RequestParam(value = "size", required = false, defaultValue = "5") Integer size,
            @RequestParam(value = "sort", required = false) String column,
            @RequestParam(value = "sortDir", required = false) String sortDirection,
            @RequestParam(value = "companyId") String companyId) {
        List<ActivityDto> response = requisitionService.getRequisitionActivity(jobId, searchText, page, size, column, sortDirection, companyId);
        return new ResponseEntity<List<ActivityDto>>(response, HttpStatus.OK);
    }

    @RequestMapping(value = {"/allrolesincompany/{companyid}"}, method = RequestMethod.GET)
    public ResponseEntity<List<String>> findallByCompany(
            @PathVariable String companyid) {
        List<String> rolesList = requisitionService.getAllRolesInACompany(companyid);
        if (rolesList == null)
            return new ResponseEntity<List<String>>(rolesList, HttpStatus.NOT_FOUND);
        return new ResponseEntity<List<String>>(rolesList, HttpStatus.OK);
    }

    @RequestMapping(value = "/getjobboardparameters", method = RequestMethod.GET)
    public ResponseEntity<JobBoardParameters> getJobBoardParameterList() {
        JobBoardParameters jobBoardParameters = requisitionService.getJobBoardParameterList();
        return new ResponseEntity<>(jobBoardParameters, HttpStatus.OK);
    }

    @RequestMapping(value = "/getradiusandrecencyoptions", method = RequestMethod.GET)
    public ResponseEntity<Map<String, List<String>>> getRadiusAndRecencyOptionsList() {
        Map<String, List<String>> parameterOptionsListMap = requisitionService.getRadiusAndRecencyOptionsList();
        return new ResponseEntity<>(parameterOptionsListMap, HttpStatus.OK);
    }

    @RequestMapping(value = "getrequisitionfile/{requisitionId}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getRequisitionFile(@PathVariable String requisitionId) throws IOException {
        String requisitionName = requisitionService.getRequisitionById(requisitionId);
        byte[] downloadInputStream = requisitionService.getJobRequisitionFile(requisitionId);
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + requisitionName)
                .body(downloadInputStream);
    }

    @RequestMapping(value = "/getvendorlist/{companyId}", method = RequestMethod.GET)
    public ResponseEntity<List<VendorMinDto>> getVendorList(@PathVariable(value = "companyId") String companyId,
                                                            @RequestParam(value = "buId", required = false, defaultValue = "") String buId) {

        List<VendorMinDto> response = requisitionService.getVendorList(companyId, buId);
        return new ResponseEntity<>(response, HttpStatus.OK);

    }

    @RequestMapping(value = "/getassignedvendors/{jobId}", method = RequestMethod.GET)
    public ResponseEntity<List<VendorMinDto>> getAssignedVendors(@PathVariable(value = "jobId") String jobId) {
        if (StringUtils.isBlank(jobId)) {
            throw new PlatformException("Job id can not be null.");
        }

        List<VendorMinDto> response = requisitionService.getAssignedVendors(jobId);
        return new ResponseEntity<>(response, HttpStatus.OK);

    }

    @RequestMapping(value = {"getvendorsofjob/{requisitionId}"}, method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<List<JobVendorDetailDTO>> getVendorsOfJob(@PathVariable("requisitionId") String requisitionId) {
        if (StringUtils.isBlank(requisitionId))
            throw new PlatformException("Job id can not be null.");

        List<JobVendorDetailDTO> jobVendorDetailDTOS = requisitionService.getVendorsOfJob(requisitionId);
        return new ResponseEntity<>(jobVendorDetailDTOS, HttpStatus.OK);
    }

    @RequestMapping(value = {"getrequisitiondetailsbyclientorbuid"}, method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<List<RequisitionDetailsResponseDTO>> getRequisitionDetailsByClientOrBuId(@RequestBody RequisitionDetailsRequestDTO requisitionDetailsRequestDTO) {

        List<RequisitionDetailsResponseDTO> requisitionDetailsDTO = requisitionService.getRequisitionDetailsByClientOrBuId(requisitionDetailsRequestDTO);
        return new ResponseEntity<>(requisitionDetailsDTO, HttpStatus.OK);
    }

    @RequestMapping(value = "/salarydurationoptions", method = RequestMethod.GET)
    public ResponseEntity<String> getSalaryDurationOptions() {
        String response = requisitionService.getSalaryDurationOptions();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/currencycodes", method = RequestMethod.GET)
    public ResponseEntity<String> getCurrencyCodes() {
        String response = requisitionService.getCurrencyCodes();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
