package com.fourdotfive.missioncontrol.requisition;

import java.util.List;

import com.fourdotfive.missioncontrol.dtos.job.MockJobProfileDto;
import com.fourdotfive.missioncontrol.dtos.requisition.CountDto;
import com.fourdotfive.missioncontrol.dtos.requisition.RequisitionDetailedDto;
import com.fourdotfive.missioncontrol.dtos.requisition.RequisitionMinDto;

public interface RequisitionMockApiExecutor {

	List<RequisitionMinDto> getAllReqs();

	/**
	 * To Get Job details, this method is called.
	 * 
	 * @return response from platform
	 */
	RequisitionDetailedDto getMockJob(String jobId);

	/**
	 * To Get Job details, this method is called.
	 * 
	 * @return response from platform
	 */
	MockJobProfileDto getMockJobProfile();

	CountDto getRequisitionCount(String reqId);
}
