package com.fourdotfive.missioncontrol.requisition;

import java.util.List;
import java.io.IOException;
import java.util.Map;

import com.fourdotfive.missioncontrol.dtos.candidate.JobBoardParameters;
import com.fourdotfive.missioncontrol.dtos.company.CompanyMinDto;
import com.fourdotfive.missioncontrol.dtos.job.JobOpeningDto;
import com.fourdotfive.missioncontrol.dtos.job.JobResponseDto;
import com.fourdotfive.missioncontrol.dtos.job.NewJobDto;
import com.fourdotfive.missioncontrol.dtos.requisition.*;
import com.fourdotfive.missioncontrol.dtos.user.TeamMemberCollectionDTO;
import com.fourdotfive.missioncontrol.dtos.vendor.VendorMinDto;
import com.fourdotfive.missioncontrol.pojo.candidate.CandidateCardDto;
import com.fourdotfive.missioncontrol.pojo.job.JobState;
import com.fourdotfive.missioncontrol.pojo.job.JobVendorDetailDTO;
import org.springframework.web.multipart.MultipartFile;
import org.json.JSONObject;

import com.fourdotfive.missioncontrol.dtos.company.CompanyMinInfoDto;
import com.fourdotfive.missioncontrol.dtos.job.JobDto;
import com.fourdotfive.missioncontrol.dtos.user.UserDto;
import com.fourdotfive.missioncontrol.licensepreference.LicensePrefDtoList;
import com.fourdotfive.missioncontrol.licensepreference.LicensePreferences;
import com.fourdotfive.missioncontrol.pojo.candidate.Candidate;
import com.fourdotfive.missioncontrol.pojo.job.Job;

public interface RequisitionService {

    /**
     * To Get client or bu Companies for a parent company, this method is
     * called.
     *
     * @return response from platform
     */
    List<CompanyMinDto> getClientOrBuList(String userId, String companyId, String requisitionId);

    /**
     * To Get Reportees, this method is called.
     *
     * @return response from platform
     */
    TeamMemberCollectionDTO getRecruiterList(String userId, String companyId, String clientId, String requisitionId);

    /**
     * To Set LicensePreferences for the Job, this method is called.
     */
    void setLicensePreferences(LicensePreferences licensePreferences,
                               String jobId);

    /**
     * To upload RequistionDoc for the Job, this method is called.
     *
     * @return
     */
    JobResponseDto uploadRequistionDoc(List<MultipartFile> reqFiles, String userId, String companyId, String buId,
                                       String sharedClientBuOrCorpId, boolean isSharedClientRequisition) throws IOException;

    /**
     * To process the job, this method is called.
     *
     * @return
     */
    String processJob(JSONObject fileInfo);

    /**
     * To set the job status, this method is called.
     *
     * @return JobDto
     */

    Job setJobStatus(JobState jobState);

    /**
     * To save and parse the job, this method is called.
     *
     * @return
     */
    JobResponseDto saveAndParseJob(String userId, String hrxmlFileInfoJsonString);

    /**
     * To save job openings.
     *
     * @return List<JobOpeningDto>
     */
    List<JobOpeningDto> saveJobOpenings(List<JobOpeningDto> jobOpeningDtos);

    /**
     * To get all candidates based on scoring, this method is called.
     */

    List<String> runJobForScoring(String CompanyId, String JobId, String TransactionType, String TransactionId, String ReqTransactionId);

    /**
     * To Get Job Types, this method is called.
     */
    String getJobTypes();

    /**
     * To Get Sponsorship Types, this method is called.
     */
    String getSponsorshipTypes();

    /**
     * To get UserDetails based on userID, this method is called.
     *
     * @return UserDto
     */
    UserDto getUserDetails(String userId);

    /**
     * To Save Job, this method is called.
     *
     * @return response from platform
     */
    Job saveJob(Job job, String companyId);

    /**
     * To Get Job details, this method is called.
     *
     * @return response from platform
     */
    JobDto getJob(String jobId);

    /**
     * To Get Complete Job details in the Job Object, this method is called.
     *
     * @return response from platform
     */
    Job getCompleteJob(String jobId);

    /**
     * To get Candidates based on jobId
     *
     * @param id
     * @return
     */
    List<Candidate> getCandidatesByRequisitionId(String id);


    /**
     * To Get all Jobs , this method is called.
     *
     * @return response from platform
     */
    List<JobDto> getJobAll(String userId);

    /**
     * To Get all Jobs , this method is called.
     *
     * @return response from platform
     */
    List<RequisitionMinDto> findAllJobs();

    /**
     * To Get Job details, this method is called.
     *
     * @return response from platform
     */
    RequisitionDetailedDto getMockJob(String jobId);

    /**
     * To Get all reqs, this method is called.
     *
     * @return response from platform
     */
    List<RequisitionMinDto> getRequisistionBasedOnClient(List<CompanyMinInfoDto> clients);

    /**
     * To Get Job details, this method is called.
     *
     * @return response from platform
     */
    CountDto getJobCandidateCount(String jobId, Float score, String reqTransactionId);

    /**
     * @param jobId
     * @param requisitionTransactionId
     * @param resumeTransactionId
     * @param countType
     * @param score
     * @return
     */
    List<CandidateCardDto> getCandidatesByRequisitionTransaction(String jobId, String companyId, String requisitionTransactionId, String resumeTransactionId, String countType, Double score);

    /**
     * To get LicensePreferences for the Job, this method is called.
     */
    LicensePrefDtoList getReqLicensePreferences(String companyId, String clientOrBuId, String jobId);

    List<ActivityDto> getRequisitionActivity(String jobId, String searchText, int page, int size, String column, String sortDirection, String companyId);

    List<String> getAllRolesInACompany(String companyId);

    NewJobDto getRequisitionCard(String jobId, String companyId);

    void setAutoMatchedLicencePreference(List<NewJobDto> jobs, String companyId);

    JobBoardParameters getJobBoardParameterList();

    Map<String, List<String>> getRadiusAndRecencyOptionsList();

    JobResponseDto getJobById(String jobId);

    String getRequisitionById(String requisitionId);

    byte[] getJobRequisitionFile(String requisitionId);

    List<VendorMinDto> getVendorList(String companyId, String buId);

    List<VendorMinDto> getAssignedVendors(String jobId);

    List<JobVendorDetailDTO> getVendorsOfJob(String requisitionId);

    List<RequisitionDetailsResponseDTO> getRequisitionDetailsByClientOrBuId(RequisitionDetailsRequestDTO requisitionDetailsRequestDTO);

    String getSalaryDurationOptions();

    String getCurrencyCodes();

}
