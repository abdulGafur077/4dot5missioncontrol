package com.fourdotfive.missioncontrol.requisition;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourdotfive.missioncontrol.candidate.CandidateService;
import com.fourdotfive.missioncontrol.common.AccessUtil;
import com.fourdotfive.missioncontrol.common.AppConstants;
import com.fourdotfive.missioncontrol.common.CommonUtil;
import com.fourdotfive.missioncontrol.company.CompanyService;
import com.fourdotfive.missioncontrol.dtos.candidate.CandidatesEnabledStepDto;
import com.fourdotfive.missioncontrol.dtos.candidate.JobBoardParameters;
import com.fourdotfive.missioncontrol.dtos.company.CompanyMinDto;
import com.fourdotfive.missioncontrol.dtos.company.CompanyMinInfoDto;
import com.fourdotfive.missioncontrol.dtos.job.*;
import com.fourdotfive.missioncontrol.dtos.requisition.*;
import com.fourdotfive.missioncontrol.dtos.user.TeamMemberCollectionDTO;
import com.fourdotfive.missioncontrol.dtos.user.TeamMemberDto;
import com.fourdotfive.missioncontrol.dtos.user.UserDto;
import com.fourdotfive.missioncontrol.dtos.vendor.VendorMinDto;
import com.fourdotfive.missioncontrol.dtos.workflowstep.WorkflowStepRequestDTO;
import com.fourdotfive.missioncontrol.job.JobService;
import com.fourdotfive.missioncontrol.licensepreference.*;
import com.fourdotfive.missioncontrol.platformexcecutor.PlatformApiExecutorImp;
import com.fourdotfive.missioncontrol.pojo.FileInfo;
import com.fourdotfive.missioncontrol.pojo.candidate.ActiveCandidateJobStepType;
import com.fourdotfive.missioncontrol.pojo.candidate.Candidate;
import com.fourdotfive.missioncontrol.pojo.candidate.CandidateCardDto;
import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.company.CompanyState;
import com.fourdotfive.missioncontrol.pojo.company.CompanyType;
import com.fourdotfive.missioncontrol.pojo.job.*;
import com.fourdotfive.missioncontrol.pojo.user.AccessControlList;
import com.fourdotfive.missioncontrol.pojo.user.Contact;
import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.user.UserService;
import com.fourdotfive.missioncontrol.userentityscope.UserScopeOfManagerService;
import com.fourdotfive.missioncontrol.workflowstep.WorkflowStepService;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

import static com.fourdotfive.missioncontrol.dtos.company.CompanyMinDto.nameComparator;
import static com.fourdotfive.missioncontrol.pojo.user.User.userNameComparator;

@Service
public class RequisitionServiceImp implements RequisitionService {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(RequisitionServiceImp.class);

    @Autowired
    private CompanyService companyService;

    @Autowired
    private UserService userService;

    @Autowired
    private LicensePrefService licensePrefService;

    @Autowired
    private RequisitionApiExecutor reqApiExecutor;

    @Autowired
    private PlatformApiExecutorImp platformApiExecutorImp;

    @Autowired
    private RequisitionMockApiExecutor reqMockApiExecutor;

    @Autowired
    private UserScopeOfManagerService managerService;

    @Autowired
    private RequisitionService requisitionService;

    @Autowired
    private AccessUtil accessUtil;

    @Autowired
    private CandidateService candidateService;

    @Autowired
    private WorkflowStepService workflowStepService;

    @Autowired
    private JobService jobService;

    @Override
    public List<CompanyMinDto> getClientOrBuList(String userId, String companyId, String requisitionId) {
        List<CompanyMinDto> companyMinDtos = new ArrayList<>();
        List<Company> clientCompanies = new ArrayList<>();
        User user = userService.getUserById(userId);
        boolean hasPermissibilityToAllClient = false;
        if (accessUtil.isSuperUser(user.getRole().getId())
                || accessUtil.isFourDot5Admin(user.getRole())
                || accessUtil.isCompanyAdmin(user.getRole())) {
            hasPermissibilityToAllClient = true;
        } else {
            AccessControlList accessControlList = userService.getEnityScope(userId);
            if (accessControlList.isAllclientOrBU()) {
                hasPermissibilityToAllClient = true;
            } else {
                clientCompanies = user.getAccessControlList().getClientOrBUList();
            }
        }

        if (hasPermissibilityToAllClient) {
            clientCompanies = companyService.getClientOrBUs(companyId);
        } else if (StringUtils.isNotEmpty(requisitionId)) {
            Company client = reqApiExecutor.getClientByRequisitionId(requisitionId);
            if (client != null && !clientCompanies.contains(client)) {
                clientCompanies.add(client);
            }
        }
        if (!CollectionUtils.isEmpty(clientCompanies)) {
            for (Company company : clientCompanies) {
                if (company != null && !(StringUtils.equals(company.getCompanyState(), String.valueOf(CompanyState.Draft))
                        || StringUtils.equals(company.getCompanyState(), String.valueOf(CompanyState.Archived)))) {
                    CompanyMinDto companyMinDto = CommonUtil.mapCompanytoCompanyMinDto(company);
                    companyMinDtos.add(companyMinDto);
                }
            }
        }
        if (companyMinDtos.size() > 1) {
            Collections.sort(companyMinDtos, nameComparator);
        }
        return companyMinDtos;
    }

    @Override
    public TeamMemberCollectionDTO getRecruiterList(String userId, String companyId, String clientId, String requisitionId) {
        User user = userService.getUserById(userId);
        List<User> users = new ArrayList<>();
        List<User> usersList = new ArrayList<>();
        Set<User> finalUsersList = new HashSet<>();
        List<TeamMemberDto> teamMembers;
        if (accessUtil.isSuperUser(user.getRole().getId()) || accessUtil.isFourDot5Admin(user.getRole())) {
            teamMembers = userService.getRecruitersList(userId, companyId);
            LOGGER.info(getClass() + " teamMembers " + teamMembers.size());
            for (TeamMemberDto teamMember : teamMembers) {
                if (teamMember.getUserState().equals(AppConstants.ACTIVE)) {
                    String tempUserId = teamMember.getUserId();
                    User tempUser = userService.getUserById(tempUserId);
                    usersList.add(tempUser);
                    List<User> tempUsers = userService.getAllReporteeUnder(tempUserId, tempUser.getRole());
                    if (tempUsers != null) {
                        for (User temp : tempUsers) {
                            if (temp.getAccessControlList().getAccountState().equals(AppConstants.ACTIVE)) {
                                usersList.add(temp);
                            }
                        }
                    }
                }
            }

            if (StringUtils.isNotBlank(requisitionId)) {
                List<String> userIds = new ArrayList<>();
                for (User recruiterOrHiringManager : usersList) {
                    userIds.add(recruiterOrHiringManager.getId());
                }

                List<User> hiringManagers = userService.getAllRecruitersOrHiringManagers(userId, companyId, userIds);
                if (!CollectionUtils.isEmpty(hiringManagers)) {
                    usersList.addAll(hiringManagers);
                }
            }

        } else {
            users = userService.getAllReporteeUnder(userId, user.getRole());
            for (User tempUser : users) {
                if (tempUser.getAccessControlList().getAccountState().equals(AppConstants.ACTIVE)) {
                    usersList.add(tempUser);
                }
            }

            User manager = userService.getManager(userId);
            if (manager != null) {
                usersList.add(manager);
            }

            List<String> userIds = new ArrayList<>();
            for (User recruiter : usersList) {
                if (recruiter == null)
                    continue;

                userIds.add(recruiter.getId());
            }

            if (StringUtils.isNotBlank(requisitionId)) {
                List<User> recruitersOrHiringManagers = userService.getAllRecruitersOrHiringManagers(userId, companyId, userIds);
                if (!CollectionUtils.isEmpty(recruitersOrHiringManagers)) {
                    usersList.addAll(recruitersOrHiringManagers);
                }
            }
        }

        Company company = companyService.getCompany(companyId);
        if (StringUtils.equals(company.getCompanyType().name(), CompanyType.Corporation.name())
                || StringUtils.equals(company.getCompanyType().name(), CompanyType.StaffingCompany.name())) {
            if (StringUtils.isNotEmpty(clientId)) {
                for (User currentUser : usersList) {
                    if (StringUtils.equals(AppConstants.SUPERUSER_ROLEID, currentUser.getRole().getId())
                            || StringUtils.equals(AppConstants.FOURDOT5ADMIN_ROLEID, currentUser.getRole().getId())
                            || StringUtils.equals(AppConstants.STAFFING_ADMIN_ROLEID, currentUser.getRole().getId())
                            || StringUtils.equals(AppConstants.CORPORATE_ADMIN_ROLEID, currentUser.getRole().getId())) {
                        finalUsersList.add(currentUser);
                    } else {
                        boolean hasScopeToClientOrBu = userService.hasAccessToClientOrBu(currentUser.getId(), clientId);
                        if (hasScopeToClientOrBu) {
                            finalUsersList.add(currentUser);
                        }
                    }
                }
            } else if (StringUtils.isNotBlank(requisitionId)) {
                List<User> permissibleUsers = userService.getAllPermissibleUsers(usersList, companyId);
                finalUsersList.addAll(permissibleUsers);
            } else {
                finalUsersList.addAll(usersList);
            }
        }

        List<User> clientHiringManagerList = new ArrayList<>();
        if (StringUtils.isNotEmpty(clientId)) {
            Company client = companyService.getCompany(clientId);
            if (client != null && client.getCompanyType().equals(CompanyType.Client)) {
                List<Contact> contactList = client.getContactList();
                if (contactList != null && !contactList.isEmpty()) {
                    for (Contact contact : contactList) {
                        if (contact.getIsHiringManager() && contact.getUserId() != null) {
                            User hiringManager = userService.getUserById(contact.getUserId());
                            if (hiringManager != null) {
                                clientHiringManagerList.add(hiringManager);
                            }
                        }
                    }
                }
            }
        }

        if (!clientHiringManagerList.isEmpty())
            finalUsersList.addAll(clientHiringManagerList);

        if (StringUtils.isNotBlank(requisitionId)) {
            Set<String> permissibleUserIds = new HashSet<>();
            for (User permissibleUser : finalUsersList) {
                permissibleUserIds.add(permissibleUser.getId());
            }

            JobResponseDto jobResponseDto = requisitionService.getJobById(requisitionId);
            addCurrentUserAndManagerAndAssignedRecruitersAndAssignedHiringManagers(user, finalUsersList, permissibleUserIds, jobResponseDto);
        }

        TeamMemberCollectionDTO teamMemberCollectionDTO = new TeamMemberCollectionDTO();
        if (!CollectionUtils.isEmpty(finalUsersList)) {
            List<User> hiringManagers = new ArrayList<>();
            List<User> recruiters = new ArrayList<>();
            List<String> hiringManagerIds = new ArrayList<>();
            for (User teamMember : finalUsersList) {
                if (teamMember == null || teamMember.getRole() == null)
                    continue;

                if (StringUtils.equals(teamMember.getRole().getId(), AppConstants.HIRING_MANAGER_ROLE_ID)
                        || StringUtils.equals(teamMember.getRole().getId(), AppConstants.CLIENT_EMPLOYEE_ROLEID)) {
                    if (hiringManagerIds.contains(teamMember.getId()))
                        continue;

                    hiringManagers.add(teamMember);
                    hiringManagerIds.add(teamMember.getId());
                } else {
                    recruiters.add(teamMember);
                }
            }

            if (!CollectionUtils.isEmpty(hiringManagers)) {
                Collections.sort(hiringManagers, userNameComparator);
                teamMemberCollectionDTO.setHiringManagers(TeamMemberDto.mapUsersToTeamMembers(hiringManagers));
            }
            if (!CollectionUtils.isEmpty(recruiters)) {
                Collections.sort(recruiters, userNameComparator);
                teamMemberCollectionDTO.setRecruiters(TeamMemberDto.mapUsersToTeamMembers(recruiters));
            }
        }
        return teamMemberCollectionDTO;
    }

    private void addCurrentUserAndManagerAndAssignedRecruitersAndAssignedHiringManagers(User user, Set<User> users, Set<String> userIds, JobResponseDto jobResponseDto) {

        if (!accessUtil.isSuperUser(user.getRole().getId()) && !accessUtil.isFourDot5Admin(user.getRole())) {
            User manager = userService.getManager(user.getId());
            if (!userIds.contains(user.getId())) {
                userIds.add(user.getId());
                users.add(user);
            }

            if (manager != null && !userIds.contains(manager.getId())) {
                userIds.add(manager.getId());
                users.add(manager);
            }
        }

        if (jobResponseDto != null) {
            List<User> assignedRecruiters = jobResponseDto.getRecruiters();
            for (User recruiter : assignedRecruiters) {
                if (recruiter == null)
                    continue;

                if (!userIds.contains(recruiter.getId()))
                    users.add(recruiter);
            }

            Set<User> assignedHiringManagers = getAssignedHiringManagers(jobResponseDto);
            for (User hiringManager : assignedHiringManagers) {
                if (hiringManager == null)
                    continue;

                if (!userIds.contains(hiringManager.getId()))
                    users.add(hiringManager);
            }
        }
    }

    private Set<User> getAssignedHiringManagers(JobResponseDto jobResponseDto) {
        Set<User> hiringManagers = new HashSet<>();
        if (jobResponseDto == null)
            return hiringManagers;

        for (HiringManagerInfo hiringManagerInfo : jobResponseDto.getHiringManagers()) {
            hiringManagers.add(hiringManagerInfo.getUser());
        }

        return hiringManagers;
    }

    @Override
    public void setLicensePreferences(LicensePreferences licensePreferences,
                                      String jobId) {

        platformApiExecutorImp.setReqLicensePreferences(licensePreferences,
                jobId);

    }

    @Override
    public JobResponseDto uploadRequistionDoc(List<MultipartFile> reqFiles, String userId, String companyId, String buId,
                                              String sharedClientBuOrCorpId, boolean isSharedClientRequisition) throws IOException {
        String docUploadResponse = platformApiExecutorImp.uploadRequistionDoc(reqFiles, userId, companyId, buId,
                sharedClientBuOrCorpId, isSharedClientRequisition);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        List<FileInfo> fileInfoList = objectMapper.readValue(docUploadResponse, new TypeReference<List<FileInfo>>() {
        });

        JSONObject jsonFileInfo = new JSONObject();
        jsonFileInfo.put("inputFileInfos", fileInfoList);

        // Call the process jobs API
        String processJobResponse;
        processJobResponse = processJob(jsonFileInfo);
        // hard coding the process job response to prevent sovren calls.
        // processJobResponse = "{\"transactionID\":\"5a1e0076d4c61e7286865511\",\"files\":[{\"hrXmlFileName\":\"5835c248e4b02bdd60a9bbe5-1511915641140-UI_Developer.xml\",\"hrXmlFileLocation\":\"58b72f13e4b0c7dcd9b73874\",\"rawFileName\":\"5835c248e4b02bdd60a9bbe5-1511915636390-UI_Developer.docx\",\"rawFileLocation\":\"58b72f13e4b0c7dcd9b73874\",\"subDir\":\"58b73c2fe4b0c7dcd9b73876\",\"resumeId\":null,\"isSuccess\":true,\"errorCode\":null}],\"successCount\":1,\"failureCount\":0}";

        // Call the save and parse jobs API
        JobResponseDto saveAndParseJobResponse;
        saveAndParseJobResponse = saveAndParseJob(userId, processJobResponse);

        return saveAndParseJobResponse;
    }

    @Override
    public String processJob(JSONObject fileInfo) {
        //System.out.println("In Req service process job - " + fileInfo.toString());
        String processJobResponse = platformApiExecutorImp.processJob(fileInfo);
        //System.out.println("processJobResponse ---------------------- "+processJobResponse);
        return processJobResponse;
    }


    @Override
    public JobResponseDto saveAndParseJob(String userId, String hrxmlFileInfoJsonString) {
        //System.out.println("In Req service save and parse job - " + hrxmlFileInfoJsonString);
        JobResponseDto saveAndParseJobResponse = platformApiExecutorImp.saveAndParseJob(userId, hrxmlFileInfoJsonString);
        //System.out.println("saveAndParseJobResponse ---------------------- "+saveAndParseJobResponse);
        return saveAndParseJobResponse;
    }

    @Override
    public List<JobOpeningDto> saveJobOpenings(List<JobOpeningDto> jobOpeningDtos) {
        //System.out.println(" In req save job openings - " + jobOpeningDtos.toString());
        List<JobOpeningDto> saveJobOpeningDtosResponse = platformApiExecutorImp.saveJobOpenings(jobOpeningDtos);
        //System.out.println( " saveJobOpeningDtosResponse ---------------------- "+ saveJobOpeningDtosResponse.toString());
        return saveJobOpeningDtosResponse;
    }

    @Override
    public Job setJobStatus(JobState jobState) {
        String loggedInUserId = userService.getCurrentLoggedInUser().getId();
        jobState.setRecruiterId(loggedInUserId);
        return platformApiExecutorImp.setJobStatus(jobState);
    }

    @Override
    public List<String> runJobForScoring(String companyId, String jobId, String transactionType, String transactionId, String reqTransactionId) {
        return platformApiExecutorImp.runJobForScoring(companyId, jobId, transactionType, transactionId, reqTransactionId);
    }


    @Override
    public UserDto getUserDetails(String userId) {

        UserDto dto = userService.getUserDtoById(userId);
        return dto;

    }

    @Override
    public String getJobTypes() {
        System.out.println("in mission control req service impl");
        String response = platformApiExecutorImp.getJobTypes();
        return response;
    }

    @Override
    public String getSponsorshipTypes() {
        String response = platformApiExecutorImp.getSponsorshipTypes();
        return response;
    }

    @Override
    public Job saveJob(Job job, String companyId) {

        //Job job = mapJobOpeningstoJobDTO(jobDto.getId(), jobDto);
        //System.out.println("Get Job Response - " + job.toString());
        Job savedJob = platformApiExecutorImp.saveJob(job);
        LOGGER.debug("Saved Job Response {} ", savedJob);
        //return mapJobToJobDto(savedJob);
        return savedJob;
    }

    private JobDto mapJobOpeningstoJobDTO(String jobId, JobDto jobDto) {

        //Job job = getCompleteJob(jobId);
        JobDto job = getJob(jobId);
        if (job == null) {
            job = new JobDto();
        }
        if (job.getJobOpenings() != null && job.getJobOpenings().size() > 0) {
            List<JobOpeningDto> jobOpenings = job.getJobOpenings();

            jobOpenings = compareJobOpenings(jobOpenings, jobDto.getJobOpenings());
            job.getJobOpenings().addAll(jobOpenings);
        } else {
            List<JobOpeningDto> jobOpenings = new ArrayList<>();
            for (JobOpeningDto jobOpeningDto : jobDto.getJobOpenings()) {
                JobOpeningDto jobOpening = getJobOpenings(jobOpeningDto);
                jobOpenings.add(jobOpening);
            }
            job.getJobOpenings().addAll(jobOpenings);

        }
        return job;

    }

    private List<JobOpeningDto> compareJobOpenings(List<JobOpeningDto> jobOpenings,
                                                   List<JobOpeningDto> jobOpeningDtos) {
        if (!jobOpenings.isEmpty() && !jobOpeningDtos.isEmpty()) {
            for (JobOpeningDto jobOpening : jobOpenings) {
                for (Iterator<JobOpeningDto> iterator = jobOpeningDtos
                        .iterator(); iterator.hasNext(); ) {
                    JobOpeningDto obj = iterator.next();
                    for (TeamMemberDto recruiter : obj.getAssignedRecruiters()) {
                        if (jobOpening.getAssignedRecruiters().contains(recruiter) && obj.getOpeningNumber().equals(jobOpening.getOpeningNumber())) {
                            iterator.remove();
                        }
                    }
                }
            }
        }

        List<JobOpeningDto> openings = new ArrayList<JobOpeningDto>();

        for (JobOpeningDto jobOpeningDto : jobOpeningDtos) {
            JobOpeningDto jobOpening = getJobOpenings(jobOpeningDto);
            openings.add(jobOpening);
        }
        return openings;

    }

    private JobOpeningDto getJobOpenings(JobOpeningDto jobOpeningDto) {
        JobOpeningDto jobOpening = new JobOpeningDto();
        List<TeamMemberDto> recruiters = new ArrayList<>();
        //System.out.println("Job Opening recruiters - " + jobOpeningDto.getAssignedRecruiters());
        if (jobOpeningDto.getAssignedRecruiters() != null && jobOpeningDto.getAssignedRecruiters().size() > 0) {
            for (TeamMemberDto teamMember : jobOpeningDto.getAssignedRecruiters()) {
                //User tempUser = userService.getUserById(teamMember.getUserId());
                recruiters.add(teamMember);
            }
        }
        jobOpening.setAssignedRecruiters(recruiters);
        jobOpening.setOpeningNumber(jobOpeningDto.getOpeningNumber());
        jobOpening.setRequisitionOpeningNumber(jobOpeningDto
                .getRequisitionOpeningNumber());
        return jobOpening;
    }

    public Job getCompleteJob(String jobId) {
        return reqApiExecutor.getJob(jobId);
    }

    public List<OpenRequisitionsResponseDto> getOpenRequisitions(
            OpenRequisitionRequestDto dto, String userId) {

        // get date range

        // Call API

        List<Job> jobList = reqApiExecutor.getJobAll(userId);
        for (Job job : jobList) {
            // TODO - get the latest Job requisition
            JobRequisition req = job.getJobRequisitionList().get(0);
            List<Candidate> candidateList = getCandidatesByRequisitionId(req
                    .getId());
            int activeCandidates = 0;
            int passiveCandidates = 0;
            for (Candidate candidate : candidateList) {
                // TODO how do i know which job match to get
                List<JobStateStep> jobStateStepList = candidate
                        .getJobMatchList().get(0).getJobStateSteps();
                // TODO get latest jobStateStep
                JobStateStep jobStateStep = jobStateStepList.get(0);
                List<ActiveCandidateJobStepType> list = Arrays
                        .asList(ActiveCandidateJobStepType.values());
                if (list.contains(jobStateStep)) {
                    activeCandidates++;
                } else {
                    passiveCandidates++;
                }

                /*
                 * OpenRequisitionsResponseDto responseDto = new
                 * OpenRequisitionsResponseDto(req.getId(), job.getTitle(),
                 * companyName, state, activeCandidateCount,
                 * passiveCandidateCount, daysToSource, levelOfDaysToSource,
                 * daysToFill, levelOfDaysToFill);
                 */
            }
            /*
             * sortCandidates(candidateList); int passiveCount =
             * getCandidatesByRequisitionId(req.getId(),
             * CandidateState.PASSIVE).size();
             */
        }
        // create dto

        return null;
    }

    @Override
    public List<Candidate> getCandidatesByRequisitionId(String id) {

        return reqApiExecutor.getCandidatesByRequisitionId(id);
    }

    public void updateJobRequisition(String requisitionNumber,
                                     RequisitionState requisitionStatus) {

    }

    @Override
    public JobDto getJob(String jobId) {
        Job job = getCompleteJob(jobId);
        JobDto dto = mapJobToJobDto(job);
        return dto;
    }

    private JobDto mapJobToJobDto(Job job) {
        JobDto jobDto = new JobDto();
        jobDto.setId(job.getId());
        jobDto.setJobOpenings(CommonUtil.getJobOpeningsDto(job.getJobOpenings()));
        jobDto.setNumberOfOpenings(job.getNumberOfOpenings());
        //jobDto.setClientorbu(CommonUtil.mapCompanytoCompanyMinDto(job.getCompany()));
        return jobDto;
    }

    @Override
    public List<JobDto> getJobAll(String userId) {
        List<Job> jobs = reqApiExecutor.getJobAll(userId);

        List<JobDto> jobDtos = new ArrayList<JobDto>();
        for (Job job : jobs) {
            JobDto jobDto = mapJobToJobDto(job);
            jobDtos.add(jobDto);
        }

        return jobDtos;
    }

    @Override
    public List<RequisitionMinDto> findAllJobs() {
        return reqMockApiExecutor.getAllReqs();
    }

    @Override
    public RequisitionDetailedDto getMockJob(String jobId) {
        return reqMockApiExecutor.getMockJob(jobId);
    }

    @Override
    public List<RequisitionMinDto> getRequisistionBasedOnClient(
            List<CompanyMinInfoDto> clients) {
        return reqMockApiExecutor.getAllReqs();
    }

    @Override
    public CountDto getJobCandidateCount(String jobId, Float score, String reqTransactionId) {
        return platformApiExecutorImp.getJobCandidateCount(jobId, score, reqTransactionId);
        //return reqMockApiExecutor.getRequisitionCount(jobId);
    }

    @Override
    public List<CandidateCardDto> getCandidatesByRequisitionTransaction(String jobId,String companyId, String requisitionTransactionId, String resumeTransactionId, String countType, Double score) {
        User user = userService.getCurrentLoggedInUser();
        String userId = user.getId();
        return platformApiExecutorImp.getCandidatesByRequisitionTransaction(jobId, companyId,  requisitionTransactionId, resumeTransactionId, countType, score, userId);
    }

    @Override
    public LicensePrefDtoList getReqLicensePreferences(String companyId, String clientOrBuId, String jobId) {
        //Company company = companyService.getCompany(companyId);
        //LicensePrefDtoList licensePreferences = toLicensePrefDto.convert(company.getLicensePreferences());
        LicensePrefDtoList licensePrefDtoList = licensePrefService.getClientOrBuLicensePref(companyId, clientOrBuId);
        LicensePrefDtoList licensePreferences = setCandidatesPresentOnStep(companyId, clientOrBuId, jobId, licensePrefDtoList);
        LicensePrefMarkerDto licensePrefMarkerDto = platformApiExecutorImp.getLicensePreferenceMarkerValues(clientOrBuId, jobId);
        // set 4dtofive intelligence marker value
        List<LicensePrefDetails> licensePrefDetailsList = licensePreferences.getFourDotFiveIntellList().getLicensePrefDetailsList();
        for (LicensePrefDetails tempLicense : licensePrefDetailsList) {
            tempLicense.setLicensePrefMarkerValue(licensePrefMarkerDto.getFourDotFiveIntelligenceMarkerValue());
            tempLicense.setMarkerCompanyName(licensePrefMarkerDto.getMarkerValueOfCompnany());
        }
        // set tech assessment marker value
        licensePrefDetailsList = licensePreferences.getTechAssessmentList().getLicensePrefDetailsList();
        for (LicensePrefDetails tempLicense : licensePrefDetailsList) {
            tempLicense.setLicensePrefMarkerValue(licensePrefMarkerDto.getTechAssessmentMarkerValue());
            tempLicense.setMarkerCompanyName(licensePrefMarkerDto.getMarkerValueOfCompnany());
        }
        Company company = companyService.getCompany(companyId);
        if (company.getCompanyType() == CompanyType.StaffingCompany || company.getCompanyType() == CompanyType.Corporation) {
            licensePrefService.evaluateWithPlan(companyId, licensePreferences);
        }
        Job job = jobService.findJobById(jobId);
        WorkflowStepRequestDTO workflowStepRequestDTO = new WorkflowStepRequestDTO();
        if (job.getJobType() != null)
            workflowStepRequestDTO.setJobType(job.getJobType().getName());

        workflowStepRequestDTO.setCompanyId(companyId);
        workflowStepRequestDTO.setRequisitionId(jobId);
        Map<String, List<String>> workflowSteps = workflowStepService.getWorkflowStepsByRequisitionId(workflowStepRequestDTO);

        licensePreferences.setWorkflowStepsOrder(workflowSteps);
        return licensePreferences;
    }

    @Override
    public List<ActivityDto> getRequisitionActivity(String jobId, String searchText, int page, int size, String column, String sortDirection, String companyId) {
        User user = userService.getCurrentLoggedInUser();
        RequisitionActivityFilterDto requisitionActivityFilterDto = new RequisitionActivityFilterDto(jobId, searchText,
                page, size, column, sortDirection, companyId, user.getId());
        return reqApiExecutor.getRequisitionActivity(requisitionActivityFilterDto);
    }

    @Override
    public List<String> getAllRolesInACompany(String jobId) {
        return platformApiExecutorImp.getAllRolesInACompany(jobId);

    }

    @Override
    public NewJobDto getRequisitionCard(String jobId, String companyId) {
        User user = userService.getCurrentLoggedInUser();
        NewJobDto jobDto = reqApiExecutor.getRequisitionCard(jobId, companyId, user.getId());
        List<NewJobDto> jobs = new ArrayList<>();
        jobs.add(jobDto);
        setAutoMatchedLicencePreference(jobs, companyId);
        return jobs.get(0);
    }

    @Override
    public void setAutoMatchedLicencePreference(List<NewJobDto> jobs, String companyId) {
        LicensePrefDtoList licensePrefDtoList = licensePrefService.getStaffOrCorpLicensePref(companyId);
        boolean isAutoMatedMatchEnabled = true;
        if (licensePrefDtoList != null) {
            LicensePrefDto autoMatchLicensePreference = licensePrefDtoList.getAutoMatch();
            for (LicensePrefDetails licensePrefDetails : autoMatchLicensePreference.getLicensePrefDetailsList()) {
                if (licensePrefDetails.getLicensePrefEnum() == LicensePrefEnum.AUTO_MATCH) {
                    isAutoMatedMatchEnabled = licensePrefDetails.isEnabled();
                }
            }
        }
        if (isAutoMatedMatchEnabled) {
            for (NewJobDto job : jobs) {
                LicensePrefDtoList clientOrBuLicensePrefList = getReqLicensePreferences(companyId, job.getClientOrBuId(), job.getRequisitionNumber());
                if (clientOrBuLicensePrefList != null) {
                    LicensePrefDto autoMatchLicensePreference = clientOrBuLicensePrefList.getAutoMatch();
                    for (LicensePrefDetails licensePrefDetails : autoMatchLicensePreference.getLicensePrefDetailsList()) {
                        if (licensePrefDetails.getLicensePrefEnum() == LicensePrefEnum.AUTO_MATCH) {
                            job.setAutoMatedMatchEnabled(licensePrefDetails.isEnabled());
                        }
                    }
                }
            }
        } else {
            for (NewJobDto job : jobs) {
                job.setAutoMatedMatchEnabled(false);
            }
        }
    }

    @Override
    public JobBoardParameters getJobBoardParameterList() {
        return reqApiExecutor.getJobBoardParameterList();
    }

    @Override
    public Map<String, List<String>> getRadiusAndRecencyOptionsList() {
        return reqApiExecutor.getRadiusAndRecencyOptionsList();
    }

    @Override
    public JobResponseDto getJobById(String jobId) {
        JobResponseDto jobResponseDto = reqApiExecutor.getJobById(jobId);
        if (jobResponseDto.getJobType() != null) {
            Map<String, List<String>> jobWorkflowSteps = workflowStepService.getWorkflowStepsByCompanyId(jobResponseDto.getCompany().getId());
            jobResponseDto.setWorkflowStepsOrder(jobWorkflowSteps);
        }
        return jobResponseDto;
    }

    @Override
    public String getRequisitionById(String requisitionId) {
        return reqApiExecutor.getRequisitionById(requisitionId);
    }

    @Override
    public byte[] getJobRequisitionFile(String requisitionId) {
        return reqApiExecutor.getJobRequisitionFile(requisitionId);
    }

    @Override
    public List<VendorMinDto> getVendorList(String companyId, String buId) {
        return reqApiExecutor.getVendorList(companyId, buId);
    }

    @Override
    public List<VendorMinDto> getAssignedVendors(String jobId) {
        return reqApiExecutor.getAssignedVendors(jobId);
    }

    @Override
    public List<JobVendorDetailDTO> getVendorsOfJob(String requisitionId) {
        return reqApiExecutor.getVendorsOfJob(requisitionId);
    }

    @Override
    public List<RequisitionDetailsResponseDTO> getRequisitionDetailsByClientOrBuId(RequisitionDetailsRequestDTO requisitionDetailsRequestDTO) {
        RequisitionDetailsDTO requisitionDetailsDTO = new RequisitionDetailsDTO();
        User user = userService.getCurrentLoggedInUser();
        JobRequestDto jobRequestDto = new JobRequestDto();
        jobRequestDto.setIsAllRequisition(true);
        jobRequestDto.setAassignedToMe(true);
        jobRequestDto.setAssignedToOthers(true);
        jobRequestDto.setIsSharedWithMe(true);
        jobRequestDto.setUnassigned(true);
        List<NewJobDto> newJobDtos = licensePrefService.getAllJobsByClientOrBuIdAndCompanyId(jobRequestDto, requisitionDetailsRequestDTO.getCompanyId(), requisitionDetailsRequestDTO.getClientOrBuId(), user.getId());
        if (!CollectionUtils.isEmpty(newJobDtos)) {
            requisitionDetailsDTO.setCompanyId(requisitionDetailsRequestDTO.getCompanyId());
            requisitionDetailsDTO.setUserId(user.getId());
            requisitionDetailsDTO.setNewJobDtos(newJobDtos);
            requisitionDetailsDTO.setWorkflowStep(requisitionDetailsRequestDTO.getWorkflowStep());
            return reqApiExecutor.getRequisitionDetailsByClientOrBuId(requisitionDetailsDTO);
        }
        return null;
    }

    @Override
    public String getSalaryDurationOptions() {
        return reqApiExecutor.getSalaryDurationOptions();
    }

    @Override
    public String getCurrencyCodes() {
        return reqApiExecutor.getCurrencyCodes();
    }

    private LicensePrefDtoList setCandidatesPresentOnStep(String companyId, String clientId, String jobId, LicensePrefDtoList licensePreferences) {
        User user = userService.getCurrentLoggedInUser();
        String currentCompanyId = companyId;
        if (StringUtils.isNotEmpty(clientId)) {
            currentCompanyId = clientId;
        }
        CandidatesEnabledStepDto candidatesEnabledStepDto = candidateService.getCandidatesPresentOnStep(currentCompanyId, user.getId(), jobId);
        if (candidatesEnabledStepDto.isCandidatesPresentOnRecruiterScreening()
                && licensePreferences.getRecruiterScreening().getLicensePrefDetailsList().get(0).isEnabled()) {
            licensePreferences.getRecruiterScreening().getLicensePrefDetailsList().get(0).setCandidatesPresentOnStep(true);
            licensePreferences.getRecruiterScreening().getLicensePrefDetailsList()
                    .get(0).setMessageForCandidatePresent("There are " + candidatesEnabledStepDto.getCandidatesCountOnRecruiterScreening() + " candidate cards in this step. Please move all candidate cards out of this step before performing this action.");
        }
        if (candidatesEnabledStepDto.isCandidatesPresentOnTechAssessment()
                && licensePreferences.getTechAssessmentList().getLicensePrefDetailsList().get(0).isEnabled()) {
            licensePreferences.getTechAssessmentList().getLicensePrefDetailsList().get(0).setCandidatesPresentOnStep(true);
            licensePreferences.getTechAssessmentList().getLicensePrefDetailsList()
                    .get(0).setMessageForCandidatePresent("There are " + candidatesEnabledStepDto.getCandidatesCountOnTechAssessment() + " candidate cards in this step. Please move all candidate cards out of this step before performing this action.");
        }
        if (candidatesEnabledStepDto.isCandidatesPresentOnValueAssessment()
                && licensePreferences.getValueAssessmentList().getLicensePrefDetailsList().get(0).isEnabled()) {
            licensePreferences.getValueAssessmentList().getLicensePrefDetailsList().get(0).setCandidatesPresentOnStep(true);
            licensePreferences.getValueAssessmentList().getLicensePrefDetailsList()
                    .get(0).setMessageForCandidatePresent("There are " + candidatesEnabledStepDto.getCandidatesCountOnValueAssessment() + " candidate cards in this step. Please move all candidate cards out of this step before performing this action.");
        }
        if (candidatesEnabledStepDto.isCandidatesPresentOnPhoneScreen()
                && licensePreferences.getPhoneScreenList().getLicensePrefDetailsList().get(0).isEnabled()) {
            licensePreferences.getPhoneScreenList().getLicensePrefDetailsList().get(0).setCandidatesPresentOnStep(true);
            licensePreferences.getPhoneScreenList().getLicensePrefDetailsList()
                    .get(0).setMessageForCandidatePresent("There are " + candidatesEnabledStepDto.getCandidatesCountOnPhoneScreen() + " candidate cards in this step. Please move all candidate cards out of this step before performing this action.");
        }
        if (candidatesEnabledStepDto.isCandidatesPresentOnInterview()
                && licensePreferences.getInterviewList().getLicensePrefDetailsList().get(0).isEnabled()) {
            licensePreferences.getInterviewList().getLicensePrefDetailsList().get(0).setCandidatesPresentOnStep(true);
            licensePreferences.getInterviewList().getLicensePrefDetailsList()
                    .get(0).setMessageForCandidatePresent("There are " + candidatesEnabledStepDto.getCandidatesCountOnInterview() + " candidate cards in this step. Please move all candidate cards out of this step before performing this action.");
        }
        if (candidatesEnabledStepDto.isCandidatesPresentOnVerification()
                && licensePreferences.getVerificationList().getLicensePrefDetailsList().get(0).isEnabled()) {
            licensePreferences.getVerificationList().getLicensePrefDetailsList().get(0).setCandidatesPresentOnStep(true);
            licensePreferences.getVerificationList().getLicensePrefDetailsList()
                    .get(0).setMessageForCandidatePresent("There are " + candidatesEnabledStepDto.getCandidatesCountOnVerification() + " candidate cards in this step. Please move all candidate cards out of this step before performing this action.");
        }
        return licensePreferences;
    }

}
