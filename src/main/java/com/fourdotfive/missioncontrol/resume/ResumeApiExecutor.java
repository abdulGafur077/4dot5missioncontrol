package com.fourdotfive.missioncontrol.resume;

import com.fourdotfive.missioncontrol.dtos.candidate.CandidateReqResumeMinInfoDto;
import com.fourdotfive.missioncontrol.pojo.job.*;
import org.json.JSONObject;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;


public interface ResumeApiExecutor {
    List<ResumeSourceType> getResumeSourceTypes(String companyid, String buId);

	ResumeSourceType getResumeSourceTypeById(String id);

	String uploadResumes(List<MultipartFile> files, ResumeUpload resumeUpload) throws IOException;
	String processResume(JSONObject fileInfo);
	String runResumesForScoring(ParsingOutputObject parsingOutputObject, String transactionType,
									  String clientorbuid, String reqtransactionId, String jobId, String match, String loggedInUserId, boolean hasToOverride);
	String runResumesForAssociate(CandidateResumeOutput parsingOutputObject, String transactionType,
								  String clientorbuid, String reqtransactionId, String jobId, boolean forceToMatch, String loggedInUserId, String candidateId);
	UploadResumeCandidateDto getCandidateCountByResumeTransaction(String transactionId);
	List<CandidateReqResumeMinInfoDto> getCandidatesByResumeTransaction(String transactionId, String countType);
	String updateScoreBasedOnPreviousResume(String resumeId);
	String processResumesWithoutScoring(ParsingOutputObject parsingOutputObject, String transactionType,
										String clientorbuid, String reqtransactionId, String jobId, String match, String loggedInUserId);
	byte[] getResume(String candidateId);

	byte[] getResumeHrXml(String candidateId);

	String getCandidateLatestResume(String candidateId);
}
