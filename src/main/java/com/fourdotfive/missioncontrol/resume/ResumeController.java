package com.fourdotfive.missioncontrol.resume;

import com.fourdotfive.missioncontrol.common.CommonUtil;
import com.fourdotfive.missioncontrol.dtos.candidate.CandidateReqResumeMinInfoDto;
import com.fourdotfive.missioncontrol.exception.FileUploadException;
import com.fourdotfive.missioncontrol.pojo.job.*;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/resume")
public class ResumeController {

    private final ResumeService resumeService;

    private static final Logger LOGGER = LoggerFactory
            .getLogger(ResumeController.class);

    @Autowired
    public ResumeController(ResumeService resumeService) {
        this.resumeService = resumeService;
    }


    @RequestMapping(value = "/resumesourcetypes", method = RequestMethod.GET)
    public ResponseEntity<List<ResumeSourceType>> resumeSourceTypeList(
            @RequestParam(value = "companyId", required = false) String companyId,
            @RequestParam(value = "buId", required = false) String buId) {
        List<ResumeSourceType> response = resumeService.getResumeSourceTypes(companyId, buId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PreAuthorize("@featureAccessControlServiceImpl.isAddCandidateFeatureAvailable(principal)")
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public ResponseEntity<String> uploadResumes(@RequestParam String companyId,
            @RequestParam String resumeSourceTypeId, @RequestParam String userId, @RequestParam List<String> buIdList,
            @RequestParam("file") List<MultipartFile> files, @RequestParam String otherSourceTypeNotes, @RequestParam String referrerName,
            @RequestParam String referrerEmail, ResumeUpload resumeUpload)
            throws FileUploadException, IOException {
        boolean flag = CommonUtil.checkFileWhileUploadResume(files);
        resumeUpload.setCompanyId(companyId);
        resumeUpload.setResumesourcetypeId(resumeSourceTypeId);
        resumeUpload.setUserId(userId);
        resumeUpload.setBuIdList(buIdList);
        resumeUpload.setOtherSourceTypeNotes(otherSourceTypeNotes);
        resumeUpload.setReferrerName(referrerName);
        resumeUpload.setReferrerEmail(referrerEmail);
        if (flag) {
            String response = resumeService.uploadResumes(files, resumeUpload);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("@featureAccessControlServiceImpl.isMatchingFeatureAvailable(principal)")
    @RequestMapping(value = "/runresumesforscoring/{transactionType}", method = RequestMethod.POST)
    public ResponseEntity<String> runResumesForScoring(@RequestBody ParsingOutputObject parsingOutputObject,
                                                       @PathVariable String transactionType,
                                                       @RequestParam(value = "clientorbuid", required = false, defaultValue = "") String clientOrBuId,
                                                       @RequestParam(value = "reqtransactionId", required = false, defaultValue = "") String reqtransactionId,
                                                       @RequestParam(value = "jobId", required = false, defaultValue = "") String jobId,
                                                       @RequestParam(value = "match", required = false, defaultValue = "") String match,
                                                       @RequestParam(value = "hasToOverride", required = false) boolean hasToOverride) {

        String response = resumeService.runResumesForScoring(parsingOutputObject, transactionType, clientOrBuId, reqtransactionId, jobId, match, hasToOverride);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PreAuthorize("@featureAccessControlServiceImpl.isMatchingFeatureAvailable(principal)")
    @RequestMapping(value = "/runresumesforassociate/{transactionType}/{candidateId}", method = RequestMethod.POST)
    public ResponseEntity<String> runResumesForScoring(@RequestBody CandidateResumeOutput parsingOutputObject,
                                                       @PathVariable String transactionType,
                                                       @PathVariable String candidateId,
                                                       @RequestParam(value = "clientorbuid", required = false, defaultValue = "") String clientOrBuId,
                                                       @RequestParam(value = "reqtransactionId", required = false, defaultValue = "") String reqtransactionId,
                                                       @RequestParam(value = "jobId", required = false, defaultValue = "") String jobId,
                                                       @RequestParam(value = "forceToMatch", required = false) boolean forceToMatch) {

        String response = resumeService.runResumesForAssociate(parsingOutputObject, transactionType, clientOrBuId, reqtransactionId, jobId, forceToMatch, candidateId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/getcandidatecountbyresumetransaction/{transactionid}", method = RequestMethod.GET)
    public ResponseEntity<UploadResumeCandidateDto> getCandidateCountByResumeTransaction(
            @PathVariable("transactionid") String transactionId) {
        UploadResumeCandidateDto response = resumeService.getCandidateCountByResumeTransaction(transactionId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/getcandidatesbyresumetransaction/{transactionId}/{counttype}", method = RequestMethod.GET)
    public ResponseEntity<List<CandidateReqResumeMinInfoDto>> getCandidatesByResumeTransaction(@PathVariable("transactionId") String transactionId,
        @PathVariable("counttype") String countType ){

        if(transactionId == null || countType == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<CandidateReqResumeMinInfoDto> candidates = resumeService.getCandidatesByResumeTransaction(transactionId, countType);
        return new ResponseEntity<>(candidates, HttpStatus.OK);
    }

    @RequestMapping(value = {"/updatescorebasedonpreviousresume/{resumeId}"}, method = RequestMethod.POST)
    public ResponseEntity<String> updateScoreBasedOnPreviousResume(@PathVariable("resumeId") String resumeId) {
        String response = resumeService.updateScoreBasedOnPreviousResume(resumeId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PreAuthorize("@featureAccessControlServiceImpl.isMatchingFeatureAvailable(principal)")
    @RequestMapping(value = "/runresumeswithoutscoring/{transactionType}", method = RequestMethod.POST)
    public ResponseEntity<String> runresumesWithoutScoring(@RequestBody ParsingOutputObject parsingOutputObject,
                                                       @PathVariable String transactionType,
                                                       @RequestParam(value = "clientorbuid", required = false, defaultValue = "") String clientOrBuId, @RequestParam(value = "reqtransactionId", required = false, defaultValue = "") String reqtransactionId,
                                                       @RequestParam(value = "jobId", required = false, defaultValue = "") String jobId,
                                                       @RequestParam(value = "match", required = false, defaultValue = "") String match) {

        String response = resumeService.processResumesWithoutScoring(parsingOutputObject, transactionType, clientOrBuId, reqtransactionId, jobId, match);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "getresume/{candidateId}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getResume(@PathVariable String candidateId) throws IOException {
        String resumeName = resumeService.getCandidateLatestResume(candidateId);
        LOGGER.info("The resume file name "+resumeName);
        byte[] downloadInputStream = resumeService.getResume(candidateId);
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .header(HttpHeaders.CONTENT_DISPOSITION,"attachment; filename=\"" + resumeName)
                .body(downloadInputStream);
    }

    @RequestMapping(value = "getresumehrxml/{candidateId}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getResumeHrXml(@PathVariable String candidateId) throws IOException {
//        String resumeName = resumeService.getCandidateLatestResume(candidateId);
        byte[] downloadInputStream = resumeService.getResumeHrXml(candidateId);
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .header(HttpHeaders.CONTENT_DISPOSITION,"attachment; filename=\"" + "hrxml.xml")
                .body(downloadInputStream);
    }
}