package com.fourdotfive.missioncontrol.resume;

import com.fourdotfive.missioncontrol.dtos.candidate.CandidateReqResumeMinInfoDto;
import com.fourdotfive.missioncontrol.pojo.job.*;
import org.json.JSONObject;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface ResumeService {
    List<ResumeSourceType> getResumeSourceTypes(String companyId, String buId);
	String uploadResumes(List<MultipartFile> files, ResumeUpload resumeUpload) throws IOException;
	String processResume(JSONObject fileInfo);
	String runResumesForScoring(ParsingOutputObject parsingOutputObject, String transactionType,
									  String clientorbuid, String reqtransactionId, String jobId, String match, boolean hasToOverride);

	String runResumesForAssociate(CandidateResumeOutput parsingOutputObject, String transactionType,
								String clientorbuid, String reqtransactionId, String jobId, boolean forceToMatch, String candidateId);

	UploadResumeCandidateDto getCandidateCountByResumeTransaction(String transactionId);
	List<CandidateReqResumeMinInfoDto> getCandidatesByResumeTransaction(String transactionId, String countType);
	String updateScoreBasedOnPreviousResume(String resumeId);
	String processResumesWithoutScoring(ParsingOutputObject parsingOutputObject, String transactionType,
								String clientorbuid, String reqtransactionId, String jobId, String match);

	byte[] getResume(String candidateId);

	byte[] getResumeHrXml(String candidateId);

	String getCandidateLatestResume(String candidateId);
}
