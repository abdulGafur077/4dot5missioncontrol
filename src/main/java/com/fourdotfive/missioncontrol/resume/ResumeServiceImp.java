package com.fourdotfive.missioncontrol.resume;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourdotfive.missioncontrol.candidate.CandidateDetailsResponseDTO;
import com.fourdotfive.missioncontrol.candidate.CandidateService;
import com.fourdotfive.missioncontrol.common.AccessUtil;
import com.fourdotfive.missioncontrol.dtos.candidate.CandidateReqResumeMinInfoDto;
import com.fourdotfive.missioncontrol.platformexcecutor.PlatformApiExecutorImp;
import com.fourdotfive.missioncontrol.pojo.FileInfo;
import com.fourdotfive.missioncontrol.pojo.job.*;
import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.user.UserService;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class ResumeServiceImp implements ResumeService {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ResumeServiceImp.class);

	@Autowired
	private PlatformApiExecutorImp platformApiExecutorImp;

	@Autowired
	private ResumeApiExecutor resumeApiExecutor;

	@Autowired
	private AccessUtil accessUtil;

	@Autowired
	private UserService userService;

	@Autowired
	private CandidateService candidateService;

	@Override
	public List<ResumeSourceType> getResumeSourceTypes(String companyId, String buId) {
		return resumeApiExecutor.getResumeSourceTypes(companyId, buId);
	}

	@Override
	public String uploadResumes(List<MultipartFile> files, ResumeUpload resumeUpload) throws IOException {
		String resumeResponse = resumeApiExecutor.uploadResumes(files, resumeUpload);
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		List<FileInfo> fileInfoList = objectMapper.readValue(resumeResponse, new TypeReference<List<FileInfo>>(){});
		JSONObject jsonFileInfo = new JSONObject();
		jsonFileInfo.put("inputFileInfos", fileInfoList);
		String processResumeResponse;
		processResumeResponse = processResume(jsonFileInfo);
		return processResumeResponse;
	}

	@Override
	public String processResume(JSONObject fileInfo){
		return resumeApiExecutor.processResume(fileInfo);
	}

	@Override
	public String runResumesForScoring(ParsingOutputObject parsingOutputObject, String transactionType,
									  String clientorbuid, String reqtransactionId, String jobId, String match, boolean hasToOverride) {
		User user = userService.getCurrentLoggedInUser();
		return resumeApiExecutor.runResumesForScoring(parsingOutputObject,
				transactionType, clientorbuid, reqtransactionId, jobId, match, user.getId(), hasToOverride);

	}

	@Override
	public String runResumesForAssociate(CandidateResumeOutput parsingOutputObject, String transactionType, String clientorbuid, String reqtransactionId, String jobId, boolean forceToMatch, String candidateId) {
		User user = userService.getCurrentLoggedInUser();
		return resumeApiExecutor.runResumesForAssociate(parsingOutputObject,
				transactionType, clientorbuid, reqtransactionId, jobId, forceToMatch, user.getId(), candidateId);
	}

	@Override
    public UploadResumeCandidateDto getCandidateCountByResumeTransaction(String transactionId) {
		return resumeApiExecutor.getCandidateCountByResumeTransaction(transactionId);
    }

	@Override
	public List<CandidateReqResumeMinInfoDto> getCandidatesByResumeTransaction(String transactionId, String countType) {
		return resumeApiExecutor.getCandidatesByResumeTransaction(transactionId, countType);
	}

	@Override
	public String updateScoreBasedOnPreviousResume(String resumeId) {
		return resumeApiExecutor.updateScoreBasedOnPreviousResume(resumeId);
	}

	@Override
	public String processResumesWithoutScoring(ParsingOutputObject parsingOutputObject, String transactionType, String clientorbuid, String reqtransactionId, String jobId, String match) {
		User user = userService.getCurrentLoggedInUser();
		return resumeApiExecutor.processResumesWithoutScoring(parsingOutputObject,
				transactionType, clientorbuid, reqtransactionId, jobId, match, user.getId());
	}

	@Override
	public byte[] getResume(String candidateId) {
		return resumeApiExecutor.getResume(candidateId);
	}

	@Override
	public byte[] getResumeHrXml(String candidateId) {
		return resumeApiExecutor.getResumeHrXml(candidateId);
	}

	@Override
	public String getCandidateLatestResume(String candidateId) {
		String originalResumeName = resumeApiExecutor.getCandidateLatestResume(candidateId);
		CandidateDetailsResponseDTO candidateDetails = candidateService.getCandidateDetailsById(candidateId);
		String fileExtension = originalResumeName.substring(originalResumeName.lastIndexOf("."));
		return StringUtils.isNotEmpty(candidateDetails.getContact().getLastname()) ? candidateDetails.getContact().getFirstname() + " "
				+ candidateDetails.getContact().getLastname() + fileExtension : candidateDetails.getContact().getFirstname() + fileExtension;
	}
}
