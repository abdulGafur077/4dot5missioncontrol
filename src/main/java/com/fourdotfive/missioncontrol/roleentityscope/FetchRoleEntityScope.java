package com.fourdotfive.missioncontrol.roleentityscope;

import com.fourdotfive.missioncontrol.dtos.company.RoleEntityScopeDto;
import com.fourdotfive.missioncontrol.pojo.company.Company;

public interface FetchRoleEntityScope {

	/**
	 * Get RES for Staffing company, if is empty then generate RES with default
	 * value
	 * 
	 * @param company
	 * @return
	 */
	RoleEntityScopeDto getRoleEntityScopeForStaffingCompany(Company company);

	/**
	 * Get RES for Corporation company, if is empty then generate RES with
	 * default value
	 * 
	 * @param company
	 * @return
	 */
	RoleEntityScopeDto getRoleEntityScopeForCorporation(Company company);

}
