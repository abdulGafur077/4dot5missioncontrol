package com.fourdotfive.missioncontrol.roleentityscope;

import com.fourdotfive.missioncontrol.common.AccessUtil;
import com.fourdotfive.missioncontrol.company.CompanyService;
import com.fourdotfive.missioncontrol.dtos.company.CompanyDto;
import com.fourdotfive.missioncontrol.dtos.company.RoleEntityScopeDto;
import com.fourdotfive.missioncontrol.dtos.company.RoleScopeDto;
import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.company.CompanyType;
import com.fourdotfive.missioncontrol.pojo.company.RoleEntityScope;
import com.fourdotfive.missioncontrol.pojo.user.Role;
import com.fourdotfive.missioncontrol.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Chakravarthy,Shalini
 *
 */
@Service
public class FetchRoleEntityScopeImp implements FetchRoleEntityScope {

	@Autowired
	private CompanyService companyService;

	@Autowired
	private AccessUtil accessUtil;

	@Autowired
	private UserService userService;

	private static final Logger LOGGER = LoggerFactory
			.getLogger(FetchRoleEntityScopeImp.class);

	@Override
	public RoleEntityScopeDto getRoleEntityScopeForStaffingCompany(
			Company company) {
		RoleEntityScopeDto roleEntityScopeDto = new RoleEntityScopeDto();
		// set company type
		roleEntityScopeDto.setCompanyType(company.getCompanyType());
		// get RES
		List<RoleEntityScope> scopeList = companyService
				.getBasicEntityScope(company.getId());
		// if RES is empty, then RES is not define for this company yet
		if (scopeList.isEmpty()) {
			// generate RES with default values
			roleEntityScopeDto.setScopeList(generateRoleEntityScope(company
					.getCompanyType()));
			// set RES as new
			roleEntityScopeDto.setNewScope(true);
		} else {
			// convert RES to DTO
			roleEntityScopeDto.setScopeList(getScopeDto(scopeList));
			// set RES as old
			roleEntityScopeDto.setNewScope(false);
		}
		return roleEntityScopeDto;

	}

	@Override
	public RoleEntityScopeDto getRoleEntityScopeForCorporation(Company company) {
		RoleEntityScopeDto roleEntityScopeDto = new RoleEntityScopeDto();
		// set company type
		roleEntityScopeDto.setCompanyType(company.getCompanyType());
		// get RES
		List<RoleEntityScope> scopeList = companyService
				.getBasicEntityScope(company.getId());
		// get BU list
		List<CompanyDto> buList = companyService
				.getActiveClientOrBuDtoList(company.getId());
		// if RES is empty, then RES is not define for this company yet
		if (scopeList.isEmpty()) {
			// set RES as new
			roleEntityScopeDto.setNewScope(true);
			// if BU list size empty, then no set RES for this company, hence
			// disable all the fields from selection
//			if (buList == null || buList.size() == 0) {
//				roleEntityScopeDto = getDisabledScope(roleEntityScopeDto,
//						company);
//				return roleEntityScopeDto;
//			} else {
				// generate RES with default values
				roleEntityScopeDto.setScopeList(generateRoleEntityScope(company
						.getCompanyType()));
//			}
		} else {
			// set RES as old
			roleEntityScopeDto.setNewScope(false);
			// if BU list size empty, then no set RES for this company, hence
			// disable all the fields from selection
//			if (buList == null || buList.size() == 0) {
//				roleEntityScopeDto = getDisabledScope(roleEntityScopeDto,
//						company);
//				return roleEntityScopeDto;
//			} else {
				// convert RES to DTO
				roleEntityScopeDto.setScopeList(getScopeDto(scopeList));
//			}
		}
		return roleEntityScopeDto;
	}

	/**
	 * For Corporation ,since there are no bu, set RES to disabled
	 * 
	 * @param roleEntityScopeDto
	 * @param company
	 * @return
	 */
	private RoleEntityScopeDto getDisabledScope(
			RoleEntityScopeDto roleEntityScopeDto, Company company) {
		// convert RES as disabled
		roleEntityScopeDto.setDisabled(true);
		// generate value
		roleEntityScopeDto.setScopeList(generateRoleEntityScope(company
				.getCompanyType()));
		return roleEntityScopeDto;
	}

	/**
	 * Based on role, map RES to RoleScopeDto
	 * 
	 * @param roleEntityScopes
	 * @return
	 */
	private List<RoleScopeDto> getScopeDto(
			List<RoleEntityScope> roleEntityScopes) {
		List<RoleScopeDto> scopeDtos = new ArrayList<RoleScopeDto>();
		for (RoleEntityScope roleEntityScope : roleEntityScopes) {
			// Do not get RES for Company admin,4Dot5Admin,Super user
			if (!accessUtil.isCompanyAdmin(roleEntityScope.getRole())
					&& !accessUtil.isFourDot5Admin(roleEntityScope.getRole())
					&& !accessUtil.isSuperUser(roleEntityScope.getRole()
							.getId())) {
				RoleScopeDto roleScopeDto = RoleScopeDto
						.mapRoleEntityScopeToDto(roleEntityScope);
				scopeDtos.add(roleScopeDto);
			}

		}
		return scopeDtos;

	}

	/**
	 * This method is called when the role entity scope has not yet been defined
	 * for a company We get all the roles based on hierarchy, create a scope and
	 * return
	 * 
	 * @return
	 */
	private List<RoleScopeDto> generateRoleEntityScope(CompanyType companyType) {

		List<RoleScopeDto> scopeList = new ArrayList<RoleScopeDto>();

		List<Role> roles = userService.getAllRoles();
		for (Role role : roles) {
			// for Staffing company
			if (companyType.equals(CompanyType.StaffingCompany)) {
				if (accessUtil.isEntityScopeStaffingCompanyRole(role)) {
					RoleScopeDto scope = getRoleScopeDtoBasedOnRole(role);
					if (scope != null) {
						scopeList.add(scope);
					}
				}
				// for Corporation company
			} else if (companyType.equals(CompanyType.Corporation)) {
				if (accessUtil.isEntityScopeCorporateCompanyRole(role)) {
					RoleScopeDto scope = getRoleScopeDtoBasedOnRole(role);
					if (scope != null) {
						scopeList.add(scope);
					}
				}
			}

		}

		return scopeList;
	}

	/**
	 * Do not get RES for Company admin,4Dot5Admin,Super user
	 * 
	 * @param role
	 * @return
	 */
	private RoleScopeDto getRoleScopeDtoBasedOnRole(Role role) {
		if (!accessUtil.isCompanyAdmin(role)
				&& !accessUtil.isFourDot5Admin(role)
				&& !accessUtil.isSuperUser(role.getId())) {
			RoleScopeDto scope = new RoleScopeDto(false, role);

			return scope;
		}
		return null;
	}

}
