package com.fourdotfive.missioncontrol.roleentityscope;

import com.fourdotfive.missioncontrol.dtos.company.RoleEntityScopeDto;

public interface RoleEntityScopeService {

	RoleEntityScopeDto createRoleEnityScope(String companyId, RoleEntityScopeDto dto);

	RoleEntityScopeDto updateRoleEntityScope(String companyId, RoleEntityScopeDto dto);

	RoleEntityScopeDto getRoleEntityScope(String companyId);

}
