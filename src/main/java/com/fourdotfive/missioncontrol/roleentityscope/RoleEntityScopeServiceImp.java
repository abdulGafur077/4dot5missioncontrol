package com.fourdotfive.missioncontrol.roleentityscope;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fourdotfive.missioncontrol.common.AccessUtil;
import com.fourdotfive.missioncontrol.common.CommonUtil;
import com.fourdotfive.missioncontrol.company.CompanyApiExecutor;
import com.fourdotfive.missioncontrol.company.CompanyService;
import com.fourdotfive.missioncontrol.dtos.company.RoleEntityScopeDto;
import com.fourdotfive.missioncontrol.dtos.company.RoleScopeDto;
import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.company.CompanyType;
import com.fourdotfive.missioncontrol.pojo.company.RoleEntityScope;
import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.user.UserApiExecutor;
import com.fourdotfive.missioncontrol.user.UserService;
import com.fourdotfive.missioncontrol.userentityscope.UserScopeService;

/**
 * 
 * @author Chakravarthy,Shalini
 *
 */
@Service
public class RoleEntityScopeServiceImp implements RoleEntityScopeService {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(RoleEntityScopeServiceImp.class);

	@Autowired
	private AccessUtil accessUtil;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private CompanyApiExecutor apiExecutor;

	@Autowired
	private UserApiExecutor userApiExecutor;

	@Autowired
	private UserScopeService userScopeService;

	@Autowired
	private FetchRoleEntityScope fetchRoleEntityScope;

	@Override
	public RoleEntityScopeDto getRoleEntityScope(String companyId) {
		Company company = companyService.getCompany(companyId);
		LOGGER.info("company name : "+company.getName());
		RoleEntityScopeDto roleEntityScopeDto = null;
		if (company.getCompanyType().equals(CompanyType.Corporation)) {
			LOGGER.info("company type : "+company.getCompanyType());
			roleEntityScopeDto = fetchRoleEntityScope
					.getRoleEntityScopeForCorporation(company);
			LOGGER.info("roleEntityScopeDto : "+roleEntityScopeDto.toString());
			Collections.sort(roleEntityScopeDto.getScopeList(),
					new CommonUtil.CorportateCompanyComparator());
			LOGGER.info("After sort roleEntityScopeDto : "+roleEntityScopeDto.toString());
		} else if (company.getCompanyType().equals(CompanyType.StaffingCompany)) {
			LOGGER.info("company type : "+company.getCompanyType());
			roleEntityScopeDto = fetchRoleEntityScope
					.getRoleEntityScopeForStaffingCompany(company);
			LOGGER.info("roleEntityScopeDto : "+roleEntityScopeDto.toString());
			Collections.sort(roleEntityScopeDto.getScopeList(),
					new CommonUtil.StaffingCompanyComparator());
			LOGGER.info("After sort roleEntityScopeDto : "+roleEntityScopeDto.toString());
		}

		return roleEntityScopeDto;
	}

	@Override
	public RoleEntityScopeDto createRoleEnityScope(String companyId,
			RoleEntityScopeDto newEntityScopeDto) {
		if (newEntityScopeDto != null && !newEntityScopeDto.isDisabled()) {
			List<RoleEntityScope> roleEntityScopeList = RoleEntityScope
					.mapDtotoRoleEntityScope(newEntityScopeDto.getScopeList());
			Company company = apiExecutor.createRoleEnityScope(companyId,
					roleEntityScopeList);
			if (accessUtil.isCorporateCompany(company.getCompanyType()
					.toString())) {
				for (RoleScopeDto newScopeDto : newEntityScopeDto
						.getScopeList()) {
					changeUESforCorporateUser(newScopeDto, newEntityScopeDto,
							companyId);
				}
			}
			if (company != null) {
				return getRoleEntityScope(companyId);
			}
		}
		return null;
	}

	@Override
	public RoleEntityScopeDto updateRoleEntityScope(String companyId,
			RoleEntityScopeDto newEntityScopeDto) {
		if (newEntityScopeDto != null) {
			List<RoleEntityScope> roleEntityScopeList = null;
			if (!newEntityScopeDto.isDisabled()) {
				roleEntityScopeList = RoleEntityScope
						.mapDtotoRoleEntityScope(newEntityScopeDto
								.getScopeList());
			}
			RoleEntityScopeDto oldRoleEntityScopeDto = getRoleEntityScope(companyId);
			Company company = apiExecutor.updateRoleEntityScope(companyId,
					roleEntityScopeList);
			updateUserScope(newEntityScopeDto, oldRoleEntityScopeDto, companyId);

			if (company != null) {
				return getRoleEntityScope(companyId);
			}
		}
		return null;
	}

	private void updateUserScope(RoleEntityScopeDto newEntityScopeDto,
			RoleEntityScopeDto oldEntityScopeDto, String companyId) {
		for (RoleScopeDto oldScopeDto : oldEntityScopeDto.getScopeList()) {
			for (RoleScopeDto newScopeDto : newEntityScopeDto.getScopeList()) {
				//check scope has been updated for a specific role
				if (oldScopeDto.getRole().getId()
						.equals(newScopeDto.getRole().getId()))

					checkRoleScopeHasChanged(oldScopeDto, newScopeDto,
							newEntityScopeDto, companyId);
			}
		}

	}

	private void checkRoleScopeHasChanged(RoleScopeDto oldRoleScope,
			RoleScopeDto newRoleScope,
			RoleEntityScopeDto newRoleEntityScopeDto, String companyId) {
		//compare old RES with New RES for a role
		if (oldRoleScope.isAllClientsOrBU() != newRoleScope.isAllClientsOrBU()
				||oldRoleScope.isAllBUsOfClients() != newRoleScope.isAllBUsOfClients()) {
			List<RoleEntityScope> roleEntityScopeList = RoleEntityScope
					.mapDtotoRoleEntityScope(newRoleEntityScopeDto
							.getScopeList());
			//get all user based on role
			List<User> userList = userApiExecutor.getUserListBasedOnRole(
					newRoleScope.getRole().getId(), companyId);
			if (userList != null && !userList.isEmpty()) {
				for (User user : userList) {
					LOGGER.debug("user whose scope needs to be updated {}",
							user.getFirstname());
					//reset client list for the user
					userScopeService.resetClientList(companyId, user,
							roleEntityScopeList);
				}
			}
		}
	}

	private void changeUESforCorporateUser(RoleScopeDto newRoleScope,
			RoleEntityScopeDto newRoleEntityScopeDto, String companyId) {
		List<RoleEntityScope> roleEntityScopeList = RoleEntityScope
				.mapDtotoRoleEntityScope(newRoleEntityScopeDto.getScopeList());
		List<User> userList = userApiExecutor.getUserListBasedOnRole(
				newRoleScope.getRole().getId(), companyId);
		if (userList != null && !userList.isEmpty()) {
			for (User user : userList) {
				LOGGER.debug("user whose scope needs to be updated {}",
						user.getFirstname());
				userScopeService.resetClientList(companyId, user,
						roleEntityScopeList);
			}
		}

	}

}
