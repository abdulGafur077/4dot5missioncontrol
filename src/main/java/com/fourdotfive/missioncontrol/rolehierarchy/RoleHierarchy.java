package com.fourdotfive.missioncontrol.rolehierarchy;

import java.util.List;

import com.fourdotfive.missioncontrol.pojo.user.Role;

public interface RoleHierarchy {
	List<Role> getCreateRoles(List<Role> roles, String companyType, String roleId);
	
	List<Role> getUpdateRoles(List<Role> roles, String companyType, String roleId);


}
