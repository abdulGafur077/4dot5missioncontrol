package com.fourdotfive.missioncontrol.rolehierarchy;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fourdotfive.missioncontrol.common.AccessUtil;
import com.fourdotfive.missioncontrol.common.AppConstants;
import com.fourdotfive.missioncontrol.pojo.user.Role;

@Service
public class RoleHierarchyImp implements RoleHierarchy {

    private static final Logger LOGGER = LoggerFactory.getLogger(RoleHierarchyImp.class);

    @Autowired
    private AccessUtil accessUtil;

    @Override
    public List<Role> getCreateRoles(List<Role> roles, String companyType,
                                     String roleId) {
        List<Role> userRoles = null;
        switch (roleId) {

            case AppConstants.SUPERUSER_ROLEID:
                userRoles = getRolesForSuperUserWhileCreate(companyType, roles);
                break;

            case AppConstants.FOURDOT5ADMIN_ROLEID:
                userRoles = getRolesFor4dot5Admin(companyType, roles);
                break;

            case AppConstants.STAFFING_ADMIN_ROLEID:
                userRoles = getRolesForStaffingAdmin(roles);
                break;

            case AppConstants.CORPORATE_ADMIN_ROLEID:
                userRoles = getRolesForCorporateAdmin(roles);
                break;

            case AppConstants.CORPORATE_REC_MANAGER_ROLEID:
                userRoles = getRolesForCorporateRecManger(roles);
                break;

            case AppConstants.STAFFING_REC_MANAGER_ROLEID:
                userRoles = getRolesForStaffingRecManager(roles);
                break;

            case AppConstants.HIRING_MANAGER_ROLEID:
                userRoles = getRolesForHiringManger(roles);
                break;

            default:
                break;

        }

        return userRoles;
    }

    @Override
    public List<Role> getUpdateRoles(List<Role> roles, String companyType,
                                     String roleId) {

        return getRolesForSuperUserWhileUpdate(companyType, roles);
    }

    private List<Role> getRolesForSuperUserWhileCreate(String companyType,
                                                       List<Role> roles) {

        List<Role> newRoles = new ArrayList<Role>();
        if (accessUtil.isStaffingCompany(companyType)) {
            for (Role role : roles) {
                if (accessUtil.isStaffingAdmin(role)) {
                    //role.setName(AppConstants.ADMIN);
                    newRoles.add(role);
                }
            }
        } else if (accessUtil.isCorporateCompany(companyType)) {
            for (Role role : roles) {
                if (accessUtil.isCorporateAdmin(role)) {
                    //role.setName(AppConstants.ADMIN);
                    newRoles.add(role);
                }
            }
        } else {
            for (Role role : roles) {
                if (accessUtil.isFourDot5Admin(role)) {
                    newRoles.add(role);
                }
            }
        }
        return newRoles;
    }

    private List<Role> getRolesForSuperUserWhileUpdate(String companyType,
                                                       List<Role> roles) {

        List<Role> newRoles = new ArrayList<Role>();
        if (accessUtil.isStaffingCompany(companyType)) {
            for (Role role : roles) {
                if (accessUtil.isStaffingAdmin(role)) {
                    //role.setDescription(AppConstants.ADMIN);
                    newRoles.add(role);
                } else if (accessUtil.isStaffingCompanyRole(role)) {
                    newRoles.add(role);
                } else if (accessUtil.isEmployee(role)) {
                    newRoles.add(role);
                }
            }
        } else if (accessUtil.isCorporateCompany(companyType)) {
            for (Role role : roles) {
                if (accessUtil.isCorporateAdmin(role)) {
                    //role.setDescription(AppConstants.ADMIN);
                    newRoles.add(role);
                } else if (accessUtil.isCorporateCompanyRole(role)) {
                    newRoles.add(role);
                } else if (accessUtil.isEmployee(role)) {
                    newRoles.add(role);
                }
            }
        } else if (accessUtil.isHostCompany(companyType)) {
            for (Role role : roles) {
                if (accessUtil.isFourDot5Admin(role)) {
                    newRoles.add(role);
                }
            }
        }
        return newRoles;
    }

    private List<Role> getRolesFor4dot5Admin(String companyType,
                                             List<Role> roles) {

        List<Role> newRoles = new ArrayList<Role>();

        if (accessUtil.isStaffingCompany(companyType)) {
            for (Role role : roles) {
                if (accessUtil.isStaffingAdmin(role)) {
                    //role.setDescription(AppConstants.ADMIN);
                    newRoles.add(role);
                }
            }
        } else if (accessUtil.isCorporateCompany(companyType)) {
            for (Role role : roles) {
                if (accessUtil.isCorporateAdmin(role)) {
                    //role.setDescription(AppConstants.ADMIN);
                    newRoles.add(role);
                }
            }
        }
        return newRoles;
    }

    private List<Role> getRolesForStaffingAdmin(List<Role> roles) {

        List<Role> newRoles = new ArrayList<Role>();

        for (Role role : roles) {
            if (accessUtil.isStaffingCompanyRole(role)) {
                newRoles.add(role);
            }
        }

        return newRoles;
    }

    private List<Role> getRolesForCorporateAdmin(List<Role> roles) {

        List<Role> newRoles = new ArrayList<Role>();

        for (Role role : roles) {
            if (accessUtil.isCorporateCompanyRole(role)) {
                newRoles.add(role);
            }
        }

        return newRoles;
    }

    private List<Role> getRolesForCorporateRecManger(List<Role> roles) {

        List<Role> newRoles = new ArrayList<Role>();

        for (Role role : roles) {
            if (accessUtil.isCorporateCompanyRole(role) && !accessUtil.isHiringManagerRole(role)) {
                newRoles.add(role);
            }
        }

        return newRoles;
    }

    private List<Role> getRolesForStaffingRecManager(List<Role> roles) {

        List<Role> newRoles = new ArrayList<Role>();

        for (Role role : roles) {
            if (accessUtil.isStaffingCompanyRole(role)) {
                newRoles.add(role);
            }
        }
        return newRoles;
    }

    private List<Role> getRolesForHiringManger(List<Role> roles) {
        List<Role> newRoles = new ArrayList<Role>();

        for (Role role : roles) {
            if (accessUtil.isHiringManagerRole(role)) {
                newRoles.add(role);
            }
        }
        return newRoles;
    }
}
