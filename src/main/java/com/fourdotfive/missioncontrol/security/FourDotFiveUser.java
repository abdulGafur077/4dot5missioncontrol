/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fourdotfive.missioncontrol.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

/**
 *
 * @author pradeepkm
 */
public class FourDotFiveUser extends User {

	private static final long serialVersionUID = 1L;
	private String id;

	public FourDotFiveUser(String username, Collection<? extends GrantedAuthority> authorities, String userId) {
		super(username, "", authorities);
		this.id = userId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
