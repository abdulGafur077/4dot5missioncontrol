/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fourdotfive.missioncontrol.security;

import com.fourdotfive.missioncontrol.common.TokenDetails;
import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

/**
 *
 * @author pradeepkm
 */
@Component
public class JwtAuthenticationProvider implements AuthenticationProvider{
    public static final Logger LOGGER = LoggerFactory.getLogger(JwtAuthenticationProvider.class);
    @Autowired
    private UserService userService;
    @Override
    public Authentication authenticate(Authentication a) throws AuthenticationException {
        JwtAuthRequestToken jwtAuthReq = (JwtAuthRequestToken)a;
        String token = jwtAuthReq.getJwtToken();
        TokenDetails tokenDetails = JwtUtil.getTokenDetails(token);
        FourDotFiveUser user = JwtUtil.parseToken(token);
        User retrievedUser = userService.getUserById(user.getId());
        if (retrievedUser.getVersion() == null || !retrievedUser.getVersion().equals(tokenDetails.getVersion()))
            return new JwtAuthenticationToken(user, null, user.getAuthorities());

        return new JwtAuthenticationToken(user, token, user.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> type) {
        boolean retVal = JwtAuthRequestToken.class.isAssignableFrom(type);
        LOGGER.debug("in supports with type as {}, retVal= {}", type.getName(), retVal);
        return retVal;
    }
}
