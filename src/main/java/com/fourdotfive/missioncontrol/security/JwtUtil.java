/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fourdotfive.missioncontrol.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.fourdotfive.missioncontrol.common.AppConstants;
import com.fourdotfive.missioncontrol.common.TokenDetails;
import com.fourdotfive.missioncontrol.dtos.user.UserMinInfoDto;
import com.fourdotfive.missioncontrol.pojo.user.RoleScreenXRef;

/**
 *
 * @author pradeepkm, shalini
 */
public final class JwtUtil {

	public static final Logger LOGGER = LoggerFactory.getLogger(JwtUtil.class);

	private JwtUtil() {
	}

	public static UserMinInfoDto createToken(UserMinInfoDto userObj,
			String email) {
		LOGGER.info(LOGGER.getClass()+" Method createToken "+userObj.getFirstname());
		String token, refresh;
		String role = userObj.getRole().getId();
		token = JwtUtil.createToken(email, userObj.getId(), role,
				TokenType.NORMAL_TOKEN, userObj.getRoleScreenRef(),
				userObj.getFirstname(), userObj.getLastname(), userObj.getVersion());
		refresh = JwtUtil.createToken(email, userObj.getId(), role,
				TokenType.REFRESH_TOKEN, userObj.getRoleScreenRef(),
				userObj.getFirstname(), userObj.getLastname(), userObj.getVersion());
		userObj.setEmailId(email);
		userObj.setToken(token);
		userObj.setRefreshToken(refresh);
		LOGGER.info(LOGGER.getClass()+" Method createToken "+userObj.getFirstname()+" "+userObj.getEmailId());
		return userObj;

	}

	/**
	 * To extract FourDotFiveUser details from token
	 * 
	 * @param token
	 * @return
	 * @throws AuthenticationException
	 */
	public static FourDotFiveUser parseToken(String token) {
		try {
			Claims body = Jwts.parser().setSigningKey(AppConstants.SECRET_KEY)
					.parseClaimsJws(token).getBody();
			String userName = body.getSubject();
			String role = (String) body.get(AppConstants.ROLE);
			String id = (String) body.get(AppConstants.USER_ID);
			List<GrantedAuthority> authorities = new ArrayList<>();
			authorities.add(new SimpleGrantedAuthority(role));
			return new FourDotFiveUser(userName, authorities, id);
		} catch (JwtException | ClassCastException e) {
			LOGGER.error("Error while parsing user supplied token {}",
					e.getMessage());
			throw new JwtAuthenticationException("Token parse error...");
		}

	}

	/**
	 * Create Access and Refresh Token based on the params
	 * 
	 * @param userName
	 * @param userId
	 * @param role
	 * @param tokenType
	 * @param companyId
	 * @param roleScreenXRef
	 * @return
	 * @throws AuthenticationException
	 */
	public static String createToken(String userName, String userId,
			String role, TokenType tokenType, RoleScreenXRef roleScreenXRef,
			String firstName, String lastName, Long version) {
		if (userName != null) {
			Claims claims = Jwts.claims().setSubject(userName);
			claims.put(AppConstants.USER_ID, userId);
			claims.put(AppConstants.ROLE, role);
			claims.put(AppConstants.FIRST_NAME, firstName);
			claims.put(AppConstants.LAST_NAME, lastName);
			String userVersion = null;
			if (version != null)
				userVersion = String.valueOf(version);
			claims.put(AppConstants.USER_VERSION, userVersion);
			Date now = new Date();
			claims.setExpiration(calculateTokenExpireDate(now,
					tokenType.getExpiryDays()));
			claims.setIssuedAt(now);
			return Jwts
					.builder()
					.setClaims(claims)
					.signWith(SignatureAlgorithm.HS512, AppConstants.SECRET_KEY)
					.compact();
			}
		throw new JwtAuthenticationException("Could not create JWT token...");
	}

	/**
	 * To extract details from token
	 * 
	 * @param token
	 * @return
	 */
	public static TokenDetails getTokenDetails(String token) {
		try {
			Claims body = Jwts.parser().setSigningKey(AppConstants.SECRET_KEY)
					.parseClaimsJws(token).getBody();
			String userName = body.getSubject();
			String userid = (String) body.get(AppConstants.USER_ID);
			String role = (String) body.get(AppConstants.ROLE);
			String firstName = (String) body.get(AppConstants.FIRST_NAME);
			String lastName = (String) body.get(AppConstants.LAST_NAME);
			String version = (String) body.get(AppConstants.USER_VERSION);
			Long userVersion = null;
			if (StringUtils.isNotBlank(version)) {
				userVersion = Long.parseLong(version);
			}
			return new TokenDetails(userName, userid,
					role, firstName, lastName, userVersion);
		} catch (ClassCastException e) {
			LOGGER.error("Error while parsing user supplied token {}",
					e.getMessage());
			throw new JwtAuthenticationException("Token parse error...");
		}
	}

	/**
	 * Add given day to current date
	 * 
	 * @param currentDate
	 * @return
	 */
	private static Date calculateTokenExpireDate(Date currentDate, int expiryDay) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(currentDate);
		// minus number would decrement the days
		cal.add(Calendar.DATE, expiryDay);
		return cal.getTime();
	}

	/**
	 * Token type
	 * 
	 * @author user
	 *
	 */
	public enum TokenType {
		NORMAL_TOKEN(AppConstants.ACCESS_TOKEN_VALIDITY), REFRESH_TOKEN(
				AppConstants.REMEMBER_ME_TOKEN_VALIDITY);

		private TokenType(int expiryDay) {
			this.tokenValidTill = expiryDay;
		}

		private int tokenValidTill;

		public int getExpiryDays() {
			return tokenValidTill;
		}
	}
}
