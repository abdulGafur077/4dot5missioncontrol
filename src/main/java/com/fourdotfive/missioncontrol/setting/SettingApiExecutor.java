package com.fourdotfive.missioncontrol.setting;

import com.fourdotfive.missioncontrol.pojo.setting.Setting;

public interface SettingApiExecutor {

	Setting getFourDotFiveSettings();

	Setting getStaffingOrCorpSettings(String compId, boolean escalateIfNotFound);

	Setting getClientOrBuSettings(String parentCompId, String childCompId, boolean escalateIfNotFound);

	Setting getParentSettings(String compId, boolean escalateIfNotFound);
	
	Setting saveSettings(Setting setting,String compId);
	
	void deleteSettings(String compId);
}
