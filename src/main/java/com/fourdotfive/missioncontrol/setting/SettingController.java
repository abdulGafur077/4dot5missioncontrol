package com.fourdotfive.missioncontrol.setting;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.fourdotfive.missioncontrol.common.AppConstants;
import com.fourdotfive.missioncontrol.common.CommonUtil;
import com.fourdotfive.missioncontrol.common.TokenDetails;
import com.fourdotfive.missioncontrol.dtos.setting.SettingDto;

/**
 * @author abhishek
 *
 */
@RestController
@RequestMapping("/api/setting")
public class SettingController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SettingController.class);

	@Autowired
	private SettingService settingService;

	/**
	 * To Get 4dot5 Settings, this method is called.
	 * 
	 * @return SettingDto
	 */
	@RequestMapping(value = "/getfordotfivesettings", method = RequestMethod.GET)
	public ResponseEntity<SettingDto> getFourDotFiveSettings() {
		LOGGER.debug("Get settings for the host company");
		SettingDto response = settingService.getFourDotFiveSettings();
		return new ResponseEntity<SettingDto>(response, HttpStatus.OK);
	}

	/**
	 *
	 * To Get Staffing or Corporate Settings, this method is called.
	 * 
	 * @return SettingDto
	 */
	@RequestMapping(value = "/getstaffingorcorpsettings/{compId}", method = RequestMethod.GET)
	public ResponseEntity<SettingDto> getStaffingOrCorpSettings(@PathVariable(value = "compId") String compId) {
		LOGGER.debug("Get settings for Staffing or Corporate with company ID {}",compId);
		SettingDto response = settingService.getStaffingOrCorpSettings(compId, true);
		return new ResponseEntity<SettingDto>(response, HttpStatus.OK);
	}

	/**
	 * To Get Client or BU Settings, this method is called.
	 * 
	 * @return SettingDto
	 */
	@RequestMapping(value = "/getclientorbusettings/{parentCompId}/{childCompId}", method = RequestMethod.GET)
	public ResponseEntity<SettingDto> getClientOrBuSettings(@PathVariable(value = "parentCompId") String parentCompId,
			@PathVariable(value = "childCompId") String childCompId) {
		LOGGER.debug("Get settings for Client or BU with parent company ID {} child company ID {}",parentCompId,childCompId);
		SettingDto response = settingService.getClientOrBuSettings(parentCompId, childCompId, true);
		return new ResponseEntity<SettingDto>(response, HttpStatus.OK);
	}
	
	

	/**
	 * To Get parent company Settings, this method is called.
	 * 
	 * @return SettingDto
	 */
	@RequestMapping(value = "/getparentsettings/{compid}", method = RequestMethod.GET)
	public ResponseEntity<SettingDto> getParentSettings(@PathVariable(value = "compid") String compId) {
		SettingDto response = settingService.getParentSettings(compId);
		return new ResponseEntity<SettingDto>(response, HttpStatus.OK);
	}
	
	/**
	 * Create settings for a company
	 * 
	 * @param SettingDto
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/savesettings/{compId}", method = RequestMethod.PUT)
	public ResponseEntity<SettingDto> saveSettings(
			@PathVariable(value = "compId") String compId,
			@RequestParam(value = "clientOrBuId", required = false) String clientOrBuId,
			@RequestBody SettingDto dto, HttpServletRequest request) {
		LOGGER.debug("Save settings for the company with company ID {} ",compId);
		final String authHeader = request.getHeader(AppConstants.AUTHORIZATION);
		TokenDetails tokenDetails = CommonUtil.getTokenDetails(authHeader);
		SettingDto response = settingService.saveSettings(dto,compId,tokenDetails, clientOrBuId);

		return new ResponseEntity<SettingDto>(response, HttpStatus.OK);
	}
	
	/**
	 * To Delete child company, this method is called.
	 * 
	 * @return String
	 * 
	 */
	@RequestMapping(value = "/deletestaffingorcorpsettings/{compid}", method = RequestMethod.DELETE)
	public ResponseEntity<String> deletestaffingorcorpsettings(
			@PathVariable(value = "compid") String compId,
			HttpServletRequest request) {
		LOGGER.debug("Delete settings for the staffing or corporate company with company ID {} ",compId);
		settingService.deletestaffingorcorpsettings(compId);

		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	@RequestMapping(value = "/deleteclientorbusettings/{parentcompid}/{childcompid}", method = RequestMethod.DELETE)
	public ResponseEntity<String> deleteclientorbusettings(
			@PathVariable(value = "parentcompid") String parentcompid,@PathVariable(value = "childcompid") String childcompid,
			HttpServletRequest request) {
		LOGGER.debug("Delete settings for the client or bu company with company ID {} ",childcompid);
		settingService.deleteclientorbusettings(parentcompid,childcompid);

		return new ResponseEntity<String>(HttpStatus.OK);
	}

	@RequestMapping(value = "/savesettingsforclientorbu/{companyId}/{clientOrBuId}", method = RequestMethod.PUT)
	public ResponseEntity<SettingDto> saveSettingsForClientOrBU(@PathVariable(value = "companyId") String companyId,
																@PathVariable(value = "clientOrBuId") String clientOrBuId,
																@RequestBody SettingDto settingDto, HttpServletRequest request) {
		final String authHeader = request.getHeader(AppConstants.AUTHORIZATION);
		TokenDetails tokenDetails = CommonUtil.getTokenDetails(authHeader);
		SettingDto response = settingService.saveSettings(settingDto, companyId, tokenDetails, clientOrBuId);

		return new ResponseEntity<SettingDto>(response, HttpStatus.OK);
	}
}
