package com.fourdotfive.missioncontrol.setting;

import java.util.LinkedHashMap;
import java.util.List;

import com.fourdotfive.missioncontrol.dtos.setting.*;
import com.fourdotfive.missioncontrol.pojo.setting.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import com.fourdotfive.missioncontrol.licensepreference.LicensePrefEnum;

@Service
public class SettingDtoToSetting implements Converter<SettingDto, Setting>{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SettingDtoToSetting.class);

	@Override
	public Setting convert(SettingDto dto) {
		LOGGER.debug("Converting SettingDto to Setting");
		
		Setting setting = new Setting();
		
		setting.setLicensePreferenceSetting(convertDtoToLicensePreferenceSetting(dto.getLicensePreferenceSetting()));
		setting.setPerformanceSetting(convertDtoToPerformanceSetting(dto.getPerformanceSetting()));
		setting.setUserRoleSetting(convertDtoToUserRoleSetting(dto.getUserRoleSetting()));
		
		return setting;
	}


	private LinkedHashMap<UserRoleEnum, UserRoleSetting> convertDtoToUserRoleSetting(
			List<UserRoleSettingDto> userRoleSettingList) {

		LinkedHashMap<UserRoleEnum , UserRoleSetting> userRoleMap = new LinkedHashMap<UserRoleEnum, UserRoleSetting>();
		for (UserRoleSettingDto dto : userRoleSettingList) {
			UserRoleSetting userRoleSetting = new UserRoleSetting();
			userRoleSetting.setId(dto.getId());
			userRoleSetting.setName(dto.getName());
			userRoleSetting.setUserRoleEnum(dto.getUserRoleEnum());


			LinkedHashMap<NotifyEventEnum, NotifyEvent> notifyEventMap = new LinkedHashMap<NotifyEventEnum, NotifyEvent>();
			for(NotifyEventDto eventDto : dto.getNotifyEvents()){
				NotifyEvent event = new NotifyEvent();
				event.setName(eventDto.getName());
				event.setDetectOn(eventDto.isDetectOn());
				event.setDetectOnReadOnly(eventDto.isDetectOnReadOnly());
				event.setNotifyEventEnum(eventDto.getNotifyEventEnum());

				LinkedHashMap<NotifyEnum, Notify> notifyMap = new LinkedHashMap<NotifyEnum, Notify>();
				for(NotifyDto notifyDto : eventDto.getNotifyOn()){
					if(notifyDto == null){
						continue;
					}
					Notify notify = new Notify();
					notify.setName(notifyDto.getName());
					notify.setNotifyEnum(notifyDto.getNotifyEnum());
					notify.setEnabled(notifyDto.isEnabled());
					notify.setReadOnly(notifyDto.isReadOnly());
					notifyMap.put(notifyDto.getNotifyEnum(), notify);
				}
				event.setNotifyOn(notifyMap);
				notifyEventMap.put(eventDto.getNotifyEventEnum(), event);
			}

			userRoleSetting.setNotifyEvents(notifyEventMap);

			userRoleMap.put(dto.getUserRoleEnum(), userRoleSetting);
		}
		return userRoleMap;
	}


	private LinkedHashMap<PerformanceEnum, PerformanceSetting> convertDtoToPerformanceSetting(
			List<PerformanceSettingDto> performanceSettingList) {
		
		LinkedHashMap<PerformanceEnum, PerformanceSetting> performanceMap = new LinkedHashMap<PerformanceEnum, PerformanceSetting>();
		for (PerformanceSettingDto dto : performanceSettingList) {
			PerformanceSetting performanceSetting = new PerformanceSetting();
			performanceSetting.setId(dto.getId());
			performanceSetting.setName(dto.getName());
			performanceSetting.setPerformanceEnum(dto.getPerformanceEnum());
			performanceSetting.setMaxLimit(dto.getMaxLimit());
			performanceSetting.setMaxValue(dto.getMaxValue());
			performanceSetting.setMinValue(dto.getMinValue());
			
			LinkedHashMap<NotifyEventEnum, NotifyEvent> notifyEventMap = new LinkedHashMap<NotifyEventEnum, NotifyEvent>();
			for(NotifyEventDto eventDto : dto.getNotifyEvents()){
				NotifyEvent event = new NotifyEvent();
				event.setName(eventDto.getName());
				event.setDetectOn(eventDto.isDetectOn());
				event.setDetectOnReadOnly(eventDto.isDetectOnReadOnly());
				event.setNotifyEventEnum(eventDto.getNotifyEventEnum());
				
				LinkedHashMap<NotifyEnum, Notify> notifyMap = new LinkedHashMap<NotifyEnum, Notify>();
				for(NotifyDto notifyDto : eventDto.getNotifyOn()){
					if(notifyDto == null){
						continue;
					}
					Notify notify = new Notify();
					notify.setName(notifyDto.getName());
					notify.setNotifyEnum(notifyDto.getNotifyEnum());
					notify.setEnabled(notifyDto.isEnabled());
					notify.setReadOnly(notifyDto.isReadOnly());
					notifyMap.put(notifyDto.getNotifyEnum(), notify);
				}
				event.setNotifyOn(notifyMap);
				notifyEventMap.put(eventDto.getNotifyEventEnum(), event);
			}
			
			performanceSetting.setNotifyEvents(notifyEventMap);
			
			performanceMap.put(dto.getPerformanceEnum(), performanceSetting);
		}
		return performanceMap;
	}

	private LinkedHashMap<LicensePrefEnum,LicensePreferenceSetting> convertDtoToLicensePreferenceSetting(List<LicensePreferenceSettingDto> licensePreferenceSettingDtos){
		LinkedHashMap<LicensePrefEnum, LicensePreferenceSetting> licensePrefMap = new LinkedHashMap<>();
		
		for (LicensePreferenceSettingDto dto : licensePreferenceSettingDtos) {
			LicensePreferenceSetting setting = new LicensePreferenceSetting();
			setting.setId(dto.getId());
			setting.setLicensePreferenceEnum(dto.getLicensePreferenceEnum());
			setting.setScore(dto.getScore());
			setting.setTimeToComplete(dto.getTimeToComplete());
			setting.setReminderInterval(dto.getReminderInterval());
			setting.setYear(dto.getYear());
			setting.setMonth(dto.getMonth());
			setting.setDay(dto.getDay());
			setting.setMaxLimitForTimeToComplete(dto.getMaxLimitForTimeToComplete());
			
			LinkedHashMap<NotifyEventEnum, NotifyEvent> notifyEventMap = new LinkedHashMap<NotifyEventEnum, NotifyEvent>();
			for(NotifyEventDto eventDto : dto.getNotifyEvents()){
				NotifyEvent event = new NotifyEvent();
				event.setName(eventDto.getName());
				event.setDetectOn(eventDto.isDetectOn());
				event.setDetectOnReadOnly(eventDto.isDetectOnReadOnly());
				event.setNotifyEventEnum(eventDto.getNotifyEventEnum());
				
				LinkedHashMap<NotifyEnum, Notify> notifyMap = new LinkedHashMap<NotifyEnum, Notify>();
				for(NotifyDto notifyDto : eventDto.getNotifyOn()){
					if(notifyDto == null){
						continue;
					}
					Notify notify = new Notify();
					notify.setName(notifyDto.getName());
					notify.setNotifyEnum(notifyDto.getNotifyEnum());
					notify.setEnabled(notifyDto.isEnabled());
					notify.setReadOnly(notifyDto.isReadOnly());
					notifyMap.put(notifyDto.getNotifyEnum(), notify);
				}
				event.setNotifyOn(notifyMap);
				notifyEventMap.put(eventDto.getNotifyEventEnum(), event);
			}
			
			setting.setNotifyEvents(notifyEventMap);
			
			licensePrefMap.put(dto.getLicensePreferenceEnum(), setting);
		}
		return licensePrefMap;
	}
}
