package com.fourdotfive.missioncontrol.setting;

public class SettingException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2302978329108845797L;
	
	private String message;

	public String getMessage() {
		return message;
	}

	public SettingException() {
	}

	public SettingException(String message, Object... args) {
		this.message = String.format(message, args); 
	}
	
	@Override
	public String toString() {
		return "SettingException [message=" + message + "]";
	}

}
