package com.fourdotfive.missioncontrol.setting;

import com.fourdotfive.missioncontrol.common.TokenDetails;
import com.fourdotfive.missioncontrol.dtos.setting.SettingDto;

public interface SettingService {
	
	/**
	 * To get the 4dot5 company settings.
	 * 
	 * @return response from platform
	 */
	SettingDto getFourDotFiveSettings();
	
	/**
	 * To get Staffing or Corporate company settings for the given ID.
	 * 
	 * @return response from platform
	 */
	SettingDto getStaffingOrCorpSettings(String compId, boolean escalateIfNotFound);
	
	/**
	 * To get Client or BU settings for the given ID.
	 * 
	 * @return response from platform
	 */
	SettingDto getClientOrBuSettings(String parentCompId, String childCompId, boolean escalateIfNotFound);
	
	/**
	 * To get parent settings for the given company ID.
	 * 
	 * @return response from platform
	 */
	SettingDto getParentSettings(String compId);
	
	/**
	 * To set the settings for the given company
	 * @param tokenDetails 
	 * 
	 * @return response from platform
	 */
	SettingDto saveSettings(SettingDto settingDto,String compId, TokenDetails tokenDetails, String clientOrBuId);
	
	/**
	 * To delete the settings for the given company ID
	 * 
	 * @return response from platform
	 */
	void deletestaffingorcorpsettings(String compId);

	void deleteclientorbusettings(String parentCompId, String childCompId);

	/**
	 * This method is used to check if the client has its own settings or it is using global settings.
	 * @param parentCompId
	 * @param childCompId
	 * @return
	 */
	boolean getClientsOrBUWithoutGlobalSetting(String parentCompId, String childCompId);

	SettingDto saveSettingsForClientOrBU(SettingDto settingDto, String companyId, TokenDetails tokenDetails, String clientOrBuId);


}
