package com.fourdotfive.missioncontrol.setting;

import com.fourdotfive.missioncontrol.dtos.vendor.CorporateVendorDto;
import com.fourdotfive.missioncontrol.dtos.vendor.CorporateVendorMinDto;
import com.fourdotfive.missioncontrol.user.UserService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fourdotfive.missioncontrol.common.TokenDetails;
import com.fourdotfive.missioncontrol.company.CompanyService;
import com.fourdotfive.missioncontrol.dtos.setting.SettingDto;
import com.fourdotfive.missioncontrol.licensepreference.LicensePrefDtoList;
import com.fourdotfive.missioncontrol.licensepreference.LicensePrefEvaluator;
import com.fourdotfive.missioncontrol.licensepreference.LicensePrefService;
import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.company.CompanyType;
import com.fourdotfive.missioncontrol.pojo.setting.Setting;

@Service
public class SettingServiceImp implements SettingService {
  
	private static final Logger LOGGER = LoggerFactory.getLogger(SettingServiceImp.class);

	@Autowired
	private SettingApiExecutor apiExecutor;

	@Autowired
	private LicensePrefService licensePrefService;

	@Autowired
	private SettingToSettingDto settingToDto;

	@Autowired
	private SettingDtoToSetting dtoToSetting;
	
	@Autowired
	private LicensePrefEvaluator licensePrefEvaluator;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private UserService userService;
	
	/**
	 * This method is used to get the host company settings, if the setting is
	 * not available then a default settings will be sent as response
	 */
	@Override
	public SettingDto getFourDotFiveSettings() {
		LicensePrefDtoList licensePrefDtoList = licensePrefService.getHostLicensePreferences();
		
		LicensePrefDtoList newDtoList = licensePrefEvaluator.evaluate(licensePrefDtoList, licensePrefDtoList);
		
		Setting setting = apiExecutor.getFourDotFiveSettings();
		SettingDto settingDto = null;
		if (setting != null) {
			settingDto = settingToDto.convert(setting, null,newDtoList, CompanyType.Host);
		}
		return settingDto;
	}

	/**
	 * This method is used to get Staffing or Corporate settings and converted
	 * to DTO while returning
	 */
	@Override
	public SettingDto getStaffingOrCorpSettings(String compId, boolean escalateIfNotFound) {
		LOGGER.debug("Get settings for Staffing or Corporate with company ID {} in service", compId);
		Setting setting = apiExecutor.getStaffingOrCorpSettings(compId, escalateIfNotFound);
		LicensePrefDtoList licensePrefDtoList = licensePrefService.getStaffOrCorpLicensePref(compId);
		SettingDto hostSetting =  getFourDotFiveSettings();
		SettingDto settingDto = null;
		if (setting != null) {
			settingDto = settingToDto.convert(setting, hostSetting, licensePrefDtoList,
					CompanyType.StaffingOrCorporate);
		}
		return settingDto;
	}
	
	/**
	 * This method is used to get Client or BU settings using the parent company
	 * ID and converted to DTO while returning
	 */
	@Override
	public SettingDto getClientOrBuSettings(String parentCompId, String childCompId, boolean escalateIfNotFound) {
		LOGGER.debug("Get settings for Client or BU with parent company ID {} and child company ID {} in service",
				parentCompId, childCompId);

		Setting setting = null;
		if (StringUtils.isNotEmpty(childCompId)) {
			Company client = companyService.getCompany(childCompId);
			if (client == null)
				throw new IllegalArgumentException("No clinet or Bu is found for the given client Id.");
			if (client.getCompanyType().equals(CompanyType.Corporation)) {
				CorporateVendorMinDto corporateVendorMinDto = companyService.getCorporateVendorRelationship(childCompId, parentCompId);
				if (corporateVendorMinDto != null && corporateVendorMinDto.getClientSettings() != null) {
					setting = corporateVendorMinDto.getClientSettings();
				}
			}
		}
		if (setting == null) {
			setting = apiExecutor.getClientOrBuSettings(parentCompId, childCompId, escalateIfNotFound);
		}
		if (setting != null) {
			LicensePrefDtoList licensePrefDtoList = licensePrefService.getClientOrBuLicensePref(parentCompId, childCompId);
			SettingDto parentSetting = getStaffingOrCorpSettings(parentCompId, true);
			SettingDto settingDto = settingToDto.convert(setting, parentSetting, licensePrefDtoList, CompanyType.ClientOrBU);
			return settingDto;
		}
		return null;
	}
	
	
	@Override
	public boolean getClientsOrBUWithoutGlobalSetting(String parentCompId, String childCompId) {
		Setting setting = apiExecutor.getClientOrBuSettings(parentCompId, childCompId, false);
		if(setting == null){
			return true;
		}
		return false;
	}

	@Override
	public SettingDto saveSettingsForClientOrBU(SettingDto settingDto, String companyId, TokenDetails tokenDetails, String clientOrBuId) {
		Setting setting = dtoToSetting.convert(settingDto);
		if (setting.getId() == null) {
			setting.setCreatedBy(tokenDetails.getFirstName());
			setting.setLastModifiedBy(null);
		} else {
			setting.setCreatedBy(null);
			setting.setLastModifiedBy(tokenDetails.getFirstName());
		}
		setting = apiExecutor.saveSettings(setting, clientOrBuId);
		Company company = companyService.getCompany(clientOrBuId);
		LicensePrefDtoList licensePrefDtoList = licensePrefService.getStaffOrCorpLicensePref(clientOrBuId);
		SettingDto parentSetting = getParentSettings(companyId);
		settingDto = settingToDto.convert(setting, parentSetting, licensePrefDtoList, company.getCompanyType());
		return settingDto;
	}


	@Override
	public SettingDto getParentSettings(String compId) {
		Setting setting = apiExecutor.getStaffingOrCorpSettings(compId, true);
		LicensePrefDtoList licensePrefDtoList = licensePrefService.getStaffOrCorpLicensePref(compId);
		
		SettingDto parentSetting = getFourDotFiveSettings();
		SettingDto settingDto = settingToDto.convert(setting, parentSetting,licensePrefDtoList,CompanyType.StaffingOrCorporate);
		settingDto.setGlobal(true);
		return settingDto;
	}

	/**
	 * This method is used to save company settings
	 */
	@Override
	public SettingDto saveSettings(SettingDto settingDto, String compId, TokenDetails tokenDetails, String clientOrBuId) {
		Setting setting = dtoToSetting.convert(settingDto);
		if (setting.getId() == null) {
			setting.setCreatedBy(tokenDetails.getFirstName());
			setting.setLastModifiedBy(null);
		} else {
			setting.setCreatedBy(null);
			setting.setLastModifiedBy(tokenDetails.getFirstName());
		}
		Company company = null;
		LicensePrefDtoList licensePrefDtoList = null;
		SettingDto parentSetting = null;
		String loggedInUserId = userService.getCurrentLoggedInUser().getId();
		if (StringUtils.isNotEmpty(clientOrBuId)) {
			Company client = companyService.getCompany(clientOrBuId);
			if (client == null)
				throw new IllegalArgumentException("No clinet or Bu is found for the given client Id.");
			if (client.getCompanyType().equals(CompanyType.Corporation)) {
				CorporateVendorMinDto corporateVendorMinDto = companyService.getCorporateVendorRelationship(clientOrBuId,compId);
				if (corporateVendorMinDto == null)
					corporateVendorMinDto = companyService.setCorporateVendorRelationship(compId, clientOrBuId);
				corporateVendorMinDto.setClientSettings(setting);
				CorporateVendorDto returnedCorporateVendorDto = companyService.saveCorporateVendorRelationship(new CorporateVendorDto(corporateVendorMinDto), loggedInUserId);
				LicensePrefDtoList clientOrBuLicensePref = licensePrefService.getClientOrBuLicensePref(compId, clientOrBuId);
				SettingDto parentSettings = getStaffingOrCorpSettings(compId, true);
				return settingToDto.convert(returnedCorporateVendorDto.getClientSettings(), parentSettings, clientOrBuLicensePref, CompanyType.ClientOrBU);
			}
			apiExecutor.saveSettings(setting, clientOrBuId);
			return getClientOrBuSettings(compId, clientOrBuId, true);
		} else {
			setting = apiExecutor.saveSettings(setting, compId);
			company = companyService.getCompany(compId);
			licensePrefDtoList = licensePrefService.getStaffOrCorpLicensePref(compId);
			parentSetting = getFourDotFiveSettings();
		}
		settingDto = settingToDto.convert(setting, parentSetting, licensePrefDtoList, company.getCompanyType());
		return settingDto;
	}

	/**
	 * This method is used to delete company settings
	 */
	@Override
	public void deletestaffingorcorpsettings(String compId) {
		
		LOGGER.debug("Deleting staffing or corporate settings {}",compId);
			SettingDto setting = getStaffingOrCorpSettings(compId, false);
			if(setting != null || (setting != null && setting.getId() != null)){
				apiExecutor.deleteSettings(compId);
			}
			return;
	}
	
	@Override
	public void deleteclientorbusettings(String parentCompId, String childCompId) {
			LOGGER.debug("Deleting client or bu settings {}",childCompId);
			SettingDto setting = getClientOrBuSettings(parentCompId, childCompId, false);
			if(setting != null || (setting != null && setting.getId() != null)){
				apiExecutor.deleteSettings(childCompId);
			}
			return;
	}
}
