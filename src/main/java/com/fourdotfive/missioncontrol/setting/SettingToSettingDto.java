package com.fourdotfive.missioncontrol.setting;

import com.fourdotfive.missioncontrol.common.AccessUtil;
import com.fourdotfive.missioncontrol.common.AppConstants;
import com.fourdotfive.missioncontrol.common.CommonUtil;
import com.fourdotfive.missioncontrol.common.Messages;
import com.fourdotfive.missioncontrol.dtos.setting.*;
import com.fourdotfive.missioncontrol.licensepreference.LicensePrefDetails;
import com.fourdotfive.missioncontrol.licensepreference.LicensePrefDto;
import com.fourdotfive.missioncontrol.licensepreference.LicensePrefDtoList;
import com.fourdotfive.missioncontrol.licensepreference.LicensePrefEnum;
import com.fourdotfive.missioncontrol.pojo.company.CompanyType;
import com.fourdotfive.missioncontrol.pojo.notificationpreference.NotificationPreferenceEnum;
import com.fourdotfive.missioncontrol.pojo.setting.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class SettingToSettingDto implements Converter<Setting, SettingDto> {

	private static final Logger LOGGER = LoggerFactory.getLogger(SettingToSettingDto.class);

	@Autowired
	private SettingDtoToSetting settingDtoToSetting;

	@Autowired
	private AccessUtil accessUtil;

	private static final List<NotifyEventEnum> TECH_ASSESSMENT_EVENTs = Arrays.asList(NotifyEventEnum.TECH_ASSESSMENT_COMPLETED,
            NotifyEventEnum.TECH_ASSESSMENT_REMINDER, NotifyEventEnum.TECH_ASSESSMENT_CANCELED, NotifyEventEnum.TECH_ASSESSMENT_EXPIRED,
			NotifyEventEnum.TECH_ASSESSMENT_IN_PROGRESS, NotifyEventEnum.TECH_ASSESSMENT_LEFT, NotifyEventEnum.TECH_ASSESSMENT_SENT,
			NotifyEventEnum.TECH_ASSESSMENT_TERMINATED, NotifyEventEnum.TECH_ASSESSMENT_SUSPENDED);

	private static final List<NotifyEventEnum> VALUE_ASSESSMENT_EVENTs = Arrays.asList(NotifyEventEnum.VALUE_ASSESSMENT_COMPLETED,
            NotifyEventEnum.VALUE_ASSESSMENT_REMINDER, NotifyEventEnum.VALUE_ASSESSMENT_SENT, NotifyEventEnum.VALUE_ASSESSMENT_REMINDER,
			NotifyEventEnum.VALUE_ASSESSMENT_IN_PROGRESS, NotifyEventEnum.VALUE_ASSESSMENT_CANCELED,NotifyEventEnum.VALUE_ASSESSMENT_TERMINATED,
			NotifyEventEnum.VALUE_ASSESSMENT_EXPIRED, NotifyEventEnum.VALUE_ASSESSMENT_LEFT, NotifyEventEnum.VALUE_ASSESSMENT_SUSPENDED);

	private static final List<NotifyEventEnum> PHONE_SCREEN_EVENTs = Arrays.asList(NotifyEventEnum.PHONE_SCREEN_CANCELED,
			NotifyEventEnum.PHONE_SCREEN_COMPLETED, NotifyEventEnum.PHONE_SCREEN_OVERDUE, NotifyEventEnum.PHONE_SCREEN_SCHEDULED,
			NotifyEventEnum.PHONE_SCREEN_RE_SCHEDULED, NotifyEventEnum.PHONE_SCREEN_IN_PROGRESS);

	private static final List<NotifyEventEnum> INTERVIEW_EVENTs = Arrays.asList(NotifyEventEnum.INTERVIEW_CANCELED,
			NotifyEventEnum.INTERVIEW_COMPLETED, NotifyEventEnum.INTERVIEW_OVERDUE, NotifyEventEnum.INTERVIEW_SCHEDULED,
			NotifyEventEnum.INTERVIEW_RE_SCHEDULED, NotifyEventEnum.INTERVIEW_IN_PROGRESS);

	public SettingDto convert(Setting setting, SettingDto parentSetting, LicensePrefDtoList licensePrefDtoList, CompanyType companyType) {
		LOGGER.debug("Converting Setting to SettingDto");

		SettingDto settingDto = new SettingDto();

		settingDto.setGlobal(setting.isGlobal());

		orderSettings(setting);

		// Parent settings will be null for host company
		setting = validateNotifyForLicensePref(setting, parentSetting, companyType);
		setting = validateNotifyForPerformance(setting, parentSetting, companyType);
		setting = validateNotifyForUserRole(setting,parentSetting,companyType);

		List<LicensePreferenceSettingDto> lpList = convertLicensePrefSettingToDto(setting.getLicensePreferenceSetting(), licensePrefDtoList);
		List<PerformanceSettingDto> psList = convertPerformanceSettingDto(setting.getPerformanceSetting());
		List<UserRoleSettingDto> usList = convertUserRoleSettingDto(setting.getUserRoleSetting());

		settingDto.setLicensePreferenceSetting(formatNameForLicensePrefSetting(lpList));
		settingDto.setPerformanceSetting(formatNamePerformceSetting(psList));
		settingDto.setUserRoleSetting(formatNameUserRoleSetting(usList));
		settingDto.setId(setting.getId());

		return settingDto;
	}

	/**Order settings does not do anything functionally, but null is added to Time to Source and and Time to Fill
	 maps so that requisition card comes towards the end.
	 Candidate Card , Candidate Job Card and Requisition Card should appear in this same order. Since Time to Source and Time to Fill does not
	 have candidate card and candidate job card we are adding null in that place **/

	private Setting orderSettings(Setting setting) {

		LinkedHashMap<NotifyEnum, Notify> newmap = new LinkedHashMap<NotifyEnum, Notify>();
		newmap.put(NotifyEnum.CANDIDATE_CARD, null);
		newmap.put(NotifyEnum.CANDIDATE_JOB_CARD, null);
		Notify  temp = setting.getPerformanceSetting().get(PerformanceEnum.TIME_TO_SOURCE).getNotifyEvents()
				.get(NotifyEventEnum.TIME_TO_SOURCE).getNotifyOn().get(NotifyEnum.REQUISITION_CARD);
		newmap.put(NotifyEnum.REQUISITION_CARD, temp);
		setting.getPerformanceSetting().get(PerformanceEnum.TIME_TO_SOURCE).getNotifyEvents().get(NotifyEventEnum.TIME_TO_SOURCE).setNotifyOn(newmap);

		newmap = new LinkedHashMap<NotifyEnum, Notify>();
		newmap.put(NotifyEnum.CANDIDATE_CARD, null);
		newmap.put(NotifyEnum.CANDIDATE_JOB_CARD, null);
		temp = setting.getPerformanceSetting().get(PerformanceEnum.TIME_TO_FILL).getNotifyEvents()
				.get(NotifyEventEnum.TIME_TO_FILL).getNotifyOn().get(NotifyEnum.REQUISITION_CARD);
		newmap.put(NotifyEnum.REQUISITION_CARD, temp);
		setting.getPerformanceSetting().get(PerformanceEnum.TIME_TO_FILL).getNotifyEvents().get(NotifyEventEnum.TIME_TO_FILL).setNotifyOn(newmap);

		newmap = new LinkedHashMap<NotifyEnum, Notify>();
		temp = setting.getLicensePreferenceSetting().get(LicensePrefEnum.FOUR_DOT_FIVE_INTELLIGENCE).getNotifyEvents()
				.get(NotifyEventEnum.CANDIDATE_NO_LONGER_MATCHES).getNotifyOn().get(NotifyEnum.CANDIDATE_CARD);
		newmap.put(NotifyEnum.CANDIDATE_CARD, temp);
		newmap.put(NotifyEnum.CANDIDATE_JOB_CARD, null);
		temp = setting.getLicensePreferenceSetting().get(LicensePrefEnum.FOUR_DOT_FIVE_INTELLIGENCE).getNotifyEvents()
				.get(NotifyEventEnum.CANDIDATE_NO_LONGER_MATCHES).getNotifyOn().get(NotifyEnum.REQUISITION_CARD);
		newmap.put(NotifyEnum.REQUISITION_CARD, temp);
		setting.getLicensePreferenceSetting().get(LicensePrefEnum.FOUR_DOT_FIVE_INTELLIGENCE).getNotifyEvents().get(NotifyEventEnum.CANDIDATE_NO_LONGER_MATCHES).setNotifyOn(newmap);


		return setting;
	}

	private List<UserRoleSettingDto> convertUserRoleSettingDto(
			LinkedHashMap<UserRoleEnum, UserRoleSetting> userRoleSetting) {

		List<UserRoleSettingDto> userRoleDtoList = new ArrayList<UserRoleSettingDto>();

		for (Map.Entry<UserRoleEnum, UserRoleSetting> setting : userRoleSetting.entrySet()) {

			UserRoleSetting value = setting.getValue();

			List<NotifyEventDto> notifyEventList = new ArrayList<NotifyEventDto>();
			for(Map.Entry<NotifyEventEnum, NotifyEvent> notifyEvent : setting.getValue().getNotifyEvents().entrySet()){
				NotifyEventDto event = new NotifyEventDto();
				event.setName(notifyEvent.getValue().getName());
				event.setDetectOn(notifyEvent.getValue().isDetectOn());
				event.setDetectOnReadOnly(notifyEvent.getValue().isDetectOnReadOnly());
				event.setNotifyEventEnum(notifyEvent.getValue().getNotifyEventEnum());


				List<NotifyDto> notifyList = new ArrayList<NotifyDto>();
				for(Map.Entry<NotifyEnum, Notify> notifyItem : notifyEvent.getValue().getNotifyOn().entrySet()){
					NotifyDto notify = new NotifyDto();
					if(notifyItem.getValue() == null){
						notifyList.add(null);
						continue;
					}
					notify.setName(notifyItem.getValue().getName());
					notify.setNotifyEnum(notifyItem.getValue().getNotifyEnum());
					notify.setEnabled(notifyItem.getValue().isEnabled());
					notify.setReadOnly(notifyItem.getValue().isReadOnly());
					notifyList.add(notify);
				}
				event.setNotifyOn(notifyList);
				notifyEventList.add(event);
			}
			UserRoleSettingDto dto = new UserRoleSettingDto(value.getId(),
					value.getUserRoleEnum(),value.getName(),notifyEventList);

			userRoleDtoList.add(dto);
		}

		return userRoleDtoList;
	}


	private List<PerformanceSettingDto> convertPerformanceSettingDto(
			LinkedHashMap<PerformanceEnum, PerformanceSetting> performanceSetting) {

		List<PerformanceSettingDto> performanceDtoList = new ArrayList<PerformanceSettingDto>();

		for (Map.Entry<PerformanceEnum, PerformanceSetting> setting : performanceSetting.entrySet()) {

			PerformanceSetting value = setting.getValue();

			List<NotifyEventDto> notifyEventList = new ArrayList<NotifyEventDto>();
			for(Map.Entry<NotifyEventEnum, NotifyEvent> notifyEvent : setting.getValue().getNotifyEvents().entrySet()){
				NotifyEventDto event = new NotifyEventDto();
				event.setName(notifyEvent.getValue().getName());
				event.setDetectOn(notifyEvent.getValue().isDetectOn());
				event.setDetectOnReadOnly(notifyEvent.getValue().isDetectOnReadOnly());
				event.setNotifyEventEnum(notifyEvent.getValue().getNotifyEventEnum());


				List<NotifyDto> notifyList = new ArrayList<NotifyDto>();
				for(Map.Entry<NotifyEnum, Notify> notifyItem : notifyEvent.getValue().getNotifyOn().entrySet()){
					NotifyDto notify = new NotifyDto();
					if(notifyItem.getValue() == null){
						notifyList.add(null);
						continue;
					}
					notify.setName(notifyItem.getValue().getName());
					notify.setNotifyEnum(notifyItem.getValue().getNotifyEnum());
					notify.setEnabled(notifyItem.getValue().isEnabled());
					notify.setReadOnly(notifyItem.getValue().isReadOnly());
					notifyList.add(notify);
				}
				event.setNotifyOn(notifyList);
				notifyEventList.add(event);
			}
			PerformanceSettingDto dto = new PerformanceSettingDto(value.getId(),
					value.getPerformanceEnum(),value.getName(),value.getMinValue(), value.getMaxValue(),
					value.getMaxLimit(),notifyEventList);

			performanceDtoList.add(dto);
		}

		return performanceDtoList;
	}

	private List<LicensePreferenceSettingDto> convertLicensePrefSettingToDto(
			LinkedHashMap<LicensePrefEnum,LicensePreferenceSetting> licensePreferenceSettings, LicensePrefDtoList licensePrefDtoList) {

		List<LicensePreferenceSettingDto> list = new ArrayList<LicensePreferenceSettingDto>();

		for (Map.Entry<LicensePrefEnum, LicensePreferenceSetting> setting : licensePreferenceSettings.entrySet()) {

			LicensePreferenceSetting value = setting.getValue();

			List<NotifyEventDto> notifyEventList = new ArrayList<NotifyEventDto>();
			for(Map.Entry<NotifyEventEnum, NotifyEvent> notifyEvent : setting.getValue().getNotifyEvents().entrySet()){
				NotifyEventDto event = new NotifyEventDto();
				event.setName(notifyEvent.getValue().getName());
				event.setDetectOn(notifyEvent.getValue().isDetectOn());
				event.setDetectOnReadOnly(notifyEvent.getValue().isDetectOnReadOnly());
				event.setNotifyEventEnum(notifyEvent.getValue().getNotifyEventEnum());


				List<NotifyDto> notifyList = new ArrayList<NotifyDto>();
				for(Map.Entry<NotifyEnum, Notify> notifyItem : notifyEvent.getValue().getNotifyOn().entrySet()){
					NotifyDto notify = new NotifyDto();
					if(notifyItem.getValue() == null){
						notifyList.add(null);
						continue;
					}
					notify.setName(notifyItem.getValue().getName());
					notify.setNotifyEnum(notifyItem.getValue().getNotifyEnum());
					notify.setEnabled(notifyItem.getValue().isEnabled());
					notify.setReadOnly(notifyItem.getValue().isReadOnly());
					notifyList.add(notify);
				}
				event.setNotifyOn(notifyList);
				notifyEventList.add(event);
			}

			LicensePreferenceSettingDto dto = new LicensePreferenceSettingDto(value.getId(),
					value.getLicensePreferenceEnum(),value.getScore(),value.getTimeToComplete(),
					value.getReminderInterval(),value.getYear(),value.getMonth(),value.getDay(),
					value.getMaxLimitForTimeToComplete(),notifyEventList,null);

			List<LicensePrefDetails> prefDetails = new ArrayList<>();

			if (dto.getLicensePreferenceEnum() == LicensePrefEnum.FOUR_DOT_FIVE_INTELLIGENCE) {
				LicensePrefDto prefDto = licensePrefDtoList.getFourDotFiveIntellList();
				prefDetails = prefDto.getLicensePrefDetailsList();
				dto.setScore(value.getScore());
			} else if (dto.getLicensePreferenceEnum() == LicensePrefEnum.TECH_ASSESSMENT) {
				LicensePrefDto prefDto = licensePrefDtoList.getTechAssessmentList();
				prefDetails = prefDto.getLicensePrefDetailsList();
				dto.setScore(value.getScore());
			} else if (dto.getLicensePreferenceEnum() == LicensePrefEnum.VALUE_ASSESSMENT) {
				LicensePrefDto prefDto = licensePrefDtoList.getValueAssessmentList();
				prefDetails = prefDto.getLicensePrefDetailsList();
				dto.setScore(null);
			} else if (dto.getLicensePreferenceEnum() == LicensePrefEnum.VERIFICATION) {
				LicensePrefDto prefDto = licensePrefDtoList.getVerificationList();
				prefDetails = prefDto.getLicensePrefDetailsList();
				dto.setScore(null);
			} else if (dto.getLicensePreferenceEnum() == LicensePrefEnum.PHONE_SCREEN) {
				LicensePrefDto prefDto = licensePrefDtoList.getPhoneScreenList();
				prefDetails = prefDto.getLicensePrefDetailsList();
				dto.setScore(null);
			} else if (dto.getLicensePreferenceEnum() == LicensePrefEnum.INTERVIEW) {
				LicensePrefDto prefDto = licensePrefDtoList.getInterviewList();
				prefDetails = prefDto.getLicensePrefDetailsList();
				dto.setScore(null);
			}

			StringBuilder message = new StringBuilder();
			dto.setIsEnabled(false);
			int notAvailableCount = 0;
			int notAvailableAtTheMomentCount = 0;
			for (LicensePrefDetails licensePrefDetails : prefDetails) {
				if(dto.getLicensePreferenceEnum() == LicensePrefEnum.FOUR_DOT_FIVE_INTELLIGENCE){
					dto.setIsEnabled(true);
					break;
				}
				if (licensePrefDetails.isEnabled() && licensePrefDetails.isSelected()) {
					dto.setIsEnabled(true);
				} else {
					if(dto.getIsEnabled()!= true){
						dto.setIsEnabled(false);
					}
					if (licensePrefDetails.getMessage() != null) {
						if(licensePrefDetails.getMsgVal() == 1)
						{
							notAvailableCount ++;
						}else if(licensePrefDetails.getMsgVal() == 2){
							notAvailableAtTheMomentCount++;
						}
						message.append(licensePrefDetails.getMessage());
						dto.setMessage(message.toString());
					}
				}
				if (dto.getLicensePreferenceEnum() == LicensePrefEnum.VERIFICATION) {
					message.append(" ");
					continue;
				} else {
					break;
				}
			}

			if (dto.getLicensePreferenceEnum() == LicensePrefEnum.VERIFICATION) {
				if (notAvailableCount == 2) {
					dto.setMessage(Messages.BOTH_NOT_AVAILABLE);
				} else if (notAvailableAtTheMomentCount == 2) {
					dto.setMessage(Messages.BOTH_NOT_AVAILABLE_AT_THE_MOMENT);
				}
			}

			list.add(dto);
		}
		return list;
	}

	private Setting validateNotifyForLicensePref(Setting setting,SettingDto parentSettingDto, CompanyType companyType) {

		if(accessUtil.isHostCompany(companyType.toString())){
			return setting;
		}

		//License preference setting
		Setting parentSetting = settingDtoToSetting.convert(parentSettingDto);

		//4.5 intelligence - Job match found
		setting = validateLicensePrefUtil(setting, parentSetting, LicensePrefEnum.FOUR_DOT_FIVE_INTELLIGENCE,
				NotifyEventEnum.JOB_MATCH_FOUND, NotifyEnum.CANDIDATE_CARD,companyType);
		setting = validateLicensePrefUtil(setting, parentSetting, LicensePrefEnum.FOUR_DOT_FIVE_INTELLIGENCE,
				NotifyEventEnum.JOB_MATCH_FOUND, NotifyEnum.CANDIDATE_JOB_CARD,companyType);
		setting = validateLicensePrefUtil(setting, parentSetting, LicensePrefEnum.FOUR_DOT_FIVE_INTELLIGENCE,
				NotifyEventEnum.JOB_MATCH_FOUND, NotifyEnum.REQUISITION_CARD,companyType);

		setting = validateLicensePrefUtilTurnDetectOn(setting, parentSetting, LicensePrefEnum.FOUR_DOT_FIVE_INTELLIGENCE,
				NotifyEventEnum.JOB_MATCH_FOUND,companyType);

		//4.5 intelligence - No Job match found
		setting = validateLicensePrefUtil(setting, parentSetting, LicensePrefEnum.FOUR_DOT_FIVE_INTELLIGENCE,
				NotifyEventEnum.NO_JOB_MATCH_FOUND, NotifyEnum.CANDIDATE_CARD,companyType);

		setting = validateLicensePrefUtilTurnDetectOn(setting, parentSetting, LicensePrefEnum.FOUR_DOT_FIVE_INTELLIGENCE,
				NotifyEventEnum.NO_JOB_MATCH_FOUND,companyType);

		setting = validateLicensePrefUtil(setting, parentSetting, LicensePrefEnum.FOUR_DOT_FIVE_INTELLIGENCE,
				NotifyEventEnum.CANDIDATE_NO_LONGER_MATCHES, NotifyEnum.CANDIDATE_CARD,companyType);

		setting = validateLicensePrefUtil(setting, parentSetting, LicensePrefEnum.FOUR_DOT_FIVE_INTELLIGENCE,
				NotifyEventEnum.CANDIDATE_NO_LONGER_MATCHES, NotifyEnum.REQUISITION_CARD,companyType);

		setting = validateLicensePrefUtilTurnDetectOn(setting, parentSetting, LicensePrefEnum.FOUR_DOT_FIVE_INTELLIGENCE,
				NotifyEventEnum.CANDIDATE_NO_LONGER_MATCHES,companyType);

		for (NotifyEventEnum notifyEventEnum : TECH_ASSESSMENT_EVENTs) {
			setting = validateLicensePrefUtil(setting, parentSetting, LicensePrefEnum.TECH_ASSESSMENT,
					notifyEventEnum, NotifyEnum.CANDIDATE_CARD, companyType);
			setting = validateLicensePrefUtil(setting, parentSetting, LicensePrefEnum.TECH_ASSESSMENT,
					notifyEventEnum, NotifyEnum.CANDIDATE_JOB_CARD, companyType);
			setting = validateLicensePrefUtil(setting, parentSetting, LicensePrefEnum.TECH_ASSESSMENT,
					notifyEventEnum, NotifyEnum.REQUISITION_CARD, companyType);

			setting = validateLicensePrefUtilTurnDetectOn(setting, parentSetting, LicensePrefEnum.TECH_ASSESSMENT,
					notifyEventEnum, companyType);
		}

		for (NotifyEventEnum notifyEventEnum : VALUE_ASSESSMENT_EVENTs) {
			setting = validateLicensePrefUtil(setting, parentSetting, LicensePrefEnum.VALUE_ASSESSMENT,
					notifyEventEnum, NotifyEnum.CANDIDATE_CARD, companyType);
			setting = validateLicensePrefUtil(setting, parentSetting, LicensePrefEnum.VALUE_ASSESSMENT,
					notifyEventEnum, NotifyEnum.CANDIDATE_JOB_CARD, companyType);
			setting = validateLicensePrefUtil(setting, parentSetting, LicensePrefEnum.VALUE_ASSESSMENT,
					notifyEventEnum, NotifyEnum.REQUISITION_CARD, companyType);

			setting = validateLicensePrefUtilTurnDetectOn(setting, parentSetting, LicensePrefEnum.VALUE_ASSESSMENT,
					notifyEventEnum, companyType);
		}

		for (NotifyEventEnum notifyEventEnum : PHONE_SCREEN_EVENTs) {
			setting = validateLicensePrefUtil(setting, parentSetting, LicensePrefEnum.PHONE_SCREEN,
					notifyEventEnum, NotifyEnum.CANDIDATE_CARD, companyType);
			setting = validateLicensePrefUtil(setting, parentSetting, LicensePrefEnum.PHONE_SCREEN,
					notifyEventEnum, NotifyEnum.CANDIDATE_JOB_CARD, companyType);
			setting = validateLicensePrefUtil(setting, parentSetting, LicensePrefEnum.PHONE_SCREEN,
					notifyEventEnum, NotifyEnum.REQUISITION_CARD, companyType);

			setting = validateLicensePrefUtilTurnDetectOn(setting, parentSetting, LicensePrefEnum.PHONE_SCREEN,
					notifyEventEnum, companyType);
		}

		for (NotifyEventEnum notifyEventEnum : INTERVIEW_EVENTs) {
			setting = validateLicensePrefUtil(setting, parentSetting, LicensePrefEnum.INTERVIEW,
					notifyEventEnum, NotifyEnum.CANDIDATE_CARD, companyType);
			setting = validateLicensePrefUtil(setting, parentSetting, LicensePrefEnum.INTERVIEW,
					notifyEventEnum, NotifyEnum.CANDIDATE_JOB_CARD, companyType);
			setting = validateLicensePrefUtil(setting, parentSetting, LicensePrefEnum.INTERVIEW,
					notifyEventEnum, NotifyEnum.REQUISITION_CARD, companyType);

			setting = validateLicensePrefUtilTurnDetectOn(setting, parentSetting, LicensePrefEnum.INTERVIEW,
					notifyEventEnum, companyType);
		}

		// Verification - Verification Completed
		setting = validateLicensePrefUtil(setting, parentSetting, LicensePrefEnum.VERIFICATION,
				NotifyEventEnum.VERIFICATION_COMPLETED, NotifyEnum.CANDIDATE_CARD,companyType);
		setting = validateLicensePrefUtil(setting, parentSetting, LicensePrefEnum.VERIFICATION,
				NotifyEventEnum.VERIFICATION_COMPLETED, NotifyEnum.CANDIDATE_JOB_CARD,companyType);
		setting = validateLicensePrefUtil(setting, parentSetting, LicensePrefEnum.VERIFICATION,
				NotifyEventEnum.VERIFICATION_COMPLETED, NotifyEnum.REQUISITION_CARD,companyType);

		setting = validateLicensePrefUtilTurnDetectOn(setting, parentSetting, LicensePrefEnum.VERIFICATION,
				NotifyEventEnum.VERIFICATION_COMPLETED,companyType);

		// Verification - Verification Overdue
		setting = validateLicensePrefUtil(setting, parentSetting, LicensePrefEnum.VERIFICATION,
				NotifyEventEnum.VERIFICATION_OVERDUE, NotifyEnum.CANDIDATE_CARD,companyType);
		setting = validateLicensePrefUtil(setting, parentSetting, LicensePrefEnum.VERIFICATION,
				NotifyEventEnum.VERIFICATION_OVERDUE, NotifyEnum.CANDIDATE_JOB_CARD,companyType);
		setting = validateLicensePrefUtil(setting, parentSetting, LicensePrefEnum.VERIFICATION,
				NotifyEventEnum.VERIFICATION_OVERDUE, NotifyEnum.REQUISITION_CARD,companyType);

		setting = validateLicensePrefUtilTurnDetectOn(setting, parentSetting, LicensePrefEnum.VERIFICATION,
				NotifyEventEnum.VERIFICATION_OVERDUE,companyType);

		return setting;
	}

	private Setting validateLicensePrefUtilTurnDetectOn(Setting setting, Setting parentSetting, LicensePrefEnum lpEnum,
														NotifyEventEnum neEnum,CompanyType companyType) {
		NotifyEvent pNotifyEvents_fourdotfive_cc = parentSetting.getLicensePreferenceSetting()
				.get(lpEnum).getNotifyEvents().get(neEnum);

		NotifyEvent cNotifyEvents_fourdotfive_cc = setting.getLicensePreferenceSetting().get(lpEnum).getNotifyEvents()
				.get(neEnum);

		if (companyType.equals(CompanyType.ClientOrBU)) {
			if (pNotifyEvents_fourdotfive_cc != null && (pNotifyEvents_fourdotfive_cc.isDetectOnReadOnly() || (!pNotifyEvents_fourdotfive_cc.isDetectOn() && !pNotifyEvents_fourdotfive_cc.isDetectOnReadOnly()))) {
				setting.getLicensePreferenceSetting().get(lpEnum).getNotifyEvents()
						.get(neEnum).setDetectOnReadOnly(true);
			} else if (cNotifyEvents_fourdotfive_cc != null) {
				setting.getLicensePreferenceSetting().get(lpEnum).getNotifyEvents()
						.get(neEnum).setDetectOnReadOnly(false);
			}

		} else {
			if ((pNotifyEvents_fourdotfive_cc != null && pNotifyEvents_fourdotfive_cc.isDetectOn())) {
				setting.getLicensePreferenceSetting().get(lpEnum).getNotifyEvents()
						.get(neEnum).setDetectOnReadOnly(false);
			} else if (cNotifyEvents_fourdotfive_cc != null) {
				setting.getLicensePreferenceSetting().get(lpEnum).getNotifyEvents()
						.get(neEnum).setDetectOnReadOnly(true);
			}
		}
		return setting;

	}
	private Setting validateLicensePrefUtil(Setting setting, Setting parentSetting, LicensePrefEnum lpEnum,
											NotifyEventEnum neEnum, NotifyEnum nEnum,CompanyType companyType) {


		Notify pNotify_fourdotfive_cc = parentSetting.getLicensePreferenceSetting()
				.get(lpEnum).getNotifyEvents().get(neEnum)
				.getNotifyOn().get(nEnum);
		Notify cNotify_fourdotfive_cc = setting.getLicensePreferenceSetting().get(lpEnum).getNotifyEvents()
				.get(neEnum).getNotifyOn().get(nEnum);

		if (companyType.equals(CompanyType.ClientOrBU)) {
			if (pNotify_fourdotfive_cc != null && (pNotify_fourdotfive_cc.isReadOnly() || (!pNotify_fourdotfive_cc.isEnabled() && !pNotify_fourdotfive_cc.isReadOnly()))) {
				setting.getLicensePreferenceSetting().get(lpEnum).getNotifyEvents()
						.get(neEnum).getNotifyOn().get(nEnum).setReadOnly(true);
			} else if (cNotify_fourdotfive_cc != null) {
				setting.getLicensePreferenceSetting().get(lpEnum).getNotifyEvents()
						.get(neEnum).getNotifyOn().get(nEnum).setReadOnly(false);
			}

		} else {
			if ((pNotify_fourdotfive_cc != null && pNotify_fourdotfive_cc.isEnabled())) {
				setting.getLicensePreferenceSetting().get(lpEnum).getNotifyEvents()
						.get(neEnum).getNotifyOn().get(nEnum).setReadOnly(false);
			} else if (cNotify_fourdotfive_cc != null) {
				setting.getLicensePreferenceSetting().get(lpEnum).getNotifyEvents()
						.get(neEnum).getNotifyOn().get(nEnum).setReadOnly(true);
			}
		}
		return setting;
	}

	private Setting validateNotifyForPerformance(Setting setting, SettingDto parentSettingDto,  CompanyType companyType) {

		if(accessUtil.isHostCompany(companyType.toString())){
			return setting;
		}

		// Performance setting
		Setting parentSetting = settingDtoToSetting.convert(parentSettingDto);
		// Time to Place
		setting = validatePerformanceUtil(setting, parentSetting, PerformanceEnum.TIME_TO_PLACE,
				NotifyEventEnum.TIME_TO_PLACE, NotifyEnum.CANDIDATE_CARD ,companyType);
		setting = validatePerformanceUtil(setting, parentSetting,  PerformanceEnum.TIME_TO_PLACE,
				NotifyEventEnum.TIME_TO_PLACE, NotifyEnum.CANDIDATE_JOB_CARD,companyType);

		setting = validatePerformanceUtilTurnDetect(setting, parentSetting,  PerformanceEnum.TIME_TO_PLACE,
				NotifyEventEnum.TIME_TO_PLACE,companyType);

		// Time to Convert
		setting = validatePerformanceUtil(setting, parentSetting, PerformanceEnum.TIME_TO_CONVERT,
				NotifyEventEnum.TIME_TO_CONVERT, NotifyEnum.CANDIDATE_CARD,companyType);
		setting = validatePerformanceUtil(setting, parentSetting, PerformanceEnum.TIME_TO_CONVERT,
				NotifyEventEnum.TIME_TO_CONVERT, NotifyEnum.CANDIDATE_JOB_CARD,companyType);

		setting = validatePerformanceUtilTurnDetect(setting, parentSetting,  PerformanceEnum.TIME_TO_CONVERT,
				NotifyEventEnum.TIME_TO_CONVERT, companyType);

		// Time to Source
		setting = validatePerformanceUtil(setting, parentSetting, PerformanceEnum.TIME_TO_SOURCE,
				NotifyEventEnum.TIME_TO_SOURCE, NotifyEnum.REQUISITION_CARD,companyType);

		setting = validatePerformanceUtilTurnDetect(setting, parentSetting,  PerformanceEnum.TIME_TO_SOURCE,
				NotifyEventEnum.TIME_TO_SOURCE, companyType);

		// Time to Fill
		setting = validatePerformanceUtil(setting, parentSetting, PerformanceEnum.TIME_TO_FILL,
				NotifyEventEnum.TIME_TO_FILL, NotifyEnum.REQUISITION_CARD,companyType);

		setting = validatePerformanceUtilTurnDetect(setting, parentSetting,  PerformanceEnum.TIME_TO_FILL,
				NotifyEventEnum.TIME_TO_FILL, companyType);

		return setting;
	}



	private Setting validateNotifyForUserRole(Setting setting, SettingDto parentSettingDto,  CompanyType companyType) {

		if(accessUtil.isHostCompany(companyType.toString())){
			return setting;
		}

		// Performance setting
		Setting parentSetting = settingDtoToSetting.convert(parentSettingDto);
		// Time to Place
		setting = validateUserRoleUtil(setting, parentSetting, UserRoleEnum.USER_ROLE_SCOPE,
				NotifyEventEnum.USER_ROLE_SCOPE ,NotifyEnum.USER_CARD ,companyType);

		setting = validateUserRoleTurnDetect(setting, parentSetting,  UserRoleEnum.USER_ROLE_SCOPE,
				NotifyEventEnum.USER_ROLE_SCOPE,companyType);

		return setting;
	}


	private Setting validateUserRoleTurnDetect(Setting setting, Setting parentSetting, UserRoleEnum pEnum,
											   NotifyEventEnum neEnum, CompanyType companyType) {


		NotifyEvent pNotifyEvent_fourdotfive_cc = parentSetting.getUserRoleSetting()
				.get(pEnum).getNotifyEvents().get(neEnum);


		NotifyEvent cNotifyEvent_fourdotfive_cc = setting.getUserRoleSetting().get(pEnum).getNotifyEvents().get(neEnum);

		if (companyType.equals(CompanyType.ClientOrBU)) {
			if (pNotifyEvent_fourdotfive_cc != null && (pNotifyEvent_fourdotfive_cc.isDetectOnReadOnly() || (!pNotifyEvent_fourdotfive_cc.isDetectOn() && !pNotifyEvent_fourdotfive_cc.isDetectOnReadOnly()))) {
				setting.getUserRoleSetting().get(pEnum).getNotifyEvents().get(neEnum).setDetectOnReadOnly(true);
			} else {
				setting.getUserRoleSetting().get(pEnum).getNotifyEvents().get(neEnum).setDetectOnReadOnly(false);
			}
		}  else {
			if (pNotifyEvent_fourdotfive_cc != null && pNotifyEvent_fourdotfive_cc.isDetectOn()) {
				setting.getUserRoleSetting().get(pEnum).getNotifyEvents().get(neEnum).setDetectOnReadOnly(false);
			} else if (cNotifyEvent_fourdotfive_cc != null) {
				setting.getUserRoleSetting().get(pEnum).getNotifyEvents().get(neEnum).setDetectOnReadOnly(true);
			}

		}

		return setting;
	}


	private Setting validateUserRoleUtil(Setting setting, Setting parentSetting, UserRoleEnum pEnum,
										 NotifyEventEnum neEnum, NotifyEnum nEnum, CompanyType companyType) {

		Notify pNotify_fourdotfive_cc = parentSetting.getUserRoleSetting()
				.get(pEnum).getNotifyEvents().get(neEnum)
				.getNotifyOn().get(nEnum);

		Notify cNotify_fourdotfive_cc = setting.getUserRoleSetting().get(pEnum).getNotifyEvents()
				.get(neEnum).getNotifyOn().get(nEnum);

		if (companyType.equals(CompanyType.ClientOrBU)) {
			if (pNotify_fourdotfive_cc != null && (pNotify_fourdotfive_cc.isReadOnly() || (!pNotify_fourdotfive_cc.isEnabled() && !pNotify_fourdotfive_cc.isReadOnly()))) {
				setting.getUserRoleSetting().get(pEnum).getNotifyEvents()
						.get(neEnum).getNotifyOn().get(nEnum).setReadOnly(true);
			} else if (cNotify_fourdotfive_cc != null) {
				setting.getUserRoleSetting().get(pEnum).getNotifyEvents()
						.get(neEnum).getNotifyOn().get(nEnum).setReadOnly(false);
			}
		} else {
			if (pNotify_fourdotfive_cc != null && pNotify_fourdotfive_cc.isEnabled()) {
				setting.getUserRoleSetting().get(pEnum).getNotifyEvents()
						.get(neEnum).getNotifyOn().get(nEnum).setReadOnly(false);
			} else if (cNotify_fourdotfive_cc != null) {
				setting.getUserRoleSetting().get(pEnum).getNotifyEvents()
						.get(neEnum).getNotifyOn().get(nEnum).setReadOnly(true);
			}
		}
		return setting;
	}

	private Setting validatePerformanceUtilTurnDetect(Setting setting, Setting parentSetting, PerformanceEnum pEnum,
													  NotifyEventEnum neEnum, CompanyType companyType) {


		NotifyEvent pNotifyEvent_fourdotfive_cc = parentSetting.getPerformanceSetting()
				.get(pEnum).getNotifyEvents().get(neEnum);


		NotifyEvent cNotifyEvent_fourdotfive_cc = setting.getPerformanceSetting().get(pEnum).getNotifyEvents().get(neEnum);

		if (companyType.equals(CompanyType.ClientOrBU)) {
			if (pNotifyEvent_fourdotfive_cc != null && (pNotifyEvent_fourdotfive_cc.isDetectOnReadOnly() || (!pNotifyEvent_fourdotfive_cc.isDetectOn() && !pNotifyEvent_fourdotfive_cc.isDetectOnReadOnly()))) {
				setting.getPerformanceSetting().get(pEnum).getNotifyEvents().get(neEnum).setDetectOnReadOnly(true);
			} else {
				setting.getPerformanceSetting().get(pEnum).getNotifyEvents().get(neEnum).setDetectOnReadOnly(false);
			}
		}  else {
			if (pNotifyEvent_fourdotfive_cc != null && pNotifyEvent_fourdotfive_cc.isDetectOn()) {
				setting.getPerformanceSetting().get(pEnum).getNotifyEvents().get(neEnum).setDetectOnReadOnly(false);
			} else if (cNotifyEvent_fourdotfive_cc != null) {
				setting.getPerformanceSetting().get(pEnum).getNotifyEvents().get(neEnum).setDetectOnReadOnly(true);
			}

		}

		return setting;
	}

	private Setting validatePerformanceUtil(Setting setting, Setting parentSetting, PerformanceEnum pEnum,
											NotifyEventEnum neEnum, NotifyEnum nEnum, CompanyType companyType) {

		Notify pNotify_fourdotfive_cc = parentSetting.getPerformanceSetting()
				.get(pEnum).getNotifyEvents().get(neEnum)
				.getNotifyOn().get(nEnum);

		Notify cNotify_fourdotfive_cc = setting.getPerformanceSetting().get(pEnum).getNotifyEvents()
				.get(neEnum).getNotifyOn().get(nEnum);

		if (companyType.equals(CompanyType.ClientOrBU)) {
			if (pNotify_fourdotfive_cc != null && (pNotify_fourdotfive_cc.isReadOnly() || (!pNotify_fourdotfive_cc.isEnabled() && !pNotify_fourdotfive_cc.isReadOnly()))) {
				setting.getPerformanceSetting().get(pEnum).getNotifyEvents()
						.get(neEnum).getNotifyOn().get(nEnum).setReadOnly(true);
			} else if (cNotify_fourdotfive_cc != null) {
				setting.getPerformanceSetting().get(pEnum).getNotifyEvents()
						.get(neEnum).getNotifyOn().get(nEnum).setReadOnly(false);
			}
		} else {
			if (pNotify_fourdotfive_cc != null && pNotify_fourdotfive_cc.isEnabled()) {
				setting.getPerformanceSetting().get(pEnum).getNotifyEvents()
						.get(neEnum).getNotifyOn().get(nEnum).setReadOnly(false);
			} else if (cNotify_fourdotfive_cc != null) {
				setting.getPerformanceSetting().get(pEnum).getNotifyEvents()
						.get(neEnum).getNotifyOn().get(nEnum).setReadOnly(true);
			}
		}
		return setting;
	}

	private List<LicensePreferenceSettingDto> formatNameForLicensePrefSetting(List<LicensePreferenceSettingDto> licensePrefSetting){

		for (LicensePreferenceSettingDto dto : licensePrefSetting) {
			if (dto.getLicensePreferenceEnum().equals(LicensePrefEnum.FOUR_DOT_FIVE_INTELLIGENCE)) {
				dto.setName(AppConstants.FOUR_DOT_FIVE_INTELLIGENCE_SCORE);
			} else if (dto.getLicensePreferenceEnum().equals(LicensePrefEnum.VERIFICATION)) {
				dto.setName(AppConstants.ALL_VERIFICATION);
			} else if (dto.getLicensePreferenceEnum().equals(LicensePrefEnum.AUTO_MATCH)) {
				dto.setName(AppConstants.AUTOMATED_MATCH);
			} else {
				dto.setName(CommonUtil.formatEnumToString(dto.getLicensePreferenceEnum()));
			}

			for(NotifyEventDto notifyEvent : dto.getNotifyEvents()){
				notifyEvent.setName(NotificationPreferenceEnum.valueOf(notifyEvent.getNotifyEventEnum().name()).getDescription());
				for(NotifyDto notify : notifyEvent.getNotifyOn()){
					if(notify != null){
						notify.setName(CommonUtil.formatEnumToString(notify.getNotifyEnum()));
					}
				}
			}
		}

		return licensePrefSetting;
	}

	private List<PerformanceSettingDto> formatNamePerformceSetting(List<PerformanceSettingDto> performanceSetting) {

		for (PerformanceSettingDto dto : performanceSetting) {
			dto.setName(CommonUtil.formatEnumToString(dto.getPerformanceEnum()));
			for (NotifyEventDto notifyEvent : dto.getNotifyEvents()) {
				notifyEvent.setName(CommonUtil.formatEnumToString(notifyEvent.getNotifyEventEnum()));
				for (NotifyDto notify : notifyEvent.getNotifyOn()) {
					if (notify != null) {
						notify.setName(CommonUtil.formatEnumToString(notify.getNotifyEnum()));
					}
				}
			}
		}
		return performanceSetting;
	}


	private List<UserRoleSettingDto> formatNameUserRoleSetting(List<UserRoleSettingDto> userRoleSetting) {

		for (UserRoleSettingDto dto : userRoleSetting) {
			dto.setName(CommonUtil.formatEnumToString(dto.getUserRoleEnum()));
			for (NotifyEventDto notifyEvent : dto.getNotifyEvents()) {
				notifyEvent.setName(CommonUtil.formatEnumToString(notifyEvent.getNotifyEventEnum()));
				for (NotifyDto notify : notifyEvent.getNotifyOn()) {
					if (notify != null) {
						notify.setName(CommonUtil.formatEnumToString(notify.getNotifyEnum()));
					}
				}
			}
		}
		return userRoleSetting;
	}

	@Override
	public SettingDto convert(Setting source) {
		// TODO Auto-generated method stub
		return null;
	}

}
