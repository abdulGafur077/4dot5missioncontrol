package com.fourdotfive.missioncontrol.teammembers;

import java.util.List;

import com.fourdotfive.missioncontrol.dtos.user.TeamMemberDto;
import com.fourdotfive.missioncontrol.pojo.user.User;

public interface ActionItemService {

	/**
	 * Get team members for the user
	 * 
	 * @param userId
	 * @param companyId
	 * @param user
	 * @return
	 */
	List<User> getTeamMembers(String userId, String companyId, User user);

	/**
	 * Set actions to each of the team mebers including the user
	 * 
	 * @param userId
	 * @param companyId
	 * @param users
	 * @return
	 */
	List<TeamMemberDto> otherActions(String userId, String companyId,
			List<User> users);

	/**
	 * Get forwarded recipient to the user
	 * 
	 * @param teamMembers
	 * @param userId
	 * @param companyId
	 * @return
	 */
	List<TeamMemberDto> getForwardRecipientUser(
			List<TeamMemberDto> teamMembers, User userId, String companyId);

}