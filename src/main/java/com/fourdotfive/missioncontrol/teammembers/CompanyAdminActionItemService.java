package com.fourdotfive.missioncontrol.teammembers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fourdotfive.missioncontrol.common.AccessUtil;
import com.fourdotfive.missioncontrol.common.AppConstants;
import com.fourdotfive.missioncontrol.common.CommonUtil;
import com.fourdotfive.missioncontrol.common.UserNameComparator;
import com.fourdotfive.missioncontrol.dtos.user.TeamMemberDto;
import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.pojo.user.UserForwardedReference;
import com.fourdotfive.missioncontrol.user.UserApiExecutor;
import com.fourdotfive.missioncontrol.user.UserService;

@Service
public class CompanyAdminActionItemService implements ActionItemService {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(CompanyAdminActionItemService.class);

	@Autowired
	private AccessUtil accessUtil;
	@Autowired
	private UserService userService;
	@Autowired
	private UserApiExecutor userApiExecutor;

	@Override
	public List<User> getTeamMembers(String userId, String companyId, User user) {
		LOGGER.debug("Fetching team members for company admin");

		List<User> reporteeList = userService.getCompanyAdmins(user
				.getEmployer());
		reporteeList = CommonUtil.getUnarchivedTeamMembers(reporteeList);

		return reporteeList;
	}

	@Override
	public List<TeamMemberDto> otherActions(String userId, String companyId,
			List<User> users) {
		List<TeamMemberDto> dtos = new ArrayList<>();
		for (User u : users) {
			TeamMemberDto dto = null;

			if (u.getId().equals(userId)) {
				int count = userService.getReporteeList(userId).size();
				dto = TeamMemberDto.mapUserToTeamMember(u,
						AppConstants.CAN_EDIT, AppConstants.CANNOT_DELETE,
						AppConstants.CAN_DRILLDOWN, count, AppConstants.CANNOT_VIEW_NOTIFICATIONPREF);
			} else {
				dto = TeamMemberDto.mapUserToTeamMember(u,
						AppConstants.CANNOT_EDIT, AppConstants.CANNOT_DELETE,
						AppConstants.CANNOT_DRILLDOWN,
						AppConstants.NO_REPORTEES,AppConstants.CANNOT_VIEW_NOTIFICATIONPREF);
			}

			dtos.add(dto);
		}
		return dtos;
	}

	@Override
	public List<TeamMemberDto> getForwardRecipientUser(
			List<TeamMemberDto> teamMembers, User user, String companyId) {
		List<User> forwardRecipientList = isForwardRecipientAvailable(user);
		for (User forwardRecipient : forwardRecipientList) {
			for (TeamMemberDto teamMember : teamMembers) {
				if (forwardRecipient != null
						&& forwardRecipient.getId().equals(
								teamMember.getUserId())) {
					teamMember = TeamMemberDto
							.getForwardRecipientUser(teamMember);

					teamMember.setForwardFromId(user.getId());
					StringBuilder name = new StringBuilder(
							teamMember.getFirstName());
					if (teamMember.getLastName() != null) {
						name.append(" ").append(teamMember.getLastName());
					}
					teamMember.setForwardFromUser(name.toString());
				}

			}
		}
		teamMembers = addForwardRecipientIfNotPartOfTeam(teamMembers,
				forwardRecipientList);
		Collections.sort(teamMembers, new UserNameComparator());
		teamMembers = moveUserCardToFirst(teamMembers, user);
		return teamMembers;
	}

	private List<TeamMemberDto> moveUserCardToFirst(
			List<TeamMemberDto> teamMembers, User user) {

		int index = 0;
		TeamMemberDto self = null;
		for (int i = 0; i < teamMembers.size(); i++) {
			if (teamMembers.get(i).getUserId().equals(user.getId())) {
				self = teamMembers.get(i);
				self = checkIfLoggedInUserIsForwarded(self, user);
				index = i;
			}
		}
		if (self != null) {
			teamMembers.remove(index);
			teamMembers.add(0, self);
		}
		return teamMembers;

	}

	private TeamMemberDto checkIfLoggedInUserIsForwarded(
			TeamMemberDto teamMemberDto, User user) {
		User forwardRecipient = userApiExecutor.getRecipientUser(user.getId());
		if (forwardRecipient != null
				&& CommonUtil.isForwardRecipientViewable(user)) {
			teamMemberDto.setForwardToId(forwardRecipient.getId());
			if (forwardRecipient.getLastname() != null) {
				teamMemberDto.setForwardToUser(forwardRecipient.getFirstname()
						+ " "
						+ forwardRecipient
								.getLastname());
			} else {
				teamMemberDto.setForwardToUser(forwardRecipient.getFirstname());
			}
		}
		return teamMemberDto;
	}

	private List<User> isForwardRecipientAvailable(User user) {
		List<User> forwardRecipientList = new ArrayList<User>();
		List<UserForwardedReference> forwardedReferences = userApiExecutor
				.getForwardedFromRecipients(user.getId());
		if (forwardedReferences != null) {
		List<User> reporteeList = userService.getReporteeList(user.getId());
		for (User reportee : reporteeList) {
			for (Iterator<UserForwardedReference> iterator = forwardedReferences
					.iterator(); iterator.hasNext();) {
				UserForwardedReference obj = iterator.next();
				if (obj !=null && obj.getUser().getId().equals(reportee.getId())) {
					// Remove the current element from the iterator and the
					// list.
					iterator.remove();
				}
			}
		}
		for (UserForwardedReference forwardedReference : forwardedReferences) {
			if (forwardedReference!=null && !accessUtil.isUserArchived(forwardedReference)
					&& CommonUtil
							.isForwardRecipientViewable(forwardedReference)) {
				forwardRecipientList.add(forwardedReference.getUser());
			}
		}
		}
		return forwardRecipientList;
	}

	private List<TeamMemberDto> addForwardRecipientIfNotPartOfTeam(
			List<TeamMemberDto> teamMembers, List<User> forwardRecipientList) {
		for (TeamMemberDto teamMember : teamMembers) {
			for (Iterator<User> iterator = forwardRecipientList.iterator(); iterator
					.hasNext();) {
				User obj = iterator.next();
				if (obj.getId().equals(teamMember.getUserId())) {
					// Remove the current element from the iterator and the
					// list.
					iterator.remove();
				}
			}
		}
		for (User forwardRecipient : forwardRecipientList) {
			teamMembers.add(TeamMemberDto.getForwardRecipientUser(TeamMemberDto
					.mapUserToTeamMember(forwardRecipient)));
		}
		return teamMembers;

	}

}