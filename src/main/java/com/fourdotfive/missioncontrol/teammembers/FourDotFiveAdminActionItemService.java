package com.fourdotfive.missioncontrol.teammembers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fourdotfive.missioncontrol.common.AccessUtil;
import com.fourdotfive.missioncontrol.common.AppConstants;
import com.fourdotfive.missioncontrol.common.CommonUtil;
import com.fourdotfive.missioncontrol.common.UserNameComparator;
import com.fourdotfive.missioncontrol.company.CompanyService;
import com.fourdotfive.missioncontrol.dtos.user.TeamMemberDto;
import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.user.UserApiExecutor;
import com.fourdotfive.missioncontrol.user.UserService;

@Service
public class FourDotFiveAdminActionItemService implements ActionItemService {

	private static final Logger LOGGER = LoggerFactory.getLogger(FourDotFiveAdminActionItemService.class);

	private boolean canDelete = true; // the user is authorized to delete
	private boolean canEdit = true; // update

	// or update
	private boolean canDrillDown = true;// the user can drill down to view

	@Autowired
	private CompanyService companyService;
	@Autowired
	private AccessUtil accessUtil;
	@Autowired
	private UserService userService;
	@Autowired
	private UserApiExecutor userApiExecutor;

	@Override
	public List<User> getTeamMembers(String userId, String companyId, User user) {
		LOGGER.info("Fetching team members for FourDotFive Admin");
		Company company = companyService.getCompany(companyId);
		List<User> reporteeList = new ArrayList<>();
		if (!accessUtil.isHostCompany(company)) {
			reporteeList = userService.getCompanyAdmins(company);
			reporteeList = CommonUtil.getUnarchivedTeamMembers(reporteeList);

		}
		return reporteeList;
	}

	@Override
	public List<TeamMemberDto> otherActions(String userId, String companyId,
			List<User> users) {
		List<TeamMemberDto> teamMemberList = new ArrayList<TeamMemberDto>();
		for (User user : users) {
			int count = userService.getReporteeList(user.getId()).size();
			TeamMemberDto dto = TeamMemberDto.mapUserToTeamMember(user,
					canEdit, canDelete, canDrillDown, count, AppConstants.CAN_VIEW_NOTIFICATIONPREF);
			teamMemberList.add(dto);
		}
		return teamMemberList;
	}

	@Override
	public List<TeamMemberDto> getForwardRecipientUser(
			List<TeamMemberDto> teamMembers, User userId, String companyId) {

		for (TeamMemberDto teamMember1 : teamMembers) {
			User forwardRecipient = userApiExecutor.getRecipientUser(teamMember1.getUserId());
			if(forwardRecipient!=null){
				User user = userApiExecutor.getUserById(teamMember1.getUserId());
				if(!accessUtil.isUserArchived(user)
						&& CommonUtil
								.isForwardRecipientViewable(user)){
					teamMember1.setForwardRecipient(true);
					String name = CommonUtil.getUserName(forwardRecipient);
					teamMember1.setForwardFromUser(name);
					teamMember1.setForwardFromId(forwardRecipient.getId());
				}
			}
		}

		Collections.sort(teamMembers, new UserNameComparator());
		return teamMembers;
	}
	

}
