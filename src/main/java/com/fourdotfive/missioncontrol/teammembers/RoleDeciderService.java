package com.fourdotfive.missioncontrol.teammembers;

public interface RoleDeciderService {

	ActionItemService getServiceBasedOnRole(String roleId);

}
