package com.fourdotfive.missioncontrol.teammembers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fourdotfive.missioncontrol.common.AppConstants;

@Service
public class RoleDeciderServiceImp implements RoleDeciderService {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(RoleDeciderServiceImp.class);
	@Autowired
	private SuperUserActionItemService superUserActionItemService;
	@Autowired
	private CompanyAdminActionItemService companyAdminActionItemService;
	@Autowired
	private RecManagerActionItemService recManagerActionItemService;
	@Autowired
	private FourDotFiveAdminActionItemService fourDotFiveAdminActionItemService;
	@Autowired
	private SeniorRecActionItemService seniorRecActionItemService;

	@Override
	public ActionItemService getServiceBasedOnRole(String roleId) {
		switch (roleId) {

		case AppConstants.SUPERUSER_ROLEID:
			return superUserActionItemService;
		case AppConstants.FOURDOT5ADMIN_ROLEID:
			return fourDotFiveAdminActionItemService;
		case AppConstants.STAFFING_ADMIN_ROLEID:
			return companyAdminActionItemService;
		case AppConstants.CORPORATE_ADMIN_ROLEID:
			return companyAdminActionItemService;
		case AppConstants.CORPORATE_REC_MANAGER_ROLEID:
			return recManagerActionItemService;
		case AppConstants.STAFFING_REC_MANAGER_ROLEID:
			return recManagerActionItemService;
		case AppConstants.STAFFING_SRREC_ROLEID:
			return seniorRecActionItemService;
		case AppConstants.CORPORATE_SRREC_ROLEID:
			return seniorRecActionItemService;
		case AppConstants.STAFFING_REC_ROLEID:
			return seniorRecActionItemService;
		case AppConstants.CORPORATE_REC_ROLEID:
			return seniorRecActionItemService;
		case AppConstants.HIRING_MANAGER_ROLEID:
			return recManagerActionItemService;
		default:
			return null;
		}
	}

}
