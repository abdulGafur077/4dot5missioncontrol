package com.fourdotfive.missioncontrol.teammembers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fourdotfive.missioncontrol.common.AccessUtil;
import com.fourdotfive.missioncontrol.common.AppConstants;
import com.fourdotfive.missioncontrol.common.CommonUtil;
import com.fourdotfive.missioncontrol.common.UserNameComparator;
import com.fourdotfive.missioncontrol.company.CompanyService;
import com.fourdotfive.missioncontrol.dtos.user.TeamMemberDto;
import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.user.UserApiExecutor;
import com.fourdotfive.missioncontrol.user.UserService;

@Service
public class SuperUserActionItemService implements ActionItemService {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(SuperUserActionItemService.class);

	@Autowired
	private CompanyService companyService;
	@Autowired
	private AccessUtil accessUtil;
	@Autowired
	private UserService userService;
	@Autowired
	private UserApiExecutor userApiExecutor;

	@Override
	public List<User> getTeamMembers(String userId, String companyId, User user) {
		LOGGER.debug("Fetching team members for super user");

		Company company = companyService.getCompany(companyId);

		if (accessUtil.isHostCompany(company)) {
			return userService.getReporteeList(userId);

		} else {
			List<User> companyAdmins = userService.getCompanyAdmins(company);
			return companyAdmins;
		}
	}

	@Override
	public List<TeamMemberDto> otherActions(String userId, String companyId,
			List<User> users) {
		List<TeamMemberDto> dtos = new ArrayList<>();
		for (User u : users) {
			if (accessUtil.isActiveFourDot5Admin(u)) {
				TeamMemberDto dto = TeamMemberDto.mapUserToTeamMember(u,
						AppConstants.CAN_EDIT, AppConstants.CAN_DELETE,
						AppConstants.CANNOT_DRILLDOWN,
						AppConstants.NO_REPORTEES, AppConstants.CANNOT_VIEW_NOTIFICATIONPREF);
				dtos.add(dto);
			} else {
				int count = userService.getReporteeList(u.getId()).size();
				TeamMemberDto dto = TeamMemberDto.mapUserToTeamMember(u,
						AppConstants.CAN_EDIT, AppConstants.CAN_DELETE,
						AppConstants.CAN_DRILLDOWN, count, AppConstants.CAN_VIEW_NOTIFICATIONPREF);
				dtos.add(dto);
			}
		}
		return dtos;
	}


	@Override
	public List<TeamMemberDto> getForwardRecipientUser(
			List<TeamMemberDto> teamMembers, User userId, String companyId) {

		for (TeamMemberDto teamMember1 : teamMembers) {
			User forwardRecipient = userApiExecutor.getRecipientUser(teamMember1.getUserId());
			if(forwardRecipient!=null){
				User user = userApiExecutor.getUserById(teamMember1.getUserId());
				if(!accessUtil.isUserArchived(user)
						&& CommonUtil
								.isForwardRecipientViewable(user)){
					teamMember1.setForwardRecipient(true);
					String name = CommonUtil.getUserName(forwardRecipient);
					teamMember1.setForwardFromUser(name);
					teamMember1.setForwardFromId(forwardRecipient.getId());
				}
			}
		}

		Collections.sort(teamMembers, new UserNameComparator());
		return teamMembers;
	}

	


}
