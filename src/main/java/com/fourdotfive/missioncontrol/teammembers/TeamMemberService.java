package com.fourdotfive.missioncontrol.teammembers;

import java.util.List;

import com.fourdotfive.missioncontrol.dtos.user.TeamMemberDto;

public interface TeamMemberService {
	
	List<TeamMemberDto> getTeamMembers(String userId, String companyId);

	List<TeamMemberDto> getRecruitersList(String userId, String companyId);

}
