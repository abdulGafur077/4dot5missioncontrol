package com.fourdotfive.missioncontrol.teammembers;

import java.util.List;

import com.fourdotfive.missioncontrol.dtos.candidate.CandidateStatusCountDto;
import com.fourdotfive.missioncontrol.platformexcecutor.PlatformApiExecutorImp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fourdotfive.missioncontrol.dtos.user.TeamMemberDto;
import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.user.UserService;

@Service
public class TeamMemberServiceImp implements TeamMemberService {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(TeamMemberServiceImp.class);

	@Autowired
	private UserService userService;

	@Autowired
	private RoleDeciderService roleDeciderService;

	@Autowired
	private PlatformApiExecutorImp platformApiExecutorImp;

	@Override
	public List<TeamMemberDto> getTeamMembers(String userId, String companyId) {
		User user = userService.getUserById(userId);
		return callActionService(userId, companyId, user);
	}

	@Override
	public List<TeamMemberDto> getRecruitersList(String userId, String companyId) {
		LOGGER.info(getClass()+ " getRecruiterList(userid, compnayId)");
		User user = userService.getUserById(userId);
		LOGGER.info(getClass()+ "user "+user.getFirstname());
		return callRecruiterActionService(userId, companyId, user);
	}

	/**
	 * Appropriate service will be called based on role of the user
	 * 
	 * @param userId
	 * @param companyId
	 * @param user
	 * @return
	 */
	private List<TeamMemberDto> callActionService(String userId,
			String companyId, User user) {
		ActionItemService actionItemService = roleDeciderService
				.getServiceBasedOnRole(user.getRole().getId());
		return getTeamMembers(userId, companyId, user, actionItemService);
	}

	/**
	 * Get team members for the user(getting peers including forward recipient)
	 * 
	 * @param userId
	 * @param companyId
	 * @param user
	 * @param actionItemService
	 * @return
	 */
	private List<TeamMemberDto> getTeamMembers(String userId, String companyId, User user, ActionItemService actionItemService) {
		List<User> userList = actionItemService.getTeamMembers(userId, companyId, user);
		List<TeamMemberDto> teamMembers = actionItemService.otherActions(userId, companyId, userList);
		teamMembers = actionItemService.getForwardRecipientUser(teamMembers, user, companyId);
		return teamMembers;
	}

	private List<TeamMemberDto> callRecruiterActionService(String userId,
														   String companyId, User user) {
		ActionItemService actionItemService = roleDeciderService
				.getServiceBasedOnRole(user.getRole().getId());
		LOGGER.info(getClass()+ " callRecruiterActionService " );
		return getRecruiterList(userId, companyId, user, actionItemService);
	}

	private List<TeamMemberDto> getRecruiterList(String userId, String companyId, User user, ActionItemService actionItemService) {
		List<User> userList = actionItemService.getTeamMembers(userId, companyId, user);
		LOGGER.info(getClass()+ " getRecruiterList "+userList.size());
		List<TeamMemberDto> teamMembers = actionItemService.otherActions(userId, companyId, userList);
		return actionItemService.getForwardRecipientUser(teamMembers, user, companyId);
	}
}