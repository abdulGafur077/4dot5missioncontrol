package com.fourdotfive.missioncontrol.user;

import com.fourdotfive.missioncontrol.common.TokenDetails;
import com.fourdotfive.missioncontrol.dtos.user.UserDto;

public interface CreateUserService {
	UserDto saveUser(UserDto userDto, String roleId, String companyId,
			TokenDetails tokenDetails);
}
