package com.fourdotfive.missioncontrol.user;

import java.util.List;

import com.fourdotfive.missioncontrol.dtos.employee.DepartmentDto;
import com.fourdotfive.missioncontrol.employee.EmployeeService;
import com.fourdotfive.missioncontrol.pojo.company.Company;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fourdotfive.missioncontrol.common.AccessUtil;
import com.fourdotfive.missioncontrol.common.CommonUtil;
import com.fourdotfive.missioncontrol.common.Messages;
import com.fourdotfive.missioncontrol.common.TokenDetails;
import com.fourdotfive.missioncontrol.company.CompanyService;
import com.fourdotfive.missioncontrol.dtos.user.UserDto;
import com.fourdotfive.missioncontrol.exception.AssignManagerPlatformException;
import com.fourdotfive.missioncontrol.exception.DeleteUserPlatformException;
import com.fourdotfive.missioncontrol.exception.SaveUserMCException;
import com.fourdotfive.missioncontrol.exception.SaveUserPlatformException;
import com.fourdotfive.missioncontrol.manager.ManagerService;
import com.fourdotfive.missioncontrol.pojo.company.RoleEntityScope;
import com.fourdotfive.missioncontrol.pojo.user.AccessControlList;
import com.fourdotfive.missioncontrol.pojo.user.User;

@Service
public class CreateUserServiceImp implements CreateUserService {

	@Autowired
	private UserService userService;

	@Autowired
	private UserApiExecutor apiUserExecutor;

	@Autowired
	private AccessUtil accessUtil;

	@Autowired
	private ManagerService managerService;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private EmployeeService employeeService;

	private static final Logger LOGGER = LoggerFactory
			.getLogger(CreateUserServiceImp.class);

	@Override
	public UserDto saveUser(UserDto userDto, String roleId, String companyId,
			TokenDetails tokenDetails) {
		User user = null;
		UserDto dto = null;
		try {
			user = UserDto.mapUserDtoToUser(userDto);
			user = getAclDetails(userDto, user, companyId);
			user = setFRDetials(user);
			if(StringUtils.isEmpty(userDto.getBuId()))
				user.setBuId(null);
			if(StringUtils.isEmpty(userDto.getDepartmentId()))
				user.setDepartment(null);
			// save user
			LOGGER.debug("create user info {}", user);
			user = apiUserExecutor.saveUser(user, roleId, companyId);

			// check if it is a create for 4dot 5 admin
			checkUpdateFor4dot5Admin(user, tokenDetails, companyId);

			if (accessUtil.isManagerApplicable(user.getRole().getId())) {
				managerService.setManager(user.getId(), userDto
						.getUserDetails().getManager().getUserId(), companyId);
			}
			// get user dto from user
			dto = userService.getUserDto(user);
			if(StringUtils.isNotEmpty(user.getDepartment())){
				DepartmentDto departmentDto = employeeService.getDepartmentById(user.getDepartment());
				if(departmentDto != null){
					dto.setDepartmentId(user.getDepartment());
					dto.setDepartmentName(departmentDto.getName());
				}
			}
			if(StringUtils.isNotEmpty(user.getBuId())){
				Company company = companyService.getCompany(user.getBuId());
				if(company != null){
					dto.setBuId(user.getBuId());
					dto.setBuName(company.getName());
				}
			}
		} catch (SaveUserPlatformException e) {
			// While saving user if there is an exception, it will be caught and
			// thrown with a proper message
			LOGGER.error(Messages.EXCEPTION_WHILE_CREATING_USER);
			throw new SaveUserPlatformException(
					Messages.EXCEPTION_WHILE_CREATING_USER,
					CommonUtil.getUserName(userDto));
		} catch (AssignManagerPlatformException e) {
			LOGGER.error(
					"An error occured while saving manager {} for user {}",
					userDto.getUserDetails().getManager().getUserId(), user.getId());
			// While assigning the manager if there is an exception, it will be
			// caught and the user who was created recently will be deleted
			deleteUser(user);
		}
		return dto;
	}

	private void deleteUser( User user) {
		try {
			apiUserExecutor.deleteUser(user.getId());

			// If the user is deleted successfully, an exception will be
			// thrown with the message saying that the user was not
			// created
			throw new SaveUserMCException(Messages.EXCEPTION_WHILE_CREATING_USER,
					CommonUtil.getUserName(user));
		} catch (DeleteUserPlatformException e1) {
			// If there is an exception while deleting user, then it will be
			// caught here and an exception will be thrown with appropriate
			// message.
			throw new SaveUserMCException(Messages.EXCEPTION_WHILE_CREATING_USER_AND_ROLLBACK,
					CommonUtil.getUserName(user));
		}
	}

	private User setFRDetials(User user) {
		user.setForwardFromStartDate(null);
		user.setForwardToEndDate(null);
		user.setRecipientUser(null);
		return user;
	}

	private void checkUpdateFor4dot5Admin(User user, TokenDetails tokenDetails,
			String companyId) {
		// assign super user as manager if the user created is a 4dot5 admin
		if (accessUtil.isFourDot5Admin(user.getRole())) {
			userService.setManager(user.getId(), tokenDetails.getUserId(),
					companyId);
		}
	}

	private User getAclDetails(UserDto userDto, User user, String companyId) {
		if (userDto.getUserDetails().getUserId() == null) {
			List<RoleEntityScope> entityScopes = companyService
					.getBasicEntityScope(companyId);
			if (entityScopes != null && !entityScopes.isEmpty()) {
				for (RoleEntityScope entityScope : entityScopes) {
					if (entityScope.getRole().getId()
							.equals(userDto.getUserDetails().getRole().getId())) {
						if (!accessUtil.isFourDot5Admin(userDto
								.getUserDetails().getRole())
								&& !accessUtil.isCompanyAdmin(userDto
										.getUserDetails().getRole())) {
							AccessControlList accessControlList = new AccessControlList();
							accessControlList.setAllclientOrBU(entityScope
									.isAllClients());
							user.setAccessControlList(accessControlList);
							break;

						}
					}
				}

			}
		}
		return user;
	}

}
