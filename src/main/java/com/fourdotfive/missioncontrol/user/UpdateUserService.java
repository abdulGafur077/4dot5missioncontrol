package com.fourdotfive.missioncontrol.user;

import com.fourdotfive.missioncontrol.common.TokenDetails;
import com.fourdotfive.missioncontrol.dtos.user.UserDto;

public interface UpdateUserService {

	UserDto updateUser(UserDto userDto, String roleId, String companyId,
			TokenDetails tokenDetails);

}
