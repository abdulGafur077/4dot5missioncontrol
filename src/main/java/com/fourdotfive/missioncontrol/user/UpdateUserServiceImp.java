package com.fourdotfive.missioncontrol.user;

import com.fourdotfive.missioncontrol.dtos.employee.DepartmentDto;
import com.fourdotfive.missioncontrol.employee.EmployeeService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fourdotfive.missioncontrol.common.AccessUtil;
import com.fourdotfive.missioncontrol.common.AppConstants;
import com.fourdotfive.missioncontrol.common.Messages;
import com.fourdotfive.missioncontrol.common.TokenDetails;
import com.fourdotfive.missioncontrol.company.CompanyService;
import com.fourdotfive.missioncontrol.dtos.user.UserDto;
import com.fourdotfive.missioncontrol.exception.AssignManagerPlatformException;
import com.fourdotfive.missioncontrol.exception.RemoveManagerPlatformException;
import com.fourdotfive.missioncontrol.exception.RoleEntityScopeConflictException;
import com.fourdotfive.missioncontrol.exception.SaveUserPlatformException;
import com.fourdotfive.missioncontrol.exception.UserConflictException;
import com.fourdotfive.missioncontrol.forwardrecipient.ForwardRecipientService;
import com.fourdotfive.missioncontrol.manager.ManagerService;
import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.pojo.user.UserScope;
import com.fourdotfive.missioncontrol.userentityscope.UserScopeService;

@Service
public class UpdateUserServiceImp implements UpdateUserService {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(UpdateUserServiceImp.class);

	@Autowired
	private UserService userService;

	@Autowired
	private UserApiExecutor apiUserExecutor;

	@Autowired
	private UserScopeService userScopeService;

	@Autowired
	private AccessUtil accessUtil;

	@Autowired
	private ManagerService managerService;

	@Autowired
	private ForwardRecipientService forwardRecipientService;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private EmployeeService employeeService;

	@Override
	public UserDto updateUser(UserDto userDto, String roleId, String companyId,
			TokenDetails tokenDetails) {
		UserDto dto = null;
		User oldUserDetails = null;
		User user = null;
		boolean isRoleUpdatedToCompanyAdmin = false;
		try {

			checkForRES(userDto, companyId);
			oldUserDetails = userService.getUserById(userDto.getUserDetails()
					.getUserId());
			// Check if role can be updated
			isRoleUpdatedToCompanyAdmin = checkForRoleUpdate(userDto, roleId,
					companyId, oldUserDetails);
			if (isRoleUpdatedToCompanyAdmin) {
				LOGGER.debug("remove manager was success info {}");
			}
			if(roleId.equals(AppConstants.FOURDOT5ADMIN_ROLEID)){
				Company company = companyService.getHostCompany();
				companyId = company.getId();
			}

			user = UserDto.mapUserDtoToUser(userDto);
			// set forward recipient to user obj
			user = forwardRecipientService.setForwardRecipient(user, userDto);
			if(StringUtils.isEmpty(userDto.getBuId()))
				user.setBuId(null);
			if(StringUtils.isEmpty(userDto.getDepartmentId()))
				user.setDepartment(null);

			// save user
			LOGGER.debug("user info {}", user);
			user = apiUserExecutor.saveUser(user, roleId, companyId);
			if (accessUtil.isManagerApplicable(user.getRole().getId())) {
				userService.setManager(user.getId(), userDto.getUserDetails()
						.getManager().getUserId(), companyId);

			}
			// get user dto from user
			dto = userService.getUserDto(user);
			if (userDto.getUserScopeDetails() != null) {
				dto.setUserScopeDetails(userDto.getUserScopeDetails());
			}
			// set reportee count
			dto = getReporteeCount(user, dto);
			// action needs to taken if role was updated
			actionForRoleUpdate(oldUserDetails, dto, companyId, user);
			if(StringUtils.isNotEmpty(user.getDepartment())){
				DepartmentDto departmentDto = employeeService.getDepartmentById(user.getDepartment());
				if(departmentDto != null){
					dto.setDepartmentId(user.getDepartment());
					dto.setDepartmentName(departmentDto.getName());
				}
			}
			if(StringUtils.isNotEmpty(user.getBuId())){
				Company company = companyService.getCompany(user.getBuId());
				if(company != null){
					dto.setBuId(user.getBuId());
					dto.setBuName(company.getName());
				}
			}
		} catch (SaveUserPlatformException e) {
			// If update user throws an exception, update the old manager for
			// the user and throw user exception with message as
			// "Error while updating role, please try to update again"
			if (isRoleUpdatedToCompanyAdmin) {
				LOGGER.error(
						"assign manager was called since saving user failed during company admin role update:  managerid{} ",
						userDto.getUserDetails().getManager().getUserId());
				assignOldManager(oldUserDetails, userDto, companyId);
			}
		} catch (AssignManagerPlatformException e) {
			// If Assign manager throws an exception, update the user with old
			// role and throw user exception
			rollBackWhileUpdatingRole(user, oldUserDetails, roleId, companyId);
		}

		return dto;
	}

	private void rollBackWhileUpdatingRole(User user, User oldUserDetails,
			String roleId, String companyId) {
		user.setRole(oldUserDetails.getRole());
		try {
			user = apiUserExecutor.saveUser(user, roleId, companyId);
			throw new UserException(Messages.EXCEPTION_WHILE_ROLE_UPDATE,
					oldUserDetails.getRole());
		} catch (Exception e2) {
			throw new UserException(
					Messages.EXCEPTION_WHILE_ROLE_UPDATE_ROLLBACK,
					oldUserDetails.getRole());
		}
	}

	private void assignOldManager(User user, UserDto userDto, String companyId) {
		String message = userDto.getUserDetails().getRole().getDescription();
		try {
			managerService.setManager(userDto.getUserDetails().getUserId(),
					userDto.getUserDetails().getManager().getUserId(),
					companyId);

			throw new UserException(Messages.EXCEPTION_WHILE_ROLE_UPDATE,
					message);
		} catch (AssignManagerPlatformException e) {
			// If setOld manager throws an exception, catch it here and throw
			// user exception with message as
			// "Error while updating role, please contact system admin as rollback was not possible"
			throw new UserException(
					Messages.EXCEPTION_WHILE_ROLE_UPDATE_ROLLBACK, message);
		}
	}

	private void checkForRES(UserDto userDto, String companyId) {
		if (!accessUtil.isFourDot5Admin(userDto.getUserDetails().getRole())
				&& !accessUtil.isCompanyAdmin(userDto.getUserDetails()
						.getRole())) {
			if (!userService.checkRoleEntityScopeIsDefined(companyId)) {
				throw new RoleEntityScopeConflictException(
						Messages.ROLE_ENTITY_SCOPE_NOT_DEFINED,
						Messages.ROLE_ENTITY_SCOPE_NOT_DEFINED);
			}
		}
	}

	private UserDto getReporteeCount(User user, UserDto userDto) {
		if (accessUtil.canRoleHaveReportees(user.getRole().getId())) {
			int count = userService.getReporteeList(
					userDto.getUserDetails().getUserId()).size();
			userDto.setReporteeCount(count);
		}
		return userDto;
	}

	private boolean checkForRoleUpdate(UserDto userDto, String roleId,
			String companyId, User oldUserDetails) {
		try {
			if (userDto.getUserDetails().getUserId() != null) {

				int count = userService.getReporteeList(oldUserDetails.getId())
						.size();

				if (!accessUtil.canRoleBeUpdated(oldUserDetails, roleId, count)) {
					throw new UserConflictException(Messages.REPORTEES_ASSIGN,
							Messages.REPORTEES_ASSIGN);
				}

				return managerService.removeManager(userDto.getUserDetails()
						.getUserId(), userDto);
			}

		} catch (RemoveManagerPlatformException e) {
			LOGGER.error("error while removing manager ");
			// If there is an exception while removing the manager, catch the
			// exception and throw User exception with message
			// "Error while updating role, please try to update again"
			throw new UserException(Messages.EXCEPTION_WHILE_ROLE_UPDATE,
					userDto.getUserDetails().getRole().getDescription());
		}
		return false;
	}

	private void actionForRoleUpdate(User oldUserDetails, UserDto userDto,
			String companyId, User user) {
		// when role is updated
		if (oldUserDetails != null
				&& !oldUserDetails.getRole().getId()
						.equals(userDto.getUserDetails().getRole().getId())) {
			if (!accessUtil.isCompanyAdmin(userDto.getUserDetails().getRole())) {
				userScopeService.updateClientListWhenRoleOrManagerUpdate(user,
						null, companyId, false);
			} else {
				// role updated to company admin
				UserScope scope = UserScope.setScopeForCompanyAdmin();
				apiUserExecutor.setScope(scope, user.getId());

			}
		}
	}

}
