package com.fourdotfive.missioncontrol.user;

import com.fourdotfive.missioncontrol.dtos.candidate.DeleteDTO;
import com.fourdotfive.missioncontrol.dtos.candidate.UserDTO;
import com.fourdotfive.missioncontrol.dtos.company.ClientBUInfo;
import com.fourdotfive.missioncontrol.dtos.job.NotifyUserDTO;
import com.fourdotfive.missioncontrol.dtos.user.UserFilterDTO;
import com.fourdotfive.missioncontrol.dtos.user.VendorRecruiterRequestDto;
import com.fourdotfive.missioncontrol.dtos.user.VendorRecruitersCollectionRequestDto;
import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.user.*;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.Map;

public interface UserApiExecutor {

	/**
	 * To Get Roles, this method is called.
	 *
	 * @return response from platform
	 */
	List<Role> getAllRoles();

	/**
	 * To Set Manager to this user, this method is called.
	 *
	 * @return response from platform
	 */
	HttpStatus setManager(String userId, String managerId);

	/**
	 * To Save and Update user, this method is called.
	 *
	 * @return response from platform
	 */
	User saveUser(User user, String role, String companyId);

	/**
	 * To get UserDetails based on userID, this method is called.
	 *
	 * @return response from platform
	 */
	boolean deleteUser(String userId);

	String getOneTimePassword(String userId);

	/**
	 * To Get Entity Scope for the user, this method is called.
	 *
	 * @return response from platform
	 */
	AccessControlList getEnityScope(String userId);

	/**
	 * To Set Entity Scope for the user, this method is called.
	 *
	 * @return AccessControlList
	 *
	 */
	User setScope(UserScope scope, String userId);

	/**
	 * To Set Entity Scope for the user, this method is called.
	 *
	 * @return AccessControlList
	 *
	 */
	AccessControlList setScope(Scope scope, String userId);

	/**
	 * To get Reportees based on userID, this method is called.
	 *
	 * @return List<User>
	 */
	List<User> getReporteeList(String userId);

	/**
	 * To get UserDetails based on userID, this method is called.
	 *
	 * @return User
	 */
	User getUserById(String userId);

	/**
	 * To get managers for a user, this method is called.
	 *
	 * @return List<TeamMemeberDto>
	 */
	User getManager(String userId);

	/**
	 * To get managers for a user, this method is called.
	 *
	 * @return List<TeamMemeberDto>
	 */
	User getRecipientUser(String userId);

	/**
	 * To get Forwarded From Recipients, this method is called.
	 *
	 * @return List<UserForwardedReference>
	 */
	List<UserForwardedReference> getForwardedFromRecipients(String userId);

	/**
	 * To get Company admins, this method is called.
	 *
	 * @return List<TeamMemeberDto>
	 */
	List<User> getCompanyAdmins(String companyId);

	/**
	 * To remove manager
	 *
	 */
	boolean removeManager(String userId);


	/**
	 * To get Users based on role, this method is called.
	 *
	 * @return List<User>
	 */
	List<User> getUserListBasedOnRole(String roleId, String companyId);

	boolean addForwardedFromRecipient(UserRecipient userRecipient);

	/**
	 * To remove user reference to the forward recipient
	 */
	boolean removeForwardedFromRecipient(UserRecipient userRecipient);


    User verifyUsername(String email);

	String getTokenPayload(String userId, String token);

    String getFilteredUserDetails(UserFilterDTO userFilter);

	String deleteUser(DeleteDTO deleteDTO);

	String getUserDeleteInfo(String userId);

	String resendInvite(String userId);

	String getUsersOfSharedRequisition(String requisitionId);

	String getNotifyUsersOfRequisition(NotifyUserDTO notifyUserDTO);

    String getVendorRecruiters(VendorRecruiterRequestDto request);

	String getVendorRecruitersOfSharedRequisition(String requisitionId, String vendorId);

	List<User> getPossibleManagersForHiringManager(String companyId, String userId);

	UserLoggedInStatus getUserLoggedInStatus(String userId);

	Map<String, List<User>> getVendorRecruiters(VendorRecruitersCollectionRequestDto request);

	Map<String, List<User>> getAssignedHiringManagersAndRecruiters(String requisitionId);

	Map<String, List<User>> getAssignedVendorsAndRecruiters(String requisitionId);

    List<UserDTO> getCompanyUsersForActivities(String companyId);

	Map<String, ClientBUInfo>  setClientBuHierarchy(String userId, String companyId);

	List<User> getAllPermissibleUsers(List<String> userIds, String companyId);

	List<User> getAllRecruitersOrHiringManagers(String userId, String companyId, List<String> userIds);

	boolean hasAccessToClientOrBu(String userId, String clientOrBuId);

	String getSubordinatesByBU(String userId, String buId);

	List<Company> getAllClientOrBusOfUser(String userId, String companyId);

	String getUserByToken(String token);
 }
