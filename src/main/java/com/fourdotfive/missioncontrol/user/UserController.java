package com.fourdotfive.missioncontrol.user;

import com.fourdotfive.missioncontrol.common.AppConstants;
import com.fourdotfive.missioncontrol.common.CommonUtil;
import com.fourdotfive.missioncontrol.common.TokenDetails;
import com.fourdotfive.missioncontrol.dtos.candidate.DeleteDTO;
import com.fourdotfive.missioncontrol.dtos.candidate.UserDTO;
import com.fourdotfive.missioncontrol.dtos.company.AccessControlDto;
import com.fourdotfive.missioncontrol.dtos.company.CompanyDto;
import com.fourdotfive.missioncontrol.dtos.job.NotifyUserDTO;
import com.fourdotfive.missioncontrol.dtos.user.DashBoardDto;
import com.fourdotfive.missioncontrol.dtos.user.TeamMemberDto;
import com.fourdotfive.missioncontrol.dtos.user.UserDto;
import com.fourdotfive.missioncontrol.dtos.user.UserFilterDTO;
import com.fourdotfive.missioncontrol.dtos.user.*;
import com.fourdotfive.missioncontrol.exception.PlatformException;
import com.fourdotfive.missioncontrol.pojo.user.Role;
import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.requisition.RequisitionService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author shalini, chakravarthy
 */
@RestController
@RequestMapping("/api/user")
public class UserController {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(UserController.class);

    @Autowired
    private UserService userService;
    @Autowired
    private RequisitionService requisitionService;

    /**
     * To Get Roles based on role, this method is called.
     *
     * @return List<Role>
     */
    @PreAuthorize("@accessControlImp.canGetRoles(principal)")
    @RequestMapping(value = "/getcreateroles/{roleid}/{companytype}", method = RequestMethod.GET)
    public ResponseEntity<List<Role>> getCreateRoles(
            @PathVariable(value = "roleid") String roleid,
            @PathVariable(value = "companytype") String companytype) {
        List<Role> response = userService.getCreateRoles(roleid, companytype);
        return new ResponseEntity<List<Role>>(response, HttpStatus.OK);
    }

    /**
     * To Get Roles based on role, this method is called.
     *
     * @return List<Role>
     */
    @PreAuthorize("@accessControlImp.canGetRoles(principal)")
    @RequestMapping(value = "/getupdateroles/{roleid}/{companytype}", method = RequestMethod.GET)
    public ResponseEntity<List<Role>> getUpdateRoles(
            @PathVariable(value = "roleid") String roleid,
            @PathVariable(value = "companytype") String companytype,
            HttpServletRequest request) {
        final String authHeader = request.getHeader(AppConstants.AUTHORIZATION);
        TokenDetails tokenDetails = CommonUtil.getTokenDetails(authHeader);
        List<Role> response = userService.getUpdateRoles(roleid, companytype,
                tokenDetails);
        return new ResponseEntity<List<Role>>(response, HttpStatus.OK);
    }

    /**
     * To Get User Scope, this method is called.
     *
     * @return EntityScope
     */
    @PreAuthorize("@accessControlImp.canGetUserEnitityScope(principal)")
    @RequestMapping(value = "/getuserentityscope/{userid}/{companyid}/{roleid}", method = RequestMethod.GET)
    public ResponseEntity<AccessControlDto> getUserEnitityScope(
            @PathVariable(value = "userid") String userId,
            @PathVariable(value = "companyid") String companyId,
            @PathVariable(value = "roleid") String roleId,
            HttpServletRequest request) {
        final String authHeader = request.getHeader(AppConstants.AUTHORIZATION);
        TokenDetails tokenDetails = CommonUtil.getTokenDetails(authHeader);
        AccessControlDto response = userService.getUserEnityScope(userId,
                companyId, roleId, tokenDetails);

        return new ResponseEntity<AccessControlDto>(response, HttpStatus.OK);
    }

    /**
     * To Get User Scope, this method is called.
     *
     * @return EntityScope
     */
    @PreAuthorize("@accessControlImp.canGetUserEnitityScope(principal)")
    @RequestMapping(value = "/getuserclientsorbu/{userid}/{companyid}", method = RequestMethod.GET)
    public ResponseEntity<AccessControlDto> getUserClientsOrBU(
            @PathVariable(value = "userid") String userId,
            @PathVariable(value = "companyid") String companyId) {
        AccessControlDto response = userService.getUserClientsOrBU(companyId,
                userId);

        return new ResponseEntity<AccessControlDto>(response, HttpStatus.OK);
    }

    /**
     * To Get EntityScope for a role, this method is called.
     *
     * @return EntityScope
     */
    @PreAuthorize("@accessControlImp.canCheckRoleEnitityScope(principal)")
    @RequestMapping(value = "/checkroleentityscope/{companyid}", method = RequestMethod.GET)
    public ResponseEntity<Boolean> checkRoleEnitityScope(
            @PathVariable(value = "companyid") String companyId) {

        boolean response = userService.checkRoleEntityScopeIsDefined(companyId);

        return new ResponseEntity<Boolean>(response, HttpStatus.OK);
    }

    /**
     * To Set EntityScope for a user, this method is called.
     *
     * @return EntityScope
     */
    @PreAuthorize("@accessControlImp.canSetScope(principal)")
    @RequestMapping(value = "/setuserentityscope/{userid}/{companyid}", method = RequestMethod.POST)
    public ResponseEntity<UserDto> setUserEntityScope(
            @PathVariable(value = "userid") String userId,
            @PathVariable(value = "companyid") String companyId,
            @RequestBody AccessControlDto scope) {

        UserDto response = userService.setUserScope(scope, userId, companyId);

        return new ResponseEntity<UserDto>(response, HttpStatus.OK);
    }

    /**
     * To Get Team members of a user, this method is called.
     *
     * @return List<TeamMemeberDto>
     */
    @PreAuthorize("@accessControlImp.canGetTeamMembers(principal)")
    @RequestMapping(value = "/getteammembers/{companyid}", method = RequestMethod.GET)
    public ResponseEntity<List<TeamMemberDto>> getTeamMembers(
            @PathVariable(value = "companyid") String companyId,
            HttpServletRequest request) {

        final String authHeader = request.getHeader(AppConstants.AUTHORIZATION);
        TokenDetails tokenDetails = CommonUtil.getTokenDetails(authHeader);
        List<TeamMemberDto> response = userService.getTeamMembers(
                tokenDetails.getUserId(), companyId);
        return new ResponseEntity<List<TeamMemberDto>>(response, HttpStatus.OK);
    }

    /**
     * To Get UserDetails, this method is called.
     *
     * @return UserDto
     */
    @PreAuthorize("@accessControlImp.canGetUserDetails(principal)")
    @RequestMapping(value = "/getuserdetails/{userid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> getUserDetails(
            @PathVariable(value = "userid") String userId) {

        UserDto response = userService.getUserDtoById(userId);
        return new ResponseEntity<UserDto>(response, HttpStatus.OK);
    }

    /**
     * To Save User, this method is called.
     *
     * @return UserDto
     */
    @PreAuthorize("@accessControlImp.canSaveUser(principal)")
    @RequestMapping(value = "/saveuser/{companyid}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> saveUser(@RequestBody UserDto userDto,
                                            @PathVariable(value = "companyid") String companyId,
                                            HttpServletRequest request) {
        UserDto.validateFields(userDto);
        final String authHeader = request.getHeader(AppConstants.AUTHORIZATION);
        TokenDetails tokenDetails = CommonUtil.getTokenDetails(authHeader);
        UserDto response = userService.saveUser(userDto, tokenDetails, userDto
                .getUserDetails().getRole().getId(), companyId);
        return new ResponseEntity<UserDto>(response, HttpStatus.OK);
    }

    /**
     * To update User, this method is called.
     *
     * @return UserDto
     */
    @PreAuthorize("@accessControlImp.canUpdateUser(principal)")
    @RequestMapping(value = "/updateuser/{companyid}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> updateUser(@RequestBody UserDto userDto,
                                              @PathVariable(value = "companyid") String companyId,
                                              HttpServletRequest request) {
        UserDto.validateFields(userDto);
        final String authHeader = request.getHeader(AppConstants.AUTHORIZATION);
        TokenDetails tokenDetails = CommonUtil.getTokenDetails(authHeader);
        UserDto response = userService.updateUser(userDto, tokenDetails,
                userDto.getUserDetails().getRole().getId(), companyId);
        return new ResponseEntity<UserDto>(response, HttpStatus.OK);
    }

    /**
     * To assign manager to a user, this method is called.
     *
     * @return UserDto
     */
    @PreAuthorize("@accessControlImp.canAssignManager(principal)")
    @RequestMapping(value = "/assignmanager/{managerid}/{userid}/{companyid}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> assignManager(
            @PathVariable(value = "managerid") String managerId,
            @PathVariable(value = "userid") String userId,
            @PathVariable(value = "companyid") String companyId) {

        UserDto response = userService.setManager(userId, managerId, companyId);
        return new ResponseEntity<UserDto>(response, HttpStatus.OK);
    }

    /**
     * To Delete user, this method is called.
     *
     * @return String
     */
    @PreAuthorize("@accessControlImp.canDeleteUser(principal)")
    @RequestMapping(value = "/deleteuser/{userid}", method = RequestMethod.GET)
    public ResponseEntity<Boolean> deleteUser(
            @PathVariable(value = "userid") String userId) {

        boolean response = userService.deleteUser(userId);
        return new ResponseEntity<Boolean>(response, HttpStatus.OK);
    }

    /**
     * To Get reportees of a user, this method is called.
     *
     * @return List<TeamMemeberDto>
     */
    @PreAuthorize("@accessControlImp.canGetReportees(principal)")
    @RequestMapping(value = "/getreportees/{userid}", method = RequestMethod.GET)
    public ResponseEntity<List<TeamMemberDto>> getReportees(
            @PathVariable(value = "userid") String userId) {
        List<TeamMemberDto> response = null;
        response = userService.getReporteesWithForwardRecipient(userId);

        return new ResponseEntity<List<TeamMemberDto>>(response, HttpStatus.OK);
    }

    /**
     * To Get Logged in User details, this method is called.
     *
     * @return TeamMemeberDto
     */
    @PreAuthorize("@accessControlImp.canLoggedInUserDetails(principal)")
    @RequestMapping(value = "/getloggedinuserdetails", method = RequestMethod.GET)
    public ResponseEntity<TeamMemberDto> getLoggedInUserDetails(
            HttpServletRequest request) {

        final String authHeader = request.getHeader(AppConstants.AUTHORIZATION);
        TokenDetails tokenDetails = CommonUtil.getTokenDetails(authHeader);
        UserDto response = userService.getUserDtoById(tokenDetails.getUserId());
        TeamMemberDto dto = TeamMemberDto.mapUserDtoToTeamMember(response);
        return new ResponseEntity<TeamMemberDto>(dto, HttpStatus.OK);
    }

    /**
     * Assigns a company for a 4dot5admin, this method is called
     *
     * @return StubUser
     */
    @PreAuthorize("@accessControlImp.canAssignCompanyFor4dot5Admin(principal)")
    @RequestMapping(value = "/updatecompanyfor4dot5admin/{userid}/{companyid}", method = RequestMethod.POST)
    public ResponseEntity<CompanyDto> assignCompanyFor4dot5Admin(
            @PathVariable(value = "userid") String userId,
            @PathVariable(value = "companyid") String companyId,
            @RequestParam(value = "oldfourdotfiveadmin") String olduserId,
            HttpServletRequest request) {

        final String authHeader = request.getHeader(AppConstants.AUTHORIZATION);
        TokenDetails tokenDetails = CommonUtil.getTokenDetails(authHeader);

        CompanyDto companyDto = userService.assignCompanyFor4dot5Admin(userId,
                olduserId, tokenDetails, companyId);

        return new ResponseEntity<CompanyDto>(companyDto, HttpStatus.OK);
    }

    /**
     * To Get ForwardRecipient Users details, this method is called.
     *
     * @return List<TeamMemeberDto>
     */
    @PreAuthorize("@accessControlImp.canGetForwardRecipients(principal)")
    @RequestMapping(value = "/getforwardrecipientuser/{userid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TeamMemberDto>> getForwardRecipients(
            @PathVariable(value = "userid") String userId,
            HttpServletRequest request) {
        List<TeamMemberDto> response = userService
                .getMyForwardRecipients(userId);
        return new ResponseEntity<List<TeamMemberDto>>(response, HttpStatus.OK);
    }

    /**
     * To Get PossibleManagers while Updating User, this method is called.
     *
     * @param userId
     * @param role
     * @return
     */
    @PreAuthorize("@accessControlImp.canGetPossibleManagersForUpdate(principal)")
    @RequestMapping(value = "/getmanagers/{userid}/{roleId}", method = RequestMethod.GET)
    public ResponseEntity<List<TeamMemberDto>> getPossibleManagersForUpdate(
            @PathVariable(value = "userid") String userId,
            @PathVariable(value = "roleId") String roleId,
            HttpServletRequest request) {

        final String authHeader = request.getHeader(AppConstants.AUTHORIZATION);
        TokenDetails tokenDetails = CommonUtil.getTokenDetails(authHeader);

        List<TeamMemberDto> managerList = userService
                .getPossibleManagersForUpdate(userId, roleId,
                        tokenDetails.getUserId());

        return new ResponseEntity<List<TeamMemberDto>>(managerList,
                HttpStatus.OK);

    }

    /**
     * To Get PossibleManagers while Updating User, this method is called.
     *
     * @param userId
     * @param role
     * @return
     */
    @PreAuthorize("@accessControlImp.canCreateUserEntityScope(principal)")
    @RequestMapping(value = "/getcompeleteuserdetails/{userid}", method = RequestMethod.GET)
    public ResponseEntity<User> getCompleteUserDetails(
            @PathVariable(value = "userid") String userId) {

        User user = userService.getUserById(userId);

        return new ResponseEntity<User>(user, HttpStatus.OK);

    }

    /**
     * To Get PossibleManagers while demoting company admin, this method is
     * called.
     *
     * @param userId
     * @param role
     * @return
     */
    @PreAuthorize("@accessControlImp.canGetPossibleManagersForUpdate(principal)")
    @RequestMapping(value = "/getmanagerswhiledemotingcompanyadmin/{userid}/{companyid}", method = RequestMethod.GET)
    public ResponseEntity<List<TeamMemberDto>> getPossibleManagersForUpdatingCompanyAdmin(
            @PathVariable(value = "userid") String userId,
            @PathVariable(value = "companyid") String companyId) {

        List<TeamMemberDto> managerList = userService
                .getPossibleManagersForUpdatingCompanyAdmin(userId, companyId);

        return new ResponseEntity<List<TeamMemberDto>>(managerList,
                HttpStatus.OK);

    }

    /**
     * To Get PossibleManagers while Updating User, this method is called.
     *
     * @param userId
     * @param role
     * @return
     */
    @RequestMapping(value = "/getdashboarddata/{userid}", method = RequestMethod.GET)
    public ResponseEntity<DashBoardDto> getDashboardDetails(
            @PathVariable(value = "userid") String userId) {

        DashBoardDto user = userService.getUserDashBoardData(userId);

        return new ResponseEntity<DashBoardDto>(user, HttpStatus.OK);

    }

    /**
     * Get reportee of all hirerachy under this user
     *
     * @param userId
     * @param role
     * @return
     */
    @RequestMapping(value = "/getuserunder/{userid}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getUserUnder(
            @PathVariable(value = "userid") String userId,
            @RequestBody Role role) {

        userService.getAllReporteeUnder(userId, role);
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    /**
     * To Save User, this method is called.
     *
     * @return UserDto
     */
    @PreAuthorize("@accessControlImp.canSaveForwardRecipient(principal)")
    @RequestMapping(value = "/saveforwardrecipient/{companyid}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> saveForwardRecipient(
            @RequestBody UserDto userDto,
            @PathVariable(value = "companyid") String companyId,
            HttpServletRequest request) {
        UserDto.validateFields(userDto);
        final String authHeader = request.getHeader(AppConstants.AUTHORIZATION);
        TokenDetails tokenDetails = CommonUtil.getTokenDetails(authHeader);
        UserDto response = userService.saveForwardRecipient(userDto,
                tokenDetails, userDto.getUserDetails().getRole().getId(),
                companyId);
        return new ResponseEntity<UserDto>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/gettokenpayload/{token}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getTokenPayload(@PathVariable(value = "token") String token) {
        if (StringUtils.isEmpty(token)) {
            throw new IllegalArgumentException("token cannot be null");
        }
        return userService.getTokenPayload(token);
    }

    @RequestMapping(value = "/getfiltereduserdetails", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getFilteredUserDetails(@RequestBody UserFilterDTO userFilter) {
        if (userFilter == null)
            throw new IllegalArgumentException("Filter parameters can not be null");

        return userService.getFilteredUserDetails(userFilter);
    }

    @RequestMapping(value = "/deleteinfos/{userId}", method = RequestMethod.GET)
    public String getUserDeleteInfo(@PathVariable(value = "userId") String userId) {
        if (StringUtils.isEmpty(userId)) {
            throw new PlatformException("User id can not be null.");
        }
        return userService.getUserDeleteInfo(userId);
    }

    @RequestMapping(value = {"/delete"}, method = RequestMethod.DELETE)
    public String deleteUser(@RequestBody DeleteDTO deleteDTO) {
        if (deleteDTO == null) {
            throw new PlatformException("Can not delete the user.");
        }
        if (StringUtils.isEmpty(deleteDTO.getId())) {
            throw new PlatformException("User id can not be null.");
        }
        return userService.deleteUser(deleteDTO);
    }

    @RequestMapping(value = {"/getOtp/{userId}"}, method = RequestMethod.GET)
    public String getOneTimePassword(@PathVariable(value = "userId") String userId) {
        if (StringUtils.isEmpty(userId)) {
            throw new PlatformException("User id can not be null.");
        }
        return userService.getOneTimePassword(userId);
    }

    @RequestMapping(value = "/resendinvite/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> resendInvite(@PathVariable(value = "userId") String userId,
                                               HttpServletRequest request) {
        String response = userService.resendInvite(userId);
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @RequestMapping(value = "getusersofsharedrequisition/{requisitionId}", method = {RequestMethod.GET})
    public String getUsersOfSharedRequisition(@PathVariable String requisitionId) throws Exception {
        if (StringUtils.isEmpty(requisitionId)) {
            throw new PlatformException("Requisition id can not be null.");
        }
        return userService.getUsersOfSharedRequisition(requisitionId);
    }

    @RequestMapping(value = "getnotifyusersofrequisition", method = {RequestMethod.POST})
    public String getNotifyUsersOfRequisition(@RequestBody NotifyUserDTO notifyUserDTO) throws Exception {
        return userService.getNotifyUsersOfRequisition(notifyUserDTO);

    }

    @RequestMapping(value = "/vendor/recruiters", method = {RequestMethod.POST})
    public String getVendorRecruiters(
            @RequestBody VendorRecruiterRequestDto request) throws Exception {
        return userService.getVendorRecruiters(request);
    }

    @RequestMapping(value = "/sharedrequisition/vendor/recruiters/{requisitionId}/{vendorId}", method = {RequestMethod.GET})
    public String getVendorRecruitersOfSharedRequisition(
            @PathVariable String vendorId,
            @PathVariable String requisitionId) throws Exception {
        if (StringUtils.isEmpty(requisitionId))
            throw new PlatformException("Requisition id can not be null.");

        if (StringUtils.isEmpty(vendorId))
            throw new PlatformException("Vendor id can not be null.");

        return userService.getVendorRecruitersOfSharedRequisition(requisitionId, vendorId);
    }

    @RequestMapping(value = "/getpossiblemanagersforhiringmanager/{companyId}", method = RequestMethod.GET)
    public ResponseEntity<List<TeamMemberDto>> getPossibleManagersForUpdate(@PathVariable(value = "companyId") String companyId,
                                                                            @RequestParam(value = "userId", required = false) String userId) {
        List<TeamMemberDto> managerList = userService.getPossibleManagersForHiringManager(companyId, userId);

        return new ResponseEntity<>(managerList, HttpStatus.OK);

    }

    @RequestMapping(value = "job/hiringmanagers/recruiters", method = RequestMethod.POST)
    public ResponseEntity<RecruitersAndManagersCollectionDto> getHiringmanagersAndRecruitesOfJob(@RequestBody TeamMemberRequestDto request) throws Exception {
        if (StringUtils.isEmpty(request.getRequisitionId()))
            throw new IllegalArgumentException("Requisition id can not be null");
        RecruitersAndManagersCollectionDto teamMembers = userService.getRecruitersAndHiringManagers(request);
        return new ResponseEntity<>(teamMembers, HttpStatus.OK);
    }

    @RequestMapping(value = "/getcompanyusersforactivities/{companyId}", method = RequestMethod.GET)
    public List<UserDTO> getCompanyUsersForActivities(@PathVariable(value = "companyId") String companyId) {
        if (StringUtils.isEmpty(companyId)) {
            throw new PlatformException("Company Id can not be null.");
        }
        return userService.getCompanyUsersForActivities(companyId);
    }

    @RequestMapping(value = "/getsubordinatesbybu/{userId}/{buId}", method = {RequestMethod.GET})
    public @ResponseBody
    String getSubordinatesByBU(@PathVariable(value = "userId") String userId,
                           @PathVariable(value = "buId") String buId) throws Exception {

        if (StringUtils.isEmpty(userId)) {
            throw new PlatformException("User Id can not be null.");
        }

        return userService.getSubordinatesByBU(userId, buId);
    }

    @RequestMapping(value = "getuserbytoken/{token}", method = {RequestMethod.GET})
    public @ResponseBody
    String getUserByTokenId(@PathVariable(value = "token") String token) throws Exception {
        if (StringUtils.isEmpty(token))
            throw new PlatformException("token can not be null");

        return userService.getUserByToken(token);
    }

}
