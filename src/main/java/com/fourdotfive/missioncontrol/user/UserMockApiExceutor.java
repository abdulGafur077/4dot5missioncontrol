package com.fourdotfive.missioncontrol.user;

import com.fourdotfive.missioncontrol.dtos.user.DashBoardDto;

public interface UserMockApiExceutor {

	/**
	 * To get Dashboard data based on userID, this method is called.
	 * 
	 * @return User
	 */
	DashBoardDto getUserDashBoardData(String userId);

}
