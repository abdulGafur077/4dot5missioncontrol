package com.fourdotfive.missioncontrol.user;

import com.fourdotfive.missioncontrol.common.TokenDetails;
import com.fourdotfive.missioncontrol.dtos.candidate.DeleteDTO;
import com.fourdotfive.missioncontrol.dtos.candidate.UserDTO;
import com.fourdotfive.missioncontrol.dtos.company.AccessControlDto;
import com.fourdotfive.missioncontrol.dtos.company.ClientBUInfo;
import com.fourdotfive.missioncontrol.dtos.company.CompanyDto;
import com.fourdotfive.missioncontrol.dtos.user.*;
import com.fourdotfive.missioncontrol.dtos.job.NotifyUserDTO;
import com.fourdotfive.missioncontrol.dtos.user.DashBoardDto;
import com.fourdotfive.missioncontrol.dtos.user.TeamMemberDto;
import com.fourdotfive.missioncontrol.dtos.user.UserDto;
import com.fourdotfive.missioncontrol.dtos.user.UserFilterDTO;
import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.user.AccessControlList;
import com.fourdotfive.missioncontrol.pojo.user.Role;
import com.fourdotfive.missioncontrol.pojo.user.Scope;
import com.fourdotfive.missioncontrol.pojo.user.User;

import java.util.List;
import java.util.Map;

/**
 * 
 * @author chakravarthy,shalini
 *
 */
public interface UserService {

	/**
	 * To Get Roles, this method is called.
	 * 
	 * @return response from platform
	 */
	List<Role> getCreateRoles(String roleId, String companyType);

	/**
	 * To Get Roles, this method is called.
	 * 
	 * @return response from platform
	 */
	List<Role> getUpdateRoles(String roleId, String companyType,
			TokenDetails tokenDetails);

	/**
	 * To Get Roles, this method is called.
	 * 
	 * @return response from platform
	 */
	List<Role> getAllRoles();

	/**
	 * To Set Manager to this user, this method is called.
	 * 
	 * @return response from platform
	 */
	UserDto setManager(String userId, String managerId, String companyId);

	/**
	 * To Get Team Members, this method is called.
	 * 
	 * @return response from platform
	 */
	List<TeamMemberDto> getTeamMembers(String userId, String companyId);

	/**
	 * To Save user, this method is called.
	 * 
	 * @return response from platform
	 */
	UserDto saveUser(UserDto user, TokenDetails tokenDetails, String role,
			String companyId);

	/**
	 * To Update user, this method is called.
	 * 
	 * @return response from platform
	 */
	UserDto updateUser(UserDto user, TokenDetails tokenDetails, String role,
			String companyId);

	/**
	 * To get UserDetails based on userID, this method is called.
	 * 
	 * @return response from platform
	 */
	boolean deleteUser(String userId);

	String getOneTimePassword(String userId);

	/**
	 * To Get Entity Scope for the user, this method is called.
	 * 
	 * @return response from platform
	 */
	AccessControlList getEnityScope(String userId);

	/**
	 * To Get Entity Scope for the user, this method is called.
	 * 
	 * @return response from platform
	 */
	AccessControlDto getUserEnityScope(String companyId, String userId,
			String roleId, TokenDetails tokenDetails);

	/**
	 * To Get client for the user, this method is called.
	 * 
	 * @return response from platform
	 */
	AccessControlDto getUserClientsOrBU(String companyId, String userId);

	/**
	 * To Set Entity Scope for the user, this method is called.
	 * 
	 * @return AccessControlList
	 * 
	 */
	UserDto setUserScope(AccessControlDto scope, String userId, String companyId);

	/**
	 * To Set Entity Scope for the user, this method is called.
	 * 
	 * @return AccessControlList
	 * 
	 */
	AccessControlList setScope(Scope scope, String userId);

	/**
	 * To get Reportees based on userID, this method is called.
	 * 
	 * @return List<TeamMemeberDto>
	 */
	List<TeamMemberDto> getReportees(String userId);

	/**
	 * To get Reportees based on userID, this method is called.
	 * 
	 * @return List<TeamMemeberDto>
	 */
	List<TeamMemberDto> getReporteesWithForwardRecipient(String userId);

	/**
	 * To get Reportees based on userID, this method is called.
	 * 
	 * @return List<User>
	 */
	List<User> getReporteeList(String userId);

	/**
	 * To Get Team Members, this method is called.
	 * 
	 * @return response from platform
	 */
	List<TeamMemberDto> getMyForwardRecipients(String userId);

	/**
	 * Assigns a company for 4dot5ADmin
	 * 
	 * @return CompanyDto
	 */
	CompanyDto assignCompanyFor4dot5Admin(String userId, String olduserId,
			TokenDetails tokenDetails, String companyId);

	/**
	 * To get UserDetails based on userID, this method is called.
	 * 
	 * @return User
	 */
	User getUserById(String userId);

	/**
	 * To get Dashboard data based on userID, this method is called.
	 * 
	 * @return User
	 */
	DashBoardDto getUserDashBoardData(String userId);

	/**
	 * To get Possible managers for a user, this method is called.
	 * 
	 * @return List<TeamMemeberDto>
	 */
	List<TeamMemberDto> getPossibleManagersForUpdate(String userId,
			String role, String loggedInUserId);

	/**
	 * To get Possible managers for a user, this method is called.
	 * 
	 * @return List<TeamMemeberDto>
	 */
	List<TeamMemberDto> getPossibleManagersForUpdatingCompanyAdmin(
			String userId, String companyId);

	/**
	 * To get user, this method is called.
	 * 
	 * @return List<TeamMemeberDto>
	 */
	UserDto getUserDtoById(String userId);

	/**
	 * To get managers for a user, this method is called.
	 * 
	 * @return List<TeamMemeberDto>
	 */
	User getManager(String userId);

	/**
	 * To get managers for a user, this method is called.
	 * 
	 * @return List<TeamMemeberDto>
	 */
	User getRecipientUser(String userId);

	/**
	 * Get all company admins
	 * 
	 * @param company
	 * @return
	 */
	List<User> getCompanyAdmins(Company company);

	/**
	 * Check if RES for a company is defeined
	 * 
	 * @param companyId
	 * @return
	 */
	boolean checkRoleEntityScopeIsDefined(String companyId);

	/**
	 * To get UserDto from user obj
	 * 
	 * @param user
	 * @return
	 */
	UserDto getUserDto(User user);

	/**
	 * To get Forward Recipient members
	 * 
	 * @param user
	 * @param userId
	 * @return
	 */
	List<User> getMyPeers(User user, String userId);

	/**
	 * Save,remove and update forward recipient
	 * 
	 * @param user
	 * @param tokenDetails
	 * @param role
	 * @param companyId
	 * @return
	 */
	UserDto saveForwardRecipient(UserDto user, TokenDetails tokenDetails,
			String role, String companyId);

	/**
	 * TO get all reportees under a manager
	 * 
	 * @param userId
	 * @param role
	 * @return
	 */
	List<User> getAllReporteeUnder(String userId, Role role);

	/**
	 * Get reportees count
	 * 
	 * @param userId
	 * @return
	 */
	int getReporteesCount(String userId);

    User getCurrentLoggedInUser();

	String getTokenPayload(String token);

    String getFilteredUserDetails(UserFilterDTO userFilter);

    String deleteUser(DeleteDTO deleteDTO);

    String getUserDeleteInfo(String userId);

	String resendInvite(String userId);

	List<TeamMemberDto> getRecruitersList(String userId, String companyId);

	String getUsersOfSharedRequisition(String requisitionId);

	String getNotifyUsersOfRequisition(NotifyUserDTO notifyUserDTO);

	String getVendorRecruiters(VendorRecruiterRequestDto request);

	String getVendorRecruitersOfSharedRequisition(String requisitionId, String vendorId);

	List<TeamMemberDto> getPossibleManagersForHiringManager(String companyId, String userId);

	boolean isAdminUser(User user);

	RecruitersAndManagersCollectionDto getRecruitersAndHiringManagers(TeamMemberRequestDto request);

	Map<String, List<User>> getVendorRecruiters(VendorRecruitersCollectionRequestDto request);

	Map<String, List<User>> getAssignedVendorsAndRecruiters(String requisitionId);

	Map<String, List<User>> getAssignedHiringManagersAndRecruiters(String requisitionId);

	List<UserDTO> getCompanyUsersForActivities(String companyId);

	Map<String, ClientBUInfo>  setClientBuHierarchy(String userId, String companyId);

	List<User> getAllPermissibleUsers(List<User> users, String companyId);

	List<User> getAllRecruitersOrHiringManagers(String userId, String companyId, List<String> userIds);

	boolean hasAccessToClientOrBu(String userId, String clientOrBuId);

	String getSubordinatesByBU(String userId, String buId);

	String getUserByToken(String token);
}
