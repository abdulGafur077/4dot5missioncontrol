package com.fourdotfive.missioncontrol.user;

import com.fourdotfive.missioncontrol.common.*;
import com.fourdotfive.missioncontrol.company.CompanyService;
import com.fourdotfive.missioncontrol.dtos.candidate.DeleteDTO;
import com.fourdotfive.missioncontrol.dtos.candidate.UserDTO;
import com.fourdotfive.missioncontrol.dtos.company.*;
import com.fourdotfive.missioncontrol.dtos.employee.DepartmentDto;
import com.fourdotfive.missioncontrol.dtos.job.NotifyUserDTO;
import com.fourdotfive.missioncontrol.dtos.user.*;
import com.fourdotfive.missioncontrol.employee.EmployeeService;
import com.fourdotfive.missioncontrol.exception.AssignManagerPlatformException;
import com.fourdotfive.missioncontrol.forwardrecipient.ForwardRecipientService;
import com.fourdotfive.missioncontrol.manager.ManagerService;
import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.company.CompanyType;
import com.fourdotfive.missioncontrol.pojo.company.RoleEntityScope;
import com.fourdotfive.missioncontrol.pojo.user.*;
import com.fourdotfive.missioncontrol.requisition.RequisitionService;
import com.fourdotfive.missioncontrol.rolehierarchy.RoleHierarchy;
import com.fourdotfive.missioncontrol.security.SecurityUtils;
import com.fourdotfive.missioncontrol.setting.SettingService;
import com.fourdotfive.missioncontrol.teammembers.TeamMemberService;
import com.fourdotfive.missioncontrol.userentityscope.FetchUserEntityScopeService;
import com.fourdotfive.missioncontrol.userentityscope.UserScopeService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

import static com.fourdotfive.missioncontrol.dtos.user.TeamMemberEnum.*;
import static com.fourdotfive.missioncontrol.pojo.user.Role.roleNameComparator;

@Service
public class UserServiceImp implements UserService {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(UserServiceImp.class);

    @Autowired
    private CompanyService companyService;

    @Autowired
    private UserMockApiExceutor userMockApiExecutor;

    @Autowired
    private RoleHierarchy roleHierarchy;

    @Autowired
    private AccessUtil accessUtil;

    @Autowired
    private UserApiExecutor apiUserExecutor;

    @Autowired
    private FetchUserEntityScopeService fetchUserScopeService;

    @Autowired
    private TeamMemberService teamMemeberService;

    @Autowired
    private ManagerService managerService;

    @Autowired
    private ForwardRecipientService forwardRecipientService;

    @Autowired
    private UserScopeService userScopeService;

    @Autowired
    private UpdateUserService updateUserService;
    @Autowired
    private CreateUserService createUserService;
    @Autowired
    private SettingService settingService;
    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private RequisitionService requisitionService;

    // TODO - Get role hierarchy from database, hard coded as of now
    @Override
    public List<Role> getCreateRoles(String roleId, String companyType) {

        return roleHierarchy.getCreateRoles(
                apiUserExecutor.getAllRoles(), companyType, roleId);
    }

    @Override
    public List<Role> getUpdateRoles(String roleId, String companyType,
                                     TokenDetails tokenDetails) {
        List<Role> rolesList;
        if (tokenDetails.getRoleId().equals(AppConstants.SUPERUSER_ROLEID)
                || tokenDetails.getRoleId().equals(
                AppConstants.FOURDOT5ADMIN_ROLEID)) {
            rolesList = roleHierarchy.getUpdateRoles(
                    apiUserExecutor.getAllRoles(), companyType, roleId);
        } else {
            rolesList = roleHierarchy.getCreateRoles(
                    apiUserExecutor.getAllRoles(), companyType, roleId);
        }
        if (rolesList.size() > 1) {
            Collections.sort(rolesList, roleNameComparator);
        }
        return rolesList;
    }

    @Override
    public List<Role> getAllRoles() {

        return apiUserExecutor.getAllRoles();
    }

    @Override
    public UserDto saveUser(UserDto userDto, TokenDetails tokenDetails,
                            String roleId, String companyId) {

        // Setting audit details
        userDto = AuditDto.setAuditDetails(userDto, tokenDetails);
        if (userDto.getUserDetails().getUserId() == null) {
            return createUserService.saveUser(userDto, roleId, companyId,
                    tokenDetails);
        } else {
            return updateUserService.updateUser(userDto, roleId, companyId,
                    tokenDetails);
        }
    }

    @Override
    public UserDto updateUser(UserDto userDto, TokenDetails tokenDetails,
                              String roleId, String companyId) {

        // Setting audit details
        userDto = AuditDto.setAuditDetails(userDto, tokenDetails);
        return updateUserService.updateUser(userDto, roleId, companyId,
                tokenDetails);
    }

    @Override
    public List<TeamMemberDto> getTeamMembers(String userId, String companyId) {

        return teamMemeberService.getTeamMembers(userId, companyId);
    }

    @Override
    public UserDto getUserDto(User user) {

        UserDto dto = UserDto.mapUserToUserDto(user);

        User manager = getManager(user.getId());
        if (manager != null) {
            dto.getUserDetails().setManager(
                    TeamMemberDto.mapUserToTeamMember(manager));
        }

        User recipient = getRecipientUser(user.getId());
        if (recipient != null) {
            if (dto.getUserForwardRecipientDetails() == null) {
                dto.setUserForwardRecipientDetails(new UserForwardRecipientDetailsDto());
            }
            dto.getUserForwardRecipientDetails().setForwardRecipient(
                    TeamMemberDto.mapUserToTeamMember(recipient));
        }

        if (accessUtil.isCompanyAdmin(dto.getUserDetails().getRole())) {
            Role role = dto.getUserDetails().getRole();
            dto.getUserDetails().setRole(role);
        }

        return dto;
    }

    @Override
    public User getManager(String userId) {

        return apiUserExecutor.getManager(userId);
    }

    @Override
    public List<User> getCompanyAdmins(Company company) {

        List<User> compAdmins = apiUserExecutor.getCompanyAdmins(company
                .getId());
        List<User> currentAdmins = new ArrayList<>();

        for (User temp : compAdmins) {
            if (!temp.getAccessControlList().getAccountState()
                    .equals(AppConstants.ARCHIVED)) {
                currentAdmins.add(temp);
            }
        }

        return currentAdmins;
    }

    @Override
    public User getUserById(String userId) {

        return apiUserExecutor.getUserById(userId);
    }

    @Override
    public UserDto getUserDtoById(String userId) {

        User user = getUserById(userId);
        UserDto userDto = getUserDto(user);
        if (accessUtil.canRoleHaveReportees(user.getRole().getId())) {
            int count = getReporteesCount(userId);
            userDto.setReporteeCount(count);
        }
        UserLoggedInStatus userLoggedInStatus = apiUserExecutor.getUserLoggedInStatus(userId);
        userDto.setIsBUsOrClientsAvailable(userLoggedInStatus.getIsBUsOrClientsAvailable());
        userDto.setIsBUOrClientListEmptyForUser(userLoggedInStatus.getIsBUOrClientListEmptyForUser());
        if(!StringUtils.isEmpty(user.getDepartment())){
            DepartmentDto departmentDto = employeeService.getDepartmentById(user.getDepartment());
            if(departmentDto != null){
                userDto.setDepartmentId(user.getDepartment());
                userDto.setDepartmentName(departmentDto.getName());
            }
        }
        if(!StringUtils.isEmpty(user.getBuId())){
            Company company = companyService.getCompany(user.getBuId());
            if(company != null){
                userDto.setBuId(user.getBuId());
                userDto.setBuName(company.getName());
            }
        }

        User manager = getManager(user.getId());
        if (manager != null) {
            UserManagerDTO userManagerDTO = new UserManagerDTO();
            userManagerDTO.setId(manager.getId());
            userManagerDTO.setFirstName(manager.getFirstname());
            userManagerDTO.setLastName(manager.getLastname());
            userDto.setManager(userManagerDTO);
        }

        return userDto;
    }

    @Override
    public AccessControlList getEnityScope(String userId) {

        return apiUserExecutor.getEnityScope(userId);
    }

    @Override
    public boolean deleteUser(String userId) {
        User user = getUserById(userId);
        int count = getReporteesCount(userId);

        if (count >= 1) {
            String userName = user.getFirstname();
            if (user.getLastname() != null) {
                userName = userName + " " +
                        user.getLastname();
            }
            throw new UserException(Messages.DELETE_USER_WITH_REPORTESS,
                    userName);
        } else {

            if (accessUtil.isFourDot5Admin(user.getRole())) {
                AccessControlList accessControlList = getEnityScope(userId);
                if (accessControlList.getCompanyList() != null
                        && accessControlList.getCompanyList().size() > 0) {
                    throw new UserException(Messages.DELETE_FOURDOTFIVE_ADMIN);
                }
            }
            forwardRecipientService.checkUserIsForwardRecipient(user);
            forwardRecipientService.removeBackwardPointer(user);
        }

        return apiUserExecutor.deleteUser(userId);
    }

    @Override
    public String getOneTimePassword(String userId) {
        return apiUserExecutor.getOneTimePassword(userId);
    }

    @Override
    public UserDto setManager(String userId, String managerId, String companyId) {
        try {
            return managerService.setManager(userId, managerId, companyId);
        } catch (AssignManagerPlatformException e) {
            throw new AssignManagerPlatformException(
                    Messages.INTERNAL_ERROR_MSG);
        }
    }

    @Override
    public List<TeamMemberDto> getReportees(String userId) {

        List<TeamMemberDto> teamMembers = new ArrayList<>();

        for (User u : getReporteeList(userId)) {
            if (accessUtil.isRecruitingManager(u.getRole())) {
                int count = getReporteesCount(u.getId());
                teamMembers.add(TeamMemberDto.mapUserToTeamMember(u,
                        AppConstants.CAN_EDIT, AppConstants.CAN_DELETE,
                        AppConstants.CAN_DRILLDOWN, count,
                        AppConstants.CAN_VIEW_NOTIFICATIONPREF));
            } else {
                teamMembers.add(TeamMemberDto.mapUserToTeamMember(u,
                        AppConstants.CAN_EDIT, AppConstants.CAN_DELETE,
                        AppConstants.CANNOT_DRILLDOWN,
                        AppConstants.NO_REPORTEES,
                        AppConstants.CAN_VIEW_NOTIFICATIONPREF));
            }
        }
        Collections.sort(teamMembers, new UserNameComparator());
        return teamMembers;
    }

    @Override
    public List<TeamMemberDto> getReporteesWithForwardRecipient(String userId) {
        List<TeamMemberDto> teamMembers = new ArrayList<>();
        // get reportee
        List<User> reporteeList = getReporteeList(userId);
        for (User reportee : reporteeList) {

            // check if reportee is rec manager
            if (accessUtil.isRecruitingManager(reportee.getRole()) || accessUtil.isHiringManagerRole(reportee.getRole())) {
                // get reportee count for reportee
                int count = getReporteesCount(reportee.getId());
                // check if this reportee has set his manager as forward
                // recipient
                TeamMemberDto memberDto = checkForForwardedRecipient(reportee,
                        userId, count);
                // set can drill down to true
                memberDto.setCanDrillDown(AppConstants.CAN_DRILLDOWN);
                memberDto
                        .setCanViewNotificationPref(AppConstants.CAN_VIEW_NOTIFICATIONPREF);
                teamMembers.add(memberDto);
            } else {
                // set cant drill down to true
                TeamMemberDto memberDto = checkForForwardedRecipient(reportee,
                        userId, AppConstants.NO_REPORTEES);
                memberDto.setCanDrillDown(AppConstants.CANNOT_DRILLDOWN);
                memberDto
                        .setCanViewNotificationPref(AppConstants.CAN_VIEW_NOTIFICATIONPREF);
                teamMembers.add(memberDto);
            }
        }
        Collections.sort(teamMembers, new UserNameComparator());
        return teamMembers;
    }

    private TeamMemberDto checkForForwardedRecipient(User reportee,
                                                     String managerId, int count) {
        TeamMemberDto teamMemberDto = new TeamMemberDto();
        // get forward recipient which was set to the reportee
        User forwardedUser = getRecipientUser(reportee.getId());
        // check if forward recipient is null and if not is he viewable
        if (forwardedUser != null && forwardedUser.getId() != null
                && forwardedUser.getId().equals(managerId)
                && CommonUtil.isForwardRecipientViewable(reportee)) {
            // set as forward recipient
            teamMemberDto = TeamMemberDto.mapUserToTeamMember(reportee,
                    AppConstants.CAN_EDIT, AppConstants.CAN_DELETE,
                    AppConstants.CAN_DRILLDOWN, count,
                    AppConstants.IS_A_FORWARDED_RECIPIENT,
                    AppConstants.CAN_VIEW_NOTIFICATIONPREF);

        } else {
            // set as normal team members
            teamMemberDto = TeamMemberDto.mapUserToTeamMember(reportee,
                    AppConstants.CAN_EDIT, AppConstants.CAN_DELETE,
                    AppConstants.CAN_DRILLDOWN, count,
                    AppConstants.CAN_VIEW_NOTIFICATIONPREF);
        }

        if (forwardedUser != null
                && CommonUtil.isForwardRecipientViewable(reportee)) {
            teamMemberDto.setForwardFromId(forwardedUser.getId());
            StringBuilder name = new StringBuilder(forwardedUser.getFirstname());
            if (forwardedUser.getLastname() != null) {
                name.append(" ").append(forwardedUser.getLastname());
            }
            teamMemberDto.setForwardFromUser(name.toString());
            teamMemberDto.setForwardRecipient(true);
        }

        return teamMemberDto;
    }

    @Override
    public List<User> getReporteeList(String userId) {
        List<User> userList = new ArrayList<>();
        LOGGER.info(getClass()+ " getReporteeList ");
        List<User> users = apiUserExecutor.getReporteeList(userId);
        if (!CollectionUtils.isEmpty(users)) {
            for (User user : users) {
                if (!accessUtil.isUserArchived(user)) {
                    userList.add(user);
                }
            }
        }
        return userList;
    }

    @Override
    public int getReporteesCount(String userId) {

        return getReporteeList(userId).size();
    }

    @Override
    public List<TeamMemberDto> getMyForwardRecipients(String userId) {
        return forwardRecipientService.getMyForwardRecipients(userId);
    }

    @Override
    public List<User> getMyPeers(User user, String userId) {

        List<User> userList = getReporteeList(user.getId());
        for (Iterator<User> iterator = userList.iterator(); iterator.hasNext(); ) {
            User obj = iterator.next();
            if (obj.getId().equals(userId)) {
                // Remove the current element from the iterator and the list.
                iterator.remove();
            }
        }
        return userList;
    }

    @Override
    public CompanyDto assignCompanyFor4dot5Admin(String userId,
                                                 String olduserId, TokenDetails tokenDetails, String companyId) {

        return managerService.assignCompanyFor4dot5Admin(userId, olduserId,
                tokenDetails, companyId);
    }

    @Override
    public UserDto setUserScope(AccessControlDto accessControlDto,
                                String userId, String companyId) {
        UserScope scope = UserScope.mapAccessControlToScope(accessControlDto);
        User user = apiUserExecutor.setScope(scope, userId);
        if (accessUtil.isRecruitingManager(user.getRole())) {
            userScopeService.updateClientListWhenRoleOrManagerUpdate(user,
                    null, companyId, true);
        }
        UserDto dto = getUserDto(user);
        dto.setUserScopeDetails(fetchUserScopeService.getUserScopeList(userId,
                companyId, dto.getUserDetails().getRole().getId()));
        return dto;
    }

    @Override
    public AccessControlList setScope(Scope scope, String userId) {

        return apiUserExecutor.setScope(scope, userId);
    }

    @Override
    public List<TeamMemberDto> getPossibleManagersForUpdate(String userId,
                                                            String role, String loggedInUserId) {

        return managerService.getPossibleManagersForUpdate(userId, role,
                loggedInUserId);
    }

    @Override
    public User getRecipientUser(String userId) {

        return apiUserExecutor.getRecipientUser(userId);
    }

    @Override
    public AccessControlDto getUserEnityScope(String userId, String companyId,
                                              String roleId, TokenDetails tokenDetails) {
        if (tokenDetails.getUserId().equals(userId)) {
            return fetchUserScopeService.getUserScopeListForLoggedInUser(
                    userId, companyId, roleId);
        } else {
            return fetchUserScopeService.getUserScopeList(userId, companyId,
                    roleId);
        }
    }

    @Override
    public AccessControlDto getUserClientsOrBU(String companyId, String userId) {

        AccessControlDto accessControlDto = fetchUserScopeService
                .getUserClientListForLoggedInUser(userId, companyId);
        if (accessControlDto.getClientOrBUList() == null) {
            accessControlDto.setClientOrBUList(new ArrayList<SelectedClientOrBuDto>());
        } else {
            List<SelectedClientOrBuDto> list = accessControlDto.getClientOrBUList();
            for (SelectedClientOrBuDto selectedClientOrBuDto : list) {
                String childCompId = selectedClientOrBuDto.getCompanyId();
                String compName = selectedClientOrBuDto.getCompanyName();
                selectedClientOrBuDto.setAliasName(compName);
                if (settingService.getClientOrBuSettings(companyId, childCompId, false) != null) {
                    String newName = compName + " " + AppConstants.MODIFIED;
                    selectedClientOrBuDto.setAliasName(newName);
                }
            }
        }
        return accessControlDto;
    }

    @Override
    public boolean checkRoleEntityScopeIsDefined(String companyId) {
        Company company = companyService.getCompany(companyId);
        if (company.getCompanyType().equals(CompanyType.Corporation)) {
            List<CompanyDto> buList = companyService
                    .getActiveClientOrBuDtoList(companyId);

            if (buList.size() > 0) {
                List<RoleEntityScope> roleEntityScopeDtos = companyService
                        .getBasicEntityScope(companyId);
                LOGGER.debug("Get entity scope - Executing roleEntityScope {}",
                        roleEntityScopeDtos);
                return !(roleEntityScopeDtos == null || roleEntityScopeDtos
                        .isEmpty());
            } else {
                return true;
            }
        } else if (company.getCompanyType().equals(CompanyType.StaffingCompany)) {
            // get company entity scope
            List<RoleEntityScope> roleEntityScopeDtos = companyService
                    .getBasicEntityScope(companyId);
            LOGGER.debug("Get entity scope - Executing roleEntityScope {}",
                    roleEntityScopeDtos);

            return !(roleEntityScopeDtos == null || roleEntityScopeDtos
                    .isEmpty());
        }
        return false;

    }

    @Override
    public List<TeamMemberDto> getPossibleManagersForUpdatingCompanyAdmin(
            String userId, String companyId) {
        return managerService.getPossibleManagersForUpdatingCompanyAdmin(
                userId, companyId);
    }

    @Override
    public UserDto saveForwardRecipient(UserDto userDto,
                                        TokenDetails tokenDetails, String roleId, String companyId) {
        return forwardRecipientService.saveForwardRecipient(userDto,
                tokenDetails, roleId, companyId);

    }

    @Override
    public DashBoardDto getUserDashBoardData(String userId) {

        return userMockApiExecutor.getUserDashBoardData(userId);
    }

    private List<User> getAllUserUnder(String userId, Role role) {
        LOGGER.info("total reportee under userId {}", userId);
        List<User> reporteeList = new ArrayList<>();
        List<User> tempReporteeList;
        if (accessUtil.isRecruitingManager(role) || accessUtil.isCompanyAdmin(role) || accessUtil.isHiringManagerRole(role)) {
            tempReporteeList = getReporteeList(userId);
            reporteeList.addAll(tempReporteeList);
            for (User user : tempReporteeList) {
                List<User> users = getAllUserUnder(user.getId(), user.getRole());
                if (users != null) {
                    reporteeList.addAll(users);
                }
            }
        }
        return reporteeList;
    }

    @Override
    public List<User> getAllReporteeUnder(String userId, Role role) {

        return getAllUserUnder(userId, role);
    }

    @Override
    public User getCurrentLoggedInUser() {
        String userName = SecurityUtils.getCurrentUserLogin();
        return apiUserExecutor.verifyUsername(userName);
    }

    @Override
    public String getTokenPayload(String token) {
        String userId = getCurrentLoggedInUser().getId();
        return apiUserExecutor.getTokenPayload(userId, token);
    }

    @Override
    public String getFilteredUserDetails(UserFilterDTO userFilter) {
        return apiUserExecutor.getFilteredUserDetails(userFilter);
    }

    @Override
    public String deleteUser(DeleteDTO deleteDTO) {
        return apiUserExecutor.deleteUser(deleteDTO);
    }

    @Override
    public String getUserDeleteInfo(String userId) {
        return apiUserExecutor.getUserDeleteInfo(userId);
    }

    @Override
    public String resendInvite(String userId) {
        return apiUserExecutor.resendInvite(userId);
    }

    @Override
    public List<TeamMemberDto> getRecruitersList(String userId, String companyId) {
        return teamMemeberService.getRecruitersList(userId, companyId);
    }

    @Override
    public String getUsersOfSharedRequisition(String requisitionId) {
        return apiUserExecutor.getUsersOfSharedRequisition(requisitionId);
    }

    @Override
    public String getVendorRecruiters(VendorRecruiterRequestDto request) {
        User loggedInUser = getCurrentLoggedInUser();
        request.setLoggedInUserId(loggedInUser.getId());
        return apiUserExecutor.getVendorRecruiters(request);
    }

	@Override
	public String getNotifyUsersOfRequisition(NotifyUserDTO notifyUserDTO) {
		return apiUserExecutor.getNotifyUsersOfRequisition(notifyUserDTO);
	}

    @Override
    public String getVendorRecruitersOfSharedRequisition(String requisitionId, String vendorId) {
        return apiUserExecutor.getVendorRecruitersOfSharedRequisition(requisitionId, vendorId);
    }

    @Override
    public List<TeamMemberDto> getPossibleManagersForHiringManager(String companyId, String userId){
        return managerService.getPossibleManagersForHiringManager(companyId,userId);
    }

    @Override
    public boolean isAdminUser(User user) {
        if (user.getRole() != null) {
            if (accessUtil.isSuperUser(user.getRole().getId())
                    || accessUtil.isFourDot5Admin(user.getRole())
                    || accessUtil.isCompanyAdmin(user.getRole())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Map<String, List<User>> getVendorRecruiters(VendorRecruitersCollectionRequestDto request) {
        return apiUserExecutor.getVendorRecruiters(request);
    }

    @Override
    public Map<String, List<User>> getAssignedVendorsAndRecruiters(String requisitionId) {
        return apiUserExecutor.getAssignedVendorsAndRecruiters(requisitionId);
    }

    @Override
    public Map<String, List<User>> getAssignedHiringManagersAndRecruiters(String requisitionId) {
        return apiUserExecutor.getAssignedHiringManagersAndRecruiters(requisitionId);
    }

    @Override
    public List<UserDTO> getCompanyUsersForActivities(String companyId) {
        return apiUserExecutor.getCompanyUsersForActivities(companyId);
    }

    @Override
    public Map<String, ClientBUInfo> setClientBuHierarchy(String userId, String companyId) {
        return apiUserExecutor.setClientBuHierarchy(userId, companyId);
    }

    @Override
    public List<User> getAllPermissibleUsers(List<User> users, String companyId) {
        List<String> userIds = new ArrayList<>();
        for (User  user : users) {
            if (user == null)
                continue;

            userIds.add(user.getId());
        }

        return apiUserExecutor.getAllPermissibleUsers(userIds, companyId);
    }

    @Override
    public List<User> getAllRecruitersOrHiringManagers(String userId, String companyId, List<String> userIds) {
        return apiUserExecutor.getAllRecruitersOrHiringManagers(userId, companyId, userIds);
    }

    @Override
    public boolean hasAccessToClientOrBu(String userId, String clientOrBuId) {
        return apiUserExecutor.hasAccessToClientOrBu(userId, clientOrBuId);
    }

    @Override
    public String getSubordinatesByBU(String userId, String buId) {
        return apiUserExecutor.getSubordinatesByBU(userId, buId);
    }

    @Override
    public String getUserByToken(String token) {
        return apiUserExecutor.getUserByToken(token);
    }


    @Override
    public RecruitersAndManagersCollectionDto getRecruitersAndHiringManagers(TeamMemberRequestDto request) {

        String userId = getCurrentLoggedInUser().getId();

        RecruitersAndManagersCollectionDto teamMembers = new RecruitersAndManagersCollectionDto();
        List<TeamMemberDto> recruiters = new ArrayList<>();
        List<TeamMemberDto> hiringManagers = new ArrayList<>();
        Map<String, List<TeamMemberDto>> vendorRecruiters = new HashMap<>();

        //Get Assigned Recruiters & hiring managers
        Map<String, List<User>> assignedHiringManagersAndRecruiters = getAssignedHiringManagersAndRecruiters(request.getRequisitionId());

        if (assignedHiringManagersAndRecruiters != null) {
            List<User> validRecruiters = assignedHiringManagersAndRecruiters.get(RECRUITERS.getDescription());
            List<User> validHiringManagers = assignedHiringManagersAndRecruiters.get(HIRING_MANAGERS.getDescription());
            if (!CollectionUtils.isEmpty(validRecruiters)) {
                for (User validRecruiter : validRecruiters) {
                    if (!StringUtils.equals(validRecruiter.getId(), userId)) {
                        recruiters.add(TeamMemberDto.mapUserToTeamMember(validRecruiter));
                    }
                }
                if (!CollectionUtils.isEmpty(recruiters)) {
                    teamMembers.setRecruiters(recruiters);
                }
            }
            if (!CollectionUtils.isEmpty(validHiringManagers)) {
                for (User validHiringManager : validHiringManagers) {
                    if (!StringUtils.equals(validHiringManager.getId(), userId)) {
                        hiringManagers.add(TeamMemberDto.mapUserToTeamMember(validHiringManager));
                    }
                }
                if (!CollectionUtils.isEmpty(hiringManagers)) {
                    teamMembers.setHiringManagers(hiringManagers);
                }
            }
        }

        if (recruiters == null || hiringManagers == null) {
            //Get Recruiters & hiring managers
            TeamMemberCollectionDTO teamMemberCollection = requisitionService.getRecruiterList(userId,
                    request.getCompanyId(), request.getClientId(), request.getRequisitionId());
            if (recruiters == null && !CollectionUtils.isEmpty(teamMemberCollection.getRecruiters())) {
                List<TeamMemberDto> validRecruiters = teamMemberCollection.getRecruiters();
                for (TeamMemberDto validRecruiter : validRecruiters) {
                    if (!StringUtils.equals(validRecruiter.getUserId(), userId)) {
                        recruiters.add(validRecruiter);
                    }
                }
                if (!CollectionUtils.isEmpty(recruiters)) {
                    teamMembers.setRecruiters(recruiters);
                }
            }
            if (hiringManagers == null && !CollectionUtils.isEmpty(teamMemberCollection.getHiringManagers())) {
                List<TeamMemberDto> validHiringManagers = teamMemberCollection.getHiringManagers();
                for (TeamMemberDto validHiringManager : validHiringManagers) {
                    if (!StringUtils.equals(validHiringManager.getUserId(), userId)) {
                        hiringManagers.add(validHiringManager);
                    }

                }
                if (!CollectionUtils.isEmpty(hiringManagers)) {
                    teamMembers.setHiringManagers(hiringManagers);
                }
            }
        }

        //Get Vendor Recruiters
        if (request.isShared()) {
            Map<String, List<User>> assignedVendorsAndRecruiters = getAssignedVendorsAndRecruiters(request.getRequisitionId());
            if (assignedVendorsAndRecruiters != null) {
                vendorRecruiters = setVendorRecruiters(assignedVendorsAndRecruiters, userId);
            } else if (!CollectionUtils.isEmpty(request.getVendorIds()) && vendorRecruiters == null) {
                VendorRecruitersCollectionRequestDto vendorRecruitersCollectionRequest = new VendorRecruitersCollectionRequestDto();
                vendorRecruitersCollectionRequest.setRequisitionId(request.getRequisitionId());
                vendorRecruitersCollectionRequest.setVendorIds(request.getVendorIds());
                Map<String, List<User>> validVendorRecruiters = getVendorRecruiters(vendorRecruitersCollectionRequest);
                vendorRecruiters = setVendorRecruiters(validVendorRecruiters, userId);
            }
            if (vendorRecruiters != null) {
                teamMembers.setVendorRecruiters(vendorRecruiters);
            }
        }
        return teamMembers;
    }

    private Map<String, List<TeamMemberDto>> setVendorRecruiters(Map<String, List<User>> vendorRecruiters, String userId) {
        Map<String, List<TeamMemberDto>> vendorsAndRecruiters = new HashMap<>();
        if (vendorRecruiters != null) {
            for (Map.Entry<String, List<User>> validVendorRecruiter : vendorRecruiters.entrySet()) {
                List<TeamMemberDto> members = new ArrayList<>();
                List<TeamMemberDto> validMembers = TeamMemberDto.mapUsersToTeamMembers(validVendorRecruiter.getValue());
                for (TeamMemberDto validMember : validMembers) {
                    if (!StringUtils.equals(validMember.getUserId(), userId)) {
                        members.add(validMember);
                    }
                }
                String vendorName = validVendorRecruiter.getKey();
                if (!CollectionUtils.isEmpty(members)) {
                    vendorsAndRecruiters.put(vendorName, members);
                }
            }
        }
        return vendorsAndRecruiters;
    }
}
