package com.fourdotfive.missioncontrol.userentityscope;

import com.fourdotfive.missioncontrol.dtos.company.AccessControlDto;
import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.user.AccessControlList;

public interface FetchUserEntityScopeService {

	/**
	 * Get user scope (manager getting client list for a reportee)
	 * 
	 * @param userId
	 * @param companyId
	 * @param roleId
	 * @return
	 */
	AccessControlDto getUserScopeList(String userId, String companyId,
			String roleId);

	/**
	 * Get user scope for the logged in user, which includes forward
	 * recipient client list
	 * 
	 * @param userId
	 * @param companyId
	 * @param roleId
	 * @return
	 */
	AccessControlDto getUserScopeListForLoggedInUser(String userId,
			String companyId, String roleId);

	/**
	 * Get user client or bu list for the logged in user, which includes forward
	 * recipient client list
	 * 
	 * @param userId
	 * @param companyId
	 * @param roleId
	 * @return
	 */
	AccessControlDto getUserClientListForLoggedInUser(String userId,
			String companyId);

	AccessControlDto getClientOrBu(AccessControlDto accessControlDto,
								   String userId, Company company, AccessControlList accessControlList , boolean isGetSharedClientBus);

}
