package com.fourdotfive.missioncontrol.userentityscope;

import com.fourdotfive.missioncontrol.common.AccessUtil;
import com.fourdotfive.missioncontrol.common.AppConstants;
import com.fourdotfive.missioncontrol.common.CommonUtil;
import com.fourdotfive.missioncontrol.common.CompanyDtoNameComparator;
import com.fourdotfive.missioncontrol.company.CompanyService;
import com.fourdotfive.missioncontrol.dtos.company.*;
import com.fourdotfive.missioncontrol.dtos.user.ClientOwner;
import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.company.CompanyType;
import com.fourdotfive.missioncontrol.pojo.company.Industry;
import com.fourdotfive.missioncontrol.pojo.user.AccessControlList;
import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.pojo.user.UserForwardedReference;
import com.fourdotfive.missioncontrol.user.UserApiExecutor;
import com.fourdotfive.missioncontrol.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.*;

@Service
public class FetchUserEntityScopeServiceImp implements
		FetchUserEntityScopeService {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(FetchUserEntityScopeServiceImp.class);

	@Autowired
	private CompanyService companyService;

	@Autowired
	private UserService userService;

	@Autowired
	private AccessUtil accessUtil;

	@Autowired
	private UserApiExecutor userApiExecutor;

	@Autowired
	private UserScopeOfManagerService managerService;

	@Override
	public AccessControlDto getUserScopeList(String userId, String companyId,
			String roleId) {
		// get company entity scope

		AccessControlDto accessControlDto = getUserScope(userId, companyId);
		return accessControlDto;
	}

	private AccessControlDto getUserScope(String userId, String companyId) {

		AccessControlDto accessControlDto = new AccessControlDto();
		Company company = companyService.getCompany(companyId);
		AccessControlList userScope = userApiExecutor.getEnityScope(userId);
//		if (company.getCompanyType().equals(CompanyType.Corporation)
//				&& companyService.getActiveClientOrBu(companyId).size() == 0) {
//			accessControlDto.setDisabled(true);
//			return accessControlDto;
//		} else {
			LOGGER.debug("RES is defined {}",
					userService.checkRoleEntityScopeIsDefined(companyId));
			if (company.getCompanyType().equals(CompanyType.Corporation)
//					&& companyService.getActiveClientOrBu(companyId).size() > 0
					&& !userService.checkRoleEntityScopeIsDefined(companyId)) {
				accessControlDto.setWithBuAndNoRES(true);
				return accessControlDto;
			}
			if (!userScope.isAllclientOrBU()) {
				accessControlDto = getClientOrBu(accessControlDto, userId,
						company, userScope, false);
			} else {
				accessControlDto = getForwardRecipientClientList(userId,
						accessControlDto, company, userScope);
			}
			return CommonUtil.mapScopetoACLdto(company, userScope,
					accessControlDto);
//		}

	}

	@Override
	public AccessControlDto getUserScopeListForLoggedInUser(String userId,
			String companyId, String roleId) {
		Company company = companyService.getCompany(companyId);
		AccessControlDto accessControlDto = new AccessControlDto();
		// if company is Corp and if do not have any BU's then set ACL dto to
		// disabled
//		if (company.getCompanyType().equals(CompanyType.Corporation)
//                && companyService.getActiveClientOrBu(companyId).size() == 0) {
//            accessControlDto.setDisabled(true);
//            return accessControlDto;
//        } else {
			LOGGER.debug("RES is defined {}",
					userService.checkRoleEntityScopeIsDefined(companyId));
			if (company.getCompanyType().equals(CompanyType.Corporation)
//					&& companyService.getActiveClientOrBu(companyId).size() > 0
					&& !userService.checkRoleEntityScopeIsDefined(companyId)) {
				accessControlDto.setWithBuAndNoRES(true);
				return accessControlDto;
			}
			AccessControlList userScope = userApiExecutor.getEnityScope(userId);

			if (!userScope.isAllclientOrBU()) {
				accessControlDto = getClientOrBuForLoggedInUser(
						accessControlDto, userId, company, userScope);
			} else {
				accessControlDto = getForwardRecipientClientList(userId,
						accessControlDto, company, userScope);
			}
			return CommonUtil.mapScopetoACLdto(company, userScope,
					accessControlDto);
//		}

	}

	@Override
	public AccessControlDto getUserClientListForLoggedInUser(String userId,
			String companyId) {
		Company company = companyService.getCompany(companyId);
		AccessControlDto accessControlDto = new AccessControlDto();

		// if company is Corp and if do not have any BU's then set ACL dto to
		// disabled
		if (companyService.getActiveClientOrBu(companyId).size() == 0) {
			accessControlDto.setDisabled(true);
			return accessControlDto;
		} else {
			
			AccessControlList userScope = null;
			User user = userService.getUserById(userId);
			if (company.getCompanyType().equals(CompanyType.Corporation)
					&& companyService.getActiveClientOrBu(companyId).size() > 0
					&& !userService.checkRoleEntityScopeIsDefined(companyId)) {
				if (accessUtil.isCompanyAdmin(user.getRole())
						|| accessUtil.is4Dot5CompanyRole(user.getRole())
						|| accessUtil.isSuperUser(user.getRole().getId())) {
					userScope = new AccessControlList();
					userScope.setAllclientOrBU(true);
					accessControlDto = getClientOrBuForCompanyAdmin(
							accessControlDto, userId, company, userScope);
				}else{
				accessControlDto.setWithBuAndNoRES(true);
				}
				return accessControlDto;
			}
			
			if (accessUtil.isCompanyAdmin(user.getRole())
					|| accessUtil.is4Dot5CompanyRole(user.getRole())
					|| accessUtil.isSuperUser(user.getRole().getId())) {
				userScope = new AccessControlList();
				userScope.setAllclientOrBU(true);
				accessControlDto = getClientOrBuForCompanyAdmin(
						accessControlDto, userId, company, userScope);
			} else {
				userScope = userApiExecutor.getEnityScope(userId);
				if (!userScope.isAllclientOrBU()) {
					accessControlDto = getClientOrBuForLoggedInUser(accessControlDto, userId, company, userScope);
				} else {
					accessControlDto = getClientOrBuForALLInRES(
							accessControlDto, userId, company, userScope);
				}
			}
			
			return CommonUtil.mapScopetoACLdto(company, userScope,
					accessControlDto);
		}
	}

	private AccessControlDto getForwardRecipientClientList(String userId, AccessControlDto accessControlDto, Company company, AccessControlList userScope) {
		List<Company> managerCompanies = managerService.getClientFromManager(userId, company.getId());
		if (managerCompanies != null) {
            User user = userService.getUserById(userId);
			getAllBusOfClient(accessControlDto, user, company, userScope, false);
			Map<String, ClientBUInfo> userClientBUInfo = setClientBuHierarchy(userId, company.getId());
			UserClientBUinfo userClientBUinfo = new UserClientBUinfo();
			userClientBUinfo.setClientBUInfo(userClientBUInfo);
            accessControlDto = setClientOrBuInfoToAccessControlDto(userClientBUInfo, company, accessControlDto, userId);
            boolean isAdmin = userService.isAdminUser(user);
			if (isAdmin) {
				userClientBUinfo.setHasAllBuOfClientEnabledForParent(true);
				userClientBUinfo.setHasAllBuOrClientEnabledForParent(true);
				accessControlDto.setUserClientBUinfo(userClientBUinfo);
				return accessControlDto;
			}

			User manager = userService.getManager(userId);
			if (manager != null && manager.getAccessControlList() != null) {
				userClientBUinfo.setHasAllBuOfClientEnabledForParent(manager.getAccessControlList().isAllBusOfClient());
				userClientBUinfo.setHasAllBuOrClientEnabledForParent( manager.getAccessControlList().isAllclientOrBU());
				accessControlDto.setUserClientBUinfo(userClientBUinfo);
				return accessControlDto;
			}
		}
		return accessControlDto;
	}

	/**
	 * Get all client list for a user along with forward recipient client list
	 * 
	 * @param accessControlDto
	 * @param userId
	 * @param company
	 * @param accessControlList
	 * @return
	 */
	public AccessControlDto getClientOrBu(AccessControlDto accessControlDto,
			String userId, Company company, AccessControlList accessControlList, boolean isGetSharedClientBus) {
        User user = userService.getUserById(userId);
		getAllBusOfClient(accessControlDto, user, company, accessControlList, isGetSharedClientBus);
		Map<String, ClientBUInfo> userClientBUInfo = setClientBuHierarchy(userId, company.getId());
		UserClientBUinfo userClientBUinfo = new UserClientBUinfo();
		userClientBUinfo.setClientBUInfo(userClientBUInfo);
        accessControlDto = setClientOrBuInfoToAccessControlDto(userClientBUInfo, company, accessControlDto, userId);
		boolean isAdmin = userService.isAdminUser(user);
		if (isAdmin) {
			userClientBUinfo.setHasAllBuOfClientEnabledForParent(true);
			userClientBUinfo.setHasAllBuOrClientEnabledForParent(true);
			accessControlDto.setUserClientBUinfo(userClientBUinfo);
			return accessControlDto;
		}
		User manager = userService.getManager(userId);
		if (manager != null && manager.getAccessControlList() != null) {
			userClientBUinfo.setHasAllBuOfClientEnabledForParent(manager.getAccessControlList().isAllBusOfClient());
			userClientBUinfo.setHasAllBuOrClientEnabledForParent( manager.getAccessControlList().isAllclientOrBU());
			accessControlDto.setUserClientBUinfo(userClientBUinfo);
            return accessControlDto;
		}

        return accessControlDto;
	}

	private Map<String, ClientBUInfo>  setClientBuHierarchy(String userId, String companyId) {
		return userService.setClientBuHierarchy(userId, companyId);
	}

	/**
	 * Getting client list for logged in user(which includes client list from
	 * forward recipient)
	 * 
	 * @param accessControlDto
	 * @param userId
	 * @param company
	 * @param accessControlList
	 * @return
	 */
	private AccessControlDto getClientOrBuForLoggedInUser(
			AccessControlDto accessControlDto, String userId, Company company,
			AccessControlList accessControlList) {
        User user = userService.getUserById(userId);
        getAllBusOfClient(accessControlDto, user, company, accessControlList, false);
        Map<String, ClientBUInfo> userClientBUInfo = setClientBuHierarchy(userId, company.getId());
        UserClientBUinfo userClientBUinfo = new UserClientBUinfo();
        userClientBUinfo.setClientBUInfo(userClientBUInfo);
		accessControlDto = setClientOrBuInfoToAccessControlDto(userClientBUInfo, company, accessControlDto, userId);
        boolean isAdmin = userService.isAdminUser(user);
        if (isAdmin) {
            userClientBUinfo.setHasAllBuOfClientEnabledForParent(true);
            userClientBUinfo.setHasAllBuOrClientEnabledForParent(true);
            accessControlDto.setUserClientBUinfo(userClientBUinfo);
            return accessControlDto;
        }

        User manager = userService.getManager(userId);
        if (manager != null && manager.getAccessControlList() != null) {
            userClientBUinfo.setHasAllBuOfClientEnabledForParent(manager.getAccessControlList().isAllBusOfClient());
            userClientBUinfo.setHasAllBuOrClientEnabledForParent( manager.getAccessControlList().isAllclientOrBU());
            accessControlDto.setUserClientBUinfo(userClientBUinfo);
            return accessControlDto;
        }
		return accessControlDto;
	}

	private AccessControlDto setClientOrBuInfoToAccessControlDto(Map<String, ClientBUInfo> userClientBUInfo,
                                   Company company, AccessControlDto accessControlDto, String userId) {
        if (userClientBUInfo == null) {
            accessControlDto.setClientOrBUList(new ArrayList<SelectedClientOrBuDto>());
        }
        List<SelectedClientOrBuDto> clientOrBuDtos = new ArrayList<>();
        if (userClientBUInfo != null) {
            List<Company> clientBuCompanies = companyService.getClientOrBUs(company.getId());
            for (Map.Entry<String, ClientBUInfo> clientBUInfoEntry : userClientBUInfo.entrySet()) {
                if (clientBUInfoEntry == null)
                    continue;

                ClientBUInfo clientBUInfo = clientBUInfoEntry.getValue();
                if (clientBUInfo == null)
                    continue;

                for (Company clientOrBu : clientBuCompanies) {
                    if (clientOrBu == null)
                        continue;

                    if(clientOrBu.getId().equals(clientBUInfoEntry.getKey())) {
                        SelectedClientOrBuDto selectedClientOrBuDto = new SelectedClientOrBuDto(clientBUInfo.getName(),
								clientBUInfoEntry.getKey(),  company.getCompanyType(), company.getCompanyState(),
								clientBUInfo.isEnabled(), !clientBUInfo.isAvailable());
						selectedClientOrBuDto.setSubordinatesBelongToClientOrBu(clientBUInfo.isSubordinatesBelongToBu());
                        clientOrBuDtos.add(selectedClientOrBuDto);
                        break;
                    }
                }
            }
        }
        return computeUserClientList(userId, company, clientOrBuDtos,
                accessControlDto);
    }

	/**
	 * Getting client list for logged in user(which includes client list from
	 * forward recipient)
	 * 
	 * @param accessControlDto
	 * @param userId
	 * @param company
	 * @param accessControlList
	 * @return
	 */
	private AccessControlDto getClientOrBuForALLInRES(
			AccessControlDto accessControlDto, String userId, Company company,
			AccessControlList accessControlList) {

		List<Company> clientCompanies = managerService.getClientFromManager(
				userId, company.getId());
		List<SelectedClientOrBuDto> clientOrBuDtos = CommonUtil
				.getAllDisabledClients(clientCompanies, true, true,
						ClientOwner.USER);
		// get forward recipient client list
		return computeUserClientList(userId, company, clientOrBuDtos,
				accessControlDto);
	}

	/**
	 * Getting client list for logged in user(which includes client list from
	 * forward recipient)
	 * 
	 * @param accessControlDto
	 * @param userId
	 * @param company
	 * @param accessControlList
	 * @return
	 */
	private AccessControlDto getClientOrBuForCompanyAdmin(
			AccessControlDto accessControlDto, String userId, Company company,
			AccessControlList accessControlList) {
		List<Company> clientCompanies = companyService
				.getActiveClientOrBu(company.getId());
		List<SelectedClientOrBuDto> clientOrBuDtos = CommonUtil
				.getAllDisabledClients(clientCompanies, true, true,
						ClientOwner.USER);
		// get forward recipient client list
		return computeUserClientList(userId, company, clientOrBuDtos,
				accessControlDto);
	}

	/**
	 * Compute client list between forward recipients client list and user
	 * client list if same client exist between user and forward recipient,
	 * remove the client from forward recipient client list
	 * 
	 * @param userId
	 * @param company
	 * @param clientOrBuDtos
	 * @param accessControlDto
	 * @return
	 */
	private AccessControlDto computeUserClientList(String userId,
			Company company, List<SelectedClientOrBuDto> clientOrBuDtos,
			AccessControlDto accessControlDto) {
		// get forward recipient client list
		List<SelectedClientOrBuDto> forwardedRecipientsClientOrBuDtos = getForwardRecipientsClients(
				userId, company.getId(), clientOrBuDtos);
		// get all client from all forward recipients
		if (forwardedRecipientsClientOrBuDtos != null) {
			accessControlDto.setHasForwardedRecipientClientList(true);
			clientOrBuDtos.addAll(forwardedRecipientsClientOrBuDtos);
		} else {
			accessControlDto.setHasForwardedRecipientClientList(false);
		}
		// sort the list in ascending order
		Collections.sort(clientOrBuDtos, new CompanyDtoNameComparator());
		accessControlDto.setClientOrBUList(clientOrBuDtos);
		return accessControlDto;
	}

	/**
	 * Get forward recipient client list
	 * 
	 * @param userId
	 * @param companyId
	 * @param loggedInClientList
	 * @return
	 */
	private List<SelectedClientOrBuDto> getForwardRecipientsClients(
			String userId, String companyId,
			List<SelectedClientOrBuDto> loggedInClientList) {

		List<Company> clientCompanies = null;
		List<SelectedClientOrBuDto> clientOrBuDtosFromForwardRecipient = null;
		// get all forward recipient
		List<User> users = isForwardRecipientAvailable(userId);
		LOGGER.debug("Getting client from forward recipient{}", users.size());
		if (users != null && !users.isEmpty()) {
			// get all forward recipient's companies
			clientCompanies = getClientCompaniesFromForwardRecipient(users,
					companyId);
			if (clientCompanies != null) {
				// get client dtos
				clientOrBuDtosFromForwardRecipient = CommonUtil
						.getAllDisabledClients(clientCompanies, true, true,
								ClientOwner.FORWARDED_RECIPIENT);
				for (SelectedClientOrBuDto buDto : clientOrBuDtosFromForwardRecipient) {
					LOGGER.debug("forward recipient clients: {}",
							buDto.getCompanyName());
				}
				// remove duplicate client, if the logged in user has it
				clientOrBuDtosFromForwardRecipient = removeDuplicateClients(
						loggedInClientList, clientOrBuDtosFromForwardRecipient);
			}

		}
		return clientOrBuDtosFromForwardRecipient;
	}

	/**
	 * Remove duplicate client if Forward recipient and the logged in user has
	 * same client
	 * 
	 * @param loggedInClientList
	 * @param clientOrBuDtosFromForwardRecipient
	 * @return
	 */
	private List<SelectedClientOrBuDto> removeDuplicateClients(
			List<SelectedClientOrBuDto> loggedInClientList,
			List<SelectedClientOrBuDto> clientOrBuDtosFromForwardRecipient) {
		for (SelectedClientOrBuDto clientOrBuDto : loggedInClientList) {
			for (Iterator<SelectedClientOrBuDto> iterator = clientOrBuDtosFromForwardRecipient
					.iterator(); iterator.hasNext();) {
				SelectedClientOrBuDto obj = iterator.next();
				if (clientOrBuDto.getCompanyId().equals(obj.getCompanyId())
						&& clientOrBuDto.isSelected()) {
					// Remove the current element from the iterator and the
					// list.
					iterator.remove();
				}
			}
		}
		return clientOrBuDtosFromForwardRecipient;
	}

	/**
	 * Remove duplicate client if Forward recipient's has same client
	 * 
	 * @param allForwardClientList
	 * @param currentForwardRecipientClient
	 * @return
	 */
	private List<Company> removeDuplicateForwardClients(
			List<Company> allForwardClientList,
			List<Company> currentForwardRecipientClient) {
		for (Company comapany : allForwardClientList) {
			for (Iterator<Company> iterator = currentForwardRecipientClient
					.iterator(); iterator.hasNext();) {
				Company obj = iterator.next();
				if (comapany.getId().equals(obj.getId())) {
					// Remove the current element from the iterator and the
					// list.
					iterator.remove();
				}
			}
		}
		allForwardClientList.addAll(currentForwardRecipientClient);
		return allForwardClientList;
	}

	/**
	 * Get client Companies from all forward recipient
	 * 
	 * @param users
	 * @param companyId
	 * @return
	 */
	private List<Company> getClientCompaniesFromForwardRecipient(
			List<User> users, String companyId) {
		List<Company> allForwardRecipientsClients = new ArrayList<Company>();
		for (User forwardRecipientUser : users) {
			List<Company> companies = null;
			if (!accessUtil.isCompanyAdmin(forwardRecipientUser.getRole())) {
				if (!forwardRecipientUser.getAccessControlList()
						.isAllclientOrBU()) {
					companies = forwardRecipientUser.getAccessControlList()
							.getClientOrBUList();
				} else {
					companies = managerService.getClientFromManager(
							forwardRecipientUser.getId(), companyId);
				}
				// remove all duplicate clients from the list
				allForwardRecipientsClients = removeDuplicateForwardClients(
						allForwardRecipientsClients, companies);
			} else {
				allForwardRecipientsClients = companyService
						.getActiveClientOrBu(companyId);
			}

		}
		return allForwardRecipientsClients;
	}

	/**
	 * Get all Forward recipient for logged in user
	 * 
	 * @param userId
	 * @return
	 */
	private List<User> isForwardRecipientAvailable(String userId) {
		List<User> forwardRecipientList = new ArrayList<User>();
		List<UserForwardedReference> forwardedReferences = userApiExecutor
				.getForwardedFromRecipients(userId);
		for (UserForwardedReference forwardedReference : forwardedReferences) {
			if (!forwardedReference.getUser().getAccessControlList()
					.getAccountState().equals(AppConstants.ARCHIVED)
					&& CommonUtil
							.isForwardRecipientViewable(forwardedReference)) {
				forwardRecipientList.add(forwardedReference.getUser());
			}
		}

		return forwardRecipientList;
	}

	private AccessControlDto getUserIndustries(
			AccessControlDto accessControlDto, User user, Company company) {

		List<Industry> selectedIndustries = user.getAccessControlList()
				.getIndustryList();
		if (selectedIndustries == null || selectedIndustries.isEmpty()) {
			List<Industry> managerIndustries = managerService
					.getIndustriesFromManager(user.getId(), company.getId());
			if (managerIndustries != null) {
				List<SubIndustryDto> subIndustryDtos = CommonUtil
						.getUserIndustries(managerIndustries, false);
				accessControlDto.setIndustryList(subIndustryDtos);
			}
		} else {
			List<Industry> managerIndustries = managerService
					.getIndustriesFromManager(user.getId(), company.getId());
			List<SubIndustryDto> subIndustryDtos = CommonUtil
					.getAllUserIndustries(selectedIndustries, managerIndustries);
			accessControlDto.setIndustryList(subIndustryDtos);
		}
		return accessControlDto;
	}

	private AccessControlDto getRequisitionCategories(
			AccessControlDto accessControlDto, User user, Company company) {
		List<String> reqCategories = user.getAccessControlList()
				.getRequisitionCategoryList();
		if (reqCategories == null || reqCategories.isEmpty()) {
			reqCategories = managerService.getReqCategoryFromManager(
					user.getId(), company.getId());
			List<ReqCategoryDto> reqCategoryDtos = CommonUtil
					.getAllReqCategories(reqCategories, false);
			accessControlDto.setRequisitionCategoryList(reqCategoryDtos);
		} else {
			List<String> companyReqCategories = managerService
					.getReqCategoryFromManager(user.getId(), company.getId());

			List<ReqCategoryDto> reqCategoryDtos = CommonUtil
					.mapSelectedReqCategories(reqCategories,
							companyReqCategories);
			accessControlDto.setRequisitionCategoryList(reqCategoryDtos);

		}
		return accessControlDto;
	}

	private void getIndustryList() {
		/*
		 * if (!scope.isAllIndustries()) {
		 * 
		 * accessControlDto = getUserIndustries(accessControlDto, user,
		 * company);
		 * 
		 * } if (!scope.isAllRequisitionCategories()) {
		 * 
		 * accessControlDto = getRequisitionCategories( accessControlDto, user,
		 * company);
		 * 
		 * } if (!scope.isAllJobRequisitionTitles()) {
		 * 
		 * List<JobRequisitionRole> jobRequisitionTitleList = new
		 * ArrayList<JobRequisitionRole>(); accessControlDto
		 * .setJobRequisitionTitleList(jobRequisitionTitleList);
		 * 
		 * }
		 */
	}


	private void getAllBusOfClient(AccessControlDto accessControlDto, User user, Company company, AccessControlList accessControlList, boolean isGetSharedClientBus) {
		if (company.getCompanyType() == CompanyType.StaffingCompany) {
			List<String> buIdsOfManager = managerService.getPossibleBusOfUser(user, company.getId(), isGetSharedClientBus);
			Map<String, AllBusOfClientList> allBusOfClients = accessControlList.getAllBusOfClientList();
			List<SelectedClientOrBuDto> clientOrBuDtos;
			Map<String, List<SelectedClientOrBuDto>> enabledBusOfClientList = new HashMap<>();
			if (CollectionUtils.isEmpty(allBusOfClients)) {
				Map<String, List<Company>> busOfClient = companyService.getAllBusOfClient(company.getId());
				for (Map.Entry<String, List<Company>> entry : busOfClient.entrySet()) {
					if (CollectionUtils.isEmpty(entry.getValue()))
						continue;

					List<Company> bus = entry.getValue();
					if (CollectionUtils.isEmpty(bus))
						continue;

					List<Company> clientBus = new ArrayList<>();
					for (Company bu : entry.getValue()) {
						if (bu == null)
							continue;

						if (!CollectionUtils.isEmpty(buIdsOfManager) && buIdsOfManager.contains(bu.getId()))
							clientBus.add(bu);

					}

					if (!CollectionUtils.isEmpty(clientBus)) {
						clientOrBuDtos = CommonUtil.getAllClients(clientBus, false, ClientOwner.USER);
						enabledBusOfClientList.put(entry.getKey(), clientOrBuDtos);
					}
				}
				accessControlDto.setAllBusOfClientList(enabledBusOfClientList);
				accessControlDto.setAllBusOfClient(accessControlList.isAllBusOfClient());
			} else {
				Map<String, AllBusOfClientList> selectedBusOfClient = accessControlList.getAllBusOfClientList();
				Map<String, List<Company>> allBusOfClient = companyService.getAllBusOfClient(company.getId());
				for (Map.Entry<String, List<Company>> busOfClientEntry : allBusOfClient.entrySet()) {
                    List<Company> filteredBus = new ArrayList<>();
                    for (Company bu : busOfClientEntry.getValue()) {
                    	if (bu == null)
                    		continue;

						LOGGER.info("User name : " + user.getFirstname() + " User id : " + user.getId() + " , bu id : " + bu.getId() + " , buIdsOfManager size() : " +  buIdsOfManager.size() );
                        if (buIdsOfManager.contains(bu.getId()))
                            filteredBus.add(bu);
                    }

                    busOfClientEntry.setValue(filteredBus);

					if (!CollectionUtils.isEmpty(selectedBusOfClient.keySet()) && selectedBusOfClient.keySet().contains(busOfClientEntry.getKey())) {
					    clientOrBuDtos = CommonUtil.mapSelectedClientOrBuDto(
								selectedBusOfClient.get(busOfClientEntry.getKey()).getAllBusOfClient(), busOfClientEntry.getValue(), ClientOwner.USER);
						enabledBusOfClientList.put(busOfClientEntry.getKey(), clientOrBuDtos);
					} else {
						clientOrBuDtos = CommonUtil.getAllClients(busOfClientEntry.getValue(), false, ClientOwner.USER);
						enabledBusOfClientList.put(busOfClientEntry.getKey(), clientOrBuDtos);

					}
				}
				accessControlDto.setAllBusOfClientList(enabledBusOfClientList);
				accessControlDto.setAllBusOfClient(accessControlList.isAllBusOfClient());
			}
		}
	}
}
