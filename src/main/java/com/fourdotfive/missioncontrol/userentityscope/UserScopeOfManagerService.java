package com.fourdotfive.missioncontrol.userentityscope;

import java.util.List;

import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.company.Industry;
import com.fourdotfive.missioncontrol.pojo.user.User;

public interface UserScopeOfManagerService {

	/**
	 * Get the client list from manager if the manager is all, then get from the
	 * who has client list but if the manager is company admin, then get client
	 * list of company
	 *
	 * @param userId
	 * @param companyId
	 * @return
	 */
	List<Company> getClientFromManager(String userId, String companyId);

	List<String> getPossibleBusOfUser(User user, String companyId, boolean isGetSharedClientBus);

	List<Industry> getIndustriesFromManager(String userId, String companyId);

	List<String> getReqCategoryFromManager(String userId, String companyId);

}
