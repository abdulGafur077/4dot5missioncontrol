package com.fourdotfive.missioncontrol.userentityscope;

import java.util.*;

import com.fourdotfive.missioncontrol.dtos.company.AllBusOfClientList;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fourdotfive.missioncontrol.common.AccessUtil;
import com.fourdotfive.missioncontrol.common.AppConstants;
import com.fourdotfive.missioncontrol.company.CompanyService;
import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.company.CorporationReqCategory;
import com.fourdotfive.missioncontrol.pojo.company.Industry;
import com.fourdotfive.missioncontrol.pojo.company.StaffingReqCategory;
import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.user.UserService;
import org.springframework.util.CollectionUtils;

@Service
public class UserScopeOfManagerServiceImp implements UserScopeOfManagerService {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(UserScopeOfManagerServiceImp.class);

	@Autowired
	private CompanyService companyService;

	@Autowired
	private UserService userService;

	@Autowired
	private AccessUtil accessUtil;

	@Override
	public List<Company> getClientFromManager(String userId, String companyId) {
		// get manager
		List<Company> companyList = new ArrayList<Company>();
		User manager = userService.getManager(userId);
		if (manager != null) {
			// check if manager role is company admin
			if (!accessUtil.isCompanyAdmin(manager.getRole())) {
				// if allClient is false, then return client list
				if (!manager.getAccessControlList().isAllclientOrBU()) {
					companyList = manager.getAccessControlList().getClientOrBUList();
				} else {
					// get client list from the manager's manager
					companyList = getClientFromManager(manager.getId(), companyId);
				}
			} else {
				companyList = companyService.getActiveClientOrBu(companyId);
			}
		} else {
			LOGGER.debug("manager does not exist");
		}
		return companyList;
	}

	@Override
	public List<String> getPossibleBusOfUser(User user, String companyId, boolean isGetSharedClientBus) {
		List<User> managers = new ArrayList<>();
		if(isGetSharedClientBus)
			managers.add(user);
		List<String> possibleBUsOfUser = new ArrayList<>();
		getManagers(user, managers);
		Collections.reverse(managers);
		for (User currentUser : managers) {
			List<String> possibleBUsOfCurrentUser = getBUIdsOfManager(currentUser, companyId, possibleBUsOfUser);
			if (CollectionUtils.isEmpty(possibleBUsOfCurrentUser))
				continue;

			if (CollectionUtils.isEmpty(possibleBUsOfUser)) {
				possibleBUsOfUser.addAll(possibleBUsOfCurrentUser);
			} else {
				for (String id : possibleBUsOfCurrentUser) {
					if (possibleBUsOfUser.contains(id)) {
						possibleBUsOfUser.add(id);
					}
				}
			}
		}
		return possibleBUsOfUser;
	}

	private void getManagers(User user, List<User> managers) {
		User manager = userService.getManager(user.getId());
		if (manager != null) {
			managers.add(manager);
			manager = userService.getManager(manager.getId());
			if (manager != null) {
				getManagers(manager, managers);
			}
		}
	}

	private  List<String> getBUIdsOfManager(User manager, String companyId, List<String> possibleBUsOfManagerOfCurrentUser) {
		List<String> buIdsOfManager = new ArrayList<>();
		if (manager != null && manager.getAccessControlList() != null) {
			if (!accessUtil.isRoleAtCompanyAdminLevel(manager.getRole().getId())) {
				if (!manager.getAccessControlList().isAllBusOfClient()) {
					Map<String, AllBusOfClientList> allBusOfManager = manager.getAccessControlList().getAllBusOfClientList();
					if (allBusOfManager != null && allBusOfManager.entrySet() != null) {
						for (Map.Entry<String, AllBusOfClientList> entry : allBusOfManager.entrySet()) {
							if (entry == null)
								continue;

							AllBusOfClientList allBusOfClientList = entry.getValue();
							if (allBusOfClientList == null)
								continue;

							List<String> buIds = new ArrayList<>();
							for (Company bu : allBusOfClientList.getAllBusOfClient()) {
								if (bu == null)
									continue;

								if (CollectionUtils.isEmpty(possibleBUsOfManagerOfCurrentUser)) {
									buIds.add(bu.getId());
								} else if (possibleBUsOfManagerOfCurrentUser.contains(bu.getId())) {
									buIds.add(bu.getId());
								}
							}
							buIdsOfManager.addAll(buIds);
						}
					}
					return buIdsOfManager;
				} else {
					return getBUIdsOfManager(manager.getManager(), companyId, possibleBUsOfManagerOfCurrentUser);
				}
			} else {
				List<Company> clientOrBusOfManager = companyService.getActiveClientOrBu(companyId);
				for (Company company : clientOrBusOfManager) {
					if (company == null)
						continue;

					List<Company> bus = companyService.getClientOrBUs(company.getId());
					List<String> buIds = new ArrayList<>();
					for (Company bu : bus) {
						if (bu == null)
							continue;

						buIds.add(bu.getId());
					}
					buIdsOfManager.addAll(buIds);
				}
				return buIdsOfManager;
			}
		} else {
			LOGGER.debug("manager does not exist");
		}
		return buIdsOfManager;
	}

	@Override
	public List<Industry> getIndustriesFromManager(String userId,
			String companyId) {
		List<Industry> industries = null;
		User user = userService.getUserById(userId);
		if (user.getRole().getId().equals(AppConstants.STAFFING_ADMIN_ROLEID)
				|| user.getRole().getId()
						.equals(AppConstants.CORPORATE_ADMIN_ROLEID)) {
			industries = companyService.getIndustries(companyId);
			return industries;

		}
		User manager = userService.getManager(user.getId());
		if (manager.getRole().getId()
				.equals(AppConstants.STAFFING_ADMIN_ROLEID)
				|| manager.getRole().getId()
						.equals(AppConstants.CORPORATE_ADMIN_ROLEID)) {
			industries = companyService.getIndustries(companyId);
			return industries;

		} else if (!manager.getAccessControlList().isAllIndustries()) {
			if (manager.getAccessControlList().getIndustryList() == null
					|| manager.getAccessControlList().getIndustryList()
							.isEmpty()) {
				getIndustriesFromManager(manager.getId(), companyId);
			} else {
				industries = manager.getAccessControlList().getIndustryList();
			}
		}
		return industries;
	}

	@Override
	public List<String> getReqCategoryFromManager(String userId,
			String companyId) {
		User user = userService.getUserById(userId);
		if (user.getRole().getId().equals(AppConstants.STAFFING_ADMIN_ROLEID)) {
			LOGGER.debug("user role staffing admin");
			return new StaffingReqCategory().addStaffingReqCategory();

		} else if (user.getRole().getId()
				.equals(AppConstants.CORPORATE_ADMIN_ROLEID)) {
			LOGGER.debug("user role corporate admin");
			return new CorporationReqCategory().addCorporationReqCategory();

		}
		User manager = userService.getManager(user.getId());
		if (manager.getRole().getId()
				.equals(AppConstants.STAFFING_ADMIN_ROLEID)) {
			LOGGER.info("manager role staffing admin");
			return new StaffingReqCategory().addStaffingReqCategory();

		} else if (manager.getRole().getId()
				.equals(AppConstants.CORPORATE_ADMIN_ROLEID)) {
			LOGGER.debug("manager role corporate admin");
			return new CorporationReqCategory().addCorporationReqCategory();

		} else if (!manager.getAccessControlList().isRequisitionCategory()) {
			LOGGER.debug("manager has access control ");
			if (manager.getAccessControlList().getRequisitionCategoryList() == null
					|| manager.getAccessControlList()
							.getRequisitionCategoryList().isEmpty()) {
				return getReqCategoryFromManager(manager.getId(), companyId);
			} else {
				return manager.getAccessControlList()
						.getRequisitionCategoryList();
			}
		} else {
			LOGGER.debug("manager do not has access control");
			return getReqCategoryFromManager(manager.getId(), companyId);
		}
	}

}
