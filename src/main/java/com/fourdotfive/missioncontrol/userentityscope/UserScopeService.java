package com.fourdotfive.missioncontrol.userentityscope;

import java.util.List;

import com.fourdotfive.missioncontrol.dtos.user.UserDto;
import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.company.RoleEntityScope;
import com.fourdotfive.missioncontrol.pojo.user.User;

/**
 * 
 * @author Chakravarthy
 *
 */
public interface UserScopeService {

	/**
	 * 
	 * @param existingClientList
	 * @param managerClientList
	 * @return
	 */
	List<Company> computeClientList(List<Company> existingClientList,
			List<Company> managerClientList);

	/**
	 * Update/reset scope , when updating role or manager
	 * 
	 * @param user
	 * @param manager
	 * @param companyId
	 * @param updateReporteesScopeOnly
	 */
	void updateClientListWhenRoleOrManagerUpdate(User user, User manager,
			String companyId, boolean updateReporteesScopeOnly);

	/**
	 * Get Reportees list of a user and update their client list
	 * 
	 * @param user
	 * @param companyId
	 * @param entityScopes
	 */
	void findAndUpdateReporteesScope(User user, String companyId,
			List<RoleEntityScope> entityScopes);

	/**
	 * Get Reportees list of a user
	 * 
	 * @param user
	 * @param companyId
	 * @param entityScopes
	 */
	void findAndResetReporteesScope(User user, String companyId,
			List<RoleEntityScope> entityScopes);

	/**
	 * reset client list of all user
	 * 
	 * @param companyId
	 * @param user
	 * @param entityScopes
	 */
	void resetClientList(String companyId, User user,
			List<RoleEntityScope> entityScopes);

	/**
	 * Get client list from manager
	 * 
	 * @param manager
	 * @param companyId
	 * @return
	 */
	List<Company> getClientFromManager(User manager, String companyId);

	/**
	 * Set scope based on role of a user
	 * 
	 * @param user
	 * @param companyId
	 * @param userId
	 */
	void setScopeBasedOnRole(UserDto user, String companyId, String userId);

	/**
	 * Remove
	 * 
	 * @param companyId
	 * @param clientCompanyId
	 */
	void deleteClientFromClientList(String companyId, String clientCompanyId);

}
