package com.fourdotfive.missioncontrol.userentityscope;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fourdotfive.missioncontrol.common.AccessUtil;
import com.fourdotfive.missioncontrol.common.Messages;
import com.fourdotfive.missioncontrol.company.CompanyService;
import com.fourdotfive.missioncontrol.dtos.user.UserDto;
import com.fourdotfive.missioncontrol.exception.RoleEntityScopeConflictException;
import com.fourdotfive.missioncontrol.pojo.company.Company;
import com.fourdotfive.missioncontrol.pojo.company.RoleEntityScope;
import com.fourdotfive.missioncontrol.pojo.user.Role;
import com.fourdotfive.missioncontrol.pojo.user.User;
import com.fourdotfive.missioncontrol.pojo.user.UserScope;
import com.fourdotfive.missioncontrol.user.UserApiExecutor;
import com.fourdotfive.missioncontrol.user.UserService;

/**
 * 
 * @author Chakravarthy
 *
 */
@Service
public class UserScopeServiceImp implements UserScopeService {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(UserScopeServiceImp.class);

	@Autowired
	private UserService userService;

	@Autowired
	private AccessUtil accessUtil;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private UserApiExecutor userApiExecutor;

	@Override
	public void updateClientListWhenRoleOrManagerUpdate(User user,
			User manager, String companyId, boolean updateReporteesScopeOnly) {
		// get role entity scope from company
		List<RoleEntityScope> entityScopes = companyService
				.getBasicEntityScope(companyId);
		if (updateReporteesScopeOnly) {
			// update reportee list
			findAndUpdateReporteesScope(user, companyId, entityScopes);
		} else {
			// update from manager client list
			updateClientList(companyId, user, entityScopes, manager);

		}
	}

	@Override
	public List<Company> computeClientList(List<Company> existingClientList,
			List<Company> managerClientList) {
		List<Company> actualClientList = new ArrayList<Company>();
		if (existingClientList != null && managerClientList != null) {
			for (Company existingClient : existingClientList) {
				for (Company managerClient : managerClientList) {
					// if client exist in both in manager and reportee, then
					// only add to the client list
					if (existingClient.getId().equals(managerClient.getId())) {
						actualClientList.add(existingClient);
					}
				}
			}
		}
		return actualClientList;
	}

	/**
	 * Update client list for a user
	 * 
	 * @param companyId
	 * @param user
	 * @param entityScopes
	 * @param manager
	 */
	private void updateClientList(String companyId, User user,
			List<RoleEntityScope> entityScopes, User manager) {
		if (!userService.checkRoleEntityScopeIsDefined(companyId)) {
			throw new RoleEntityScopeConflictException(
					Messages.ROLE_ENTITY_SCOPE_NOT_DEFINED,
					Messages.ROLE_ENTITY_SCOPE_NOT_DEFINED);
		}
		// get compare role based on role id
		RoleEntityScope entityScope = getRoleEntityScopeBasedOnRoleId(
				entityScopes, user.getRole());
		// check if all clients is selected

		if (entityScope != null) {
			if (!entityScope.isAllClients() || !entityScope.isAllBUsOfClients()) {
				if (manager == null) {
					manager = userService.getManager(user.getId());
				}
				updateUserScope(manager, user, entityScope, companyId);

			} else {
				// if all clients update entity scope with client list to null
				UserScope scope = UserScope.setScopeFromRoleEntityScope(
						entityScope, null);
				userApiExecutor.setScope(scope, user.getId());
			}
			// update reportee list
			findAndUpdateReporteesScope(user, companyId, entityScopes);
		}
	}

	@Override
	public void findAndUpdateReporteesScope(User user, String companyId,
			List<RoleEntityScope> entityScopes) {
		LOGGER.debug("reportee under userId {}", user.getId());
		if (accessUtil.isRecruitingManager(user.getRole()) || accessUtil.isHiringManagerRole(user.getRole())) {

			List<User> reporteeList = userService.getReporteeList(user.getId());
			for (User reportee : reporteeList) {

				updateClientList(companyId, reportee, entityScopes, user);
			}
		}
	}

	/**
	 * Compare roleId and return the Role Entity Scope the user belong to
	 * 
	 * @param entityScopes
	 * @param role
	 * @return
	 */
	private RoleEntityScope getRoleEntityScopeBasedOnRoleId(
			List<RoleEntityScope> entityScopes, Role role) {
		for (RoleEntityScope entityScope : entityScopes) {
			if (entityScope.getRole().getId().equals(role.getId())) {
				return entityScope;
			}
		}
		return null;
	}

	/**
	 * Compare client list with manager and update the client list
	 * 
	 * @param manager
	 * @param reportee
	 * @param roleEntityScope
	 * @param companyId
	 */
	private void updateUserScope(User manager, User reportee,
			RoleEntityScope roleEntityScope, String companyId) {
		if (manager != null && reportee != null) {
			// get client list from manager
			List<Company> clientList = getClientFromManager(manager, companyId);
			// compare and get user client list
			clientList = computeClientList(reportee.getAccessControlList()
					.getClientOrBUList(), clientList);
			// set the client list to scope
			UserScope scope = UserScope.setScopeFromRoleEntityScope(
					roleEntityScope, clientList);
			LOGGER.debug("scope of reportee {} {}", scope, reportee.getId());
			userApiExecutor.setScope(scope, reportee.getId());
		}
	}

	@Override
	public List<Company> getClientFromManager(User manager, String companyId) {
		// get manager
		if (manager != null) {
			// check if manager role is company admin
			if (!accessUtil.isCompanyAdmin(manager.getRole())) {
				// check is not allClient then return client list
				if (!manager.getAccessControlList().isAllclientOrBU()) {

					return manager.getAccessControlList().getClientOrBUList();
				} else {
					manager = userService.getManager(manager.getId());

					// get client list from the manager's manager
					return getClientFromManager(manager, companyId);
				}
			} else {
				List<Company> companies = companyService
						.getClientOrBUs(companyId);
				return companies;
			}
		} else {
			LOGGER.debug("manager does not exist");
		}
		return null;
	}

	@Override
	public void findAndResetReporteesScope(User user, String companyId,
			List<RoleEntityScope> entityScopes) {
		LOGGER.debug("reportee under userId {}", user.getId());
		if (accessUtil.isRecruitingManager(user.getRole())) {

			List<User> reporteeList = userService.getReporteeList(user.getId());
			for (User reportee : reporteeList) {
				updateClientList(companyId, reportee, entityScopes, user);
			}
		}
	}

	@Override
	public void resetClientList(String companyId, User user,
			List<RoleEntityScope> entityScopes) {
		// get role by compare role based on role id
		RoleEntityScope entityScope = getRoleEntityScopeBasedOnRoleId(
				entityScopes, user.getRole());
		if (entityScope != null) {
			UserScope scope = UserScope.setScopeForCompanyAdmin();
			// check if all clients is selected
			if (!entityScope.isAllClients()) {
				scope.setAllClients(false);
				scope.setClientList(new ArrayList<String>());
			}
			if (!entityScope.isAllBUsOfClients()) {
				scope.setAllBusOfClient(false);
				scope.setAllBusOfClientList(new HashMap<String, List<String>>());

			}
			user = userApiExecutor.setScope(scope, user.getId());
			// update reportee list
			findAndResetReporteesScope(user, companyId, entityScopes);
		}
	}

	@Override
	public void setScopeBasedOnRole(UserDto user, String companyId,
			String userId) {
		if (user.getUserDetails().getUserId() == null) {
			List<RoleEntityScope> entityScopes = companyService
					.getBasicEntityScope(companyId);
			if (entityScopes != null && !entityScopes.isEmpty()) {
				for (RoleEntityScope entityScope : entityScopes) {
					if (entityScope.getRole().getId()
							.equals(user.getUserDetails().getRole().getId())) {
						if (!accessUtil.isFourDot5Admin(user.getUserDetails()
								.getRole())
								&& !accessUtil.isCompanyAdmin(user
										.getUserDetails().getRole())) {
							UserScope scope = UserScope
									.setScopeFromRoleEntityScope(entityScope,
											null);
							userApiExecutor.setScope(scope, userId);
							break;
						}
					}

				}
			}
		}
	}

	@Override
	public void deleteClientFromClientList(String companyId,
			String clientCompanyId) {
		if (companyId != null && clientCompanyId != null) {
			// get all RES value for the company
			List<RoleEntityScope> entityScopes = companyService
					.getBasicEntityScope(companyId);
			if (entityScopes != null && !entityScopes.isEmpty()) {
				for (RoleEntityScope entityScope : entityScopes) {
					// check if all client
					if (!entityScope.isAllClients()) {
						// get all user based on the role for whom the client
						// has to removed
						List<User> userList = userApiExecutor
								.getUserListBasedOnRole(entityScope.getRole()
										.getId(), companyId);
						if (userList != null && !userList.isEmpty()) {
							for (User user : userList) {
								removeClient(user, clientCompanyId);
							}
						}

					}
				}
			}
		}
	}

	/**
	 * Remove client from user scope list
	 * 
	 * @param user
	 * @param clientCompanyId
	 */
	private void removeClient(User user, String clientCompanyId) {
		List<Company> clientList = user.getAccessControlList()
				.getClientOrBUList();
		UserScope scope = UserScope.mapAccessControlToScope(user
				.getAccessControlList());
		if (clientList != null && !clientList.isEmpty()) {
			for (Iterator<Company> iterator = clientList.iterator(); iterator
					.hasNext();) {
				Company obj = iterator.next();
				if (obj.getId().equals(clientCompanyId)) {
					// Remove the current element from the iterator and the
					// list.
					iterator.remove();
				}
			}
		}
		List<String> updatedClientList = new ArrayList<>();
		for (Company buorClient : clientList) {
			updatedClientList.add(buorClient.getId());
		}
		scope.setClientList(updatedClientList);
		userApiExecutor.setScope(scope, user.getId());
	}

}
