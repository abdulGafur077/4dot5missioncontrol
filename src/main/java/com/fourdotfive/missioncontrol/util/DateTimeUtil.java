package com.fourdotfive.missioncontrol.util;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Deprecated
public class DateTimeUtil {

    public static final String DATE_TIME_FORMAT_PATTERN = "yyyyMMdd'T'HHmmss'Z'";
    private static final String DATE_AND_TIME_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss'Z'";

    public static String dateTimeFormatter(DateTime dateTime) throws IllegalArgumentException {
        if (dateTime == null) {
            throw new IllegalArgumentException("Date and time can not be null");
        }
        DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(DATE_AND_TIME_FORMAT_PATTERN);
        return dateTimeFormatter.print(dateTime);
    }

    public static String getDateTimeFromPattern(Date date, String pattern) throws IllegalArgumentException {
        if (date == null) {
            throw new IllegalArgumentException("Date object can not be null");
        }
        DateFormat dateFormat = new SimpleDateFormat(pattern);
        return dateFormat.format(date);
    }

}
