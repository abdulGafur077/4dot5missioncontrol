package com.fourdotfive.missioncontrol.utility;


public interface UtilityApiExecutor {

    String getTypes(String typeName);

    String getTypesWithText(String typeName);
}
