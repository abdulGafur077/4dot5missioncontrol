package com.fourdotfive.missioncontrol.utility;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/utility")
public class UtilityController {


    private final UtilityService utilityService;

    @Autowired
    public UtilityController(UtilityService utilityService) {
        this.utilityService = utilityService;
    }

    @RequestMapping(value = "/gettypes/{typename}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getTypes(@PathVariable("typename") String typeName) {
        String response = utilityService.getTypes(typeName);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/gettypeswithtext/{typename}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getTypesWithText(@PathVariable("typename") String typeName) {
        String response = utilityService.getTypesWithText(typeName);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
