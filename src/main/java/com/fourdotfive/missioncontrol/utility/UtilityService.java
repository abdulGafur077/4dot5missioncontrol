package com.fourdotfive.missioncontrol.utility;


public interface UtilityService {

    String getTypes(String typeName);

    String getTypesWithText(String typeName);
}



