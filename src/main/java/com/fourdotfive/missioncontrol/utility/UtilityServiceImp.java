package com.fourdotfive.missioncontrol.utility;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UtilityServiceImp implements UtilityService {


    private final UtilityApiExecutor utilityApiExecutor;

    @Autowired
    public UtilityServiceImp(UtilityApiExecutor utilityApiExecutor) {
        this.utilityApiExecutor = utilityApiExecutor;
    }

    @Override
    public String getTypes(String typeName) {
        return utilityApiExecutor.getTypes(typeName);
    }

    @Override
    public String getTypesWithText(String typeName) {
        return utilityApiExecutor.getTypesWithText(typeName);
    }
}
