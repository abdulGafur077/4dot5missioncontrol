package com.fourdotfive.missioncontrol.valueassessment;

import com.fourdotfive.missioncontrol.pojo.bigfivequestions.BigFiveQuestions;
import com.fourdotfive.missioncontrol.pojo.bigfivequestions.CandidateBigFiveResponses;

import java.util.List;
import com.fourdotfive.missioncontrol.dtos.company.CandidateBigFiveResponsesDto;
public interface ValueAssesmentApiExecutor {

    public List<BigFiveQuestions> getBigFiveQuestions();
    public CandidateBigFiveResponsesDto getCandidateBigFiveResponses(String jobMatchId);
    public void saveCandidateBigFiveResponses(CandidateBigFiveResponses candidateBigFiveResponses);
}
