package com.fourdotfive.missioncontrol.valueassessment;

import com.fourdotfive.missioncontrol.pojo.bigfivequestions.BigFiveQuestions;
import com.fourdotfive.missioncontrol.pojo.bigfivequestions.CandidateBigFiveResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import com.fourdotfive.missioncontrol.dtos.company.CandidateBigFiveResponsesDto;
@Service
public class ValueAssesmentServiceImp implements ValueAssesmentService {


    @Autowired

    private ValueAssesmentApiExecutor valueAssesmentApiExecutor;

    public List<BigFiveQuestions> getBigFiveQuestions() {
        List<BigFiveQuestions> bigFiveQuestionsServiceList = valueAssesmentApiExecutor.getBigFiveQuestions();
        return bigFiveQuestionsServiceList;
    }

    public CandidateBigFiveResponsesDto getCandidateBigFiveResponses(String jobMathcId) {
        CandidateBigFiveResponsesDto candidateBigFiveResponsesDto = valueAssesmentApiExecutor.getCandidateBigFiveResponses(jobMathcId);
        return candidateBigFiveResponsesDto;
    }

    public void saveCandidateBigFiveResponses(CandidateBigFiveResponses candidateBigFiveResponses) {
         valueAssesmentApiExecutor.saveCandidateBigFiveResponses(candidateBigFiveResponses);

    }
}
