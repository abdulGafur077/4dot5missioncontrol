package com.fourdotfive.missioncontrol.valueassessment;


import com.fourdotfive.missioncontrol.common.Message;
import com.fourdotfive.missioncontrol.common.Messages;
import com.fourdotfive.missioncontrol.dtos.company.CandidateBigFiveResponsesDto;
import com.fourdotfive.missioncontrol.pojo.bigfivequestions.BigFiveQuestions;
import com.fourdotfive.missioncontrol.pojo.bigfivequestions.CandidateBigFiveResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/valueassessment")
public class ValueAssessmentController {


@Autowired

public ValueAssesmentService valueAssesmentService;

   /**
            * To Get Big five Questions, this method is called.
     *
             * @return  BigFiveQuestions
     */
    @RequestMapping(value = "/getbigfivequestions/{candidateId}", method = RequestMethod.GET)
    public ResponseEntity<List<BigFiveQuestions>> getbigfivequestions(
            @PathVariable("candidateId") String candidateId,
            HttpServletRequest request) {
        List<BigFiveQuestions> response = valueAssesmentService.getBigFiveQuestions();
        return new ResponseEntity<List<BigFiveQuestions>>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/getcandidatebigfiveresponses/{jobMatchId}", method = RequestMethod.GET)
    public ResponseEntity<CandidateBigFiveResponsesDto> getcandidatebigfiveresponses(
      @PathVariable("jobMatchId") String jobMatchId,
            HttpServletRequest request) {
        CandidateBigFiveResponsesDto response = valueAssesmentService.getCandidateBigFiveResponses(jobMatchId);
        return new ResponseEntity<CandidateBigFiveResponsesDto>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/savecandidatebigfiveresponses", method = RequestMethod.POST)
    public ResponseEntity<Message> savecandidatebigfiveresponses(
            @RequestBody CandidateBigFiveResponses candidateBigFiveResponses,
            HttpServletRequest request) {
        valueAssesmentService.saveCandidateBigFiveResponses(candidateBigFiveResponses);
        Message msg = Message.statusCode(HttpStatus.OK)
                .message(Messages.CHANGE_PWD_SUCCESS).build();

        return new ResponseEntity<Message>(msg, HttpStatus.OK);
    }
}
