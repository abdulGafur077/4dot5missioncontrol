package com.fourdotfive.missioncontrol.workflowstep;

import com.fourdotfive.missioncontrol.dtos.workflowstep.WorkflowStepDTO;
import com.fourdotfive.missioncontrol.dtos.workflowstep.WorkflowStepRequestDTO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/workflowstep")
public class WorkflowStepController {

    @Autowired
    private WorkflowStepService workflowStepService;

    @RequestMapping(value = "create", method = {RequestMethod.POST})
    public @ResponseBody
    ResponseEntity<WorkflowStepDTO> save(@RequestBody WorkflowStepDTO workflowStepDTO) {

        WorkflowStepDTO workflowStep = workflowStepService.saveWorkflowStepOrder(workflowStepDTO);
        return new ResponseEntity<>(workflowStep, HttpStatus.OK);
    }

    @RequestMapping(value = "getworkflowstepsorder", method = {RequestMethod.POST})
    public @ResponseBody
    ResponseEntity<WorkflowStepDTO> getWorkflowStepsOrder(@RequestBody WorkflowStepRequestDTO workflowStepRequestDTO) {

        WorkflowStepDTO workflowStepDTO = workflowStepService.getWorkflowStepsOrder(workflowStepRequestDTO);
        return new ResponseEntity<>(workflowStepDTO, HttpStatus.OK);
    }
}
