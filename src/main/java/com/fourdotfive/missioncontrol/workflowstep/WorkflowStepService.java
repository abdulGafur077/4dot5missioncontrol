package com.fourdotfive.missioncontrol.workflowstep;

import com.fourdotfive.missioncontrol.dtos.workflowstep.WorkflowStepDTO;
import com.fourdotfive.missioncontrol.dtos.workflowstep.WorkflowStepRequestDTO;

import java.util.List;
import java.util.Map;

public interface WorkflowStepService {

    WorkflowStepDTO saveWorkflowStepOrder(WorkflowStepDTO workflowStepDTO);

    WorkflowStepDTO getWorkflowStepsOrder(WorkflowStepRequestDTO workflowStepRequestDTO);

    Map<String, List<String>> getWorkflowStepsByRequisitionId(WorkflowStepRequestDTO workflowStepRequestDTO);

    Map<String, List<String>> getWorkflowStepsByCompanyId(String companyId);

    void saveWorkflowSteps(WorkflowStepDTO workflowStepDTO);

    void saveWorkflowStepsByRequisitionId(WorkflowStepDTO workflowStepDTO);
}
