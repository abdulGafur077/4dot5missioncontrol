package com.fourdotfive.missioncontrol.workflowstep;

import com.fourdotfive.missioncontrol.dtos.workflowstep.WorkflowStepDTO;
import com.fourdotfive.missioncontrol.dtos.workflowstep.WorkflowStepRequestDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class WorkflowStepServiceImpl implements WorkflowStepService {

    @Autowired
    private WorkflowStepApiExecutor workflowStepApiExecutor;

    @Override
    public WorkflowStepDTO saveWorkflowStepOrder(WorkflowStepDTO workflowStepDTO) {
        return workflowStepApiExecutor.saveWorkflowStepOrder(workflowStepDTO);
    }

    @Override
    public WorkflowStepDTO getWorkflowStepsOrder(WorkflowStepRequestDTO workflowStepRequestDTO) {
        return workflowStepApiExecutor.getWorkflowStepsOrder(workflowStepRequestDTO);
    }

    @Override
    public Map<String, List<String>> getWorkflowStepsByRequisitionId(WorkflowStepRequestDTO workflowStepRequestDTO) {
        return workflowStepApiExecutor.getWorkflowStepsByRequisitionId(workflowStepRequestDTO);
    }

    @Override
    public Map<String, List<String>> getWorkflowStepsByCompanyId(String companyId) {
        return workflowStepApiExecutor.getWorkflowStepsByCompanyId(companyId);
    }

    @Override
    public void saveWorkflowSteps(WorkflowStepDTO workflowStepDTO) {
        workflowStepApiExecutor.saveWorkflowSteps(workflowStepDTO);
    }

    @Override
    public void saveWorkflowStepsByRequisitionId(WorkflowStepDTO workflowStepDTO) {
        workflowStepApiExecutor.saveWorkflowStepsByRequisitionId(workflowStepDTO);
    }
}
