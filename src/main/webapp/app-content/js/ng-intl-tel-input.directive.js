angular.module('ngIntlTelInput')
.directive('ngIntlTelInput', ['ngIntlTelInput', '$log', '$window', '$parse',
  function (ngIntlTelInput, $log, $window, $parse) {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function (scope, elm, attr, ctrl) {
    	 /* console.log('attr : ' + angular.toJson(attr));
    	  console.log('scope : ' + angular.toJson(scope));
    	  console.log('elm : ' + angular.toJson(elm));
    	  console.log('ctrl : ' + angular.toJson(ctrl));*/
        // Warning for bad directive usage.
        if ((!!attr.type && (attr.type !== 'text' && attr.type !== 'tel')) || elm[0].tagName !== 'INPUT') {
          $log.warn('ng-intl-tel-input can only be applied to a *text* or *tel* input');
          return;
        }

        // Set Selected Country Data.
        function setSelectedCountryData(model) {
          var getter = $parse(model);
          var setter = getter.assign;
          setter(scope, elm.intlTelInput('getSelectedCountryData'));
        }

        // Handle Country Changes.
        function handleCountryChange() {
          setSelectedCountryData(attr.selectedCountry);
        }

        // Country Change cleanup.
        function cleanUp() {
          console.log('cleanUp');
          angular.element($window).off('countrychange', handleCountryChange);
        }

        // Handle Country Changes.
        function handleSelectedCountryChange(selectedCountry) {
          //console.log('handleSelectedCountryChange : ' + selectedCountry);
          setSelectedCountryData(selectedCountry);
        }

        scope.test = function(selectedCountry){
          console.log('selectedCountry : ' + selectedCountry);
          setSelectedCountryData(selectedCountry);
          angular.element($window).on('countrychange', handleCountryChange);
          //scope.ccmc();
          scope.$on('$destroy', cleanUp);
        }
        
        // Override default country.
        if (attr.defaultCountry) {
        	//console.log('attr.defaultCountry : ' + attr.defaultCountry);
        	ngIntlTelInput.set({initialCountry: attr.defaultCountry});
        }
        
        if (attr.numberType) {
        	//console.log('attr.numberType : ' + attr.numberType);
        	ngIntlTelInput.set({placeholderNumberType: attr.numberType});
        }

        // Selected Country Data.
        if (attr.selectedCountry) {
        	//console.log('attr.selectedCountry : ' + attr.selectedCountry);
          setSelectedCountryData(attr.selectedCountry);
          angular.element($window).on('countrychange', handleCountryChange);
          scope.$on('$destroy', cleanUp);
        }

        // Initialize.
        ngIntlTelInput.init(elm);
        
        // Validation.
        ctrl.$validators.ngIntlTelInput = function (value) {
        	//console.log('ctrl.$validators.ngIntlTelInput : ' + value);
          // if phone number is deleted / empty do not run phone number validation
          if (value || elm[0].value.length > 0) {
              return elm.intlTelInput('isValidNumber');
          } else {
              return true;
          }
        };
        
        // Set model value to valid, formatted version.
        ctrl.$parsers.push(function (value) {
        	//console.log('ctrl.$parsers.push : ' + value);
          return elm.intlTelInput('getNumber');
        });
        
        // Set input value to model value and trigger evaluation.
        ctrl.$formatters.push(function (value) {
        	//console.log('ctrl.$formatters.push : ' + value);
          if (value) {
            if(value.charAt(0) !== '+') {
              value = '+' + value;
            }
            elm.intlTelInput('setNumber', value);
          }
          return value;
        });
      }
    };
  }]);
