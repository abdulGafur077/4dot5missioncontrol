/**
 * This is main application module which is root module.
 *
 */
var fourdotfive = angular.module('4dot5',
    [
        'ui.router', 'ngAnimate', 'ngSanitize', 'mgcrea.ngStrap', 'ui.bootstrap', 'angularMoment',
        'ngCookies', 'ngPrint', 'ngNotify', 'ngToast', 'ngDragDrop', 'ngIntlTelInput','ngMask',
        'datatables','datatables.bootstrap','datatables.buttons',
        'dndLists', 'isteven-multi-select', 'infinite-scroll','angular-flot','easypiechart',
        'angular-loading-bar', 'angularSpinner', 'imageupload', 'angular.vertilize', 'ngTagsInput',
        '4dot5.indexmodule',
        'GenericServiceModule',
        'UtilityServiceModule',
        'InterceptorServiceModule',
        'mystorage',
        'angular-timezone-selector',
        '4dot5.directivesModule',
        '4dot5.filtersModule',
        '4dot5.appconstantsmodule',
        '4dot5.loginmodule',
        '4dot5.tokenLoginWorkflowModule',
        '4dot5.forgetpasswordmodule',
        '4dot5.validateotpmodule',
        '4dot5.resetpasswordmodule',
        '4dot5.missioncontrolmodule'
    ]);

/**
 * Application level Constants
 */
fourdotfive.constant('APPLICATIONCONSTANTS', {
    RUN: {
        IMAGEURL: 'app-content/img/'
    },
    CONFIG: {
        LOGIN_URL: '/login',
        LOGIN_STATE: 'login',
        DASHBOARD: 'missioncontrol.dashboard'
    }
});

/**
 * used to run some custom function when some event is triggered
 */
fourdotfive.run(
    [
        '$rootScope',
        '$state',
        'ngNotify',
        'StorageService',
        'genericService',
        '$exceptionHandler',
        'APPLICATIONCONSTANTS',
        'MESSAGECONSTANTS',
        '$templateCache',
        function ($rootScope, $state, ngNotify, StorageService, genericService, $exceptionHandler, APPLICATIONCONSTANTS, MESSAGECONSTANTS, $templateCache) {
            $rootScope.angular = angular;
            $rootScope.MESSAGECONSTANTS = MESSAGECONSTANTS;
            $rootScope.tokenvalidity = 24 * 60 * 60 * 1000; //One day validity;
            $rootScope.refreshtokenvalidity = 21 * 24 * 60 * 60 * 1000; // Twenty One days Validity;
            $rootScope.isOffline = false;

            // $rootScope.tokenvalidity =  2 * 60 * 1000; //five minute validity;
            // $rootScope.refreshtokenvalidity = 60 * 60 * 1000; // sixty minute Validity;

            $rootScope.systemTimeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
            $rootScope.defaultCountryCode = 'US';
            $rootScope.disableScreen = false;

            /**
             * This is absolute path for image folder from where required images are loaded
             */
            $rootScope.imageUrl = APPLICATIONCONSTANTS.RUN.IMAGEURL;

            /**
             * Base url for API call should be configured here.
             */

            StorageService.set('baseurl', '');

          /*  $rootScope.$on('$routeChangeStart', function(event, next, current) {
                if (typeof(current) !== 'undefined'){
                    $templateCache.remove(current.templateUrl);
                }
            });*/
            /**
             * @link https://github.com/angular-ui/ui-router/wiki#state-change-events
             */
            $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
                //console.log('fromState : ' + angular.toJson(fromState));
                //console.log('toState : ' + angular.toJson(toState));
                $rootScope.fromState = fromState;
                $rootScope.toState = toState;

                if (typeof(toState) !== 'undefined'){
                    //console.log("entered $templateCache $templateCache",toState.templateUrl)
                    $templateCache.remove(toState.templateUrl);
                }
                //ngNotify.dismiss();
                if (toState.data.requireLogin) {
                    StorageService.remove('wizardoperations');
                    //console.log('userDetails  c : ' + angular.toJson(StorageService.getCookie('userDetails')));
                    var userDetails = StorageService.getCookie('userDetails');
                    //console.log('User details: '+angular.toJson(userDetails));
                    if (angular.isDefined(userDetails) && angular.isDefined(userDetails.accesstoken) && angular.isDefined(userDetails.accountState)) {
                        if (userDetails.accountState === $rootScope.MESSAGECONSTANTS.USER_STATES.ACTIVE) {
                            $rootScope.userDetails = userDetails;
                            $rootScope.userActive = true;
                            //console.log('Setting user active true : ' + angular.toJson($rootScope.userDetails));
                        }
                    } else {
                        //console.log('going to login page');
                        event.preventDefault();
                        console.log(StorageService.get('baseurl'));
                        console.log('redirecting to login');
                        StorageService.set('baseurl', '');
                        $state.go('login', null, { reload: true });
                    }
                }
                  /*else {   // commented because the left is showing as half baked when logout
                     $rootScope.userActive = false;
                 }*/

                /**
                 * This condition checks
                 * 1. After login clicks on backbutton should not come to  login
                 */
                if (toState.name === 'login') {
                    /**
                     * Checking Remember Me because
                     * when login irrespective of what ever check is made rememberme will be stored
                     */
                    var rememberme = StorageService.getCookie('rememberme');
                    var userDetails = StorageService.getCookie('userDetails');
                    //console.log('rememberme : ' + angular.toJson(rememberme));
                    //console.log('userDetails : ' + angular.toJson(userDetails));
                    //console.log('fromState : ' + angular.toJson(fromState));
                    if (angular.isDefined(rememberme) && angular.isDefined(userDetails)) {
                        event.preventDefault();
                        $rootScope.userActive = true;
                        if (fromState.name === '') {
                            $state.go('missioncontrol.dashboard', null, { reload: true });
                        } else {
                            $state.go(fromState.name, fromParams, { reload: false });
                        }
                    }
                }
            });

            /**
             * When page is reloded or state is changed, view will be scrolled to top.
             * @link https://github.com/angular-ui/ui-router/wiki#state-change-events
             */
            $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
                $("html, body").animate({ scrollTop: 0 }, 'slow');
                return false;
            });

            $rootScope.$on('sessionInvalidated', function (event, toState, toParams, fromState) {
                $rootScope.$emit("CallLogoutMethod", { logoutnow: true });
            });

            $rootScope.throwError = function(error){
                if(angular.isDefined(error.developerMsg) && (error.developerMsg !== null)){
                    try{
                        throw new Error(error.message);
                    }catch(except){
                        $exceptionHandler(except,error.developerMsg);
                    }
                }
            };

            // $rootScope.showBannerMessage = function(messagedata, messagetype){
            //     switch (messagetype) {
            //         case 'success':
            //             ngNotify.set('&nbsp;' + messagedata, {
            //                 type: 'success',
            //                 html: true,
            //                 sticky: true
            //             });
            //             break;
            //         case 'info':
            //             ngNotify.set('&nbsp;' + messagedata, {
            //                 type: 'info',
            //                 html: true,
            //                 sticky: true
            //             });
            //             break;
            //         case 'warning':
            //             $rootScope.disableScreen = true;
            //              ngNotify.set('&nbsp;' + messagedata, {
            //                  type: 'warn',
            //                  html: true,
            //                  sticky: true
            //              });
            //              break;
            //          case 'danger':
            //              $rootScope.disableScreen = true;
            //              ngNotify.set('&nbsp;' + messagedata, {
            //                  type: 'error',
            //                  html: true,
            //                  sticky: true
            //              });
            //              break;
            //         default:
            //             ngNotify.set(" some thing went wrong ...... pls contact admin")
            //     }
            // }
        }
    ]
);

/**
 * This Directive Serves as a header before user Login
 * This is Separated from user header just to avoid bulk functionality
 * in header even if user is not login yet
 */
fourdotfive.directive('headerContent', [
    function () {
        return {
            restrict: "AE",
            templateUrl: 'app/partials/header/header.html',
            controller: function () {
                var header = this;
            }
        };
    }
]);

/**
 * This Directive Serves as a header after user Login
 */
fourdotfive.directive('userHeaderContent', ['genericService', 'StorageService', '$interval',
    function (genericService, StorageService, $interval) {
        return {
            restrict: "AE",
            templateUrl: 'app/partials/header/userheader.html',
            controller: 'UserHeaderCtrl'
        };
    }
]);

/**
 * This Directive Serves as a footer for 4dot5 Application
 */
fourdotfive.directive('footerContent', ['genericService', 'StorageService', '$interval',
    function (genericService, StorageService, $interval) {
        return {
            restrict: "AE",
            templateUrl: 'app/partials/footer/footer.html',
            controller: function () {
                var footer = this;
            }
        };
    }
]);
/**
 * This Directive Serves as a footer for 4dot5 Application
 */
fourdotfive.directive('userFooterContent', ['genericService', 'StorageService', '$interval',
    function (genericService, StorageService, $interval) {
        return {
            restrict: "AE",
            templateUrl: 'app/partials/footer/userfooter.html',
            controller: function () {
                var footer = this;
            }
        };
    }
]);

/**
 *
 */
fourdotfive.filter('showBannerMessage', function () {
    return function (input) {
        var output = input;
        if (angular.isDefined(output).length > 65) {
            return output.substring(0, 65) + "...";
        } else {
            return output;
        }
    }
});

/**
 * Directive to convert string to Number
 * Useful while showing default selected option for <select></select>
 */
fourdotfive.directive('convertToNumber', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function (val) {
                return parseInt(val, 10);
            });
            ngModel.$formatters.push(function (val) {
                return '' + val;
            });
        }
    };
});

fourdotfive.config(['$provide', 'cfpLoadingBarProvider', 'ngToastProvider', 'ngIntlTelInputProvider', function ($provide, cfpLoadingBarProvider, ngToast, ngIntlTelInputProvider) {
    cfpLoadingBarProvider.includeBar = true;
    cfpLoadingBarProvider.includeSpinner = true;
    cfpLoadingBarProvider.latencyThreshold = 100;
    ngToast.configure({
        verticalPosition: 'center',
        horizontalPosition: 'top',
        maxNumber: 1,
        animation: 'slide',
        dismissButton: true,
        duration : 4000
    });
    ngIntlTelInputProvider.set({
        initialCountry: 'gb',
        utilsScript: 'app-content/js/utils.js'
    });
    $provide.decorator("$exceptionHandler", ['$log', '$delegate', '$injector', function($log, $delegate, $injector) {
        return function(exception, cause) {
            console.log('config catch');
            var alertsAndNotificationsService = $injector.get('alertsAndNotificationsService');
            $delegate(exception, cause);
            if(angular.isDefined(cause)){
                var rootScopeVariable = $injector.get('$rootScope');
                alertsAndNotificationsService.showBannerMessage(exception,'danger');
            }
        };
    }]);
}]);

/**
 * Application starting point, which redirect to default state and other
 * configuration.
 */

fourdotfive.config(
    ['$stateProvider',
        '$urlRouterProvider',
        '$httpProvider',
        'cfpLoadingBarProvider',
        'APPLICATIONCONSTANTS',
        function ($stateProvider, $urlRouterProvider, $httpProvider, cfpLoadingBarProvider, APPLICATIONCONSTANTS) {

            $urlRouterProvider.otherwise(function ($injector) {
                var $state = $injector.get('$state');
                var $rootScope = $injector.get('$rootScope');
                var genericService = $injector.get('genericService');
                var StorageService = $injector.get('StorageService');

                var rememberLogin = StorageService.getCookie('rememberme');
                var reftoken = StorageService.getCookie('refreshtoken');
                var userDetails = StorageService.getCookie('userDetails');

                /**
                 * This method is invoked from login success call.
                 * it manages the redirections basod on user state
                 */
                 var manageUserState = function (data) {
                     console.log('manageUserState with : ' + angular.toJson(data));
                     if (angular.equals(data.accountState, 'Active')) {
                         $rootScope.userActive = true;
                         if(data.company.companyState === 'Active'){
                            $state.go('missioncontrol.dashboard', null, { reload: false });
                         }else{
                            $state.go('missioncontrol.companymanagement', null, { reload: true });
                         }
                     } else if (angular.equals(data.accountState, 'UnVerfied') || angular.equals(data.accountState, 'PasswordReset')) {
                         $rootScope.userActive = false;
                         var message = 'Change Password to enter into application';
                         $state.go('resetpassword', { message: message }, { reload: false });
                     } else if (angular.equals(data.accountState, 'InActive')) {
                         $rootScope.userActive = false;
                         message = 'Your Account is InActive. Contact Administrator for Details';
                         $state.go('login', { message: message }, { reload: true });
                     } else if (angular.equals(data.accountState, 'Blocked')) {
                         $rootScope.userActive = false;
                         message = 'Your Account is Blocked. Contact Administrator for Details';
                         $state.go('login', { message: message }, { reload: true });
                     }
                 };

                var afterLogin = function () {
                    console.log('afterLogin : ');
                    var createUserRoleScreenRefObj = function (roleScreenRef) {
                        var roleObj = {};
                        roleObj.role = {};
                        roleObj.role.id = roleScreenRef.role.id;
                        roleObj.role.name = roleScreenRef.role.name;
                        roleObj.screen = roleScreenRef.screen;
                        return roleObj;
                    };

                    var createUserObject = function (data) {
                        var obj = {};
                        obj.company = {};
                        obj.id = data.id;
                        obj.firstname = data.firstname;
                        obj.lastname = data.lastname;
                        obj.emailId = data.emailId;
                        obj.companyId = data.companyId;
                        obj.accesstoken = data.token;
                        obj.company.companyId = data.company.companyId;
                        obj.company.companyName = data.company.companyName;
                        obj.company.companyType = data.company.companyType;
                        obj.company.companyState = 'Active';
                        obj.accountState = data.accountState;
                        obj.shouldPopupBeDisplayed = data.shouldPopupBeDisplayed;
                        obj.roleScreenRef = createUserRoleScreenRefObj(data.roleScreenRef);
                        if (data.roleScreenRef.role.name !== '4dot5SuperUser' && data.roleScreenRef.role.name !== '4dot5Admin') {
                            obj.company.companySelected = true;
                        } else {
                            obj.company.companySelected = false;
                        }
                        return obj;
                    };

                    genericService.getObjects('api/access/rememberme').then(function (data) {
                        StorageService.reset();
                        StorageService.setCookie('userDetails', createUserObject(data), { 'expires': new Date(Date.now() + $rootScope.tokenvalidity) });
                        console.log('remember me api success : ' + angular.toJson(StorageService.getCookie('userDetails')));
                        manageUserState(data);
                    }, function (error) {
                        console.log('error : ' + angular.toJson(error));
                        $rootScope.userActive = false;
                        StorageService.resetBrowserStorage();
                        $state.go('login', null, { reload: true });
                    });
                };

                var goToLogin = function () {
                    console.log('goToLogin : ');
                    $state.go('login', null, { reload: true });
                };

                var currentSessionExists = angular.isDefined(rememberLogin) && angular.isDefined(reftoken) && angular.isDefined(userDetails) &&
                                           !angular.equals(rememberLogin,null) && !angular.equals(reftoken,null) && !angular.equals(userDetails,null);
                console.log('currentSessionExists : ' + currentSessionExists);

                console.log('angular.isDefined(rememberLogin) : ' + angular.isDefined(rememberLogin));
                console.log('angular.isDefined(reftoken) : ' + angular.isDefined(reftoken));
                console.log('!angular.isDefined(userDetails) : ' + !angular.isDefined(userDetails));
                console.log('!angular.equals(rememberLogin,null) : ' + !angular.equals(rememberLogin,null));
                console.log('!angular.equals(reftoken,null) : ' + !angular.equals(reftoken,null));
                var newSessionRequested = angular.isDefined(rememberLogin) && angular.isDefined(reftoken) && !angular.isDefined(userDetails) &&
                   !angular.equals(rememberLogin,null) && !angular.equals(reftoken,null);
                console.log('newSessionRequested : ' + newSessionRequested);

                if(currentSessionExists){
                    manageUserState(userDetails);
                }else if(newSessionRequested){
                    afterLogin();
                }else {
                    goToLogin();
                }
            });

            $httpProvider.defaults.timeout = 5000;
            $httpProvider.interceptors.push('interceptorService');
            //$urlRouterProvider.otherwise(APPLICATIONCONSTANTS.CONFIG.LOGIN_URL);
        }
    ]
);

fourdotfive.filter('subStringFilter', function () {
  return function (input) {
      if(input.lastName != null){
          var output = input.firstName+" "+input.lastName;
      }else{
          var output = input.firstName;
      }
      if (output.length > 57) {
          return output.substring(0, 57) + "...";
          } else {
              return output;
          }
          var output = input;

      }
 });

/**
 * Filter for Profile Name it the name is too long
 */
fourdotfive.filter('profileFilter', function () {
    return function (input) {
        var output = input;
        if (output.length > 15) {
            return output.substring(0, 15) + "...";
        } else {
            return output;
        }
    }
});

/**
 * Filter for company Name it the name is too long
 */
fourdotfive.filter('companyNameSettingFilter', function () {
    return function (input) {
        var output = input;
        if (output.length > 32) {
            return output.substring(0, 32) + "...";
        } else {
            return output;
        }
    }
});

/**
 * Filter for Profile Name it the name is too long
 */
fourdotfive.filter('companyNameFilter', function () {
    return function (input) {
        var output = input;
        if (output.length > 45) {
            return output.substring(0, 45) + "...";
        } else {
            return output;
        }
    }
});

/**
 * Filter for Profile Name it the name is too long
 */
fourdotfive.filter('companyNameSettingsFilter', function () {
    return function (input) {
        var output = input;
        if (angular.isDefined(output).length > 65) {
            return output.substring(0, 65) + "...";
        } else {
            return output;
        }
    }
});

/**
 * Filter for Client/BU  Name if it is modified
 */
fourdotfive.filter('clientorbuModified', function () {
    return function (clientName, isClientGlobalCopy) {
        console.log(clientName, isClientGlobalCopy);
        var output = clientName;
        if (!isClientGlobalCopy) {
            return output + " (Modified)";
        } else {
            return output;
        }
    }
});

fourdotfive.filter('singleDropdownSelect', function () {
    return function (input) {
        var specialChar =['(Modified)']
        substring = "(Modified)";
        var word = input;
        if(word.length <= 30){
            return word;
        } else {
            if(word.indexOf(substring) !== -1){
                return word.substr(0,27) + '... ' +substring;
            }else{
                return word.substr(0,30) + '...';
            }
        }
    }
});

fourdotfive.filter('phonenumber',function(){
     return function (phoneNumber,format) {
        var newphoneNumber = '';
        if(angular.isDefined(phoneNumber) && (phoneNumber !== null)){
            newphoneNumber = phoneNumber.replace(/ |-/gi,"");
        }
        var specialChars = ["+","-"," "];
        var newNumber = "";
        var count = 0;
        for(i = 0; i < format.length;i++){
        if(specialChars.indexOf(format[i]) > -1){
            if(format[i] == newphoneNumber[i]){
                newNumber = newNumber + format[i];
                count++;
            }else{
                newNumber = newNumber + format[i];
            }
        }else{
            if(newphoneNumber[count]){
                newNumber = newNumber + newphoneNumber[count];
            }
            count++;
        }
        }
        if(i== format.length){
            return newNumber;
        }
     }
});
