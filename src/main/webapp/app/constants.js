/**
 * This module maintains all the validation messages
 * required for all the pages in 4dot5
 */
var appconstantsmodule = angular.module('4dot5.appconstantsmodule', []);

appconstantsmodule.constant('MESSAGECONSTANTS', {
    VIEW_REQUISITION: {
        DELETE_STATE: "Delete",
        DRAFT_STATE: "Draft",
        OPEN_STATE: "Open",
        REOPEN_STATE: "Reopen",
        REOPEN_FOR_MATCHING_STATE: "Reopen for Matching",
        CONTINUE_STATE: "Continue",
        PAUSE_STATE: "Pause",
        SOURCE_STATE: "Source",
        CANCEL_STATE: "Cancel",
        FILL_STATE: "Fill",
        JOIN_STATE: "Join",
        COMPLETE_STATE: "Complete",
        REMOVE_FILLED_CANDIDATE: "Remove Filled Candidate",
        REMOVE_JOINED_CANDIDATE: "Remove Joined Candidate"
    },

    SLIDERS: {
        CHECK_SLIDERONE: 'slider-range-time-to-place'
    },
    /**
	 * Patterns for different kind of Entities 
	 * in 4dot5
	 */
    VALIDATIONPATTERNS: {
        EMAIL: /^[a-zA-Z](\.?\_?[a-zA-Z0-9]{1,})+@([a-zA-Z0-9]+\.)+[a-zA-Z]{2,5}$/,
        //   EMAIL: /^[a-z]+[a-z0-9._]+@([a-z0-9-]+\.)+[a-z]{2,5}$/,
        //Previous      // EMAIL: /^[^\s@]+@[^\s@]+\[^\s@]{2,}$/,
        //EMAIL: /^[a-z]+[a-z0-9._]+@[a-z0-9.]+\.[a-z.]{2,5}$/,  @(?:[A-Z0-9-]+\.)+[A-Z]{2,}$
        // EMAIL : /^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$/,
        PASSWORD: '',
        ALPHA_NUMERIC: /^[a-zA-Z0-9]+$/,
        //Previous     //NUMERIC : /^[0-9]{10,10}$/,
        NUMERIC: '',
        TEXT: /^[a-zA-Z\s]*$/, //
        TEXT_WITH_ONLY_SPACE: /^[a-zA-Z ]*$/,
        NAME : /^[a-zA-Z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF'._ -]*$/
    },

    //user states
    USER_STATES: {
        ACTIVE: 'Active',
        UNVERIFIED: 'UnVerfied',
        PASSWORDRESET: 'PasswordReset',
        ARCHIVED: 'Archived',
        INACTIVE: 'InActive',
        BLOCKED: 'Blocked'
    },

    //company types
    COMPANY_TYPES: {
        STAFFING: 'StaffingCompany',
        CORPORATE: 'Corporation',
        FOURDOTFIVE: 'Host'
    },

    //company types
    ACCESS_MODES: {
        EDIT: 'edit',
        VIEW: 'view',
        CREATE: 'create'
    },

    //company types
    ROLES: {
        SUPERUSER: '4dot5SuperUser', 
        FOURDOTFIVEADMIN: '4dot5Admin', 
        ADMIN: 'Admin',
        STAFFINGADMIN: 'StaffingAdmin',
        CORPORATEADMIN: 'CorporateAdmin', 
        RECMANAGER: 'RecruitingManager', 
        SRRECRUITER: 'SrRecruiter', 
        RECRUITER: 'Recruiter'
    },
    
    //Role names
    ROLESNAMES: {
        SUPERUSER_NAME: '4dot5SuperUser', 
        FOURDOTFIVEADMIN_NAME: '4dot5Admin',
        STAFFINGADMIN_NAME: 'StaffingAdmin',
        CORPORATEADMIN_NAME: 'CorporateAdmin', 
        STAFFING_RECMANAGER_NAME: 'Recruiting Manager', 
        STAFFING_SRRECRUITER_NAME: 'Sr Recruiter', 
        STAFFING_RECRUITER_NAME: 'Recruiter',
        CORPORATE_RECMANAGER_NAME: 'Recruiting Manager', 
        CORPORATE_SRRECRUITER_NAME: 'Sr Recruiter', 
        CORPORATE_RECRUITER_NAME: 'Recruiter'
    },

    //Role names
    ROLEIDS: {
        SUPERUSER: '57c36f5a820fea2470d6b9e3', 
        FOURDOTFIVE_ADMIN: '57c36f5a820fea2470d6b9e7',
        STAFFING_ADMIN: '57c36f5a820fea2470d6b9e4',
        CORPORATE_ADMIN: '57c36f5a820fea2470d6b9e8', 
        STAFFING_REC_MANAGER: '57c36f5a820fea2470d6b9ec', 
        CORPORATE_REC_MANAGER: '57c36f5a820fea2470d6b9e9', 
        STAFFING_SR_RECRUITER: '57c36f5a820fea2470d6b9e5', 
        CORPORATE_SR_RECRUITER: '57c36f5a820fea2470d6b9eb', 
        STAFFING_RECRUITER: '57c36f5a820fea2470d6b9e6',
        CORPORATE_RECRUITER: '57c36f5a820fea2470d6b9ea'
    },
    
    /**
	 * Common Validation Messages for Across App
	 */
    COMMONVALIDATIONS: {
        EMAIL_MANDATORY: 'Email is mandatory.',
        PASSWORD_MANDATORY: 'Password is mandatory.',
        NOT_VALID_EMAIL: 'Invalid Email format.',
        PASSWORD_MINLENGTH: 'Password should have atleast 8 characters.',
        PASSWORD_MAXLENGTH: 'Password should have atmost 15 characters.',
        ENTER_MANDATORY_FIELDS: 'Please enter all mandatory fields.',
        NOT_VALID_ZIPCODE: 'Invalid Zip Code format.',
        ZIPCODE_MINLENGTH: 'Zip Code should have atleast 5 characters.',
        ZIPCODE_MAXLENGTH: 'Zip Code should have atmost 6 characters.',
        NO_LOCATION_FOUND : "Couldn't find any location for the given Zip Code.",
        NOT_VALID_WORKTELEPHONE: 'Work telephone entered is invalid.',
        WORKTELEPHONE_MINLENGTH: 'Telephone number should have atleast 10 characters.',
        WORKTELEPHONE_MAXLENGTH: 'Telephone number should have atmost 15 characters.',
        NOT_VALID_MOBILENUMBER: 'Mobile number entered is invalid.',
  //      MOBILENUMBER_MINLENGTH: 'Mobile Number should have atleast 10 characters',
  //      MOBILENUMBER_MAXLENGTH: 'Mobile Number should have atmost 15 characters',
        MOBILENUMBER_MINLENGTH: 'Mobile Phone should have atleast 10 characters.',
        MOBILENUMBER_MAXLENGTH: 'Mobile Phone should have atmost 15 characters.',
        NOT_A_NUMBER: 'Invalid characters.',
        NOT_A_VALID_MOBILE_NUMBER: 'Mobile Phone entered is invalid.',
        NOT_A_VALID_PHONE_NUMBER: 'Phone Number entered is invalid.',
        NO_FORWARD_RECIPIENTS: 'Forward recipients are not available.',
        CITY: 'Invalid City name format.',
        STATE: 'Invalid State name format.',
        COUNTY: 'Invalid County name format.',
        COUNTRY: 'Invalid Country name format.',
        DESCRIPTION: 'Invalid Description format.',
        NAME: 'Invalid Name format.',
        FIRST_NAME: 'Invalid First Name format.',
        LAST_NAME : 'Invalid Last Name format.',
        COMPANYSHORTNAME: 'Invalid company Short Name format.',
        COMPANYNAME: 'Invalid company Name format.',
        NO_USERS : 'No users found!',
        WORK_PHONE_INVALID : 'Work Phone entered is invalid.',
        WORK_PHONE_REQUIRED : 'Work Phone is mandatory and needs to be entered.',
        MOBILE_PHONE_INVALID : 'Mobile Phone entered is invalid.',
        MOBILE_PHONE_REQUIRED : 'Mobile Phone is mandatory and needs to be entered.',
        ADDRESS_CANNOT_BE_SAME : "Address1 and Address2 cannot be the same.",
        ZIPCODE_NOT_FOUND:'Could not find any location for the given Zip Code.',
        FROMDATE_VALIDATION: 'From Date should be before To Date.',
        TODATE_VALIDATION: 'On same date From Time should be before To Time.'
    },

    LEFTNAVICONS: {
        DASHBOARD: 'Dashboard',
        ACTIVECANDIDATESJOBVIEW: 'Active Candidate Job View',
        PASSIVECANDIDATESJOBVIEW: 'Passive Candidate Job View',
        CANDIDATESVIEW: 'Candidates',
        REQUISITIONS: 'Requisitions',
        TEAMMEMBERS: 'Users',

        REPORTS: 'Reports',
        TEAMMEMBERPERFORMANCE: 'Team Member Performance',
        BYTEAMMEMBER: 'By Team Member',
        BYMEASURES: 'By Measures',
        SOURCINGPERFORMANCE: 'Sourcing Performance',
        CANDIDATEPERFORMANCE: 'Candidate Performance',
        REQUISITIONPERFORMANCE: 'Requisition Performance',
        CLIENTREPORT: 'Client Report',

        UPLOADREQUISITIONS: 'Upload Requisitions',
        UPLOADRESUMES: 'Upload Resumes',
        MAIL: 'Mail',
        NOTIFICATIONS: 'Notifications',

        ADMIN: 'Admin',
        MANAGECOMPANY: 'Manage Company',
        MANAGECLIENT: 'Manage Client',
        SWITCHCOMPANY: "Switch Company",
        MANAGEUSER: 'Manage User',
        MANAGEROLEENTITY: 'Manage Role Entity Definition',
        MANAGESETTINGS: 'Manage Settings',
        MANAGEASSESSMENTS: 'Manage Assessments',
        MANAGEBU: 'Manage BU',
        FEEDBACKFORMS: 'Feedback Forms',
        RECRUITERFEEDBACK:'Recruiter Feedback',
        PHONESCREENFEEDBACK:'Phone Screen Feedback',
        INTERVIEWFEEDBACK: 'Interview Feedback'
    },

    /**
     * work flow steps names
     */
    STEPNAMES: {
        USERMANAGEMENT: {
        	CREATE_USER: 'Create User',
            STEP_ONE: 'Personal Details',
            STEP_TWO: 'Define Scope',
            STEP_THREE: 'Add Forward Recipient'
        }
    },

    /**
     * Breadcrumbs
     */
    BREADCRUMBS: {
        HOME: 'Home',
        USER_MANAGEMENT: 'User Management',
        UPLOAD_RESUME: 'Upload Resume',
        COMPANY_MANAGEMENT: 'Company Management',
        CLIENT_MANAGEMENT: 'Client Management',
        EDIT:'Edit',
        BU_MANAGEMENT: 'BU Management',
        CLIENT_REPORTS: 'Client Reports',
        SETTINGS: 'Settings'
    },

	/**
	 * Validation Messages for Login module
	 */
    LOGIN: {
        INVALID: 'Invalid Email or Password.',
        FORGOT_PASSWORD: 'Forgot Password?',
        REMEMBER_ME: 'Remember me'
    },
    /**
	 * Validation Messages for Forget Password module
	 */
    FORGETPASSWORD: {
        FORGOT_PASSWORD: 'Forgot your password?',
        PASSWORD_RECOVERY_MESSAGE: 'Enter Email address to recover your password.',
        BACK_TO_LOGIN: 'Back to login'
    },
    /**
    * Validation Messages for Change Password module
    */
    RESETPASSWORD: {
        CURRENTPASSWORD_MANDATORY: 'Current Password should be entered.',
        NEWPASSWORD_MANDATORY: 'Enter New Password.',
        CONFIRMNEWPASSWORD_MANDATORY: 'Confirm New Password.',
        PASSWORDSNOTEQUAL: 'New/Confirm Passwords don’t match.',
        PASSWORDS_SAME: 'Old and new passwords should not be same.',
        RESET_PASSWORD_SUCCESSFULLY:'Your password has been reset successfully.',
        BACK_TO_LOGIN: 'Back to login',
        BACK_TO_DASHBOARD: 'Back to Dashboard'
    },

    STATETRANSITIONS: {
        FORMLOGIN: 'login',
        FORMDASHBOARD: 'dashboard',
        FROMLOGOUT: 'logout'
    },

    /**
     * All Labels in Company Management module
     */
    COMPANY_MANAGEMENT: {
        ENTER_MANDATORY_FIELDS: 'Please enter all mandatory fields.',
        CREATE_COMPANY: 'Create Company',
        CREATE_CLIENT: 'Create Client',
        CREATE_BU: 'Create BU',
        BU: 'BU',
        SCOPES: 'Scope available to the Admin',
        COMPANY_DETAILS: 'Company Details',
        CLIENT_DETAILS: 'Client Details',
        BU_DETAILS: 'BU Details',
        PRIMARY_CONTACT: 'Primary Contact',
        FOURDOTFIVE_ADMIN_CONTACT: 'Set 4dot5 Admin',
        //        LICENCE_PREFERENCES: 'Licence Preferences',
        LICENCE_PREFERENCES: 'Workflow Settings',
        PRICING_PLAN: 'Plan',
        SCREENING_QUESTIONS: 'Screening Questions',
        STEP_ONE: '1',
        STEP_TWO: '2',
        STEP_THREE: '3',
        STEP_FOUR: '4',
        STEP_FIVE: '5',
        COMPANY_TYPE: 'Type',
        COMPANY_SHORT_NAME: 'Short Name',
        COMPANY_NAME: 'Name',
        ADDRESS_1: 'Address 1',
        ADDRESS_2: 'Address 2',
        CITY: 'City',
        STATE: 'State',
        ZIPCODE: 'Zip Code',
        COUNTY: 'County',
        COUNTRY: 'Country',
        COMPANY_LOGO_URL: 'Company Logo URL',
        COMPANY_DESCRIPTION: 'Description',
        SELECTED_INDUSTRIES: 'Selected Industries',
        INDUSTRY_GROUP: 'Select Industry Group',
        INDUSTRY: 'Select Industry',
        FOUR_DOT_FIVE_SCOPES_INDUSTRY: 'Industry',
        FIRST_NAME: 'First Name',
        LAST_NAME: 'Last Name',
        WORK_PHONE: 'Work Phone',
        MOBILE_PHONE: 'Mobile Phone',
        EMAIL: 'Email',
        IM: 'IM',
        NAME: 'Name',
        REQUISITION_CATEGORY: 'Requisition Category',
        JOB_REQUISITION: 'Job Requisition',
        CLIENT: 'Client',
        ALL: 'All',
        SECONDARY_CONTACT: 'Secondary Contact'
    },

    LABLES: {
        FIRST_NAME: 'First Name',
        LAST_NAME: 'Last Name',
        ADDRESS_1: 'Address 1',
        ADDRESS_2: 'Address 2',
        CITY: 'City',
        STATE: 'State',
        ZIPCODE: 'Zip Code',
        COUNTY: 'County',
        COUNTRY: 'Country',
        WORK_PHONE: 'Work Phone',
        MOBILE_PHONE: 'Mobile Phone',
        EMAIL: 'Email',
        IM: 'IM',
        ALL: 'All',
        LIST: 'List',
        INDUSTRY: 'Industry',
        CLIENT: 'Client',
        BU: 'BU',
        DATE_PICKER: 'Date Picker',
        REQUISITION_ROLE:' Requisition Role',
        JOB_REQUISITION_ROLES: 'Job Requisition Roles',
        REQUISITION_CATEGORY: 'Requisition Category',
        INDUSTRY_LIST: 'Industry List',
        CLIENT_LIST: 'Client List',
        BU_LIST: 'BU List',
        JOB_REQUISITION_ROLES_LIST: 'Job Requisition Roles List',
        REQUISITION_CATEGORIES: 'Requisition Categories',
        RECRUITERS: 'Recruiters',
        RECRUITER: 'Recruiter',
        REQUISITIONS: 'Requisitions',
        REQUISITION_TITLE: 'Requisition Title',
        REQUISITION_STATUS: 'Requisition Status',
        REQUISITION_NUMBER: 'Requisition #',
        REQ_NUMBER: 'Requisition Number',
        CONTACT: 'Contact',
        USER_MANAGEMENT: {
            ROLE: 'Role',
            PROFILEPICTURE: 'Profile Picture',
            MANAGER: 'Manager',
            FORWARDRECIPIENTS: 'Forward Recipient',
            FROM_DATE: 'From Date',
            FROM_TIME: 'From Time',
            TO_DATE: 'To Date',
            TO_TIME: 'To Time'
        },
        UPLOAD_REQUISITION: {
            RECRUITER_SELECT_OPT1: 'Make all Openings Available to All Recruiters',
            RECRUITER_SELECT_OPT2: 'Split Openings between Recruiters & Adjust, if required'
        },
        UPLOAD_RESUME: {
            SOURCE_TYPE: 'Source Type'
        },
        WORKFLOW_SETTINGS : {
            TEST_LINK_NAME: 'Test Link Name',
            ACCESS_TIME: 'Access Time',
            INVITE_CANDIDATE_ONLY: ' Invite Candidate Only',
            IMAGE_PROCTORING: 'Image Proctoring',
            ADD_PROCTORING_IMAGES_TO_REPORT: 'Add Proctoring Images to Report',
            RESUMES_TO_PULL_COUNT: 'Number of Resumes to pull'
         }

    },

	/**
     * Button Names 
     */
    BUTTONS: {
        EDIT: 'Edit',
        PREVIOUS: 'Prev',
        NEXT: 'Next',
        SAVE: 'Save',
        CANCEL: 'Cancel',
        LOGIN_PAGE: {
            LOGIN: 'LOGIN'
        },
        FORGOT_PASSWORD_PAGE: {
            RESET_PASSWORD: 'Reset password'
        },
        RESET_PASSWORD_PAGE: {
            RESET_PASSWORD: 'Reset password'
        },
        COMPANY_MANAGEMENT_PAGE: {
            PREVIOUS: 'Prev',
            NEXT: 'Next',
            SAVE: 'Save',
            CANCEL: 'Cancel',
            EDIT: 'Edit',
            DELETE: 'Delete'
        }
    },

    NOTIFICATIONS: {
        NOTHING_TO_SAVE : 'Nothing to be saved!',
        NOTHING_TO_UPDATE : 'Nothing to be updated!',
        FEEDBACK:{
          RECRUITERQUESTIONS: 'Recruiter feedback saved successfully',
          PHONEQUESTIONS: 'Phone feedback saved succsssfully',
          INTERVIEWQUESTIONS: 'Interview feedback saved successfully'

        },
        USER: {
            CREATESUCCESS:" created in '<b><i>UnVerified</i></b>' state successfully." +
            		"This person has to login and change password to become '<i><b>Active</i></b>'.",
            CREATEERROR: ' not created.. something went wrong ?',
            UPDATESUCCESS: ' updated successfully.',
            UPDATEERROR: ' not updated.. something went wrong ?',
            OWN_PROFILE_UPDATE : 'Your profile has been updated successfully.',
            SAVESUCCESS: " saved in '<b><i>UnVerified</i></b>' state successfully.",
            SAVEERROR: ' not saved.. something went wrong ?',
            DELETESUCCESS: ' deleted successfully.',
            DELETEERROR: ' not deleted.. something went wrong ?',
            USER: 'User',
            NOTHING_UPDATED : 'Nothing is Updated for User',
            USER_ALERT_WITH_REPORTEES:"Cannot change the role of this user as he/she has direct reports assigned. <br>Please re-assign them before trying to change the role again.",
            USER_ALERT_WITH_NO_REPORTEES:'Changing the role could reset the scope of this user.<br> Are you sure you want to continue? ',
            CHANGE_MANAGER_TO_ADMIN_REPORTEES: 'This user has direct reports. Do you want to move the direct reports as well? <br>Remember that changing to admin could reset the scope of this user and his direct reports.<br> Are you sure you want to continue?',
            CHANGE_MANAGER: 'Changing the manager could reset the scope of this user.<br>  Are you sure you want to continue?',
            CHANGE_MANAGER_REPORTEES: 'This user has direct reports. Do you want to move the direct reports as well? <br> Also, remember that changing the manager could reset the scope of this user and his direct reports. Are you sure you want to continue?',
            DELETE_USER :'You are about to delete user ',
            WITH_ROLE: ' with role ',
            DELETE_USER_NO_REPORTEE: 'This user will no longer be accessible in the system. Past records will be maintained though. Please be very careful when confirming this operation.',
            CANNOT_DELETE_USER: "You cannot delete user",
            DELETE_USER_WITH_REPORTEE: ' This user has direct reports assigned. Please assign them to others before deleting.',
            DELETE_USER_REPORTEE_1: "You cannot delete this user as he/she has direct reports assigned.",
            DELETE_USER_REPORTEE_2: "Please assign them to others before deleting.",
            SCOPE_RESET : 'The scope of this user is modified. Please update the scope.',
            NO_CLIENTS :'No clients available at present.',
            NO_BULIST :'No BUs available at present.',
            BULIST_NOT_SETUP : 'No BU has been setup till now.',
            RES_NOT_SETUP : 'Role Entity Scope for the Company is not defined yet.',
            DEFINE_SCOPE: 'Role Scope for this company has not been defined. Please define the scope for all roles within the company and then create users.'
        },
        COMPANY: {
            CREATESUCCESS: 'created successfully',
            COMPANYDRAFTSUCCESS: "saved in '<b><i>Draft</i></b>' state successfully.",
            CREATEERROR: '4.5 Admin could not be set.. something went wrong.',
            UPDATESUCCESS: 'updated successfully.',
            UPDATEERROR: '',
            
            SAVESUCCESS: "saved in '<b><i>Draft</i></b>' state successfully.",
            SAVEEERROR: 'not saved.. something went wrong ?',
            DELETESUCCESS: 'deleted successfully.',
            DELETEERROR: '',
            COMPANY : 'Company',
            DELETE_CONFIRMATION_MESSAGE : 'will make it inaccessible for use. Please be very careful when confirming this operation',
            DELETING: 'Deleting'
        },
        CLIENT: {
            CREATESUCCESS: 'created successfully.',
            CREATEDRAFTSUCCESS: "saved in '<b><i>Draft</i></b>' state successfully.",
            CREATEERROR: '',
            UPDATESUCCESS: 'updated successfully.',
            UPDATEERROR: '',
            
            SAVESUCCESS: 'saved successfully.',
            SAVEEERROR: 'not saved.. something went wrong ?',
            DELETESUCCESS: 'deleted successfully.',
            DELETEERROR: '',
            CLIENT: 'Client',
            DELETE_CLIENT_CONFIRMATION: 'Deleting the client will make it inaccessible for use. All Open requisitions will be cancelled. Please be very careful when confirming this operation.',
            NOTHING_TO_UPDATED: 'Nothing to be updated!'
        },
        
        BU: {
            CREATESUCCESS: 'created successfully.',
            CREATEDRAFTSUCCESS: "saved in '<b><i>Draft</i></b>' state successfully.",
            CREATEERROR: '',
            UPDATESUCCESS: 'BU updated successfully.',
            UPDATEERROR: '',
            SAVESUCCESS: 'BU saved successfully.',
            SAVEEERROR: 'BU not saved.. Something went wrong ?',
            DELETESUCCESS: 'BU deleted successfully.',
            DELETEERROR: '',
            DELETE_BU_CONFIRMATION: 'Deleting the BU will make it inaccessible for use. All open requisitions will be cancelled. Please be very careful when confirming this operation.',
            BU: 'BU'
        },
        SETTINGS : {
        	
        	UPDATE_SUCCESS: 'updated successfully.',
            SETTINGS_FOR: 'Settings for ',
            SETTINGS_FOR_COMPANY: 'Settings for company ',
        	VIEWING_SETTINGS : "You are viewing",
        	MODIFYING_SETTINGS : "You are modifying",
        	GLOBAL_SETTINGS : "global settings.",
        	COMPANY_WITHOUT_SETTINGS : "settings currently set at ",
        	CLIENT_WITHOUT_SETTINGS : "settings currently set at ",
        	SETTING : "settings.",
        	COPY_SETTINGS : "You are copying settings from ",
        	NO_BUS : "Currently no BUs are available for ",
        	NO_CLIENTS : "Currently no Clients are available for ",
        	NO_CLIENTS_USER : "Currently no clients of ",
        	NO_BUS_USER : "Currently no BUs of ",
        	NO_CLIENTBU_USER_ACCESSIBLE : " are accessible to you.",
        	COMPANY : " company!",
        	RESETTING : "You are resetting ",
        	FOUR_DOT_FIVE : " global settings."
        },
        NOTIFICATION_PREFS : {
            NP_FOR_USER : 'Notification Preferences for User',
            SAVESUCCESS : 'saved successfully.',
            UPDATESUCCESS : 'updated successfully.',
            OWN_NP_UPDATE : 'You have updated your notification preferences successfully.'
        },
        COMPANY_RES : {
            SET_SUCCESS1 : "Role Entity Scope for Company '<b><i>",
            SET_SUCCESS2 : "</i></b>' is defined successfully.",
            UPDATE_SUCCESS1 : "Role Entity Scope for Company '<b><i>",
            UPDATE_SUCCESS2 : "</i></b>' is updated successfully.",
            BULIST_NOT_SETUP : 'No BU has been setup till now.'
        },
        UPLOAD:{
            UPLOAD_SUCCESS: "Requisition was uploaded successfully.",
            RESUME_UPLOAD_SUCCESS: "Resume was uploaded successfully."
        },
        ROLE_ENTTITY_SUCCESS : 'company is successfully created.',
        ROLE_ENTTITY_UPDATE : 'company is successfully updated.',
        ROLE_ENTTITY_SCOPE : 'Role Entity Scope for '
    },

    FOOTABLE: {
        NO_ROWS: 'There are no clients for this staffing company!'
    },

    GEOCODING: {
        API_KEY: 'AIzaSyA7qe_jJn9M5fjun2HxxFrzLtMdKDNI-4M'
    },
    DASHBOARD: {
    	X_AXIS_LABEL : 'Past six weeks',
    	Y_AXIS_LABEL_LEFT : 'Time (in days)',
    	Y_AXIS_LABEL_RIGHT_1 : 'Number of candidates',
    	Y_AXIS_LABEL_RIGHT_2 : 'Number of requisitions',
    	CANDIDATE_REPORT: {
            TIME_TO_CONVERT: 'Time to Convert',
            TIME_TO_PLACE:'Time to Place',
            ACTIVE: 'Active',
            PLACED: 'Placed',
            PASSIVE: 'Passive',
            CONVERTED: 'Converted'
            
        },
        REQUISITIONS_REPORT: {
        	TIME_TO_FILL: 'Time to Fill',
            NEW: 'New',
            FILLED: 'Filled'
        },
        GRAPH: {
            BAR_COLOR : '#03a9f4',
            BAR_WIDTH_1 : 24,
            BAR_WIDTH_2 : 60,
            BAR_WIDTH_3 : 60,
            BAR_WIDTH_4 : 800,
            BAR_ALIGNMENT : 'center',
            LINE_ONE_COLOR : '#e84e40',
            LINE_WIDTH : 3,
            POINT_COLOR : '#ffffff',
            POINT_WIDTH : 1,
            LINE_TWO_COLOR : '#8bc34a'
        },
        PLOT: {
            COLORS_1 : '#03a9f4',
            COLORS_2 : '#f1c40f',
            COLORS_3 : '#2ecc71',
            COLORS_4 : '#3498db',
            COLORS_5 : '#9b59b6',
            COLORS_6 : '#95a5a6',
            TICK_COLOR : "#f2f2f2",
            LABEL_BOX_BORDER_COLOR : "#000000",
            X_AXIS_LABEL_FONT : 'Open Sans, sans-serif',
            Y_AXIS_POSITION_LEFT : "left",
            Y_AXIS_POSITION_RIGHT : "right",
            AXIS_LABEL_FONT_SIZE : 12,
            Y_AXIS_LABEL_FONT : "Verdana, Arial, Helvetica, Tahoma, sans-serif",
            Y_AXIS_LABEL_PADDING : 5,
            X_AXIS_LABEL_PADDING : 10

        }
    }
});
