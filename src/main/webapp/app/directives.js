/**
 * All directives related to the 4dot5 project are written here
 */

fourdotfivedirectives.directive('candidateJobCard', function candidateJobCard() {
    return {
        restrict: 'E',
        scope: {
            candidatesData: '=',
            filterCriteria: '=',
            removeFromList: '=',
            candidateType: '=',
            qualifiedCandidatesData: '=',
            candidateClickCallback: '&',
            candidateState: '='
        },

        link: function($scope, $elem, $attr) {
            // alert(angular.toJson($scope));
            // alert(angular.toJson($attr));
            // $scope.subscr = angular.copy($scope.subscription);
            // console.log($attr);
            // console.log($scope);
            $scope.candidateState = $attr.candidateState;
            //            console.log('13' + candidateState);
            //            console.log('13' + scope.candidateState);

        },
        templateUrl: 'app/partials/directivetemplates/candidate-card.html'
    }
});

fourdotfivedirectives.directive('candidateCompareJobListItems', function candidateCompareJobListItems() {
    return {
        restrict: 'E',
        scope: {
            jobDetailsData: '=',
            assetType: '='
                /*,
                			candidateAssets: '='*/
                /*candidateType: '=',
                qualifiedCandidatesData: '=',
                candidateClickCallback: '&'*/
        },
        templateUrl: 'app/partials/directivetemplates/candidate-compare-job-list-item.html'
    }
});
fourdotfivedirectives.directive('candidateCompareAssetListItems', function candidateCompareAssetListItems() {
    return {
        restrict: 'E',
        scope: {
            candidateAssets: '=',
            assetType: '=',
            // candidateAssets: '='
            /*candidateType: '=',
            qualifiedCandidatesData: '=',
            candidateClickCallback: '&'*/
        },
        templateUrl: 'app/partials/directivetemplates/candidate-compare-list-item.html'
    }
});

fourdotfivedirectives.directive('allowPattern', function allowPatternDirective() {
    return {
        restrict: "A",
        compile: function(tElement, tAttrs) {
            return function(scope, element, attrs) {
                // I handle key events
                element.bind("keypress", function(event) {
                    var keyCode = event.which || event.keyCode; // I safely get the keyCode pressed from the event.
                    var keyCodeChar = String.fromCharCode(keyCode); // I determine the char from the keyCode.

                    // If the keyCode char does not match the allowed Regex Pattern, then don't allow the input into the field.
                    if (!keyCodeChar.match(new RegExp(attrs.allowPattern, "i"))) {
                        event.preventDefault();
                        return false;
                    }
                });
            };
        }
    };
});

fourdotfivedirectives.directive('addressCheck', function() {
    return {
        require: 'ngModel',
        link: function(scope, elem, attrs, ctrl) {
            var firstPassword = '#' + attrs.addressCheck;
            console.log('Address: ' + firstPassword);
            elem.add(firstPassword).on('keyup', function() {
                scope.$apply(function() {
                    var v = elem.val() === $(firstPassword).val();
                    ctrl.$setValidity('addressmatch', v);
                });
            });
        }
    }
});

fourdotfivedirectives.directive('convertToNumber', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function(val) {
                return val != null ? parseInt(val, 10) : null;
            });
            ngModel.$formatters.push(function(val) {
                return val != null ? '' + val : null;
            });
        }
    };
});