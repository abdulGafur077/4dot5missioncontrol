fourdotfivedirectives.directive('addCandidates', function addCandidate(){
   return {
       restrict: 'E',
       scope: {},
       bindToController:{
           jobId: '@',
           saveCallback: '&'
       },
       controller: 'AddCandidatesController',
       controllerAs: 'addCandidates',
       templateUrl: 'app/directives/addCandidates/add-candidates.html'
   }
});