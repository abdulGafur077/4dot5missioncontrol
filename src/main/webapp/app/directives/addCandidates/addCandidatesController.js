/*
    This is the controller for Add Candidates Directive
 */

function AddCandidatesController($rootScope, $state, candidateService, AddCandidate, alertsAndNotificationsService){
    var vm = this;

    // variables
    vm.currentCompanyId = null;
    // flags
    vm.addWithResumesFlag = true;
    vm.candidatesArray = [];
    vm.savingFlag = false;
    // methods
    vm.addAnotherCandidate = addAnotherCandidate;
    vm.removeCandidate = removeCandidate;
    vm.saveCandidates = saveCandidates;

    vm.init = init;

    vm.init();

    function init(){
        vm.addWithResumesFlag = true;
        vm.currentCompanyId = $rootScope.userDetails.company.companyId;
        vm.addAnotherCandidate();
    }

    function addAnotherCandidate() {
        var addCandidate = new AddCandidate();
        addCandidate.jobId = vm.jobId;
        addCandidate.companyId = vm.currentCompanyId;
        addCandidate.addStatus = 'add';
        addCandidate.errorMessages = [];
        vm.candidatesArray.push(addCandidate);
    }

    function removeCandidate(index){
        vm.candidatesArray.splice(index,1);
    }

    function saveCandidates(){
        var candidatesArrayForSave = [];
        var candidateAddUIErrorExists = false;
        angular.forEach(vm.candidatesArray, function (val, key) {
            if(val.addStatus != 'success'){
                candidatesArrayForSave.push(angular.copy(val));
                val.addForm.$submitted = true;
                if(val.addForm.$invalid){
                    candidateAddUIErrorExists = true;
                }
            }
        });
        if(!candidateAddUIErrorExists){
            var candidatesArrayForSave = _.map(candidatesArrayForSave, function(y){
                delete y.addForm;
                delete y.addStatus;
                delete y.errorMessages;
                return y;
            });
            if(candidatesArrayForSave.length > 0){
                vm.savingFlag = true;
                candidateService.addCandidates(candidatesArrayForSave, function (data) {
                    var allCandidatesAddedFlag = true;
                    var responseArrayIndexCounter = 0;
                    // iterate over candidates array and look for candidates whose current addStatus is not already 'success'
                    angular.forEach(vm.candidatesArray, function (val, key) {
                        // if status is not already a success, we have a candidate who was added in the current request
                        if(val.addStatus != 'success'){
                            if(data[responseArrayIndexCounter].status == 'PASSED'){
                                val.addStatus = 'success';
                            }else{
                                allCandidatesAddedFlag = false;
                                val.addStatus = 'error';
                                val.errorMessages = data[responseArrayIndexCounter].messages;
                            }
                            // increment the responseArrayIndexCounter to be able to access the next candidate in the response
                            responseArrayIndexCounter++;
                        }
                    });
                    vm.savingFlag = false;
                    if(allCandidatesAddedFlag){
                        bootbox.alert({
                            closeButton: false,
                            title: "<div class='alert alert-success' style='margin-bottom: 0px;'><i class='fa fa-check fa-fw fa-lg'></i><strong>Success!</strong></div>",
                            message: 'All candidates have been added successfully!',
                            className: "zIndex1060",
                            buttons: {
                                ok: {
                                    label: 'Ok',
                                    className: 'btn-info'
                                }
                            },
                            callback: function () {
                                if(vm.saveCallback){
                                   vm.saveCallback();
                                }
                            }
                        });
                    }
                }, function (error) {
                    vm.savingFlag = false;
                    if ($rootScope.isOnline) {
                        alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                    }
                });
            }
        }
    }
}

AddCandidatesController.$inject = ['$rootScope','$state','candidateService','AddCandidate','alertsAndNotificationsService'];

fourdotfivedirectives.controller('AddCandidatesController', AddCandidatesController);