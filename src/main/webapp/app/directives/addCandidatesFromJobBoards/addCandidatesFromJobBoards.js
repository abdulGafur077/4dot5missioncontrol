fourdotfivedirectives.directive('addCandidatesFromJobBoards', function addCandidatesFromJobBoards() {
    return {
        restrict: 'E',
        scope: {},
        bindToController: {
            jobId: '@',
            saveCallback: '&'
        },
        controller: 'AddCandidatesFromJobBoards',
        controllerAs: 'addCandidatesFromJobBoards',
        templateUrl: 'app/directives/addCandidatesFromJobBoards/add-candidates-from-job-boards.html'
    }
});