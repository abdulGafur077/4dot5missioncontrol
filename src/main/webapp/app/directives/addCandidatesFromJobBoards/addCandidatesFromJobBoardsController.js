/*
    This is the controller for Add Candidates from JobBoards
 */

function AddCandidatesFromJobBoards($rootScope, resumeService, jobService, alertsAndNotificationsService, modalService, Job, MESSAGECONSTANTS) {
    var vm = this;

    //variables
    vm.job = {};
    vm.jobBoardDetails = [];
    vm.requisitionDetails = {};
    vm.MESSAGECONSTANTS = MESSAGECONSTANTS;
    vm.resumePullCounts = [5, 10, 15, 20, 25, 30, 40, 50, 75, 100];
    // flags
    vm.isJobBoardSelected = false;
    vm.isGetCandidateFetchCalled = false;

    //methods
    vm.init = init;
    vm.getJob = getJob;
    vm.getAllJobBoards = getAllJobBoards;
    vm.checkStatusAndGetResumesFromJobBoards = checkStatusAndGetResumesFromJobBoards;
    vm.getResumesFromJobBoards = getResumesFromJobBoards;
    vm.getRequisitionDetails = getRequisitionDetails;

    //change events
    vm.onJobBoardSelectionChange = onJobBoardSelectionChange;

    vm.init();

    function init() {
        vm.companyId = $rootScope.userDetails.company.companyId;
        vm.savingFlag = false;
        vm.getJob(function () {
            vm.getAllJobBoards(function () {
                _setJobBoardDetails();
            });
            vm.getRequisitionDetails();
        });
    }

    function getJob(successCallback) {
        jobService.getJob(vm.jobId, function (data) {
            vm.job = _parseJobResponse(data);
            successCallback(data);
        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }

    function _parseJobResponse(data) {
        var tempJob = new Job();
        tempJob.id = data.id;
        tempJob.title = data.title;
        tempJob.licensePreferences = data.licensePreferences;
        tempJob.jobBoardDetails = angular.copy(vm.jobBoardDetails);
        if (!_.isNull(tempJob.licensePreferences) && !_.isNull(tempJob.licensePreferences.jobBoardDetails)) {
            angular.forEach(tempJob.jobBoardDetails, function (val, key) {
                if (_.findIndex(tempJob.licensePreferences.jobBoardDetails, { 'boardId': val.boardId, 'enabled': true }) == -1) {
                    val.enabled = false;
                }
            });
        }
        return tempJob;
    }

    function getAllJobBoards(successCallback) {
        resumeService.getResumeSourceTypes(vm.companyId, function (data) {
            if (data.length > 0) {
                angular.forEach(data, function (val, key) {
                    val.boardId = val.id;
                    val.enabled = true;
                    delete val.id;
                });
                vm.jobBoardDetails = data;
                if (successCallback) {
                    successCallback();
                }
            }
        }, function (error) {
            if ($rootScope.isOnline) {
            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }

    function _setJobBoardDetails() {
        vm.job.jobBoardDetails = angular.copy(vm.jobBoardDetails);
        if (!_.isNull(vm.job.licensePreferences) && !_.isNull(vm.job.licensePreferences.jobBoardDetails)) {
            angular.forEach(vm.job.jobBoardDetails, function (val, key) {
                if (_.findIndex(vm.job.licensePreferences.jobBoardDetails, { 'boardId': val.boardId, 'enabled': true }) == -1) {
                    val.enabled = false;
                }
            });
        }
        angular.forEach(vm.job.jobBoardDetails, function (val, key) {
            if (val.enabled == true) {
                vm.isJobBoardSelected = true;
            }
        });

    }

    function checkStatusAndGetResumesFromJobBoards() {
        vm.isGetCandidateFetchCalled = true;
        jobService.getGetMoreResumesJobStatus(vm.job.id, function (jobInProgress) {
            if (jobInProgress == true) {
                vm.isGetCandidateFetchCalled = false;
                alertsAndNotificationsService.showBannerMessage("Candidates Fetch from Job Boards already <b>'INPROGRESS'</b>. Please try after sometime.", 'info');
            } else {
                vm.getResumesFromJobBoards();
            }
        }, function (error) {
            vm.isGetCandidateFetchCalled = false;
            if ($rootScope.isOnline) {
            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }

    function getResumesFromJobBoards() {
        jobService.getResumesFromJobBoards(vm.job.id, vm.job.jobBoardDetails, vm.job.licensePreferences.resumesToPullPerJobBoardCount, function(data) {
            vm.isGetCandidateFetchCalled = false;
            if(vm.saveCallback){
                vm.saveCallback();
            }
            bootbox.alert({
                closeButton: false,
                title: "<div class='alert alert-success' style='margin-bottom: 0px;'><i class='fa fa-check fa-fw fa-lg'></i><strong>Success!</strong></div>",
                message: "Candidates are being fetched from Job Boards. You will be notified once the candidates are fetched",
                className: "zIndex1060",
                buttons: {
                    ok: {
                        label: 'Ok',
                        className: 'btn-info'
                    }
                },
                callback: function() {
                    modalService.addModalOpenClassToBodyIfAnyModalIsOpen();
                }
            });

        }, function(error) {
            vm.isGetCandidateFetchCalled = false;
            bootbox.alert({
                closeButton: false,
                title: "<div class='alert alert-danger' style='margin-bottom: 0px;'><i class='fa fa-close fa-fw fa-lg'></i><strong>Fail!</strong></div>",
                message: error.message,
                className: "zIndex1060",
                buttons: {
                    ok: {
                        label: 'Ok',
                        className: 'btn-info'
                    }
                },
                callback: function() {
                    modalService.addModalOpenClassToBodyIfAnyModalIsOpen();
                }
            });

            // bootbox.alert({
            //     closeButton: false,
            //     title: "<div class='alert alert-warning' style='margin-bottom: 0px;'><i class='fa fa-clpse fa-fw fa-lg'></i><strong>Alert!</strong></div>",
            //     message: error.message,
            //     className: "zIndex1060",
            //     buttons: {
            //         ok: {
            //             label: 'Ok',
            //             className: 'btn-info'
            //         }
            //     },
            //     callback: function() {
            //         modalService.addModalOpenClassToBodyIfAnyModalIsOpen();
            //     }
            // });
        });
    }

    function getRequisitionDetails() {
        jobService.getRequisitionDetails(vm.job.id, vm.companyId, function (data) {
            vm.requisitionDetails = data;
        }, function (error) {
            if ($rootScope.isOnline) {
            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }

    function onJobBoardSelectionChange() {
        vm.isJobBoardSelected = false;
        angular.forEach(vm.job.jobBoardDetails, function (val, key) {
            if (val.enabled == true) {
                vm.isJobBoardSelected = true;
            }
        });
    }
}

AddCandidatesFromJobBoards.$inject = ['$rootScope', 'resumeService', 'jobService', 'alertsAndNotificationsService', 'modalService', 'Job', 'MESSAGECONSTANTS'];

fourdotfivedirectives.controller('AddCandidatesFromJobBoards', AddCandidatesFromJobBoards);