fourdotfivedirectives.directive('addUpdateCandidateContact', function addUpdateCandidateContact(){
    return {
        restrict: 'E',
        scope: {},
        bindToController:{
            candidateId: '@',
            saveCallback: '&',
            cancelCallback: '&'
        },
        controller: 'AddUpdateCandidateContactController',
        controllerAs: 'candidateContact',
        templateUrl: 'app/directives/addUpdateCandidateContact/add-update-candidate-contact.html'
    }
});