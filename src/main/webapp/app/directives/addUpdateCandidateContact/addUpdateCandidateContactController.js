/*
    This is the controller for Add / Update of Candidate Contact Details
 */

function AddUpdateCandidateContactController($rootScope, candidateService, alertsAndNotificationsService){
    var vm = this;

    // variables
    vm.currentCompanyId = null;
    vm.updateContactType = '';
    vm.contactObject = {};
    vm.candidateName = '';
    // flags
    vm.addWithResumesFlag = true;
    vm.showProfileUrlFlag = false;
    vm.candidatesArray = [];
    vm.savingFlag = false;
    vm.loadingContactInfoFlag = false;
    // methods
    vm.getCandidateContactDetails = getCandidateContactDetails;
    vm.updateContactDetails = updateContactDetails;
    vm.init = init;

    vm.init();

    function init(){
        vm.updateContactType = 'phone';
        vm.getCandidateContactDetails();
        vm.contactObject.candidateId = vm.candidateId;
        vm.contactObject.companyId = $rootScope.userDetails.company.companyId;
    }

    function getCandidateContactDetails(){
        vm.loadingContactInfoFlag = true;
        candidateService.getCandidateProfile(vm.candidateId, function (data) {
            vm.candidateName = _.isNull(data.lastName) ? data.firstName : data.firstName + ' ' + data.lastName;
            vm.profileUrlText = '';
            vm.profileUrl = data.profileUrl;
            if(!_.isNull(vm.profileUrl)){
                vm.showProfileUrlFlag = true;
                if(data.profileType != 'Other'){
                    vm.profileUrlText = "View " + vm.candidateName + "'s " + data.profileType + " Profile";
                }else{
                    vm.profileUrlText = "View " + vm.candidateName + "'s Profile";
                }
            }
            vm.contactObject.emailId = data.email;
            vm.contactObject.phoneNumber = data.mobilePhone;
            vm.contactObject.address = data.address;
            vm.loadingContactInfoFlag = false;
        }, function (error) {
            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
        });
    }

    function updateContactDetails(){
        if(!vm.addUpdateContactForm.$invalid){
            vm.savingFlag = true;
            if(vm.updateContactType == 'phone'){
                candidateService.addUpdateCandidatePhone(vm.contactObject, function(data){
                    vm.savingFlag = false;
                    if(vm.saveCallback){
                        vm.saveCallback();
                    }
                }, function(error){
                    vm.savingFlag = false;
                    alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                });
            }else if(vm.updateContactType == 'email'){
                candidateService.addUpdateCandidateEmail(vm.contactObject, function(data){
                    vm.savingFlag = false;
                    if(vm.saveCallback){
                        vm.saveCallback();
                    }
                }, function(error){
                    vm.savingFlag = false;
                    alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                });
            }else if(vm.updateContactType == 'address'){
                candidateService.addUpdateCandidateAddress(vm.contactObject, function(data){
                    vm.savingFlag = false;
                    if(vm.saveCallback){
                        vm.saveCallback();
                    }
                }, function(error){
                    vm.savingFlag = false;
                    alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                });
            }
        }
    }
}

AddUpdateCandidateContactController.$inject = ['$rootScope','candidateService','alertsAndNotificationsService'];

fourdotfivedirectives.controller('AddUpdateCandidateContactController', AddUpdateCandidateContactController);