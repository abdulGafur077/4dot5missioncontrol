fourdotfivedirectives.directive('assessmentInfo', function assessmentInfo() {
    return {
        restrict: 'E',
        scope: {},
        bindToController: {
            jobMatchId: '@',
            assessmentType: '@'
        },
        controller: 'AssessmentInfoController',
        controllerAs: 'assessmentInfo',
        templateUrl: 'app/directives/assessmentInfo/assessment-info.html'
    }
});