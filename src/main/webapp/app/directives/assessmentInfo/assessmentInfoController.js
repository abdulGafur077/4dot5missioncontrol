/*
  * This is the controller for the assessment info directive
 */

function AssessmentInfoController($timeout, $filter, dateTimeUtilityService, DTOptionsBuilder, assessmentService, alertsAndNotificationsService) {
    var vm = this;

    //variables
    vm.assessmentTypeForService = 'Technical';
    vm.assessmentInfoDetails = [];
    vm.browserTimeZone = null;
    //flags
    vm.loadingFlag = true;
    //methods
    vm.init = init;
    vm.getSentAssessmentInfo = getSentAssessmentInfo;

    vm.init();

    function init() {
        vm.dtOptions = DTOptionsBuilder.newOptions().withOption('paging', false).withOption('ordering',false).withOption('searching',false);
        vm.dtColumnDefs = [];
        vm.browserTimeZone = dateTimeUtilityService.guessBrowserTimeZoneName();
        if(vm.assessmentType == 'Value Assessment'){
            vm.assessmentTypeForService = 'Value';
        }
        vm.getSentAssessmentInfo();
    }

    function getSentAssessmentInfo(){
        vm.loadingFlag = true;
        assessmentService.getSentAssessmentInfo(vm.jobMatchId, vm.assessmentTypeForService, function (data) {
            vm.loadingFlag = false;
            angular.forEach(data, function(val, key){
                if(_.isNull(val.assessmentAccessTime)){
                    val.assessmentAccessTime = '-'
                }else if(val.assessmentAccessTime == 'anyTime'){
                    val.assessmentAccessTime = 'Any Time';
                }else if(val.assessmentAccessTime == 'timeBound'){
                    val.assessmentAccessTime = 'Time Bound';
                }else if(val.assessmentAccessTime == 'slotWise'){
                    val.assessmentAccessTime = 'Slot Wise';
                }
                if(_.isNull(val.candidateCurrentJobState)){
                    val.candidateCurrentJobState = '-';
                }
                if(_.isNull(val.score)){
                    val.score = '-';
                }
                if(_.isNull(val.timeZone)) {
                    val.timeZone = '-';
                }
                if(!_.isNull(val.startDateTime)){
                    if(val.timeZone == '-'){
                        val.startDateTime = new Date(val.startDateTime);
                        val.startDateTimeDisplayValue = $filter('fourdotfiveTimeStampFormat')(val.startDateTime);
                    }else{
                        val.startDateTimeInBrowserTimezone = new Date(val.startDateTime);
                        val.startDateTimeInBrowserTimezoneDisplayValue = $filter('fourdotfiveTimeStampFormat')(val.startDateTimeInBrowserTimezone);
                        var startDateTimeStringInSelectedTimeZone = moment(val.startDateTime).tz(val.timeZone).format("YYYY-MM-DDTHH:mm:ss.SSS");
                        val.startDateTime = new Date(startDateTimeStringInSelectedTimeZone);
                        val.startDateTimeDisplayValue = $filter('fourdotfiveTimeStampFormat')(val.startDateTime);
                    }
                }else{
                    val.startDateTimeDisplayValue = '-';
                }
                if(!_.isNull(val.endDateTime)){
                    if(val.timeZone == '-'){
                        val.endDateTime = new Date(val.endDateTime);
                        val.endDateTimeDisplayValue = $filter('fourdotfiveTimeStampFormat')(val.endDateTime);
                    }else{
                        val.endDateTimeInBrowserTimezone = new Date(val.endDateTime);
                        val.endDateTimeInBrowserTimezoneDisplayValue = $filter('fourdotfiveTimeStampFormat')(val.endDateTimeInBrowserTimezone);
                        var endDateTimeStringInSelectedTimeZone = moment(val.endDateTime).tz(val.timeZone).format("YYYY-MM-DDTHH:mm:ss.SSS");
                        val.endDateTime = new Date(endDateTimeStringInSelectedTimeZone);
                        val.endDateTimeDisplayValue = $filter('fourdotfiveTimeStampFormat')(val.endDateTime);
                    }
                }else{
                    val.endDateTimeDisplayValue = '-';
                }
                if(_.isNull(val.numberOfRemindersSet)) {
                    val.numberOfRemindersSet = '-';
                }
                if(!_.isNull(val.mostRecentReminder)){
                    val.mostRecentReminder = new Date(val.mostRecentReminder);
                }else{
                    val.mostRecentReminder = '-';
                }
                if(_.isNull(val.numberOfReminderSent)) {
                    val.numberOfReminderSent = '-';
                }
            });
            vm.assessmentInfoDetails = data;
            $timeout(function () {
                $('#sentAssessmentsTable_info').hide();
            }, 100);

        }, function (error) {
            vm.loadingFlag = false;
            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
        });
    }
}

AssessmentInfoController.$inject = ['$timeout','$filter','dateTimeUtilityService','DTOptionsBuilder', 'assessmentService', 'alertsAndNotificationsService'];

fourdotfivedirectives.controller('AssessmentInfoController', AssessmentInfoController);