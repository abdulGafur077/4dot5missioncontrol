fourdotfivedirectives.directive('assignRecruiter', function assignRecruiter() {
    return {
        restrict: 'E',
        scope: {},
        bindToController: {
            recruiterId: '@',
            jobMatchIdArrayString: '@',
            saveCallback: '&'
        },
        controller: 'AssignRecruiterController',
        controllerAs: 'assignRecruiter',
        templateUrl: 'app/directives/assignRecruiter/assign-recruiter.html'
    }
});