/*
  * This is the controller for the assign recruiter directive
 */

function AssignRecruiterController($rootScope, jobMatchService, CandidateJobMatchCard, alertsAndNotificationsService) {
    var vm = this;

    //variables
    vm.jobMatchesDataArray = [];
    vm.jobMatchIdToRecruiterIdMap = {};
    vm.recruitersList = [];
    //flags
    vm.loadingRecruitersFlag = false;
    vm.savingFlag = false;
    //methods
    vm.init = init;
    vm.getRecruitersList = getRecruitersList;
    vm.getCandidateJobMatchCardInfo = getCandidateJobMatchCardInfo;
    vm.assignRecruiter = assignRecruiter;

    vm.init();

    function init() {
        vm.jobMatchIdArray = JSON.parse(vm.jobMatchIdArrayString);
        angular.forEach(vm.jobMatchIdArray, function (val, key) {
            var tempObject = {};
            tempObject.loadingFlag = true;
            tempObject.jobMatchId = val;
            tempObject.jobMatchData = {};
            vm.jobMatchesDataArray.push(tempObject);
        });
        angular.forEach(vm.jobMatchesDataArray, function (val, key) {
            vm.getCandidateJobMatchCardInfo(val.jobMatchId, function (data) {
                val.loadingFlag = false;
                val.jobMatchData = data;
            }, function () {
                val.loadingFlag = false;
            });
        });
        vm.loadingRecruitersFlag = true;
        vm.getRecruitersList(function () {
            vm.loadingRecruitersFlag = false;
        }, function () {
            vm.loadingRecruitersFlag = false;
        });
    }

    function assignRecruiter() {
        if(!vm.assignRecruiterForm.$invalid){
            vm.savingFlag = true;
            angular.forEach(vm.jobMatchesDataArray, function (val, key) {
                vm.jobMatchIdToRecruiterIdMap[val.jobMatchId] = vm.recruiterId;
            });
            jobMatchService.assignRecruiter(vm.jobMatchIdToRecruiterIdMap,function (data) {
                vm.savingFlag = false;
                if(!_.isUndefined(vm.saveCallback)){
                    vm.saveCallback();
                }
            },function (error) {
                if ($rootScope.isOnline) {
                    alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                }
            });
        }
    }

    function getRecruitersList(successCallback, errorCallback) {
        jobMatchService.getJobMatchRecruiters(vm.jobMatchIdArray, function (recruitersByJobMatchIdArray) {
            var tempRecruitersList = [];
            angular.forEach(recruitersByJobMatchIdArray, function (recruitersArray, jobMatchId) {
               angular.forEach(recruitersArray, function (val, key) {
                   tempRecruitersList.push(val);
               });
            });
            // remove duplicates if any
            tempRecruitersList = _.uniqBy(tempRecruitersList, 'id');
            // sort on name
            vm.recruitersList = _.sortBy(tempRecruitersList, 'name');
            if(!_.isUndefined(successCallback)){
                successCallback();
            }
        },function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
            if(!_.isUndefined(errorCallback)){
                errorCallback();
            }
        });
    }

    function getCandidateJobMatchCardInfo(jobMatchId, successCallback, errorCallback){
        jobMatchService.getCandidateJobMatchCard(jobMatchId, function (data) {
            data.name = data.firstName + ' ' + data.lastName;
            var candidateJobMatchInfo = angular.merge(new CandidateJobMatchCard(),data);
            if(successCallback){
                successCallback(candidateJobMatchInfo);
            }
        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
            if(errorCallback){
                errorCallback();
            }
        });
    }
}

AssignRecruiterController.$inject = ['$rootScope','jobMatchService','CandidateJobMatchCard','alertsAndNotificationsService'];

fourdotfivedirectives.controller('AssignRecruiterController', AssignRecruiterController);