fourdotfivedirectives.directive('associateCandidates', function associateCandidates() {
    return {
        restrict: 'E',
        scope: {},
        bindToController: {
            jobId: '@',
            saveCallback: '&'
        },
        controller: 'AssociateCandidatesController',
        controllerAs: 'associateCandidates',
        templateUrl: 'app/directives/associateCandidates/associate-candidates.html'
    }
});