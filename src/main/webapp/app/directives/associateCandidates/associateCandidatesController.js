/*
    This is the controller for Associate Candidates Directive
 */

function AssociateCandidatesController($rootScope, $scope, $timeout, jobService, candidateService, jobMatchService, genericService, modalService, alertsAndNotificationsService){
    var vm = this;

    //variables
    vm.jobDetails;
    vm.matchedCandidatesContainer = null;
    vm.selectedCandidates = [];
    vm.createJobMatchesObject = null;
    vm.minimumScoreSliderPadding = 0;
    vm.maximumScoreSliderPadding = 100;
    vm.lastModifiedOptions = [
        {text: 'Any Time', value: 0},
        {text: '1 day', value: 1},
        {text: '2 days', value: 2},
        {text: '3 days', value: 3},
        {text: '1 week', value: 7},
        {text: '2 weeks', value: 14},
        {text: '1 month', value: 30},
        {text: '3 months', value: 90},
        {text: '6 months', value: 180},
        {text: '1 year', value: 365}
    ];
    vm.topMatchesOptions = [
        {text: 'All', value: 0},
        {text: '5', value: 5},
        {text: '10', value: 10},
        {text: '25', value: 25},
        {text: '50', value: 50},
        {text: '75', value: 75},
        {text: '100', value: 100},
        {text: '200', value: 200}
    ];
    // flags
    vm.savingFlag = false;
    vm.selectAllCandidatesFlag = false;
    //methods
    vm.init = init;
    vm.getJobDetails = getJobDetails;
    vm.searchForMatchingCandidates = searchForMatchingCandidates;
    vm.getCandidateJobMatches = getCandidateJobMatches;
    vm.selectCandidate = selectCandidate;
    vm.removeCandidate = removeCandidate;
    vm.associateCandidates = associateCandidates;
    vm.searchTypeChanged = searchTypeChanged;
    vm.selectAllCandidates = selectAllCandidates;
    vm.deselectAllCandidates = deselectAllCandidates;
    vm.deselectAllSelectedCandidates = deselectAllSelectedCandidates;

    vm.init();

    function init(){
        vm.savingFlag = false;
        vm.getJobDetails();
        vm.matchedCandidatesContainer = new matchedCandidatesContainer();
        vm.createJobMatchesObject = {
          requisitionId: vm.jobId,
          candidateIds: []
        };
        _createMatchScoreSlider();
        _createSlimScroll();
    }

    function getJobDetails(){
        jobService.getJobProfile(vm.jobId, function(data){
            vm.jobDetails = data;
        }, function(error){
            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
        });
    }

    function selectCandidate(candidate) {
        candidate.selectedFlag = true;
        vm.selectedCandidates.push(candidate);
        $scope.$emit('candidateList:filtered');
    }

    function removeCandidate(candidate) {
        _.remove(vm.selectedCandidates, {
            id: candidate.id
        });
        var index = _.findIndex(vm.matchedCandidatesContainer.candidates, {
            id: candidate.id
        });
        if(index >= 0){
            vm.matchedCandidatesContainer.candidates[index].selectedFlag = false;
        }
        vm.matchedCandidatesContainer.selectAllDoneFlag = false;
    }

    function _createSlimScroll(){
        $timeout(function () {
            $('.associate-candidates-matched-candidates-div').slimScroll({
                start: 'top',
                alwaysVisible: false,
                railVisible: false,
                wheelStep: 20,
                allowPageScroll: true,
                height: '350px'
            });
            $('.associate-candidates-selected-candidates-div').slimScroll({
                start: 'top',
                alwaysVisible: false,
                railVisible: false,
                wheelStep: 20,
                allowPageScroll: true,
                height: '380px'
            });
        }, 100);
    }

    function _getBasicCandidateInfo(candidate){
        var basicCandidateInfo = {};
        basicCandidateInfo.name = candidate.contact.firstname + ' ' + candidate.contact.lastname;
        basicCandidateInfo.id = candidate.id;
        basicCandidateInfo.email = candidate.contact.email;
        basicCandidateInfo.mobilePhone = candidate.contact.mobilephone;
        basicCandidateInfo.workPhone = candidate.contact.workphone;
        basicCandidateInfo.matchScore = candidate.matchScore;
        basicCandidateInfo.lastModifiedDate = candidate.lastModifiedDate;
        if(!_.isNull(basicCandidateInfo.lastModifiedDate)){
            basicCandidateInfo.lastModifiedDate = new Date(basicCandidateInfo.lastModifiedDate);
        }
        basicCandidateInfo.candidateJobMatches = [];
        basicCandidateInfo.jobMatchesPopoverPosition = 'bottom';
        if(basicCandidateInfo.matchScore == 0){
            basicCandidateInfo.matchScore = 'N/A';
        }else{
            basicCandidateInfo.intelligenceScorePassFlag = false;
            if(basicCandidateInfo.matchScore >= vm.jobDetails.fourDotFiveIntelligenceScore){
                basicCandidateInfo.intelligenceScorePassFlag = true;
            }
            // round the match score to the lower integer value.
            basicCandidateInfo.matchScore = Math.floor(parseFloat(basicCandidateInfo.matchScore));

        }
        return basicCandidateInfo;
    }

    function getCandidateJobMatches(candidateId, successCallback) {
        var candidateJobMatches = [];
        candidateService.getCandidateJobMatches(candidateId, false, 1, 250, '', function (data) {
            angular.forEach(data, function (val, key) {
                val.jobMatchDate = new Date(val.jobMatchDate);
                candidateJobMatches.push(val);
            });
            if(successCallback){
                successCallback(candidateJobMatches)
            }
        }, function (error) {
            console.log('candidate job matches get error - ', error);
            // do nothing.
        });
    }

    function selectAllCandidates(){
        vm.matchedCandidatesContainer.selectAllDoneFlag = true;
        var tempSearchObject = angular.copy(vm.matchedCandidatesContainer.searchObject);
        if(vm.matchedCandidatesContainer.searchByTextFlag){
            delete tempSearchObject.minimumScore;
            delete tempSearchObject.maximumScore;
        }else{
            delete tempSearchObject.searchText;
        }
        tempSearchObject.page = 1;
        tempSearchObject.size = vm.matchedCandidatesContainer.totalCandidateCount;
        var selectingAllCandidatesDialog = bootbox.dialog({
            message: '<p class="text-center">Selecting ' + vm.matchedCandidatesContainer.totalCandidateCount + ' Candidates <i class="fa fa-spinner fa-spin"></i></p>',
            closeButton: false
        });
        jobService.findMatchingCandidatesForJob(tempSearchObject, function (data) {
            selectingAllCandidatesDialog.modal('hide');
            modalService.addModalOpenClassToBodyIfAnyModalIsOpen();
            angular.forEach(data.candidateMinimumInfoDtos, function (val, key) {
                var basicCandidateInfo = _getBasicCandidateInfo(val);
                var indexInSelectedCandidates = _.findIndex(vm.selectedCandidates, {
                    id: basicCandidateInfo.id
                });
                // if the candidate is not already in the selected candidates list, select the candidate
                if(indexInSelectedCandidates < 0){
                    vm.selectCandidate(basicCandidateInfo);
                }
                var indexInVisibleCandidates = _.findIndex(vm.matchedCandidatesContainer.candidates, {
                    id: basicCandidateInfo.id
                });
                // if the candidate is already in the visible candidates list, make the candidate selected
                if(indexInVisibleCandidates >= 0){
                    vm.matchedCandidatesContainer.candidates[indexInVisibleCandidates].selectedFlag = true;
                }
            });
        }, function(error){
            selectingAllCandidatesDialog.modal('hide');
            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
        });
    }


    function deselectAllCandidates() {
        vm.matchedCandidatesContainer.selectAllDoneFlag = false;
        var tempSearchObject = angular.copy(vm.matchedCandidatesContainer.searchObject);
        if(vm.matchedCandidatesContainer.searchByTextFlag){
            delete tempSearchObject.minimumScore;
            delete tempSearchObject.maximumScore;
        }else{
            delete tempSearchObject.searchText;
        }
        tempSearchObject.page = 1;
        tempSearchObject.size = vm.matchedCandidatesContainer.totalCandidateCount;
        var selectingAllCandidatesDialog = bootbox.dialog({
            message: '<p class="text-center">Deselecting ' + vm.matchedCandidatesContainer.totalCandidateCount + ' Candidates <i class="fa fa-spinner fa-spin"></i></p>',
            closeButton: false
        });
        jobService.findMatchingCandidatesForJob(tempSearchObject, function (data) {
            selectingAllCandidatesDialog.modal('hide');
            modalService.addModalOpenClassToBodyIfAnyModalIsOpen();
            angular.forEach(data.candidateMinimumInfoDtos, function (val, key) {
                var basicCandidateInfo = _getBasicCandidateInfo(val);
                var indexInSelectedCandidates = _.findIndex(vm.selectedCandidates, {
                    id: basicCandidateInfo.id
                });
                // if the candidate is in the selected candidates list, remove the candidate
                if(indexInSelectedCandidates >= 0){
                    vm.removeCandidate(basicCandidateInfo);
                }
            });
        }, function(error){
            selectingAllCandidatesDialog.modal('hide');
            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
        });
    }

    function deselectAllSelectedCandidates(){
        angular.forEach(vm.selectedCandidates, function(val, key){
            var index = _.findIndex(vm.matchedCandidatesContainer.candidates, {
                id: val.id
            });
            if(index >= 0){
                vm.matchedCandidatesContainer.candidates[index].selectedFlag = false;
            }
        });
        vm.matchedCandidatesContainer.selectAllDoneFlag = false;
        vm.selectedCandidates = [];
    }

    function searchTypeChanged(){
        // reset the 'number of days' filter to zero
        vm.matchedCandidatesContainer.searchObject.numberOfDays = 0;
        // maximum size reset to zero
        vm.matchedCandidatesContainer.maximumSize = 0;
        vm.searchForMatchingCandidates();
    }

    function _createMatchScoreSlider(){
        $('#matchScoreSlider').noUiSlider({
            range: [0.00,100.00],
            start: [0, 100],
            margin: 1.00,
            handles: 2,
            connect: true,
            step: 1,
            slide: function () {
                var val = $(this).val();
                vm.matchedCandidatesContainer.searchObject.minimumScore = parseInt(val[0]);
                vm.matchedCandidatesContainer.searchObject.maximumScore = parseInt(val[1]);
                vm.minimumScoreSliderPadding = vm.matchedCandidatesContainer.searchObject.minimumScore < 65 ? vm.matchedCandidatesContainer.searchObject.minimumScore : (vm.matchedCandidatesContainer.searchObject.minimumScore - 4);
                vm.maximumScoreSliderPadding = vm.matchedCandidatesContainer.searchObject.maximumScore < 65 ? vm.matchedCandidatesContainer.searchObject.maximumScore : (vm.matchedCandidatesContainer.searchObject.maximumScore - 4);
                $('#matchScoreSliderDisplayMinValue').addClass('job-match-score-slider-highlight-score');
                $('#matchScoreSliderDisplayMaxValue').addClass('job-match-score-slider-highlight-score');
                $scope.$apply();
            },
            set: function () {
                var val = $(this).val();
                vm.matchedCandidatesContainer.searchObject.minimumScore = parseInt(val[0]);
                vm.matchedCandidatesContainer.searchObject.maximumScore = parseInt(val[1]);
                $('#matchScoreSliderDisplayMinValue').removeClass('job-match-score-slider-highlight-score');
                $('#matchScoreSliderDisplayMaxValue').removeClass('job-match-score-slider-highlight-score');
                vm.searchForMatchingCandidates();
            }
        });
        $(".noUi-origin.noUi-origin-upper").css('background', '#eee');
    }
    
    function searchForMatchingCandidates() {
        // clear any existing query requests
        // check for an existing promise and see if it has not been fulfilled yet. If it has not been fulfilled yet, cancel the promise.
        if(!_.isNull(vm.matchedCandidatesContainer.currentPromise) && vm.matchedCandidatesContainer.currentPromise.$$state.status == 0) {
            genericService.cancelRequest(vm.matchedCandidatesContainer.currentPromise);
        }
        // reset the initial value of candidates exist flag to true
        vm.matchedCandidatesContainer.candidatesExistFlag = true;
        // reset the page number to 1 as this is a new query
        vm.matchedCandidatesContainer.searchObject.page = 1;
        // clear select all candidates selection
        vm.selectAllCandidatesFlag = false;
        // clear the existing candidates
        vm.matchedCandidatesContainer.candidates = [];
        vm.matchedCandidatesContainer.totalCandidateCount = null;
        vm.matchedCandidatesContainer.selectAllDoneFlag = false;
        vm.matchedCandidatesContainer.disableInfiniteScroll = false;
        // wait for the cancelled promise to be fulfilled.
        $timeout(function () {
            vm.matchedCandidatesContainer.getNext();
        }, 200);
    }

    function associateCandidates(){
        angular.forEach(vm.selectedCandidates, function (val, key) {
           vm.createJobMatchesObject.candidateIds.push(val.id);
        });
        vm.savingFlag = true;
        var associatingDialog = bootbox.dialog({
            message: '<p class="text-center">Associating Candidates <i class="fa fa-spinner fa-spin"></i></p>',
            closeButton: false
        });
        jobMatchService.createJobMatches(vm.createJobMatchesObject, function (data) {
            vm.savingFlag = false;
            associatingDialog.modal('hide');
            modalService.addModalOpenClassToBodyIfAnyModalIsOpen();
            bootbox.alert({
                closeButton: false,
                title: "<div class='alert alert-success' style='margin-bottom: 0px;'><i class='fa fa-check fa-fw fa-lg'></i><strong>Success!</strong></div>",
                message: "Candidate(s) associated successfully.",
                className: "zIndex1060",
                backdrop: true,
                onEscape: false,
                buttons: {
                    ok: {
                        label: 'Done',
                        className: 'btn-info'
                    }
                },
                callback: function(){
                    if(vm.saveCallback){
                        vm.saveCallback();
                    }
                }
            });
        }, function(error){
            vm.savingFlag = false;
            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
        });
    }

    function searchObject(jobId, companyId) {
        this.requisitionId = jobId;
        this.companyId = companyId;
        this.searchText = "";
        this.page = "";
        this.size = 9;
        this.minimumScore = 0;
        this.maximumScore = 100;
        this.numberOfDays = 0;
        //this.column = 'matchScore';
        //this.sortDirection = 'DSC';
    }

    function matchedCandidatesContainer(){
        this.candidates = [];
        this.busy = false;
        this.candidatesExistFlag = true;
        this.searchObject = new searchObject(vm.jobId, $rootScope.userDetails.company.companyId);
        this.currentPromise = null;
        this.disableInfiniteScroll = false;
        this.searchByTextFlag = true;
        this.totalCandidateCount = null;
        this.selectAllDoneFlag = false;
        this.maximumSize = 0;

        this.getNext = function () {
            var parent = this;
            if(this.busy) return;
            this.busy = true;

            if(this.searchObject.page == ''){
                this.searchObject.page = 1;
            }
            var tempSearchObject = angular.copy(this.searchObject);
            if(this.searchByTextFlag){
                delete tempSearchObject.minimumScore;
                delete tempSearchObject.maximumScore;
            }else{
                delete tempSearchObject.searchText;
            }
            if(tempSearchObject.numberOfDays == 0){
                delete tempSearchObject.numberOfDays;
            }
            // if top matches is chosen, then modify the page size to be that.
            if(parent.maximumSize != 0){
                tempSearchObject.size = parent.maximumSize;
            }
            this.currentPromise = jobService.findMatchingCandidatesForJob(tempSearchObject, function (data) {
                if(parent.searchObject.page == 1){
                    // if it is first page, reset the candidates/data array.
                    parent.candidates = [];
                }
                if(angular.isDefined(data) && data != 'user cancellation'){
                    parent.totalCandidateCount = data.count;
                    if(data.candidateMinimumInfoDtos.length > 0){
                        angular.forEach(data.candidateMinimumInfoDtos, function (val, key) {
                            var basicCandidateInfo = _getBasicCandidateInfo(val);
                            var currentIndex = parent.candidates.length;
                            if(((currentIndex+1) % 3) == 1){
                                basicCandidateInfo.jobMatchesPopoverPosition = 'bottom-left';
                            }else if(((currentIndex+1) % 3) == 0){
                                basicCandidateInfo.jobMatchesPopoverPosition = 'bottom-right';
                            }
                            vm.getCandidateJobMatches(basicCandidateInfo.id, function (data) {
                                basicCandidateInfo.candidateJobMatches = data;
                            });
                            var index = _.findIndex(vm.selectedCandidates, {
                                id: basicCandidateInfo.id
                            });
                            if(index >= 0){
                                basicCandidateInfo.selectedFlag = true;
                            }else{
                                basicCandidateInfo.selectedFlag = false;
                            }
                            parent.candidates.push(basicCandidateInfo);
                        });
                        // if the length of the returned values is less than page size, it means there are not enough values.
                        // hence we can disable the infinite scroll
                        if(data.candidateMinimumInfoDtos.length < parent.searchObject.size){
                            parent.disableInfiniteScroll = true;
                        }else{
                            parent.searchObject.page++;
                        }
                    }else{
                        // if the length of the returned values is 0, it means there are no more values.
                        // hence we can disable the infinite scroll
                        parent.disableInfiniteScroll = true;
                    }
                    // set the candidates exist flag based on existence of candidates.
                    if(parent.candidates.length > 0){
                        parent.candidatesExistFlag = true;
                    }else{
                        parent.candidatesExistFlag = false;
                    }
                }
                //set parent.busy to false
                parent.busy = false;
            }, function (error) {
                if ($rootScope.isOnline) {
                    alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                }
                parent.busy = false;
            });
        }
    }

}

AssociateCandidatesController.$inject = ['$rootScope','$scope','$timeout','jobService','candidateService','jobMatchService','genericService','modalService','alertsAndNotificationsService'];

fourdotfivedirectives.controller('AssociateCandidatesController', AssociateCandidatesController);