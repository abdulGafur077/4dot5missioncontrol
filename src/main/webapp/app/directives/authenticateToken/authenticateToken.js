fourdotfivedirectives.directive('authenticateToken', function authenticateToken() {
    return {
        restrict: 'E',
        scope: {},
        bindToController: {
            token: '@',
            successCallback: '&',
            errorCallback: '&'
        },
        controller: 'AuthenticateTokenController',
        controllerAs: 'authToken',
        templateUrl: 'app/directives/authenticateToken/authenticate-token.html'
    }
});