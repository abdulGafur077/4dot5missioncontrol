/*
  * This is the controller for the authenticate token directive
 */

function AuthenticateTokenController(authService, MESSAGECONSTANTS) {
    var vm = this;

    //variables
    vm.tokenObject = {};
    vm.MESSAGECONSTANTS = MESSAGECONSTANTS;
    //flags
    vm.loadingFlag = true;
    //methods
    vm.init = init;
    vm.authenticateToken = authenticateToken;

    vm.init();

    function init() {
        vm.tokenObject.token = vm.token;
        vm.tokenObject.email = '';
    }

    function authenticateToken() {
        if(!vm.authenticateForm.$invalid){
            authService.authenticateToken(vm.tokenObject, function (data) {
                if(vm.successCallback){
                    vm.successCallback({tokenResponse: data});
                }
            }, function (error) {
                if(vm.errorCallback){
                    vm.errorCallback({tokenResponse: error});
                }
            });
        }
    }

}

AuthenticateTokenController.$inject = ['authService','MESSAGECONSTANTS'];

fourdotfivedirectives.controller('AuthenticateTokenController', AuthenticateTokenController);