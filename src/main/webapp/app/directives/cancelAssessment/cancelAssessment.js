fourdotfivedirectives.directive('cancelAssessment', function cancelAssessment() {
    return {
        restrict: 'E',
        scope: {},
        bindToController: {
            jobMatchId: '@',
            assessmentType: '@',
            saveCallback: '&'
        },
        controller: 'CancelAssessmentController',
        controllerAs: 'cancelAssessment',
        templateUrl: 'app/directives/cancelAssessment/cancel-assessment.html'
    }
});