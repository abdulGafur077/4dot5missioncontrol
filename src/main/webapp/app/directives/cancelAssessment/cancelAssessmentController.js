/*
  * This is the controller for the cancel assessment directive
 */

function CancelAssessmentController($rootScope, jobMatchService, CandidateJobMatchCard, alertsAndNotificationsService) {
    var vm = this;

    //variables
    vm.assessmentData = {
        jobMatchId: null,
        notes: ''
    };
    vm.cancelAssessmentLabel = '';
    vm.candidateJobMatch = {};
    //flags
    vm.savingFlag = false;
    //methods
    vm.init = init;
    vm.cancelAssessment = cancelAssessment;
    vm.getCandidateJobMatchCardInfo = getCandidateJobMatchCardInfo;

    vm.init();

    function init() {
        vm.assessmentData.jobMatchId = vm.jobMatchId;
        switch(vm.assessmentType){
            case 'Technical':
                vm.cancelAssessmentLabel = 'Technical Assessment';
                break;
            case 'Value':
                vm.cancelAssessmentLabel = 'Value Assessment';
                break;
        }
        // vm.assessmentData.notes = 'Canceling ' + vm.cancelAssessmentLabel;
        vm.candidateJobMatch = new CandidateJobMatchCard();
        vm.getCandidateJobMatchCardInfo();
    }

    function cancelAssessment() {
        if(!vm.cancelAssessmentForm.$invalid){
            vm.savingFlag = true;
            jobMatchService.cancelAssessment(vm.assessmentType,vm.assessmentData,function (data) {
                vm.savingFlag = false;
                if(!_.isUndefined(vm.saveCallback)){
                    vm.saveCallback();
                }
            },function (error) {
                vm.savingFlag = false;
                if ($rootScope.isOnline) {
                    alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                }
            });
        }
    }

    function getCandidateJobMatchCardInfo(){
        jobMatchService.getCandidateJobMatchCard(vm.jobMatchId, function (data) {
            data.name = data.firstName + ' ' + data.lastName;
            angular.merge(vm.candidateJobMatch,data);
        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }
}

CancelAssessmentController.$inject = ['$rootScope','jobMatchService','CandidateJobMatchCard', 'alertsAndNotificationsService'];

fourdotfivedirectives.controller('CancelAssessmentController', CancelAssessmentController);