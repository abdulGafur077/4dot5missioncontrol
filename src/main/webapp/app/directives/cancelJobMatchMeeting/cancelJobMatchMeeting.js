fourdotfivedirectives.directive('cancelJobMatchMeeting', function cancelJobMatchMeeting() {
    return {
        restrict: 'E',
        scope: {},
        bindToController: {
            jobMatchObject: '=',
            saveCallback: '&',
            itemType: '@',
            jobMatchId: '@'
        },
        controller: 'CancelJobMatchMeetingController',
        controllerAs: 'cancelJobMatchMeeting',
        templateUrl: 'app/directives/cancelJobMatchMeeting/cancel-job-match-meeting.html'
    }
});