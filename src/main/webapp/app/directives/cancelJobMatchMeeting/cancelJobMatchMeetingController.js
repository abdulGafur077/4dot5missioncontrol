/*
  * This is the controller for the cancelling a job match meeting.
 */

function CancelJobMatchMeetingController($rootScope, jobMatchService, dateTimeUtilityService, alertsAndNotificationsService) {
    var vm = this;
    //variables
    vm.scheduleData = {
        isCompany: true,
        jobMatchId: '',
        note: '',
        emailAddress: '',
        firstName: '',
        lastName: '',
        role: '',
        department: '',
        company: '',
        startDateTime: '',
        endDateTime: ''
    };
    vm.itemDisplayLabel = '';
    vm.usersListLabel = '';
    vm.cancelLabel = '';
    vm.chosenMeeting = '';
    vm.clientorbulist = [];
    vm.meetingsList = [];
    vm.startDate = '';
    vm.startTime = '';
    vm.endDate = '';
    vm.endTime = '';
    //flags
    vm.savingFlag = false;
    vm.showFormFlag = false;
    vm.loadingFlag = false;
    vm.cancellableMeetingsExistFlag = false;
    //methods
    vm.init = init;
    vm.cancelMeeting = cancelMeeting;
    vm.getScheduledItems = getScheduledItems;
    vm.meetingChanged = meetingChanged;

    vm.init();

    function init() {
        vm.scheduleData.company = $rootScope.userDetails.company.companyId;
        switch(vm.itemType){
            case 'phoneScreen':
                vm.itemDisplayLabel = 'Phone Screen';
                vm.usersListLabel = 'Select Phone Screen';
                vm.cancelLabel = 'Cancel Phone Screen';
                vm.noCancellableMeetingsLabel = "No 'Scheduled' Phone Screens are available to cancel !";
                break;
            case 'interview':
                vm.itemDisplayLabel = 'Interview';
                vm.usersListLabel = 'Select Interview';
                vm.cancelLabel = 'Cancel Interview';
                vm.noCancellableMeetingsLabel = "No 'Scheduled' Interviews are available to cancel !";
                break;
        }
        _initializeDateAndTimePickers();
        vm.getScheduledItems();
    }

    function cancelMeeting() {
        if(!vm.cancelForm.$invalid){
            vm.savingFlag = true;
            var meetingObject = {
                note: vm.scheduleData.note
            };
            jobMatchService.cancelMeeting(vm.chosenMeeting, meetingObject, function (data) {
                vm.savingFlag = false;
                if(!_.isUndefined(vm.saveCallback)){
                    vm.saveCallback();
                }
            }, function (error) {
                vm.savingFlag = false;
                if ($rootScope.isOnline) {
                    alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                }
            });
        }
    }

    function _getMeetingsList(response){
        var meetingsList = [];
        angular.forEach(response, function (meetingDetails, key) {
            var startDateTime = new Date(meetingDetails.meetingStartTime);
            var endDateTime = new Date(meetingDetails.meetingEndTime);
            angular.forEach(meetingDetails.participants, function (meetingUser, k) {
                var meeting = {};
                meeting.id = meetingDetails.meetingScheduleId;
                meeting.userId = meetingUser.id;
                meeting.userName = meetingUser.firstName;
                if(!_.isUndefined(meetingUser.lastName) && !_.isNull(meetingUser.lastName)){
                    meeting.userName += ' ' + meetingUser.lastName;
                }
                meeting.status = meetingDetails.status;
                switch(meeting.status){
                    case 'SCHEDULED':
                        meeting.statusClass = 'fa fa-calendar meeting-icon-scheduled';
                        meeting.statusLabel = 'Scheduled';
                        meeting.editableFlag = true;
                        vm.cancellableMeetingsExistFlag = true;
                        break;
                    case 'IN_PROGRESS':
                        meeting.statusClass = 'fa fa-hourglass-half meeting-icon-in-progress';
                        meeting.statusLabel = 'In Progress';
                        meeting.editableFlag = false;
                        break;
                    case 'COMPLETED':
                        meeting.statusClass = 'fa fa-check meeting-icon-completed';
                        meeting.statusLabel = 'Completed';
                        meeting.editableFlag = false;
                        break;
                    case 'CANCELLED':
                        meeting.statusClass = 'fa fa-ban meeting-icon-cancelled';
                        meeting.statusLabel = 'Canceled';
                        meeting.editableFlag = false;
                        break;
                    default:
                        // set defaults
                        meeting.statusClass = 'fa fa-question meeting-icon-in-progress';
                        meeting.statusLabel = 'Status Unknown';
                        meeting.editableFlag = false;
                }
                meeting.scheduledTime = moment(startDateTime).format("MMM DD, YYYY ( hh:mm a") + ' to ' + moment(endDateTime).format("hh:mm a )");
                meeting.label = meeting.userName + ' - ' + meeting.scheduledTime;
                meetingsList.push(meeting);
            });
        });
        return meetingsList;
    }

    function getScheduledItems(){
        vm.loadingFlag = true;
        if(vm.itemType == 'phoneScreen'){
            jobMatchService.getPhoneScreeners(vm.jobMatchId, false, function (data) {
                vm.loadingFlag = false;
                vm.meetingsList = _getMeetingsList(data);
            }, function (error) {
                vm.loadingFlag = false;
                if ($rootScope.isOnline) {
                    alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                }
            });
        }else{
            jobMatchService.getInterviewers(vm.jobMatchId, false, function (data) {
                vm.loadingFlag = false;
                vm.meetingsList = _getMeetingsList(data);
            }, function (error) {
                vm.loadingFlag = false;
                if ($rootScope.isOnline) {
                    alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                }
            });
        }
    }

    function meetingChanged(){
        vm.showFormFlag = false;
        vm.loadingFlag = true;
        jobMatchService.getMeetingDetails(vm.chosenMeeting, function (data) {
            var participant = data.participants[0];
            vm.scheduleData.firstName = participant.firstName;
            vm.scheduleData.lastName = participant.lastName;
            vm.scheduleData.emailAddress = participant.email;
            vm.scheduleData.role = participant.role;
            vm.scheduleData.department = participant.department;
            vm.scheduleData.note = '';
            if(_.isNull(vm.scheduleData.department)){
                vm.scheduleData.department = '-';
            }else if(vm.scheduleData.department.toString().trim() == ''){
                vm.scheduleData.department = '-';
            }
            var startDateTime = new Date(data.startTime);
            var endDateTime = new Date(data.endTime);
            $("#jobMatchScheduleStartDate").datepicker("setDate", startDateTime);
            $("#jobMatchScheduleEndDate").datepicker("setDate", endDateTime);
            $('#jobMatchScheduleStartTime').timepicker("setTime", startDateTime);
            $('#jobMatchScheduleEndTime').timepicker("setTime", endDateTime);
            vm.loadingFlag = false;
            vm.showFormFlag = true;
        }, function(error){
            vm.loadingFlag = false;
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }

    function _initializeDateAndTimePickers(){
        $(document).ready(function () {
            var currentDate = new Date();
            $('#jobMatchScheduleStartDate').datepicker({
                format : "M d, yyyy",
                startDate: currentDate,
                weekStart: 1,
                autoclose: true,
                enableOnReadonly: false
            });

            $('#jobMatchScheduleEndDate').datepicker({
                format : "M d, yyyy",
                startDate: currentDate,
                weekStart: 1,
                autoclose: true,
                enableOnReadonly: false
            });

            $('#jobMatchScheduleStartTime').timepicker({
                minuteStep : 5,
                showSeconds : false,
                showMeridian : true,
                disableFocus : true,
                defaultTime: '11:00 AM'
            });

            $('#jobMatchScheduleEndTime').timepicker({
                minuteStep : 5,
                showSeconds : false,
                showMeridian : true,
                disableFocus : true,
                defaultTime: '11:30 AM'
            });
        });
    }

}

CancelJobMatchMeetingController.$inject = ['$rootScope','jobMatchService','dateTimeUtilityService', 'alertsAndNotificationsService'];

fourdotfivedirectives.controller('CancelJobMatchMeetingController', CancelJobMatchMeetingController);