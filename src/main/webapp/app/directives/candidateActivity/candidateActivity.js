fourdotfivedirectives.directive('candidateActivity', function candidateActivity() {
    return {
        restrict: 'E',
        scope: {},
        bindToController: {
            candidateId: '@',
            jobMatchId: '@',
            candidateName: '@',
            requisitionNumber: '@',
            jobTitle: '@',
            clientOrBuName: '@',
            context: '@',
            activityMap: '='
        },
        controller: 'CandidateActivityController',
        controllerAs: 'candidateActivity',
        templateUrl: 'app/directives/candidateActivity/candidate-activity.html'
    }
});