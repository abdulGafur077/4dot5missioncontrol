/*
  * This is the controller for the candidate activities
 */

function CandidateActivityController($rootScope, candidateService, jobService, companyService, CandidateActivityFilter, moment, dateTimeUtilityService, $q, $timeout, alertsAndNotificationsService, $filter) {
    var vm = this;

    //variables
    vm.startDate;
    vm.startTime;
    vm.endDate;
    vm.endTime;
    vm.activityTypes = [];
    vm.activities = [];
    vm.clientsOrBu = [];
    vm.requisitionIds = [];
    vm.requisitionIdsCopy = [];
    vm.uniqueRoles = [];
    vm.allRoles = [];
    vm.companyType = null;
    //flags
    vm.loadingFlag = true;
    vm.candidateActivityFilter = new CandidateActivityFilter();
    vm.candidateActivityFilterCopy = new CandidateActivityFilter();
    //methods
    vm.init = init;
    vm.getActivityTypes = getActivityTypes;
    vm.getCandidateActivity = getCandidateActivity;
    vm.getCandidateClientsOrBu = getCandidateClientsOrBu;
    vm.getCandidateJobs = getCandidateJobs;
    vm.initializeDatePickers = initializeDatePickers;
    vm.resetCandidateFilters = resetCandidateFilters;
    vm.setFilters = setFilters;
    vm.onRoleChange = onRoleChange;


    vm.init();

    function init() {

        if (_.isUndefined(vm.activityMap)) {
            vm.activityMap = [];
        }
        vm.companyType = $rootScope.userDetails.company.companyType;
        // vm.initializeDatePickers();
        vm.resetCandidateFilters();
        vm.getActivityTypes();
        vm.getCandidateClientsOrBu();
        vm.getCandidateJobs();
    }

    function initializeDatePickers() {
        $(document).ready(function () {
            var currentDate = new Date();
            var momentOfDefaultStartDate = moment(currentDate).add(-7, 'd');

            $('#startDate').datepicker({
                format: "M d, yyyy",
                endDate: currentDate,
                weekStart: 1,
                autoclose: true
            });
            $("#startDate").datepicker("setDate", momentOfDefaultStartDate.toDate());
            $("#startDate").datepicker("update", $filter('fourdotfiveDateFormat')(momentOfDefaultStartDate.toDate()));
            $("#startTime").timepicker("setTime", "09:00 AM");

            $('#endDate').datepicker({
                format: "M d, yyyy",
                endDate: currentDate,
                weekStart: 1,
                autoclose: true
            });

            $('#startTime').timepicker({
                minuteStep : 5,
                showSeconds : false,
                showMeridian : true,
                disableFocus : true,
                defaultTime: '09:00 AM'
            });

            $('#endTime').timepicker({
                minuteStep : 5,
                showSeconds : false,
                showMeridian : true,
                disableFocus : true,
                defaultTime: '12:00 PM'
            });

            $("#endDate").datepicker("setDate", currentDate);
            $('#endTime').timepicker("setTime", "12:00 PM");

            vm.candidateActivityFilter.startDate = dateTimeUtilityService.getDateTimeInAPIFormat(new Date(vm.startDate + ' ' + vm.startTime));
            vm.candidateActivityFilter.endDate = dateTimeUtilityService.getDateTimeInAPIFormat(new Date(vm.endDate + ' ' + vm.endTime));

            $('#startDate').datepicker().on('changeDate', function (e) {
                vm.candidateActivityFilter.startDate = dateTimeUtilityService.getDateTimeInAPIFormat(new Date(vm.startDate + ' ' + vm.startTime));
                vm.getCandidateActivity();
            });

            $('#endDate').datepicker().on('changeDate', function (e) {
                vm.candidateActivityFilter.endDate = dateTimeUtilityService.getDateTimeInAPIFormat(new Date(vm.endDate + ' ' + vm.endTime));
                vm.getCandidateActivity();
            });

            $('#startTime').timepicker().on('changeTime.timepicker', function (e) {
                vm.candidateActivityFilter.startDate = dateTimeUtilityService.getDateTimeInAPIFormat(new Date(vm.startDate + ' ' + vm.startTime));
                $timeout(function(){
                    vm.getCandidateActivity();
                }, 100);
            });

            $('#endTime').timepicker().on('changeTime.timepicker', function (e) {
                vm.candidateActivityFilter.endDate = dateTimeUtilityService.getDateTimeInAPIFormat(new Date(vm.endDate + ' ' + vm.endTime));
                $timeout(function(){
                    vm.getCandidateActivity();
                }, 100);
            });
        });
    }

    function getActivityTypes() {
        jobService.getActivityTypes(function (data) {
            angular.forEach(data, function (activityType) {
                if (activityType.value.startsWith("CANDIDATE_")) {
                    vm.activityTypes.push(activityType);
                }
            });
        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }

    function getCandidateActivity() {
        vm.setFilters();
        candidateService.getCandidateActivities(vm.candidateActivityFilterCopy, function (data) {
            vm.activities = [];
            angular.forEach(data, function (val, key) {
                val.activityDisplayValue = vm.activityMap[val.activityType];
                val.miniStatement = vm.activityMap[val.activityType];
                val.createdBy = val.createdBy.trim();
                val.statement = '';
                if (val.activityType.includes("CHANGED") || val.activityType.includes("STATE_CHANGE")) {
                    val.activityType = 'change';
                    val.statement = val.miniStatement + ' from ' + val.fromState + ' to ' + val.toState;
                    if (val.fromState.includes("Assessment") || val.toState.includes("Assessment")) {
                        val.activityDisplayValue = "Assessment Status Changed";
                    }
                    else if (val.fromState.includes("Phone Screen") || val.toState.includes("Phone Screen")) {
                        val.activityDisplayValue = "Phone Screen Status Changed";
                    } else if (val.fromState.includes("Interview") || val.toState.includes("Interview")) {
                        val.activityDisplayValue = "Interview Status Changed";
                    }
                }
                if (val.activityType.includes("STEP_CHANGE")) {
                    val.activityType = 'change';
                    val.activityDisplayValue = "Candidate Moved";
                }
                else if (val.activityType.includes("ADDED") || val.activityType.includes("REMINDER") || val.activityType.includes("COMMUNICATIONS")) {
                    val.activityType = 'add';
                }
                else if (val.activityType.includes("ASSIGNED")) {
                    val.activityType = 'assign';
                } else if (val.activityType.includes("REMOVED") || val.activityType.includes("DELETED")) {
                    val.activityType = 'remove';
                } else {
                }
                val.activityDisplayValue = val.activityDisplayValue.replace("Candidate Note", "Note");
                val.activityDisplayValue = val.activityDisplayValue.replace("Matched Candidate Recruiter", "Recruiter");
                val.activityDisplayValue = val.activityDisplayValue.replace("Matched Candidate State Changed", "Candidate State Changed");
                val.activityDisplayValue = val.activityDisplayValue.replace("Candidate Communications", "Communication");
                val.displayDate = new Date(val.date);
                if (!_.isNull(val.clientOrBuLocation)) {
                    val.clientOrBuLocationText = companyService.getParsedCompanyLocationText(val.clientOrBuLocation, true);
                }
                vm.activities.push(val);
            });
            vm.loadingFlag = false;
        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }

    function getCandidateClientsOrBu() {
        candidateService.getCandidateClientsOrBu(vm.candidateId, function (data) {
            angular.forEach(data, function (val, key) {
                vm.clientsOrBu.push({ id: key, val: val });
            });
        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }

    function getCandidateJobs() {
        candidateService.getCandidateJobs(vm.candidateId, function (data) {
            angular.forEach(data, function (val, key) {
                vm.requisitionIds.push({ id: key, requisitionNumber: val.requisitionNumber, role: val.role });
                vm.allRoles.push(val.role);
            });
            vm.requisitionIdsCopy = vm.requisitionIds;
                $.each(vm.allRoles, function(i, el){
                    if ($.inArray(el, vm.uniqueRoles) === -1){
                        vm.uniqueRoles.push(el);
                    }
                });

        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }

    function onRoleChange() {
        var currentRequisitions = vm.requisitionIds;
        vm.requisitionIds = vm.requisitionIdsCopy;
        if (vm.candidateActivityFilter.title !== "any") {
            vm.requisitionIds = vm.requisitionIds.filter(requisition => requisition.role === vm.candidateActivityFilter.title);
            var isRolePresent = false;
            angular.forEach(currentRequisitions, function (i, requisitionId) {
                if (requisitionId.role == vm.candidateActivityFilter.title)
                    isRolePresent = true;
            });
            if (!isRolePresent) {
                vm.candidateActivityFilter.requisitionId = "any";
            }
        }
        vm.getCandidateActivity();
    }

    function resetCandidateFilters() {
        vm.candidateActivityFilter.activityType = "any";
        vm.candidateActivityFilter.clientOrBuId = "any";
        vm.candidateActivityFilter.requisitionId = "any";
        vm.candidateActivityFilter.title = "any";
        vm.requisitionIds = vm.requisitionIdsCopy;
        vm.initializeDatePickers();
        vm.getCandidateActivity();
    }

    function setFilters() {
        vm.candidateActivityFilter.candidateId = vm.candidateId;
        vm.candidateActivityFilter.jobMatchId = vm.jobMatchId;
        vm.candidateActivityFilter.page = 1;
        vm.candidateActivityFilter.size = 250;
        vm.candidateActivityFilterCopy = angular.copy(vm.candidateActivityFilter);
        if (vm.candidateActivityFilter.activityType === "any") {
            vm.candidateActivityFilterCopy.activityType = null;
        }
        if (vm.candidateActivityFilter.clientOrBuId === "any") {
            vm.candidateActivityFilterCopy.clientOrBuId = null;
        }
        if (vm.candidateActivityFilter.title === "any") {
            vm.candidateActivityFilterCopy.title = null;
        }

        if (vm.candidateActivityFilter.requisitionId === "any") {
            vm.candidateActivityFilterCopy.requisitionId = null;
        }
        vm.candidateActivityFilterCopy.startDate = vm.candidateActivityFilter.startDate;
        vm.candidateActivityFilterCopy.endDate = vm.candidateActivityFilter.endDate;
    }

    $(document).ready(function () {
        $("#search-panel").hide();
        $(document).off("click", "#open-filter").on("click", "#open-filter",
            function () {
                $("#search-panel").animate({
                    height: 'toggle'
                });
            });
    });
}

CandidateActivityController.$inject = ['$rootScope', 'candidateService', 'jobService', 'companyService', 'CandidateActivityFilter', 'moment', 'dateTimeUtilityService','$q','$timeout', 'alertsAndNotificationsService','$filter'];

fourdotfivedirectives.controller('CandidateActivityController', CandidateActivityController);