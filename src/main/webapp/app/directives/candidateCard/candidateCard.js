fourdotfivedirectives.directive('candidateCard', function candidateCard() {
    return {
        restrict: 'E',
        scope: {
            'candidateInfo': '=',
            'filterParams': '=',
            'activityMap': '=',
            'userId': '@',
            'jobMatchesPopoverPosition': '@',
            'setCandidateInfo': '&'
        },
        bindToController: true,
        controller:'CandidateCardController',
        controllerAs: 'candidateCard',
        templateUrl: 'app/directives/candidateCard/candidate-card.html'
    }
});