/*
  * This is the controller for the candidate card directive
 */

function CandidateCardController($uibModal, $rootScope, candidateService, companyService, alertsAndNotificationsService, CandidateCard, CandidateCardQueryFilter) {
    var vm = this;

    //variables
    vm.candidate = null;
    vm.candidateJobMatches = [];
    vm.notifications = [];
    vm.newNotifications = [];
    vm.candidateCardNotesModal = null;
    vm.candidateCardCommunicationModal = null;
    vm.candidateCardShowAllActivitiesModal = null;
    vm.showCurrentJobDetails = false;
    vm.notificationsSearchObject = {};
    vm.userNotificationControls = {};
    vm.recentActivitiesControls = {};
    vm.numberOfNewNotifications = 0;
    vm.recentActivities = [];
    //flags
    vm.loadingFlag = true;
    //methods
    vm.init = init;
    vm.getCandidateCardInfo = getCandidateCardInfo;
    vm.getCandidateJobMatches = getCandidateJobMatches;
    vm.launchNotesModal = launchNotesModal;
    vm.launchCommunicationModal = launchCommunicationModal;
    vm.launchActionsModal = launchActionsModal;
    vm.showAllActivityModal = showAllActivityModal;

    vm.init();

    function init(){
        vm.candidate = new CandidateCard();
        if(_.isUndefined(vm.filterParams)){
            vm.filterParams = new CandidateCardQueryFilter();
        }if(_.isUndefined(vm.activityMap)){
            vm.activityMap = [];
        }
        if(_.isUndefined(vm.jobMatchesPopoverPosition)){
            vm.jobMatchesPopoverPosition = 'bottom';
        }
        if(_.isUndefined(vm.setCandidateInfo)){
            vm.setCandidateInfo = function () {
                // do nothing
            };
        }
        vm.numberOfNewNotifications = 0;
        vm.notificationsSearchObject.page = 1;
        vm.notificationsSearchObject.size = 3;
        vm.notificationsSearchObject.candidateId = vm.candidateInfo.id;
        vm.notificationsSearchObject.companyId = $rootScope.userDetails.company.companyId;
        vm.notificationsSearchObject.notifyOn = 'CANDIDATE_CARD';
        vm.userNotificationControls.setNewNotificationsAsViewed = null;
        vm.recentActivitiesControls.getActivities = null;
        //get the candidate card
        vm.getCandidateCardInfo();
    }

    function getCandidateCardInfo(successCallback){
        vm.loadingFlag = true;
        //var parentFilterParams = angular.copy(vm.filterParams);
        var filterParams = _.pick(vm.filterParams, Object.keys(new CandidateCardQueryFilter()));
        //console.log('candidate card filter params -',filterParams);
        candidateService.getCandidateCard(vm.candidateInfo.id, vm.userId, filterParams, function (data) {
            angular.merge(vm.candidate,data);
            vm.candidate = _massageCandidateData(vm.candidate);
            vm.getCandidateJobMatches(successCallback);
            //vm.jobMatchesPopover = _buildJobMatchesPopover(vm.candidate);
            vm.loadingFlag = false;
        }, function (error) {
            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
        });
    }

    function _massageCandidateData(candidate){
        candidate.name = candidate.firstName + ' ' + candidate.lastName;
        if(candidate.bestMatchJobRequisitionNum == null){
            candidate.bestMatchJobRequisitionNum = '-';
        }
        if(_.isNull(candidate.currentJobCompanyName) || candidate.currentJobCompanyName == ''){
            candidate.currentJobCompanyName = '-';
        }
        if(_.isNull(candidate.currentJobDuration) || candidate.currentJobDuration == ''){
            candidate.currentJobDuration = '-';
        }
        if(_.isNull(candidate.currentJobLocation) || candidate.currentJobLocation == ''){
            candidate.currentJobLocation = '-';
        }
        if(_.isNull(candidate.currentJobTitle) || candidate.currentJobTitle == ''){
            candidate.currentJobTitle = '-';
        }
        if(candidate.candidateStatus === 'INERT'){
            vm.showCurrentJobDetails = true;
            if(candidate.currentJobCompanyName == '-' && candidate.currentJobDuration == '-' && candidate.currentJobLocation == '-' && candidate.currentJobTitle == '-'){
                vm.showCurrentJobDetails = false;
            }
        }
        if(candidate.recruitersList != null && candidate.recruitersList.length){
            candidate.bestMatchRecruiterName = candidate.recruitersList[0];
        }else{
            candidate.bestMatchRecruiterName = '';
        }
        candidate.bestMatchJobDate = new Date(candidate.bestMatchJobDate);
        candidate.bestJobMatchClientLocationHtmlText = "";
        if (!_.isNull(candidate.bestJobMatchClientLocation)) {
            candidate.bestJobMatchClientLocationHtmlText = companyService.getParsedCompanyLocationText(candidate.bestJobMatchClientLocation, true);
        }
        return candidate;
    }

    

    function getCandidateJobMatches(successCallback) {
        candidateService.getCandidateJobMatches(vm.candidateInfo.id, true, '', '', '', function (data) {
            angular.forEach(data, function (val, key) {
                val.jobMatchDate = new Date(val.jobMatchDate);
                if(_.isNull(val.fourDotFiveIntelligentScore)){
                    val.fourDotFiveIntelligentScore = 'N/A';
                }
                if (!_.isNull(val.companyLocation)) {
                    val.companyDetailsText = val.company + companyService.getParsedCompanyLocationText(val.companyLocation, false);
                }
                vm.candidateJobMatches.push(val);
            });
            if(successCallback){
                successCallback();
            }
        }, function (error) {
            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
        });
    }

    function launchNotesModal(){
        vm.candidateCardNotesModal = $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'app/directives/candidateCard/candidate-card-notes-modal.html',
            controller: ['$uibModalInstance','jobMatchId',function ($uibModalInstance, jobMatchId) {
                var vm = this;
                vm.jobMatchId = jobMatchId;
                vm.closeModal = function () {
                    $uibModalInstance.close('cancel');
                };
            }],
            controllerAs: 'notesModal',
            size: 'md',
            resolve:{
                jobMatchId: function () {
                    return vm.candidate.bestMatchId;
                }
            }
        });
    }

    function launchCommunicationModal(){
        vm.candidateCardCommunicationModal = $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'app/directives/candidateCard/candidate-card-communication-modal.html',
            controller: ['$uibModalInstance','jobMatchId',function ($uibModalInstance, jobMatchId) {
                var vm = this;
                vm.jobMatchId = jobMatchId;
                vm.closeModal = function () {
                    $uibModalInstance.close('cancel');
                };
            }],
            controllerAs: 'communicationModal',
            size: 'md',
            resolve:{
                jobMatchId: function () {
                    return vm.candidate.bestMatchId;
                }
            }
        });
    }

    function launchActionsModal(type){
        var actionType = '';
        var actionLabel = '';
        switch(type){
            case 'availability':
                actionType = 'availability';
                actionLabel = 'Set Candidate Availability Date';
                break;
            case 'jobLeft':
                actionType = 'jobLeft';
                actionLabel = 'Set Candidate Job Left Date';
                break;
            case 'delete':
                actionType = 'delete';
                actionLabel = 'Delete Candidate';
                break;
            case 'updateContact':
                actionType = 'updateContact';
                actionLabel = 'Update Candidate Contact Details';
                break;
            default:
                break;
        }
        vm.candidateCardActionsModal = $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'app/directives/candidateCard/candidate-card-actions-modal.html',
            controller: ['$uibModalInstance','candidateId','candidateInfo','reloadCardCallback','actionType','actionLabel',function ($uibModalInstance, candidateId, candidateInfo, reloadCardCallback, actionType, actionLabel) {
                var vm = this;
                vm.candidateId = candidateId;
                vm.candidateInfo = candidateInfo;
                vm.actionType = actionType;
                vm.actionLabel = actionLabel;
                vm.reloadCardCallback = reloadCardCallback;
                vm.closeModal = function () {
                    $uibModalInstance.close('cancel');
                };
                vm.saveCallback = function(){
                    if(vm.actionType == 'updateContact'){
                        vm.reloadCardCallback();
                    }
                    vm.closeModal();
                };
            }],
            controllerAs: 'actionsModal',
            size: 'lg',
            resolve:{
                actionType: function(){
                    return actionType;
                },
                actionLabel: function(){
                    return actionLabel;
                },
                candidateId: function () {
                    return vm.candidateInfo.id;
                }, 
                candidateInfo: function () {
                    return {
                        candidateId: vm.candidateInfo.id,
                        candidateName: vm.candidate.name,
                        jobMatchCount: vm.candidate.jobMatchesCount,
                        emailId: vm.candidate.email,
                        phoneNumber: vm.candidate.mobilephone
                    };
                },
                reloadCardCallback: function(){
                    return vm.init;
                }
            }
        });
    }

    function showAllActivityModal() {
       vm.candidateCardShowAllActivitiesModal= $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'app/directives/candidateCard/candidate-activities-modal.html',
            windowClass: 'candidate-activitites-modal-window',
            controller: ['$uibModalInstance','candidateInfo','jobMatchId','activityMap',function ($uibModalInstance, candidateInfo, jobMatchId, activityMap) {
                var vm = this;
                vm.candidateInfo = candidateInfo;
                vm.jobMatchId = jobMatchId;
                vm.activityMap = activityMap;
                vm.closeModal = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }],
            controllerAs: 'activityModal',
            size: 'lg',
            resolve:{
                candidateInfo: function () {
                    return vm.candidate;
                },
                jobMatchId: function () {
                    return undefined;
                },
                activityMap: function () {
                    return vm.activityMap;
                }
            }
        });
    }
}

CandidateCardController.$inject = ['$uibModal', '$rootScope','candidateService', 'companyService', 'alertsAndNotificationsService', 'CandidateCard','CandidateCardQueryFilter'];

fourdotfivedirectives.controller('CandidateCardController', CandidateCardController);