fourdotfivedirectives.directive('candidateJobComparison', function candidateJobComparison() {
    return {
        restrict: 'E',
        scope: {},
        bindToController: {
            jobMatchId: '@'
        },
        controller: 'CandidateJobComparisonController',
        controllerAs: 'candidateJobComparison',
        templateUrl: 'app/directives/candidateJobComparison/candidate-job-comparison.html'
    }
});