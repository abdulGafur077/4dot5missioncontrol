/*
  * This is the controller for the candidate card directive
 */

function CandidateJobComparisonController(jobMatchService) {
    var vm = this;

    //variables
    vm.comparisonData = [];
    //flags
    vm.loadingFlag = true;
    //methods
    vm.init = init;
    vm.getCandidateJobComparison = getCandidateJobComparison;

    vm.init();

    function init() {
        //get the candidate job matches
        vm.getCandidateJobComparison();
    }

    function getCandidateJobComparison() {
        jobMatchService.getCandidateJobComparison(vm.jobMatchId, function (data) {
            vm.comparisonData = data;
            vm.jobLocationForDisplay = '';
            if(!_.isNull(vm.comparisonData.jobLocation.municipality)){
                vm.jobLocationForDisplay += vm.comparisonData.jobLocation.municipality;
            }
            if(!_.isNull(vm.comparisonData.jobLocation.city)){
                vm.jobLocationForDisplay += ( vm.jobLocationForDisplay === '' ? vm.comparisonData.jobLocation.city : (', ' + vm.comparisonData.jobLocation.city) );
            }
            if(!_.isNull(vm.comparisonData.jobLocation.region)){
                vm.jobLocationForDisplay += ( vm.jobLocationForDisplay === '' ? vm.comparisonData.jobLocation.region : (', ' + vm.comparisonData.jobLocation.region) );
            }
            if(!_.isNull(vm.comparisonData.jobLocation.state)){
                vm.jobLocationForDisplay += ( vm.jobLocationForDisplay === '' ? vm.comparisonData.jobLocation.state : (', ' + vm.comparisonData.jobLocation.state) );
            }
            vm.educationMatches = _.filter(vm.comparisonData.educationComparisons, ['status', 'MATCH']);
            vm.educationNonMatches = _.filter(vm.comparisonData.educationComparisons, ['status', 'NOT_MATCH']);
            vm.experienceMatches = _.filter(vm.comparisonData.experienceComparisons, ['status', 'MATCH']);
            vm.experienceNonMatches = _.filter(vm.comparisonData.experienceComparisons, ['status', 'NOT_MATCH']);
            vm.techSkillsMatches = _.filter(vm.comparisonData.technicalSkillComparisons, ['status', 'MATCH']);
            vm.techSkillsNonMatches = _.filter(vm.comparisonData.technicalSkillComparisons, ['status', 'NOT_MATCH']);
            vm.softSkillsMatches = _.filter(vm.comparisonData.softSkillComparisons, ['status', 'MATCH']);
            vm.softSkillsNonMatches = _.filter(vm.comparisonData.softSkillComparisons, ['status', 'NOT_MATCH']);
            vm.certificationMatches = _.filter(vm.comparisonData.certificationComparisons, ['status', 'MATCH']);
            vm.certificationNonMatches = _.filter(vm.comparisonData.certificationComparisons, ['status', 'NOT_MATCH']);
            vm.compensationMatches = _.filter(vm.comparisonData.compensationComparisons, ['status', 'MATCH']);
            vm.compensationNonMatches = _.filter(vm.comparisonData.compensationComparisons, ['status', 'NOT_MATCH']);
            vm.loadingFlag = false;
        }, function (error) {
            console.log('candidate job match get error - ', error);
            // do nothing.
        });
    }
}

CandidateJobComparisonController.$inject = ['jobMatchService'];

fourdotfivedirectives.controller('CandidateJobComparisonController', CandidateJobComparisonController);