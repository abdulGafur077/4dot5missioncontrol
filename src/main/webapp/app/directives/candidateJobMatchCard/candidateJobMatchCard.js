fourdotfivedirectives.directive('candidateJobMatchCard', function candidateJobMatchCard() {
    return {
        restrict: 'E',
        scope: {
            'candidateId': '@',
            'jobMatchId': '@',
            'saveCallback': '&',
            'selectedFlag': '=',
            'disabledFlag': '=',
            'activityMap': '='
        },
        bindToController: true,
        controller:'CandidateJobMatchCardController',
        controllerAs: 'candidateJobMatchCard',
        templateUrl: 'app/directives/candidateJobMatchCard/candidate-job-match-card.html'
    }
});