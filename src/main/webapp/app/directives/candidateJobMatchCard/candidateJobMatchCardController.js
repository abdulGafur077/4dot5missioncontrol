/*
  * This is the controller for the candidate job match card directive
 */

function CandidateJobMatchCardController($uibModal, $rootScope, $timeout, jobMatchService,candidateService, alertsAndNotificationsService, userNotificationsService, CandidateJobMatchCard, CandidateActivityFilter) {
    var vm = this;

    //variables
    vm.candidateJobMatch = null;
    vm.candidateJobMatches = [];
    vm.candidateCardNotesModal = null;
    vm.jobMatchIdModal = null;
    vm.candidateMeetingsModal = null;
    vm.setStatusModal = null;
    vm.candidateReportModal = null;
    vm.candidateAssessmentInfoModal = null;
    vm.candidateCardCommunicationModal = null;
    vm.candidateCardShowAllActivitiesModal = null;
    vm.notificationsSearchObject = {};
    vm.userNotificationControls = {};
    vm.recentActivitiesControls = {};
    vm.numberOfNewNotifications = 0;
    vm.recentActivities = [];
    vm.communicationActionObject = null;
    vm.notesActionObject = null;
    vm.assignRecruiterActionObject = null;
    vm.notInterestedActionObject = null;
    vm.releaseActionObject = null;
    vm.updateContactActionObject = {
        action: "Update Candidate Contact Details",
        actionType: "UPDATE_CONTACT",
        allowed: true,
        message: null
    };
    //flags
    vm.loadingFlag = true;
    vm.loadingMeetingsFlag = true;
    vm.showIntelligenceScoreFlag = false;
    vm.intelligenceScorePassFlag = false;
    vm.showTechAssessmentScoreFlag = false;
    vm.techAssessmentScorePassFlag = false;
    vm.showValueAssessmentScoreFlag = false;
    vm.showCommunicationFlag = false;
    vm.showNotesFlag = false;
    vm.showAssignRecruiterFlag = false;
    vm.showNotInterestedFlag = false;
    vm.showReleaseFlag = false;
    vm.showInterviewCountFlag = false;
    vm.showPhoneScreenCountFlag = false;
    vm.showAssessmentInfoFlag = false;
    // flag used to reload the notifications directive.
    vm.showNotificationsFlag = false;
    //methods
    vm.init = init;
    vm.refreshCard = refreshCard;
    vm.getCandidateJobMatchCardInfo = getCandidateJobMatchCardInfo;
    vm.performAction = performAction;
    vm.launchJobMatchIdModal = launchJobMatchIdModal;
    vm.cardMenuToggled = cardMenuToggled;
    //vm.launchCommunicationModal = launchCommunicationModal;
    vm.launchSetStatusModal = launchSetStatusModal;
    vm.getReport = getReport;
    vm.openMeetingsModal = openMeetingsModal;
    vm.openAssessmentInfoModal = openAssessmentInfoModal;
    vm.showAllActivityModal = showAllActivityModal;

    vm.init();

    function init(){
        vm.userId = $rootScope.userDetails.id;
        vm.candidateJobMatch = new CandidateJobMatchCard();
        vm.numberOfNewNotifications = 0;
        vm.notificationsSearchObject.page = 1;
        vm.notificationsSearchObject.size = 3;
        vm.notificationsSearchObject.jobMatchId = vm.jobMatchId;
        vm.notificationsSearchObject.companyId = $rootScope.userDetails.company.companyId;
        vm.notificationsSearchObject.notifyOn = 'CANDIDATE_JOB_CARD';
        vm.userNotificationControls.setNewNotificationsAsViewed = null;
        vm.recentActivitiesControls.getActivities = null;
        vm.showNotificationsFlag = true;
        if(_.isUndefined(vm.activityMap)){
            vm.activityMap = [];
        }
        // get the candidate card
        vm.getCandidateJobMatchCardInfo();
    }

    function refreshCard(){
        //re initialize flags
        vm.loadingFlag = true;
        vm.loadingMeetingsFlag = true;
        vm.showIntelligenceScoreFlag = false;
        vm.intelligenceScorePassFlag = false;
        vm.showTechAssessmentScoreFlag = false;
        vm.techAssessmentScorePassFlag = false;
        vm.showValueAssessmentScoreFlag = false;
        vm.showCommunicationFlag = false;
        vm.showNotesFlag = false;
        vm.showAssignRecruiterFlag = false;
        vm.showNotInterestedFlag = false;
        vm.showReleaseFlag = false;
        vm.communicationActionObject = null;
        vm.notesActionObject = null;
        vm.assignRecruiterActionObject = null;
        vm.notInterestedActionObject = null;
        vm.releaseActionObject = null;
        vm.showInterviewCountFlag = false;
        vm.showPhoneScreenCountFlag = false;
        vm.showAssessmentInfoFlag = false;
        vm.notifications = [];
        vm.showNotificationsFlag = false;
        // timeout so that the notifications module can be removed and added back
        $timeout(function(){
            // re initialize
            vm.init();
        }, 300);

    }

    function getReport(reportType, url) {
        // candidateService.getCandidateReport(url, function (data) {
        //     var pdfData = data;
        //     var blob = new Blob([pdfData], {type: 'application/pdf'});
        //     var objectURL = URL.createObjectURL(blob);
        vm.candidateReportModal=$uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'app/directives/candidateJobMatchCard/candidate-job-match-card-report-modal.html',
            backdrop: 'static',
            windowClass: 'candidate-job-match-card-assessment-report-modal-window',
            controller: ['$uibModalInstance','$sce','candidateService','url','reportType','candidateJobMatch', function ($uibModalInstance, $sce, candidateService, url,reportType, candidateJobMatch) {
                var vm = this;
                vm.candidateJobMatch = candidateJobMatch;
                vm.reportLoadingFlag = true;
                vm.modalTitle = 'Technical Assessment Report';
                if(reportType == 'ValueAssessment'){
                    vm.modalTitle = 'Value Assessment Report';
                }
                vm.closeModal = function () {
                    $uibModalInstance.dismiss('close');
                };
                candidateService.getCandidateReport(url, function (data) {
                    var pdfData = data;
                    var blob = new Blob([pdfData], {type: 'application/pdf'});
                    var objectURL = URL.createObjectURL(blob);
                    vm.content = $sce.trustAsResourceUrl(objectURL);
                    vm.reportLoadingFlag = false;
                }, function(error){
                    if ($rootScope.isOnline) {
                        alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                    }
                });
            }],
            controllerAs: 'reportModal',
            size: 'lg',
            resolve:{
                url: function(){
                    return url;
                },
                reportType: function(){
                    return reportType;
                },
                candidateJobMatch: function(){
                    return vm.candidateJobMatch;
                }
            }
        });
        // }, function(error){
        //     if ($rootScope.isOnline) {
        //         alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
        //     }
        // });
    }

    function getCandidateJobMatchCardInfo(successCallback, errorCallback){
        jobMatchService.getCandidateJobMatchCard(vm.jobMatchId, function (data) {
            angular.merge(vm.candidateJobMatch,data);
            vm.candidateJobMatch = _massageCandidateJobMatchData(vm.candidateJobMatch);
            _setActionFlags();
            // remove common actions which will show up as icons in the header
            _.remove(vm.candidateJobMatch.availableActions, {actionType: 'COMMUNICATION'});
            _.remove(vm.candidateJobMatch.availableActions, {actionType: 'NOTES'});
            _.remove(vm.candidateJobMatch.availableActions, {actionType: 'ASSIGN_RECRUITER'});
            _.remove(vm.candidateJobMatch.availableActions, {actionType: 'RELEASE'});
            _.remove(vm.candidateJobMatch.availableActions, {actionType: 'NOT_INTERESTED'});
            // add no actions available, if no explicit actions other than the common actions are available for a job match
            if(_.keys(vm.candidateJobMatch.availableActions).length == 0){
                vm.candidateJobMatch.availableActions.push({
                    action: "No Actions Available !",
                    actionType: "NO_ACTIONS_AVAILABLE",
                    allowed: true,
                    message: null
                });
            }
            vm.loadingFlag = false;
            if(!_.isNull(vm.candidateJobMatch.jobMatchScore) && vm.candidateJobMatch.jobMatchScore.toString() != ''){
                vm.showIntelligenceScoreFlag = true;
                if(vm.candidateJobMatch.jobMatchScore >= vm.candidateJobMatch.fourDotFiveIntelligenceScore){
                    vm.intelligenceScorePassFlag = true;
                }
                // round the job 4dot5 intelligence score to the lower integer value.
                vm.candidateJobMatch.jobMatchScore = Math.floor(parseFloat(vm.candidateJobMatch.jobMatchScore));
            }
            if(!_.isNull(vm.candidateJobMatch.techAssessmentScore) && vm.candidateJobMatch.techAssessmentScore.toString() != ''){
                vm.showTechAssessmentScoreFlag = true;
                if(vm.candidateJobMatch.techAssessmentScore >= vm.candidateJobMatch.techAssessmentThresholdScore){
                    vm.techAssessmentScorePassFlag = true;
                }
                // round the tech assessment score to the lower integer value.
                vm.candidateJobMatch.techAssessmentScore = Math.floor(parseFloat(vm.candidateJobMatch.techAssessmentScore));
            }
            if(!_.isNull(vm.candidateJobMatch.valueAssessmentScore) && vm.candidateJobMatch.valueAssessmentScore.toString() != ''){
                vm.showValueAssessmentScoreFlag = true;
            }
            if((vm.candidateJobMatch.jobMatchCurrentState == 'Phone Screen Scheduled' || vm.candidateJobMatch.jobMatchCurrentState == 'Phone Screen Completed') && vm.candidateJobMatch.numberOfOpenPhoneScreens > 0){
                vm.showPhoneScreenCountFlag = true;
            }
            if((vm.candidateJobMatch.jobMatchCurrentState == 'Interview Scheduled' || vm.candidateJobMatch.jobMatchCurrentState == 'Interview Completed') && vm.candidateJobMatch.numberOfOpenInterviews > 0){
                vm.showInterviewCountFlag = true;
            }
            // if it is an assessment related state and not one of 'In Tech Assessment' and 'In Value Assessment', then show assessment info
            if((vm.candidateJobMatch.jobMatchCurrentState.indexOf('Assessment') != -1) && (vm.candidateJobMatch.jobMatchCurrentState != 'In Tech Assessment') && (vm.candidateJobMatch.jobMatchCurrentState != 'In Value Assessment')){
                vm.showAssessmentInfoFlag = true;
            }
            if(successCallback){
                successCallback();
            }
        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
            if(errorCallback){
                errorCallback();
            }
        });
    }

    function _massageCandidateJobMatchData(candidateJobMatch){
        candidateJobMatch.name = candidateJobMatch.firstName + ' ' + candidateJobMatch.lastName;
        candidateJobMatch.jobMatchDate = new Date(candidateJobMatch.jobMatchDate);
        if(_.isNull(candidateJobMatch.jobMatchRequisitionNum)){
            candidateJobMatch.jobMatchRequisitionNum = '-';
        }
        candidateJobMatch.primaryRecruiter = {};
        candidateJobMatch.primaryRecruiterName = '';
        var tempPrimaryRecruiterIndex = -1;
        if(!_.isNull(candidateJobMatch.recruitersList) && candidateJobMatch.recruitersList.length){
            angular.forEach(candidateJobMatch.recruitersList, function (val, key) {
                val.name = val.firstName;
                if(!_.isNull(val.lastName)){
                    val.name += ' ' + val.lastName;
                }
                if(val.isPrimary){
                   candidateJobMatch.primaryRecruiter = val;
                   candidateJobMatch.primaryRecruiterName = val.name;
                   tempPrimaryRecruiterIndex = key;
                }
            });
        }
        if(tempPrimaryRecruiterIndex != -1){
            // remove the primary recruiter object from the recruiter list array
            candidateJobMatch.recruitersList.splice(tempPrimaryRecruiterIndex, 1);
        }
        if(_.isNull(candidateJobMatch.candidateStatus)){
            candidateJobMatch.candidateStatus = 'Active';
        }
        if(_.isNull(candidateJobMatch.jobMatchStatus)){
            candidateJobMatch.jobMatchStatus = 'ACTIVE';
        }
        candidateJobMatch.numberOfOpenOpenings = (candidateJobMatch.totalOpenings - candidateJobMatch.numberOfFilledOpenings);
        //replace Cancelled with Canceled, if it exits
        candidateJobMatch.jobMatchCurrentStateDisplayLabel = candidateJobMatch.jobMatchCurrentState.replace('Cancelled','Canceled');
        return candidateJobMatch;
    }

    function _setActionFlags(){
        var tempObject = _.find(vm.candidateJobMatch.availableActions, {actionType: 'COMMUNICATION'});
        vm.showCommunicationFlag = !_.isUndefined(tempObject);
        if(vm.showCommunicationFlag){
            vm.communicationActionObject = tempObject;
        }
        tempObject = _.find(vm.candidateJobMatch.availableActions, {actionType: 'NOTES'});
        vm.showNotesFlag = !_.isUndefined(tempObject);
        if(vm.showNotesFlag){
            vm.notesActionObject = tempObject;
        }
        tempObject = _.find(vm.candidateJobMatch.availableActions, {actionType: 'ASSIGN_RECRUITER'});
        vm.showAssignRecruiterFlag = !_.isUndefined(tempObject);
        if(vm.showAssignRecruiterFlag){
            vm.assignRecruiterActionObject = tempObject;
        }
        tempObject = _.find(vm.candidateJobMatch.availableActions, {actionType: 'NOT_INTERESTED'});
        vm.showNotInterestedFlag = !_.isUndefined(tempObject);
        if(vm.showNotInterestedFlag){
            vm.notInterestedActionObject = tempObject;
        }
        tempObject = _.find(vm.candidateJobMatch.availableActions, {actionType: 'RELEASE'});
        vm.showReleaseFlag = !_.isUndefined(tempObject);
        if(vm.showReleaseFlag){
            vm.releaseActionObject = tempObject;
        }
    }

    function performAction(actionObject) {
        if(!actionObject.allowed){
            var message = _.isNull(actionObject.message) ? 'This Action is not allowed.': actionObject.message;
            bootbox.alert({
                closeButton: false,
                title: "<div class='alert alert-warning' style='margin-bottom: 0px;'><i class='fa fa-warning fa-fw fa-lg'></i><strong>Action Not Allowed!</strong></div>",
                message: message,
                className: "zIndex1060",
                buttons: {
                    ok: {
                        label: 'Ok',
                        className: 'btn-info'
                    }
                }
            });
        }else{
            var actionType = actionObject.actionType;
            var label = actionObject.action;
            switch(actionType){
                case 'NOT_INTERESTED':
                case 'RELEASE':
                    vm.launchSetStatusModal(label);
                    break;
                case 'NOTES':
                case 'COMMUNICATION':
                case 'START_SCREENING':
                case 'PHONE_SCREEN_FEEDBACK':
                case 'INTERVIEW_FEEDBACK':
                case 'PHONE_SCREEN_FEEDBACK_FROM_MEETING_POPUP':
                case 'INTERVIEW_FEEDBACK_FROM_MEETING_POPUP':
                case 'MOVE_BACKWARD':
                case 'MOVE_FORWARD':
                case 'ASSIGN_RECRUITER':
                case 'SEND_TECHNICAL_ASSESSMENT':
                case 'SEND_VALUE_ASSESSMENT':
                case 'RE_SEND_TECHNICAL_ASSESSMENT':
                case 'RE_SEND_VALUE_ASSESSMENT':
                case 'CANCEL_TECHNICAL_ASSESSMENT':
                case 'CANCEL_VALUE_ASSESSMENT':
                case 'SCHEDULE_PHONE_SCREEN':
                case 'RESHEDULE_PHONE_SCREEN':
                case 'SCHEDULE_INTERVIEW':
                case 'RESHEDULE_INTERVIEW':
                case 'CANCEL_PHONE_SCREEN':
                case 'CANCEL_INTERVIEW':
                case 'UPDATE_CONTACT':
                    vm.launchJobMatchIdModal(actionType, label);
                    break;
                default:
                //do nothing
            }
        }
    }

    function launchSetStatusModal(modalType) {
        var templateUrl = '';
        var statusType = '';
        switch(modalType){
            case 'Not Interested':
                templateUrl = 'app/directives/candidateJobMatchCard/candidate-job-match-card-not-interested-modal.html';
                statusType = 'Not Interested';
                break;
            case 'Release':
                templateUrl = 'app/directives/candidateJobMatchCard/candidate-job-match-card-released-modal.html';
                statusType = 'Release';
                break;
        }

        vm.setStatusModal = $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: templateUrl,
            backdrop: 'static',
            keyboard: false,
            controller: ['$uibModalInstance','jobMatchDetailsArray','statusType','saveCallback',function ($uibModalInstance, jobMatchDetailsArray, statusType, saveCallback) {
                var vm = this;
                vm.jobMatchDetailsArray = jobMatchDetailsArray;
                vm.statusType = statusType;
                vm.saveCallback = function(){
                    $uibModalInstance.close('cancel');
                    saveCallback();
                };
                vm.closeModal = function () {
                    $uibModalInstance.close('cancel');
                };
            }],
            controllerAs: 'setStatusModal',
            size: 'lg',
            resolve:{
                jobMatchDetailsArray: function () {
                    var tempArray = [];
                    var tempObject = {};
                    tempObject.jobMatchId = vm.jobMatchId;
                    tempObject.candidateId = vm.candidateId;
                    tempArray.push(tempObject);
                    return tempArray;
                },
                statusType: function () {
                    return statusType;
                },
                saveCallback: function () {
                    return vm.saveCallback;
                }
            }
        }); 
    }

    function openMeetingsModal(){
        vm.candidateMeetingsModal = $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'app/directives/candidateJobMatchCard/candidate-job-match-card-meetings-modal.html',
            backdrop: 'static',
            keyboard: false,
            bindToController: true,
            controller: ['$uibModalInstance','jobMatchId','meetingType','saveCallback', function ($uibModalInstance, jobMatchId, meetingType, saveCallback) {
                var vm = this;
                vm.jobMatchId = jobMatchId;
                vm.meetingType = meetingType;
                vm.title = 'Interviews';
                if(vm.meetingType == 'PhoneScreen'){
                    vm.title = 'Phone Screens';
                }
                vm.closeModal = function () {
                    $uibModalInstance.close('cancel');
                };
                vm.saveCallback = function(){
                    saveCallback();
                };
            }],
            controllerAs: 'jobMatchMeetingsModal',
            size: 'lg',
            resolve:{
                jobMatchId: function () {
                    return vm.jobMatchId;
                },
                saveCallback: function () {
                    return vm.refreshCard;
                },
                meetingType: function(){
                    if(vm.showInterviewCountFlag){
                        return 'Interview';
                    }else if(vm.showPhoneScreenCountFlag){
                        return 'PhoneScreen';
                    }
                }
            }
        }); 
    }

    function openAssessmentInfoModal(){
        vm.candidateAssessmentInfoModal = $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'app/directives/candidateJobMatchCard/candidate-job-match-card-assessment-info-modal.html',
            backdrop: 'static',
            keyboard: false,
            bindToController: true,
            controller: ['$uibModalInstance','jobMatchId','assessmentType', function ($uibModalInstance, jobMatchId, assessmentType) {
                var vm = this;
                vm.jobMatchId = jobMatchId;
                vm.assessmentType = assessmentType;
                vm.title = 'Technical Assessment Info';
                if(vm.assessmentType == 'Value Assessment'){
                    vm.title = 'Value Assessment Info';
                }
                vm.closeModal = function () {
                    $uibModalInstance.close('cancel');
                };
            }],
            controllerAs: 'assessmentInfoModal',
            size: 'lg',
            resolve:{
                jobMatchId: function () {
                    return vm.jobMatchId;
                },
                assessmentType: function(){
                    if(vm.candidateJobMatch.jobMatchCurrentState.indexOf('Tech Assessment') != -1){
                        return 'Technical Assessment';
                    }else if(vm.candidateJobMatch.jobMatchCurrentState.indexOf('Value Assessment') != -1){
                        return 'Value Assessment';
                    }else{
                        return 'Technical Assessment';
                    }
                }
            }
        }); 
    }

    function launchJobMatchIdModal(modalType, label){
        var templateUrl = '';
        var callbackType = '';
        switch(modalType){
            case 'NOTES':
                templateUrl = 'app/directives/candidateJobMatchCard/candidate-job-match-card-notes-modal.html';
                callbackType = 'refreshCard';
                break;
            case 'COMMUNICATION':
                templateUrl = 'app/directives/candidateJobMatchCard/candidate-job-match-card-communication-modal.html';
                callbackType = 'refreshCard';
                break;
            case 'ASSIGN_RECRUITER':
                templateUrl = 'app/directives/candidateJobMatchCard/candidate-job-match-card-assign-recruiter-modal.html';
                callbackType = 'refreshCard';
                break;
            case 'START_SCREENING':
                templateUrl = 'app/directives/candidateJobMatchCard/candidate-job-match-card-screening-modal.html';
                // refresh workflow because the user can mark the job as not interested and then the card has to move to not interested column
                callbackType = 'refreshWorkflow';
                break;
            case 'PHONE_SCREEN_FEEDBACK':
            case 'INTERVIEW_FEEDBACK':
            case 'PHONE_SCREEN_FEEDBACK_FROM_MEETING_POPUP':
            case 'INTERVIEW_FEEDBACK_FROM_MEETING_POPUP':
                templateUrl = 'app/directives/candidateJobMatchCard/candidate-job-match-card-screening-modal.html';
                callbackType = 'refreshCard';
                break;
            case 'MOVE_FORWARD':
            case 'MOVE_BACKWARD':
                templateUrl = 'app/directives/candidateJobMatchCard/candidate-job-match-card-move-modal.html';
                callbackType = 'refreshWorkflow';
                break;
            case 'SEND_TECHNICAL_ASSESSMENT':
            case 'SEND_VALUE_ASSESSMENT':
            case 'RE_SEND_TECHNICAL_ASSESSMENT':
            case 'RE_SEND_VALUE_ASSESSMENT':
                templateUrl = 'app/directives/candidateJobMatchCard/candidate-job-match-card-send-assessment-modal.html';
                callbackType = 'refreshCard';
                break;
            case 'CANCEL_TECHNICAL_ASSESSMENT':
            case 'CANCEL_VALUE_ASSESSMENT':
                templateUrl = 'app/directives/candidateJobMatchCard/candidate-job-match-card-cancel-assessment-modal.html';
                callbackType = 'refreshCard';
                break;
            case 'SCHEDULE_PHONE_SCREEN':
            case 'RESHEDULE_PHONE_SCREEN':
            case 'SCHEDULE_INTERVIEW':
            case 'RESHEDULE_INTERVIEW':
                templateUrl = 'app/directives/candidateJobMatchCard/candidate-job-match-card-schedule-modal.html';
                callbackType = 'refreshCard';
                break;
            case 'CANCEL_PHONE_SCREEN':
            case 'CANCEL_INTERVIEW':
                templateUrl = 'app/directives/candidateJobMatchCard/candidate-job-match-card-cancel-meeting-modal.html';
                callbackType = 'refreshCard';
                break;
            case 'UPDATE_CONTACT':
                templateUrl = 'app/directives/candidateJobMatchCard/candidate-job-match-card-update-contact-modal.html';
                callbackType = 'refreshCard';
                break;
        }
        vm.jobMatchIdModal = $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: templateUrl,
            backdrop: 'static',
            keyboard: false,
            controller: ['$uibModalInstance','jobMatchId','candidateId','modalType','label','refreshCallback','primaryRecruiter','candidateJobMatch', function ($uibModalInstance, jobMatchId, candidateId, modalType, label, refreshCallback, primaryRecruiter, candidateJobMatch) {
                var vm = this;
                vm.jobMatchId = jobMatchId;
                vm.jobMatchIdArray = [];
                vm.jobMatchIdArray.push(vm.jobMatchId);
                vm.candidateId = candidateId;
                vm.recruiterId = !_.isUndefined(primaryRecruiter.id) ? primaryRecruiter.id : undefined;
                vm.modalType = modalType;
                vm.candidateJobMatch = candidateJobMatch;
                vm.label = label;
                vm.moveType = 'Forward';
                if(vm.modalType == 'MOVE_BACKWARD'){
                    vm.moveType = 'Backward';
                }
                vm.assessmentType = 'Technical';
                vm.assessmentSendMode = 'send';
                if(vm.modalType == 'SEND_VALUE_ASSESSMENT' || vm.modalType == 'CANCEL_VALUE_ASSESSMENT' || vm.modalType == 'RE_SEND_VALUE_ASSESSMENT'){
                    vm.assessmentType = 'Value';
                }
                if(vm.modalType == 'RE_SEND_TECHNICAL_ASSESSMENT' || vm.modalType == 'RE_SEND_VALUE_ASSESSMENT'){
                    vm.assessmentSendMode = 'resend';
                }
                vm.scheduleType = 'phoneScreen';
                if(vm.modalType == 'SCHEDULE_INTERVIEW' || vm.modalType == 'RESHEDULE_INTERVIEW' || vm.modalType == 'CANCEL_INTERVIEW'){
                    vm.scheduleType = 'interview';
                }
                vm.scheduleMode = 'add';
                if(vm.modalType == 'RESHEDULE_PHONE_SCREEN' || vm.modalType == 'RESHEDULE_INTERVIEW'){
                    vm.scheduleMode = 'edit';
                }
                vm.feedbackType = 'Screening';
                if(vm.modalType == 'INTERVIEW_FEEDBACK'){
                    vm.feedbackType = 'Interview'
                }else if(vm.modalType == 'PHONE_SCREEN_FEEDBACK'){
                    vm.feedbackType = 'PhoneScreen'
                }else{
                    // do nothing
                }
                vm.closeModal = function () {
                    $uibModalInstance.close('cancel');
                };
                vm.saveCallback = function(){
                    $uibModalInstance.close('cancel');
                    refreshCallback();
                };
                vm.refreshCallback = function(){
                    refreshCallback();
                }
            }],
            controllerAs: 'jobMatchIdModal',
            size: 'lg',
            resolve:{
                jobMatchId: function () {
                    return vm.jobMatchId;
                },
                modalType: function () {
                    return modalType;
                },
                label: function () {
                    return label;
                },
                refreshCallback: function () {
                    if(callbackType == 'refreshCard'){
                        return vm.refreshCard;
                    }else{
                        return vm.saveCallback;
                    }
                },
                candidateId: function(){
                    return vm.candidateId;
                },
                candidateJobMatch: function () {
                    return vm.candidateJobMatch;
                },
                primaryRecruiter: function(){
                    return vm.candidateJobMatch.primaryRecruiter;
                }
            }
        });
    }

    function cardMenuToggled(openFlag, id) {
        if(openFlag){
            $timeout(function(){
                scrollIntoView(document.getElementById(id), {behavior: "smooth", block: "end", scrollMode: "if-needed"});
            }, 200);
        }
    }

    function showAllActivityModal() {
        console.log('test');
       vm.candidateCardShowAllActivitiesModal= $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'app/directives/candidateCard/candidate-activities-modal.html',
            windowClass: 'candidate-activitites-modal-window',
            controller: ['$uibModalInstance','candidateInfo','jobMatchId','activityMap',function ($uibModalInstance, candidateInfo, jobMatchId, activityMap) {
                var vm = this;
                vm.candidateInfo = candidateInfo;
                vm.jobMatchId = jobMatchId;
                vm.activityMap = activityMap;
                vm.closeModal = function () {
                    $uibModalInstance.close('cancel');
                };
            }],
            controllerAs: 'activityModal',
            size: 'lg',
            resolve:{
                candidateInfo: function () {
                    return vm.candidateJobMatch;
                },
                jobMatchId: function () {
                    return vm.jobMatchId;
                },
                activityMap: function () {
                    return vm.activityMap;
                }
            }
        });
    }
}

CandidateJobMatchCardController.$inject = ['$uibModal', '$rootScope', '$timeout', 'jobMatchService', 'candidateService', 'alertsAndNotificationsService', 'userNotificationsService', 'CandidateJobMatchCard', 'CandidateActivityFilter'];

fourdotfivedirectives.controller('CandidateJobMatchCardController', CandidateJobMatchCardController);