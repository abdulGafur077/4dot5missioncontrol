fourdotfivedirectives.directive('candidateJobMatches', function candidateJobMatches() {
    return {
        restrict: 'E',
        scope: {},
        bindToController: {
            candidateId: '=',
            userId: '='
        },
        controller: 'CandidateJobMatchesController',
        controllerAs: 'candidateJobMatches',
        templateUrl: 'app/directives/candidateJobMatches/candidate-job-matches.html'
    }
});