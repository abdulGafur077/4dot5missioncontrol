/*
  * This is the controller for the candidate job matches directive
 */

function CandidateJobMatchesController(candidateService, companyService, CandidateJobMatch, alertsAndNotificationsService, DTOptionsBuilder) {
    var vm = this;

    //variables
    vm.jobMatches = [];
    //flags
    vm.loadingFlag = true;
    //methods
    vm.init = init;
    vm.getCandidateJobMatches = getCandidateJobMatches;
    vm.dtOptions = DTOptionsBuilder.newOptions()
        .withPaginationType('full_numbers')
        .withOption('order', [0, 'desc']);

    vm.init();

    function init() {
        //get the candidate job matches
        vm.getCandidateJobMatches();
    }

    function getCandidateJobMatches() {
        candidateService.getCandidateJobMatches(vm.candidateId, false, 1, 250, '', function (data) {
            angular.forEach(data, function (val, key) {
                val.jobMatchDate = new Date(val.jobMatchDate);
                if(_.isNull(val.fourDotFiveIntelligentScore)){
                    val.fourDotFiveIntelligentScore = 'N.A';
                }
                if(_.isNull(val.techAssessmentScore)){
                    val.techAssessmentScore = '-';
                }
                if (!_.isNull(val.companyLocation)) {
                    val.companyDetailsText = companyService.getParsedCompanyLocationText(val.companyLocation, false);
                }
                vm.jobMatches.push(val);
            });
            //vm.loadingFlag = false;
        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }
}

CandidateJobMatchesController.$inject = ['candidateService', 'companyService', 'CandidateJobMatch', 'alertsAndNotificationsService', 'DTOptionsBuilder'];

fourdotfivedirectives.controller('CandidateJobMatchesController', CandidateJobMatchesController);