fourdotfivedirectives.directive('candidateProfile', function candidateProfile() {
    return {
        restrict: 'E',
        scope: {
            'candidateId': '@'
        },
        bindToController: true,
        controller:'CandidateProfileController',
        controllerAs: 'candidateProfile',
        templateUrl: 'app/directives/candidateProfile/candidate-profile.html'
    }
});