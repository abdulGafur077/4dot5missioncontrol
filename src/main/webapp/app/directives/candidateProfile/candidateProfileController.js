/*
  * This is the controller for the candidate profile directive
 */

function CandidateProfileController($uibModal, candidateService, CandidateProfile) {
    var vm = this;

    //variables
    vm.candidate = null;
    vm.phoneInConflictToolTipMessage = "Phone Number in conflict.<br/> Update contact details.";
    vm.phoneMissingToolTipMessage = "Phone Number missing.<br/> Update contact details.";
    vm.emailInConflictToolTipMessage = "Email in conflict.<br/> Update contact details.";
    vm.emailMissingToolTipMessage = "Email missing.<br/> Update contact details.";
    //flags
    vm.loadingFlag = true;
    vm.showProfileUrlFlag = false;
    //methods
    vm.init = init;
    vm.getCandidateProfile = getCandidateProfile;
    vm.launchActionsModal = launchActionsModal;

    function init(){
        vm.candidate = new CandidateProfile();
        //get the candidate profile
        vm.getCandidateProfile();
    }

    function getCandidateProfile(){
        vm.loadingFlag = true;
        candidateService.getCandidateProfile(vm.candidateId, function (data) {
            //angular.merge(vm.candidate,data);
            _.mergeWith(vm.candidate, data, function (objValue, srcValue) {
                return _.isNull(srcValue) ? objValue : undefined;
            });
            vm.candidate = _massageCandidateData(vm.candidate);
            vm.candidate.profileUrlText = '';
            if(!_.isNull(vm.candidate.profileUrl)){
                vm.showProfileUrlFlag = true;
                if(vm.candidate.profileType != 'Other'){
                    vm.candidate.profileUrlText = "View Candidate's " + vm.candidate.profileType + " Profile";
                }else{
                    vm.candidate.profileUrlText = "View Candidate's Profile";
                }
            }
            vm.loadingFlag = false;
        }, function (error) {
            console.log('candidate get error - ', error);
            // do nothing.
        });
    }

    function _massageCandidateData(candidate){
        candidate.name = candidate.firstName + ' ' + candidate.lastName;
        if(_.isNull(candidate.title)){
            candidate.title = '-';
        }
        if(_.isNull(candidate.email)){
            candidate.email = '-';
        }
        if(_.isNull(candidate.mobilePhone)){
            candidate.mobilePhone = '-';
        }
        candidate.displayAddress1 = '';
        candidate.displayAddress2 = '';
        if(_.isNull(candidate.address)){
            candidate.displayAddress1 = '-';
            candidate.displayAddress2 = '-';
        }else{
            candidate.displayAddress1 += _.isNull(candidate.address.address1) ? '' : candidate.address.address1;
            candidate.displayAddress1 += _.isNull(candidate.address.address2) ? '' : ', ' + candidate.address.address2;
            candidate.displayAddress2 += _.isNull(candidate.address.city) ? '' : candidate.address.city;
            candidate.displayAddress2 += _.isNull(candidate.address.county) ? '' : ', ' + candidate.address.county;
            candidate.displayAddress2 += _.isNull(candidate.address.municipality) ? '' : ', ' + candidate.address.municipality;
            candidate.displayAddress2 += _.isNull(candidate.address.state) ? '' : ', ' + candidate.address.state;
            candidate.displayAddress2 += _.isNull(candidate.address.region) ? '' : ', ' + candidate.address.region;
            candidate.displayAddress2 += _.isNull(candidate.address.county) ? '' : ', ' + candidate.address.country;
            candidate.displayAddress2 += _.isNull(candidate.address.zipcode) ? '' : ', ' + candidate.address.zipcode;
        }
        // if(_.isNull(candidate.educations)){
        //     candidate.educations = [];
        // }
        // if(_.isNull(candidate.experiences)){
        //     candidate.experiences = [];
        // }
        // if(_.isNull(candidate.technicalSkills)){
        //     candidate.technicalSkills = [];
        // }
        // if(_.isNull(candidate.softSkills)){
        //     candidate.softSkills = [];
        // }
        // if(_.isNull(candidate.certifications)){
        //     candidate.certifications = [];
        // }
        // if(_.isNull(candidate.others)){
        //     candidate.others = [];
        // }
        // if(_.isNull(candidate.compensations)){
        //     candidate.compensations = [];
        // }
        return candidate;
    }

    function launchActionsModal(type){
        var actionType = '';
        var actionLabel = '';
        switch(type){
            case 'updateContact':
                actionType = 'updateContact';
                actionLabel = 'Update Candidate Contact Details';
                break;
            default:
                break;
        }
        vm.candidateCardActionsModal = $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'app/directives/candidateProfile/candidate-profile-actions-modal.html',
            controller: ['$uibModalInstance','candidateId','reloadCandidateProfile','actionType','actionLabel',function ($uibModalInstance, candidateId, reloadCandidateProfile, actionType, actionLabel) {
                var vm = this;
                vm.candidateId = candidateId;
                vm.actionType = actionType;
                vm.actionLabel = actionLabel;
                vm.reloadCandidateProfile = reloadCandidateProfile;
                vm.closeModal = function () {
                    $uibModalInstance.close('cancel');
                };
                vm.saveCallback = function(){
                    if(vm.actionType == 'updateContact'){
                        vm.reloadCandidateProfile();
                    }
                    vm.closeModal();
                };
            }],
            controllerAs: 'actionsModal',
            size: 'lg',
            resolve:{
                actionType: function(){
                    return actionType;
                },
                actionLabel: function(){
                    return actionLabel;
                },
                candidateId: function () {
                    return vm.candidateId;
                },
                reloadCandidateProfile: function(){
                    return vm.init;
                }
            }
        });
    }

    vm.init();
}

CandidateProfileController.$inject = ['$uibModal','candidateService','CandidateProfile'];

fourdotfivedirectives.controller('CandidateProfileController', CandidateProfileController);