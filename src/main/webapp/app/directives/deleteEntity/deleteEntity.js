fourdotfivedirectives.directive('deleteEntity', function deleteEntity() {
    return {
        restrict: 'E',
        scope: {},
        bindToController: {
            entity: '@',
            candidateInfo: '=',
            userInfo: '=',
            clientOrBuInfo: '=',
            companyInfo: '=',
            deleteCallback: '&'
        },
        controller: 'DeleteEntityController',
        controllerAs: 'deleteEntity',
        templateUrl: 'app/directives/deleteEntity/delete-entity.html'
    }
});