/*
  * This is the controller for the delete entity directive
 */

function DeleteEntityController($state, $rootScope, $timeout, candidateService, userService, companyService, alertsAndNotificationsService, MESSAGECONSTANTS) {
    var vm = this;

    vm.MESSAGECONSTANTS = MESSAGECONSTANTS;
    vm.availabilityDate = null;
    vm.availabilityObject = {};
    // methods
    vm.init = init;
    vm.cancel = cancel;
    vm.deleteEntity = deleteEntity;
    vm.deleteCandidate = deleteCandidate;
    vm.deleteUser = deleteUser;
    vm.deleteClientOrBU = deleteClientOrBU;
    vm.deleteCompany = deleteCompany;
    vm.isStaffingCompany = isStaffingCompany;
    //flags
    vm.deletingFlag = false;
    
    vm.date;
    vm.time;
    vm.note;
    vm.init();

    function init() {
        _setDateAndTime();
    }

    function _setDateAndTime() {
        vm.date = moment().format('ll');
        vm.time = moment().format('hh:mm:ss a');
        $timeout(function () {
            _setDateAndTime();
        }, 1000);
    }

    function cancel() {
        if (vm.deleteCallback) {
            vm.deleteCallback();
        }
    }

    function deleteEntity() {
        if (vm.entity === "candidate") {
            var deleteCandidateObject = { id: vm.candidateInfo.candidateId, note: vm.note };
            vm.deleteCandidate(deleteCandidateObject);
        }
        else if (vm.entity === "user") {
            var deleteUserObject = { id: vm.userInfo.userId, note: vm.note };
            vm.deleteUser(deleteUserObject);
        }
        else if (vm.entity === "clientOrBU") {
            var deleteClientOrBUObject = { id: vm.clientOrBuInfo.clientOrBUId, note: vm.note };
            vm.deleteClientOrBU(deleteClientOrBUObject);
        }
        else if (vm.entity === "company") {
            var deleteCompanyObject = { id: vm.companyInfo.companyId, note: vm.note };
            vm.deleteCompany(deleteCompanyObject);
        }
    }

    function deleteCandidate(deleteCandidateObject) {
        candidateService.deleteCandidate(deleteCandidateObject, function (data) {
            vm.deletingFlag = false;
            alertsAndNotificationsService.showBannerMessage("Candidate '" + vm.candidateInfo.candidateName + "' deleted successfully", 'success');
            $state.reload();
            if (vm.deleteCallback) {
                vm.deleteCallback();
            }
        },
            function (error) {
                vm.deletingFlag = false;
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            });
    }

    function deleteUser(deleteUserObject) {
        userService.deleteUser(deleteUserObject, function (data) {
            vm.deletingFlag = false;
                alertsAndNotificationsService.showBannerMessage("User '" + vm.userInfo.fullName + "' deleted successfully", 'success');
                $state.reload();
                if (vm.deleteCallback) {
                    vm.deleteCallback();
                }
        },
            function (error) {
                vm.deletingFlag = false;
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            });
    }

    function deleteClientOrBU(deleteClientOrBUObject) {
        companyService.deleteCompany(deleteClientOrBUObject, function (data) {
            vm.deletingFlag = false;
            if (vm.isStaffingCompany()) {
                alertsAndNotificationsService.showBannerMessage("Client '" + vm.clientOrBuInfo.clientOrBUName + "' deleted successfully", 'success');
            }
            else {
                alertsAndNotificationsService.showBannerMessage("BU '" + vm.clientOrBuInfo.clientOrBUName + "' deleted successfully", 'success');
            }
           $state.reload();
           if (vm.deleteCallback) {
               vm.deleteCallback();
           }
        },
            function (error) {
                vm.deletingFlag = false;
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            });
    }

    function deleteCompany(deleteCompanyObject) {
        companyService.deleteCompany(deleteCompanyObject, function (data) {
            vm.deletingFlag = false;
                alertsAndNotificationsService.showBannerMessage("Company '" + vm.companyInfo.companyName + "' deleted successfully", 'success');
                if (vm.deleteCallback) {
                    vm.deleteCallback();
                }
        },
            function (error) {
                vm.deletingFlag = false;
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            });
    }

    function isStaffingCompany() {
        if (angular.equals($rootScope.userDetails.company.companyType, MESSAGECONSTANTS.COMPANY_TYPES.STAFFING)) {
            return true;
        }
        return false;
    }
}

DeleteEntityController.$inject = ['$state', '$rootScope', '$timeout', 'candidateService', 'userService', 'companyService', 'alertsAndNotificationsService', 'MESSAGECONSTANTS'];

fourdotfivedirectives.controller('DeleteEntityController', DeleteEntityController);