fourdotfivedirectives.directive('feedbackExecutionScreen', function feedbackExecutionScreen() {
    return {
        restrict: 'E',
        scope: {
            'jobMatchId': '@',
            'saveCallback': '&',
            'userId' : '@',
            'feedbackType': '@'
        },
        bindToController: true,
        controller:'FeedbackExecutionScreenController',
        controllerAs: 'feedbackExecution',
        templateUrl: 'app/directives/feedbackExecutionScreen/feedback-execution-screen.html'
    }
});