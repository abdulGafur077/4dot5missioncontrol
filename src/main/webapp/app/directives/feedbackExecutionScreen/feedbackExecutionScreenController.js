/*
  * This is the controller for the feedback execution directive
 */

function FeedbackExecutionScreenController($rootScope, $timeout, feedbackExecutionService, MESSAGECONSTANTS, ScreeningExecutionResult, jobMatchService, modalService, dateTimeUtilityService, alertsAndNotificationsService) {
    var vm = this;
    vm.init = init;
    vm.getRecruiterQuestions = getRecruiterQuestions;
    vm.getUserList = getUserList;
    vm.checkifCompanyCompleted = checkifCompanyCompleted;
    vm.checkifAllClientCompleted = checkifAllClientCompleted;
    vm.checkifClientCompleted = checkifClientCompleted;
    vm.checkifJobCompleted = checkifJobCompleted;
    vm.isEmpty = isEmpty;
    vm.filterFunction = filterFunction;
    vm.initializeSliders = initializeSliders;
    vm.initCoSliders = initCoSliders;
    vm.initClSliders = initClSliders;
    vm.initJobSliders = initJobSliders;
    vm.clearObjects = clearObjects;
    vm.checkifAllJobCompleted = checkifAllJobCompleted;
    vm.filterFunctionForOverall = filterFunctionForOverall;
    vm.filterByInterested = filterByInterested;
    vm.saveResults = saveResults;
    vm.checkifCompanyOverallCompleted = checkifCompanyOverallCompleted;
    vm.countClientQns = countClientQns;
    vm.countJobQns = countJobQns;
    vm.toDisplayJobPanel = toDisplayJobPanel;
    vm.toDisplayClientPanel = toDisplayClientPanel;
    vm.setClientJobCompleted = setClientJobCompleted;
    vm.updateRecruiterQns = updateRecruiterQns;
    vm.closeModal = closeModal;
    vm.cancelModal = cancelModal;
    vm.completeModal= completeModal;
    vm.feedbackEntityChanged = feedbackEntityChanged;
    vm.getCandidateJobMatchCardInfo = getCandidateJobMatchCardInfo;
    vm.isClientJobEnabled = isClientJobEnabled;
    // initialize variables
    vm.totalClQnsCount = 0;
    vm.totalCount = 0;
    vm.recruiterqns = null;
    vm.companyQnsComplete;
    vm.type;
    vm.loggedInUserId = null;
    vm.isCorporateFlag = false;
    vm.showInterested = true;
    vm.savingFlag = false;
    vm.completingFlag = false;
    vm.saveComplete = false;
    vm.displayQuestions = false;
    vm.userList = [];
    vm.savedRecruiterQns;
    vm.userGetRecruiterQns;
    vm.jobMatch;
    vm.candidateJobMatch = {};
    vm.displayRecruiterName = false;
    vm.feedbackTypeLabel = '';
    vm.noEditableMeetingsLabel = '';
    vm.completeButtonLabel = '';
    vm.chosenUser = null;
    vm.chosenMeeting = null;
    // flags
    vm.loadingJobMatchDetailsFlag = false;
    vm.loadingFeedbackOptionsFlag = false;
    vm.loadingFeedbackDetailsFlag = false;
    vm.showMeetingsFlag = false;
    vm.editableMeetingsExistFlag = false;

    vm.init();

    function init(refreshFlag) {
        vm.loggedInUserId = $rootScope.userDetails.id;
        vm.isCorporateFlag = ($rootScope.userDetails.company.companyType === MESSAGECONSTANTS.COMPANY_TYPES.CORPORATE);
        vm.feedbackExecutionResult = new ScreeningExecutionResult();
        if(_.isNull(vm.feedbackType) || _.isUndefined(vm.feedbackType)){
            vm.feedbackType = 'Screening';
            vm.feedbackTypeLabel = 'Screening Feedback for';
        }else{
            if(vm.feedbackType == 'Interview'){
                vm.feedbackTypeLabel = 'Interview Feedback for';
                vm.noEditableMeetingsLabel = "No 'Interviews' available for Feedback !";
            }else if(vm.feedbackType == 'PhoneScreen'){
                vm.feedbackTypeLabel = 'Phone Screen Feedback for';
                vm.noEditableMeetingsLabel = "No 'Phone Screens' available for Feedback !";
            }else{
                vm.feedbackTypeLabel = 'Screening Feedback for';
            }
        }

        if(!_.isUndefined(refreshFlag) && refreshFlag){
            vm.loadingFeedbackOptionsFlag = true;
            vm.loadingFeedbackDetailsFlag = true;
        }else{
            vm.loadingJobMatchDetailsFlag = true;
        }
        vm.getCandidateJobMatchCardInfo(refreshFlag, function(){
            vm.loadingJobMatchDetailsFlag = false;
            if(vm.feedbackType == 'Screening'){
                vm.loadingFeedbackOptionsFlag = true;
                vm.getUserList(function () {
                    vm.loadingFeedbackOptionsFlag = false;
                    // if a user id was passed in to the directive
                    if(!_.isUndefined(vm.userId)){
                        var passedInUser = {};
                        angular.forEach(vm.userList, function (val, key) {
                            if(val.id == vm.userId){
                                passedInUser = val;
                            }
                        });
                        vm.userList = [];
                        vm.userList.push(passedInUser);
                        vm.chosenUser = passedInUser;
                        vm.feedbackEntityChanged();
                    }else if(vm.userList.length == 1) {
                        vm.chosenUser = vm.userList[0];
                        vm.feedbackEntityChanged();
                    }else{
                        // there exist multiple users and refresh flag is true
                        if(!_.isUndefined(refreshFlag) && refreshFlag){
                            $timeout(function(){
                                $('#chosenUser-' + vm.chosenUser.id).prop("checked", true);
                            }, 500);
                            // reload the feedback data
                            vm.feedbackEntityChanged();
                        }
                    }
                }, function () {
                    vm.loadingFeedbackOptionsFlag = false;
                });
            }else if (vm.feedbackType == 'Interview'){
                vm.showMeetingsFlag = false;
                vm.loadingFeedbackOptionsFlag = true;
                jobMatchService.getInterviewers(vm.jobMatchId, false, function (data) {
                    vm.meetingsList = _getMeetingsList(data);
                    // if a user id was passed in to the directive
                    if(!_.isUndefined(vm.userId)){
                        var meetingOfPassedInUser = {};
                        angular.forEach(vm.meetingsList, function (val, key) {
                            if(val.userId == vm.userId){
                                meetingOfPassedInUser = val;
                            }
                        });
                        vm.meetingsList = [];
                        vm.meetingsList.push(meetingOfPassedInUser);
                        vm.loadingFeedbackOptionsFlag = false;
                        vm.chosenMeeting = meetingOfPassedInUser;
                        vm.feedbackEntityChanged();
                    }else if(vm.meetingsList.length == 1) {
                        vm.loadingFeedbackOptionsFlag = false;
                        vm.chosenMeeting = vm.meetingsList[0];
                        vm.feedbackEntityChanged();
                    }else{
                        vm.loadingFeedbackOptionsFlag = false;
                    }
                    vm.showMeetingsFlag = true;
                    // there exist multiple meetings and refresh flag is true
                    if(vm.meetingsList.length > 1 && !_.isUndefined(refreshFlag) && refreshFlag){
                        $timeout(function(){
                            $('#chosenMeeting-' + vm.chosenMeeting.id).prop("checked", true);
                        }, 500);
                        // reload the feedback data
                        vm.feedbackEntityChanged();
                    }
                }, function (error) {
                    vm.loadingFeedbackOptionsFlag = false;
                    if ($rootScope.isOnline) {
                        alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                    }
                });
            }else if (vm.feedbackType == 'PhoneScreen'){
                vm.showMeetingsFlag = false;
                vm.loadingFeedbackOptionsFlag = true;
                jobMatchService.getPhoneScreeners(vm.jobMatchId, false, function (data) {
                    vm.meetingsList = _getMeetingsList(data);
                    // if a user id was passed in to the directive
                    if(!_.isUndefined(vm.userId)){
                        var meetingOfPassedInUser = {};
                        angular.forEach(vm.meetingsList, function (val, key) {
                            if(val.userId == vm.userId){
                                meetingOfPassedInUser = val;
                            }
                        });
                        vm.meetingsList = [];
                        vm.meetingsList.push(meetingOfPassedInUser);
                        vm.loadingFeedbackOptionsFlag = false;
                        vm.chosenMeeting = meetingOfPassedInUser;
                        vm.feedbackEntityChanged();
                    }else if(vm.meetingsList.length == 1) {
                        vm.loadingFeedbackOptionsFlag = false;
                        vm.chosenMeeting = vm.meetingsList[0];
                        vm.feedbackEntityChanged();
                    }else{
                        vm.loadingFeedbackOptionsFlag = false;
                    }
                    vm.showMeetingsFlag = true;
                    // there exist multiple meetings and refresh flag is true
                    if(vm.meetingsList.length > 1 && !_.isUndefined(refreshFlag) && refreshFlag){
                        $timeout(function(){
                            $('#chosenMeeting-' + vm.chosenMeeting.id).prop("checked", true);
                        }, 500);
                        // reload the feedback data
                        vm.feedbackEntityChanged();
                    }
                }, function (error) {
                    vm.loadingFeedbackOptionsFlag = false;
                    if ($rootScope.isOnline) {
                        alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                    }
                });
            }
        });
    }

    function feedbackEntityChanged(){
        // reset question counts
        vm.totalClQnsCount = 0;
        vm.totalCount = 0;
        if(vm.feedbackType == 'Screening'){
            vm.userGetRecruiterQns = vm.chosenUser.id;
            vm.completeButtonLabel = 'Complete';
        }else{
            vm.userGetRecruiterQns = vm.chosenMeeting.userId;
            if(vm.chosenMeeting.status == 'COMPLETED'){
                vm.completeButtonLabel = 'Update';
            }else{
                vm.completeButtonLabel = 'Complete';
            }
        }
        vm.getRecruiterQuestions();
    }

    function getCandidateJobMatchCardInfo(refreshFlag, successCallback){
        if(!_.isUndefined(refreshFlag) && refreshFlag){
            if(successCallback){
                successCallback();
            }
        }else{
            jobMatchService.getCandidateJobMatchCard(vm.jobMatchId, function (data) {
                data.name = data.firstName + ' ' + data.lastName;
                angular.merge(vm.candidateJobMatch, data);
                if(successCallback){
                    successCallback();
                }
            }, function (error) {
                if ($rootScope.isOnline) {
                    alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                }
            });
        }
    }

    function getUserList(successCallback, errorCallback){
        feedbackExecutionService.getUserList( vm.jobMatchId,vm.loggedInUserId, function (data) {
            vm.userList = angular.copy(data);
            vm.displayRecruiterName = true;
            if(successCallback){
                successCallback();
            }
        }, function (error){
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
            if(errorCallback){
                errorCallback();
            }
        });
    }

    function _getMeetingsList(response){
        var meetingsList = [];
        angular.forEach(response, function (meetingDetails, key) {
            //var currentDateTime = new Date();
            var startDateTime = new Date(meetingDetails.meetingStartTime);
            // only meetings that are after the current time should be displayed
            //if(currentDateTime >= startDateTime){
            var endDateTime = new Date(meetingDetails.meetingEndTime);
            angular.forEach(meetingDetails.participants, function (meetingUser, k) {
                var meeting = {};
                meeting.id = meetingDetails.meetingScheduleId;
                meeting.userId = meetingUser.id;
                meeting.userName = meetingUser.firstName;
                if(!_.isUndefined(meetingUser.lastName) && !_.isNull(meetingUser.lastName)){
                    meeting.userName += ' ' + meetingUser.lastName;
                }
                meeting.status = meetingDetails.status;
                switch(meeting.status){
                    case 'SCHEDULED':
                    case 'ACTIVE':
                        meeting.statusClass = 'fa fa-calendar feedback-meeting-icon-scheduled';
                        meeting.statusLabel = 'Scheduled';
                        meeting.editableFlag = true;
                        vm.editableMeetingsExistFlag = true;
                        break;
                    case 'IN_PROGRESS':
                        meeting.statusClass = 'fa fa-hourglass-half feedback-meeting-icon-in-progress';
                        meeting.statusLabel = 'In Progress';
                        meeting.editableFlag = true;
                        vm.editableMeetingsExistFlag = true;
                        break;
                    case 'COMPLETED':
                        meeting.statusClass = 'fa fa-check feedback-meeting-icon-completed';
                        meeting.statusLabel = 'Completed';
                        meeting.editableFlag = true;
                        vm.editableMeetingsExistFlag = true;
                        break;
                    case 'CANCELLED':
                        meeting.statusClass = 'fa fa-ban feedback-meeting-icon-cancelled';
                        meeting.statusLabel = 'Canceled';
                        meeting.editableFlag = false;
                        break;
                    default:
                        // set defaults
                        meeting.statusClass = 'fa fa-question feedback-meeting-icon-in-progress';
                        meeting.statusLabel = 'Status Unknown';
                        meeting.editableFlag = false;
                }
                meeting.scheduledTime = moment(startDateTime).format("MMM DD, YYYY ( hh:mm a") + ' to ' + moment(endDateTime).format("hh:mm a )");
                meeting.label = meeting.userName + ' - ' + meeting.scheduledTime;
                meetingsList.push(meeting);
            });
            //}
        });
        return meetingsList;
    }

    function getRecruiterQuestions(successCallback) {
        vm.loadingFeedbackDetailsFlag = true;
        feedbackExecutionService.getRecruiterQuestions(vm.jobMatchId, vm.userGetRecruiterQns, function (data) {
            vm.loadingFeedbackDetailsFlag = false;
            vm.recruiterqns = angular.copy(data);
            vm.displayQuestions = true;
            for (var key in vm.recruiterqns.jobDetails) {
                var jobDetailval = vm.recruiterqns.jobDetails[key];
                vm.type = jobDetailval.feedbackType;
                break;
            }
            if (vm.type != 'R') {
                vm.showInterested = false;
            }
            vm.savedRecruiterQns = angular.copy(vm.recruiterqns);
            vm.checkifCompanyCompleted();
            if (successCallback) {
                successCallback();
            }
        }, function (error) {
            vm.loadingFeedbackDetailsFlag = false;
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });

    }

    function getCompanyClass() {
        if (vm.companyQnsComplete)
            return "panel-collapse collapse";
        else
            return "panel-collapse collapse in";
    }

    function getClientClass() {
        if (vm.checkifClientCompleted(keyval))
            return "panel-collapse collapse";
        else
            return "panel-collapse collapse in";
    }


    function getJobClass() {
        if (vm.checkifJobCompleted(keyval))
            return "panel-collapse collapse";
        else
            return "panel-collapse collapse in";
    }

    function getOverallClass() {
        if (vm.checkifCompanyOverallCompleted())
            return "panel-collapse collapse";
        else
            return "panel-collapse collapse in";
    }

    function setClientJobCompleted() {
        for (var clientId in vm.recruiterqns.clientListJobMatchDetails) {
            var jobArray = vm.recruiterqns.clientListJobMatchDetails[clientId];
            for (var i = 0; i < jobArray.length; i++) {
                var jobId = jobArray[i];
                // commenting the 'checkifJobCompleted' and including just checkIfClientCompleted for now as job questions are not involved.
                // checkifClientCompleted(clientId) && checkifJobCompleted(jobId)
                if (checkifClientCompleted(clientId)) {
                    vm.recruiterqns.jobDetails[jobId].completed = true;
                } else {
                    vm.recruiterqns.jobDetails[jobId].completed = false;
                }
            }
        }
    }

    function checkifCompanyCompleted() {
        if (vm.recruiterqns === undefined || vm.recruiterqns == '') {
            vm.companyQnsComplete = false;
            return false;
        }

        var companykey;
        for (var key in vm.recruiterqns.companyQuestionsResults) {
            companykey = key
        }
        var companyQuestion = vm.recruiterqns.companyQuestionsResults[companykey];
        if (companyQuestion != null) {
            for (var i = 0; i < companyQuestion.length; i++) {
                var recruiterComment = companyQuestion[i].recruiterComment;
                var overallQuestion = companyQuestion[i].question;
                var sliderId = companyQuestion[i].rating;
                if (overallQuestion != 'Overall Comment') {
                    if (isEmpty(recruiterComment) && sliderId == "0") {
                        vm.companyQnsComplete = false;
                        return false;
                    }
                }
            }
        }
        vm.companyQnsComplete = true;
        return true;
    }

    function checkifAllClientCompleted() {
        for (var key in vm.recruiterqns.clientQuestionsResults) {
            var clientQuestion = vm.recruiterqns.clientQuestionsResults[key];
            if (vm.clientNotInterestedList.indexOf(key) > -1) {
                continue;
            }
            if (clientQuestion != null) {
                for (var i = 0; i < clientQuestion.length; i++) {
                    var recruiterComment = clientQuestion[i].recruiterComment
                    var sliderId = clientQuestion[i].rating;
                    if (isEmpty(recruiterComment) && sliderId == "0") {
                        return false;
                    }
                }
            }
        }
        return true;

    }


    function checkifClientCompleted(key) {
        var clientQuestion = vm.recruiterqns.clientQuestionsResults[key];
        if (clientQuestion != null) {
            for (var i = 0; i < clientQuestion.length; i++) {
                var recruiterComment = clientQuestion[i].recruiterComment;
                var sliderId = clientQuestion[i].rating;
                if (isEmpty(recruiterComment) && sliderId == "0") {
                    return false;
                }
            }
        }
        return true;

    }


    function checkifJobCompleted(key) {
        var jobQuestion = vm.recruiterqns.jobQuestionsResults[key];
        for (var i = 0; i < jobQuestion.length; i++) {
            var recruiterComment = jobQuestion[i].recruiterComment;
            var sliderId = jobQuestion[i].rating;
            if (isEmpty(recruiterComment) && sliderId == "0") {
                return false;
            }
        }
        return true;
    }


    function checkifAllJobCompleted() {
        for (var key in vm.recruiterqns.jobQuestionsResults) {
            var jobQuestion = vm.recruiterqns.jobQuestionsResults[key];
            if (vm.jobNotInterestedList.indexOf(key) > -1) {
                continue;
            }
            for (var i = 0; i < jobQuestion.length; i++) {
                var recruiterComment = jobQuestion[i].recruiterComment
                var sliderId = jobQuestion[i].rating;
                if (isEmpty(recruiterComment) && sliderId == "0") {
                    return false;
                }
            }
        }
        return true;

    }

    function checkifCompanyOverallCompleted() {
        if (vm.recruiterqns === undefined || vm.recruiterqns == '')
            return true;

        var companykey;
        if (vm.recruiterqns != null && vm.recruiterqns.companyQuestionsResults != null) {
        for (var key in vm.recruiterqns.companyQuestionsResults) {
            companykey = key
        }

        var companyQuestion = vm.recruiterqns.companyQuestionsResults[companykey];
        if (companyQuestion != null) {
            for (var i = 0; i < companyQuestion.length; i++) {
                var recruiterComment = companyQuestion[i].recruiterComment;
                var overallQuestion = companyQuestion[i].question;
                var sliderId = companyQuestion[i].rating;
                if (overallQuestion == 'Overall Comment') {
                    if (isEmpty(recruiterComment) && sliderId == "0") {
                        return false;
                    }
                }
            }
        }
    }
        return true;

    }


    function countJobQns() {
        return vm.totalCount++;
    }


    function countClientQns() {
        return vm.totalClQnsCount++;
    }

    function isEmpty(str) {
        return (!str || 0 === str.trim().length);
    }

    function initializeSliders() {
        initCoSliders();
        initClSliders();
        initJobSliders();

        $("div[id='overall-qn-slider']").each(function (i, obj) {
            var labelId = "#overall-qn-rating";
            $(obj).noUiSlider({
                range: [0, 10], start: [0], step: 1, handles: 1, connect: 'lower',
                slide: function () {
                    var val = $(this).val();
                    $(labelId).text(Math.round(val));
                },
                set: function () {
                    var val = $(this).val();
                    $(labelId).text(Math.round(val));
                }
            });
        });

        var companyQuestionIndex;
        if (vm.recruiterqns != undefined || vm.recruiterqns != '') {
            for (var key in  vm.recruiterqns.companyQuestionsResults) {
                var l = vm.recruiterqns.companyQuestionsResults[key].length;
                var overallRating = vm.recruiterqns.companyQuestionsResults[key][l - 1]["rating"];
                $("#overall-qn-slider").val(overallRating, true);
            }
        }
    }

    function initCoSliders() {
        $("div[id='co-qn-slider']").each(function (i, obj) {
            var labelId = "#co-qn-rating" + ++i;
            $(obj).noUiSlider({
                range: [0, 10], start: [0], step: 1, handles: 1, connect: 'lower',
                slide: function () {
                    var val = $(this).val();
                    $(labelId).text(Math.round(val));
                },
                set: function () {
                    var val = $(this).val();
                    $(labelId).text(Math.round(val));
                }
            });
            var qnRating = $(labelId).text();
            $(obj).val(qnRating, true);
        });
    }

    function initClSliders() {
        $("div[id='cl-qn-slider']").each(function (i, obj) {
            var labelId = "#cl-qn-rating" + ++i;
            $(obj).noUiSlider({
                range: [0, 10], start: [0], step: 1, handles: 1, connect: 'lower',
                slide: function () {
                    var val = $(this).val();
                    $(labelId).text(Math.round(val));
                },
                set: function () {
                    var val = $(this).val();
                    $(labelId).text(Math.round(val));
                }
            });
            var qnRating = $(labelId).text();
            $(obj).val(qnRating, true);
        });
    }

    function initJobSliders() {
        $("div[id='job-qn-slider']").each(function (i, obj) {
            var labelId = "#job-qn-rating" + ++i;
            $(obj).noUiSlider({
                range: [0, 10], start: [0], step: 1, handles: 1, connect: 'lower',
                slide: function () {
                    var val = $(this).val();
                    $(labelId).text(Math.round(val));
                },
                set: function () {
                    var val = $(this).val();
                    $(labelId).text(Math.round(val));
                }
            });
            var qnRating = $(labelId).text();
            $(obj).val(qnRating, true);
        });
    }


    function filterFunction(element) {
        return element.question.match(/^Overall/) ? false : true;
    }

    function filterFunctionForOverall(element) {
        if (element != null) {
            return element.question.match(/^Overall/) ? true : false;
        }
    }


    function clearObjects() {
        vm.recruiterqns = "";
    }


    function filterByInterested() {
        for (var clientId in vm.recruiterqns.clientListJobMatchDetails) {
            var showClient = false;
            var jobArray = vm.recruiterqns.clientListJobMatchDetails[clientId];
            for (var i = 0; i < jobArray.length; i++) {
                var jobId = jobArray[i];
                // commenting as job questions are not involved right now
                // var x = document.getElementById(jobId);
                if (vm.recruiterqns.jobDetails[jobId].interested) {
                    // commenting as job questions are not involved right now
                    // x.style.display = "block";
                    showClient = true;
                } else {
                    // commenting as job questions are not involved right now
                    // x.style.display = "none";
                }

                var y = document.getElementById(clientId);
                if (showClient) {
                    y.style.display = "block";
                } else {
                    y.style.display = "none";
                }
            }
        }
    }

    function toDisplayJobPanel(jobMatchId) {
        var jobQuestionList = vm.recruiterqns.jobQuestionsResults[jobMatchId];
        if (jobQuestionList == null || jobQuestionList.length == 0) {
            return false;
        } else {
            return true;
        }
    }

    function toDisplayClientPanel(clientKey) {
        var clientQuestionList = vm.recruiterqns.clientQuestionsResults[clientKey];
        if (clientQuestionList == null || clientQuestionList.length == 0) {
            return false;
        } else {

            return true;
        }
    }

    function isClientJobEnabled(clientId){
        var jobArray = vm.recruiterqns.clientListJobMatchDetails[clientId];
        var isClientJobEnabledFlag = false;
        angular.forEach(jobArray, function(jobId, key){
            if(vm.recruiterqns.jobDetails[jobId].interested){
                isClientJobEnabledFlag = true;
            }
        });
        return isClientJobEnabledFlag;
    }


    function closeModal(){
        if(!_.isUndefined(vm.saveCallback)){
            vm.saveCallback();
        }
     }

    function completeModal(fromButton){
       vm.saveResults(fromButton);
       /* setTimeout(function() {
          if(!_.isUndefined(vm.saveCallback)){
                vm.saveCallback();
            }
        }, 5000);*/
    }

    function cancelModal(){
        vm.updateRecruiterQns();
        var showConfirmationPopup = false;
        if (!angular.equals(vm.savedRecruiterQns, vm.recruiterqns)){
            showConfirmationPopup = true;
        }
        if (showConfirmationPopup) {
            bootbox.confirm({
                closeButton: false,
                title: "<div class='alert alert-warning' style='margin-bottom: 0px;'><i class='fa fa-warning fa-fw fa-lg'></i><strong>Warning!</strong></div>",
                message: "Unsaved data on the page will be lost. Do you still want to continue?",
                className: "zIndex1060",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'Cancel',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result) {
                        if(!_.isUndefined(vm.closeModal)){
                            vm.closeModal();
                        }
                    }
                    _addModalOpenClassToBodyIfAnyModalIsOpen();
                }
            });
        }else {
            if(!_.isUndefined(vm.closeModal)){
                vm.closeModal();
            }
        }
      }

    function updateRecruiterQns(){
        var companyQuestionIndex = 0;
        for (var key in vm.recruiterqns.companyQuestionsResults) {
            $("span[id='co-qn']").each(function (i, obj) {
                var suffix = i + 1;
                var sliderId = "#co-qn-rating" + suffix;
                var textAreaId = "#co-qn-comments" + suffix;
                vm.recruiterqns.companyQuestionsResults[key][i]["rating"] = parseInt($(sliderId).text());
                if($(textAreaId).val().toString().trim() === ''){
                    vm.recruiterqns.companyQuestionsResults[key][i]["recruiterComment"] = vm.savedRecruiterQns.companyQuestionsResults[key][i]["recruiterComment"];
                }else{
                    vm.recruiterqns.companyQuestionsResults[key][i]["recruiterComment"] = $(textAreaId).val();
                }
                companyQuestionIndex = i;
            });
            companyQuestionIndex = companyQuestionIndex + 1;
            var overallsliderId = "#overall-qn-rating";
            var overalltextAreaId = "#overall-qn-comments";
            vm.recruiterqns.companyQuestionsResults[key][companyQuestionIndex]["question"] = "Overall Comment";
            vm.recruiterqns.companyQuestionsResults[key][companyQuestionIndex]["rating"] = parseInt($(overallsliderId).text());
            if($(overalltextAreaId).val().toString().trim() === ''){
                vm.recruiterqns.companyQuestionsResults[key][companyQuestionIndex]["recruiterComment"] = vm.savedRecruiterQns.companyQuestionsResults[key][companyQuestionIndex]["recruiterComment"];
            }else{
                vm.recruiterqns.companyQuestionsResults[key][companyQuestionIndex]["recruiterComment"] = $(overalltextAreaId).val();
            }
        }

        for (var key in vm.recruiterqns.clientQuestionsResults) {
            var spanId = "cl-qn" + key;
            $('.' + spanId).each(function (i) {
                var clQnNoFldId = "cl-qn-no" + key;
                var clQnNo = $("input[id=" + clQnNoFldId + "]")[i].value;
                var sliderId = "#cl-qn-rating" + clQnNo;
                var textAreaId = "#cl-qn-comments" + clQnNo;
                vm.recruiterqns.clientQuestionsResults[key][i]["rating"] = parseInt($(sliderId).text());
                if($(textAreaId).val().toString().trim() === ''){
                    vm.recruiterqns.clientQuestionsResults[key][i]["recruiterComment"] = vm.savedRecruiterQns.clientQuestionsResults[key][i]["recruiterComment"];
                }else{
                    vm.recruiterqns.clientQuestionsResults[key][i]["recruiterComment"] = $(textAreaId).val();
                }
            });
        }

        for (var key in vm.recruiterqns.jobQuestionsResults) {
            var spanId = "job-qn" + key;
            $("span[id=" + spanId + "]").each(function (i, obj) {
                var jobQnNoFldId = "job-qn-no" + key;
                var jobQnNo = $("input[id=" + jobQnNoFldId + "]")[i].value;
                var sliderId = "#job-qn-rating" + jobQnNo;
                var textAreaId = "#job-qn-comments" + jobQnNo;
                vm.recruiterqns.jobQuestionsResults[key][i]["rating"] = parseInt($(sliderId).text());
                if($(textAreaId).val().toString().trim() === ''){
                    vm.recruiterqns.jobQuestionsResults[key][i]["recruiterComment"] = vm.savedRecruiterQns.jobQuestionsResults[key][i]["recruiterComment"];
                }else{
                    vm.recruiterqns.jobQuestionsResults[key][i]["recruiterComment"] = $(textAreaId).val();
                }
            });
        }
    }

    function _addModalOpenClassToBodyIfAnyModalIsOpen(){
        if(modalService.isAnyModalOpen()){
            // add back modal-open class to body to enable scrolling of the modal
            $timeout(function(){
                $('body').addClass('modal-open');
            }, 500);
        }
    }

    function saveResults(fromButton) {

        var statusOfSave;
        var listofCompanyQuestions;
        var listofClientQuestions = [];
        var listofJobQuestions = [];
        var companyQuestionIndex;
        var companyCompleted = true;
        var clientAllCompleted = true;
        var joballCompleted = true;
        var participantId = '';
        var companyOverallCompleted = true;
        // populate the participant Id
        if (vm.userList.length == 1) {
            participantId = vm.userList[0].id;
        }else{
            participantId = vm.userGetRecruiterQns;
        }
        if (vm.recruiterqns != null) {
            for (var key in vm.recruiterqns.companyQuestionsResults) {
                $("span[id='co-qn']").each(function (i, obj) {
                    var suffix = i + 1;
                    var sliderId = "#co-qn-rating" + suffix;
                    var textAreaId = "#co-qn-comments" + suffix;
                    vm.recruiterqns.companyQuestionsResults[key][i]["rating"] = $(sliderId).text();
                    vm.recruiterqns.companyQuestionsResults[key][i]["recruiterComment"] = $(textAreaId).val();
                    vm.recruiterqns.companyQuestionsResults[key][i]["questionareDateTime"] = dateTimeUtilityService.getCurrentTimeStampInAPIFormat();
                    vm.recruiterqns.companyQuestionsResults[key][i]["questionType"] = vm.type;
                    vm.recruiterqns.companyQuestionsResults[key][i]["lastModifiedBy"] = _.isNull(vm.userId) ? vm.loggedInUserId: vm.userId;
                    vm.recruiterqns.companyQuestionsResults[key][i]["participantId"] = participantId;
                    companyQuestionIndex = i;
                    if (vm.recruiterqns.companyQuestionsResults[key][i]["rating"] == "0" && isEmpty(vm.recruiterqns.companyQuestionsResults[key][i]["recruiterComment"])) {
                        companyCompleted = false;
                    }
                });
                companyQuestionIndex = companyQuestionIndex + 1;
                var overallsliderId = "#overall-qn-rating";
                var overalltextAreaId = "#overall-qn-comments";
                vm.recruiterqns.companyQuestionsResults[key][companyQuestionIndex]["question"] = "Overall Comment";
                vm.recruiterqns.companyQuestionsResults[key][companyQuestionIndex]["rating"] = $(overallsliderId).text();
                vm.recruiterqns.companyQuestionsResults[key][companyQuestionIndex]["recruiterComment"] = $(overalltextAreaId).val();
                vm.recruiterqns.companyQuestionsResults[key][companyQuestionIndex]["questionareDateTime"] = dateTimeUtilityService.getCurrentTimeStampInAPIFormat();
                vm.recruiterqns.companyQuestionsResults[key][companyQuestionIndex]["questionType"] = vm.type;
                vm.recruiterqns.companyQuestionsResults[key][companyQuestionIndex]["lastModifiedBy"] = _.isNull(vm.userId) ? vm.loggedInUserId: vm.userId;
                vm.recruiterqns.companyQuestionsResults[key][companyQuestionIndex]["participantId"] = participantId;
                listofCompanyQuestions = vm.recruiterqns.companyQuestionsResults[key];
                if (vm.recruiterqns.companyQuestionsResults[key][companyQuestionIndex]["rating"] == "0" && isEmpty(vm.recruiterqns.companyQuestionsResults[key][companyQuestionIndex]["recruiterComment"])) {
                    companyOverallCompleted = false;
                }
            }


            for (var key in vm.recruiterqns.clientQuestionsResults) {
                if(vm.isClientJobEnabled(key)){
                    var spanId = "cl-qn" + key;
                    $('.' + spanId).each(function (i) {
                        var clQnNoFldId = "cl-qn-no" + key;
                        var clQnNo = $("input[id=" + clQnNoFldId + "]")[i].value;
                        var sliderId = "#cl-qn-rating" + clQnNo;
                        var textAreaId = "#cl-qn-comments" + clQnNo;
                        vm.recruiterqns.clientQuestionsResults[key][i]["rating"] = $(sliderId).text();
                        vm.recruiterqns.clientQuestionsResults[key][i]["recruiterComment"] = $(textAreaId).val();
                        vm.recruiterqns.clientQuestionsResults[key][i]["questionareDateTime"] = dateTimeUtilityService.getCurrentTimeStampInAPIFormat();
                        vm.recruiterqns.clientQuestionsResults[key][i]["questionType"] = vm.type;
                        vm.recruiterqns.clientQuestionsResults[key][i]["lastModifiedBy"] = _.isNull(vm.userId) ? vm.loggedInUserId: vm.userId;
                        vm.recruiterqns.clientQuestionsResults[key][i]["participantId"] = participantId;
                        if (vm.recruiterqns.clientQuestionsResults[key][i]["rating"] == "0" && isEmpty(vm.recruiterqns.clientQuestionsResults[key][i]["recruiterComment"])) {
                            clientAllCompleted = false;
                        }
                    });

                    if (listofClientQuestions != null) {
                        listofClientQuestions = listofClientQuestions.concat(vm.recruiterqns.clientQuestionsResults[key]);
                    }
                    else {
                        listofClientQuestions = vm.recruiterqns.clientQuestionsResults[key];
                    }
                }
            }

            for (var key in vm.recruiterqns.jobQuestionsResults) {
                if(vm.toDisplayJobPanel(key)) {
                    var spanId = "job-qn" + key;
                    $("span[id=" + spanId + "]").each(function (i, obj) {
                        var jobQnNoFldId = "job-qn-no" + key;
                        var jobQnNo = $("input[id=" + jobQnNoFldId + "]")[i].value;
                        var sliderId = "#job-qn-rating" + jobQnNo;
                        var textAreaId = "#job-qn-comments" + jobQnNo;
                        vm.recruiterqns.jobQuestionsResults[key][i]["rating"] = $(sliderId).text();
                        vm.recruiterqns.jobQuestionsResults[key][i]["recruiterComment"] = $(textAreaId).val();
                        vm.recruiterqns.jobQuestionsResults[key][i]["questionareDateTime"] = dateTimeUtilityService.getCurrentTimeStampInAPIFormat();
                        vm.recruiterqns.jobQuestionsResults[key][i]["questionType"] = vm.type;
                        vm.recruiterqns.jobQuestionsResults[key][i]["lastModifiedBy"] = _.isNull(vm.userId) ? vm.loggedInUserId : vm.userId;
                        vm.recruiterqns.jobQuestionsResults[key][i]["participantId"] = participantId;
                        if (vm.recruiterqns.jobQuestionsResults[key][i]["rating"] == "0" && isEmpty(vm.recruiterqns.jobQuestionsResults[key][i]["recruiterComment"])) {
                            joballCompleted = false;
                        }
                    });

                    if (listofJobQuestions != null) {
                        listofJobQuestions = listofJobQuestions.concat(vm.recruiterqns.jobQuestionsResults[key]);
                    }
                    else {
                        listofJobQuestions = vm.recruiterqns.jobQuestionsResults[key];

                    }
                    var jobQuestionList = vm.recruiterqns.jobQuestionsResults[key];
                    if (jobQuestionList == null || jobQuestionList.length == 0) {
                        joballCompleted = true;
                    }
                }
            }

            var listOfCompanyquest = listofCompanyQuestions.concat(listofClientQuestions);
            // commenting for now as job questions are not involved now.
            //listOfQuestionsResults = listOfCompanyquest.concat(listofJobQuestions);
            var listOfQuestionsResults = listOfCompanyquest;

            vm.setClientJobCompleted();

            //  vm.feedbackExecutionResult = new FeedbackExecutionResult();
            if (companyCompleted && clientAllCompleted && companyOverallCompleted) {
                vm.feedbackExecutionResult.companyAndOverallComplete = "complete";
            } else {
                vm.feedbackExecutionResult.companyAndOverallComplete = "notcomplete";
            }

            var listOfjobdetails = [];

            for (var key in vm.recruiterqns.jobDetails) {
                listOfjobdetails.push(vm.recruiterqns.jobDetails[key]);
            }

            vm.feedbackExecutionResult.feedbackAnswersList = listOfQuestionsResults;
            vm.feedbackExecutionResult.jobDetailsRecruiterScreeningList = listOfjobdetails;
            vm.feedbackExecutionResult.fromButton = fromButton;
            // validation before save
            if(fromButton === 'complete' && ( !companyCompleted || !clientAllCompleted || !companyOverallCompleted )){
                // complete / update is clicked and all the questions are not answered
                bootbox.alert({
                    closeButton: false,
                    title: "<div class='alert alert-warning' style='margin-bottom: 0px;'><i class='fa fa-warning fa-fw fa-lg'></i><strong>Incomplete Form!</strong></div>",
                    message: "Please answer all the questions to complete the feedback form.",
                    className: "zIndex1060",
                    buttons: {
                        ok: {
                            label: 'Ok',
                            className: 'btn-info'
                        }
                    },
                    callback: function(){
                        _addModalOpenClassToBodyIfAnyModalIsOpen();
                    }
                });
            }else if( (vm.feedbackType == 'Interview' || vm.feedbackType == 'PhoneScreen') && vm.chosenMeeting.status == 'COMPLETED' && ( !companyCompleted || !clientAllCompleted || !companyOverallCompleted )){
                // save is clicked for an interview or phone screen that is already completed and all questions are not answered.
                bootbox.alert({
                    closeButton: false,
                    title: "<div class='alert alert-warning' style='margin-bottom: 0px;'><i class='fa fa-warning fa-fw fa-lg'></i><strong>Incomplete Form!</strong></div>",
                    message: "You are saving a Completed Feedback form. Please answer all the questions.",
                    className: "zIndex1060",
                    buttons: {
                        ok: {
                            label: 'Ok',
                            className: 'btn-info'
                        }
                    },
                    callback: function(){
                        _addModalOpenClassToBodyIfAnyModalIsOpen();
                    }
                });
            }else {
                if(fromButton == 'save') {
                    $("#cancelBtn").prop("disabled", true);
                    $("#completeBtn").prop("disabled", true);
                    vm.savingFlag = true;
                } else if (fromButton == 'complete') {
                    $("#cancelBtn").prop("disabled", true);
                    $("#saveBtn").prop("disabled", true);
                    vm.completingFlag = true;
                }
                feedbackExecutionService.saveRecruiterQuestions(angular.toJson(vm.feedbackExecutionResult), vm.jobMatchId, function (data) {
                    if (fromButton == 'save') {
                        vm.savingFlag = false;
                        $("#cancelBtn").prop("disabled", false);
                        $("#completeBtn").prop("disabled", false);
                        // refresh the data on screen.
                        vm.init(true);
                    } else if (fromButton == 'complete') {
                        var feedbackTypeLabel = 'Screening';
                        var completeTypeLabel = 'completed';
                        if(vm.feedbackType == 'Interview' || vm.feedbackType == 'PhoneScreen'){
                            feedbackTypeLabel = vm.feedbackType == 'Interview' ? 'Interview' : 'Phone Screen';
                            if(vm.chosenMeeting.status == 'COMPLETED'){
                                completeTypeLabel = 'updated';
                            }
                        }
                        var successMessage = 'The ' + feedbackTypeLabel + ' Feedback has been ' + completeTypeLabel +  ' successfully.';
                        bootbox.alert({
                            closeButton: false,
                            title: "<div class='alert alert-success' style='margin-bottom: 0px;'><i class='fa fa-check fa-fw fa-lg'></i><strong>Success!</strong></div>",
                            message: successMessage,
                            className: "zIndex1060",
                            buttons: {
                                ok: {
                                    label: 'Ok',
                                    className: 'btn-info'
                                }
                            },
                            callback: function () {
                                if (!_.isUndefined(vm.saveCallback)) {
                                    setTimeout(function () {
                                        vm.saveCallback();
                                    }, 300);
                                }else{
                                    vm.completingFlag = false;
                                    // refresh the data on screen.
                                    vm.init(true);
                                }
                                _addModalOpenClassToBodyIfAnyModalIsOpen();
                            }
                        });
                    }
                }, function (error) {
                    vm.savingFlag = false;
                    vm.completingFlag = false;
                    $("#cancelBtn").prop("disabled", false);
                    if ($rootScope.isOnline) {
                        alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                    }
                });
            }

        }
    }

}

FeedbackExecutionScreenController.$inject = ['$rootScope', '$timeout', 'feedbackExecutionService', 'MESSAGECONSTANTS', 'ScreeningExecutionResult', 'jobMatchService', 'modalService', 'dateTimeUtilityService', 'alertsAndNotificationsService'];

fourdotfivedirectives.controller('FeedbackExecutionScreenController', FeedbackExecutionScreenController);