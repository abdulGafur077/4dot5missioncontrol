fourdotfivedirectives.directive('jobBoardsSearchHistory', function jobBoardsSearchHistory() {
    return {
        restrict: 'E',
        scope: {},
        bindToController: {
            jobId: '@',
            jobTitle: '@',
            clientOrBuName: '@'
        },
        controller: 'JobBoardsSearchHistoryController',
        controllerAs: 'jobBoardsSearchHistory',
        templateUrl: 'app/directives/jobBoardsSearchHistory/job-boards-search-history.html'
    }
});