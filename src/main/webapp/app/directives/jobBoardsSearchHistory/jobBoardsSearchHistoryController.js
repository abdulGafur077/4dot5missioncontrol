/*
 * This is the controller for the job search history
 */

function JobBoardsSearchHistoryController($rootScope, jobService, resumeService, alertsAndNotificationsService) {
    var vm = this;

    //variables
    vm.searchHistory = [];
    vm.jobBoards = [];
    vm.searchHistoryContainer = null;
    // vm.Math = $window.Math;

    //flags
    vm.isPanelOpen = [];

    //methods
    vm.init = init;
    vm.getJobBoardsSearchHistory = getJobBoardsSearchHistory;
    vm.getAllJobBoards = getAllJobBoards;

    vm.init();

    function init() {
        vm.isPanelOpen.push(true);
        getAllJobBoards(function () {
            vm.searchHistoryContainer = new searchHistoryContainer();
            vm.searchHistoryContainer.searchHistoryQueryFilter = {
                pageNum: 1,
                size: 5
            };
        });
    }

    function getAllJobBoards(successCallback) {
        resumeService.getAllSourceTypes(function (data) {
            if (data.length > 0) {
                vm.jobBoards = data;
                if (successCallback) {
                    successCallback();
                }
            }
        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }

    function getJobBoardsSearchHistory() {
        jobService.getJobBoardsSearchHistory(vm.jobId, function (data) {
            vm.searchHistory = data;
            _parseSearch();
        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error, 'danger');
            }
        });
    }

    function _parseSearch() {
        angular.forEach(vm.searchHistory, function (search, key) {
            search.jobBoards = angular.copy(_addJobBoardDetails(search.jobBoardId, search.daxtraParameters));
            delete search.jobBoardId;
        });
        console.log('parsed search history');
        console.log(vm.searchHistory);
    }

    function _addJobBoardDetails(jobBoardIds, daxtraParameters) {
        var jobBoards = [];
        angular.forEach(jobBoardIds, function (jobBoardId, key) {
            var jobBoard = vm.jobBoards.find(function (jobBoard) {
                return jobBoard.id === jobBoardId;
            });
            //Parse daxtra params
            // jobBoard.allDaxtraParameters = angular.copy(_addDaxtraParametersToJobBoard(daxtraParameters, jobBoard.daxtraCode.toLowerCase()));
            // jobBoard.daxtraParameters = jobBoard.allDaxtraParameters.filter(function (param) {
            //     return param.value !== "";
            // });
            jobBoards.push(jobBoard);
        });
        return jobBoards;
    }

    // function _addDaxtraParametersToJobBoard(daxtraParameters, jobBoardCode) {
    //     parsedParameters = [];
    //     var parameterPrefix = "jb_" + jobBoardCode + "_";
    //     if (!_.isNull(daxtraParameters)) {
    //         angular.forEach(daxtraParameters, function (value, parameterName) {
    //             if (parameterName.includes(parameterPrefix)) {
    //                 var formattedParameter = parameterName.replace(parameterPrefix, "").replace(/_/g, " ");
    //                 parsedParameters.push({ name: formattedParameter, value: value });
    //             }
    //         });
    //     }
    //     return parsedParameters;
    // }

    function searchHistoryContainer() {
        this.searchHistory = [];
        this.busy = false;
        this.searchHistoryExistFlag = true;
        this.currentPromise = null;
        this.disableInfiniteScroll = false;

        this.getNext = function () {
            var parent = this;
            if (this.busy) return;
            this.busy = true;

            this.currentPromise = jobService.getJobBoardsSearchHistory(vm.jobId, this.searchHistoryQueryFilter.pageNum, this.searchHistoryQueryFilter.size, function (data) {
                if (angular.isDefined(data) && data != 'user cancellation') {
                    if (data.length > 0) {
                        angular.forEach(data, function(search, key) {
                            search.jobBoards = angular.copy(_addJobBoardDetails(search.jobBoardId, search.daxtraParameters));
                            delete search.jobBoardId;
                            parent.searchHistory.push(search);
                        });
                        console.log('Parsed search history');
                        console.log(parent.searchHistory);
                        if (parent.searchHistoryQueryFilter.pageNum == 1) {
                            $('.panel-group').css({
                                "height": "500px",
                                "overflow-y": "scroll"
                            });
                        }

                        if (data.length < parent.searchHistoryQueryFilter.size) {
                            parent.disableInfiniteScroll = true;
                        } else {
                            parent.searchHistoryQueryFilter.pageNum++;
                        }
                    } else {
                        parent.disableInfiniteScroll = true;
                    }
                    if (parent.searchHistory.length > 0) {
                        parent.searchHistoryExistFlag = true;
                    } else {
                        parent.searchHistoryExistFlag = false;
                    }
                }
                parent.busy = false;

            }, function (error) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            });
        }
    }
}

JobBoardsSearchHistoryController.$inject = ['$rootScope', 'jobService', 'resumeService', 'alertsAndNotificationsService'];

fourdotfivedirectives.controller('JobBoardsSearchHistoryController', JobBoardsSearchHistoryController);