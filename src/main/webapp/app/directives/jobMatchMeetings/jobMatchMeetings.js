fourdotfivedirectives.directive('jobMatchMeetings', function jobMatchMeetings() {
    return {
        restrict: 'E',
        scope: {},
        bindToController: {
            jobMatchId: '@',
            meetingType: '@',
            saveCallback: '&'
        },
        controller: 'JobMatchMeetingsController',
        controllerAs: 'jobMatchMeetings',
        templateUrl: 'app/directives/jobMatchMeetings/job-match-meetings.html'
    }
});