/*
  * This is the controller for the job match meetings directive
 */

function JobMatchMeetingsController($uibModal, $filter, moment, dateTimeUtilityService, DTOptionsBuilder, DTColumnDefBuilder, jobMatchService, alertsAndNotificationsService) {
    var vm = this;

    //variables
    vm.meetingsList = [];
    vm.meetingUserType = '';
    vm.browserTimeZone = null;
    //flags
    vm.loadingFlag = true;
    //methods
    vm.init = init;
    vm.getScheduledItems = getScheduledItems;
    vm.openFeedbackForm = openFeedbackForm;
    vm.feedbackCallback = feedbackCallback;

    vm.init();

    function init() {
        vm.dtOptions = DTOptionsBuilder.newOptions().withOption('paging', false).withOption('ordering',false);
        vm.dtColumnDefs = [];
        vm.browserTimeZone = dateTimeUtilityService.guessBrowserTimeZoneName();
        if(vm.meetingType == 'PhoneScreen'){
            vm.meetingUserType = 'Phone Screener';
        }else if(vm.meetingType == 'Interview'){
            vm.meetingUserType = 'Interviewer';
        }
        //get the candidate job match meetings
        vm.loadingFlag = true;
        vm.getScheduledItems();
    }

    function getScheduledItems(){
        vm.loadingFlag = true;
        if(vm.meetingType == 'PhoneScreen'){
            jobMatchService.getPhoneScreeners(vm.jobMatchId, true, function (data) {
                vm.loadingFlag = false;
                vm.meetingsList = _getMeetingsList(data);
            }, function (error) {
                vm.loadingFlag = false;
                if ($rootScope.isOnline) {
                    alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                }
            });
        }else if(vm.meetingType == 'Interview'){
            jobMatchService.getInterviewers(vm.jobMatchId, true, function (data) {
                vm.loadingFlag = false;
                vm.meetingsList = _getMeetingsList(data);
            }, function (error) {
                vm.loadingFlag = false;
                if ($rootScope.isOnline) {
                    alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                }
            });
        }
    }

    function _getMeetingsList(response){
        var meetingsList = [];
        angular.forEach(response, function (meetingDetails, key) {
            var startDateTime = new Date(meetingDetails.meetingStartTime);
            var endDateTime = new Date(meetingDetails.meetingEndTime);
            angular.forEach(meetingDetails.participants, function (meetingUser, k) {
                var meeting = {};
                meeting.id = meetingDetails.meetingScheduleId;
                meeting.userId = meetingUser.id;
                meeting.userName = meetingUser.firstName;
                if(!_.isUndefined(meetingUser.lastName) && !_.isNull(meetingUser.lastName)){
                    meeting.userName += ' ' + meetingUser.lastName;
                }
                meeting.status = meetingDetails.status;
                meeting.role = meetingUser.role;
                meeting.email = _.isNull(meetingUser.email) ? '-' : meetingUser.email;
                meeting.phoneNumber = _.isNull(meetingUser.phoneNumber) ? '-' : meetingUser.phoneNumber;
                meeting.title = _.isNull(meetingUser.title) ? '-' : meetingUser.title;
                meeting.feedbackTooltip = 'Click to view feedback.';
                switch(meeting.status){
                    case 'SCHEDULED':
                        meeting.statusClass = 'fa fa-calendar meeting-icon-scheduled';
                        meeting.statusLabel = 'Scheduled';
                        meeting.feedbackEnabledFlag = true;
                        break;
                    case 'IN_PROGRESS':
                        meeting.statusClass = 'fa fa-hourglass-half meeting-icon-in-progress';
                        meeting.statusLabel = 'In Progress';
                        meeting.feedbackEnabledFlag = true;
                        break;
                    case 'COMPLETED':
                        meeting.statusClass = 'fa fa-check meeting-icon-completed';
                        meeting.statusLabel = 'Completed';
                        meeting.feedbackEnabledFlag = true;
                        break;
                    case 'CANCELLED':
                        meeting.statusClass = 'fa fa-ban meeting-icon-cancelled';
                        meeting.statusLabel = 'Canceled';
                        meeting.feedbackEnabledFlag = false;
                        meeting.feedbackTooltip = 'Feedback is disabled.';
                        break;
                    default:
                        // set defaults
                        meeting.statusClass = 'fa fa-question meeting-icon-in-progress';
                        meeting.statusLabel = 'Status Unknown';
                        meeting.feedbackEnabledFlag = false;
                        meeting.feedbackTooltip = 'Feedback is disabled.';
                }
                meeting.timeZone = _.isNull(meetingDetails.timeZone) ? '-' : meetingDetails.timeZone;
                if(meeting.timeZone != '-'){
                    meeting.scheduledDateInBrowserTimeZone = startDateTime;
                    meeting.scheduledTimeLabelInBrowserTimeZone = $filter('fourdotfiveTimeFormat')(startDateTime) + ' to ' + $filter('fourdotfiveTimeFormat')(endDateTime);
                    var startDateTimeStringInSelectedTimeZone = moment(startDateTime).tz(meeting.timeZone).format("YYYY-MM-DDTHH:mm:ss.SSS");
                    var startDateTimeInSelectedTimeZone = new Date(startDateTimeStringInSelectedTimeZone);
                    var endDateTimeStringInSelectedTimeZone = moment(endDateTime).tz(meeting.timeZone).format("YYYY-MM-DDTHH:mm:ss.SSS");
                    var endDateTimeInSelectedTimeZone = new Date(endDateTimeStringInSelectedTimeZone);
                    meeting.scheduledDate = new Date(startDateTimeInSelectedTimeZone);
                    meeting.scheduledTimeLabel = $filter('fourdotfiveTimeFormat')(startDateTimeInSelectedTimeZone) + ' to ' + $filter('fourdotfiveTimeFormat')(endDateTimeInSelectedTimeZone);
                }else{
                    meeting.scheduledDate = startDateTime;
                    meeting.scheduledTimeLabel = $filter('fourdotfiveTimeFormat')(startDateTime) + ' to ' + $filter('fourdotfiveTimeFormat')(endDateTime);
                }
                meetingsList.push(meeting);
            });
        });
        return meetingsList;
    }

    function openFeedbackForm(userId){
        $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'app/directives/jobMatchMeetings/job-match-meetings-feedback-modal.html',
            backdrop: 'static',
            keyboard: false,
            bindToController: true,
            controller: ['$uibModalInstance','jobMatchId','feedbackType','saveCallback','meetingUserId', function ($uibModalInstance, jobMatchId, feedbackType, saveCallback, meetingUserId) {
                var vm = this;
                vm.jobMatchId = jobMatchId;
                vm.feedbackType = feedbackType;
                vm.meetingUserId = meetingUserId;
                vm.title = 'Interview Feedback';
                if(vm.meetingType == 'PhoneScreen'){
                    vm.title = 'Phone Screen Feedback';
                }
                vm.closeModal = function () {
                    $uibModalInstance.dismiss('cancel');
                };
                vm.saveCallback = function(){
                    $uibModalInstance.dismiss('cancel');
                    saveCallback();
                };
            }],
            controllerAs: 'feedbackModal',
            size: 'lg',
            resolve:{
                jobMatchId: function () {
                    return vm.jobMatchId;
                },
                saveCallback: function () {
                    return vm.feedbackCallback;
                },
                feedbackType: function(){
                    return vm.meetingType;
                },
                meetingUserId: function () {
                    return userId;
                }
            }
        });
    }

    function feedbackCallback(){
        vm.init();
        if(vm.saveCallback){
            vm.saveCallback();
        }
    }
}

JobMatchMeetingsController.$inject = ['$uibModal', '$filter', 'moment', 'dateTimeUtilityService', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'jobMatchService', 'alertsAndNotificationsService'];

fourdotfivedirectives.controller('JobMatchMeetingsController', JobMatchMeetingsController);