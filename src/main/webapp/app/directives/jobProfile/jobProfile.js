fourdotfivedirectives.directive('jobProfile', function jobProfile() {
    return {
        restrict: 'E',
        scope: {
            'jobId': '@'
        },
        bindToController: true,
        controller:'JobProfileController',
        controllerAs: 'jobProfile',
        templateUrl: 'app/directives/jobProfile/job-profile.html'
    }
});