/*
  * This is the controller for the job profile directive
 */

function JobProfileController(jobService, JobProfile) {
    var vm = this;

    //variables
    vm.job = null;
    //flags
    vm.loadingFlag = true;
    //methods
    vm.init = init;
    vm.getJobProfile = getJobProfile;

    function init(){
        vm.job = new JobProfile();
        //get the candidate profile
        vm.getJobProfile();
    }

    function getJobProfile(){
        console.log('job id -', vm.jobId);
        jobService.getJobProfile(vm.jobId, function (data) {
            _.mergeWith(vm.job, data, function (objValue, srcValue) {
                return _.isNull(srcValue) ? objValue : undefined;
            });
            vm.loadingFlag = false;
        }, function (error) {
            console.log('candidate get error - ', error);
            // do nothing.
        });
    }

    vm.init();
}

JobProfileController.$inject = ['jobService','JobProfile'];

fourdotfivedirectives.controller('JobProfileController', JobProfileController);