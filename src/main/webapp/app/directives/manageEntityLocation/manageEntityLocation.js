fourdotfivedirectives.directive('manageEntityLocation',function () {
    return{
        restrict: 'E',
        controller: 'ManageEntityLocationController',
        controllerAs: 'manageLocation',
        bindToController: true,
        scope:{
            entityType: '@',
            entityId: '@',
            locationObject: '=',
            saveCallback: '&',
            cancelCallback: '&'
        },
        templateUrl: 'app/directives/manageEntityLocation/manage-entity-location.html'
    };
});