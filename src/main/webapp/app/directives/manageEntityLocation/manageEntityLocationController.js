/**
 * This is a controller for the manageEntityLocation directive
 */

function ManageEntityLocationController($timeout, $rootScope, jobService, candidateService, MESSAGECONSTANTS){
    var vm = this;

    vm.location = null;
    vm.geoLocations = [];
    vm.MESSAGECONSTANTS = {};
    //flags
    vm.isZipCodeValidFlag = true;
    vm.doesCountyExistFlag = false;
    vm.savingFlag = false;
    vm.cancellingFlag = false;
    //methods
    vm.init = init;
    vm.getZipCodeDetails = getZipCodeDetails;
    vm.getAddressDetails = getAddressDetails;
    vm.saveLocation = saveLocation;
    vm.cancelLocationUpdate = cancelLocationUpdate;

    vm.init();

    function init(){
        vm.MESSAGECONSTANTS = MESSAGECONSTANTS;
        if(_.isNull(vm.locationObject)){
            vm.location = new _getLocationObject();
        }else{
            vm.location = vm.locationObject;
            vm.location.zipCode = angular.copy(vm.location.zipcode);
            delete vm.location.zipcode;
            $timeout(function(){
                vm.getZipCodeDetails();
            }, 100);
        }
    }

    function _getLocationObject(){
        this.address1;
        this.address2;
        this.city;
        this.county;
        this.region;
        this.municipality;
        this.state;
        this.zipCode;
    }

    function saveLocation(){
        if(!vm.locationForm.$invalid){
            vm.savingFlag = true;
            var locationObject = angular.copy(vm.location);
            if(vm.entityType == 'job'){
                locationObject.jobId = vm.entityId;
                jobService.updateJobLocation(locationObject, function(data){
                    vm.savingFlag = false;
                    if(!_.isUndefined(vm.saveCallback)){
                        vm.saveCallback({locationObject: locationObject});
                    }
                }, function(error){
                    vm.savingFlag = false;
                });
            }else if(vm.entityType == 'candidate'){
                var contactObject = {};
                contactObject.candidateId = vm.entityId;
                contactObject.companyId = $rootScope.userDetails.company.companyId;
                contactObject.address = locationObject;
                contactObject.address.zipcode = contactObject.address.zipCode;
                contactObject.address.region = null;
                contactObject.address.municipality = null;
                candidateService.addUpdateCandidateAddress(contactObject, function(data){
                    vm.savingFlag = false;
                    if(!_.isUndefined(vm.saveCallback)){
                        vm.saveCallback({contactObject: contactObject});
                    }
                }, function(error){
                    vm.savingFlag = false;
                });
            }

        }
    }

    function _resetLocationObject(){
        $timeout(function(){
            vm.location.city = '';
            vm.location.state = '';
            vm.location.county = '';
            vm.location.country = '';
            vm.doesCountyExistFlag = false;
        }, 100);
    }

    function cancelLocationUpdate(){
        if(!_.isUndefined(vm.cancelCallback)){
            vm.cancelCallback();
        }
    }

    function getZipCodeDetails() {
        if(!vm.locationForm.zipCode.$invalid) {
            vm.getAddressDetails(vm.location.zipCode);
        } else {
            vm.geoLocations = [];
            vm.doesCountyExistFlag = false;
            _resetLocationObject();
        }
    }

    function getAddressDetails(zipCode) {
        if (angular.isDefined(vm.locationForm.zipCode) && vm.locationForm.zipCode.$valid) {
            vm.isZipCodeValidFlag = true;
        } else {
            vm.isZipCodeValidFlag = false;
        }
        var locations = [];

        if (angular.isDefined(zipCode) && !angular.equals(zipCode, '')) {
            $.getJSON({
                url: 'https://maps.googleapis.com/maps/api/geocode/json?address=' + zipCode + '&key=' + MESSAGECONSTANTS.GEOCODING.API_KEY,
                data: {
                    sensor: false
                },
                success: function (data, textStatus) {
                    var createGeoLocation = function (addressdetails, postalname) {
                        var addressobj = {};
                        for (var p = 0; p < addressdetails.address_components.length; p++) {
                            for (var t = 0; t < addressdetails.address_components[p].types.length; t++) {
                                switch (addressdetails.address_components[p].types[t]) {
                                    case 'country':
                                        addressobj.country = addressdetails.address_components[p].long_name;
                                        break;
                                    case 'administrative_area_level_2':
                                        addressobj.county = addressdetails.address_components[p].long_name;
                                        break;
                                    case 'administrative_area_level_1':
                                        addressobj.state = addressdetails.address_components[p].long_name;
                                        break;
                                    case 'locality':
                                        if (angular.isDefined(postalname) && postalname !== '') {
                                            addressobj.city = postalname;
                                        } else {
                                            addressobj.city = addressdetails.address_components[p].long_name;
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }

                        if (angular.isUndefined(addressobj.city) || angular.isUndefined(addressobj.state) || angular.isUndefined(addressobj.country)) {
                            for (var p = 0; p < addressdetails.address_components.length; p++) {
                                for (var t = 0; t < addressdetails.address_components[p].types.length; t++) {
                                    switch (addressdetails.address_components[p].types[t]) {
                                        case 'locality':
                                            if (angular.isUndefined(addressobj.state)) {
                                                addressobj.state = addressdetails.address_components[p].long_name;
                                            }
                                            if (angular.isUndefined(addressobj.country)) {
                                                addressobj.country = addressdetails.address_components[p].long_name;
                                            }
                                            break;
                                        case 'administrative_area_level_2':
                                            if (angular.isUndefined(addressobj.city)) {
                                                if (angular.isDefined(postalname) && postalname !== '') {
                                                    addressobj.city = postalname;
                                                } else {
                                                    addressobj.city = addressdetails.address_components[p].long_name;
                                                }
                                            }
                                            if (angular.isUndefined(addressobj.state)) {
                                                addressobj.state = addressdetails.address_components[p].long_name;
                                            }
                                            if (angular.isUndefined(addressobj.country)) {
                                                addressobj.country = addressdetails.address_components[p].long_name;
                                            }

                                            break;
                                        case 'administrative_area_level_3':
                                            if (angular.isUndefined(addressobj.city)) {
                                                if (angular.isDefined(postalname) && postalname !== '') {
                                                    addressobj.city = postalname;
                                                } else {
                                                    addressobj.city = addressdetails.address_components[p].long_name;
                                                }
                                            }
                                            break;
                                        case 'administrative_area_level_4':
                                            if (angular.isUndefined(addressobj.city)) {
                                                if (angular.isDefined(postalname) && postalname !== '') {
                                                    addressobj.city = postalname;
                                                } else {
                                                    addressobj.city = addressdetails.address_components[p].long_name;
                                                }
                                            }
                                            break;
                                        case 'sublocality' || 'sublocality_level_1' || 'sublocality_level_5':
                                            if (angular.isDefined(postalname) && postalname !== '') {
                                                addressobj.city = postalname;
                                            } else {
                                                addressobj.city = addressdetails.address_components[p].long_name;
                                            }
                                            break;
                                        case 'administrative_area_level_1':
                                            if (angular.isUndefined(addressobj.city)) {
                                                if (angular.isDefined(postalname) && postalname !== '') {
                                                    addressobj.city = postalname;
                                                } else {
                                                    addressobj.city = addressdetails.address_components[p].long_name;
                                                }

                                                if (angular.isUndefined(addressobj.country)) {
                                                    addressobj.country = addressdetails.address_components[p].long_name;
                                                }
                                            }
                                            break;
                                    }
                                }
                            }
                        }
                        return addressobj;
                    };

                    var checkCountyExists = function (locations, company) {
                        for (var i = 0; i < locations.length; i++) {
                            if (angular.equals(locations[i].city, company.city) &&
                                angular.isDefined(locations[i].county) && (locations.county !== null) &&
                                angular.isDefined(company.county) &&
                                (company.county !== null) && angular.equals(locations[i].county, company.county)) {
                                return true;
                            }
                        }
                        return false;
                    };


                    if (data.status === 'OK') {
                        for (var i = 0; i < data.results.length; i++) {
                            if (angular.isDefined(data.results[i].postcode_localities) && (data.results[i].postcode_localities.length > 0)) {
                                for (var k = 0; k < data.results[i].postcode_localities.length; k++) {
                                    if ((data.results[i].types[0] === 'postal_code')) {
                                        console.log('entered zipCode is a postal code');
                                        var location = createGeoLocation(data.results[i], data.results[i].postcode_localities[k]);
                                        locations.push(angular.copy(location));
                                    } else {
                                        vm.isZipCodeValidFlag = false;
                                        console.log('entered zipCode is not a postal code');
                                    }
                                }
                            } else {
                                if ((data.results[i].types[0] === 'postal_code')) {
                                    console.log('entered zipCode is a postal code');
                                    var location = createGeoLocation(data.results[i]);
                                    locations.push(angular.copy(location));
                                } else {
                                    vm.isZipCodeValidFlag = false;
                                    console.log('entered zipCode is not a postal code');
                                }
                            }
                        }
                        vm.temp = locations;
                        if (locations.length) {
                            vm.temp.city = locations[0].city;
                            vm.temp.state = locations[0].state;
                            vm.temp.county = (angular.isUndefined(locations[0].county)) ? null : locations[0].county;
                            if (angular.isDefined(locations[0].county) && (locations[0].county !== null)) {
                                vm.doesCountyExistFlag = true;
                            } else {
                                vm.doesCountyExistFlag = false;
                            }
                            vm.temp.country = locations[0].country;
                        }

                        if(locations.length){
                            vm.isZipCodeValidFlag = true;
                            $timeout(function(){
                                vm.location.city = locations[0].city;
                                vm.location.state = locations[0].state;
                                vm.location.county = angular.isUndefined(locations[0].county) ? null : locations[0].county;
                                vm.location.country = locations[0].country;
                                vm.location.city = locations[0].city;
                            }, 100);
                        }else{
                            _resetLocationObject();
                        }
                    } else if (data.status === 'ZERO_RESULTS') {
                        vm.isZipCodeValidFlag = false;
                        _resetLocationObject();

                        // if (!cancelClicked) {
                        //     console.log('if ');
                        //     vm.isZipCodeValidFlag = false;
                        //     if (!vm.invalidZipcode) {
                        //         vm.invalidZipcode = true;
                        //     }
                        //     locations = [];
                        //     vm.location.city = '';
                        //     vm.location.state = '';
                        //     vm.location.county = '';
                        //     vm.doesCountyExistFlag = false;
                        //     vm.location.country = '';
                        //     vm.zipcodeCallCompleted = true;
                        // } else {
                        //     locations = angular.copy(vm.copyOfGeoLocations);
                        //     vm.geoLocations = angular.copy(vm.copyOfGeoLocations);
                        //     vm.geoLocationscopy = angular.copy(vm.copyOfGeoLocations);
                        //     vm.location.city = vm.originalStepOne.city;
                        //     vm.location.state = vm.originalStepOne.addressState;
                        //     vm.location.county = vm.originalStepOne.county;
                        //     if (checkCountyExists(angular.copy(locations), angular.copy(vm.originalStepOne))) {
                        //         vm.doesCountyExistFlag = true;
                        //     } else {
                        //         vm.doesCountyExistFlag = false;
                        //     }
                        //     vm.location.country = vm.originalStepOne.country;
                        //     vm.zipcodeCallCompleted = true;
                        // }
                    }
                    vm.geoLocations = locations;
                    if (!vm.geoLocations.length) {
                        vm.isZipCodeValidFlag = false;
                        _resetLocationObject();
                    }
                },
                error: function (data) {
                    vm.invalidZipcode = true;
                    vm.doesCountyExistFlag = false;
                }
            });
        } else {
            vm.geoLocations = [];
            vm.doesCountyExistFlag = false;
        }
    }
}

ManageEntityLocationController.$inject = ['$timeout','$rootScope','jobService','candidateService','MESSAGECONSTANTS'];

fourdotfivedirectives.controller('ManageEntityLocationController',ManageEntityLocationController);