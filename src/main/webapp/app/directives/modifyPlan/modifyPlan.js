fourdotfivedirectives.directive('modifyPlan', function modifyPlan(){
   return {
       restrict: 'E',
       scope: {},
       bindToController:{
           selectedPlan: '=',
           selectedPlanName: '=',
           planTypeList: '=',
           isModifyPlan: '=',
           saveCallback: '&',
           cancelCallback: '&'
       },
       controller: 'ModifyPlanController',
       controllerAs: 'modifyPlan',
       templateUrl: 'app/directives/modifyPlan/modify-plan.html'
   }
});