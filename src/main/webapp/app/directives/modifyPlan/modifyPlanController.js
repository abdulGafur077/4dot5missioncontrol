/*
    This is the controller for Modify Plan Directive
 */

function ModifyPlanController($rootScope, $filter, planService, dateTimeUtilityService, alertsAndNotificationsService){
    var vm = this;

    // variables
    // flags
    vm.isTrialPlan = false;
    // methods
    vm.init = init;
    vm.cancel = cancel;
    vm.savePlan = savePlan;
    
    vm.initializeStartAndEndDates = initializeStartAndEndDates;
    vm.onPlanTypeChange = onPlanTypeChange;

    vm.init();

    function init() {
        vm.initializeStartAndEndDates();
        vm.selectedPlanType = vm.selectedPlan.planHeaderDto.planType; 
        if(vm.selectedPlanName.toLowerCase() == "trial"){
            vm.isTrialPlan = true;
        }
        vm.selectedPlan.planDetailDto.taAdded = 0;
        vm.selectedPlan.planDetailDto.jbiAdded = 0;
    } 

    function initializeStartAndEndDates() {
        $(document).ready(function() {
            var currentDate = vm.selectedPlan.planHeaderDto.unformattedStartDate;
            var momentOfDefaultEndDate = moment(vm.selectedPlan.planHeaderDto.unformattedEndDate);
            $('#effectiveStartDate').datepicker({    
                format : "M d, yyyy",
                weekStart: 1,
                autoclose: true
            });
            $("#effectiveStartDate").datepicker("setDate", currentDate);
            $("#effectiveStartDate").datepicker("update", $filter('fourdotfiveDateFormat')(currentDate));
    
            $('#effectiveEndDate').datepicker({
                format : "M d, yyyy",
                weekStart: 1,
                autoclose: true
            });
            $("#effectiveEndDate").datepicker("setDate", momentOfDefaultEndDate.toDate());
            $("#effectiveEndDate").datepicker("update", $filter('fourdotfiveDateFormat')(momentOfDefaultEndDate.toDate()));
        });
 
    }

    function onPlanTypeChange() {
        console.log(vm.selectedPlan.planHeaderDto.planType);
        var currentDate = vm.selectedPlan.planHeaderDto.unformattedStartDate;
        if (vm.selectedPlan.planHeaderDto.planType == "Month to Month") {
            vm.selectedPlan.planHeaderDto.planEndDateDisplay = $filter('fourdotfiveDateFormat')(moment(currentDate).add(1, 'M').toDate());
            vm.selectedPlan.planHeaderDto.unformattedEndDate = moment(currentDate).add(1, 'M').toDate();
            vm.selectedPlan.planHeaderDto.planEndDate = dateTimeUtilityService.getDateTimeInAPIFormat(new Date(vm.selectedPlan.planHeaderDto.unformattedEndDate));
        }
        else {
            vm.selectedPlan.planHeaderDto.planEndDateDisplay = $filter('fourdotfiveDateFormat')(moment(currentDate).add(6, 'M').toDate());
            vm.selectedPlan.planHeaderDto.unformattedEndDate = moment(currentDate).add(6, 'M').toDate();
            vm.selectedPlan.planHeaderDto.planEndDate = dateTimeUtilityService.getDateTimeInAPIFormat(new Date(vm.selectedPlan.planHeaderDto.unformattedEndDate));
        }

        $("#effectiveEndDate").datepicker("setDate", vm.selectedPlan.planHeaderDto.unformattedEndDate);
        $("#effectiveEndDate").datepicker("update", $filter('fourdotfiveDateFormat')(vm.selectedPlan.planHeaderDto.unformattedEndDate));
    }

    function cancel() {
        if (vm.cancelCallback) {
            vm.cancelCallback();
        }
    }

    function savePlan() {
        vm.selectedPlan.planHeaderDto.unformattedEndDate = moment(vm.selectedPlan.planHeaderDto.planEndDateDisplay).toDate();
        vm.selectedPlan.planHeaderDto.planEndDate = dateTimeUtilityService.getDateTimeInAPIFormat(new Date(vm.selectedPlan.planHeaderDto.unformattedEndDate));
        vm.selectedPlan.planHeaderDto.unformattedStartDate = moment(vm.selectedPlan.planHeaderDto.planStartDateDisplay).toDate();
        vm.selectedPlan.planHeaderDto.planStartDate = dateTimeUtilityService.getDateTimeInAPIFormat(new Date(vm.selectedPlan.planHeaderDto.unformattedStartDate));
        if (vm.isTrialPlan) {
            vm.selectedPlan.planDetailDto.taAllocated = vm.selectedPlan.planDetailDto.taAllocated + vm.selectedPlan.planDetailDto.taAdded;
            vm.selectedPlan.planDetailDto.jbiAllocated = vm.selectedPlan.planDetailDto.jbiAllocated + vm.selectedPlan.planDetailDto.jbiAdded;
        }

        planService.savePlan(vm.selectedPlan, function(data) {
            vm.selectedPlan = data;
            var successMessage = "";
            if (vm.isModifyPlan) {
                successMessage = "Company Plan modified successfully";
            } else {
                successMessage = "Company Plan chosen successfully";
            }
            alertsAndNotificationsService.showBannerMessage(successMessage, 'success');
            if (vm.saveCallback) {
                vm.saveCallback();
            }
        }, function(error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }
 
}

ModifyPlanController.$inject = ['$rootScope','$filter', 'planService', 'dateTimeUtilityService','alertsAndNotificationsService'];

fourdotfivedirectives.controller('ModifyPlanController', ModifyPlanController);