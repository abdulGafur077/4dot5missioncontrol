fourdotfivedirectives.directive('moveJobMatchStep', function moveJobMatchStep() {
    return {
        restrict: 'E',
        scope: {},
        bindToController: {
            jobMatchId: '@',
            moveType: '@',
            candidateId: '@',
            label: '@',
            saveCallback: '&'
        },
        controller: 'MoveJobMatchStepController',
        controllerAs: 'moveJobMatchStep',
        templateUrl: 'app/directives/moveJobMatchStep/move-job-match-step.html'
    }
});