/*
  * This is the controller for the set job match status directive
 */

function MoveJobMatchStepController($rootScope, jobMatchService) {
    var vm = this;

    //variables
    vm.moveData = {
        jobMatchId: null,
        notes: ''
    };
    vm.moveTypeLabel = '';
    vm.candidateJobMatch = {};
    //flags
    vm.savingFlag = false;
    //methods
    vm.init = init;
    vm.moveJobMatchStep = moveJobMatchStep;
    vm.getCandidateJobMatchCardInfo = getCandidateJobMatchCardInfo;

    vm.init();

    function init() {
        vm.userId = $rootScope.userDetails.id;
        vm.moveData.jobMatchId = vm.jobMatchId;
        vm.moveTypeLabel = vm.label.split(" to ")[1];
        // do not add default notes if the action is moving back to a previous state
        if(vm.label.indexOf('back') == -1){
            vm.moveData.notes = 'Moving candidate to ' + vm.moveTypeLabel;
        }
        vm.labelLoadingFlag = true;
        vm.getCandidateJobMatchCardInfo(function () {
            vm.labelLoadingFlag = false;
        });
    }

    function getCandidateJobMatchCardInfo(successCallback){
        jobMatchService.getCandidateJobMatchCard(vm.jobMatchId, function (data) {
            angular.merge(vm.candidateJobMatch,data);
            vm.candidateJobMatch = _massageCandidateJobMatchData(vm.candidateJobMatch);
            if(successCallback){
                successCallback();
            }
        }, function (error) {
            console.log('candidate get error - ', error);
            // do nothing.
        });
    }

    function _massageCandidateJobMatchData(candidateJobMatch){
        candidateJobMatch.name = candidateJobMatch.firstName + ' ' + candidateJobMatch.lastName;
        return candidateJobMatch;
    }

    function moveJobMatchStep() {
        if(!vm.moveJobMatchStepForm.$invalid){
            vm.savingFlag = true;
            if(vm.moveType == 'Forward'){
                jobMatchService.moveStepForward(vm.moveData,function (data) {
                    vm.savingFlag = false;
                    if(!_.isUndefined(vm.saveCallback)){
                        vm.saveCallback();
                    }
                },function (error) {
                    // do nothing
                });
            }else if(vm.moveType == 'Backward'){
                jobMatchService.moveStepBackward(vm.moveData,function (data) {
                    vm.savingFlag = false;
                    if(!_.isUndefined(vm.saveCallback)){
                        vm.saveCallback();
                    }
                },function (error) {
                    // do nothing
                });
            }else{
                // do nothing
            }
        }
    }
}

MoveJobMatchStepController.$inject = ['$rootScope','jobMatchService'];

fourdotfivedirectives.controller('MoveJobMatchStepController', MoveJobMatchStepController);