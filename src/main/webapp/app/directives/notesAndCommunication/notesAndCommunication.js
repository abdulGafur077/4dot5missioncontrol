fourdotfivedirectives.directive('notesAndCommunication', function notesAndCommunication() {
    return {
        restrict: 'E',
        scope: {
            'jobMatchId': '@',
            // type can be - notes, communication
            'type': '@',
            'saveCallback': '&'
        },
        bindToController: true,
        controller:'NotesAndCommunicationController',
        controllerAs: 'notes',
        templateUrl: 'app/directives/notesAndCommunication/notes-and-communication.html'
    }
});