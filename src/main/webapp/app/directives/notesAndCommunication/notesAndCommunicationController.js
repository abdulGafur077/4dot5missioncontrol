/*
  * This is the controller for the job profile directive
 */

function NotesAndCommunicationController($rootScope, $timeout, jobMatchService, ContactNote, alertsAndNotificationsService) {
    var vm = this;

    //variables
    vm.contactNotes = [];
    vm.newContactNote = null;
    vm.userDetails = null;
    vm.communicationTypesList = [
        {
            "id": 1,
            "name": "Outgoing Call",
            "value": "Outgoing Call"
        },
        {
            "id": 2,
            "name": "Incoming Call",
            "value": "Incoming Call"
        },
        {
            "id": 3,
            "name": "Outgoing Email",
            "value": "Outgoing Email"
        },
        {
            "id": 4,
            "name": "Incoming Email",
            "value": "Incoming Email"
        }
    ];
    vm.durationTypesList = [
        {
            "id": 1,
            "name": "1 Minute",
            "value": "1"
        },
        {
            "id": 2,
            "name": "3 Minutes",
            "value": "3"
        },
        {
            "id": 3,
            "name": "5 Minutes",
            "value": "5"
        },
        {
            "id": 4,
            "name": "10 Minutes",
            "value": "10"
        },
        {
            "id": 5,
            "name": "15 Minutes",
            "value": "15"
        },
        {
            "id": 6,
            "name": "Other",
            "value": "Other"
        }
    ];
    //flags
    vm.loadingFlag = true;
    vm.savingFlag = false;
    vm.durationEnabledFlag = false;
    vm.displayType = 'notes';
    //methods
    vm.init = init;
    vm.intializeNewContactNote = intializeNewContactNote;
    vm.getNotesAndCommunications = getNotesAndCommunications;
    vm.saveContactNote = saveContactNote;
    vm.deleteContactNote = deleteContactNote;
    vm.communicationTypeChanged = communicationTypeChanged;

    function init(){
        vm.userDetails = $rootScope.userDetails;
        vm.intializeNewContactNote();
        _createSlimScroll();
        // set the type
        if(_.isNull(vm.type) || (!(_.isNull(vm.type)) && vm.type.toString().trim() == '')){
            vm.type = 'notes';
        }
        // set the display type
        if(vm.type == 'notes'){
            vm.displayType = 'Notes';
        }else{
            vm.displayType = 'Communication';
        }
        // get the notes and communications
        vm.getNotesAndCommunications(function () {
            //_createSlimScroll();
        });
    }

    function _createSlimScroll() {
        $timeout(function () {
            $('.conversation-inner').slimScroll({
                start: 'bottom',
                alwaysVisible: false,
                railVisible: false,
                wheelStep: 20,
                allowPageScroll: true
            });
        }, 70);

    }

    function _destroySlimScroll() {
        $(".conversation-inner").parent().replaceWith($(".conversation-inner"));
    }

    function intializeNewContactNote(){
        vm.newContactNote = new ContactNote();
        vm.newContactNote.jobMatchId = vm.jobMatchId;
        vm.newContactNote.user.id = $rootScope.userDetails.id;
    }

    function getNotesAndCommunications(successCallback){
        vm.loadingFlag = true;
        // make the contact notes array empty
        vm.contactNotes = [];
        jobMatchService.getNotesAndCommunications(vm.jobMatchId, function (data) {
            angular.forEach(data, function (val, key) {
                var tempContactNote = new ContactNote();
                angular.merge(tempContactNote,val);
                // set the user's full name
                tempContactNote.user.name = tempContactNote.user.firstName + ' ' + tempContactNote.user.lastName;
                tempContactNote.dateTime = new Date(tempContactNote.dateTime);
                vm.contactNotes.push(tempContactNote);
            });
            vm.loadingFlag = false;
            if(successCallback){
                successCallback();
            }
        }, function (error) {
            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
        });
    }
    
    function saveContactNote() {
        if(!vm.contactNoteForm.$invalid){
            vm.savingFlag = true;
            var contactNoteSaveObject = angular.copy(vm.newContactNote);
            // if communication is being saved with other duration, capture the value from other field
            if(vm.type == "communication" && !_.isNull(contactNoteSaveObject.duration) && contactNoteSaveObject.duration == 'Other'){
                contactNoteSaveObject.duration = vm.durationOtherValue;
            }
            jobMatchService.saveNoteOrCommunication(contactNoteSaveObject, function(data) {
                // reset the saving flag
                vm.savingFlag = false;
                // refresh the view
                vm.getNotesAndCommunications(function () {
                   // destroy old slim scroll
                   _destroySlimScroll();
                   // create slim scroll again
                   _createSlimScroll();
                });
                // reset the new contact note
                vm.intializeNewContactNote();
                // reset the form submitted value
                vm.contactNoteForm.$submitted = false;
                // reset duration other value
                if(vm.type == "communication"){
                    vm.durationOtherValue = '';
                }
                if(vm.saveCallback){
                    vm.saveCallback();
                }
            }, function (error) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            });
        }
    }

    function deleteContactNote(contactNote) {
        if(confirm('Are you sure you want to delete this note ?')){
            jobMatchService.deleteNoteOrCommunication(contactNote, function (data) {
                // refresh the view
                vm.getNotesAndCommunications(function () {
                    // destroy old slim scroll
                    _destroySlimScroll();
                    // create slim scroll again
                    _createSlimScroll();
                });
            }, function (error) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            });
        }
    }

    function communicationTypeChanged() {
        if(vm.newContactNote.contactMethod.toString().includes('Call')){
            vm.durationEnabledFlag = true;
        }else{
            vm.durationEnabledFlag = false;
        }
    }

    vm.init();
}

NotesAndCommunicationController.$inject = ['$rootScope','$timeout','jobMatchService','ContactNote','alertsAndNotificationsService'];

fourdotfivedirectives.controller('NotesAndCommunicationController', NotesAndCommunicationController);