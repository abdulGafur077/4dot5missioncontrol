fourdotfivedirectives.directive(
  "recentActivities",
  function recentActivities() {
    return {
      restrict: "E",
      scope: {},
      bindToController: {
        context: "@",
        candidateId: "@",
        jobMatchId: "@",
        requisitionId: "@",
        candidateId: "@",
        recentActivitiesControls: "=",
        activityMap: "=",
        showAllCallback: "&"
      },
      controller: "RecentActivitiesController",
      controllerAs: "recentActivities",
      templateUrl: "app/directives/recentActivities/recent-activities.html"
    };
  }
);
