/*
  * This is the controller for the recent activities directive
 */

function RecentActivitiesController(candidateService, requisitionService, companyService, alertsAndNotificationsService, CandidateActivityFilter) {
    var vm = this;

    //methods
    vm.init = init;
    vm.showAllActivity = showAllActivity;
    vm.getActivities = getActivities;
    vm.expandText = expandText;

    vm.init();

    function init() {
        if (!vm.activities) {
            vm.activities = [];
        }
        if (!vm.context) {
            vm.context = 'global';
        }
        if (_.isUndefined(vm.recentActivitiesControls)) {
            vm.recentActivitiesControls = {};
        }
        vm.recentActivitiesControls.getActivities = getActivities;
    }

    function getActivities() {

        var activityFilter = new CandidateActivityFilter();
        activityFilter.page = 1;
        activityFilter.size = 5;
        if (vm.context === "candidateCard" || vm.context === "candidateJobMatchCard") {
            activityFilter.candidateId = vm.candidateId;
            if (vm.context === "candidateJobMatchCard") {
                activityFilter.jobMatchId = vm.jobMatchId;
            }
            vm.activities = [];
            candidateService.getCandidateActivities(activityFilter, function (data) {
                angular.forEach(data, function (val, key) {
                    val.activityDisplayValue = vm.activityMap[val.activityType];
                    val.miniStatement = vm.activityMap[val.activityType];
                    val.createdBy = val.createdBy.trim();
                    val.statement = '';
                    if (val.activityType.includes("CHANGED") || val.activityType.includes("STATE_CHANGE")) {
                        val.activityType = 'change';
                        if (val.fromState.includes("Assessment") || val.toState.includes("Assessment")) {
                            val.activityDisplayValue = "Assessment Status Changed to <span class='emerald''>" + val.toState + "'</span> for role <span class='emerald'>'" + val.title+"'</span>";
                        }
                        else if (val.fromState.includes("Phone Screen") || val.toState.includes("Phone Screen")) {
                            val.activityDisplayValue = "Phone Screen Status Changed to <span class='emerald''>" + val.toState + "'</span> for role <span class='emerald'>'" + val.title + "'</span>";
                        } else if (val.fromState.includes("Interview") || val.toState.includes("Interview")) {
                            val.activityDisplayValue = "Interview Status Changed to <span class='emerald''>" + val.toState + "'</span> for role <span class='emerald'>'" + val.title + "'</span>";
                        }            
                    }
                    if (val.activityType.includes("STEP_CHANGE")) {
                        val.activityType = 'change';
                        val.activityDisplayValue = "Candidate Moved to <span class='emerald'>'" + val.toState + "'</span> for role <span class='emerald'>'" + val.title + "'</span>";
                    }
                    else if (val.activityType.includes("ADDED") || val.activityType.includes("REMINDER") || val.activityType.includes("COMMUNICATION")) {
                        val.activityType = 'add';
                        val.activityDisplayValue = val.activityDisplayValue + " for role <span class='emerald'>'" + val.title + "'</span>";
                    }
                    else if (val.activityType.includes("ASSIGNED")) {
                        val.activityType = 'assign';
                        val.activityDisplayValue = val.activityDisplayValue + " for role <span class='emerald'>'" + val.title + "'</span>";
                    } else if (val.activityType.includes("REMOVED") || val.activityType.includes("DELETED")) {
                        val.activityType = 'remove';
                        val.activityDisplayValue = val.activityDisplayValue + " for role <span class='emerald'>'" + val.title + "'</span>";
                    } else {
                    }
                    val.activityDisplayValue = val.activityDisplayValue.replace("Candidate Note", "Note");
                    val.activityDisplayValue = val.activityDisplayValue.replace("Matched Candidate Recruiter", "Recruiter");
                    val.activityDisplayValue = val.activityDisplayValue.replace("Candidate Communications", "Communication");
                    if (val.clientOrBuName !== null) {
                        val.activityDisplayValue = val.activityDisplayValue + " for <span class='emerald'>'" + val.clientOrBuName + "'</span>";
                        if (!_.isNull(val.clientOrBuLocation)) {
                            var companyLocationText = companyService.getParsedCompanyLocationText(val.clientOrBuLocation, true);
                            val.activityDisplayValue = val.activityDisplayValue + companyLocationText;
                        }
                    }
                    
                    val.activityDisplayValue = val.activityDisplayValue.replace("Matched Candidate State Changed", "Candidate State Changed");
                    val.displayDate = new Date(val.date);
                    val.showShortMessageFlag = true;
                    vm.activities.push(val);
                });
            }, function (error) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            });
        }
        if (vm.context === "requisitionCard") {
            requisitionService.getRequisitionActivity(vm.requisitionId, 1, 5, function (data) {
                angular.forEach(data, function (val, key) {
                    val.activityDisplayValue = vm.activityMap[val.activityType];
                    val.date = new Date(val.date);
                });
                vm.activities = data;
            }, function (error) {
                // do nothing
            });
        }
    }

    function expandText($event, activity) {
        if (activity.showShortMessageFlag) {
            activity.showShortMessageFlag = false;
        } else {
            activity.showShortMessageFlag = true;
        }
        $event.stopPropagation();
    }

    function showAllActivity() {
        if (vm.showAllCallback) {
            vm.showAllCallback();
        }
    }
}

RecentActivitiesController.$inject = ['candidateService', 'requisitionService', 'companyService', 'alertsAndNotificationsService', 'CandidateActivityFilter'];

fourdotfivedirectives.controller('RecentActivitiesController', RecentActivitiesController);