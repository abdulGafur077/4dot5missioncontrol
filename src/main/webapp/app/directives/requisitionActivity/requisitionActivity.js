fourdotfivedirectives.directive('requisitionActivity', function requisitionActivity() {
    return {
        restrict: 'E',
        scope: {},
        bindToController: {
            jobId: '@'
        },
        controller: 'RequisitionActivityController',
        controllerAs: 'reqActivity',
        templateUrl: 'app/directives/requisitionActivity/requisition-activity.html'
    }
});