/*
  * This is the controller for the requisition activities
 */

function RequisitionActivityController(requisitionService, jobService, moment, alertsAndNotificationsService) {
    var vm = this;

    //variables
    vm.activities = [];
    //flags
    vm.loadingFlag = true;
    //methods
    vm.init = init;
    vm.getRequisitionActivity = getRequisitionActivity;
    vm.getActivityTypes = getActivityTypes;

    vm.init();

    function init() {
        //get the requisition activity types and then requisition activities
        vm.getActivityTypes(function () {
            vm.getRequisitionActivity();
        });
    }

    function getRequisitionActivity() {
        requisitionService.getRequisitionActivity(vm.jobId, 1, 250, function (data) {
            angular.forEach(data, function (val, key) {
                val.activityDisplayValue = vm.activityMap[val.activityType];
                val.miniStatement = vm.activityMap[val.activityType];
                val.createdBy = val.createdBy.trim();
                // if(val.createdBy.trim() !== ""){
                //     val.miniStatement += " by " + val.createdBy;
                // }
                val.statement = '';
                if(val.activityType.includes("CHANGED")){
                    val.activityType = 'change';
                    val.statement = val.miniStatement + ' from ' + val.fromState + ' to ' + val.toState;
                }else if(val.activityType.includes("ADDED")){
                    val.activityType = 'add';
                    val.statement = val.miniStatement + ' - ' + val.toState;
                }else if(val.activityType.includes("REMOVED") || val.activityType.includes("DELETED")){
                    val.activityType = 'remove';
                    val.statement = val.miniStatement + ' - ' + val.toState;
                }else{
                    val.statement = val.miniStatement;
                }
                val.displayDate = new Date(val.date);
                vm.activities.push(val);
            });
            vm.loadingFlag = false;
        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }

    function getActivityTypes(successCallback){
        jobService.getActivityTypes(function (data) {
            vm.fullActivityList = data;
            vm.activityMap = [];
            angular.forEach(data, function (val, key) {
                vm.activityMap[val.value] = val.name;
            });
            successCallback();
        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }
}

RequisitionActivityController.$inject = ['requisitionService','jobService','moment', 'alertsAndNotificationsService'];

fourdotfivedirectives.controller('RequisitionActivityController', RequisitionActivityController);