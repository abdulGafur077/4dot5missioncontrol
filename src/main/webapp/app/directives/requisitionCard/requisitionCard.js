fourdotfivedirectives.directive('requisitionCard', function requisitionCard() {
    return {
        restrict: 'E',
        scope: {
            'requisitionInfo': '=',
            'activityMap': '='
        },
        bindToController: true,
        controller:'RequisitionCardController',
        controllerAs: 'req',
        templateUrl: 'app/directives/requisitionCard/requisition-card.html'
    }
});