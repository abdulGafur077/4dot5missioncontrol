/*
  * This is the controller for the requisition card directive
 */

function RequisitionCardController( $state, $timeout, $compile, $http, $uibModal,  $rootScope, genericService, StorageService, candidateService, alertsAndNotificationsService, jobService, CandidateCard, CandidateCardQueryFilter, CandidateActivityFilter, MESSAGECONSTANTS, REQUISITIONSCONSTANTS) {
    var vm = this;
    vm.MESSAGECONSTANTS = MESSAGECONSTANTS;
    vm.REQUISITIONSCONSTANTS = REQUISITIONSCONSTANTS;
    //variables
    vm.recentActivities = [];
    vm.userNotificationControls = {};
    vm.recentActivitiesControls = {};
    vm.userNotificationControls.setNewNotificationsAsViewed = null;
    vm.recentActivitiesControls.getActivities = null;
    vm.userId = $rootScope.userDetails.id;
    vm.contents = [];
    vm.showpopup = false;
    //flags
    vm.loadingFlag = true;

    //methods
    vm.init = init;
    vm.getRequisitionCardInfo = getRequisitionCardInfo;
    vm.redirectToJobWorkflow = redirectToJobWorkflow;
    vm.showAllActivityModal = showAllActivityModal;
    vm.redirectToUpdateRequisition = redirectToUpdateRequisition;
    vm.getRequisitionStateObject = getRequisitionStateObject;
    vm.modifyRequisitionStates = modifyRequisitionStates;
    vm.showAlert = showAlert;
    vm.deleteRequisition = deleteRequisition;
    vm.showActionModal = showActionModal;
    vm.showBackwardActionModal = showBackwardActionModal;
    vm.popOverActivate = popOverActivate;
    vm.popOverDeactivate = popOverDeactivate;

    vm.init();

    function init() {
        vm.showpopup = false;
         // set the new notifications count
         vm.numberOfNewNotifications = 0;
         // set notifications search object
         vm.notificationsSearchObject = {};
         vm.notificationsSearchObject.page = 1;
         vm.notificationsSearchObject.size = 3;
         vm.notificationsSearchObject.jobId = vm.requisitionInfo.requisitionNumber;
         vm.notificationsSearchObject.companyId = $rootScope.userDetails.company.companyId;
         vm.notificationsSearchObject.notifyOn = 'REQUISITION_CARD';
         vm.options = {
            barColor: '#03a9f4',
            trackColor: '#f2f2f2',
            scaleColor: false,
            lineWidth: 8,
            size: 130,
            animate: 1500,
            onStep: function (from, to, percent) {
                $(this.el).find('.percent').text(Math.round(percent));
            }
        };
        vm.companyId = $rootScope.userDetails.company.companyId;
        vm.getRequisitionCardInfo();
    }

    
    function getRequisitionCardInfo(){
        jobService.getRequisitionDetails(vm.requisitionInfo.requisitionNumber, vm.companyId, function (data) {
            angular.merge(vm.requisitionInfo, data);
            _setRequisitionGraphInfo(vm.requisitionInfo);
            vm.loadingFlag = false;
        }, function (error) {
            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
        });
    }

    function _setRequisitionGraphInfo(requisitionInfo) {
        // Make the graph colour as Green if Days TO Source and Days To Fill reaches the minimum threshold days
        var optionsGreen = angular.copy(vm.options);
        optionsGreen.barColor = '#8bc34a';

        // Make the graph colour as Red if no criteria matches
        var optionsRed = angular.copy(vm.options);
        optionsRed.barColor = '#e84e40';

        var optionsYellow = angular.copy(vm.options);
        optionsYellow.barColor = '#ffc107';

        //Make the colour as Orange
        var optionsOrange = angular.copy(vm.options);
        optionsOrange.barColor = '#ff751a';

        var optionsPurple = angular.copy(vm.options);
        optionsPurple.barColor = '#9c27b0';

        var optionsGray = angular.copy(vm.options);
        optionsGray.barColor = '#90a4ae';

        /**
         * Check whether the requisitions are empty or not.
         * If not empty check the conditions to show DaysToSource and DaysTOFill
         */
        if (requisitionInfo.daysToSource < 20) {
            requisitionInfo.daysToSourceGraphOptions = optionsGreen;
        } else if (requisitionInfo.daysToSource > 20 && requisitionInfo.daysToSource < 40) {
            requisitionInfo.daysToSourceGraphOptions = optionsOrange;
        } else {
            requisitionInfo.daysToSourceGraphOptions = optionsRed;
        }
        if (requisitionInfo.daysToFill < 20) {
            requisitionInfo.daysToFillGraphOptions = optionsGreen;
        } else if (requisitionInfo.daysToFill > 20 && requisitionInfo.daysToFill < 40) {
            requisitionInfo.daysToFillGraphOptions = optionsOrange;
        } else {
            requisitionInfo.daysToFillGraphOptions = optionsRed;
        }

        if (requisitionInfo.canEdit != null || typeof canEdit !== "undefined") {
            requisitionInfo.canEdit = _convertToBoolean(requisitionInfo.canEdit);
        }

        if (requisitionInfo.canDelete != null || typeof canDelete !== "undefined") {
            requisitionInfo.canDelete = _convertToBoolean(requisitionInfo.canDelete);
        }

        if (requisitionInfo.disableCompletedCandidate != null || typeof disableCompletedCandidate !== "undefined") {
            requisitionInfo.disableCompletedCandidate = _convertToBoolean(requisitionInfo.disableCompletedCandidate);
        }

        if (requisitionInfo.disableFilledCandidate != null || typeof disableFilledCandidate !== "undefined") {
            requisitionInfo.disableFilledCandidate = _convertToBoolean(requisitionInfo.disableFilledCandidate);
        }

        if (requisitionInfo.disableOpenState != null || typeof disableOpenState !== "undefined") {
            requisitionInfo.disableOpenState = _convertToBoolean(requisitionInfo.disableOpenState);
        }

        if (requisitionInfo.disableCancelState != null || typeof disableCancelState !== "undefined") {
            requisitionInfo.disableCancelState = _convertToBoolean(requisitionInfo.disableCancelState);
        }

        if (requisitionInfo.disablePausedState != null || typeof disablePausedState !== "undefined") {
            requisitionInfo.disablePausedState = _convertToBoolean(requisitionInfo.disablePausedState);
        }

        if (requisitionInfo.disableCompleteState != null || typeof disableCompleteState !== "undefined") {
            requisitionInfo.disableCompleteState = _convertToBoolean(requisitionInfo.disableCompleteState);
        }

        if (requisitionInfo.disableJoinState != null || typeof disableJoinState !== "undefined") {
            requisitionInfo.disableJoinState = _convertToBoolean(requisitionInfo.disableJoinState);
        }

        if (requisitionInfo.disableFilledState != null || typeof disableFilledState !== "undefined") {
            requisitionInfo.disableFilledState = _convertToBoolean(requisitionInfo.disableFilledState);
        }

        if (requisitionInfo.disableSourcedState != null || typeof disableSourcedState !== "undefined") {
            requisitionInfo.disableSourcedState = _convertToBoolean(requisitionInfo.disableSourcedState);
        }

    }

    function _convertToBoolean(value) {
        return value === "true" || value === true;
    }

    function redirectToJobWorkflow(id, title, client, city, state){
        if(angular.isDefined(id)){
            $state.go('missioncontrol.candidateJobMatchesWorkflow',{jobId:id, jobTitle: title, jobClient: client, city: city, state: state});
        }
    }

     function redirectToUpdateRequisition(id){
        $state.go('missioncontrol.uploadreq',{'jobId':id});
    }

     function getRequisitionStateObject(stateName, requisitionObject) {
        vm.isCandidateFilled = true;
        vm.requisitionSPOC = {};
        vm.requisitionSPOC.time = '';
        vm.requisitionSPOC.requisitionStatus = requisitionObject.requisitionStatus;
        vm.requisitionSPOC.RequisitionId = requisitionObject.requisitionNumber;
        vm.requisitionSPOC.Role = requisitionObject.requisitionRole;
        vm.requisitionSPOC.noOfOpenings = requisitionObject.noOfOpenings;
        vm.requisitionSPOC.noOfFilledOpenings = requisitionObject.noOfFilledOpenings;
        vm.requisitionSPOC.noOfJustFilledOpenings = requisitionObject.noOfJustFilledOpenings;
        vm.requisitionSPOC.Company = requisitionObject.companyName;
        setAndUpdateRequisitionSPOCDateTime();
        vm.requisitionSPOC.activeCandidateCount = requisitionObject.activeCandidateCount;
        vm.requisitionSPOC.passiveCandidateCount = requisitionObject.passiveCandidateCount;
        vm.requisitionSPOC.requisitionActualNumber = requisitionObject.requisitionActualNumber;

        function setAndUpdateRequisitionSPOCDateTime(){
            vm.requisitionSPOC.date = moment().format('ll');
            vm.requisitionSPOC.time = moment().format('hh:mm:ss a');
            $timeout(function(){
                setAndUpdateRequisitionSPOCDateTime();
            },1000);
        }

        var requisitionFlag = false;
        if ((requisitionObject.requisitionStatus === "FILLED" || requisitionObject.requisitionStatus === "Filled") && stateName === "Source") {
            vm.requisitionSPOC.titleName = "Fill to Sourced";
            vm.requisitionSPOC.currentStateName = stateName;
        } else if (stateName === "Remove Filled Candidate") {
            vm.requisitionSPOC.titleName = "Remove Filled Candidate";
            vm.requisitionSPOC.currentStateName = "Source";
        } else if ((requisitionObject.requisitionStatus === "COMPLETED" || requisitionObject.requisitionStatus === "Completed") && stateName === "Join") {
            vm.requisitionSPOC.titleName = "Completed to Joined";
            vm.requisitionSPOC.currentStateName = stateName;
         }else if (stateName === "Remove Joined Candidate") {
            vm.requisitionSPOC.titleName = "Remove Joined Candidate";
            vm.requisitionSPOC.currentStateName = "Fill";
        } else if ((angular.lowercase(requisitionObject.requisitionStatus) === "joined" || angular.lowercase(requisitionObject.requisitionStatus) === "filled")  && stateName === "Remove Joined Candidate") {
            vm.requisitionSPOC.titleName = "Remove Joined Candidate";
            vm.requisitionSPOC.currentStateName = "Fill";
        } else if (requisitionObject.requisitionStatus === "Canceled" && stateName === "Continue") {
            requisitionFlag = true;
            vm.requisitionSPOC.titleName = "Reopen";
            vm.requisitionSPOC.currentStateName = "Reopen";
        }
        else if (requisitionObject.requisitionStatus === "Sourced" && stateName === "Continue") {
            vm.requisitionSPOC.titleName = "Reopen for Matching";
            vm.requisitionSPOC.currentStateName = "Reopen for Matching";
        } else {
            requisitionFlag = true;
            vm.requisitionSPOC.currentStateName = stateName;
        }

        // set default notes
        if (stateName === "Open") {
            vm.requisitionSPOC.note = "Opening Requisition";
        } else if (stateName === "Source") {
            vm.requisitionSPOC.note = "Sourcing Requisition";
        } else if (stateName === "Continue" && vm.requisitionSPOC.currentStateName == "Reopen for Matching") {
            vm.requisitionSPOC.note = "Reopening Requisition for Matching";
        } else if (stateName === "Continue") {
            vm.requisitionSPOC.note = "Continuing Requisition";
        } else if (stateName === "Fill") {
            vm.requisitionSPOC.note = "Filling Candidate";
        } else if (stateName === "Join") {
            vm.requisitionSPOC.note = "Joining Candidate";
        } 

        if (requisitionFlag) {
            $http.get("api/job/getcandidatesforreqstatus/" + requisitionObject.requisitionNumber + "/" + stateName).success(function (data) {
                vm.candidates = data.candidates;
                if (vm.candidates.length === 0) {
                    vm.candidates = [];
                }
                vm.requisitionSPOC.requisitionOpeningNumber = data.requisitionOpeningNumber;
                if (stateName === "Fill" && vm.candidates.length === 0) {
                    vm.showAlert('No candidates are available in Interviewed state to Fill');
                } else if (stateName === "Join" && vm.candidates.length === 0) {
                    vm.showAlert('No candidates are available in filled state to Join');
                } else {
                      if (vm.candidates != null && vm.candidates.length > 0) {
                          for (var i=0;i<vm.candidates.length;i++) {
                              if (vm.candidates[i].lastName != null) {
                                  vm.candidates[i].name = vm.candidates[i].firstName + " " + vm.candidates[i].lastName;
                              } else {
                                  vm.candidates[i].name = vm.candidates[i].firstName;
                              }
                          }
                      }
                    vm.showActionModal(vm.requisitionSPOC);

                }
            }, function myError(response) {
                vm.candidates = [];
            });
        } else {
            $http.get("api/job/getcandidatesforcurrentstatus/" + requisitionObject.requisitionNumber + "/" + vm.requisitionSPOC.currentStateName).success(function (data) {
                vm.candidates = data;
                if (vm.candidates != null && vm.candidates.length > 0) {
                      for (var i=0;i<vm.candidates.length;i++) {
                          if(vm.requisitionSPOC.currentStateName === "Source") {
                            if (vm.candidates[i].fillCandidate.contact.lastname != null) {
                                vm.candidates[i].name = vm.candidates[i].fillCandidate.contact.firstname + " " + vm.candidates[i].fillCandidate.contact.lastname;
                            } else {
                                vm.candidates[i].name = vm.candidates[i].fillCandidate.contact.firstname;
                            }
                          } else if (vm.requisitionSPOC.currentStateName === "Fill") {
                            if (vm.candidates[i].joinCandidate.contact.lastname != null) {
                                vm.candidates[i].name = vm.candidates[i].joinCandidate.contact.firstname + " " + vm.candidates[i].joinCandidate.contact.lastname;
                            } else {
                                vm.candidates[i].name = vm.candidates[i].joinCandidate.contact.firstname;
                            }
                          }

                      }
                  }

                if (angular.lowercase(requisitionObject.requisitionStatus) === "filled" && stateName === "Source" && vm.candidates.length === 0) {
                    vm.showAlert('No candidates are available in Filled state to remove');
                } else if (angular.lowercase(requisitionObject.requisitionStatus) === "sourced" && stateName === "Remove Filled Candidate" && vm.candidates.length === 0) {
                    vm.showAlert('No candidates are available in Filled state to remove');
                } else if ((angular.lowercase(requisitionObject.requisitionStatus) === "completed" || angular.lowercase(requisitionObject.requisitionStatus) === "joined") && stateName === "Fill" && vm.candidates.length === 0) {
                    vm.showAlert('No candidates are available in Joined state to remove');
                } else if (angular.lowercase(requisitionObject.requisitionStatus) === "filled" && stateName === "Remove Joined Candidate" && vm.candidates.length === 0) {
                    vm.showAlert('No candidates are available in Joined state to remove');
                } else {
                    vm.showBackwardActionModal();
                }
            }, function myError(response) {
                vm.candidates = [];
            });
        }

    }

    function showActionModal(req){
        $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'app/directives/requisitionCard/requisition-action-modal.html',
            controller: ['$uibModalInstance','requisitionSPOC','MESSAGECONSTANTS','candidates',function ($uibModalInstance, requisitionSPOC, MESSAGECONSTANTS, candidates) {
                var ctrl = this;
                ctrl.requisitionSPOC = requisitionSPOC;
                ctrl.MESSAGECONSTANTS = MESSAGECONSTANTS;
                ctrl.candidates = candidates;
                ctrl.modifyRequisitionStates = function (req, direction) {
                    $uibModalInstance.dismiss('cancel');
                    vm.modifyRequisitionStates(req, direction);
                };
                ctrl.closeModal = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }],
            controllerAs: 'requisitionAction',
            size: 'md',
            resolve:{
                requisitionSPOC: function () {
                    return vm.requisitionSPOC;
                },
                MESSAGECONSTANTS: function () {
                    return vm.MESSAGECONSTANTS;
                },
                candidates: function () {
                    return vm.candidates;
                }
            }
        });
    } 

    function showBackwardActionModal() {
        $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'app/directives/requisitionCard/requisition-backward-action-modal.html',
            controller: ['$uibModalInstance','requisitionSPOC','MESSAGECONSTANTS','candidates',function ($uibModalInstance, requisitionSPOC, MESSAGECONSTANTS, candidates) {
                var ctrl = this;
                ctrl.requisitionSPOC = requisitionSPOC;
                ctrl.MESSAGECONSTANTS = MESSAGECONSTANTS;
                ctrl.candidates = candidates;
                ctrl.modifyRequisitionStates = function (req, direction) {
                    $uibModalInstance.dismiss('cancel');
                    vm.modifyRequisitionStates(req, direction);
                },
                ctrl.closeModal = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }],
            controllerAs: 'backwardAction',
            size: 'md',
            resolve:{
                requisitionSPOC: function () {
                    return vm.requisitionSPOC;
                },
                MESSAGECONSTANTS: function () {
                    return vm.MESSAGECONSTANTS;
                },
                candidates: function () {
                    return vm.candidates;
                }
            }
        });
    }

    function deleteRequisition(requisition) {
        var requisitionObject = { id: requisition.jobId, note: requisition.note };
        jobService.deleteJob(requisitionObject, function (data) {
            alertsAndNotificationsService.showBannerMessage("Requisition '" + requisition.requisitionActualNumber + "' deleted successfully", 'success');
            $state.reload();
         },
            function (error) {
                alertsAndNotificationsService.showBannerMessage(error.message, "danger");
            });
    };

    function modifyRequisitionStates(requisitionSPOC, direction) {
        //Construct update status object
        var updateRequisitionObject = {};
        updateRequisitionObject.jobId = requisitionSPOC.RequisitionId;
        if (requisitionSPOC.currentStateName === "Source") {
            updateRequisitionObject.jobStateType = "SOURCED";
        } else if (requisitionSPOC.currentStateName === "Fill") {
            updateRequisitionObject.jobStateType = "FILLED";
            //updateRequisitionObject.candidateId = requisitionSPOC.candidate.id;
        } else if (requisitionSPOC.currentStateName === "Complete") {
            updateRequisitionObject.jobStateType = "COMPLETED";
        } else if (requisitionSPOC.currentStateName === "Cancel") {
            updateRequisitionObject.jobStateType = "CANCELED";
        } else if (requisitionSPOC.currentStateName === "Continue" || requisitionSPOC.currentStateName === "Reopen" || requisitionSPOC.currentStateName === "Reopen for Matching") {
            updateRequisitionObject.jobStateType = "OPEN";
        } else if (requisitionSPOC.currentStateName === "Pause") {
            updateRequisitionObject.jobStateType = "PAUSED";
        } else if (requisitionSPOC.currentStateName === "Delete") {
            updateRequisitionObject.jobStateType = "ARCHIVED";
        } else if (requisitionSPOC.currentStateName === "Join") {
          updateRequisitionObject.jobStateType = "JOINED";
        } else {
            updateRequisitionObject.jobStateType = requisitionSPOC.currentStateName;
        }
        updateRequisitionObject.note = requisitionSPOC.note;
        updateRequisitionObject.updationDate = requisitionSPOC.date;
        updateRequisitionObject.updationTime = requisitionSPOC.time;
        updateRequisitionObject.requisitionActualNumber = requisitionSPOC.requisitionActualNumber;
        if (!angular.isUndefined(requisitionSPOC.candidate)) {
            if (requisitionSPOC.titleName === MESSAGECONSTANTS.VIEW_REQUISITION.REMOVE_FILLED_CANDIDATE) {
                updateRequisitionObject.candidateId = requisitionSPOC.candidate.fillCandidate.id;
            } else if (requisitionSPOC.titleName === MESSAGECONSTANTS.VIEW_REQUISITION.REMOVE_JOINED_CANDIDATE) {
                   updateRequisitionObject.candidateId = requisitionSPOC.candidate.joinCandidate.id;
            } else {
                updateRequisitionObject.candidateId = requisitionSPOC.candidate.id;
            }
        }
        updateRequisitionObject.direction = direction;
        updateRequisitionObject.recruiterId = vm.userId;
        if (direction === "Forward") {
            updateRequisitionObject.requisitionOpeningNumber = requisitionSPOC.requisitionOpeningNumber;
        } else if (direction === "Backward" && !angular.isUndefined(requisitionSPOC.candidate)) {
            updateRequisitionObject.requisitionOpeningNumber = requisitionSPOC.candidate.requisitionOpeningNumber;
        }

        //Call the API to update the status
        if (updateRequisitionObject.jobStateType !== "ARCHIVED") {
            genericService.addObject(StorageService.get('baseurl') + vm.REQUISITIONSCONSTANTS.CONTROLLER.UPDATE_JOB_STATUS, updateRequisitionObject).then(function (data) {
                if (updateRequisitionObject.jobStateType == "OPEN") {
                    if (requisitionSPOC.currentStateName === "Reopen") {
                        updateRequisitionObject.jobStateType = "reopened";
                    } else if(requisitionSPOC.currentStateName === "Reopen for Matching") {
                        updateRequisitionObject.jobStateType = "reopened for matching"
                    } else {
                        updateRequisitionObject.jobStateType = "opened";
                    }
                }else if (requisitionSPOC.titleName === MESSAGECONSTANTS.VIEW_REQUISITION.REMOVE_FILLED_CANDIDATE || requisitionSPOC.titleName === MESSAGECONSTANTS.VIEW_REQUISITION.REMOVE_JOINED_CANDIDATE) {
                    updateRequisitionObject.jobStateType = "updated";
                }
                var successmessage = "Requisition '<b><i class='wrap-word'>(" + requisitionSPOC.requisitionActualNumber+ ")</i></b>' "+angular.lowercase(updateRequisitionObject.jobStateType) +" successfully.";
                alertsAndNotificationsService.showBannerMessage(successmessage, 'success');
                $state.reload();
            }, function (error) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            });
        } else {
            deleteRequisition(updateRequisitionObject);
      }
    }
 
    function showAlert(message) {
        bootbox.alert({
            closeButton: false,
            title: '<div class="alert alert-danger" ><i class="fa fa-times-circle fa-fw fa-lg"></i><strong>Oh snap!</strong></div>',
            message: message
        });
    }

    function popOverActivate(Recruiter) {
            $('[data-toggle="popover"]').popover('destroy');
            $('body').popover('destroy');
            vm.contents = [];
            var index = 0;
            angular.forEach(Recruiter, function (recruiter) {
                var recruiterLength = recruiter.length;
                if (recruiterLength > 14) {
                    var recruitCut = recruiter.substring(0, 12) + '...';
                } else {
                    var recruitCut = recruiter;
                }
                index++;
                if (index > 1) {
                    vm.contents.push('<div class="recruiterpopup text-center popover-width-limit"><img ng-click="hello()" class="project-img-owner recruiter-image recruiter-image-resize"  alt="" src="app-content/img/samples/4dot5-default-no-profile-pic.jpg" data-toggle="popover" title="' + recruiter + '" /> <span class="" data-toggle="popover" title="' + recruiter + '">&nbsp' + recruitCut + '</span> </div>');
                    $(document).ready(function () {
                        $('[data-toggle="popover"]').tooltip();
                    });
                }
            });
            var CONTENT = vm.contents.join(' ');
            vm.showpopup = true;
            angular.element("body").popover({
                selector: '[id=toggleforRecruit]',
                trigger: 'click',
                html: true,
                content: CONTENT,
                placement: "left"
            });
            $compile($('.popover.in').contents())(vm);
        }

    function popOverDeactivate(CONTENT) {
        $('body').popover('destroy');

        $('body').popover({
            selector: '[id=toggleforRecruit]',
            trigger: 'hover',
            html: true,
            content: CONTENT,
            placement: "left"
        });
    }

    function showAllActivityModal(req){
        $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'app/partials/missioncontrol/requisitions/requisition-activity-modal.html',
            controller: ['$uibModalInstance','jobId','role','requisitionNumber','clientName',function ($uibModalInstance, jobId, role, requisitionNumber, clientName) {
                var vm = this;
                vm.jobId = jobId;
                vm.role = role;
                vm.requisitionNumber = requisitionNumber;
                vm.clientName = clientName;
                vm.closeModal = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }],
            controllerAs: 'activityModal',
            size: 'lg',
            resolve:{
                jobId: function () {
                    return vm.requisitionInfo.requisitionNumber;
                },
                role: function () {
                    return vm.requisitionInfo.requisitionRole;
                },
                requisitionNumber: function(){
                    return vm.requisitionInfo.requisitionActualNumber;
                },
                clientName: function () {
                    return vm.requisitionInfo.companyName;
                }
            }
        });
    } 
}

RequisitionCardController.$inject = ['$state', '$timeout', '$compile', '$http', '$uibModal', '$rootScope', 'genericService', 'StorageService', 'candidateService', 'alertsAndNotificationsService', 'jobService', 'CandidateCard','CandidateCardQueryFilter', 'CandidateActivityFilter','MESSAGECONSTANTS', 'REQUISITIONSCONSTANTS'];

fourdotfivedirectives.controller('RequisitionCardController', RequisitionCardController);