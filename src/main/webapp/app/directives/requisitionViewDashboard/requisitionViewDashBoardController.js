
function RequisitionViewDashBoardController(StorageService, jobService, $rootScope, genericService, MESSAGECONSTANTS, alertsAndNotificationsService) {
    var vm = this; 
    vm.init = init;
    vm.getAllActivityTypes = getAllActivityTypes;
    vm.getRequisitionsByLatestActivity = getRequisitionsByLatestActivity;
    vm.MESSAGECONSTANTS = MESSAGECONSTANTS;
    vm.init();

    function init() {
        vm.companyType = $rootScope.userDetails.company.companyType;
        vm.userId = $rootScope.userDetails.id;
        vm.companyId = $rootScope.userDetails.company.companyId;
        vm.getRequisitionsByLatestActivity(function(data){
            vm.requisitions = data;
        });
        vm.getAllActivityTypes();
    }

    function getRequisitionsByLatestActivity(callback) {
        genericService.getObjects(StorageService.get('baseurl') + 'api/job/jobsbyactivities' + '/' + vm.userId + '/' + vm.companyId).then(function (data) {
            callback(data);
        }, function (error) {
            callback(error);
        });
    } 

    function getAllActivityTypes(){
        jobService.getActivityTypes(function (data) {
            vm.fullActivityList = data;
            vm.activityMap = [];
            angular.forEach(data, function (val, key) {
                vm.activityMap[val.value] = val.name;
            });
        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }  
};

RequisitionViewDashBoardController.$inject = ['StorageService',
                                            'jobService',
                                            '$rootScope',
                                            'genericService',
                                            'MESSAGECONSTANTS',
                                            'alertsAndNotificationsService'];

fourdotfivedirectives.controller('RequisitionViewDashBoardController', RequisitionViewDashBoardController);