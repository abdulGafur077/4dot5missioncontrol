fourdotfivedirectives.directive('requisitionViewDashboard', function requisitionView() {
    return {
        restrict: 'E',
        scope: {
            dashboardView: '@',
            limit: "@"
        },
        bindToController: true,
        controller: 'RequisitionViewDashBoardController',
        controllerAs: 'recentRequisitions',
        templateUrl: 'app/directives/requisitionViewDashboard/requisition-view-dashboard.html'
    }
});