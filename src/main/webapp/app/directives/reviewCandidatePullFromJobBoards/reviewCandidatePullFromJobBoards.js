fourdotfivedirectives.directive('reviewCandidatePullFromJobBoards', function reviewCandidatePullFromJobBoards(){
    return {
        restrict: 'E',
        scope: {},
        bindToController:{
            jbiTransactionId: '@',
            jobId: '@'
        },
        controller: 'ReviewCandidatePullFromJobBoardsController',
        controllerAs: 'reviewCandidatePull',
        templateUrl: 'app/directives/reviewCandidatePullFromJobBoards/review-candidate-pull-from-job-boards.html'
    }
});