/*
    This is the controller for Review Candidate Pull From Job Board
 */

function ReviewCandidatePullFromJobBoardsController(jobService, jobMatchService, modalService, alertsAndNotificationsService){
    var vm = this;

    // variables
    vm.candidateCounts = {};
    vm.candidatesList = [];
    vm.candidatesToAssociateList = [];
    vm.createJobMatchesObject = {};
    // flags
    vm.loadingCountsInfoFlag = false;
    vm.loadingJobMatchesInfoFlag = false;
    vm.selectedAllCandidatesFlag = false;
    vm.associatingCandidatesFlag = false;
    // methods
    vm.getJobBoardPullCounts = getJobBoardPullCounts;
    vm.getJobBoardPullMatchInfo = getJobBoardPullMatchInfo;
    vm.init = init;
    vm.toggleSelectAllCandidates = toggleSelectAllCandidates;
    vm.candidateToAssociateToggle = candidateToAssociateToggle;
    vm.associateCandidates = associateCandidates;

    vm.init();

    function init(){
        vm.createJobMatchesObject = {
            requisitionId: vm.jobId,
            candidateIds: []
        };
        vm.getJobBoardPullCounts();
        vm.getJobBoardPullMatchInfo();
    }

    function getJobBoardPullCounts(){
        vm.loadingCountsInfoFlag = true;
        jobService.getJobBoardPullCounts(vm.jobId, vm.jbiTransactionId, function(data){
            vm.candidateCounts = data;
            vm.loadingCountsInfoFlag = false;
        }, function(error){
            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
        });
    }

    function getJobBoardPullMatchInfo(){
        vm.loadingJobMatchesInfoFlag = true;
        jobService.getJobBoardPullMatchInfo(vm.jbiTransactionId, function(data){
            angular.forEach(data, function(val, key){
                val.name = val.firstName + ' ' + val.lastName;
                if(val.jobMatchCreatedForEligibleScore){
                    val.addedSuccessfullyFlag = true;
                    val.status = 'Candidate Added.';
                }else if(val.jobMatchNotCreatedForInEligibleScore){
                    val.addedSuccessfullyFlag = false;
                    val.status = 'Not added as score is less than job threshold score';
                }else if(val.autoMatchCountExceeded){
                    val.addedSuccessfullyFlag = false;
                    val.status = 'Not added as auto match count for the job was exceeded.';
                }
                // floor the match score to the lower integer value.
                val.score = Math.floor(parseFloat(val.score));
                val.selectedFlag = false;
                vm.candidatesList.push(val);
            });
            vm.loadingJobMatchesInfoFlag = false;
        }, function (error) {
            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
        })
    }

    function toggleSelectAllCandidates(){
        if(vm.selectedAllCandidatesFlag){
            // if selectAllCandidates was already selected, remove all selections
            angular.forEach(vm.candidatesList, function(val, key){
                // if candidate was not already added successfully and was selected, de select it and remove it from associate list
                if(!val.addedSuccessfullyFlag && val.selectedFlag){
                    val.selectedFlag = false;
                    // if selected is removed, remove from candidates to associate list
                    _.remove(vm.candidatesToAssociateList, {
                        candidateId: val.candidateId
                    });
                }
            });
            vm.selectedAllCandidatesFlag = false;
        }else{
            // if selectAllCandidates was not already selected
            angular.forEach(vm.candidatesList, function(val, key){
                // if candidate was not already added successfully and also not already selected, select it and add to associate list
                if(!val.addedSuccessfullyFlag && !val.selectedFlag){
                    val.selectedFlag = true;
                    vm.candidatesToAssociateList.push(val);
                }
            });
            vm.selectedAllCandidatesFlag = true;
        }
    }

    function candidateToAssociateToggle(candidate){
        // if it is selected, add to associate list
        if(candidate.selectedFlag){
            vm.candidatesToAssociateList.push(candidate);
        }else{
            // if selected is removed, remove from candidates to associate list
            _.remove(vm.candidatesToAssociateList, {
                candidateId: candidate.candidateId
            });
        }
    }

    function associateCandidates(successCallback){
        angular.forEach(vm.candidatesToAssociateList, function (val, key) {
            vm.createJobMatchesObject.candidateIds.push(val.candidateId);
        });
        vm.associatingCandidatesFlag = true;
        var associatingDialog = bootbox.dialog({
            message: '<p class="text-center">Adding Candidates <i class="fa fa-spinner fa-spin"></i></p>',
            closeButton: false
        });
        jobMatchService.createJobMatches(vm.createJobMatchesObject, function (data) {
            vm.associatingCandidatesFlag = false;
            associatingDialog.modal('hide');
            modalService.addModalOpenClassToBodyIfAnyModalIsOpen();
            angular.forEach(vm.candidatesToAssociateList, function(val, key){
                // find each associated candidate in the original candidates list
                var index = _.findIndex(vm.candidatesList, {
                    candidateId: val.candidateId
                });
                // for each candidate, set the added successfully flag to true and selected flag to false
                vm.candidatesList[index].addedSuccessfullyFlag = true;
                vm.candidatesList[index].status = 'Candidate Added.';
                vm.candidatesList[index].selectedFlag = false;
            });
            // make the candidates to associate list as empty.
            vm.candidatesToAssociateList = [];
            // reload the counts to refresh manually added candidates counts
            vm.getJobBoardPullCounts();
            if(successCallback){
                successCallback();
            }
        }, function(error){
            vm.associatingCandidatesFlag = false;
            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
        });
    }

}

ReviewCandidatePullFromJobBoardsController.$inject = ['jobService','jobMatchService','modalService','alertsAndNotificationsService'];

fourdotfivedirectives.controller('ReviewCandidatePullFromJobBoardsController', ReviewCandidatePullFromJobBoardsController);