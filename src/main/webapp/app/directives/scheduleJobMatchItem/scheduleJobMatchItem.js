fourdotfivedirectives.directive('scheduleJobMatchItem', function scheduleJobMatchItem() {
    return {
        restrict: 'E',
        scope: {},
        bindToController: {
            jobMatchObject: '=',
            saveCallback: '&',
            itemType: '@',
            jobMatchId: '@',
            mode: '@'
        },
        controller: 'ScheduleJobMatchItemController',
        controllerAs: 'scheduleJobMatchItem',
        templateUrl: 'app/directives/scheduleJobMatchItem/schedule-job-match-item.html'
    }
});