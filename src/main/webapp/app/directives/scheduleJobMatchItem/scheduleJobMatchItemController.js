/*
  * This is the controller for the scheduling job match item.
 */

function ScheduleJobMatchItemController($rootScope, $timeout, $filter, jobMatchService, moment, dateTimeUtilityService, userService, browserService, alertsAndNotificationsService) {
    var vm = this;
    //variables
    vm.scheduleData = {
        isCompany: true,
        jobMatchId: '',
        userId: null,
        note: '',
        emailAddress: '',
        firstName: '',
        lastName: '',
        title: '',
        department: '',
        company: '',
        startDateTime: '',
        endDateTime: '',
        phoneNumber:'',
        timeZoneName: ''
    };
    vm.selectedUserObject = undefined;
    vm.selectedUserObjectsArray = [];
    vm.userSearchPlaceholder = 'Type first name or last name or email to search.';
    vm.itemDisplayLabel = '';
    vm.usersListLabel = '';
    vm.userTypeLabel = '';
    vm.scheduleLabel = 'Schedule';
    vm.noEditableMeetingsLabel = '';
    vm.companyName = '';
    vm.clientName = '';
    vm.chosenMeeting = '';
    vm.clientorbulist = [];
    vm.meetingsList = [];
    vm.startDate = '';
    vm.startTime = '';
    vm.endDate = '';
    vm.endTime = '';
    vm.browserTimeZone = null;
    //flags
    vm.savingFlag = false;
    vm.editModeFlag = false;
    vm.showFormFlag = false;
    vm.loadingFlag = false;
    vm.editableMeetingsExistFlag = false;
    vm.userSearchFlag = true;
    //methods
    vm.init = init;
    vm.schedule = schedule;
    vm.getScheduledItems = getScheduledItems;
    vm.meetingChanged = meetingChanged;
    vm.getUsers = getUsers;
    vm.userSelected = userSelected;
    vm.userTypeChanged = userTypeChanged;
    vm.userTypeAheadChanged = userTypeAheadChanged;
    vm.companyClientUserTypeChanged = companyClientUserTypeChanged;
    vm.timeZoneChanged = timeZoneChanged;

    vm.init();

    function init() {
        vm.browserTimeZone = dateTimeUtilityService.guessBrowserTimeZoneName();
        if(vm.mode == 'edit'){
            vm.editModeFlag = true;
            vm.scheduleLabel = 'Reschedule';
            vm.getScheduledItems();
        }else{
            vm.showFormFlag = true;
        }
        vm.scheduleData.jobMatchId = vm.jobMatchId;
        vm.scheduleData.company = $rootScope.userDetails.company.companyId;
        vm.companyName = $rootScope.userDetails.company.companyName;
        vm.clientName = vm.jobMatchObject.jobMatchClientName;
        vm.clientId = vm.jobMatchObject.jobMatchClientId;
        switch(vm.itemType){
            case 'phoneScreen':
                vm.itemDisplayLabel = 'Phone Screen';
                vm.usersListLabel = 'Select Phone Screen';
                vm.userTypeLabel = 'Phone Screener';
                vm.noEditableMeetingsLabel = "No 'Scheduled' Phone Screens are available to reschedule !";
                break;
            case 'interview':
                vm.itemDisplayLabel = 'Interview';
                vm.usersListLabel = 'Select Interview';
                vm.userTypeLabel = 'Interviewer';
                vm.noEditableMeetingsLabel = "No 'Scheduled' Interviews are available to reschedule !";
                break;
        }
        vm.scheduleData.note = "Scheduling " + vm.itemDisplayLabel;
        _initializeDateAndTimePickers();
        // to disable chrome autocomplete of the type ahead field.
        $timeout(function(){
            if(browserService.isGoogleChrome()){
                $("[placeholder='" + vm.userSearchPlaceholder + "']").attr("autocomplete","nope");
            }
        }, 200);
    }

    function schedule() {
        if(!vm.scheduleForm.$invalid){
            vm.savingFlag = true;
            vm.scheduleData.startDateTime = dateTimeUtilityService.getDateTimeInAPIFormat(new Date(vm.startDate + ' ' + vm.startTime));
            vm.scheduleData.endDateTime = dateTimeUtilityService.getDateTimeInAPIFormat(new Date(vm.endDate + ' ' + vm.endTime));
            if(vm.editModeFlag == true){
                var rescheduleData = {};
                rescheduleData.startDateTime = vm.scheduleData.startDateTime;
                rescheduleData.endDateTime = vm.scheduleData.endDateTime;
                rescheduleData.note = vm.scheduleData.note;
                rescheduleData.timeZoneName = vm.scheduleData.timeZoneName;
                jobMatchService.rescheduleMeeting(vm.chosenMeeting,rescheduleData,function (data) {
                    vm.savingFlag = false;
                    if(!_.isUndefined(vm.saveCallback)){
                        vm.saveCallback();
                    }
                },function (error) {
                    vm.savingFlag = false;
                    if ($rootScope.isOnline) {
                        alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                    }
                });
            }else{
                jobMatchService.schedulePhoneScreenOrInterview(vm.itemType,vm.scheduleData,function (data) {
                    vm.savingFlag = false;
                    if(!_.isUndefined(vm.saveCallback)){
                        vm.saveCallback();
                    }
                },function (error) {
                    vm.savingFlag = false;
                    if ($rootScope.isOnline) {
                        alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                    }
                });
            }
        }
    }

    function _getMeetingsList(response){
        var meetingsList = [];
        angular.forEach(response, function (meetingDetails, key) {
            var timeZone = meetingDetails.timeZone;
            if(_.isNull(timeZone)){
                timeZone = vm.browserTimeZone;
            }
            var startDateTime = new Date(meetingDetails.meetingStartTime);
            var endDateTime = new Date(meetingDetails.meetingEndTime);
            angular.forEach(meetingDetails.participants, function (meetingUser, k) {
                var meeting = {};
                meeting.id = meetingDetails.meetingScheduleId;
                meeting.userId = meetingUser.id;
                meeting.userName = meetingUser.firstName;
                if(!_.isUndefined(meetingUser.lastName) && !_.isNull(meetingUser.lastName)){
                    meeting.userName += ' ' + meetingUser.lastName;
                }
                meeting.status = meetingDetails.status;
                switch(meeting.status){
                    case 'SCHEDULED':
                        meeting.statusClass = 'fa fa-calendar meeting-icon-scheduled';
                        meeting.statusLabel = 'Scheduled';
                        meeting.editableFlag = true;
                        vm.editableMeetingsExistFlag = true;
                        break;
                    case 'IN_PROGRESS':
                        meeting.statusClass = 'fa fa-hourglass-half meeting-icon-in-progress';
                        meeting.statusLabel = 'In Progress';
                        meeting.editableFlag = false;
                        break;
                    case 'COMPLETED':
                        meeting.statusClass = 'fa fa-check meeting-icon-completed';
                        meeting.statusLabel = 'Completed';
                        meeting.editableFlag = false;
                        break;
                    case 'CANCELLED':
                        meeting.statusClass = 'fa fa-ban meeting-icon-cancelled';
                        meeting.statusLabel = 'Canceled';
                        meeting.editableFlag = false;
                        break;
                    default:
                        // set defaults
                        meeting.statusClass = 'fa fa-question meeting-icon-in-progress';
                        meeting.statusLabel = 'Status Unknown';
                        meeting.editableFlag = false;
                }
                meeting.scheduledTimeInSelectedTimeZone = moment(startDateTime).tz(timeZone).format("MMM DD, YYYY ( hh:mm a") + ' to ' + moment(endDateTime).tz(timeZone).format("hh:mm a )") + ' in ' + timeZone;
                meeting.scheduledTimeInBrowserTimeZone = null;
                if(timeZone != vm.browserTimeZone){
                    meeting.scheduledTimeInBrowserTimeZone = moment(startDateTime).format("MMM DD, YYYY ( hh:mm a") + ' to ' + moment(endDateTime).format("hh:mm a )") + " in your Computer's time zone (" + vm.browserTimeZone + ")";
                }
                meeting.label = meeting.userName + ' - ' + meeting.scheduledTime;
                meetingsList.push(meeting);
            });
        });
        return meetingsList;
    }

    function getScheduledItems(){
        vm.loadingFlag = true;
        if(vm.itemType == 'phoneScreen'){
            jobMatchService.getPhoneScreeners(vm.jobMatchId, false, function (data) {
                vm.loadingFlag = false;
                vm.meetingsList = _getMeetingsList(data);
            }, function (error) {
                vm.loadingFlag = false;
                if ($rootScope.isOnline) {
                    alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                }
            });
        }else{
            jobMatchService.getInterviewers(vm.jobMatchId, false, function (data) {
                vm.loadingFlag = false;
                vm.meetingsList = _getMeetingsList(data);
            }, function (error) {
                vm.loadingFlag = false;
                if ($rootScope.isOnline) {
                    alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                }
            });
        }
    }

    function meetingChanged(){
        vm.showFormFlag = false;
        vm.loadingFlag = true;
        jobMatchService.getMeetingDetails(vm.chosenMeeting, function (data) {
            var participant = data.participants[0];
            vm.scheduleData.firstName = participant.firstName;
            vm.scheduleData.lastName = participant.lastName;
            vm.scheduleData.emailAddress = participant.email;
            vm.scheduleData.phoneNumber = participant.phoneNumber;
            vm.scheduleData.title = participant.title;
            vm.scheduleData.department = participant.department;
            vm.scheduleData.note = '';
            if(_.isNull(vm.scheduleData.title)){
                vm.scheduleData.title = '-';
            }
            if(_.isNull(vm.scheduleData.department)){
                vm.scheduleData.department = '-';
            }
            if(!_.isNull(data.timeZoneName)){
                // if time zone information exists
                vm.scheduleData.timeZoneName = data.timeZoneName;
                // get the time stamp string in the relevant time zone.
                var currentDateTimeStringInSelectedTimeZone = new Date(moment().tz(vm.scheduleData.timeZoneName).format("YYYY-MM-DDTHH:mm:ss.SSS"));
                var startDateTimeStringInSelectedTimeZone = moment(data.startTime).tz(vm.scheduleData.timeZoneName).format("YYYY-MM-DDTHH:mm:ss.SSS");
                var endDateTimeStringInSelectedTimeZone = moment(data.endTime).tz(vm.scheduleData.timeZoneName).format("YYYY-MM-DDTHH:mm:ss.SSS");
                var startDateTime = new Date(startDateTimeStringInSelectedTimeZone);
                var endDateTime = new Date(endDateTimeStringInSelectedTimeZone);

                $("#jobMatchScheduleStartDate").datepicker("setDate", startDateTime);
                $("#jobMatchScheduleStartDate").datepicker("setStartDate", currentDateTimeStringInSelectedTimeZone);
                $("#jobMatchScheduleStartDate").datepicker("update", $filter('fourdotfiveDateFormat')(startDateTime));
                $("#jobMatchScheduleEndDate").datepicker("setDate", endDateTime);
                $("#jobMatchScheduleEndDate").datepicker("setStartDate", startDateTime);
                $("#jobMatchScheduleEndDate").datepicker("update", $filter('fourdotfiveDateFormat')(endDateTime));
                $("#jobMatchScheduleStartTime").timepicker("setTime", _getTimeStringInAMPMFormat(startDateTime));
                $('#jobMatchScheduleEndTime').timepicker("setTime", _getTimeStringInAMPMFormat(endDateTime));
            }else{
                var currentDateTime = new Date();
                var startDateTime = new Date(data.startTime);
                var endDateTime = new Date(data.endTime);
                if(startDateTime > currentDateTime){
                    $("#jobMatchScheduleStartDate").datepicker("setDate", startDateTime);
                    $("#jobMatchScheduleStartDate").datepicker("setStartDate", currentDateTime);
                    $("#jobMatchScheduleStartDate").datepicker("update", $filter('fourdotfiveDateFormat')(startDateTime));
                    $("#jobMatchScheduleEndDate").datepicker("setDate", endDateTime);
                    $("#jobMatchScheduleEndDate").datepicker("setStartDate", startDateTime);
                    $("#jobMatchScheduleEndDate").datepicker("update", $filter('fourdotfiveDateFormat')(endDateTime));
                    $('#jobMatchScheduleStartTime').timepicker("setTime", _getTimeStringInAMPMFormat(startDateTime));
                    $('#jobMatchScheduleEndTime').timepicker("setTime", _getTimeStringInAMPMFormat(endDateTime));
                }else{
                    // the defaults will load.
                }
            }
            vm.loadingFlag = false;
            vm.showFormFlag = true;
        }, function(error){
            vm.loadingFlag = false;
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }

    function _getTimeStringInAMPMFormat(date){
        return moment(date).format("hh:mm A");
    }

    function _validateAndCorrectStartDateTimeAndEndDateTime(){
        var currentDateTimeStringInSelectedTimeZone = moment().tz(vm.scheduleData.timeZoneName).format("YYYY-MM-DDTHH:mm:ss.SSS");
        var momentOfCurrentDateTime = moment(currentDateTimeStringInSelectedTimeZone);
        var currentStartDateTime = new Date(vm.startDate + ' ' + vm.startTime);
        var momentOfCurrentStartDateTime = moment(currentStartDateTime);
        var currentEndDateTime = new Date(vm.endDate + ' ' + vm.endTime);
        // if the start date is same as the current date
        var startDateAndCurrentDateAreSameFlag = momentOfCurrentStartDateTime.isSame(momentOfCurrentDateTime, 'day');
        // if the start hours are less than current hours
        var startHoursLessThanCurrentHoursFlag = momentOfCurrentStartDateTime.isBefore(momentOfCurrentDateTime,'hour');
        // if the hours are equal but minutes are less than current minutes.
        var hoursEqualButStartMinutesLessThanCurrentMinutesFlag = ( momentOfCurrentStartDateTime.isSame(momentOfCurrentDateTime,'hour') && momentOfCurrentStartDateTime.isBefore(momentOfCurrentDateTime,'minute') );
        if(startDateAndCurrentDateAreSameFlag && (startHoursLessThanCurrentHoursFlag || hoursEqualButStartMinutesLessThanCurrentMinutesFlag)){
            var currentDateRoundedToTheHour = momentOfCurrentDateTime.minute() || momentOfCurrentDateTime.second() || momentOfCurrentDateTime.millisecond() ? momentOfCurrentDateTime.add(1, 'hour').startOf('hour').toDate() : momentOfCurrentDateTime.startOf('hour').toDate();
            $("#jobMatchScheduleStartTime").timepicker("setTime", _getTimeStringInAMPMFormat(currentDateRoundedToTheHour));
        }else if(currentStartDateTime >= currentEndDateTime){
            // after the change to 'start time', check if the existing 'end time' component keeps 'end date time' as greater than 'start date time'
            // if not, make the 'end time' 30 mins greater than current start time.
            var currentStartDateTimePlus30Minutes = momentOfCurrentStartDateTime.add('30','minutes').toDate();
            $("#jobMatchScheduleEndDate").datepicker("setDate", currentStartDateTimePlus30Minutes);
            $("#jobMatchScheduleEndDate").datepicker("update", $filter('fourdotfiveDateFormat')(currentStartDateTimePlus30Minutes));
            $('#jobMatchScheduleEndTime').timepicker("setTime", _getTimeStringInAMPMFormat(currentStartDateTimePlus30Minutes));
        }
        // set the start date of the end date calendar to be the current start date of the start date calendar.
        $("#jobMatchScheduleEndDate").datepicker("setStartDate", currentStartDateTime);
    }

    function timeZoneChanged() {
        var currentDateTimeStringInSelectedTimeZone = moment().tz(vm.scheduleData.timeZoneName).format("YYYY-MM-DDTHH:mm:ss.SSS");
        var currentDate = new Date(currentDateTimeStringInSelectedTimeZone);
        var momentOfCurrentDate = moment(currentDate);
        var currentDateTimeRoundedToTheHour = momentOfCurrentDate.minute() || momentOfCurrentDate.second() || momentOfCurrentDate.millisecond() ? momentOfCurrentDate.add(1, 'hour').startOf('hour').toDate() : momentOfCurrentDate.startOf('hour').toDate();

        $("#jobMatchScheduleStartDate").datepicker("setDate", currentDate);
        $("#jobMatchScheduleStartDate").datepicker("setStartDate", currentDate);
        $("#jobMatchScheduleStartDate").datepicker("update", $filter('fourdotfiveDateFormat')(currentDate));
        $("#jobMatchScheduleEndDate").datepicker("setDate", currentDateTimeRoundedToTheHour);
        $("#jobMatchScheduleEndDate").datepicker("setStartDate", currentDate);
        $("#jobMatchScheduleEndDate").datepicker("update", $filter('fourdotfiveDateFormat')(currentDateTimeRoundedToTheHour));
        $("#jobMatchScheduleStartTime").timepicker("setTime", _getTimeStringInAMPMFormat(currentDateTimeRoundedToTheHour));
        $('#jobMatchScheduleEndTime').timepicker("setTime", _getTimeStringInAMPMFormat(moment(currentDateTimeRoundedToTheHour).add(30, 'm').toDate()));
    }

    function _initializeDateAndTimePickers(){
        $(document).ready(function () {
            var currentDateTimeStringInSelectedTimeZone = moment().tz(vm.scheduleData.timeZoneName).format("YYYY-MM-DDTHH:mm:ss.SSS");
            var currentDate = new Date(currentDateTimeStringInSelectedTimeZone);
            var momentOfCurrentDate = moment(currentDate);
            var currentDateTimeRoundedToTheHour = momentOfCurrentDate.minute() || momentOfCurrentDate.second() || momentOfCurrentDate.millisecond() ? momentOfCurrentDate.add(1, 'hour').startOf('hour').toDate() : momentOfCurrentDate.startOf('hour').toDate();

            $('#jobMatchScheduleStartDate').datepicker({
                format : "M d, yyyy",
                startDate: currentDateTimeRoundedToTheHour,
                weekStart: 1,
                autoclose: true
            });
            // set current date as default value
            $("#jobMatchScheduleStartDate").datepicker("setDate", currentDate);
            $("#jobMatchScheduleStartDate").datepicker("update", $filter('fourdotfiveDateFormat')(currentDate));

            $('#jobMatchScheduleEndDate').datepicker({
                format : "M d, yyyy",
                startDate: currentDateTimeRoundedToTheHour,
                weekStart: 1,
                autoclose: true
            });

            $("#jobMatchScheduleEndDate").datepicker("setDate", currentDateTimeRoundedToTheHour);
            $("#jobMatchScheduleEndDate").datepicker("update", $filter('fourdotfiveDateFormat')(currentDateTimeRoundedToTheHour));

            $('#jobMatchScheduleStartTime').timepicker({
                minuteStep : 5,
                showSeconds : false,
                showMeridian : true,
                disableFocus : true,
                defaultTime: currentDateTimeRoundedToTheHour
            });

            $('#jobMatchScheduleEndTime').timepicker({
                minuteStep : 5,
                showSeconds : false,
                showMeridian : true,
                disableFocus : true,
                defaultTime: moment(currentDateTimeRoundedToTheHour).add(30, 'm').toDate()
            });

            $('#jobMatchScheduleStartDate').datepicker().on('changeDate', function(e) {
                _validateAndCorrectStartDateTimeAndEndDateTime();
            });

            $('#jobMatchScheduleEndDate').datepicker().on('changeDate', function(e) {
                _validateAndCorrectStartDateTimeAndEndDateTime();
            });

            $('#jobMatchScheduleStartTime').timepicker().on('changeTime.timepicker', function(e) {
                _validateAndCorrectStartDateTimeAndEndDateTime();
            });

            $('#jobMatchScheduleEndTime').timepicker().on('changeTime.timepicker', function(e) {
                _validateAndCorrectStartDateTimeAndEndDateTime();
            });

        });
    }

    function getUsers(searchTerm){
        var searchObject = {
            'companyOrClientBuId' : vm.scheduleData.isCompany ? vm.scheduleData.company : vm.clientId,
            'email': searchTerm,
            'firstName': searchTerm,
            'lastName': searchTerm
        };
        return userService.getFilteredUsersPromiseForTypeAhead(searchObject).then(function (data) {
            angular.forEach(data, function(val, key){
                val.displayString = val.firstName + ' ' + val.lastName + ' - ' + val.email;
            });
            return data;
        });
    }

    function userSelected(item){
        vm.scheduleData.userId = item.id;
        vm.scheduleData.firstName = item.firstName;
        vm.scheduleData.lastName = item.lastName;
        vm.scheduleData.emailAddress = item.email;
        vm.scheduleData.title = item.title;
        if(_.isNull(item.mobilephone)){
            vm.scheduleData.phoneNumber = '-';
        }else{
            vm.scheduleData.phoneNumber = item.mobilephone;
        }
        if(!_.isNull(item.role) && item.role.length > 0){
            vm.scheduleData.role = item.role[0].description;
        }
        vm.scheduleData.department = item.department;
    }

    function _resetScheduleData() {
        vm.scheduleData.userId = null;
        vm.scheduleData.firstName = '';
        vm.scheduleData.lastName = '';
        vm.scheduleData.emailAddress = '';
        vm.scheduleData.title = '';
        vm.scheduleData.phoneNumber = '';
        vm.scheduleData.department = '';
    }

    function userTypeChanged(){
        // reset the schedule data.
        _resetScheduleData();
        vm.selectedUserObjectsArray = [];
    }

    function companyClientUserTypeChanged() {
        // reset the schedule data.
        _resetScheduleData();
        vm.selectedUserObjectsArray = [];
    }

    function userTypeAheadChanged() {
        if(vm.selectedUserObjectsArray.length > 0){
            vm.selectedUserObject = vm.selectedUserObjectsArray[0];
            vm.userSelected(vm.selectedUserObject);
        }else{
            _resetScheduleData();
        }
    }
}

ScheduleJobMatchItemController.$inject = ['$rootScope','$timeout','$filter','jobMatchService','moment','dateTimeUtilityService','userService','browserService', 'alertsAndNotificationsService'];

fourdotfivedirectives.controller('ScheduleJobMatchItemController', ScheduleJobMatchItemController);