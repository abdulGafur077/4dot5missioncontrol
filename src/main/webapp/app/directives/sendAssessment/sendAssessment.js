fourdotfivedirectives.directive('sendAssessment', function sendAssessment() {
    return {
        restrict: 'E',
        scope: {},
        bindToController: {
            jobMatchId: '@',
            candidateId: '@',
            assessmentType: '@',
            saveCallback: '&',
            assessmentSendMode: '@'
        },
        controller: 'SendAssessmentController',
        controllerAs: 'sendAssessment',
        templateUrl: 'app/directives/sendAssessment/send-assessment.html'
    }
});