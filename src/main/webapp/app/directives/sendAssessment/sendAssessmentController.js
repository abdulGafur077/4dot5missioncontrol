/*
  * This is the controller for the send assessment directive
 */

function SendAssessmentController($rootScope, $uibModal, $timeout, $filter, moment, dateTimeUtilityService, jobMatchService, jobService, assessmentService, CandidateJobMatchCard, alertsAndNotificationsService) {
    var vm = this;

    //variables
    vm.assessmentData = {
        jobMatchId: null,
        notes: '',
        startDateTime: null,
        endDateTime: null,
        accessTime: null,
        reminder: 2,
        timeToComplete: null,
        //timeZoneId: null
        timeZoneName: null
    };
    vm.sendAssessmentLabel = '';
    vm.candidateJobMatch = {};
    vm.accessTimePreferences = {};
    vm.remindAfterDays = [];
    vm.sendAssessmentLicensePreferenceEnum = null;
    vm.assessmentDetails = null;
    vm.previousStateDateTime = null;
    //flags
    vm.savingFlag = false;
    vm.loadingAccessTimeOptionsFlag = false;
    vm.showAccessTimeOptionsFlag = false;
    vm.reminderErrorFlag = false;
    //methods
    vm.init = init;
    vm.sendAssessment = sendAssessment;
    vm.getCandidateJobMatchCardInfo = getCandidateJobMatchCardInfo;
    vm.getAccessTimePreferences = getAccessTimePreferences;
    vm.accessTimeTypeChanged = accessTimeTypeChanged;
    vm.timeZoneChanged = timeZoneChanged;
    vm.reminderChanged = reminderChanged;

    vm.init();

    function init() {
        vm.userId = $rootScope.userDetails.id;
        vm.assessmentData.jobMatchId = vm.jobMatchId;
        vm.loadingAccessTimeOptionsFlag = true;
        for(var i=0;i<=31;i++){
            vm.remindAfterDays.push({
                id: i,
                text: '' + i
            });
        }
        if(_.isNull(vm.assessmentSendMode)){
            vm.assessmentSendMode = 'send';
        }
        if(vm.assessmentSendMode == 'resend'){
            var assessmentDetailsObject = {};
            assessmentDetailsObject.jobMatchId = vm.jobMatchId;
            assessmentDetailsObject.assessmentType = vm.assessmentType;
            assessmentService.getAssessmentDetails(assessmentDetailsObject, function (data) {
                vm.assessmentDetails = data;
                if(!_.isNull(vm.assessmentDetails.expiredDateTime)){
                    vm.assessmentDetails.expiredDateTime = new Date(vm.assessmentDetails.expiredDateTime);
                }
                if(!_.isNull(vm.assessmentDetails.cancelledDateTime)){
                    vm.assessmentDetails.cancelledDateTime = new Date(vm.assessmentDetails.cancelledDateTime);
                }
                if(!_.isNull(vm.assessmentDetails.completedDateTime)){
                    vm.assessmentDetails.completedDateTime = new Date(vm.assessmentDetails.completedDateTime);
                }
                if(!_.isNull(vm.assessmentDetails.sentDateTime)){
                    vm.assessmentDetails.sentDateTime = new Date(vm.assessmentDetails.sentDateTime);
                }
                if(!_.isNull(vm.assessmentDetails.suspendedDateTime)){
                    vm.assessmentDetails.suspendedDateTime = new Date(vm.assessmentDetails.suspendedDateTime);
                }
                if(!_.isNull(vm.assessmentDetails.terminatedDateTime)){
                    vm.assessmentDetails.terminatedDateTime = new Date(vm.assessmentDetails.terminatedDateTime);
                }
                if(!_.isNull(vm.assessmentDetails.testLeftDateTime)){
                    vm.assessmentDetails.testLeftDateTime = new Date(vm.assessmentDetails.testLeftDateTime);
                }
                if(vm.assessmentDetails.assessmentLinkStatus == 'Expired' && !_.isNull(vm.assessmentDetails.expiredDateTime)){
                    vm.previousStateDateTime = vm.assessmentDetails.expiredDateTime;
                }else if(vm.assessmentDetails.assessmentLinkStatus == 'Suspended' && !_.isNull(vm.assessmentDetails.suspendedDateTime)){
                    vm.previousStateDateTime = vm.assessmentDetails.suspendedDateTime;
                }else if(vm.assessmentDetails.assessmentLinkStatus == 'Terminated' && !_.isNull(vm.assessmentDetails.terminatedDateTime)){
                    vm.previousStateDateTime = vm.assessmentDetails.terminatedDateTime;
                }else if(vm.assessmentDetails.assessmentLinkStatus == 'Test Left' && !_.isNull(vm.assessmentDetails.testLeftDateTime)){
                    vm.previousStateDateTime = vm.assessmentDetails.testLeftDateTime;
                }else if(vm.assessmentDetails.assessmentLinkStatus == 'Completed' && !_.isNull(vm.assessmentDetails.completedDateTime)){
                    vm.previousStateDateTime =  vm.assessmentDetails.completedDateTime;
                }else{
                    vm.previousStateDateTime = '';
                }
                if(vm.assessmentDetails.assessmentLinkStatus == 'Expired' || vm.assessmentDetails.assessmentLinkStatus == 'Suspended' || vm.assessmentDetails.assessmentLinkStatus == 'Test Left' || vm.assessmentDetails.assessmentLinkStatus == 'Terminated' || vm.assessmentDetails.assessmentLinkStatus == 'Completed'){
                    $uibModal.open({
                        animation: false,
                        ariaLabelledBy: 'modal-title',
                        ariaDescribedBy: 'modal-body',
                        templateUrl: 'app/directives/sendAssessment/send-assessment-confirm-resend-modal.html',
                        backdrop: 'static',
                        keyboard: false,
                        controller: ['$uibModalInstance','assessmentDetails','previousStateDateTime','closeCallback', function ($uibModalInstance, assessmentDetails, previousStateDateTime, closeCallback) {
                            var vm = this;
                            vm.assessmentDetails = assessmentDetails;
                            vm.label = 'Confirm Resend';
                            vm.previousStateDateTime = previousStateDateTime;
                            vm.resendAction = function(flag){
                                $uibModalInstance.dismiss('cancel');
                                closeCallback(flag, vm.resendReason, vm.otherReasonNotes);
                            };
                        }],
                        controllerAs: 'resendConfirmationModal',
                        size: 'lg',
                        resolve:{
                            assessmentDetails: function () {
                                return vm.assessmentDetails;
                            },
                            closeCallback: function () {
                                return function(resendFlag, reason, otherReasonNotes){
                                    if(resendFlag){
                                        if(!_.isUndefined(reason)){
                                            if(reason != 'Other'){
                                                vm.assessmentData.notes = reason;
                                            }else if(!_.isUndefined(otherReasonNotes)){
                                                vm.assessmentData.notes = otherReasonNotes;
                                            }
                                        }
                                    }else{
                                        if(!_.isUndefined(vm.saveCallback)){
                                            vm.saveCallback();
                                        }
                                    }
                                };
                            },
                            previousStateDateTime: function(){
                                return vm.previousStateDateTime;
                            }
                        }
                    });
                }
            }, function(error){
                if ($rootScope.isOnline) {
                    alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                }
            });
        }
        switch(vm.assessmentType){
            case 'Technical':
                vm.sendAssessmentLabel = 'Technical Assessment';
                vm.sendAssessmentLicensePreferenceEnum = 'TECH_ASSESSMENT';
                break;
            case 'Value':
                vm.sendAssessmentLabel = 'Value Assessment';
                vm.sendAssessmentLicensePreferenceEnum = 'VALUE_ASSESSMENT';
                break;
        }
        vm.assessmentData.notes = 'Sending ' + vm.sendAssessmentLabel;
        if(vm.assessmentSendMode == 'resend'){
            vm.assessmentData.notes = 'Resending ' + vm.sendAssessmentLabel;
        }
        vm.candidateJobMatch = new CandidateJobMatchCard();
        vm.getCandidateJobMatchCardInfo(function(){
            vm.getAccessTimePreferences(function () {
                if(_.isNull(vm.accessTimePreferences.timeToComplete)){
                    // default time to complete, if it is null
                    vm.accessTimePreferences.timeToComplete = 2;
                }
                if(_.isNull(vm.accessTimePreferences.reminderInterval)){
                    // default reminder interval, if it is null
                    vm.accessTimePreferences.reminderInterval = 2;
                }
                vm.assessmentData.reminder = vm.accessTimePreferences.reminderInterval;
                // initialize all access time options to be false
                vm.accessTimePreferences.anyTime = false;
                vm.accessTimePreferences.timeBound = false;
                vm.accessTimePreferences.slotWise = false;
                // TODO: remove jQuery and use data binding to set the default value.
                $('#reminderInDays').val(vm.assessmentData.reminder);
                var accessTimeOptionsToShowCount = 0;
                angular.forEach(vm.accessTimePreferences.accessTimeDtos, function(val, key) {
                    if (val.accessTimeLabel == 'Any Time') {
                        if (val.accessTimeEnabled && val.accessTimeValue) {
                            vm.accessTimePreferences.anyTime = true;
                            accessTimeOptionsToShowCount++;
                        }
                    } else if (val.accessTimeLabel == 'Time Bound') {
                        if (val.accessTimeEnabled && val.accessTimeValue) {
                            vm.accessTimePreferences.timeBound = true;
                            accessTimeOptionsToShowCount++;
                        }
                    } else if (val.accessTimeLabel == 'Slot Wise') {
                        if (val.accessTimeEnabled && val.accessTimeValue) {
                            vm.accessTimePreferences.slotWise = true;
                            accessTimeOptionsToShowCount++;
                        }
                    }
                });
                if(accessTimeOptionsToShowCount == 0){
                    vm.showAccessTimeOptionsFlag = false;
                    // default to any time if all the options are sent as 'false'
                    vm.assessmentData.accessTime = 'anyTime';
                }else if(accessTimeOptionsToShowCount == 1){
                    vm.showAccessTimeOptionsFlag = false;
                    if(vm.accessTimePreferences.anyTime){
                        vm.assessmentData.accessTime = 'anyTime';
                    }else if(vm.accessTimePreferences.timeBound){
                        vm.assessmentData.accessTime = 'timeBound';
                    }else if(vm.accessTimePreferences.slotWise){
                        vm.assessmentData.accessTime = 'slotWise';
                    }
                }else if(accessTimeOptionsToShowCount > 1){
                    vm.showAccessTimeOptionsFlag = true;
                    if(vm.accessTimePreferences.anyTime){
                        vm.assessmentData.accessTime = 'anyTime';
                    }
                    if(vm.accessTimePreferences.timeBound){
                        _initializeDateAndTimePickersForTimeBound();
                    }
                    if(vm.accessTimePreferences.slotWise){
                        _initializeDateAndTimePickersForSlotWise();
                    }
                    // default to the first available option
                    if(vm.accessTimePreferences.anyTime){
                        vm.assessmentData.accessTime = 'anyTime';
                    }else{
                        // time bound should be available since anyTime is not available
                        vm.assessmentData.accessTime = 'timeBound';
                    }
                }
                vm.loadingAccessTimeOptionsFlag = false;
            });
        });

    }

    function sendAssessment() {
        if(!vm.sendAssessmentForm.$invalid && !vm.reminderErrorFlag){
            vm.savingFlag = true;
            var assessmentDataForSave = angular.copy(vm.assessmentData);
            if(vm.assessmentData.accessTime == 'anyTime'){
                assessmentDataForSave.startDateTime = null;
                assessmentDataForSave.endDateTime = null;
            }else if(vm.assessmentData.accessTime == 'timeBound'){
                assessmentDataForSave.startDateTime = dateTimeUtilityService.getDateTimeInAPIFormat(new Date(vm.timeBoundStartDate + ' ' + vm.timeBoundStartTime));
                assessmentDataForSave.endDateTime = dateTimeUtilityService.getDateTimeInAPIFormat(new Date(vm.timeBoundEndDate + ' ' + vm.timeBoundEndTime));
            }else if(vm.assessmentData.accessTime == 'slotWise'){
                assessmentDataForSave.startDateTime = dateTimeUtilityService.getDateTimeInAPIFormat(new Date(vm.slotWiseStartDate + ' ' + vm.slotWiseStartTime));
                assessmentDataForSave.endDateTime = dateTimeUtilityService.getDateTimeInAPIFormat(new Date(vm.slotWiseEndDate + ' ' + vm.slotWiseEndTime));
            }
            if(vm.assessmentSendMode == 'send'){
                jobMatchService.sendAssessment(vm.assessmentType, assessmentDataForSave, function (data) {
                    vm.savingFlag = false;
                    if(!_.isUndefined(vm.saveCallback)){
                        vm.saveCallback();
                    }
                },function (error) {
                    vm.savingFlag = false;
                    if ($rootScope.isOnline) {
                        alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                    }
                });
            }else{
                assessmentDataForSave.testType = vm.assessmentType;
                jobMatchService.resendAssessment(assessmentDataForSave, function (data) {
                    vm.savingFlag = false;
                    if(!_.isUndefined(vm.saveCallback)){
                        vm.saveCallback();
                    }
                },function (error) {
                    vm.savingFlag = false;
                    if ($rootScope.isOnline) {
                        alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                    }
                });
            }

        }
    }

    function getCandidateJobMatchCardInfo(successCallback){
        jobMatchService.getCandidateJobMatchCard(vm.jobMatchId, function (data) {
            angular.merge(vm.candidateJobMatch,data);
            vm.candidateJobMatch = _massageCandidateJobMatchData(vm.candidateJobMatch);
            if(successCallback){
                successCallback();
            }
        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }

    function _massageCandidateJobMatchData(candidateJobMatch){
        candidateJobMatch.name = candidateJobMatch.firstName + ' ' + candidateJobMatch.lastName;
        if(candidateJobMatch.jobMatchRequisitionNum == null){
            candidateJobMatch.jobMatchRequisitionNum = '-';
        }
        return candidateJobMatch;
    }

    function getAccessTimePreferences(successCallback){
        jobService.getAssessmentAccessTime(vm.candidateJobMatch.jobMatchJobId, vm.sendAssessmentLicensePreferenceEnum, function (data) {
            vm.accessTimePreferences = data;
            if(successCallback){
                successCallback();
            }
        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }

    function accessTimeTypeChanged(){
        // make sure the latest time is shown in the chosen access time option according to the chosen time zone.
        vm.timeZoneChanged();
    }

    function reminderChanged(){
        if(vm.assessmentData.accessTime == 'timeBound' || vm.assessmentData.accessTime == 'slotWise'){
            if(vm.assessmentData.accessTime == 'timeBound'){
                var currentStartDateTime = new Date(vm.timeBoundStartDate + ' ' + vm.timeBoundStartTime);
                var momentOfCurrentStartDateTime = moment(currentStartDateTime);
                var currentEndDateTime = new Date(vm.timeBoundEndDate + ' ' + vm.timeBoundEndTime);
                var momentOfCurrentEndDateTime = moment(currentEndDateTime);
            }else{
                var momentOfCurrentStartDateTime = moment(new Date(vm.slotWiseStartDate));
                var momentOfCurrentEndDateTime = moment(new Date(vm.slotWiseEndDate));
            }
            var differenceInDays = momentOfCurrentEndDateTime.diff(momentOfCurrentStartDateTime, 'days', true);
            if(vm.assessmentData.reminder >= differenceInDays){
                vm.reminderErrorFlag = true;
            }else{
                vm.reminderErrorFlag = false;
            }
        }
    }

    function timeZoneChanged() {
        if(vm.assessmentData.accessTime == 'timeBound'){
            var currentDateTimeStringInSelectedTimeZone = moment().tz(vm.assessmentData.timeZoneName).format("YYYY-MM-DDTHH:mm:ss.SSS");
            var currentDate = new Date(currentDateTimeStringInSelectedTimeZone);
            var momentOfCurrentDate = moment(currentDate);
            var remainderWithClosestQuarter = momentOfCurrentDate.minute() % 15;
            var momentOfCurrentDateTimeFlooredToClosetQuarter = moment(momentOfCurrentDate).subtract(remainderWithClosestQuarter,'m');
            var momentOfDefaultEndDateTime = moment(momentOfCurrentDateTimeFlooredToClosetQuarter).add(vm.accessTimePreferences.timeToComplete,'d');

            $("#timeBoundStartDate").datepicker("setDate", momentOfCurrentDateTimeFlooredToClosetQuarter.toDate());
            $("#timeBoundStartDate").datepicker("setStartDate", momentOfCurrentDateTimeFlooredToClosetQuarter.toDate());
            $("#timeBoundStartDate").datepicker("update", $filter('fourdotfiveDateFormat')(momentOfCurrentDateTimeFlooredToClosetQuarter.toDate()));

            $("#timeBoundEndDate").datepicker("setDate", momentOfDefaultEndDateTime.toDate());
            // allowed start date on the calendar of end date should still be the same as start date.
            $("#timeBoundEndDate").datepicker("setStartDate", momentOfCurrentDateTimeFlooredToClosetQuarter.toDate());
            $("#timeBoundEndDate").datepicker("update", $filter('fourdotfiveDateFormat')(momentOfDefaultEndDateTime.toDate()));

            $("#timeBoundStartTime").timepicker("setTime", _getTimeStringInAMPMFormat(momentOfCurrentDateTimeFlooredToClosetQuarter.toDate()));
            $('#timeBoundEndTime').timepicker("setTime", "11:59 PM");
            // trigger a reminder change to check if remind after days is in bound
            vm.reminderChanged();
        }else if(vm.assessmentData.accessTime == 'slotWise'){
            var currentDateTimeStringInSelectedTimeZone = moment().tz(vm.assessmentData.timeZoneName).format("YYYY-MM-DDTHH:mm:ss.SSS");
            var currentDate = new Date(currentDateTimeStringInSelectedTimeZone);
            var momentOfDefaultEndDateTime = moment(currentDate).add(vm.accessTimePreferences.timeToComplete,'d');

            $("#slotWiseStartDate").datepicker("setDate", currentDate);
            $("#slotWiseStartDate").datepicker("setStartDate", currentDate);
            $("#slotWiseStartDate").datepicker("update", $filter('fourdotfiveDateFormat')(currentDate));

            $("#slotWiseEndDate").datepicker("setDate", momentOfDefaultEndDateTime.toDate());
            // allowed start date on the calendar of end date should still be the same as start date.
            $("#slotWiseEndDate").datepicker("setStartDate", currentDate);
            $("#slotWiseEndDate").datepicker("update", $filter('fourdotfiveDateFormat')(momentOfDefaultEndDateTime.toDate()));

            $("#slotWiseStartTime").timepicker("setTime", "09:00 AM");
            $('#slotWiseEndTime').timepicker("setTime", "12:00 PM");
            // trigger a reminder change to check if remind after days is in bound
            vm.reminderChanged();
        }
    }

    function _initializeDateAndTimePickersForTimeBound(){
        $(document).ready(function () {
            var currentDateTimeStringInSelectedTimeZone = moment().tz(vm.assessmentData.timeZoneName).format("YYYY-MM-DDTHH:mm:ss.SSS");
            var currentDate = new Date(currentDateTimeStringInSelectedTimeZone);
            var momentOfCurrentDate = moment(currentDate);
            var remainderWithClosestQuarter = momentOfCurrentDate.minute() % 15;
            var momentOfCurrentDateTimeFlooredToClosetQuarter = moment(momentOfCurrentDate).subtract(remainderWithClosestQuarter,'m');
            var momentOfDefaultEndDateTime = moment(momentOfCurrentDateTimeFlooredToClosetQuarter).add(vm.accessTimePreferences.timeToComplete,'d');

            $('#timeBoundStartDate').datepicker({
                format : "M d, yyyy",
                startDate: momentOfCurrentDateTimeFlooredToClosetQuarter.toDate(),
                weekStart: 1,
                autoclose: true
            });
            // set current date as default value
            $("#timeBoundStartDate").datepicker("setDate", momentOfCurrentDateTimeFlooredToClosetQuarter.toDate());
            $("#timeBoundStartDate").datepicker("update", $filter('fourdotfiveDateFormat')(momentOfCurrentDateTimeFlooredToClosetQuarter.toDate()));

            $('#timeBoundEndDate').datepicker({
                format : "M d, yyyy",
                startDate: momentOfCurrentDateTimeFlooredToClosetQuarter.toDate(),
                weekStart: 1,
                autoclose: true
            });

            $("#timeBoundEndDate").datepicker("setDate", momentOfDefaultEndDateTime.toDate());
            $("#timeBoundEndDate").datepicker("update", $filter('fourdotfiveDateFormat')(momentOfDefaultEndDateTime.toDate()));

            $('#timeBoundStartTime').timepicker({
                minuteStep : 5,
                showSeconds : false,
                showMeridian : true,
                disableFocus : true,
                defaultTime: _getTimeStringInAMPMFormat(momentOfCurrentDateTimeFlooredToClosetQuarter.toDate())
            });

            $('#timeBoundEndTime').timepicker({
                minuteStep : 5,
                showSeconds : false,
                showMeridian : true,
                disableFocus : true,
                defaultTime: '11:59 PM'
            });

            $('#timeBoundStartDate').datepicker().on('changeDate', function(e) {
                _validateAndCorrectStartDateTimeAndEndDateTimeForTimeBound();
                $timeout(function(){
                    // trigger a reminder change to check if remind after days is in bound
                    vm.reminderChanged();
                }, 100);
            });

            $('#timeBoundEndDate').datepicker().on('changeDate', function(e) {
                _validateAndCorrectStartDateTimeAndEndDateTimeForTimeBound();
                $timeout(function(){
                    // trigger a reminder change to check if remind after days is in bound
                    vm.reminderChanged();
                }, 100);
            });

            $('#timeBoundStartTime').timepicker().on('changeTime.timepicker', function(e) {
                _validateAndCorrectStartDateTimeAndEndDateTimeForTimeBound();
                $timeout(function(){
                    // trigger a reminder change to check if remind after days is in bound
                    vm.reminderChanged();
                }, 100);
            });

            $('#timeBoundEndTime').timepicker().on('changeTime.timepicker', function(e) {
                _validateAndCorrectStartDateTimeAndEndDateTimeForTimeBound();
                $timeout(function(){
                    // trigger a reminder change to check if remind after days is in bound
                    vm.reminderChanged();
                }, 100);
            });

        });
    }

    function _initializeDateAndTimePickersForSlotWise(){
        $(document).ready(function () {
            var currentDateTimeStringInSelectedTimeZone = moment().tz(vm.assessmentData.timeZoneName).format("YYYY-MM-DDTHH:mm:ss.SSS");
            var currentDate = new Date(currentDateTimeStringInSelectedTimeZone);
            var momentOfDefaultEndDateTime = moment(currentDate).add(vm.accessTimePreferences.timeToComplete,'d');

            $('#slotWiseStartDate').datepicker({
                format : "M d, yyyy",
                startDate: currentDate,
                weekStart: 1,
                autoclose: true
            });
            // set current date as default value
            $("#slotWiseStartDate").datepicker("setDate", currentDate);
            $("#slotWiseStartDate").datepicker("update", $filter('fourdotfiveDateFormat')(currentDate));

            $('#slotWiseEndDate').datepicker({
                format : "M d, yyyy",
                startDate: currentDate,
                weekStart: 1,
                autoclose: true
            });

            $("#slotWiseEndDate").datepicker("setDate", momentOfDefaultEndDateTime.toDate());
            $("#slotWiseEndDate").datepicker("update", $filter('fourdotfiveDateFormat')(momentOfDefaultEndDateTime.toDate()));

            $('#slotWiseStartTime').timepicker({
                minuteStep : 5,
                showSeconds : false,
                showMeridian : true,
                disableFocus : true,
                defaultTime: '09:00 AM'
            });

            $('#slotWiseEndTime').timepicker({
                minuteStep : 5,
                showSeconds : false,
                showMeridian : true,
                disableFocus : true,
                defaultTime: '12:00 PM'
            });

            $('#slotWiseStartDate').datepicker().on('changeDate', function(e) {
                _validateAndCorrectStartDateAndEndDateForSlotWise();
                $timeout(function(){
                    // trigger a reminder change to check if remind after days is in bound
                    vm.reminderChanged();
                }, 100);
            });

            $('#slotWiseEndDate').datepicker().on('changeDate', function(e) {
                _validateAndCorrectStartDateAndEndDateForSlotWise();
                $timeout(function(){
                    // trigger a reminder change to check if remind after days is in bound
                    vm.reminderChanged();
                }, 100);
            });

            $('#slotWiseStartTime').timepicker().on('changeTime.timepicker', function(e) {
                _validateAndCorrectStartTimeAndEndTimeForSlotWise();
            });

            $('#slotWiseEndTime').timepicker().on('changeTime.timepicker', function(e) {
                _validateAndCorrectStartTimeAndEndTimeForSlotWise();
            });

        });
    }

    function _getTimeStringInAMPMFormat(date){
        return moment(date).format("hh:mm A");
    }

    function _validateAndCorrectStartDateTimeAndEndDateTimeForTimeBound(){
        var currentDateTimeStringInSelectedTimeZone = moment().tz(vm.assessmentData.timeZoneName).format("YYYY-MM-DDTHH:mm:ss.SSS");
        var momentOfCurrentDateTime = moment(currentDateTimeStringInSelectedTimeZone);
        var currentStartDateTime = new Date(vm.timeBoundStartDate + ' ' + vm.timeBoundStartTime);
        var momentOfCurrentStartDateTime = moment(currentStartDateTime);
        var currentEndDateTime = new Date(vm.timeBoundEndDate + ' ' + vm.timeBoundEndTime);
        // if the start date is same as the current date
        var startDateAndCurrentDateAreSameFlag = momentOfCurrentStartDateTime.isSame(momentOfCurrentDateTime, 'day');
        // if the start hours are less than current hours
        var startHoursLessThanCurrentHoursFlag = momentOfCurrentStartDateTime.isBefore(momentOfCurrentDateTime,'hour');
        var remainderWithClosestQuarter = momentOfCurrentDateTime.minute() % 15;
        var currentDateTimeFlooredToClosetQuarter = momentOfCurrentDateTime.subtract(remainderWithClosestQuarter,'m').toDate();
        // if the hours are equal but minutes are less than current date time floored to closest quarter minutes.
        var hoursEqualButStartMinutesLessThanCurrentMinutesFlag = ( momentOfCurrentStartDateTime.isSame(momentOfCurrentDateTime,'hour') && momentOfCurrentStartDateTime.isBefore(currentDateTimeFlooredToClosetQuarter,'minute') );
        if(startDateAndCurrentDateAreSameFlag && (startHoursLessThanCurrentHoursFlag || hoursEqualButStartMinutesLessThanCurrentMinutesFlag)){
            $("#timeBoundStartTime").timepicker("setTime", _getTimeStringInAMPMFormat(currentDateTimeFlooredToClosetQuarter));
        }else if(currentStartDateTime >= currentEndDateTime){
            // after the change to 'start time', check if the existing 'end time' component keeps 'end date time' as greater than 'start date time'
            // if not, make the 'end date time' as 'time to complete' days greater than currentStartDateTime
            var defaultEndDateTime = moment(currentStartDateTime).add(vm.accessTimePreferences.timeToComplete,'d').toDate();
            $("#timeBoundEndDate").datepicker("setDate", defaultEndDateTime);
            $("#timeBoundEndDate").datepicker("update", $filter('fourdotfiveDateFormat')(defaultEndDateTime));
            $('#timeBoundEndTime').timepicker("setTime", '11:59 PM');
        }
        // set start date of end date calendar to be current start date
        $("#timeBoundEndDate").datepicker("setStartDate", currentStartDateTime);
    }

    function _validateAndCorrectStartDateAndEndDateForSlotWise(){
        var momentOfCurrentStartDate = moment(new Date(vm.slotWiseStartDate));
        var momentOfCurrentEndDate = moment(new Date(vm.slotWiseEndDate));
        if(momentOfCurrentEndDate.isBefore(momentOfCurrentStartDate, 'day')){
            // make the 'end date' as 'time to complete' days greater than 'start date'
            var defaultEndDate = momentOfCurrentStartDate.add(vm.accessTimePreferences.timeToComplete,'d').toDate();
            $("#slotWiseEndDate").datepicker("setDate", defaultEndDate);
            $("#slotWiseEndDate").datepicker("update", $filter('fourdotfiveDateFormat')(defaultEndDate));
        }
        // set start date of end date calendar to be current start date
        $("#slotWiseEndDate").datepicker("setStartDate", momentOfCurrentStartDate.toDate());
    }

    function _validateAndCorrectStartTimeAndEndTimeForSlotWise(){
        // use start date for the date part. Otherwise we can't parse date.
        var momentOfCurrentStartTime = moment(new Date(vm.slotWiseStartDate + " " + vm.slotWiseStartTime));
        var momentOfCurrentEndTime = moment(new Date(vm.slotWiseStartDate + " " + vm.slotWiseEndTime));
        if(momentOfCurrentEndTime.isBefore(momentOfCurrentStartTime)){
            // make the 'end time' as 1 hour greater than 'start time'
            var defaultEndTime = momentOfCurrentStartTime.add(1,'h').toDate();
            $('#slotWiseEndTime').timepicker("setTime", _getTimeStringInAMPMFormat(defaultEndTime));
        }
    }
}

SendAssessmentController.$inject = ['$rootScope','$uibModal','$timeout','$filter','moment','dateTimeUtilityService','jobMatchService','jobService','assessmentService','CandidateJobMatchCard', 'alertsAndNotificationsService'];

fourdotfivedirectives.controller('SendAssessmentController', SendAssessmentController);