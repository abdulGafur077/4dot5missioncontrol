fourdotfivedirectives.directive('setCandidateAvailability', function setCandidateAvailability() {
    return {
        restrict: 'E',
        scope: {},
        bindToController: {
            candidateId: '@',
            availabilityType: '@',
            saveCallback: '&'
        },
        controller: 'SetCandidateAvailabilityController',
        controllerAs: 'setCandidateAvailability',
        templateUrl: 'app/directives/setCandidateAvailability/set-candidate-availability.html'
    }
});