/*
  * This is the controller for the set candidate availability directive
 */

function SetCandidateAvailabilityController($filter,candidateService,dateTimeUtilityService,alertsAndNotificationsService) {
    var vm = this;

    vm.availabilityDate = null;
    vm.availabilityObject = {};
    // methods
    vm.init = init;
    vm.setCandidateAvailability = setCandidateAvailability;
    //flags
    vm.savingFlag = false;

    vm.init();

    function init(){
        vm.availabilityObject.candidateId = vm.candidateId;
        if(vm.availabilityType == 'availability'){

        }else if(vm.availabilityType == 'jobLeft'){

        }
        _initializeDatePicker();
    }

    function _initializeDatePicker(){
        $(document).ready(function () {
            var currentDate = new Date();

            if(vm.availabilityType == 'availability'){
                $('#availabilityDate').datepicker({
                    format : "M d, yyyy",
                    startDate: currentDate,
                    weekStart: 1,
                    autoclose: true
                });
            }else if(vm.availabilityType == 'jobLeft'){
                $('#availabilityDate').datepicker({
                    format : "M d, yyyy",
                    endDate: currentDate,
                    weekStart: 1,
                    autoclose: true
                });
            }

            // set current date as default value
            $("#availabilityDate").datepicker("setDate", currentDate);
            $("#availabilityDate").datepicker("update", $filter('fourdotfiveDateFormat')(currentDate));

        });
    }

    function setCandidateAvailability(){
        if(!vm.setAvailabilityDateForm.$invalid){
            vm.savingFlag = true;
            if(vm.availabilityType == 'availability'){
                vm.availabilityObject.availabilityDate = dateTimeUtilityService.getDateTimeInAPIFormat(new Date(vm.availabilityDate));
                vm.availabilityObject.jobLeftDate = null;
            }else if(vm.availabilityType == 'jobLeft'){
                vm.availabilityObject.jobLeftDate = dateTimeUtilityService.getDateTimeInAPIFormat(new Date(vm.availabilityDate));
                vm.availabilityObject.availabilityDate = null;
            }
            candidateService.setCandidateAvailability(vm.availabilityObject, function(data){
                vm.savingFlag = false;
                if(vm.saveCallback){
                    vm.saveCallback();
                }
            }, function(error){
                vm.savingFlag = false;
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            });
        }
    }
}

SetCandidateAvailabilityController.$inject = ['$filter','candidateService','dateTimeUtilityService','alertsAndNotificationsService'];

fourdotfivedirectives.controller('SetCandidateAvailabilityController', SetCandidateAvailabilityController);