fourdotfivedirectives.directive('setJobMatchStatus', function setJobMatchStatus() {
    return {
        restrict: 'E',
        scope: {},
        bindToController: {
            jobMatchDetailsArrayString: '@',
            statusType: '@',
            saveCallback: '&'
        },
        controller: 'SetJobMatchStatusController',
        controllerAs: 'setJobMatchStatus',
        templateUrl: 'app/directives/setJobMatchStatus/set-job-match-status.html'
    }
});