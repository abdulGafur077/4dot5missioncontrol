/*
  * This is the controller for the set job match status directive
 */

function SetJobMatchStatusController($rootScope, jobMatchService) {
    var vm = this;

    //variables
    vm.jobMatchesDataArray = [];
    vm.jobMatchDetailsArray = [];
    vm.statusDisplayLabel = '';
    vm.notes = '';
    //flags
    vm.savingFlag = false;
    //methods
    vm.init = init;
    vm.setJobMatchStatus = setJobMatchStatus;
    vm.getCandidateJobMatchCardInfo = getCandidateJobMatchCardInfo;

    vm.init();

    function init() {
        vm.userId = $rootScope.userDetails.id;
        vm.jobMatchDetailsArray = JSON.parse(vm.jobMatchDetailsArrayString);
        switch(vm.statusType){
            case 'Not Interested':
                vm.statusDisplayLabel = 'Not Interested';
                break;
            case 'Release':
                vm.statusDisplayLabel = 'Released';
                break;
        }
        angular.forEach(vm.jobMatchDetailsArray, function (val, key) {
            var tempObject = {};
            tempObject.loadingFlag = true;
            tempObject.jobMatchId = val.jobMatchId;
            tempObject.candidateId = val.candidateId;
            tempObject.jobMatchData = {};
            vm.jobMatchesDataArray.push(tempObject);
        });
        angular.forEach(vm.jobMatchesDataArray, function (val, key) {
            vm.getCandidateJobMatchCardInfo(val.jobMatchId, val.candidateId, function (data) {
                val.loadingFlag = false;
                val.jobMatchData = data;
            });
        });
    }

    function setJobMatchStatus() {
        if(!vm.setStatusForm.$invalid){
            vm.savingFlag = true;
            var statusDataArray = [];
            angular.forEach(vm.jobMatchDetailsArray, function (val, key) {
               var tempObject = {};
               tempObject.jobMatchId = val.jobMatchId;
               tempObject.notes = vm.notes;
               statusDataArray.push(tempObject);
            });
            jobMatchService.setJobMatchStatus(vm.statusType,statusDataArray,function (data) {
                vm.savingFlag = false;
                if(!_.isUndefined(vm.saveCallback)){
                    vm.saveCallback();
                }
            },function (error) {

            });
        }
    }

    function getCandidateJobMatchCardInfo(jobMatchId, candidateId, successCallback){
        jobMatchService.getCandidateJobMatchCard(jobMatchId, function (data) {
            var candidateJobMatch = {};
            angular.merge(candidateJobMatch,data);
            candidateJobMatch = _massageCandidateJobMatchData(candidateJobMatch);
            if(successCallback){
                successCallback(candidateJobMatch);
            }
        }, function (error) {
            console.log('candidate get error - ', error);
            // do nothing.
        });
    }

    function _massageCandidateJobMatchData(candidateJobMatch){
        candidateJobMatch.name = candidateJobMatch.firstName + ' ' + candidateJobMatch.lastName;
        return candidateJobMatch;
    }
}

SetJobMatchStatusController.$inject = ['$rootScope', 'jobMatchService'];

fourdotfivedirectives.controller('SetJobMatchStatusController', SetJobMatchStatusController);