fourdotfivedirectives.directive('uploadResumes', function addCandidate(){
   return {
       restrict: 'E',
       scope: {},
       bindToController:{
           jobId: '@',
           cancelCallback: '&',
           completeCallback: '&'
       },
       controller: 'UploadResumesController',
       controllerAs: 'uploadResumes',
       templateUrl: 'app/directives/uploadResumes/upload-resumes.html'
   }
});