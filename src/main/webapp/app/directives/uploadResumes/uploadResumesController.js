/**
 * This controller deals with Upload Resume directive
 */

function UploadResumesController ($scope, $rootScope, $timeout, Resume, modalService, alertsAndNotificationsService, resumeService, jobMatchService, CandidateCount, MESSAGECONSTANTS) {

    var vm = this;
    vm.MESSAGECONSTANTS = MESSAGECONSTANTS;
    vm.sourceTypeList = [];
    vm.resumeList = [];
    vm.sourceType = null;
    vm.sourceTypeId = null;
    vm.uploadResumesResponse = null;
    vm.totalUploads = null;
    vm.candidatesList = [];
    vm.candidatesToAssociateList = [];
    vm.candidateCount = null;
    vm.createJobMatchesObject = null;
    // flags
    vm.disableUploadButtonFlag = true;
    vm.fileExistsFlag = false;
    vm.disablePreviousButtonFlag = false;
    vm.disableSaveButtonFlag = true;
    vm.disableNextButtonFlag = true;
    vm.disableCancelButtonFlag = false;
    vm.fileUploadInProgressFlag = false;
    vm.mandatoryFieldsError = false;
    vm.disableSourceTypesFlag = false;
    vm.resumeScoringDoneFlag = false;
    vm.scoringCandidatesFlag = false;
    vm.gettingCountsFlag = false;
    vm.jobContextExistsFlag = false;
    vm.associatingCandidatesFlag = false;
    vm.selectedAllCandidatesFlag = false;
    //controller methods
    vm.initialize = initialize;
    vm.createResumeUploadDropzone = createResumeUploadDropzone;
    vm.createUploadResumeWizard = createUploadResumeWizard;
    vm.candidateToAssociateToggle = candidateToAssociateToggle;
    vm.getAllSourceTypes = getAllSourceTypes;
    vm.uploadResumes = uploadResumes;
    vm.associateCandidates = associateCandidates;
    vm.toggleSelectAllCandidates = toggleSelectAllCandidates;
        // wizard steps
    vm.wizardCancel = wizardCancel;
        // save / update methods
    vm.updateActionButtons = updateActionButtons;
        // change methods
    vm.sourceTypeChanged = sourceTypeChanged;

    vm.initialize();

    function initialize() {
        //initialize the variables
        if(!_.isNull(vm.jobId) && vm.jobId != ''){
            vm.jobContextExistsFlag = true;
            vm.createJobMatchesObject = {
                requisitionId: vm.jobId,
                candidateIds: [],
                manuallyMatchedFromUI: true,
                jbiTransactionId: null
            };
        }
        vm.resumeList = [];
        // get the source types
        vm.getAllSourceTypes();
        // wait for changes to take effect and then create wizard
        $timeout(function () {
            //create the wizard
            vm.uploadResumesWizard = vm.createUploadResumeWizard();
            //initialize the drop zone configuration
            vm.resumeUploadDropzone = vm.createResumeUploadDropzone();
        }, 200);
    }


    /**
     * creates the resume upload drop zone.
     */
    function createResumeUploadDropzone(){
        var resumeUploadDropzone = new Dropzone("form#resumeUploadDropzone", {
            url: resumeService.getUploadUrl(),
            headers: {
                Authorization: 'Bearer ' + $rootScope.userDetails.accesstoken
            },
            paramName: function () {
                return "file";
            },
            acceptedFiles: ".pdf,.xlsx,.docx",
            maxFiles: 100,
            maxFilesize: 10,
            maxThumbnailFilesize: 10,
            addRemoveLinks: true,
            autoProcessQueue: false,
            uploadMultiple: true,
            parallelUploads: 100
        });

        // add event handlers for the drop zone
        resumeUploadDropzone.on('addedfile', function (file, response) {
            vm.fileExistsFlag = true;
            vm.updateActionButtons();
            $('.dz-progress').css({top: '70%'});
        });

        resumeUploadDropzone.on('removedfile', function (file, response) {
            if(vm.resumeUploadDropzone.files.length === 0){
                vm.fileExistsFlag = false;
                // enable adding of further files if it was disabled
                $(".dz-hidden-input").prop("disabled",true);
            }
            vm.updateActionButtons();
        });

        resumeUploadDropzone.on('sendingmultiple', function (file, xhr, formData) {
            // disable the upload button to avoid multiple clicks
            vm.disableUploadButtonFlag = true;
            // indicate that file upload is in progress
            vm.fileUploadInProgressFlag = true;
            var resumeUpload = {};
            resumeUpload.companyId = $rootScope.userDetails.company.companyId;
            resumeUpload.userId = $rootScope.userDetails.id;
            resumeUpload.resumeSourceTypeId = vm.sourceTypeId;
            resumeUpload.buIdList = [];
            //formData.append('resumeUpload', JSON.stringify(resumeUpload));
            formData.append("companyId",resumeUpload.companyId);
            formData.append("resumeSourceTypeId",resumeUpload.resumeSourceTypeId);
            formData.append("userId",resumeUpload.userId);
            formData.append("buIdList",resumeUpload.buIdList);
            if(!_.isNull(vm.jobId)){
                formData.append("jobId",vm.jobId);
            }
        });

        resumeUploadDropzone.on('successmultiple', function (file, response) {
            // remove file upload in progress flag
            vm.fileUploadInProgressFlag = false;
            //alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.UPLOAD.RESUME_UPLOAD_SUCCESS, 'success');
            vm.uploadResumesResponse = response;
            if(vm.jobContextExistsFlag){
                vm.createJobMatchesObject.jbiTransactionId = vm.uploadResumesResponse.transactionID;
            }
            vm.totalUploads = response.successCount + response.failureCount;
            // go over the list, merge each item into a resume object and add to resume list
            if(angular.isDefined(response.files) && response.files.length > 0){
                angular.forEach(response.files,function (val, key) {
                    if(val.isSuccess){
                        vm.resumeList.push(angular.merge(new Resume(),val));
                    }
                });
            }
            vm.updateActionButtons();
            // disable adding of further files
            $(".dz-hidden-input").prop("disabled",true);
        });

        return resumeUploadDropzone;
    }

    /**
     * create the wizard for the upload resume workflow
     */

    function createUploadResumeWizard() {
        var uploadResumesWizard = $("#uploadResumesWizard").wizard();

        $('#uploadResumesWizard').on('actionclicked.fu.wizard', function (evt, data) {
            if(data.direction == "next"){
                switch(data.step){
                    case 1:
                        // remove the remove button from the file.
                        $('.dz-remove').remove();
                        if(!vm.resumeScoringDoneFlag){
                            // disable previous and next (complete) button when processing
                            vm.disableNextButtonFlag = true;
                            vm.disablePreviousButtonFlag = true;
                            // initialize the loading flag
                            vm.scoringCandidatesFlag = true;
                            // run resume for scoring.
                            resumeService.runResumesForScoring(vm.uploadResumesResponse, vm.jobId, function (data) {
                                angular.forEach(data, function(val, key){
                                    val.name = val.firstName + ' ' + val.lastName;
                                    if(val.jobMatchCreatedForEligibleScore){
                                        val.addedSuccessfullyFlag = true;
                                        val.status = 'Candidate Added.';
                                    }else if(val.jobMatchNotCreatedForInEligibleScore){
                                        val.addedSuccessfullyFlag = false;
                                        val.status = 'Not added as score is less than job threshold score';
                                    }else if(val.autoMatchCountExceeded){
                                        val.addedSuccessfullyFlag = false;
                                        val.status = 'Not added as auto match count for the job was exceeded.';
                                    }
                                    // floor the match score to the lower integer value.
                                    val.score = Math.floor(parseFloat(val.score));
                                    val.selectedFlag = false;
                                    vm.candidatesList.push(val);
                                });
                                vm.scoringCandidatesFlag = false;
                                vm.resumeScoringDoneFlag = true;
                                // enable previous and next (complete) button after processing
                                vm.disableNextButtonFlag = false;
                                vm.disablePreviousButtonFlag = false;
                                //if(!vm.jobContextExistsFlag){
                                    _getCandidateCounts();
                                //}
                            }, function (error) {
                                // do nothing
                            });
                        }else{
                            //if(!vm.jobContextExistsFlag){
                                _getCandidateCounts();
                            //}
                        }
                        break;
                    case 2:
                        // do nothing
                        // complete click
                        // handled in finish event
                        break;
                    default:
                        break;
                }
            }else if(data.direction == "previous"){
                switch(data.step){
                    case 1:
                        // not possible
                        break;
                    case 2:
                        updateActionButtons();
                        break;
                    default:
                        break;
                }
            }
        });

        $('#uploadResumesWizard').on('finished.fu.wizard', function (evt, data) {
            if(vm.jobContextExistsFlag){
                if(vm.candidatesToAssociateList.length != 0){
                    vm.associateCandidates(function(){
                        if(vm.completeCallback){
                            vm.completeCallback();
                        }
                    });
                }else{
                    if(vm.completeCallback){
                        vm.completeCallback();
                    }
                }
            }else{
                if(vm.completeCallback){
                    vm.completeCallback();
                }
            }
        });

        return uploadResumesWizard;
    }

    function wizardCancel() {
        bootbox.confirm({
            closeButton: false,
            title: "<div class='alert alert-warning' style='margin-bottom: 0px;'><i class='fa fa-warning fa-fw fa-lg'></i><strong>Confirm</strong></div>",
            message: "Are you sure you want to cancel ?",
            className: "zIndex1060",
            backdrop: true,
            onEscape: false,
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-info'
                },
                cancel:{
                    label: 'Cancel',
                    className: 'btn-danger'
                }
            },
            callback: function(result){
                if(result){
                    vm.cancelCallback();
                }
            }
        });
    }

    /**
     * update the action buttons
     */
    function updateActionButtons() {
        var wizardCurrentStep = $('#uploadResumesWizard').wizard('selectedItem').step;
        switch(wizardCurrentStep){
            case 1:
                // if resumes exist and scoring is completed, then the functions on this tab are disabled.
                if(vm.resumeList.length > 0){
                    vm.disableSourceTypesFlag = true;
                    vm.resumeUploadDropzone.clickable = false;
                    vm.disableUploadButtonFlag = true;
                    vm.disableNextButtonFlag = false;
                }else{
                    // if - source type is chosen and files exist in drop zone
                    // Enable the upload button
                    if((vm.sourceTypeId !== null) && vm.fileExistsFlag){
                        vm.disableUploadButtonFlag = false;
                    }else{
                        vm.disableUploadButtonFlag = true;
                    }
                    // save button is always disabled in this step
                    vm.disableSaveButtonFlag = true;
                    // if resumeList has items, that means a successful upload was done. Hence the next button should be enabled.
                    if(vm.resumeList.length > 0){
                        // enable the next button
                        vm.disableNextButtonFlag = false;
                    }else{
                        // disable the next button
                        vm.disableNextButtonFlag = true;
                    }
                }
                break;
            case 2:
                break;
            default:
                break;
        }
        $timeout(function () {
            $scope.$apply();
        }, 200);
    }

    function candidateToAssociateToggle(candidate){
        // if it is selected, add to associate list
        if(candidate.selectedFlag){
            vm.candidatesToAssociateList.push(candidate);
        }else{
            // if selected is removed, remove from candidates to associate list
            _.remove(vm.candidatesToAssociateList, {
                candidateId: candidate.candidateId
            });
        }
    }

    function associateCandidates(successCallback){
        angular.forEach(vm.candidatesToAssociateList, function (val, key) {
            vm.createJobMatchesObject.candidateIds.push(val.candidateId);
        });
        vm.associatingCandidatesFlag = true;
        var associatingDialog = bootbox.dialog({
            message: '<p class="text-center">Adding Candidates <i class="fa fa-spinner fa-spin"></i></p>',
            closeButton: false
        });
        jobMatchService.createJobMatches(vm.createJobMatchesObject, function (data) {
            vm.associatingCandidatesFlag = false;
            associatingDialog.modal('hide');
            modalService.addModalOpenClassToBodyIfAnyModalIsOpen();
            angular.forEach(vm.candidatesToAssociateList, function(val, key){
                // find each associated candidate in the original candidates list
                var index = _.findIndex(vm.candidatesList, {
                    candidateId: val.candidateId
                });
                // for each candidate, set the added successfully flag to true and selected flag to false
                vm.candidatesList[index].addedSuccessfullyFlag = true;
                vm.candidatesList[index].status = 'Candidate Added.';
                vm.candidatesList[index].selectedFlag = false;
            });
            // make the candidates to associate list as empty.
            vm.candidatesToAssociateList = [];
            if(successCallback){
                successCallback();
            }
        }, function(error){
            vm.associatingCandidatesFlag = false;
            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
        });
    }

    function _getCandidateCounts(){
        // get counts of candidates that matched jobs.
        // call the get counts API
        vm.gettingCountsFlag = true;
        resumeService.getCandidateCount(vm.uploadResumesResponse.transactionID, function (data) {
            vm.candidateCount = angular.merge(new CandidateCount(), data);
            vm.updateActionButtons();
            vm.gettingCountsFlag = false;
        }, function (error) {
            // do nothing
        });
    }

    function getAllSourceTypes() {
        resumeService.getAllSourceTypes(function (data) {
            vm.sourceTypeList = data;
        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }

    function sourceTypeChanged() {
        if(angular.isDefined(vm.sourceType) && angular.isDefined(vm.sourceType.id)){
            vm.sourceTypeId = vm.sourceType.id;
            vm.updateActionButtons();
        }else{
            vm.sourceTypeId = null;
        }
    }

    function uploadResumes() {
        vm.resumeUploadDropzone.processQueue();
    }

    function toggleSelectAllCandidates(){
        if(vm.selectedAllCandidatesFlag){
            // if selectAllCandidates was already selected, remove all selections
            angular.forEach(vm.candidatesList, function(val, key){
                // if candidate was not already added successfully and was selected, de select it and remove it from associate list
                if(!val.addedSuccessfullyFlag && val.selectedFlag){
                    val.selectedFlag = false;
                    // if selected is removed, remove from candidates to associate list
                    _.remove(vm.candidatesToAssociateList, {
                        candidateId: val.candidateId
                    });
                }
            });
            vm.selectedAllCandidatesFlag = false;
        }else{
            // if selectAllCandidates was not already selected
            angular.forEach(vm.candidatesList, function(val, key){
                // if candidate was not already added successfully and also not already selected, select it and add to associate list
                if(!val.addedSuccessfullyFlag && !val.selectedFlag){
                    val.selectedFlag = true;
                    vm.candidatesToAssociateList.push(val);
                }
            });
            vm.selectedAllCandidatesFlag = true;
        }
    }
}

UploadResumesController.$inject = ['$scope','$rootScope','$timeout','Resume','modalService','alertsAndNotificationsService','resumeService','jobMatchService','CandidateCount','MESSAGECONSTANTS'];

fourdotfivedirectives.controller('UploadResumesController',UploadResumesController);
