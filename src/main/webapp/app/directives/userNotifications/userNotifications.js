fourdotfivedirectives.directive('userNotifications', function userNotifications() {
    return {
        restrict: 'E',
        scope: {},
        bindToController: {
            height: '@',
            width: '@',
            numberOfNewNotifications: '=',
            searchObject: '@',
            viewAllFlag: '@',
            context: '@',
            userNotificationControls: '=',
            showFilters: '@'
        },
        controller: 'UserNotificationsController',
        controllerAs: 'userNotifications',
        templateUrl: 'app/directives/userNotifications/user-notifications.html'
    }
});