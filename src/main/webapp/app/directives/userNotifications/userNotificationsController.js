/*
  * This is the controller for the user notifications directive
 */

function UserNotificationsController($rootScope, $uibModal, alertsAndNotificationsService, userNotificationsService, dateTimeUtilityService) {
    var vm = this;

    //flags
    vm.showUnreadFlag = false;
    vm.showShortMessagesFlag = true;
    //variables
    vm.searchObjectStartDate = null;
    vm.searchObjectEndDate = null;
    //objects
    vm.filterObject = {};
    vm.alertTypes = [
        {
            name: 'Technical Assessment',
            value: 'Technical Assessment'
        },
        {
            name: 'Value Assessment',
            value: 'Value Assessment'
        },
        {
            name: 'Phone Screen ',
            value: 'Phone Screen '
        },
        {
            name: 'Interview',
            value: 'Interview'
        }
    ];
    //methods
    vm.init = init;
    vm.setNewNotificationsAsViewed = setNewNotificationsAsViewed;
    vm.notificationClicked = notificationClicked;
    vm.markNotificationAsRead = markNotificationAsRead;
    vm.performAction = performAction;
    vm.searchNotifications = searchNotifications;

    vm.init();

    function init() {
        if(_.isUndefined(vm.height)){
            vm.height = '400px';
        }
        if(_.isUndefined(vm.width)){
            vm.width = '100%';
        }
        if(_.isUndefined(vm.viewAllFlag)){
            vm.viewAllFlag = false;
        }
        if(_.isUndefined(vm.context)){
            vm.context = 'global';
        }
        if(_.isUndefined(vm.userNotificationControls)){
            vm.userNotificationControls = {};
        }
        if(_.isUndefined(vm.numberOfNewNotifications)){
            vm.numberOfNewNotifications = 0;
        }
        if(_.isUndefined(vm.showFilters)){
            vm.showFilters = false;
        }
        if(vm.showFilters){
            _initializeDateAndTimePickersForFilters();
            // do not show short messages because this is the full notifications view.
            vm.showShortMessagesFlag = false;
        }
        vm.userNotificationControls.setNewNotificationsAsViewed = setNewNotificationsAsViewed;
        vm.notificationsContainer = new notificationsContainer();
        vm.filterObject.text = '';
        _initializeNotificationReceive();
        $(document).ready(function () {
            $("#user-notifications-search-panel").hide();
            $(document).off("click", "#user-notifications-open-filter").on("click", "#user-notifications-open-filter",
                function () {
                    $("#user-notifications-search-panel").animate({
                        height: 'toggle'
                    });
                }
            );
        });
    }

    function searchNotifications() {
        vm.notificationsContainer.searchObject.page = "";
        vm.notificationsContainer.busy = false;
        vm.notificationsContainer.currentPromise = null;
        vm.notificationsContainer.disableInfiniteScroll = false;
        vm.notificationsContainer.notificationsExistFlag = true;
        vm.notificationsContainer.notifications = [];
        vm.notificationsContainer.newNotifications = [];
        vm.notificationsContainer.getNext();
    }

    function searchObject(companyId) {
        this.companyId = companyId;
        this.page = "";
        this.size = 3;
        if(vm.showFilters){
            this.alertType = '';
            this.startDateTime = null;
            this.endDateTime = null;
        }
    }

    function notificationsContainer(){
        this.notifications = [];
        this.newNotifications = [];
        this.busy = false;
        this.notificationsExistFlag = true;
        this.searchObject = _.isUndefined(vm.searchObject) ? new searchObject($rootScope.userDetails.company.companyId) : JSON.parse(vm.searchObject);
        this.currentPromise = null;
        this.disableInfiniteScroll = false;

        this.getNext = function () {
            var parent = this;
            if(this.busy) return;
            this.busy = true;

            if(this.searchObject.page == ''){
                this.searchObject.page = 1;
            }
            var tempSearchObject = angular.copy(this.searchObject);
            this.currentPromise = alertsAndNotificationsService.getNotifications(tempSearchObject, function (data) {
                if(angular.isDefined(data) && data != 'user cancellation'){
                    if(data.length > 0){
                        angular.forEach(data, function (val, key) {
                            val = _massageMessageData(val);
                            // push new notifications into the new notifications array
                            if(val.state == 'NEW'){
                                parent.newNotifications.push(val);
                                vm.numberOfNewNotifications = parent.newNotifications.length;
                            }
                            parent.notifications.push(val);
                        });
                        // if the length of the returned values is less than page size, it means there are not enough values.
                        // hence we can disable the infinite scroll
                        if(data.length < parent.searchObject.size){
                            parent.disableInfiniteScroll = true;
                        }else{
                            parent.searchObject.page++;
                        }
                    }else{
                        // if the length of the returned values is 0, it means there are no more values.
                        // hence we can disable the infinite scroll
                        parent.disableInfiniteScroll = true;
                    }
                    // set the notifications exist flag based on existence of notifications.
                    if(parent.notifications.length > 0){
                        parent.notificationsExistFlag = true;
                    }else{
                        parent.notificationsExistFlag = false;
                    }
                }
                //set parent.busy to false
                parent.busy = false;
            }, function (error) {
                if ($rootScope.isOnline) {
                    alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                }
                parent.busy = false;
            });
        }
    }

    function _massageMessageData(message){
        if(_.isNull(message.state)){
            message.state = 'NEW';
        }
        message.createdDate = new Date(message.createdDate);
        message.selectedFlag = false;
        if(!_.isNull(message.taggedText)){
            message.highlightedHTMLText = _getHighlightedNotificationHTML(message.taggedText);
        }else {
            message.highlightedHTMLText = message.text;
        }
        message.showShortMessageFlag = vm.showShortMessagesFlag;
        return message;
    }

    function _getHighlightedNotificationHTML(text){
        var newText = text.replace(/<4dot5>/g, "<span class='emerald'>");
        return newText.replace(/<\/4dot5>/g, "</span>");
    }

    function _initializeNotificationReceive(){
        userNotificationsService.receive().then(null, null, function(message) {
            var performChangeFlag = false;
            if(vm.context == 'global'){
                performChangeFlag = true;
            }else if(vm.context == 'candidateCard'){
                if(!_.isNull(message.candidateId) && (message.notifyOn.indexOf('CANDIDATE_CARD') != -1) && (message.candidateId == vm.notificationsContainer.searchObject.candidateId)){
                    performChangeFlag = true;
                }
            }else if(vm.context == 'candidateJobMatchCard'){
                if(!_.isNull(message.jobMatchId) && (message.notifyOn.indexOf('CANDIDATE_JOB_CARD') != -1) && (message.jobMatchId == vm.notificationsContainer.searchObject.jobMatchId)){
                    performChangeFlag = true;
                }
            }else if(vm.context == 'requisitionCard'){
                if(!_.isNull(message.jobId) && (message.notifyOn.indexOf('REQUISITION_CARD') != -1) && (message.jobId == vm.notificationsContainer.searchObject.jobId)){
                    performChangeFlag = true;
                }
            }
            if(performChangeFlag){
                message = _massageMessageData(message);
                if(message.operation == 'new'){
                    vm.notificationsContainer.newNotifications.unshift(message);
                    vm.numberOfNewNotifications = vm.notificationsContainer.newNotifications.length;
                    vm.notificationsContainer.notifications.unshift(message);
                }else if(message.operation == 'update'){
                    var index = _.findIndex(vm.notificationsContainer.notifications, {
                        notificationId: message.notificationId
                    });
                    if(index != -1){
                        vm.notificationsContainer.notifications[index].state = message.state;
                        // remove if it exists in new notifications.
                        var removedArray = _.remove(vm.notificationsContainer.newNotifications, {
                            notificationId: message.notificationId
                        });
                        if(removedArray.length > 0){
                            vm.numberOfNewNotifications = vm.notificationsContainer.newNotifications.length;
                        }
                    }
                }
            }
        });
    }

    function setNewNotificationsAsViewed(){
        if(vm.notificationsContainer.newNotifications.length > 0){
            var stateObject = {
                notificationIds: [],
                state: 'VIEWED'
            };
            angular.forEach(vm.notificationsContainer.newNotifications, function(val,key){
                stateObject.notificationIds.push(val.notificationId);
            });
            alertsAndNotificationsService.setNotificationsState(stateObject, function(data){
                // do nothing. notification state change will be received as a push update and be handled in the _initializeNotificationReceive above.
            }, function(error){
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            });
        }
    }

    function notificationClicked($event, notification){
        // trigger the regular marking notification as read.
        vm.markNotificationAsRead($event, notification);
        // perform relevant actions as notification is clicked
        if(!_.isUndefined(notification.jbiTransactionId) && !_.isNull(notification.jbiTransactionId)){
            // if jbiTransactionId exists and is not null, this notification was caused because a job board pull was finished.
            var actionType = 'reviewJobPull';
            var actionLabel = 'Review Job Board Pull';
            $uibModal.open({
                animation: false,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/directives/userNotifications/user-notifications-actions-modal.html',
                backdrop: 'static',
                keyboard: false,
                controller: ['$uibModalInstance','actionType','actionLabel','jobId','jbiTransactionId',function ($uibModalInstance, actionType, actionLabel, jobId, jbiTransactionId) {
                    var vm = this;
                    vm.jobId = jobId;
                    vm.jbiTransactionId = jbiTransactionId;
                    vm.actionType = actionType;
                    vm.actionLabel = actionLabel;
                    vm.saveCallback = function(){
                        $uibModalInstance.close('cancel');
                        saveCallback();
                    };
                    vm.closeModal = function () {
                        $uibModalInstance.close('cancel');
                    };
                }],
                controllerAs: 'userNotificationsActionModal',
                size: 'lg',
                resolve:{
                    jobId: function () {
                        return notification.jobId;
                    },
                    jbiTransactionId: function () {
                        return notification.jbiTransactionId;
                    },
                    actionType: function(){
                        return actionType;
                    },
                    actionLabel: function(){
                        return actionLabel;
                    }
                }
            });
        }
    }

    function markNotificationAsRead($event, notification) {
        // show the full notification
        notification.showShortMessageFlag = false;
        $event.stopPropagation();
        if(notification.state == 'NEW' || notification.state == 'VIEWED'){
            var stateObject = {
                notificationIds: [],
                state: 'READ'
            };
            stateObject.notificationIds.push(notification.notificationId);
            alertsAndNotificationsService.setNotificationsState(stateObject, function(data){
                // do nothing. notification state change will be received as a push update and be handled in the _initializeNotificationReceive above.
            }, function(error){
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            });
        }
    }

    function performAction(actionType){
        var selectedUserNotifications = [];
        angular.forEach(vm.notificationsContainer.notifications, function (val, key) {
            if(val.selectedFlag){
                selectedUserNotifications.push(val);
            }
        });
        if(selectedUserNotifications.length == 0) {
            bootbox.alert({
                closeButton: false,
                title: "<div class='alert alert-warning' style='margin-bottom: 0px;'><i class='fa fa-warning fa-fw fa-lg'></i><strong>No Notifications Selected!</strong></div>",
                message: "Please select at least one notification to perform this action.",
                className: "zIndex1060",
                buttons: {
                    ok: {
                        label: 'Ok',
                        className: 'btn-info'
                    }
                }
            });
        }else{
            switch(actionType){
                case 'markAsRead':
                    var stateObject = {
                        notificationIds: [],
                        state: 'READ'
                    };
                    angular.forEach(selectedUserNotifications, function(val, key){
                        stateObject.notificationIds.push(val.notificationId);
                    });
                    alertsAndNotificationsService.setNotificationsState(stateObject, function(data){
                        // do nothing. notification state change will be received as a push update and be handled in the _initializeNotificationReceive above.
                        angular.forEach(selectedUserNotifications, function (val, key) {
                           val.selectedFlag = false;
                        });
                    }, function(error){
                        alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                    });
                    break;
                default:
                    // do nothing
            }
        }

    }

    function _initializeDateAndTimePickersForFilters(){
        $(document).ready(function () {
            var currentDate = new Date();

            $('#userNotificationsStartDate').datepicker({
                format : "M d, yyyy",
                endDate: currentDate,
                weekStart: 1,
                autoclose: true
            });
            // set current date as default value
            //$("#timeBoundStartDate").datepicker("setDate", currentDate);

            $('#userNotificationsEndDate').datepicker({
                format : "M d, yyyy",
                endDate: currentDate,
                weekStart: 1,
                autoclose: true
            });

            $('#userNotificationsStartDate').datepicker().on('changeDate', function(e) {
                _validateAndCorrectStartDateAndEndDate();
                vm.notificationsContainer.searchObject.startDateTime = dateTimeUtilityService.getDateTimeInAPIFormat(new Date(vm.searchObjectStartDate));
                vm.searchNotifications();
            });

            $('#userNotificationsEndDate').datepicker().on('changeDate', function(e) {
                _validateAndCorrectStartDateAndEndDate();
                vm.notificationsContainer.searchObject.endDateTime = dateTimeUtilityService.getDateTimeInAPIFormat(new Date(vm.searchObjectEndDate + ' 11:59:59 PM'));
                vm.searchNotifications();
            });

        });
    }

    function _validateAndCorrectStartDateAndEndDate(){
        if(!_.isNull(vm.searchObjectStartDate) && vm.searchObjectStartDate != '' && !_.isNull(vm.searchObjectEndDate) && vm.searchObjectEndDate != ''){
            var momentOfCurrentStartDate = moment(new Date(vm.searchObjectStartDate));
            var momentOfCurrentEndDate = moment(new Date(vm.searchObjectEndDate));
            if(momentOfCurrentEndDate.isBefore(momentOfCurrentStartDate, 'day')){
                // make the 'end date' as 1 days greater than 'start date'
                var defaultEndDate = momentOfCurrentStartDate.add(1,'d').toDate();
                $("#userNotificationsEndDate").datepicker("setDate", defaultEndDate);
            }
        }
    }

}

UserNotificationsController.$inject = ['$rootScope','$uibModal','alertsAndNotificationsService','userNotificationsService','dateTimeUtilityService'];

fourdotfivedirectives.controller('UserNotificationsController', UserNotificationsController);