fourdotfivefilters.filter('fourdotfiveDateFormat', ['moment', function (moment) {
    return function (input) {
        return moment(input).format("MMM DD, YYYY");
    }
}]);