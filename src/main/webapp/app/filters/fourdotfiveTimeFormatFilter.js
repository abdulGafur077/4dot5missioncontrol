fourdotfivefilters.filter('fourdotfiveTimeFormat', ['moment', function (moment) {
    return function (input) {
        return moment(input).format("hh:mm a");
    }
}]);