fourdotfivefilters.filter('fourdotfiveTimeStampFormat', ['moment', function (moment) {
    return function (input) {
        return moment(input).format("MMM DD, YYYY hh:mm A");
    }
}]);