/**
 * factory methods for most commonly used http methods like get,
 * post, put and delete.
 * Note: All the function here follows the REST standard.
 *
 * @author Ashok Kumar KV
 *
 * @CopyRights Reserved with Spaneos Software Solutions Pvt. Ltd.
 *
 * All of the code within the Spaneos Software Solutions is developed and copyrighted
 * by Spaneos Software Solutions Pvt. Ltd., and may not be copied,replicated, or used in any
 * other software or application without prior permission from Spaneos Software Solutions Pvt. Ltd.
 * All usage must coincide with the Spaneos Software Solutions pvt ltd End User License Agreement.
 */
interceptorservice = angular.module('InterceptorServiceModule', []);
/**
 * All module level constants goes here
 */
interceptorservice.constant('INTERCEPTORSERVICECONSTANTS', [

]);

var urls = ['auth'];

/**
 * This is factory for this module
 */
interceptorservice.factory('interceptorService',
    ['$q',
        '$rootScope',
        'StorageService',
        '$injector',
        'INTERCEPTORSERVICECONSTANTS',
        function ($q, $rootScope, StorageService, $injector, INTERCEPTORSERVICECONSTANTS) {
            return {
                // On request success
                request: function (config) {
                    var notInUrlArray = function () {
                        for (var i = 0; i < urls.length; i++) {
                            if (config.url.includes(urls[i])) {
                                return false;
                            }
                        }
                        return true;
                    };
                    var baseurl =  StorageService.get('baseurl');
                    if(baseurl === null){
                    	StorageService.set('baseurl', '');
                    	baseurl =  StorageService.get('baseurl');
                    }
                    config.url =  baseurl + angular.copy(config.url); 
                    if(config.url.includes('rememberme')) {
                        var refreshtoken = StorageService.getCookie('refreshtoken');
                        if (angular.isDefined(refreshtoken)){
                            config.headers.Authorization = 'Bearer ' + refreshtoken;
                        }
                    }else {
                        var userDetails = StorageService.getCookie('userDetails');
                        if (notInUrlArray() && angular.isDefined(userDetails) && (userDetails.accesstoken !== null)){
                            config.headers.Authorization = 'Bearer ' + userDetails.accesstoken;
                        }
                    }
                    return config;
                },
                response: function (res) {
                	$rootScope.isOffline = angular.isDefined(res.data) && angular.isDefined(res.data.statusCode) && angular.equals(res.data.statusCode,'500') && (res.data.developerMsg !== null) &&  res.data.developerMsg.includes("I/O error");
                	if(angular.copy($rootScope.isOffline)){
                		$rootScope.throwError(res.data);
                	}
                	$rootScope.isOnline = !angular.copy($rootScope.isOffline);
                    return res;
                },
                responseError: function (res) {
                	console.log('responseError : ',res);
                	if (angular.equals(res.status, 401) || angular.equals(res.status, 406)) {
                		/**
                		 * This method serves for invalidating session currently exists.
                		 * This method will be caught in app.js
                		 */
                        $rootScope.$broadcast('sessionInvalidated');
                    }
                    return $q.reject(res);
                }
            }
        }
    ]);