/**
 * This Module is for defining a Controller for
 * User Header(ie., User After Login) 
 */

var indexmodule = angular.module('4dot5.indexmodule', []);

indexmodule.controller('UserHeaderCtrl',
	['$scope',
		'$state',
		'$rootScope',
		'$uibModalStack',
		'userNotificationsService',
		'alertsAndNotificationsService',
		'utilityService',
		'StorageService',
		'genericService',
		'MESSAGECONSTANTS',
		function ($scope, $state, $rootScope, $uibModalStack, userNotificationsService, alertsAndNotificationsService, utilityService, StorageService, genericService, MESSAGECONSTANTS) {
			//console.log('UserHeaderCtrl');
			$scope.userDetails = angular.copy(StorageService.getCookie('userDetails'));
            $scope.searchObject = {};
            $scope.searchObject.page = 1;
            $scope.searchObject.size = 20;
            $scope.searchObject.companyId = $rootScope.userDetails.company.companyId;
            $scope.userNotificationControls = {};
            $scope.userNotificationControls.setNewNotificationsAsViewed = null;
			/**
             * Function to destroy modal
             */
            function destroyModal() {
            	switchCompanyRepeat = 0;
            	$rootScope.switchClicked = false;
                var loadingModal = $('#myModal');
                loadingModal.modal("hide");
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
            }
			
			/**
			 * user when clicked on logout from any of
			 * the page, This function will be invoked
			 */
			$rootScope.$watch('userDetails',function(){
				$scope.userDetails = angular.copy($rootScope.userDetails);
				//console.log('$scope.userDetails : '+ angular.toJson($scope.userDetails));
			}, true);

			var isStateChageValid = function(){
				var currentstate = angular.copy($state.current.name);
				var currentStateSatisfies = (currentstate === 'missioncontrol.usermanagement') ||
											(currentstate === 'missioncontrol.companymanagement') ||
											(currentstate === 'missioncontrol.clientorbu') || 
											(currentstate === 'missioncontrol.uploadreq') || 
											(currentstate === 'missioncontrol.roleentityscope')||
											(currentstate === 'missioncontrol.settings') ||
											(currentstate === 'missioncontrol.notificationpreference');

				return currentStateSatisfies;
			};

			$scope.switchCompany = function(){
				 if(isStateChageValid()){
					 console.log('switch company clicked with isStateChageValid');
					 destroyModal();
					 $rootScope.switchClicked = true;
					 $state.go('missioncontrol.dashboard',null,{reload : true});
				 }else {
				 	$rootScope.$emit('switchnow');
				 }
			};

			$rootScope.$on("CallLogoutMethod", function (event, data) {
				console.log(data);
				$scope.logout(data);
			});
			
			var createUserObj = function(userObj){
				var obj = {};
				obj.userId = userObj.id;
				return obj;
			};

			$scope.showUserProfile = function(user){
				console.log('in showUserProfile before api : ' + angular.toJson(user));
				utilityService.getUserProfile(createUserObj(user)).then(function (data) {
					console.log('in showUserProfile before moving to usermanagement: ' + angular.toJson(data));
					$state.go('missioncontrol.usermanagement', { userObject: data, accessMode: MESSAGECONSTANTS.ACCESS_MODES.EDIT, manager: null, purpose: 'view userprofile' }, { reload: false });
				}, function (error) {
					console.log('error : ' + angular.toJson(error));
				});
			};

			$scope.logout = function (data) {
				console.log('logout clicked : '+angular.toJson(data));
				var temp = false;
				if(isStateChageValid()){
					if(angular.isDefined(data) && data.logoutnow){
						temp = true;
					}else{
						temp = false;
					}
				}else {
					temp = true;
				}

				if(temp){
					var rememberMeStatus = angular.copy(StorageService.getCookie('rememberme'));
					var userDetails = angular.copy(StorageService.getCookie('userDetails'));
					/**
					* Api Call for informing Server that
					* a person want to Logout will happen here
					*/
					$rootScope.companyDeletedShowPopup = false;
					StorageService.resetBrowserStorage();
					
					if (angular.isDefined(rememberMeStatus) && (rememberMeStatus !== null) && (rememberMeStatus === true)) {
						if (angular.isDefined(userDetails) && (userDetails !== null)) {
							StorageService.setCookie('logoutemail', userDetails.emailId);
						}
					}
				}
				$uibModalStack.dismissAll();
				$state.go('login', null, { reload: true });
			};

            // $scope.getNotifications = function(){
            //     var searchObject = {};
            //     searchObject.page = 1;
            //     searchObject.size = 20;
            //     searchObject.companyId = $rootScope.userDetails.company.companyId;
            //     alertsAndNotificationsService.getNotifications(searchObject, function (data) {
            //         angular.forEach(data, function(val, key){
            //             if(_.isNull(val.state)){
            //                 val.state = 'NEW';
            //             }
            //             // push new notifications into the new notifications array
            //             if(val.state == 'NEW'){
            //                 $scope.userNewNotifications.push(val);
            //             }
            //             $scope.userNotifications.push(val);
            //         });
            //         $scope.initializeNotificationReceive();
            //     }, function(error){
            //         alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
				// });
            // };
            //
            // $scope.initializeNotificationReceive = function(){
            //     userNotificationsService.receive().then(null, null, function(message) {
            //         //console.log('message received in candidate card - ', message.text);
				// 	$scope.userNewNotifications.unshift(message);
            //         $scope.userNotifications.unshift(message);
            //     });
            // };
            //
            // $scope.setNewNotificationsAsViewed = function(){
            //     if($scope.userNewNotifications.length > 0){
            //         var stateObject = {
            //             notificationIds: [],
            //             state: 'VIEWED'
            //         };
            //         angular.forEach($scope.userNewNotifications, function(val,key){
            //             stateObject.notificationIds.push(val.notificationId);
            //         });
            //         alertsAndNotificationsService.setNotificationsState(stateObject, function(data){
            //             angular.forEach($scope.userNewNotifications, function(val,key){
            //                 var index = _.findIndex($scope.userNotifications, {
            //                     notificationId: val.notificationId
            //                 });
            //                 $scope.userNotifications[index].state = 'VIEWED';
            //             });
            //             $scope.userNewNotifications = [];
            //         }, function(error){
            //             alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            //         });
            //     }
            // };
            //
            // $scope.markNotificationAsRead = function($event, notification) {
            //     $event.stopPropagation();
            //     if (notification.state == 'NEW' || notification.state == 'VIEWED') {
            //         var stateObject = {
            //             notificationIds: [],
            //             state: 'READ'
            //         };
            //         stateObject.notificationIds.push(notification.notificationId);
            //         alertsAndNotificationsService.setNotificationsState(stateObject, function (data) {
            //             var index = _.findIndex($scope.userNotifications, {
            //                 notificationId: notification.notificationId
            //             });
            //             $scope.userNotifications[index].state = 'READ';
            //             // broadcast the event that the notification was read.
            //             $rootScope.$broadcast("notificationStateChange", stateObject);
            //         }, function (error) {
            //             alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            //         });
            //     }
            // }

            // get the initial userNotifications.
            //$scope.getNotifications();
		}
	]);

