var modelsModule = angular.module('4dot5.missioncontrol.modelsModule');

/**
 *  This is the factory for the 'candidate' model
 */
modelsModule.factory('AddCandidate', function () {
        function AddCandidate() {
            this.firstName;
            this.lastName;
            this.emailId;
            this.phoneNumber;
            this.companyId;
            this.jobId;
        }

        return AddCandidate;
    }
);

