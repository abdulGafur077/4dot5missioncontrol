var modelsModule = angular.module('4dot5.missioncontrol.modelsModule');

/**
 *  This is the factory for the 'candidate' model
 */
modelsModule.factory('Candidate', function () {
        function Candidate() {
            this.id = null;
            this.name;
            this.firstName;
            this.lastName;
            this.workphone;
            this.mobilephone;
            this.email;
            this.im;
        }

        return Candidate;
    }
);

