var modelsModule = angular.module('4dot5.missioncontrol.modelsModule');

/**
 *  This is the factory for the 'CandidateActivityFilter' model
 */
modelsModule.factory('CandidateActivityFilter', function () {
        function CandidateActivityFilter() {
            this.candidateId;
            this.jobMatchId;
            this.companyId;
            this.clientOrBuId;
            this.title;
            this.activityType;
            this.startDate;
            this.endDate;
            this.currentStep;
            this.createdBy;
            this.page;
            this.size;
            this.column;
            this.sortDir;
        }

        return CandidateActivityFilter;
    }
);