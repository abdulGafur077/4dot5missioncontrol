var modelsModule = angular.module('4dot5.missioncontrol.modelsModule');

/**
 *  This is the factory for the 'candidate card' model
 */
modelsModule.factory('CandidateCard', function () {
        function CandidateCard() {
            this.id = null;
            this.name;
            this.firstName;
            this.lastName;
            this.candidateStatus;
            this.workphone;
            this.mobilephone;
            this.email;
            this.im;
            this.noOfDaysSinceLastContact;
            this.bestMatchJobId;
            this.bestMatchJobTitle;
            this.bestMatchJobDate;
            this.bestMatchRequisitionNum;
            this.bestMatchJobScore;
            this.bestMatchClientName;
            this.bestMatchRecruiterName;
            this.bestMatchJobCurrentState;
            this.currentJobCompanyName;
            this.currentJobDuration;
            this.currentJobLocation;
            this.currentJobTitle;
            this.noOfDaysSinceLastUpload;
            this.jobMatchesCount;
            this.recruiterList = [];
        }

        return CandidateCard;
    }
);

