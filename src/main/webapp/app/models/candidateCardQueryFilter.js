var modelsModule = angular.module('4dot5.missioncontrol.modelsModule');

/**
 *  This is the factory for the 'candidate card query filter' model
 */
modelsModule.factory('CandidateCardQueryFilter', function () {

        function CandidateCardQueryFilter(companyId, userId) {
            this.parentCompanyId = companyId;
            this.loggedInUserId = userId;
            this.status = [];
            this.roleList = [];
            this.recruiterList = [];
            this.requisitionNumbers = [];
            this.clientOrBuList = [];
            this.requisitionStatus = [];
        }

        return CandidateCardQueryFilter;
    }
);

