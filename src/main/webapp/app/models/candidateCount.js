var modelsModule = angular.module('4dot5.missioncontrol.modelsModule');

/**
 *  This is the factory for the 'candidate count' model
 */
modelsModule.factory('CandidateCount', function () {
        function CandidateCount() {
            this.activeCandidatesCount = 0;
            this.passiveCandidatesCount = 0;
            this.matchedCandidateCount = 0;
            this.unMatchedCandidateCount = 0;
            this.justAddedCandidateCount = 0;
            this.notProcessedCandidateCount = 0;
            this.candidateList = null
        }

        return CandidateCount;
    }
);

