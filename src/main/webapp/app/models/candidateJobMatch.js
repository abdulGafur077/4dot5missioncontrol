var modelsModule = angular.module('4dot5.missioncontrol.modelsModule');

/**
 *  This is the factory for the 'candidateJobMatch' model
 */
modelsModule.factory('CandidateJobMatch', function () {
        function CandidateJobMatch() {
            this.jobMatchDate;
            this.title;
            this.company;
            this.jobMatchStatus;
            this.requisitionNumber;
            this.recruiters = [];
            this.intelligentScore;
            this.techAssessmentScore;
        }

        return CandidateJobMatch;
    }
);