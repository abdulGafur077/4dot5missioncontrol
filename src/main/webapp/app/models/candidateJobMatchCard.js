var modelsModule = angular.module('4dot5.missioncontrol.modelsModule');

/**
 *  This is the factory for the 'candidate job match card' model
 */
modelsModule.factory('CandidateJobMatchCard', function () {
        function CandidateJobMatchCard() {
            this.id = null;
            this.firstName;
            this.lastName;
            this.name;
            this.workphone;
            this.mobilephone;
            this.email;
            this.im;
            this.jobMatchTitle;
            this.jobMatchRequisitionNum;
            this.jobMatchRequisitionStatus;
            this.jobMatchScore;
            this.jobMatchStatus;
            this.recruitersList = [];
            this.primaryRecruiter;
            this.jobMatchClientName;
            this.jobMatchClientId;
            this.jobMatchCurrentState;
            this.noOfDaysSinceLastUpload;
            this.jobMatchDate;
            this.candidateStatus;
            this.numberOfFilledOpenings;
            this.totalOpenings;
            this.numberOfOpenOpenings = 0;
            this.candidateStatus = "Active";
            this.actions;
        }

        return CandidateJobMatchCard;
    }
);

