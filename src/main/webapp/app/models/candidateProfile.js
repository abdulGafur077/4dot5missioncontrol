var modelsModule = angular.module('4dot5.missioncontrol.modelsModule');

/**
 *  This is the factory for the 'candidateProfile' model
 */
modelsModule.factory('CandidateProfile', function () {
        function CandidateProfile() {
            this.id = null;
            this.name;
            this.firstName;
            this.lastName;
            this.title;
            this.companyName;
            this.educations = [];
            this.experiences = [];
            this.certifications = [];
            this.compensations = [];
            this.technicalSkills = [];
            this.softSkills = [];
            this.others = [];
        }

        return CandidateProfile;
    }
);

