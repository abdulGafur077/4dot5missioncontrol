var modelsModule = angular.module('4dot5.missioncontrol.modelsModule');

/**
 *  This is the factory for the 'candidate query filter' model
 */
modelsModule.factory('CandidatesQueryFilter', function () {

        function CandidatesQueryFilter(companyId, userId) {
            this.parentCompanyId = companyId;
            this.loggedInUserId = userId;
            this.status = ["Active", "Passive","Inert"];
            this.roleList = [];
            this.recruiterList = [];
            this.requisitionNumbers = [];
            this.clientOrBuList = [];
            this.firstName = "";
            this.lastName = "";
            this.requisitionStatus = [];
            this.visibility = [];
            this.searchString = "";
            this.pageSize = 12;
            this.pageNum = "";
            // the below properties cannot be empty and should be added as per requirement
            //this.column;
            //this.sortDirection;
        }

        return CandidatesQueryFilter;
    }
);

