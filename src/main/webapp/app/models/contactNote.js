var modelsModule = angular.module('4dot5.missioncontrol.modelsModule');

/**
 *  This is the factory for the 'contactNotes' model
 */
modelsModule.factory('ContactNote', function () {
        function ContactNote() {
            this.noteId = null;
            this.jobMatchId = null;
            this.contactMethod = null;
            this.duration = null;
            this.dateTime = null;
            this.note = '';
            this.user = {};
        }

        return ContactNote;
    }
);