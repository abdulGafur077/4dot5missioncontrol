var modelsModule = angular.module('4dot5.missioncontrol.modelsModule');

/**
 * This is factory for this module
 */
modelsModule.factory('Job', function () {
        function Job (){
            this.id = null;
            this.title = '';
            this.company = null;
            this.clientorbu = null;
            this.requistionNumber = null;
            this.openings;
            this.jobType = null;
            this.transactionId = null;
            this.sponsorships = [];
            this.recruiters = [];
            // preferences
            this.score = null;
            this.techAssessmentId = null;
            this.techAssessmentScore = null;
            this.techAssessment = false;
            this.valueAssessmentId = null;
            this.valueAssessment = false;
            this.eduVerification = null;
            this.expVeification = null;
            this.phoneScreen = null;
            this.interview = null;
            this.licensePreferences = {
                fourDotFiveIntelligence: true,
                techAssessment: false,
                valueAssessment: false,
                educationVerification: false,
                experienceVerification: false,
                phoneScreen: false,
                interview: false,
                imageProctoring: false,
                jobBoardCredentialList: []
            };
        }

        return Job;
        
    }
);

