var modelsModule = angular.module('4dot5.missioncontrol.modelsModule');

/**
 *  This is the factory for the 'job matches query filter' model
 */
modelsModule.factory('JobMatchesQueryFilter', function () {

        function JobMatchesQueryFilter(workflowStep) {
            this.clientOrBU =  [];
            this.recruiters = [];
            this.roleList = [];
            this.requisitionStates = [];
            this.requisitionNums = [];
            this.firstName = "";
            this.lastName = "";
            this.phone = "";
            this.email = "";
            this.workflowStates = [];
            this.workflowSteps =[];
            if(!_.isUndefined(workflowStep) && !_.isNull(workflowStep)){
                this.workflowSteps.push(workflowStep);
            }
            this.searchString = "";
            this.pageNum = "";
            this.column;
            this.sortDirection;
            this.filtered = false;
            this.userIdScope = [];
            this.forwardedUserIds = [];
            this.isUnAssignedCandidate = false;
            //visibility filters
            this.isUnassigned = false;
            this.isAssignedToMe = false;
            this.isAssignedToAll = false;
        }

        return JobMatchesQueryFilter;
    }
);

