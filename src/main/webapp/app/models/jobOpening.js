var modelsModule = angular.module('4dot5.missioncontrol.modelsModule');

/**
 * This is factory for this module
 */
modelsModule.factory('jobOpening',
    function () {
        return {
            id: null,
            openingNumber: null,
            requisitionOpeningNumber: null,
            assignedRecruiters: null,
            note: ''
        }

    }
);

