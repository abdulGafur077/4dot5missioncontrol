var modelsModule = angular.module('4dot5.missioncontrol.modelsModule');

/**
 *  This is the factory for the 'jobProfile' model
 */
modelsModule.factory('JobProfile', function () {
        function JobProfile() {
            this.id = null;
            this.title;
            this.minYears;
            this.maxYears;
            this.companyName;
            this.educations = [];
            this.experiences = [];
            this.certifications = [];
            this.compensations = [];
            this.technicalSkills = [];
            this.softSkills = [];
            this.extraSkills = [];
            this.others = [];
        }

        return JobProfile;
    }
);

