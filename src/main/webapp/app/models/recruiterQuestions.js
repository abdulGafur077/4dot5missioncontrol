var modelsModule = angular.module('4dot5.missioncontrol.modelsModule');

/**
 *  This is the factory for the 'RecruiterQuestions' model
 */
modelsModule.factory('RecruiterQuestions', function () {
        function RecruiterQuestions() {
            this.companyName;
            this.clientJobMatchDetails = [];
            this.clientListJobMatchDetails =[];
            this.clientDetails = [];
            this.jobDetails = [];
            this.jobQuestionsResults = [];
            this.companyQuestionsResults = [];
            this.sortedClientList = [];
            this.clientQuestionsResults = [];
            this.candidate = [];
        }

        return RecruiterQuestions;
    }
);

