var modelsModule = angular.module('4dot5.missioncontrol.modelsModule');

/**
 * This is factory for this module
 */
modelsModule.factory('Requisition', function () {
        function Requisition() {
            this.cloudFileLocation = '';
            this.hrxmlCloudFileLocation= '';
            this.length= '';
            this.fileSize= '';
            this.fileName= '';
            this.hrxmlFileName= '';
            this.user= null;
            this.company= null;
            this.clientOrBU= null;
        }

        return Requisition;
        
    }
);

