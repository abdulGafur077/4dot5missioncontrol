var modelsModule = angular.module('4dot5.missioncontrol.modelsModule');

/**
 *  This is the factory for Resume model
 */
modelsModule.factory('Resume', function () {
        function Resume(){
            this.resumeId = null;
            this.sourceTypeId = null;
        }

        return Resume;
    }
);

