var modelsModule = angular.module('4dot5.missioncontrol.modelsModule');

/**
 *  This is the factory for the 'ScreeningExecutionResult' model
 */
modelsModule.factory('ScreeningExecutionResult', function () {

        function ScreeningExecutionResult() {
            this.feedbackAnswersList = [];
            this.jobDetailsRecruiterScreeningList = [];
            this.companyAndOverallComplete;
            this.fromButton;

        }

        return ScreeningExecutionResult;
    }
);



