var modelsModule = angular.module('4dot5.missioncontrol.modelsModule');

/**
 *  This is the factory for UiError model
 */
modelsModule.factory('UiError', function () {
        function UiError(){
            this.hasErrorFlag = false;
            this.errorMessage = '';
            this.severity = 'normal';
        }

        return UiError;
    }
);