/**
 * All the major modules are declared here and used in other places in the application.
 *
 */
var fourdotfivedirectives = angular.module('4dot5.directivesModule', []);
var fourdotfivefilters = angular.module('4dot5.filtersModule',[]);
var modelsModule = angular.module('4dot5.missioncontrol.modelsModule', []);
var servicesmodule = angular.module('4dot5.missioncontrol.servicesmodule', []);
