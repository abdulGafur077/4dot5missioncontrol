/**
 * All the Functionalities Related to Forget password
 * Module is Included Here
 */

var dummymodule = angular.module('4dot5.dummymodule', []);

dummymodule.constant('DUMMYCONSTANTS', {
    CONFIG: {
        STATE: 'dummymodule',
        URL: '/dummymodule',
        CONTROLLER: 'DummyController',
        TEMPLATEURL: 'app/partials/dummymodule/dummymodule.html',
    }
});

dummymodule.filter('unique', function() {
   return function(collection, keyname) {
      var output = [], 
          keys = [];
      angular.forEach(collection, function(item) {
          var key = item[keyname];
          if(keys.indexOf(key) === -1) {
              keys.push(key); 
              output.push(item);
          }
      });
      return output;
   };
});

dummymodule.config(
    ['$stateProvider',
        'DUMMYCONSTANTS',
        function ($stateProvider, DUMMYCONSTANTS) {
            $stateProvider.state(DUMMYCONSTANTS.CONFIG.STATE, {
                url: DUMMYCONSTANTS.CONFIG.URL,
                templateUrl: DUMMYCONSTANTS.CONFIG.TEMPLATEURL,
                controller: DUMMYCONSTANTS.CONFIG.CONTROLLER,
                data: {
                    requireLogin: false
                }
            });
        }
]);

dummymodule.controller('DummyController',
    ['$scope',
        '$state',
        '$stateParams',
        '$rootScope',
        'StorageService',
        'genericService',
        'DUMMYCONSTANTS',
        'MESSAGECONSTANTS',
        function ($scope, $state, $stateParams, $rootScope, StorageService, genericService, DUMMYCONSTANTS, MESSAGECONSTANTS) {
            console.log('ForgetPasswordController');
            $scope.form = {};
            $scope.user = {};
            // var assessmentscopy = [];
            // $scope.assessments = [];
            // genericService.getObjects(StorageService.get('baseurl') + 'api/company/getlicensepreferences/'+ $rootScope.userDetails.company.companyId).then(function (data) {
            //     $scope.assessments = angular.copy(data);
            //     assessmentscopy = angular.copy(data);
            //     originaldata
            // }, function (error) {
            //     console.log('error : ' + angular.toJson(error));
            // });

            /**
             * This method calls google api for getting address details
             * based on Zipcode.
             * will be called 
             */
            $scope.getAddressDetails = function (zipcode,type) {
                $scope.validzipcode = true;
                $scope.cities = [];
                $scope.states = [];
                $scope.counties = [];
                $scope.countries = [];
                var locations = [];
                var url = 'maps.googleapis.com/maps/api/geocode/json?address=' + $scope.user.zipcode + '&key=' + MESSAGECONSTANTS.GEOCODING.API_KEY;

                $.getJSON({
                    url: 'https://maps.googleapis.com/maps/api/geocode/json?address=' + zipcode + '&key=' + MESSAGECONSTANTS.GEOCODING.API_KEY,
                    data: {
                        sensor: false,
                    },
                    success: function (data, textStatus) {
                        var createGeoLocation = function(addressdetails,postalname){
                            var addressobj = {};
                            for(var p = 0; p < addressdetails.address_components.length; p++){
                                for(var t = 0; t < addressdetails.address_components[p].types.length; t++){
                                    switch(addressdetails.address_components[p].types[t]) {
                                        case 'country' :
                                                addressobj.country = addressdetails.address_components[p].long_name;
                                                break;
                                        case 'administrative_area_level_2' :
                                                addressobj.county = addressdetails.address_components[p].long_name;
                                                break;
                                        case 'administrative_area_level_1' :
                                                addressobj.state = addressdetails.address_components[p].long_name;
                                                break;
                                        case 'locality':
                                                if(angular.isDefined(postalname) && postalname !== ''){
                                                    addressobj.city = postalname;
                                                }else {
                                                    addressobj.city = addressdetails.address_components[p].long_name;
                                                }
                                                break;
                                        default :
                                                break;
                                    }
                                }
                            }

                            if(angular.isUndefined(addressobj.city) || angular.isUndefined(addressobj.state) || angular.isUndefined(addressobj.country)){
                                for(var p = 0; p < addressdetails.address_components.length; p++){
                                    for(var t = 0; t < addressdetails.address_components[p].types.length; t++){
                                        switch(addressdetails.address_components[p].types[t]){
                                            case 'locality' :
                                                if(angular.isUndefined(addressobj.state)){
                                                    addressobj.state = addressdetails.address_components[p].long_name;
                                                }
                                                if(angular.isUndefined(addressobj.country)){
                                                    addressobj.country = addressdetails.address_components[p].long_name;
                                                }
                                                break;
                                            case 'administrative_area_level_2' :
                                                if(angular.isUndefined(addressobj.city)){
                                                    if(angular.isDefined(postalname) && postalname !== ''){
                                                        addressobj.city = postalname;
                                                    }else {
                                                        addressobj.city = addressdetails.address_components[p].long_name;
                                                    }
                                                }
                                                if(angular.isUndefined(addressobj.state)){
                                                    addressobj.state = addressdetails.address_components[p].long_name;
                                                }
                                                if(angular.isUndefined(addressobj.country)){
                                                    addressobj.country = addressdetails.address_components[p].long_name;
                                                }
                                                
                                                break;
                                            case 'administrative_area_level_3' : 
                                                if(angular.isUndefined(addressobj.city)){
                                                    if(angular.isDefined(postalname) && postalname !== ''){
                                                        addressobj.city = postalname;
                                                    }else {
                                                        addressobj.city = addressdetails.address_components[p].long_name;
                                                    }
                                                }
                                                break;
                                            case 'administrative_area_level_4' : 
                                                if(angular.isUndefined(addressobj.city)){
                                                    if(angular.isDefined(postalname) && postalname !== ''){
                                                        addressobj.city = postalname;
                                                    }else {
                                                        addressobj.city = addressdetails.address_components[p].long_name;
                                                    }
                                                }
                                                break;
                                            case 'sublocality' || 'sublocality_level_1' || 'sublocality_level_5' :
                                                if(angular.isDefined(postalname) && postalname !== ''){
                                                    addressobj.city = postalname;
                                                }else {
                                                    addressobj.city = addressdetails.address_components[p].long_name;
                                                }
                                                break;
                                            case 'administrative_area_level_1' :
                                                if(angular.isUndefined(addressobj.city)){
                                                    if(angular.isDefined(postalname) && postalname !== ''){
                                                        addressobj.city = postalname;
                                                    }else {
                                                        addressobj.city = addressdetails.address_components[p].long_name;
                                                    }

                                                    if(angular.isUndefined(addressobj.country)){
                                                        addressobj.country = addressdetails.address_components[p].long_name;
                                                    }
                                                }
                                                break;
                                        }
                                    }
                                }
                            }
                            //console.log('returning : '+ angular.toJson(addressobj))
                            return addressobj;
                        }
                        if (data.status === 'OK') {
                            for(var i = 0; i < data.results.length; i++){
                                if(angular.isDefined(data.results[i].postcode_localities) && (data.results[i].postcode_localities.length > 0)){
                                    for(var k = 0; k < data.results[i].postcode_localities.length; k++){
                                        var location = createGeoLocation(data.results[i],data.results[i].postcode_localities[k]);
                                        locations.push(angular.copy(location));
                                    }
                                }else {
                                    //console.log('data.results[i] : '+ angular.toJson(data.results[i]));
                                    var location = createGeoLocation(data.results[i]);
                                    locations.push(angular.copy(location));
                                }
                            }
                            //console.log('locations : '+angular.toJson(locations));
                            if(type === 'zipchanged'){
                            	$scope.user.city = locations[0].city;
                                $scope.user.state = locations[0].state;
                                $scope.user.county = locations[0].county;
                                $scope.user.country = locations[0].country;
                            }
                        } else {
                            $scope.validzipcode = false;
                            locations = [];
                            $scope.geoLocations = [];
                            $scope.geoLocationscopy = [];
                            $scope.userprofile.userDetails.city = '';
                            $scope.userprofile.userDetails.state = '';
                            $scope.userprofile.userDetails.county = '';
                            $scope.userprofile.userDetails.country = '';
                        }
                        $scope.geoLocations = locations;
                        $scope.geoLocationscopy = locations;
                        //console.log('$scope.geoLocationscopy : '+ angular.toJson($scope.geoLocationscopy));
                        $scope.$apply();
                    },
                    error: function (data) {
                        console.log('Error: ' + angular.toJson(data));
                        $scope.invalidZipcode = true;
                    }
                });
            }

            $scope.changeAddressObject = function(type,obj){
                //console.log('type : '+type + ' obj : '+ obj);
                switch (type) {
                    case 'city' :
                            for(var p = 0; p < $scope.geoLocationscopy.length; p++){
                                if($scope.geoLocationscopy[p].city === obj){
                                    $scope.user.state = $scope.geoLocationscopy[p].state;
                                    $scope.user.country = $scope.geoLocationscopy[p].country;
                                    $scope.user.county = $scope.geoLocationscopy[p].county;
                                    
                                }
                            }
                            break;
                    case 'state' :
                            for(var p = 0; p < $scope.geoLocationscopy.length; p++){
                                if($scope.geoLocationscopy[p].state === obj){
                                    $scope.user.city = $scope.geoLocationscopy[p].city;
                                    $scope.user.country = $scope.geoLocationscopy[p].country;
                                    $scope.user.county = $scope.geoLocationscopy[p].county;
                                }
                            }
                            break;
                    case 'country' : 
                            for(var p = 0; p < $scope.geoLocationscopy.length; p++){
                                if($scope.geoLocationscopy[p].country === obj){
                                    $scope.user.city = $scope.geoLocationscopy[p].city;
                                    $scope.user.state = $scope.geoLocationscopy[p].state;
                                    $scope.user.county = $scope.geoLocationscopy[p].county;
                                }
                            }
                            break;
                    case 'county' :
                            for(var p = 0; p < $scope.geoLocationscopy.length; p++){
                                if($scope.geoLocationscopy[p].county === obj){
                                    $scope.user.city = $scope.geoLocationscopy[p].city;
                                    $scope.user.country = $scope.geoLocationscopy[p].country;
                                    $scope.user.state = $scope.geoLocationscopy[p].state;
                                }
                            }
                            break;
                }
            }
            
    }
]);