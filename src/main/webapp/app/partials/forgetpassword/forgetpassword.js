/**
 * All the Functionalities Related to Forget password
 * Module is Included Here
 */

var forgetpasswordmodule = angular.module('4dot5.forgetpasswordmodule',
    []);

forgetpasswordmodule.constant('FORGETPASSWORDCONSTANTS', {
    CONFIG: {
        STATE: 'forgetpassword',
        URL: '/forgetpassword',
        CONTROLLER: 'ForgetPasswordController',
        TEMPLATEURL: 'app/partials/forgetpassword/forgetpassword.html',
    },
    SERVICE: {
        FORGET_PASSWORD: 'auth/forgotpassword'
    }
});

forgetpasswordmodule.config(
    ['$stateProvider',
        'FORGETPASSWORDCONSTANTS',
        function ($stateProvider, FORGETPASSWORDCONSTANTS) {
            $stateProvider.state(FORGETPASSWORDCONSTANTS.CONFIG.STATE, {
                url: FORGETPASSWORDCONSTANTS.CONFIG.URL,
                templateUrl: FORGETPASSWORDCONSTANTS.CONFIG.TEMPLATEURL,
                controller: FORGETPASSWORDCONSTANTS.CONFIG.CONTROLLER,
                data: {
                    requireLogin: false
                },
                params: {
                    message: null
                }
            });
        }
    ]);

forgetpasswordmodule.controller('ForgetPasswordController',
    ['$scope',
        '$state',
        '$stateParams',
        '$rootScope',
        'StorageService',
        'forgetPasswordService',
        'genericService',
        'FORGETPASSWORDCONSTANTS',
        function ($scope, $state, $stateParams, $rootScope, StorageService, forgetPasswordService, genericService, FORGETPASSWORDCONSTANTS) {
            console.log('ForgetPasswordController');
            $scope.form = {};
            $scope.user = {};
            if ($stateParams.message !== null) {
                $scope.message = angular.copy($stateParams.message);
                StorageService.set('forgotpasswordmessage',$scope.message);
            }else{
                var message = StorageService.get('forgotpasswordmessage');
                if (message !== null) {
                    $scope.message = message;
                } else {
                    /**
                     * When both state params and backup data is null
                     */
                }
            }

            $scope.getPassword = function (form) {
                var email = $scope.user.email;
                console.log('Requested for Password' + angular.toJson($scope.user.email));
                genericService.getObjects(FORGETPASSWORDCONSTANTS.SERVICE.FORGET_PASSWORD + "/?emailId=" + $scope.user.email).then(function (data) {
                //forgetPasswordService.sendEmail($scope.user.email).then(function (data) {
                    console.log('success : '+angular.toJson(data));
                    $scope.errormessage = null;
                    $scope.form.forgetpasswordform.$setPristine();
                    $scope.confirmlink = 'Password has been sent to your email';
                    //$state.go('validateotp',null,{reload : true});
                }, function (error) {
                    console.log('controller error : ' + angular.toJson(error));
                    $scope.form.forgetpasswordform.$setPristine();
                    $scope.confirmlink = null;
                    $scope.errormessage = error.message;
                });
            }
        }
    ]);
forgetpasswordmodule.factory('forgetPasswordService', ['$q',
    '$http',
    'StorageService',
    'FORGETPASSWORDCONSTANTS',
    function ($q, $http, StorageService, FORGETPASSWORDCONSTANTS) {

        var factory = {};
        /**
         * This will allow to login by calling backend API
         */
        factory.sendEmail = function (email) {
            console.log('email : '+email);
            var deferred = $q.defer();
            $http.get(FORGETPASSWORDCONSTANTS.SERVICE.FORGET_PASSWORD + "/?emailId=" + email).success(function (data, status, headers, config) {
                console.log('service success status : ' + angular.toJson(status));
                console.log('service success data : ' + angular.toJson(data));
                if (angular.isDefined(data.statusCode)) {
                    if (angular.equals(data.statusCode, 200)) {
                        deferred.resolve(data);
                    } else {
                        deferred.reject(data);
                    }
                }else {
                    if(status === 200){
                        deferred.resolve(status);
                    }else {
                        deferred.reject(data);
                    }
                }
            }).error(function (data, status, headers, config) {
                console.log('service error : ' + angular.toJson(data));
                deferred.reject(data);
            });
            return deferred.promise;
        };
        return factory;
    }
]);