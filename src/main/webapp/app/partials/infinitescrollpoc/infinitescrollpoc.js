/**
 * All the Functionalities Related to Forget password
 * Module is Included Here
 */

var infinitescrollpocmodule = angular.module('4dot5.infinitescrollpoc',
    ['infinite-scroll']);

infinitescrollpocmodule.constant('INFINITESCROLLPOCCONSTANTS', {
    CONFIG: {
        STATE: 'infinitescrollpoc',
        URL: '/infinitescrollpoc',
        CONTROLLER: 'InfiniteScrollPocController',
        TEMPLATEURL: 'app/partials/infinitescrollpoc/infinitescrollpoc.html',
    },
});

infinitescrollpocmodule.config(
    ['$stateProvider',
        'INFINITESCROLLPOCCONSTANTS',
        function ($stateProvider, INFINITESCROLLPOCCONSTANTS) {
            $stateProvider.state(INFINITESCROLLPOCCONSTANTS.CONFIG.STATE, {
                url: INFINITESCROLLPOCCONSTANTS.CONFIG.URL,
                templateUrl: INFINITESCROLLPOCCONSTANTS.CONFIG.TEMPLATEURL,
                controller: INFINITESCROLLPOCCONSTANTS.CONFIG.CONTROLLER,
                data: {
                    requireLogin: false
                },
                params: {
                    message: null
                }
            });
        }
    ]);

infinitescrollpocmodule.controller('InfiniteScrollPocController',
    ['$scope',
        '$state',
        '$stateParams',
        '$rootScope',
        'StorageService',
        'forgetPasswordService',
        'genericService',
        'INFINITESCROLLPOCCONSTANTS',
        'Reddit',
        function ($scope, $state, $stateParams, $rootScope, StorageService, forgetPasswordService, genericService, INFINITESCROLLPOCCONSTANTS, Reddit) {
            console.log('InfiniteScrollPocController');
            $scope.reddit = new Reddit();

        }
    ]);
infinitescrollpocmodule.factory('forgetPasswordService', ['$q',
    '$http',
    'StorageService',
    'INFINITESCROLLPOCCONSTANTS',
    function ($q, $http, StorageService, INFINITESCROLLPOCCONSTANTS) {

        var factory = {};
        /**
         * This will allow to login by calling backend API
         */
        factory.sendEmail = function (email) {
            var Reddit = function () {
                this.items = [];
                this.busy = false;
                this.after = '';
            };

            Reddit.prototype.nextPage = function () {
                if (this.busy) return;
                this.busy = true;

                var url = "https://api.reddit.com/hot?after=" + this.after + "&jsonp=JSON_CALLBACK";
                $http.jsonp(url).success(function (data) {
                    var items = data.data.children;
                    for (var i = 0; i < items.length; i++) {
                        this.items.push(items[i].data);
                    }
                    this.after = "t3_" + this.items[this.items.length - 1].id;
                    this.busy = false;
                }.bind(this));
            };

            return Reddit;
        };
        return factory;
    }
]);