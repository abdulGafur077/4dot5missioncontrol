/**
 * All the functionalities related to login
 * module is included here
 * @author Ashok
 */

var loginmodule = angular.module('4dot5.loginmodule',
    []);

loginmodule.constant('LOGINCONSTANTS', {
    CONFIG: {
        STATE: 'login',
        URL: '/login',
        CONTROLLER: 'LoginController',
        TEMPLATEURL: 'app/partials/login/login.html'
    },
    CONTROLLER: {
        REMEMBER_ME: ''
    },
    SERVICE: {
        LOGIN: 'auth/login',
        REMEMBER_ME: ''
    }
});

loginmodule.config(
    ['$stateProvider',
        'LOGINCONSTANTS',
        function ($stateProvider, LOGINCONSTANTS) {
            $stateProvider.state(LOGINCONSTANTS.CONFIG.STATE, {
                url: LOGINCONSTANTS.CONFIG.URL,
                templateUrl: LOGINCONSTANTS.CONFIG.TEMPLATEURL,
                controller: LOGINCONSTANTS.CONFIG.CONTROLLER,
                data: {
                    requireLogin: false
                },
                params: {
                    message: null
                }
            });
        }
    ]);

loginmodule.controller('LoginController',
    ['$scope',
        '$state',
        '$window',
        '$stateParams',
        '$rootScope',
        'StorageService',
        'loginService',
        'genericService',
        'LOGINCONSTANTS',
        'MESSAGECONSTANTS',
        'alertsAndNotificationsService',
        function ($scope, $state, $window, $stateParams, $rootScope, StorageService, loginService, genericService, LOGINCONSTANTS, MESSAGECONSTANTS, alertsAndNotificationsService) {
            $scope.form = {};
            $scope.user = {};

            $rootScope.userActive = false;
            $scope.rememberme = true;
            
            var logoutemail = angular.copy(StorageService.getCookie('logoutemail'));
            if(angular.isDefined(logoutemail) && (logoutemail !== null)){
                $scope.user.email = logoutemail;
            }
            
            var manageScreenListforRoles = function(screenlist){
            	for(var i = 0; i < screenlist.length; i++){
            		delete screenlist[i].name;
            	}
            	return screenlist;
            };
            
            var createRoleScreenRefObj = function(roleScreenRef){
            	var roleObj = {};
            	roleObj.role = {};
            	roleObj.role.id = roleScreenRef.role.id;
            	roleObj.role.name = roleScreenRef.role.name;
            	roleObj.screen = manageScreenListforRoles(roleScreenRef.screen);
            	return roleObj;
            };

            var createUserDetailsObject = function(data){
                var obj = {};
                obj.company = {};
                obj.id = data.id;
                obj.firstname = data.firstname;
                obj.lastname = data.lastname;
                obj.emailId = data.emailId;
                obj.companyId = data.companyId;
                obj.accesstoken = data.token;
                obj.company.companyId = data.company.companyId;
                obj.company.companyName = data.company.companyName;
                obj.company.companyType = data.company.companyType;
                obj.company.companyState = 'Active';
                obj.accountState = data.accountState;
                obj.shouldPopupBeDisplayed = data.shouldPopupBeDisplayed;
                obj.roleScreenRef = createRoleScreenRefObj(data.roleScreenRef);
                if(data.roleScreenRef.role.name !=='4dot5SuperUser' && data.roleScreenRef.role.name !=='4dot5Admin'){
                    obj.company.companySelected = true; 
                }else {
                    obj.company.companySelected = false; 
                }
                return obj;
            };

            /**
             * This method is invoked from login success call.
             * it manages the redirections based on user state
             */
            var manageUserState = function (data) {
                if (angular.equals(data.accountState, MESSAGECONSTANTS.USER_STATES.ACTIVE)) {
                    $rootScope.userActive = true;
                    console.log('createUserDetailsObject(data) : ' + angular.toJson(createUserDetailsObject(data)));
                    StorageService.setCookie('userDetails', createUserDetailsObject(data), { 'expires': new Date(Date.now() + $rootScope.tokenvalidity) });
                    if(data.roleScreenRef.role.name === '4dot5SuperUser'){
                        $state.go('missioncontrol.teammembers');
                    }else{
                        $state.go('missioncontrol.dashboard', null, { reload: false });
                    }
                } else if (angular.equals(data.accountState, MESSAGECONSTANTS.USER_STATES.UNVERIFIED) || angular.equals(data.accountState, MESSAGECONSTANTS.USER_STATES.PASSWORDRESET)) {
                    StorageService.setCookie('userDetails', createUserDetailsObject(data), { 'expires': new Date(Date.now() + $rootScope.tokenvalidity) });
                    var message = 'Change Password to enter into application';
                    $state.go('resetpassword', { message: message}, { reload: false });
                } else if (angular.equals(data.accountState, MESSAGECONSTANTS.USER_STATES.INACTIVE)) {
                    $scope.message = 'Your Account is InActive. Contact Administrator for Details';
                } else if (angular.equals(data.accountState, MESSAGECONSTANTS.USER_STATES.BLOCKED)) {
                    $scope.message = 'Your Account is Blocked. Contact Administrator for Details';
                }
            };

            /**
             * This method is invoked from login success call.
             * It manages cookis storage and rememberme status
             */
            var manageCookiesandRememberMe = function (data) {
                if ($scope.rememberme) {
                    StorageService.setCookie('rememberme', true, { 'expires': new Date(Date.now() + $rootScope.refreshtokenvalidity) });
                } else {
                    StorageService.setCookie('rememberme', false, { 'expires': new Date(Date.now() + $rootScope.refreshtokenvalidity) });
                }
                StorageService.setCookie('refreshtoken', data.refreshToken, { 'expires': new Date(Date.now() + $rootScope.refreshtokenvalidity) });
            };

            /**
             * When user Gives input and initiates Login
             * This function is called after the form is validated
             */
            $scope.validateUser = function () {
                loginService.login($scope.user).then(function (data) {
                    StorageService.removeCookie('logoutemail');
                    manageCookiesandRememberMe(data);
                    manageUserState(data);
                }, function (error) {
                    $scope.form.loginform.$setPristine();
                    if(error.statusCode === '401'){
                    	$scope.exception = error.message;
                    }else if($rootScope.isOnline){
                    	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    }
                });
            }
        }
    ]);

loginmodule.factory('loginService', ['$q',
    '$http',
    'StorageService',
    '$state',
    'LOGINCONSTANTS',
    function ($q, $http, StorageService, $state, LOGINCONSTANTS) {

        var factory = {};
        /**
         * This will allow to login by calling backend API
         */
        factory.login = function (user) {
            var deferred = $q.defer();
            $http.post(LOGINCONSTANTS.SERVICE.LOGIN, user).success(function (data, status, headers, config) {
                if (angular.equals(status, 200) && angular.isDefined(data.token) && angular.isDefined(data.refreshToken) && angular.isDefined(data.roleScreenRef) && angular.isDefined(data.accountState)) {
                    deferred.resolve(data);
                } else {
                    deferred.reject(data);
                }
            }).error(function (data, status, headers, config) {
                console.log('service error : ' + angular.toJson(data));
                deferred.reject(data);
            });
            return deferred.promise;
        };
        return factory;
    }
]);
