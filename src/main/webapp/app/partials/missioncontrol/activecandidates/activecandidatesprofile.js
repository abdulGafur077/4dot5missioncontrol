/**
 * 
 */

var activecandidatesprofilemodule = angular.module('4dot5.missioncontrolmodule.activecandidatesprofilemodule', []);

activecandidatesprofilemodule.constant('ACTIVECANDIDATESPROFILECONSTANTS', {

    CONFIG: {
        STATE: 'missioncontrol.activecandidatesprofile',
        URL: '/activecandidatesprofile',
        CONTROLLER: 'ActiveCandidatesprofileController',
        TEMPLATEURL: 'app/partials/missioncontrol/activecandidates/activecandidatesprofile.html',
    },
    CONTROLLER: {

    }

});

activecandidatesprofilemodule.config(
    ['$stateProvider',
        'ACTIVECANDIDATESPROFILECONSTANTS',
        function($stateProvider, ACTIVECANDIDATESPROFILECONSTANTS) {
            $stateProvider.state(ACTIVECANDIDATESPROFILECONSTANTS.CONFIG.STATE, {
                url: ACTIVECANDIDATESPROFILECONSTANTS.CONFIG.URL,
                templateUrl: ACTIVECANDIDATESPROFILECONSTANTS.CONFIG.TEMPLATEURL,
                controller: ACTIVECANDIDATESPROFILECONSTANTS.CONFIG.CONTROLLER,
                params: {
                    candidatesType: null,
                    filterValues: null
                },
                data: {
                    requireLogin: true
                }
            });
        }
    ]);

activecandidatesprofilemodule.controller('ActiveCandidatesprofileController', ['$scope',
    '$http',
    '$state',
    '$timeout',
    '$rootScope',
    '$stateParams',
    'genericService',
    'StorageService',
    'ACTIVECANDIDATESPROFILECONSTANTS',
    function($scope, $http, $state, $timeout, $rootScope, $stateParams, genericService, StorageService, ACTIVECANDIDATESPROFILECONSTANTS) {
        console.log('ActiveCandidatesprofileController');

        $scope.companyType = $rootScope.userDetails.company.companyType;
        $scope.inputTextbox = false;
        $scope.toggleInputBox = function() {
            $scope.inputTextbox = !$scope.inputTextbox;
        };


        $scope.searchTerm = '';
        $scope.candidatesType = $stateParams.candidatesType;
        $scope.candidatesTypeLabel = 'Active Candidates Job View';
        // console.log('Filter Values are: ' + $stateParams.filterValues)
        //filters
        $scope.filterValues = $stateParams.filterValues;
        $scope.filterType = '';
        $scope.requisitionRoleFilter = '';
        $scope.requistionCompanyFilter = '';
        $scope.teamMemberFilter = '';


        $scope.candidateNamesList = [];
        $scope.filteredCandidateList = [];
        $scope.filteredCandidateNamesList = [];
        $scope.compareArray = []; //compare array

        $scope.init = function() {

            // active candidates list
            $scope.qualifiedCandidatesData = [];
            $scope.techAssCandidatesData = [];
            $scope.valueAssCandidatesData = [];
            $scope.verificationCandidatesData = [];
            $scope.goldenStandardCandidatesData = [];         
            $scope.notInterestedCandidatesData = [];
            $scope.releasedCandidatesData = [];

            //filters
            /*$scope.filterValues = $scope.filterValues.split('|');*/
            // console.log('filterValues - ', $scope.filterValues);
            if (angular.isDefined($scope.filterValues) && $scope.filterValues != null) {
                if ($scope.filterValues.length) {
                    $scope.filterType = $scope.filterValues[0];
                    if ($scope.filterType === 'requisition') {
                        $scope.requisitionRoleFilter = $scope.filterValues[1];
                        $scope.requistionCompanyFilter = $scope.filterValues[2];
                    } else if ($scope.filterType === 'teamMember') {
                        $scope.teamMemberFilter = $scope.filterValues[1];
                    } else {
                        //
                    }
                }
            }

            // localhost:8080/missioncontrolapiserver/app/partials/validateotp/validateotp.js
            // ttp://localhost:8080/data/passiveCandidates.json
            if ($scope.candidatesType === 'active') {
                $http.get("app-content/jsonobjects/candidates.json")
                    .success(function(data) {
                        angular.forEach(data, function(val, key) {
                             console.log('Values is: ' + angular.toJson(val));
                            if ($scope.candidateToBeIncluded(val)) {
                                if (val.State === 'Qualified') {
                                    $scope.qualifiedCandidatesData.push(val);
                                } else if (val.State === 'Tech. Assessment') {
                                    $scope.techAssCandidatesData.push(val);
                                } else if (val.State === 'Value Assessment') {
                                    $scope.valueAssCandidatesData.push(val);
                                } else if (val.State === 'Verification') {
                                    $scope.verificationCandidatesData.push(val);
                                } else if (val.State === 'Golden Standard') {
                                    $scope.goldenStandardCandidatesData.push(val);
                                } else if (val.State === 'Not Interested') {
                                    $scope.notInterestedCandidatesData.push(val);
                                } else if (val.State === 'Released') {
                                    $scope.releasedCandidatesData.push(val);
                                } else {
                                    // do nothing
                                }
                                $scope.candidateNamesList.push({ name: val.Name, ticked: true });
                            }
                        });
                    });
            }
        }

        $scope.candidateToBeIncluded = function(candidate) {
            if ($scope.filterType === 'requisition') {
                return (candidate.Role === $scope.requisitionRoleFilter && candidate.Company === $scope.requistionCompanyFilter);
            } else if ($scope.filterType === 'teamMember') {
                return (candidate.Recruiter === $scope.teamMemberFilter);
            } else {
                return true;
            }
        };

        $scope.copyAllCandidatesToFiltered = function() {
            $scope.filteredCandidateNamesList = $scope.candidateNamesList;
        };

        $scope.removeItemFromList = function(list, item) {
            for (i = 0; i < list.length; i++) {
                if (angular.equals(list[i], item)) {
                    list.splice(i, 1);
                }
            }
        };

        $scope.searchCandidates = function(candidate) {
            // console.log('called -',candidate);
            return (angular.lowercase(candidate.Name).indexOf(angular.lowercase($scope.searchTerm) || '') !== -1 ||
                angular.lowercase(candidate.Company).indexOf(angular.lowercase($scope.searchTerm) || '') !== -1 ||
                angular.lowercase(candidate.Role).indexOf(angular.lowercase($scope.searchTerm) || '') !== -1);
        };

        $scope.toggleView = function(columnType) {
            // console.log('entering the toggle'+columnType);
            var column = angular.element(document.getElementById(columnType + '-candidates-column'));
            var toggleIcon = angular.element(document.getElementById(columnType + '-candidates-column-toogle-icon'));
            // console.log('has class - ', column.hasClass('candidate-state-column'));
            if (column.hasClass('candidate-state-column')) {
                column.removeClass('candidate-state-column');
                column.addClass('candidate-state-column-collapse');
                toggleIcon.removeClass('fa-chevron-circle-left');
                toggleIcon.addClass('fa-chevron-circle-right');
                toggleIcon.attr('title', 'Expand Column');
            } else {
                column.removeClass('candidate-state-column-collapse');
                column.addClass('candidate-state-column');
                toggleIcon.removeClass('fa-chevron-circle-right');
                toggleIcon.addClass('fa-chevron-circle-left');
                toggleIcon.attr('title', 'Collapse Column');
            }
        };

        $scope.compareCandidates = function() { //comparing candidates
            if ($scope.compareArray.length <= 1) {
                alert('Please choose at least two candidates to compare');
                // console.log('$scope.compareArray' + angular.toJson($scope.compareArray));
            } else {
                // console.log('$scope.compareArray' + angular.toJson($scope.compareArray));
                jobType = $scope.compareArray[0].Role;
                company = $scope.compareArray[0].Company;
                candidate1 = $scope.compareArray[0].Name;
                candidate2 = '';
                candidate3 = '';
                breadcrumb = 'Active Candidates';
                if (angular.isDefined($scope.compareArray[1])) {
                    candidate2 = $scope.compareArray[1].Name;
                    if (angular.isDefined($scope.compareArray[2])) {
                        candidate3 = $scope.compareArray[2].Name;
                    }
                }
                $state.go('missioncontrol.candidatecompare', { jobType: jobType, companyName: company, candidate1: candidate1, candidate2: candidate2, candidate3: candidate3, breadcrumb: breadcrumb });
            }
        };


        $scope.addOrRemoveFromCompare = function(candidate, event) { //add or remove from compare
            // console.log('entering here successfully' + angular.toJson(candidate.Role) + ' ' + angular.toJson(candidate.Company)); //need to be worked

            // console.log('candidate.Role of selection' + angular.toJson(candidate.Role));
            // console.log('candidate.Company of selection' + angular.toJson(candidate.Company));
            // console.log('seee' + angular.toJson($scope.compareArray[0]));

            if ($scope.compareArray.length >= 1) {
                // console.log('lets start validation');
                // console.log('seee' + angular.toJson($scope.compareArray[0].Role) + ' ' + angular.toJson($scope.compareArray[0]));
                // console.log('candidate.Company of selection' + angular.toJson(candidate));



                if (($scope.compareArray[0].Company) === (candidate.Company) & ($scope.compareArray[0].Role) === (candidate.Role)) {
                    // console.log('validating company');
                } else {
                    // console.log('child inner else');
                    alert('Candiadtes of same Company and Role can only be compared');
                    $scope.compareArray.splice(key, 1);
                    var removed = false;
                    angular.forEach($scope.compareArray, function(val, key) {
                        if (val.Role === candidate.Role && val.Name === candidate.Name && val.Company === candidate.Company) {
                            _removeCandidate(key, image);
                            removed = true;
                        }
                    });
                    if (!removed) {
                        _addCandidate(candidate, image);
                    }

                }
            } else {
                // console.log('parent outer else');
            }

            var image = angular.element(event.target);
            if ($scope.compareArray.length === 0) {
                _addCandidate(candidate, image);
            } else {
                var removed = false;
                angular.forEach($scope.compareArray, function(val, key) {
                    if (val.Role === candidate.Role && val.Name === candidate.Name && val.Company === candidate.Company) {
                        _removeCandidate(key, image);
                        removed = true;
                    }
                });
                if (!removed) {
                    _addCandidate(candidate, image);
                }
            }

            function _removeCandidate(key, image) { //remove candidate
                $scope.compareArray.splice(key, 1);
                image.toggleClass('profile-img-selected');
            }

            function _addCandidate(obj) { //add candidate
                if ($scope.compareArray.length === 3) {

                    alert('A maximum of 3 candidates can be compared at once. Please unselect another candidate to add ' + candidate.Name + ' ' + candidate.Company + candidate.Role + ' to compare');
                } else {
                    $scope.compareArray.push(obj);
                    image.toggleClass('profile-img-selected');
                    // console.log('compareArray' + angular.toJson($scope.compareArray));
                }
            }
            // console.log('compare array', $scope.compareArray);

        };


        $scope.searchBasedOnFilters = function(filter) {
            $scope.SearchFilters = filter;
            // console.log('SearchBasedOnFilters function : ' + angular.toJson(filter));
            //  $("#search-panel").hide();
            $(document).off("click", "#filterSearch").on("click", "#filterSearch",
                function() {
                    $("#search-panel").animate({
                        height: 'toggle'
                    });
                });
        }

        
        //Implementaion of filters 
        $(document).ready(function() { 
            $("#search-panel").hide();
            $(document).off("click", "#open-filter").on("click", "#open-filter",
                function() {
                    $("#search-panel").animate({
                        height: 'toggle'
                    });
                });
        });

        $scope.dropCallback = function(index, item, external, type){    	
        	//alert(angular.toJson(type));
        	//$scope.qualifiedCandidatesData;
        	console.log('qualified list'+ angular.toJson($scope.qualifiedCandidatesData));
           // $scope.techAssCandidatesData;
            console.log('tech assess list'+angular.toJson($scope.techAssCandidatesData));
           // $scope.valueAssCandidatesData;
            console.log('value assess list'+angular.toJson($scope.valueAssCandidatesData));
            
            if($scope.updateCandidateStep(item, type))
            	return item;
            else{
            	alert('This is not a valid step for '+item.Company);
            	return false;
            }
        }
        
       $scope.updateCandidateCard = function() {
    	   alert('ss');
       }
       
       $scope.updateCandidateStep = function(item, newStep){
//    	   	item.State = newStep;
//           	genericService.putObject(StorageService.get('baseurl') + 'updateCandidateStep', item).then(function (data) {
//           	
//           		return true;
//           }, function (error) {
//               return false;
//           });
//           return false;
           
    	   if(item.Company === 'The Red Tree Company' && newStep === 'Tech. Assessment'){		   
    		   return false;    		   
    	   }else {
    		   return true;
    	   }
       }
        
        $scope.init();
    }
]);