/**
 * This module deals with add candidates operations
 */

var addCandidatesModule = angular.module('4dot5.missioncontrolmodule.addCandidatesModule', []);

addCandidatesModule.constant('AddCandidatesConstants', {
    CONFIG: {
        STATE: 'missioncontrol.addCandidates',
        URL: '/addCandidates',
        CONTROLLER: 'AddCandidatesViewController',
        CONTROLLERAS: 'addCandidatesView',
        TEMPLATEURL: 'app/partials/missioncontrol/addCandidates/add-candidates.html'
    }
});

addCandidatesModule.config(['$stateProvider', 'AddCandidatesConstants',
    function ($stateProvider, AddCandidatesConstants) {
        $stateProvider.state(AddCandidatesConstants.CONFIG.STATE, {
            url: AddCandidatesConstants.CONFIG.URL,
            templateUrl: AddCandidatesConstants.CONFIG.TEMPLATEURL,
            controller: AddCandidatesConstants.CONFIG.CONTROLLER,
            controllerAs: AddCandidatesConstants.CONFIG.CONTROLLERAS,
            data: {
                requireLogin: true
            },
            params: {
                jobId: null
            }
        });
    }
]);

addCandidatesModule.controller('AddCandidatesViewController', AddCandidatesViewController);

addCandidatesModule.$inject = ['$state','MESSAGECONSTANTS'];

function AddCandidatesViewController ($state, MESSAGECONSTANTS) {
    var vm = this;
    vm.MESSAGECONSTANTS = MESSAGECONSTANTS;
    vm.saveCallback = saveCallback;

    function saveCallback(){
        $state.go('missioncontrol.candidates');
    }
}