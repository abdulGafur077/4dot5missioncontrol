/**
 * This module deals with Management of assessments
 */

var assessmentManagementModule = angular.module('4dot5.missioncontrolmodule.assessmentManagementModule', []);

assessmentManagementModule.constant('ASSESSMENTMANAGEMENTCONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.assessmentManagement',
        URL: '/assessmentManagement',
        CONTROLLER: 'AssessmentManagementController',
        CONTROLLERAS: 'assessManage',
        TEMPLATEURL: 'app/partials/missioncontrol/assessmentManagement/assessmentManagement.html'
    }
});
assessmentManagementModule.config(['$stateProvider', 'ASSESSMENTMANAGEMENTCONSTANTS',
    function ($stateProvider, ASSESSMENTMANAGEMENTCONSTANTS) {
        $stateProvider.state(ASSESSMENTMANAGEMENTCONSTANTS.CONFIG.STATE, {
            url: ASSESSMENTMANAGEMENTCONSTANTS.CONFIG.URL,
            templateUrl: ASSESSMENTMANAGEMENTCONSTANTS.CONFIG.TEMPLATEURL,
            controller: ASSESSMENTMANAGEMENTCONSTANTS.CONFIG.CONTROLLER,
            controllerAs: ASSESSMENTMANAGEMENTCONSTANTS.CONFIG.CONTROLLERAS,
            data: {
                requireLogin: true
            },
            params: {
                jobId: null,
                jobTitle: null,
                jobClient: null
            }
        });
    }
]);

assessmentManagementModule.controller('AssessmentManagementController', AssessmentManagementController);

assessmentManagementModule.$inject = ['$rootScope', '$stateParams','assessmentService', 'alertsAndNotificationsService'];

function AssessmentManagementController($rootScope, $stateParams, assessmentService, alertsAndNotificationsService) {
    var vm = this;

    vm.assignedAssessments = [];
    vm.unAssignedAssessments = [];
    vm.assessmentsToAssign = [];
    vm.assessmentsToRemove = [];
    vm.companyId = $rootScope.userDetails.company.companyId;
    //flags
    vm.assignInProgressFlag = false;
    vm.removeInProgressFlag = false;
    vm.syncInProgressFlag = false;
    vm.disableAssignAssessmentsButtonFlag = true;
    vm.disableRemoveAssessmentsButtonFlag = true;
    vm.disableSyncAssessmentsButtonFlag = false;
    // methods
    vm.init = init;
    vm.getAllAssignedAssessments = getAllAssignedAssessments;
    vm.getAllUnassignedAssessments = getAllUnassignedAssessments;
    vm.addAssessment = addAssessment;
    vm.removeAssessment = removeAssessment;
    vm.assignAssessments = assignAssessments;
    vm.removeAssessments = removeAssessments;
    vm.syncAssessments = syncAssessments;

    vm.init();

    function init(){
        vm.getAllUnassignedAssessments();
        vm.getAllAssignedAssessments();
    }

    function getAllAssignedAssessments(){
        var counter = 0;
        var techAssessmentsArray = [];
        var valueAssessmentsArray = [];
        assessmentService.getAllValueAssessments(vm.companyId, function (data) {
            angular.forEach(data, function (val, key) {
                val.testName = val.assessmentAliasName;
                val.removeFlag = false;
                valueAssessmentsArray.push(val);
            });
            counter++;
            if(counter == 2){
                vm.assignedAssessments = techAssessmentsArray;
                angular.forEach(valueAssessmentsArray, function (val, key) {
                    vm.assignedAssessments.push(val);
                });
            }
        }, function(error){
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
        assessmentService.getAllTechAssessments(vm.companyId, function (data) {
            angular.forEach(data, function (val, key) {
               val.testName = val.assessmentAliasName;
               val.removeFlag = false;
               techAssessmentsArray.push(val);
            });
            counter++;
            if(counter == 2){
                vm.assignedAssessments = valueAssessmentsArray;
                angular.forEach(techAssessmentsArray, function (val, key) {
                    vm.assignedAssessments.push(val);
                });
            }
        }, function(error){
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }

    function getAllUnassignedAssessments(){
        assessmentService.getAllUnassignedAssessments(vm.companyId, function (data) {
            angular.forEach(data, function (val, key) {
                val.addFlag = false;
                val.testName = val.assessmentAliasName;
            });
            vm.unAssignedAssessments = data;
        }, function(error){
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }

    function addAssessment(assessment) {
        if(assessment.addFlag){
            vm.assessmentsToAssign.push(assessment.assessmentId);
            vm.disableAssignAssessmentsButtonFlag = false;
        }else{
            _.remove(vm.assessmentsToAssign, function (n) {
                return n == assessment.assessmentId;
            });
            if(vm.assessmentsToAssign.length == 0){
                vm.disableAssignAssessmentsButtonFlag = true;
            }
        }
    }

    function removeAssessment(assessment) {
        if(assessment.removeFlag){
            vm.assessmentsToRemove.push(assessment.assessmentId);
            vm.disableRemoveAssessmentsButtonFlag = false;
        }else{
            _.remove(vm.assessmentsToRemove, function (n) {
                return n == assessment.assessmentId;
            });
            if(vm.assessmentsToRemove.length == 0){
                vm.disableRemoveAssessmentsButtonFlag = true;
            }
        }
    }

    function assignAssessments(){
        var assignObject = {};
        assignObject[vm.companyId] = vm.assessmentsToAssign;
        vm.assignInProgressFlag = true;
        vm.disableAssignAssessmentsButtonFlag = true;
        assessmentService.assignAssessments(assignObject, function(data) {
            vm.getAllUnassignedAssessments();
            vm.getAllAssignedAssessments();
            vm.assessmentsToAssign = [];
            vm.assignInProgressFlag = false;
        }, function (error) {
            vm.disableAssignAssessmentsButtonFlag = false;
            vm.assignInProgressFlag = false;
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }

    function removeAssessments(){
        vm.removeInProgressFlag = true;
        vm.disableRemoveAssessmentsButtonFlag = true;
        var counter = 0;
        angular.forEach(vm.assessmentsToRemove, function (val, key) {
           assessmentService.removeAssignment(vm.companyId, val, function (data) {
               counter++;
               if(counter == vm.assessmentsToRemove.length){
                   vm.getAllUnassignedAssessments();
                   vm.getAllAssignedAssessments();
                   vm.assessmentsToRemove = [];
                   vm.removeInProgressFlag = false;
               }
           }, function(error) {
               vm.disableRemoveAssessmentsButtonFlag = false;
               vm.removeInProgressFlag = false;
               if ($rootScope.isOnline) {
                   alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
               }
           });
        });
    }

    function syncAssessments(){
        vm.syncInProgressFlag = true;
        vm.disableSyncAssessmentsButtonFlag = true;
        assessmentService.syncAssessments(vm.companyId, function (data) {
            vm.getAllUnassignedAssessments();
            vm.getAllAssignedAssessments();
            vm.disableSyncAssessmentsButtonFlag = false;
            vm.syncInProgressFlag = false;
        }, function (error) {
            vm.disableSyncAssessmentsButtonFlag = false;
            vm.syncInProgressFlag = false;
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }
}
