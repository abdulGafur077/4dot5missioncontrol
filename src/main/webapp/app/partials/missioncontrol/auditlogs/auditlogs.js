/**
 * 
 */
var auditlogsmodule = angular.module('4dot5.missioncontrolmodule.auditlogsmodule',[]);

auditlogsmodule.constant('AUDITLOGSCONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.auditlogs',
        URL: '/auditlogs',
        CONTROLLER: 'AuditlogsController',
        TEMPLATEURL: 'app/partials/missioncontrol/auditlogs/auditlogs.html',
    },
    CONTROLLER: {

    }
});

auditlogsmodule.config(
	    ['$stateProvider',
	        'AUDITLOGSCONSTANTS',
	        function ($stateProvider, AUDITLOGSCONSTANTS) {
	            $stateProvider.state(AUDITLOGSCONSTANTS.CONFIG.STATE, {
	                url: AUDITLOGSCONSTANTS.CONFIG.URL,
	                templateUrl: AUDITLOGSCONSTANTS.CONFIG.TEMPLATEURL,
	                controller: AUDITLOGSCONSTANTS.CONFIG.CONTROLLER,
	                data: {
	                    requireLogin: true
	                }
	            });
	        }
	    ]);

auditlogsmodule.controller('AuditlogsController',
		['$scope',
         '$state',
         '$timeout',
         '$rootScope',
         '$stateParams',
         'genericService',
         'AUDITLOGSCONSTANTS',
         'StorageService',
         function ($scope, $state, $timeout, $rootScope, $stateParams, genericService, AUDITLOGSCONSTANTS, StorageService) {
			console.log('AuditlogsController');

		}
]);