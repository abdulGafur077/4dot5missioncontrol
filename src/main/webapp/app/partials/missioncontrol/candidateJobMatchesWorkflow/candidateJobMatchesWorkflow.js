/**
 * This module deals with Candidate Workflow Card
 */

var candidateJobMatchesWorkflowModule = angular.module('4dot5.missioncontrolmodule.candidateJobMatchesWorkflowModule', []);

candidateJobMatchesWorkflowModule.constant('CANDIDATEJOBMATCHESWORKFLOWCONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.candidateJobMatchesWorkflow',
        URL: '/candidateJobMatchesWorkflow',
        CONTROLLER: 'CandidateJobMatchesWorkflowController',
        CONTROLLERAS: 'workflow',
        TEMPLATEURL: 'app/partials/missioncontrol/candidateJobMatchesWorkflow/workflow.html'
    }
});
candidateJobMatchesWorkflowModule.config(['$stateProvider', 'CANDIDATEJOBMATCHESWORKFLOWCONSTANTS',
    function ($stateProvider, CANDIDATEJOBMATCHESWORKFLOWCONSTANTS) {
        $stateProvider.state(CANDIDATEJOBMATCHESWORKFLOWCONSTANTS.CONFIG.STATE, {
            url: CANDIDATEJOBMATCHESWORKFLOWCONSTANTS.CONFIG.URL,
            templateUrl: CANDIDATEJOBMATCHESWORKFLOWCONSTANTS.CONFIG.TEMPLATEURL,
            controller: CANDIDATEJOBMATCHESWORKFLOWCONSTANTS.CONFIG.CONTROLLER,
            controllerAs: CANDIDATEJOBMATCHESWORKFLOWCONSTANTS.CONFIG.CONTROLLERAS,
            data: {
                requireLogin: true
            },
            params: {
                jobId: null,
                jobTitle: null,
                jobClient: null,
                city: null,
                state: null,
                visibilityFiltersObject: null
            }
        });
    }
]);

candidateJobMatchesWorkflowModule.controller('CandidateJobMatchesWorkflowController', CandidateJobMatchesWorkflowController);

candidateJobMatchesWorkflowModule.$inject = ['$rootScope', '$stateParams', '$uibModal', '$timeout','jobMatchService', 'jobService', 'planService', 'Candidate', 'JobMatchesQueryFilter', 'alertsAndNotificationsService'];

function CandidateJobMatchesWorkflowController($rootScope, $stateParams, $uibModal, $timeout, jobMatchService, jobService, planService, Candidate, JobMatchesQueryFilter, alertsAndNotificationsService) {
    var vm = this;
    // variables
    vm.companyId = $rootScope.userDetails.company.companyId;
    vm.userId = $rootScope.userDetails.id;
    vm.jobId = null;
    vm.candidatesObject = {};
    vm.globalQueryFilterObject = {};
    vm.enabledWorkflowSteps = {};
    vm.activityMap = [];
    vm.jobMatchStates = [];
    vm.selectedJobMatchStates = [];
    //flags
    vm.hideSearchStringFlag = false;
    vm.jobIntegrationEnabled = false;
    // functions
    vm.init = init;
    vm.resetCandidateJobMatches = resetCandidateJobMatches;
    vm.queryJobMatches =  queryJobMatches;
    vm.getEnabledWorkflowSteps = getEnabledWorkflowSteps;
    vm.performActionMultiple = performActionMultiple;
    vm.toggleColumn = toggleColumn;
    vm.getAllActivityTypes = getAllActivityTypes;
    vm.getAllJobMatchStates = getAllJobMatchStates;
    vm.filterSelectRemoveAll = filterSelectRemoveAll;
    vm.onFilterItemClick = onFilterItemClick;
    vm.resetWorkflowFilters = resetWorkflowFilters;
    vm.getPlanDetails = getPlanDetails;

    function init(){
        vm.jobId = _.isNull($stateParams.jobId) ? '' : $stateParams.jobId;
        vm.jobTitle = _.isNull($stateParams.jobTitle) ? '' : $stateParams.jobTitle;
        vm.jobClient = _.isNull($stateParams.jobClient) ? '' : $stateParams.jobClient;
        vm.city = _.isNull($stateParams.city) ? '' : $stateParams.city;
        vm.state = _.isNull($stateParams.state) ? '' : $stateParams.state;
        vm.globalQueryFilterObject = new JobMatchesQueryFilter();
        if(!_.isNull($stateParams.visibilityFiltersObject)){
            vm.globalQueryFilterObject.isUnassigned = (!_.isNull($stateParams.visibilityFiltersObject.isUnassigned)) && ($stateParams.visibilityFiltersObject.isUnassigned);
            vm.globalQueryFilterObject.isAssignedToOthers = (!_.isNull($stateParams.visibilityFiltersObject.isAssignedToOthers)) && ($stateParams.visibilityFiltersObject.isAssignedToOthers);
            vm.globalQueryFilterObject.isAssignedToMe = (!_.isNull($stateParams.visibilityFiltersObject.isAssignedToMe)) && ($stateParams.visibilityFiltersObject.isAssignedToMe);
        }else{
            vm.globalQueryFilterObject.isUnassigned = true;
            vm.globalQueryFilterObject.isAssignedToOthers = true;
            vm.globalQueryFilterObject.isAssignedToMe = true;
        }
        vm.getAllJobMatchStates();
        vm.getPlanDetails();
        if(vm.jobId != ''){
            vm.getEnabledWorkflowSteps(function(){
                _createCandidatesObject();
            });
        }else{
            _createCandidatesObject();
        }
        _createSlimScrollForColumns();
        // whenever the window re-sizes, re calculate and reset the column container height
        $(window).bind('resize', _setCandidateStateWrapperColumnContainerHeight);
    }

    function getPlanDetails(){
        planService.getCompanyPlanDetails(vm.companyId, function(data){
            if(!_.isNull(data.features) && data.features.length){
                var jobIntegrationDetails = _.find(data.features, { 'name': "Job Board Integration", 'available': true });
                vm.jobIntegrationEnabled = _.isUndefined(jobIntegrationDetails) ? false : true;
            }
        }, function(error){
            alertsAndNotificationsService.showBannerMessage(error.message,'danger');
        });
    }

    function _createSlimScrollForColumns(){
        // check if the state wrapper column has rendered. If rendered, add slim scroll. If not, wait for 300 ms and try again.
        if($('.candidate-state-wrapper-column').length){
            $('.candidate-state-wrapper-column').slimScroll({
                start: 'top',
                alwaysVisible: false,
                railVisible: false,
                wheelStep: 20,
                allowPageScroll: true,
                height: 'auto'
            });
        }else{
            $timeout(function () {
                _createSlimScrollForColumns();
            }, 300);
        }
    }

    function _setCandidateStateWrapperColumnContainerHeight() {
        var windowHeight = $(window).height();
        var containerHeight = windowHeight - 280;
        containerHeight = parseInt(containerHeight) + 'px';
        $(".candidate-state-wrapper-column-container").css('height',containerHeight);
        // set slim scrolling for the state columns
        _createSlimScrollForColumns();
    }


    function _createCandidatesObject() {
        vm.candidatesObject = {};
        if(vm.jobId != ''){
            angular.forEach(vm.enabledWorkflowSteps, function (val, key) {
                var tempVal = val;
                var infoVariableStringArray = tempVal.split(" ");
                var infoVariableString = '';
                var identifierString = '';
                if(infoVariableStringArray.length > 1){
                    infoVariableString = infoVariableStringArray[0].toLowerCase() + infoVariableStringArray[1] + 'Info';
                    identifierString = infoVariableStringArray[0].toLowerCase() + '-' + infoVariableStringArray[1].toLowerCase();
                }else{
                    infoVariableString = infoVariableStringArray[0].toLowerCase() + 'Info';
                    identifierString = infoVariableStringArray[0].toLowerCase();
                }
                vm.candidatesObject[infoVariableString] = new candidatesContainer(val,infoVariableString,identifierString);
            });
        }else{
            vm.candidatesObject = {
                recruiterScreeningInfo: new candidatesContainer('Recruiter Screening','recruiterScreeningInfo','recruiter-screening'),
                //qualifiedInfo:  new candidatesContainer('Qualified','qualifiedInfo','soft-qualified'),
                techAssessmentInfo:  new candidatesContainer('Tech Assessment','techAssessmentInfo','tech-assessment'),
                valueAssessmentInfo:  new candidatesContainer('Value Assessment','valueAssessmentInfo','value-assessment'),
                verificationInfo:  new candidatesContainer('Verification','verificationInfo','verification'),
                phoneScreenInfo:  new candidatesContainer('Phone Screen','phoneScreenInfo','phone-screen'),
                goldStandardInfo:  new candidatesContainer('Interview','interviewInfo','interview'),
                //likelyInfo:  new candidatesContainer('Likely','likelyInfo','soft-qualified'),
                notInterestedInfo:  new candidatesContainer('Not Interested','notInterestedInfo','not-interested'),
                releasedInfo:  new candidatesContainer('Released','releasedInfo','released')
            };
        }

    }

    function getAllJobMatchStates(){
        jobMatchService.getAllJobMatchStates(function(data){
            angular.forEach(data, function(val, key){
               val.name = val.value;
            });
            vm.jobMatchStates = data;
        }, function (error) {
            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
        });
    }

    function resetCandidateJobMatches() {
        _createCandidatesObject();
        angular.forEach(vm.candidatesObject, function (val, key) {
            var tempWorkflowSteps = val.jobMatchesQueryFilter.workflowSteps;
            val.jobMatchesQueryFilter = angular.copy(vm.globalQueryFilterObject);
            val.jobMatchesQueryFilter.workflowSteps = tempWorkflowSteps;
            val.getNext();
        });
    }

    function queryJobMatches(){
        _createCandidatesObject();
        angular.forEach(vm.candidatesObject, function (val, key) {
            var tempWorkflowSteps = val.jobMatchesQueryFilter.workflowSteps;
            val.jobMatchesQueryFilter = angular.copy(vm.globalQueryFilterObject);
            val.jobMatchesQueryFilter.workflowSteps = tempWorkflowSteps;
            val.getNext();
        });
    }

    function resetWorkflowFilters() {
        angular.forEach(vm.jobMatchStates, function (val, key) {
            val.ticked = false;
        });
        $timeout(function(){
            vm.globalQueryFilterObject = new JobMatchesQueryFilter();
            vm.queryJobMatches();
        }, 100);

    }

    function getEnabledWorkflowSteps(successCallback){
        jobService.getEnabledWorkflowSteps(vm.jobId, function (data) {
            vm.enabledWorkflowSteps = data;
            if(successCallback){
                successCallback();
            }
        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }

    function performActionMultiple(action) {
        var selectedCandidateJobMatches = [];
        angular.forEach(vm.candidatesObject, function (val, key) {
           angular.forEach(val.candidates, function (candidate, k) {
                if(candidate.selectedFlag){
                    selectedCandidateJobMatches.push(candidate);
                }
           });
        });
        if(action == 'compareCandidates' && selectedCandidateJobMatches.length < 2){
            bootbox.alert({
                closeButton: false,
                title: "<div class='alert alert-warning' style='margin-bottom: 0px;'><i class='fa fa-warning fa-fw fa-lg'></i><strong>Select Cards!</strong></div>",
                message: "Please select at least two cards to compare.",
                className: "zIndex1060",
                buttons: {
                    ok: {
                        label: 'Ok',
                        className: 'btn-info'
                    }
                }
            });
        }else if(action != 'addCandidates' && action != 'associateCandidates' && action != 'compareCandidates' && action != 'addCandidatesFromJobBoards' && selectedCandidateJobMatches.length == 0) {
            bootbox.alert({
                closeButton: false,
                title: "<div class='alert alert-warning' style='margin-bottom: 0px;'><i class='fa fa-warning fa-fw fa-lg'></i><strong>No Cards Selected!</strong></div>",
                message: "Please select at least one card to perform this action.",
                className: "zIndex1060",
                buttons: {
                    ok: {
                        label: 'Ok',
                        className: 'btn-info'
                    }
                }
            });
        }else{
            var statusType = '';
            var windowTopClass = '';
            switch(action){
                case 'release':
                    statusType = 'Release';
                    break;
                case 'notInterested':
                    statusType = 'Not Interested';
                    break;
                case 'addCandidates':
                    windowTopClass = 'job-match-workflow-multi-action-modal-large';
                    break;
                case 'addCandidatesFromJobBoards':
                    break;
                case 'associateCandidates':
                    windowTopClass = 'job-match-workflow-multi-action-modal-large';
                    break;
            }
            $uibModal.open({
                animation: false,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/partials/missioncontrol/candidateJobMatchesWorkflow/job-matches-multiple-action-modal.html',
                backdrop: 'static',
                controller: ['$uibModalInstance','action','jobId','jobMatchDetailsArray','statusType','saveCallback',function ($uibModalInstance, action, jobId, jobMatchDetailsArray, statusType, saveCallback) {
                    var vm = this;
                    vm.action = action;
                    vm.jobId = jobId;
                    vm.jobMatchDetailsArray = jobMatchDetailsArray;
                    vm.jobMatchIdArray = [];
                    angular.forEach(vm.jobMatchDetailsArray, function (val, key) {
                        vm.jobMatchIdArray.push(val.jobMatchId);
                    });
                    vm.statusType = statusType;
                    vm.modalHeading = '';
                    if(vm.action == 'addCandidates'){
                        vm.modalHeading = 'Add Candidates';
                    }else if(vm.action == 'addCandidatesFromJobBoards'){
                        vm.modalHeading = 'Get Candidates From Job Boards';
                    }else if(vm.action == 'associateCandidates'){
                        vm.modalHeading = 'Associate Candidates';
                    }else if(vm.action == 'release'){
                        vm.modalHeading = 'Release';
                    }else if(vm.action == 'notInterested'){
                        vm.modalHeading = 'Not Interested';
                    }else if(vm.action == 'assignRecruiter'){
                        vm.modalHeading = 'Assign Recruiter';
                    }
                    vm.saveCallback = function(){
                        $uibModalInstance.dismiss('cancel');
                        saveCallback();
                    };
                    vm.closeModal = function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                }],
                controllerAs: 'multiActionModal',
                size: 'lg',
                windowTopClass: windowTopClass,
                resolve:{
                    jobMatchDetailsArray: function () {
                        var tempArray = [];
                        angular.forEach(selectedCandidateJobMatches, function (val, key) {
                            var tempObject = {};
                            tempObject.jobMatchId = val.jobMatchId;
                            tempObject.candidateId = val.candidateId;
                            tempArray.push(tempObject);
                        });
                        return tempArray;
                    },
                    statusType: function () {
                        return statusType;
                    },
                    saveCallback: function () {
                        return vm.resetCandidateJobMatches;
                    },
                    jobId: function () {
                        return vm.jobId;
                    },
                    action: function () {
                        return action;
                    }
                }
            });
        }

    }

    function toggleColumn(workflowStep) {
        if(workflowStep.isCollapsedFlag){
            workflowStep.isCollapsedFlag = false;
            $timeout(function(){
                workflowStep.showCollapseIconFlag = true;
                workflowStep.expandCollapseLabel = 'Collapse Column';
                workflowStep.countAndLabelTooltipPlacement = 'bottom-right';
            }, 600);
        }else{
            workflowStep.isCollapsedFlag = true;
            $timeout(function(){
                workflowStep.showCollapseIconFlag = false;
                workflowStep.expandCollapseLabel = 'Expand Column';
                workflowStep.countAndLabelTooltipPlacement = 'right';
            }, 600);
        }
    }

    function candidatesContainer(workflowStep, workflowStepVariable, uiIdentifier){
        this.candidates = [];
        this.candidatesExistFlag = true;
        this.busy = false;
        this.disableInfiniteScroll = false;
        this.jobMatchesQueryFilter = new JobMatchesQueryFilter(workflowStep);
        if(!_.isNull($stateParams.visibilityFiltersObject)){
            this.jobMatchesQueryFilter.isUnassigned = (!_.isNull($stateParams.visibilityFiltersObject.isUnassigned)) && ($stateParams.visibilityFiltersObject.isUnassigned);
            this.jobMatchesQueryFilter.isAssignedToOthers = (!_.isNull($stateParams.visibilityFiltersObject.isAssignedToOthers)) && ($stateParams.visibilityFiltersObject.isAssignedToOthers);
            this.jobMatchesQueryFilter.isAssignedToMe = (!_.isNull($stateParams.visibilityFiltersObject.isAssignedToMe)) && ($stateParams.visibilityFiltersObject.isAssignedToMe);
        }else{
            this.jobMatchesQueryFilter.isUnassigned = true;
            this.jobMatchesQueryFilter.isAssignedToOthers = true;
            this.jobMatchesQueryFilter.isAssignedToMe = true;
        }
        this.currentPromise = null;
        this.workflowStep = workflowStep;
        this.workflowStepVariable = workflowStepVariable;
        this.uiIdentifier = uiIdentifier;
        this.isCollapsedFlag = false;
        this.showCollapseIconFlag = true;
        this.expandCollapseLabel = 'Collapse Column';
        this.countAndLabelTooltipPlacement = 'bottom-right';
        this.totalCandidateCount = '..';
        this.pageNum = 1;
        this.pageSize = 3;
        this.jobId = vm.jobId;

        this.getNext = function () {
            var parent = this;
            if(this.busy) return;
            this.busy = true;

            //console.log('filter object in get next -', this.jobMatchesQueryFilter);
            this.currentPromise = jobMatchService.getJobMatchCandidates(vm.userId,vm.companyId,this.pageNum,this.pageSize,this.jobId,this.jobMatchesQueryFilter, function (data) {
                if(angular.isDefined(data) && data != 'user cancellation'){
                    parent.totalCandidateCount = data.totalCardCount;
                    var candidatesArray = JSON.parse(data[parent.workflowStepVariable]);
                    if(candidatesArray.length > 0){
                        angular.forEach(candidatesArray, function (val, key) {
                            val.id = val.candidateId;
                            val.selectedFlag = false;
                            val.disabledFlag = false;
                            parent.candidates.push(angular.merge(new Candidate(), val));
                        });
                        // if the length of the returned values is less than page size, it means there are not enough values.
                        // hence we can disable the infinite scroll
                        if(candidatesArray.length < parent.pageSize){
                            parent.disableInfiniteScroll = true;
                        }else{
                            parent.pageNum++;
                        }
                    }else{
                        // if the length of the returned values is 0, it means there are no more values.
                        // hence we can disable the infinite scroll
                        parent.disableInfiniteScroll = true;
                    }
                }
                //set parent.busy to false
                parent.busy = false;
                // set current promise to null
                parent.currentPromise = null;
                // set the candidates exist flag based on existence of candidates.
                if(parent.candidates.length > 0){
                    parent.candidatesExistFlag = true;
                }else{
                    parent.candidatesExistFlag = false;
                }
                vm.getAllActivityTypes();
            }, function (error) {
                if ($rootScope.isOnline) {
                    alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                }
                parent.busy = false;
                // if there was an error in the get call, we need to disable infinite scroll. Else it will continue indefinitely.
                parent.disableInfiniteScroll = true;
                // set current promise to null
                parent.currentPromise = null;
            });
        }
    }

    function getAllActivityTypes(){
        jobService.getActivityTypes(function (data) {
            vm.activityMap = [];
            angular.forEach(data, function (val, key) {
                vm.activityMap[val.value] = val.name;
            });
        }, function (error) {
            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
        });
    }

    function onFilterItemClick(selectedItems, label) {
        switch (label){
            case 'JobMatchStates':
                // set the selected job match states on the global query filter object
                vm.globalQueryFilterObject.workflowStates = vm.selectedJobMatchStates.map(function (val, index) {
                    return val.value;
                });
                break;
            default:
            //do nothing;
        }
        // trigger a new query
        vm.queryJobMatches();
    }

    function filterSelectRemoveAll(label, type){
        if(type == 'selectAll'){
            switch (label){
                case 'JobMatchStates':
                    // set the selected job match states on the global query filter object
                    vm.globalQueryFilterObject.workflowStates = vm.jobMatchStates.map(function (val, index) {
                        return val.value;
                    });
                    break;
                default:
                //do nothing;
            }
        }else{
            switch (label){
                case 'JobMatchStates':
                    // set the selected job match states to empty on the global query filter object
                    vm.globalQueryFilterObject.workflowStates = [];
                    break;
                default:
                //do nothing
            }
        }
        // trigger a new query
        vm.queryJobMatches();
    }

    $(document).ready(function () {
        $("#workflow-search-panel").hide();
        $(document).off("click", "#open-filter").on("click", "#open-filter",
            function () {
                $("#workflow-search-panel").animate({
                    height: 'toggle'
                });
            });
    });

    vm.init();
}