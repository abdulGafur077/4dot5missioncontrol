var candidatecomparemodule = angular.module('4dot5.missioncontrolmodule.candidatecomparemodule', []);

candidatecomparemodule.constant('CANDIDATECOMPARECONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.candidatecompare',
        URL: '/candidatecompare',
        CONTROLLER: 'CandidatecompareController',
        TEMPLATEURL: 'app/partials/missioncontrol/candidatecompare/candidatecompare.html',
    },
    CONTROLLER: {

    }
});
candidatecomparemodule.config(
    ['$stateProvider',
        'CANDIDATECOMPARECONSTANTS',
        function ($stateProvider, CANDIDATECOMPARECONSTANTS) {
            $stateProvider.state(CANDIDATECOMPARECONSTANTS.CONFIG.STATE, {
                url: CANDIDATECOMPARECONSTANTS.CONFIG.URL,
                templateUrl: CANDIDATECOMPARECONSTANTS.CONFIG.TEMPLATEURL,
                controller: CANDIDATECOMPARECONSTANTS.CONFIG.CONTROLLER,
                data: {
                    requireLogin: true
                },
                params: {
                    candidatesType: null,
                    teamMemberName: null,
                    jobType: null,
                    companyName: null,
                    candidate1: null,
                    candidate2: null,
                    candidate3: null,
                    breadcrumb: null

                }
            });
        }
    ]);

candidatecomparemodule.controller('CandidatecompareController', ['$scope',
    '$http',
    '$state',
    '$timeout',
    '$rootScope',
    '$stateParams',
    'genericService',
    'StorageService',
    'CANDIDATECOMPARECONSTANTS',
    function ($scope, $http, $state, $timeout, $rootScope, $stateParams, genericService, StorageService, CANDIDATECOMPARECONSTANTS) {
        console.log('CANDIDATECOMPARECONSTANTS');



        $scope.jobType = '';
        $scope.jobData = [];
        $scope.jobDetail = [];
        $scope.assetList = [];
        $scope.candidatesData = [{}, {}, {}];
        $scope.dummyJob = {
            "JobType": "",
            "CompanyName": "",
            "AssetType": "",
            "AssetName": "",
            "AssetDetails": "",
            "AssetValueRequirement": "",
            "RequiredOptionalNA": ""
        };
        $('.candidate-pricing-package').mouseover(
            function () {
                $('.clrChange').addClass('change')
            });
        $('.candidate-pricing-package').mouseleave(
            function () {
                $('.clrChange').removeClass('change')
            });
        $scope.init = function () {

            $scope.breadcrumb = $stateParams.breadcrumb;
            $scope.jobType = $stateParams.jobType;
            $scope.companyName = $stateParams.companyName;
            $scope.candidatesData[0].candidateName = $stateParams.candidate1;
            $scope.candidatesData[1].candidateName = $stateParams.candidate2;
            $scope.candidatesData[2].candidateName = $stateParams.candidate3;
            $scope.candidatesData[0].candidateAssets = [];
            $scope.candidatesData[1].candidateAssets = [];
            $scope.candidatesData[2].candidateAssets = [];
            $scope.show = false;
            $scope.expand = function (detail) {
                detail.show = !detail.show;
            }

            $scope.breadCrumb = function () {
                console.log('entering the function');
                if ($scope.breadcrumb === 'Active Candidates') {
                    console.log('Active Candiadtes routing');
                    $state.go('missioncontrol.activecandidatesprofile', { candidatesType: 'active' });
                } else {
                    console.log('passive Candiadtes routing');
                    $state.go('missioncontrol.passivecandidates', { candidatesType: 'passive' });
                }
            }

            $http.get("data/jobDetail.json")
                .success(function (data) {
                    $scope.jobDetail = data;

                });
            $http.get("data/candidateDetail.json")
                .success(function (data) {
                    var candidateData;
                    $scope.selectedCandidatesDetail = [];
                    $scope.selectedCandidatesDetailAsset = [];
                    angular.forEach(data, function (val, key) {
                        for (i = 0; i < $scope.candidatesData.length; i++) {
                            if (val.candidateName === $scope.candidatesData[i].candidateName) {
                                $scope.selectedCandidatesDetail[i] = val;
                                $scope.selectedCandidatesDetailAsset[i] = $scope.selectedCandidatesDetail[i].Asset;
                            }
                        }
                    });
                    angular.forEach(data, function (selectedCandidate, key1) {
                        for (i = 0; i < selectedCandidate.Asset.length; i++) {
                            if (selectedCandidate.Asset[i].AssetType === $scope.jobDetail[0].Asset[i].AssetType) {
                                if (selectedCandidate.Asset[i].AssetType === 'Certifications' || selectedCandidate.Asset[i].AssetType === 'Other') {
                                    for (j = 0; j < selectedCandidate.Asset[i].AssetTypeDetails.length; j++) {
                                        selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Additional';
                                    }
                                } else {
                                    for (j = 0; j < selectedCandidate.Asset[i].AssetTypeDetails.length; j++) {
                                        if (selectedCandidate.Asset[i].AssetTypeDetails[j].AssetName == $scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetName) {
                                            if ($scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetDetails === "") {
                                                if ($scope.jobDetail[0].Asset[i].AssetTypeDetails[j].RequiredOptionalNA === 'Required') {
                                                    if ($scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetValueRequirement != "") {
                                                        if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateExp === "") {
                                                            if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData === 'None' || selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData === "") {
                                                                selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                            } else {
                                                                if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData === $scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetValueRequirement) {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Match';
                                                                } else {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                                }
                                                            }
                                                        } else {
                                                            if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateExp === $scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetValueRequirement) {
                                                                selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Match';
                                                            } else if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateExp > ($scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetValueRequirement) / 2) {
                                                                selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Almost';
                                                            } else {
                                                                selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                            }
                                                        }
                                                    } else {
                                                        if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateExp === "") {
                                                            if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData === 'None' || selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData === "") {
                                                                selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                            } else {
                                                                var mainStr = selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData;
                                                                var subStr = $scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetDetails;
                                                                if (mainStr.indexOf(subStr) == -1) {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                                } else {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Match';
                                                                }
                                                            }
                                                        } else {
                                                            selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Match';
                                                        }
                                                    }
                                                } else if ($scope.jobDetail[0].Asset[i].AssetTypeDetails[j].RequiredOptionalNA === 'Optional') {
                                                    if ($scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetValueRequirement != "") {
                                                        if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateExp === "") {
                                                            if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData === 'None' || selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData === "") {
                                                                selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                            } else {
                                                                if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData === $scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetValueRequirement) {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Match';
                                                                } else {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                                }
                                                            }
                                                        } else {
                                                            if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateExp === $scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetValueRequirement) {
                                                                selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Match';
                                                            } else if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateExp > ($scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetValueRequirement) / 2) {
                                                                selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Almost';
                                                            } else {
                                                                selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                            }
                                                        }

                                                    } else {
                                                        if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateExp === "") {
                                                            if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData === 'None' || selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData === "") {
                                                                selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                            } else {
                                                                var mainStr = selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData;
                                                                var subStr = $scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetDetails;
                                                                if (mainStr.indexOf(subStr) == -1) {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                                } else {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Match';
                                                                }
                                                            }
                                                        } else {
                                                            selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Match';
                                                        }
                                                    }
                                                } else if ($scope.jobDetail[0].Asset[i].AssetTypeDetails[j].RequiredOptionalNA === 'NA') {
                                                    if ($scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetValueRequirement != "") {
                                                        if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateExp === "") {
                                                            if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData === 'None' || selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData === "") {
                                                                selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                            } else {
                                                                if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData === $scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetValueRequirement) {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Match';
                                                                } else {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                                }
                                                            }
                                                        } else {
                                                            if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateExp === $scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetValueRequirement) {
                                                                selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Match';
                                                            } else if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateExp > ($scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetValueRequirement) / 2) {
                                                                selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Almost';
                                                            } else {
                                                                selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Additional';
                                                            }
                                                        }
                                                    } else {
                                                        if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateExp === "") {
                                                            if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData === 'None' || selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData === "") {
                                                                selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Match';
                                                            } else {
                                                                selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                            }
                                                        } else {
                                                            selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Additional';
                                                        }
                                                    }
                                                }
                                            } else {
                                                if ($scope.jobDetail[0].Asset[i].AssetTypeDetails[j].RequiredOptionalNA === 'Required') {
                                                    if ($scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetValueRequirement != "") {
                                                        if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateExp === "") {
                                                            if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData === 'None' || selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData === "") {
                                                                selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                            } else {
                                                                if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData === $scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetValueRequirement) {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Match';
                                                                } else {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                                }
                                                            }
                                                        } else {
                                                            var jobExp = $scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetValueRequirement;
                                                            var candExp = selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateExp;
                                                            if ($scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetDetails === "Minimum" || $scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetDetails === "Atleast") {
                                                                if (candExp >= jobExp) {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Match';
                                                                } else if (candExp > (jobExp / 2)) {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Almost';
                                                                } else {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                                }
                                                            } else if ($scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetDetails == "Maximum") {
                                                                if (candExp <= jobExp) {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Match';
                                                                } else if (candExp < (jobExp / 2)) {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Almost';
                                                                } else {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                                }
                                                            } else {
                                                                if (candExp === jobExp) {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Match';
                                                                } else {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                                }
                                                            }
                                                        }
                                                    } else {
                                                        if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateExp === "") {
                                                            if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData === 'None' || selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData === "") {
                                                                selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                            } else {
                                                                var mainStr = selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData;
                                                                var subStr = $scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetDetails;
                                                                if (mainStr.indexOf(subStr) == -1) {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                                } else {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Match';
                                                                }
                                                            }
                                                        } else {
                                                            if ($scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetDetails === selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateExp) {
                                                                selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Match';
                                                            } else {
                                                                selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                            }
                                                        }
                                                    }
                                                } else if ($scope.jobDetail[0].Asset[i].AssetTypeDetails[j].RequiredOptionalNA === 'Optional') {
                                                    if ($scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetValueRequirement != "") {
                                                        if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateExp === "") {
                                                            if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData === 'None' || selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData === "") {
                                                                selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                            } else {
                                                                if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData === $scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetValueRequirement) {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Match';
                                                                } else {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                                }
                                                            }
                                                        } else {
                                                            var jobExp = $scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetValueRequirement;
                                                            var candExp = selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateExp;
                                                            if ($scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetDetails === "Minimum" || $scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetDetails === "Atleast") {
                                                                if (candExp >= jobExp) {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Match';
                                                                } else if (candExp > (jobExp / 2)) {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Almost';
                                                                } else {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                                }
                                                            } else if ($scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetDetails == "Maximum") {
                                                                if (candExp <= jobExp) {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Match';
                                                                } else if (candExp < (jobExp / 2)) {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Almost';
                                                                } else {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                                }
                                                            } else {
                                                                if (candExp === jobExp) {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Match';
                                                                } else {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                                }
                                                            }
                                                        }
                                                    } else {
                                                        if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateExp === "") {
                                                            if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData === 'None' || selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData === "") {
                                                                selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                            } else {
                                                                var mainStr = selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData;
                                                                var subStr = $scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetDetails;
                                                                if (mainStr.indexOf(subStr) == -1) {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                                } else {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Match';
                                                                }
                                                            }
                                                        } else {
                                                            if ($scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetDetails === selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateExp) {
                                                                selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Match';
                                                            } else {
                                                                selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                            }
                                                        }
                                                    }
                                                } else if ($scope.jobDetail[0].Asset[i].AssetTypeDetails[j].RequiredOptionalNA === 'NA') {
                                                    if ($scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetValueRequirement != "") {
                                                        if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateExp === "") {
                                                            if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData === 'None' || selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData === "") {
                                                                selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                            } else {
                                                                if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData === $scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetValueRequirement) {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Match';
                                                                } else {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                                }
                                                            }
                                                        } else {
                                                            var jobExp = $scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetValueRequirement;
                                                            var candExp = selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateExp;
                                                            if ($scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetDetails === "Minimum" || $scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetDetails === "Atleast") {
                                                                if (candExp >= jobExp) {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Match';
                                                                } else if (candExp > (jobExp / 2)) {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Almost';
                                                                } else {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Additional';
                                                                }
                                                            } else if ($scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetDetails == "Maximum") {
                                                                if (candExp <= jobExp) {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Match';
                                                                } else if (candExp < (jobExp / 2)) {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Almost';
                                                                } else {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Additional';
                                                                }
                                                            } else {
                                                                if (candExp === jobExp) {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Match';
                                                                } else {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                                }
                                                            }
                                                        }
                                                    } else {
                                                        if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateExp === "") {
                                                            if (selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData === 'None' || selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData === "") {
                                                                selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                            } else {
                                                                var mainStr = selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateData;
                                                                var subStr = $scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetDetails;
                                                                if (mainStr.indexOf(subStr) == -1) {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Match';
                                                                } else {
                                                                    selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                                }
                                                            }
                                                        } else {
                                                            if ($scope.jobDetail[0].Asset[i].AssetTypeDetails[j].AssetDetails === selectedCandidate.Asset[i].AssetTypeDetails[j].CandidateExp) {
                                                                selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Match';
                                                            } else {
                                                                selectedCandidate.Asset[i].AssetTypeDetails[j].CandidatematchType = 'Missing';
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        console.log("Matched inside selectedCandidate Finalllll  " + key1 + "  " + angular.toJson(selectedCandidate));
                    });
                });
        };

        $scope.init();

    }

]);