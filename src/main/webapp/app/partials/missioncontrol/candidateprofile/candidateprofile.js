/**
 * This module deals with candidate profile
 */

var candidateprofilemodule = angular.module('4dot5.missioncontrolmodule.candidateprofilemodule', []);

candidateprofilemodule.constant('CANDIDATEPROFILECONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.candidateprofile',
        URL: '/candidateProfile',
        CONTROLLER: 'CandidateProfileViewController',
        CONTROLLERAS: 'candidateProfileView',
        TEMPLATEURL: 'app/partials/missioncontrol/candidateprofile/candidateprofile.html'
    }
});

candidateprofilemodule.config(
    ['$stateProvider',
        'CANDIDATEPROFILECONSTANTS',
        function($stateProvider, CANDIDATEPROFILECONSTANTS) {
            $stateProvider.state(CANDIDATEPROFILECONSTANTS.CONFIG.STATE, {
                url: CANDIDATEPROFILECONSTANTS.CONFIG.URL,
                templateUrl: CANDIDATEPROFILECONSTANTS.CONFIG.TEMPLATEURL,
                controller: CANDIDATEPROFILECONSTANTS.CONFIG.CONTROLLER,
                controllerAs: CANDIDATEPROFILECONSTANTS.CONFIG.CONTROLLERAS,
                data: {
                    requireLogin: true
                },
                params: {
                    candidateId: null
                }
            });
        }
    ]);

candidateprofilemodule.controller('CandidateProfileViewController',CandidateProfileViewController);

candidateprofilemodule.$inject = ['$stateParams'];

function CandidateProfileViewController($stateParams) {
    var vm = this;
    vm.candidateId = $stateParams.candidateId;
}