/**
 * This module deals with Candidates
 */

var candidatemodule = angular.module('4dot5.missioncontrolmodule.candidatemodule', []);

candidatemodule.constant('CANDIDATESCONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.candidates',
        URL: '/candidates',
        CONTROLLER: 'CandidatesController',
        CONTROLLERAS: 'candidates',
        TEMPLATEURL: 'app/partials/missioncontrol/candidates/candidates.html'
    }
});
candidatemodule.config(['$stateProvider', 'CANDIDATESCONSTANTS',
        function ($stateProvider, CANDIDATESCONSTANTS) {
            $stateProvider.state(CANDIDATESCONSTANTS.CONFIG.STATE, {
                url: CANDIDATESCONSTANTS.CONFIG.URL,
                templateUrl: CANDIDATESCONSTANTS.CONFIG.TEMPLATEURL,
                controller: CANDIDATESCONSTANTS.CONFIG.CONTROLLER,
                controllerAs: CANDIDATESCONSTANTS.CONFIG.CONTROLLERAS,
                data: {
                    requireLogin: true
                },
                params: {
                    candidateStatuses: null,
                    recruiters: null,
                    visibilityFilters: null
                }
            });
        }
    ]);

candidatemodule.controller('CandidatesController', CandidatesController);

candidatemodule.$inject = ['$scope', '$rootScope', '$stateParams', '$timeout','genericService', 'jobService', 'candidateService', 'Candidate', 'CandidateQueryFilter', 'alertsAndNotificationsService'];

function CandidatesController($scope, $rootScope, $stateParams, $timeout, genericService, jobService, candidateService, Candidate, CandidatesQueryFilter, alertsAndNotificationsService) {
        var vm = this;
        //variables
        vm.candidates = [];
        vm.candidatesContainer = null;
        vm.userId = null;
        vm.infoOfCandidateInContext = null;
        vm.companyType = null;
        vm.clientOrBuList = null;
        vm.candidateStatusList = null;
        vm.requisitionStatusList = null;
        vm.recruitersList = null;
        vm.requisitionNumbersList = null;
        vm.candidateInfoTemplate = '';
        vm.candidateInfoContainerId = null;
        vm.selectedDateRange = '';
        vm.activityMap = [];
        vm.visibilityFilters = {
            unassigned: '',
            assignedToMe: '',
            assignedToOthers: ''
        };
        vm.sortByFilters = [
            {
                "column": "clientOrBuList",
                "columnDisplayName": "Client"
            },
            {
                "column": "status",
                "columnDisplayName": "Candidate Status"
            },
            {
                "column": "requisitionStatus",
                "columnDisplayName": "Requisition Status"
            },
            {
                "column": "requisitionNumbers",
                "columnDisplayName": "Requisition Number"
            },
            {
                "column": "firstName",
                "columnDisplayName": "First Name"
            },
            {
                "column": "lastName",
                "columnDisplayName": "Last Name"
            },
            {
                "column": "phone",
                "columnDisplayName": "Phone"
            },
            {
                "column": "email",
                "columnDisplayName": "Email"
            },
            {
                "column": "recruiterList",
                "columnDisplayName": "Recruiters"
            }
        ];
        vm.selectedSort = 'Please Select';
        vm.selectedSortDirection = '';
        // selected lists
        vm.selectedClientOrBuList = [];
        vm.selectedRequisitionStatusList = [];
        vm.selectedRecruitersList = [];
        vm.selectedRequisitionNumbersList = [];
        vm.selectedCandidateStatusList = ["Active","Passive","Inert"];
        //flags
        vm.hideSearchStringFlag = false;
        vm.candidatesExistFlag = true;
        vm.showCandidateInfo = false;
        //controller methods
        vm.init = init;
        vm.queryCandidates = queryCandidates;
        vm.getAllClientsOrBu = getAllClientsOrBu;
        vm.getAllCandidateStatuses = getAllCandidateStatuses;
        vm.getAllRequisitionStatuses = getAllRequisitionStatuses;
        vm.getAllRecruiters = getAllRecruiters;
        vm.getAllRequisitionNumbers = getAllRequisitionNumbers;
        vm.getAllActivityTypes = getAllActivityTypes;
        vm.selectRemoveAll = selectRemoveAll;
        vm.visibilityFilterChanged = visibilityFilterChanged;
        vm.onClientClick = onClientClick;
        vm.onCandidateStatusClick = onCandidateStatusClick;
        vm.onRequisitionStatusClick = onRequisitionStatusClick;
        vm.onRecruiterClick = onRecruiterClick;
        vm.onRequisitionNumbersClick = onRequisitionNumbersClick;
        vm.sortCandidates = sortCandidates;
        vm.setCandidateInfo = setCandidateInfo;
        vm.closeCandidateInfoRow = closeCandidateInfoRow;
        vm.filterByDate = filterByDate;
        vm.resetCandidateQueryFilters = resetCandidateQueryFilters;

        vm.init();

        function init(){
            vm.candidates = [];
            vm.candidatesContainer = new candidatesContainer();
            vm.userId = $rootScope.userDetails.id;
            vm.companyType = $rootScope.userDetails.company.companyType;
            if(!_.isNull($stateParams.visibilityFilters)){
                vm.visibilityFilters = $stateParams.visibilityFilters;
                if(vm.visibilityFilters.unassigned != ''){
                    vm.candidatesContainer.candidatesQueryFilter.visibility.push('unassigned');
                }
                if(vm.visibilityFilters.assignedToMe != ''){
                    vm.candidatesContainer.candidatesQueryFilter.visibility.push('assignedToMe');
                }
                if(vm.visibilityFilters.assignedToOthers != ''){
                    vm.candidatesContainer.candidatesQueryFilter.visibility.push('assignedToOthers');
                }
            }else{
                vm.candidatesContainer.candidatesQueryFilter.visibility.push('unassigned');
                vm.visibilityFilters.unassigned = 'unassigned';
                vm.candidatesContainer.candidatesQueryFilter.visibility.push('assignedToOthers');
                vm.visibilityFilters.assignedToOthers = 'assignedToOthers';
                vm.candidatesContainer.candidatesQueryFilter.visibility.push('assignedToMe');
                vm.visibilityFilters.assignedToMe = 'assignedToMe';
            }
            // get all the filter data
            vm.getAllClientsOrBu();
            vm.getAllActivityTypes();
            vm.getAllCandidateStatuses(function () {
                // check if any state params are present and add them onto the filters
                if(!_.isNull($stateParams.candidateStatuses) && _.isArray($stateParams.candidateStatuses)){
                    vm.candidatesContainer.candidatesQueryFilter.status = $stateParams.candidateStatuses;
                    _setFilterValues('CandidateStatus',$stateParams.candidateStatuses);
                }
            });
            vm.getAllRequisitionStatuses();
            vm.getAllRecruiters(function () {
                // check if any state params are present and add them onto the filters
                if(!_.isNull($stateParams.recruiters) && _.isArray($stateParams.recruiters)){
                    vm.candidatesContainer.candidatesQueryFilter.recruiterList = $stateParams.recruiters;
                    _setFilterValues('Recruiters',$stateParams.recruiters);
                }
            });
            vm.getAllRequisitionNumbers();
        }

        function resetCandidateQueryFilters() {
            vm.selectedDateRange = '';
            angular.forEach(vm.candidateStatusList, function (val, key) {
                val.ticked = false;
            });
            angular.forEach(vm.recruitersList, function (val, key) {
                val.ticked = false;
            });
            angular.forEach(vm.requisitionNumbersList, function (val, key) {
                val.ticked = false;
            });
            angular.forEach(vm.requisitionStatusList, function (val, key) {
                val.ticked = false;
            });
            angular.forEach(vm.clientOrBuList, function (val, key) {
                val.ticked = false;
            });
            if(vm.selectedSort != "Please Select"){
                vm.selectedSort = "Please Select";
                vm.selectedSortDirection = "";
                delete(vm.candidatesContainer.candidatesQueryFilter.column);
                delete(vm.candidatesContainer.candidatesQueryFilter.sortDirection);
            }
            vm.candidatesContainer.candidatesQueryFilter = new CandidatesQueryFilter($rootScope.userDetails.company.companyId, $rootScope.userDetails.id);
            //to set existing visibility filters
            visibilityFilterChanged();
            vm.queryCandidates();
        }

        function _setFilterValues(label, arrayOfFilterValues){
            switch(label){
                case 'CandidateStatus':
                    angular.forEach(vm.candidateStatusList, function (candidateStatus, key) {
                       if(arrayOfFilterValues.indexOf(candidateStatus.value) != -1){
                           candidateStatus.ticked = true;
                       }
                    });
                    break;
                case 'Recruiters':
                    angular.forEach(vm.recruitersList, function (recruiter, key) {
                       if(arrayOfFilterValues.indexOf(recruiter.userId) != -1){
                           recruiter.ticked = true;
                       }
                    });
                    break;
                default:
                    break;
            }
        }

        function selectRemoveAll(label, type, doQueryFlag){
            if(type == 'selectAll'){
                switch (label){
                    case 'Clients':
                        // set the selected clients on the query filter
                        vm.candidatesContainer.candidatesQueryFilter.clientOrBuList = vm.clientOrBuList.map(function (val, index) {
                            return val.id;
                        });
                        break;
                    case 'CandidateStatus':
                        // set the selected clients on the query filter
                        vm.candidatesContainer.candidatesQueryFilter.status = vm.candidateStatusList.map(function (val, index) {
                            return val.value;
                        });
                        break;
                    case 'RequisitionStatus':
                        // set the selected requisition statuses on the query filter
                        vm.candidatesContainer.candidatesQueryFilter.requisitionStatus = vm.requisitionStatusList.map(function (val, index) {
                            return val.name;
                        });
                        break;
                    case 'Recruiters':
                        // set the selected recruiters on the query filter
                        vm.candidatesContainer.candidatesQueryFilter.recruiterList = vm.recruitersList.map(function (val, index) {
                            return val.userId;
                        });
                        break;
                    case 'RequisitionNumbers':
                        // set the selected requisition numbers on the query filter
                        vm.candidatesContainer.candidatesQueryFilter.requisitionNumbers = vm.requisitionNumbersList.map(function (val, index) {
                            return val.value;
                        });
                        break;
                    default:
                        //do nothing;
                }
            }else{
                switch (label){
                    case 'Clients':
                        // set the selected clients on the query filter
                        vm.candidatesContainer.candidatesQueryFilter.clientOrBuList = [];
                        break;
                    case 'CandidateStatus':
                        // set the selected clients on the query filter
                        vm.candidatesContainer.candidatesQueryFilter.status = [];
                        break;
                    case 'RequisitionStatus':
                        // set the selected requisition statuses on the query filter
                        vm.candidatesContainer.candidatesQueryFilter.requisitionStatus = [];
                        break;
                    case 'Recruiters':
                        // set the selected recruiters on the query filter
                        vm.candidatesContainer.candidatesQueryFilter.recruiterList = [];
                        break;
                    case 'RequisitionNumbers':
                        // set the selected requisition numbers on the query filter
                        vm.candidatesContainer.candidatesQueryFilter.requisitionNumbers = [];
                        break;
                    default:
                        //do nothing
                }
            }
            if(_.isUndefined(doQueryFlag) || (!_.isUndefined(doQueryFlag) && doQueryFlag)) {
                // trigger a new query
                vm.queryCandidates();
            }
        }

        function visibilityFilterChanged(){
            vm.candidatesContainer.candidatesQueryFilter.visibility = [];
            if(vm.visibilityFilters.unassigned != ''){
                vm.candidatesContainer.candidatesQueryFilter.visibility.push('unassigned');
            }
            if(vm.visibilityFilters.assignedToMe != ''){
                vm.candidatesContainer.candidatesQueryFilter.visibility.push('assignedToMe');
            }
            if(vm.visibilityFilters.assignedToOthers != ''){
                vm.candidatesContainer.candidatesQueryFilter.visibility.push('assignedToOthers');
            }
            // trigger a new query
            vm.queryCandidates();
        }

        function onClientClick(selectedClients) {
            // set the selected clients on the query filter
            vm.candidatesContainer.candidatesQueryFilter.clientOrBuList = vm.selectedClientOrBuList.map(function (val, index) {
                return val.id;
            });
            // trigger a new query
            vm.queryCandidates();
        }

        function onCandidateStatusClick(selectedCandidateStatus){
            // set the selected clients on the query filter
            vm.candidatesContainer.candidatesQueryFilter.status = vm.selectedCandidateStatusList.map(function (val, index) {
                return val.value;
            });
            // trigger a new query
            vm.queryCandidates();
        }

        function onRequisitionStatusClick(selectedRequisitionStatus){
            // set the selected requisition statues on the query filter
            vm.candidatesContainer.candidatesQueryFilter.requisitionStatus = vm.selectedRequisitionStatusList.map(function (val, index) {
                return val.name;
            });
            // trigger a new query
            vm.queryCandidates();
        }

        function onRecruiterClick(selectedRecruiter){
            // set the selected recruiters on the query filter
            vm.candidatesContainer.candidatesQueryFilter.recruiterList = vm.selectedRecruitersList.map(function (val, index) {
                return val.userId;
            });
            // trigger a new query
            vm.queryCandidates();
        }

        function onRequisitionNumbersClick(selectedRequisitionNumbers) {
            // set the selected requisition numbers on the query filter
            vm.candidatesContainer.candidatesQueryFilter.requisitionNumbers = vm.selectedRequisitionNumbersList.map(function (val, index) {
                return val.value;
            });
            // trigger a new query
            vm.queryCandidates();
        }

        function sortCandidates(sortByFilter, direction){
            if(_.isNull(sortByFilter)){
                vm.selectedSort = 'Please Select';
                vm.selectedSortDirection = '';
                delete vm.candidatesContainer.candidatesQueryFilter.column;
                delete vm.candidatesContainer.candidatesQueryFilter.sortDirection;
            }else{
                vm.selectedSort = sortByFilter.columnDisplayName;
                vm.candidatesContainer.candidatesQueryFilter.column = sortByFilter.column;
                vm.selectedSortDirection = 'Asc';
                vm.candidatesContainer.candidatesQueryFilter.sortDirection = 'ASC';
                if(direction == 'Desc') {
                    vm.selectedSortDirection = 'Desc';
                    vm.candidatesContainer.candidatesQueryFilter.sortDirection = 'DESC';
                }
            }
            // trigger a new query
            vm.queryCandidates();
        }

        function queryCandidates() {
            // clear any existing query requests
            // check for an existing promise and see if it has not been fulfilled yet. If it has not been fulfilled yet, cancel the promise.
            if(!_.isNull(vm.candidatesContainer.currentPromise) && vm.candidatesContainer.currentPromise.$$state.status == 0) {
                genericService.cancelRequest(vm.candidatesContainer.currentPromise);
            }
            // reset the initial value of candidates exist flag to true
            vm.candidatesContainer.candidatesExistFlag = true;
            // reset the page number to 1 as this is a new query
            vm.candidatesContainer.candidatesQueryFilter.pageNum = 1;
            // clear the existing candidates
            vm.candidatesContainer.candidates = [];
            vm.candidatesContainer.disableInfiniteScroll = false;
            // wait for the cancel promise to be fulfilled, if it was called.
            $timeout(function () {
                vm.candidatesContainer.getNext();
            }, 200);
        }

        function candidatesContainer(){
            this.candidates = [];
            this.busy = false;
            this.candidatesExistFlag = true;
            this.candidatesQueryFilter = new CandidatesQueryFilter($rootScope.userDetails.company.companyId,$rootScope.userDetails.id);
            this.currentPromise = null;
            this.disableInfiniteScroll = false;

            this.getNext = function () {
                var parent = this;
                if(this.busy) return;
                this.busy = true;

                if(this.candidatesQueryFilter.pageNum == ''){
                    this.candidatesQueryFilter.pageNum = 1;
                }
                //console.log('filter object in get next -', this.candidatesQueryFilter);
                this.currentPromise = candidateService.queryCandidateIdNames(this.candidatesQueryFilter, function (data) {
                    if(angular.isDefined(data) && data != 'user cancellation'){
                        if(data.length > 0){
                            angular.forEach(data, function (val, key) {
                                if(key % 4 == 0){
                                    val.jobMatchesPopoverPosition = 'bottom-left';
                                }else{
                                    val.jobMatchesPopoverPosition = 'bottom';
                                }
                                val.candidateIndex = (parent.candidates.length + 1);
                                val.jobMatchesPopoverIsOpenFlag = false;
                                parent.candidates.push(angular.merge(new Candidate(), val));
                            });
                            // if the length of the returned values is less than page size, it means there are not enough values.
                            // hence we can disable the infinite scroll
                            if(data.length < parent.candidatesQueryFilter.pageSize){
                                parent.disableInfiniteScroll = true;
                            }else{
                                parent.candidatesQueryFilter.pageNum++;
                            }
                        }else{
                            // if the length of the returned values is 0, it means there are no more values.
                            // hence we can disable the infinite scroll
                            parent.disableInfiniteScroll = true;
                        }
                        // set the candidates exist flag based on existence of candidates.
                        if(parent.candidates.length > 0){
                            parent.candidatesExistFlag = true;
                        }else{
                            parent.candidatesExistFlag = false;
                        }
                    }
                    //set parent.busy to false
                    parent.busy = false;
                }, function (error) {
                    if ($rootScope.isOnline) {
                        alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                    }
                    parent.busy = false;
                });
            }
        }

        function getAllClientsOrBu (successCallback) {
            candidateService.getAllClientsOrBu($rootScope.userDetails, function (data) {
                vm.clientOrBuList = data;
                if(successCallback){
                    successCallback();
                }
            }, function (error) {
                if ($rootScope.isOnline) {
                    alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                }
            });
        }

        function setCandidateInfo(candidateInfo, type) {
            // if a current container id exits, close it.
            if(!_.isNull(vm.candidateInfoContainerId)){
                vm.closeCandidateInfoRow(vm.candidateInfoContainerId);
            }
            //$('.candidate-info-container').hide();
            vm.infoOfCandidateInContext = candidateInfo;

            var allCandidatesLength = vm.candidatesContainer.candidates.length;
            var allCandidatesFullQuadrants = Math.floor(allCandidatesLength / 4);
            var currentCandidateQuadrant = Math.ceil(candidateInfo.candidateIndex / 4);
            var candidateInfoContainerId = null;
            if(currentCandidateQuadrant <= allCandidatesFullQuadrants){
                candidateInfoContainerId = 'candidate-info-' + (currentCandidateQuadrant * 4);
            }else{
                candidateInfoContainerId = 'candidate-info-' + allCandidatesLength;
            }
            switch(type){
                case 'jobMatches':
                    vm.candidateInfoTemplate = 'app/directives/candidateCard/candidate-job-matches-all.html';
                    break;
                default:
                    break;
            }
            // time out needed for variables to refresh on view and reload the job matches directive.
            $timeout(function () {
                vm.candidateInfoContainerId = candidateInfoContainerId;
                candidateInfo.jobMatchesPopoverIsOpenFlag = false;
                $("#" + candidateInfoContainerId).show();
            }, 100);
        }

        function closeCandidateInfoRow(containerId){
            $('#'+containerId).hide();
            vm.candidateInfoContainerId = null;
        }

        function getAllCandidateStatuses(successCallback){
            vm.candidateStatusList = [
                {
                    "name": "Active",
                    "value": "Active"
                },
                {
                    "name": "Passive",
                    "value": "Passive"
                },
                {
                    "name": "Inert",
                    "value": "Inert"
                }
            ];
            if(successCallback){
                successCallback();
            }
        }

        function getAllRequisitionStatuses(successCallback){
            jobService.getAllStatusTypes(function (data) {
                vm.requisitionStatusList = data;
                if(successCallback){
                    successCallback();
                }
            }, function (error) {
                if ($rootScope.isOnline) {
                    alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                }
            });
        }

        function getAllRecruiters(successCallback) {
            jobService.getAllRecruiters($scope.userDetails, $rootScope.userDetails.company, function(data){
                vm.recruitersList = data;
                angular.forEach(vm.recruitersList, function (val, key) {
                    var name = '';
                    if(val.lastName !== null){
                        name = val.firstName+" "+val.lastName;
                    }else{
                        name = val.firstName;
                    }
                    val.name = name;
                });
                if(successCallback){
                    successCallback();
                }
            }, function (error) {
                if ($rootScope.isOnline) {
                    alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                }
            });
        }

        function getAllRequisitionNumbers(successCallback){
            jobService.searchByReqNum($scope.userDetails.id,$rootScope.userDetails.company.companyId,'all',function (data) {
                vm.requisitionNumbersList = [];
                angular.forEach(data, function (val, key) {
                   vm.requisitionNumbersList.push({'name': val, 'value': val});
                });
                if(successCallback){
                    successCallback();
                }
            }, function (error) {
                if ($rootScope.isOnline) {
                    alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                }
            });
        }

        function filterByDate() {
            vm.candidatesContainer.candidatesQueryFilter.fromDate = "";
            vm.candidatesContainer.candidatesQueryFilter.toDate = "";
            if (angular.isDefined(vm.selectedDateRange) &&  vm.selectedDateRange !== "") {
                var array =  vm.selectedDateRange.split("-");
                vm.candidatesContainer.candidatesQueryFilter.fromDate = array[0].toString().trim();
                vm.candidatesContainer.candidatesQueryFilter.toDate = array[1].toString().trim();
            } else {
                vm.selectedDateRange = "";
            }
            vm.queryCandidates();
    }
    
    function getAllActivityTypes(){
        jobService.getActivityTypes(function (data) {
            vm.activityMap = [];
            angular.forEach(data, function (val, key) {
                vm.activityMap[val.value] = val.name;
            });
        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }

    $(document).ready(function () {
        $("#search-panel").hide();
        $(document).off("click", "#open-filter").on("click", "#open-filter",
            function () {
                $("#search-panel").animate({
                    height: 'toggle'
                });
            });
    });
}
