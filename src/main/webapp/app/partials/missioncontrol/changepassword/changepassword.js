/**
 * All the Functionalities Related to Reset password
 * Module is included here
 */

var changepasswordmodule = angular.module('4dot5.missioncontrolmodule.changepasswordmodule',
    []);

changepasswordmodule.constant('CHANGEPASSWORDCONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.changepassword',
        URL: '/changepassword',
        CONTROLLER: 'ChangePasswordController',
        TEMPLATEURL: 'app/partials/missioncontrol/changepassword/changepassword.html',
    },
    CONTROLLER: {
        CHANGE_PASSWORD: 'api/access/changepassword',
        AUTHENTICATE_OTP: 'auth/verifyonetimepassword'
    }
});

changepasswordmodule.config(
    ['$stateProvider',
        'CHANGEPASSWORDCONSTANTS',
        function ($stateProvider, CHANGEPASSWORDCONSTANTS) {
            $stateProvider.state(CHANGEPASSWORDCONSTANTS.CONFIG.STATE, {
                url: CHANGEPASSWORDCONSTANTS.CONFIG.URL,
                templateUrl: CHANGEPASSWORDCONSTANTS.CONFIG.TEMPLATEURL,
                controller: CHANGEPASSWORDCONSTANTS.CONFIG.CONTROLLER,
                data: {
                    requireLogin: true
                },
                params: {
                    
                }
            });
        }
    ]);

changepasswordmodule.directive('validPasswordNew', function () {
    return {
        require: 'ngModel',
        scope: {
            reference: '=validPasswordNew'
        },
        link: function (scope, elm, attrs, ctrl) {
            ctrl.$parsers.unshift(function (viewValue, $scope) {
                var noMatch = viewValue != scope.reference
                ctrl.$setValidity('noMatch', !noMatch);
                return (noMatch) ? noMatch : !noMatch;
            });
            scope.$watch("reference", function (value) {
                ctrl.$setValidity('noMatch', value === ctrl.$viewValue);
            });
        }
    }
});

changepasswordmodule.controller('ChangePasswordController',
    ['$scope',
        '$state',
        '$window',
        '$location',
        '$stateParams',
        '$rootScope',
        'StorageService',
        'genericService',
        'MESSAGECONSTANTS',
        'CHANGEPASSWORDCONSTANTS',
        'alertsAndNotificationsService',
        function ($scope, $state, $window, $location, $stateParams, $rootScope, StorageService, genericService, MESSAGECONSTANTS, CHANGEPASSWORDCONSTANTS, alertsAndNotificationsService) {

            console.log('ChangePasswordController : '+ $rootScope.userActive);
            $scope.form = {};
            $scope.user = {};

            $scope.$watchGroup(['user.currentPassword','user.newPassword'],function(newValue, oldValue, scope){
                if($scope.user.currentPassword === $scope.user.newPassword){
                    console.log('equal');
                    $scope.form.changepasswordform.$setValidity('unMatched', false);
                    $scope.form.changepasswordform.newpassword.$setValidity('unMatched', false);
                }else {
                    console.log('not equal');
                    $scope.form.changepasswordform.$setValidity('unMatched', true);
                    $scope.form.changepasswordform.newpassword.$setValidity('unMatched', true);
                }
            })

            $scope.changePassword = function () {
                $scope.user.email = $rootScope.userDetails.emailId;
                $scope.user.verifyPassword = $scope.user.newPassword;
                console.log('Requested for Resetting Password : ' + angular.toJson($scope.user));
                genericService.addObject(CHANGEPASSWORDCONSTANTS.CONTROLLER.CHANGE_PASSWORD, $scope.user).then(function (data) {
                	alertsAndNotificationsService.showBannerMessage(data.message, 'success');
                	console.log($rootScope.userDetails.company.companyType = MESSAGECONSTANTS.COMPANY_TYPES.FOURDOTFIVE);
                	if($rootScope.userDetails.company.companyType = MESSAGECONSTANTS.COMPANY_TYPES.FOURDOTFIVE){
                		$state.go('missioncontrol.dashboardsuperusermodule', null, { reload: true });
                	}else{
                		$state.go('missioncontrol.dashboard', null, { reload: true });
                	}
                }, function (error) {
                    if(error.statusCode === '401'){
                    	$scope.form.changepasswordform.$setPristine();
                    	$scope.errormessage = error.message;
                    }else if($rootScope.isOnline){
                    	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    }
                });
            }
        }
    ]);