/**
 * All the Functionalities Related to Client Management
 * Module is Included Here
 */

var clientmanagementmodue = angular.module('4dot5.missioncontrolmodule.clientmanagementmodule',
    []);

clientmanagementmodue.constant('CLIENTMANAGEMENTCONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.clientmanagement',
        URL: '/clientmanagement',
        CONTROLLER: 'ClientManagementController',
        TEMPLATEURL: 'app/partials/missioncontrol/clientmanagementmodule/clientmanagement.html',
    },
    CONTROLLER: {
        DELETE_CLIENT: 'api/company/deleteclient',
        GET_CLIENT_OR_BU: 'api/company/getclientsorbu',
        GET_ASSESSMENT_WORK_FLOW: 'api/createcompany/getassessmentworkflow',
        GET_COMPANY: 'api/createcompany/getcompanydetails',
        SAVE_COMPANY: 'api/createcompany/savecompany'
    }
});

clientmanagementmodue.config(
    ['$stateProvider',
        'CLIENTMANAGEMENTCONSTANTS',
        function ($stateProvider, CLIENTMANAGEMENTCONSTANTS) {
            $stateProvider.state(CLIENTMANAGEMENTCONSTANTS.CONFIG.STATE, {
                url: CLIENTMANAGEMENTCONSTANTS.CONFIG.URL,
                templateUrl: CLIENTMANAGEMENTCONSTANTS.CONFIG.TEMPLATEURL,
                controller: CLIENTMANAGEMENTCONSTANTS.CONFIG.CONTROLLER,
                resolve: {
                    ClientOrBuListDetails: function ($rootScope, genericService) {
                        return genericService.getObjects(CLIENTMANAGEMENTCONSTANTS.CONTROLLER.GET_CLIENT_OR_BU + '/' + $rootScope.userDetails.company.companyId);
                    }
                },
                data: {
                    requireLogin: true
                }
            });
        }
    ]);

clientmanagementmodue.controller('ClientManagementController',
    ['$scope',
        '$http',
        '$state',
        '$timeout',
        '$compile',
        '$rootScope',
        '$stateParams',
        '$uibModal',
        'ClientOrBuListDetails',
        'DTOptionsBuilder',
        'DTColumnDefBuilder',
        'StorageService',
        'genericService',
        'companyService',
        'CLIENTMANAGEMENTCONSTANTS',
        'alertsAndNotificationsService',
        function ($scope, $http, $state, $timeout, $compile, $rootScope, $stateParams, $uibModal, ClientOrBuListDetails, DTOptionsBuilder, DTColumnDefBuilder, StorageService, genericService, companyService, CLIENTMANAGEMENTCONSTANTS, alertsAndNotificationsService) {
            console.log('ClientManagementController');
            var parentCompanyId;
            var clientOrBuObjectToBeDeleted;
            var clientOrBuIndexToBeDeleted
            var loggedInUserDetails = StorageService.getCookie('userDetails');
            $scope.ClientOrBuList = [];
            $scope.companyType = $rootScope.userDetails.company.companyType;

            if ($scope.companyType === 'Corporation') {
                var buttonLabel = 'Add New BU';
                var backGroundEmptyLabel = 'No BUs to display';
            }
            else {
                var buttonLabel = 'Add new client';
                var backGroundEmptyLabel = 'No Clients to display';
            }


            if (ClientOrBuListDetails !== null) {
                $scope.ClientOrBuList = angular.copy(ClientOrBuListDetails);
            }

            function getClientOrBuList(companyid) {
                $timeout(function () {
                    genericService.getObjects(CLIENTMANAGEMENTCONSTANTS.CONTROLLER.GET_CLIENT_OR_BU + '/' + companyid).then(function (data) {
                        $scope.ClientOrBuList = data;
                    }, function (error) {
                    	if($rootScope.isOnline){
                        	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                        }
                    });
                }, 1000);
            }

            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withPaginationType('full_numbers')
                .withOption('order',[4,'desc'])
                .withOption('bFilter', true)
                .withOption('bLengthChange', false)
                .withOption('bInfo', true)
                .withOption('language', {  search: "",searchPlaceholder: 'type to filter',zeroRecords: backGroundEmptyLabel })
                .withBootstrap()
                // Active Buttons extension
                .withButtons([
                    {
                        tag: 'button',
                        className: 'btn btn-primary datatablebutton',
                        text: buttonLabel,
                        key: '2',
                        action: function (e, dt, node, config) {
                            $scope.addClientOrBu()
                        },
                    }
                ])
                .withDOM("<'row'<'col-xs-12 text-right'Bf>><t>ip");

            $scope.dtColumns = [
                DTColumnDefBuilder.newColumnDef(0),
                DTColumnDefBuilder.newColumnDef(1),
                DTColumnDefBuilder.newColumnDef(2).notSortable(),
                DTColumnDefBuilder.newColumnDef(3),
                DTColumnDefBuilder.newColumnDef(4),
                DTColumnDefBuilder.newColumnDef(5),
                DTColumnDefBuilder.newColumnDef(6),
                DTColumnDefBuilder.newColumnDef(7).notSortable()
            ];

            /**
             * Function to delete client
             */
            $scope.deleteClientOrBU = function (clientOrBU) {
                $scope.getDeleteClientOrBUDetails(clientOrBU);
            };

            $scope.getDeleteClientOrBUDetails = function (clientOrBU) {
                companyService.getDeleteCompanyInfo(clientOrBU.company.id, function (data) {
                   $scope.deleteClientOrBUObject = {
                        clientOrBUId: clientOrBU.company.id,
                        clientOrBUName:  clientOrBU.company.name,
                        requisitionsCount: data.numberOfRequisitions,
                       usersCount: data.numberOfUsers,
                       companyType: $scope.companyType
                    }  
                    $scope.launchDeleteModal();
                }, function (error) {
                    if ($rootScope.isOnline) {
                        alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                    }
                });
            }

            $scope.launchDeleteModal = function () {
                $scope.deleteClientOrModal = $uibModal.open({
                    animation: false,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/partials/missioncontrol/clientmanagementmodule/delete-client-or-bu-modal.html',
                    controller: ['$uibModalInstance','clientOrBUInfo',function ($uibModalInstance, clientOrBUInfo) {
                        var vm = this;
                        vm.clientOrBUInfo = clientOrBUInfo;
                        vm.closeModal = function () {
                            $uibModalInstance.close('cancel');
                        };
                    }],
                    controllerAs: 'deleteClientOrBUModal',
                    size: 'md',
                    resolve:{
                        clientOrBUInfo: function () {
                            return $scope.deleteClientOrBUObject;
                        }
                    }
                });
            }

            /**
             * Function to update clientOrBu
             */
            $scope.updateClientOrBu = function (clientOrBu) {
                StorageService.set('clientOrBuToBeUpdated', clientOrBu);
                $state.go('missioncontrol.companymanagement', null, { reload: true });
              
            }

            /**
             * Function to create clientOrBu
             */
            $scope.addClientOrBu = function () {
                StorageService.set('clientOrBuCreationInProgress', true);
                $state.go('missioncontrol.companymanagement', null, { reload: true });
                
            }

        }
    ]); 