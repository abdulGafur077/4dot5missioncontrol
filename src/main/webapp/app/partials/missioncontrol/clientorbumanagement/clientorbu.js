var clientorbumanagement = angular.module('4dot5.missioncontrolmodule.clientorbumanagement',
    []);

clientorbumanagement.constant('COMPANYMANAGEMENTCONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.clientorbu',
        URL: '/clientorbu',
        CONTROLLER: 'ClientOrBuManagementController',
        TEMPLATEURL: 'app/partials/missioncontrol/clientorbumanagement/clientorbu.html',
    },
    CONTROLLER: {
        ASSIGN_CLIENT_TO_STAFFING_COMPANY: 'api/company/assignclientorbu',
        GET_INDUSTRY_SUBLIST: 'api/company/getindustrysublist/',
        GET_INDUSTRY_GROUPS: 'api/company/getindustrylist',
        GET_4DOT5_ADMINS: 'api/company/get4dot5admins',
        GET_ENTITY_SCOPES: 'api/company/getentityscope',
        GET_COMPANY_TYPE_DETAILS: 'api/company/getcompanytype',
        GET_ASSESSMENT_WORK_FLOW: 'api/licensepref/getlicensepreferences/',
        GET_ASSESSMENT_WORK_FLOW_FOR_CLIENT_BU: 'api/licensepref/getclientorbulicensepreferences/',
        SET_LICENCE_PREFERENCES: 'api/licensepref/setlicensepreferences',
        SET_LICENCE_PREFERENCES_FOR_CLIENT_BU: 'api/licensepref/setclientorbulicensepreferences',
        GET_COMPANY: 'api/company/getcompanydetails',
        SAVE_COMPANY: 'api/company/savecompany',
        DELETE_COMPANY: 'api/company/deletecompany',
        UPDATE_COMPANY_FOR_FOURDOTFIVE_ADMIN: 'api/user/updatecompanyfor4dot5admin',
        CHANGE_COMPANYSTATE: 'api/company/changecompanystate',
        GET_COMPANY_DETAILS: 'api/company/getcompanydetails'
    }
});


clientorbumanagement.filter('unique', function() {
    return function(collection, keyname) {
        var output = [],
            keys = [];
        angular.forEach(collection, function(item) {
            var key = item[keyname];
            if (keys.indexOf(key) === -1) {
                keys.push(key);
                output.push(item);
            }
        });
        return output;
    };
});

clientorbumanagement.config(
    ['$stateProvider',
        'COMPANYMANAGEMENTCONSTANTS',
        function($stateProvider, COMPANYMANAGEMENTCONSTANTS) {
            $stateProvider.state(COMPANYMANAGEMENTCONSTANTS.CONFIG.STATE, {
                url: COMPANYMANAGEMENTCONSTANTS.CONFIG.URL,
                templateUrl: COMPANYMANAGEMENTCONSTANTS.CONFIG.TEMPLATEURL,
                controller: COMPANYMANAGEMENTCONSTANTS.CONFIG.CONTROLLER,
                data: {
                    requireLogin: true
                },
                params: {
                    companyObject: null,
                    userId: null,
                    accessMode: null,
                    addClient: null,
                    updateClient: null,
                    bu: null,
                    selectedCompanyId: null
                }
            });
        }
    ]);

clientorbumanagement.controller('ClientOrBuManagementController',
    ['$scope',
        '$state',
        '$stateParams',
        '$rootScope',
        'genericService',
        'StorageService',
        'COMPANYMANAGEMENTCONSTANTS',
        'MESSAGECONSTANTS',
        'usSpinnerService',
        '$timeout',
        '$http',
        '$compile',
        'ngToast',
        '$filter',
        'alertsAndNotificationsService',
        function($scope, $state, $stateParams, $rootScope, genericService, StorageService, COMPANYMANAGEMENTCONSTANTS, MESSAGECONSTANTS, usSpinnerService, $timeout, $http, $compile, ngToast, $filter, alertsAndNotificationsService) {
            var loggedInUserDetails = StorageService.getCookie('userDetails');
            console.log('Client new page :');
            var newlyCreatedCompanyId = null;
            var fourDotFiveCompanyAdmin;
            var assignClient2StaffingCompany;  //NP
            var industrySublist = [];          //NP
            var count = 0;
            var confirmationAtStep;
            var showConfirmationPopup = false;
            var updatedAssessementFlow;
            var companyStepOneSaved = false;
            var companyStepTwoSaved = false;
            var companyStepThreeSaved = false;
            var companyStepFourSaved = false;
            var stepOneChanged = false;
            var stepTwoChanged = false;
            var stepThreeChanged = false;
            var stepFourChanged = false;
            var getDetailsOfCompany;
            var fourDotFiveAdminObject = null;
            var oldfourdotfiveadminid = null;
            var clientOrBuUpdateInProgress = false;
            var clientOrBuCreationInProgress = false;
            var companyDeleted = false;

            var maskedMobilePhoneError;
            var maskedWorkPhoneError;
            var maskedWorkPhoneSecondaryError;
            var maskedMobilePhoneSecondaryError;
            var countryForContactDetails;
            var industryListOfCompany = 0;
            var oldCheckedAdmin = null;
            var selectedSubIndustries = []
            var cancelClicked = false;

            $scope.zipcodeCalling = false;
            $scope.zipcodeCallCompleted = false;
            var saveButtonClicked = false;
            var nextButtonClicked = false;
            var nextButtonFlag = 0;
            var saveButtonFlag = 0;

            $scope.form = {};
            $scope.form.companydetailsform = {};
            $scope.form.companycontactpersondetailsform = {};
            $scope.form.companysecondarycontactpersondetailsform = {};
            $scope.form.company4dot5admindetailsform = {};
            $scope.form.companyassessementflowdetailsform = {};
            $scope.maskedMobilePhoneSecondarycursorPosition = true;

            //This is the UI object (Transient object)
            $scope.CompanyManagement = {};
            $scope.CompanyManagementReset = {};
            $scope.CompanyManagement.company = {};//Step 1 object.
            $scope.CompanyManagement.contactList = [];//Step 2 object.
            $scope.fourDotFiveAdminsList = [];//Step 3 object.
            $scope.AssessmentWorkFlow = [];//Step 4 object.
            
            //This is the Saved object (Saved object),the object that will be sent while saving the step details.
            $scope.SavedCompanyManagement = {};
            $scope.SavedCompanyManagement.company = {};//Step 1 object
            $scope.SavedCompanyManagement.contactList = [];//Step 2 object
            $scope.SavedfourDotFiveAdmin = [];//Step 3 object
            $scope.SavedAssessmentWorkFlow = [];//Step 4 object
            
            var savedCompanyManagementCopy = {};
            savedCompanyManagementCopy.company = {};
            savedCompanyManagementCopy.contactList = [];
            var savedfourDotFiveAdminCopy = [];
            var savedAssessmentWorkFlowCopy = [];
            
            $scope.preferences = null;
            $scope.errorInSavingPrefsCheck = false;
            $scope.cancelNotClicked = true;
            $scope.invalidZipcode = false;

            $scope.licensePreferencesFetched = false;

            $scope.selectedIndustryGroup = null;
            $scope.laststep = false;
            $scope.TypeOfCompany = 'Company';
            $scope.SecondaryContact = 'Add';
            $stateParams.accessMode = 'view';
            $scope.IndustrySubList = [];
            $scope.SelectedIndustrySubList = [];
            $scope.typeaheadNotSelected = true;
            var valid = false;
            var companyTypes = null;
            var triggercount = 0;
            $scope.CompanyTypes = [];
            $scope.lastStepCompleted = false;
            $scope.ShowCreateCompanyHeading = true;
            $scope.ClientOrBuUpdate = false;
            $scope.ClientOrBuCreate = false;
            $scope.IndustryGroupList = [];
            $scope.IndustrySubGroupList = [];
            $scope.dataLastLabel = 'Complete';
            $scope.selectedSubGroup = false;
            $scope.industriesInPanel = [];
            $scope.temp =[]; 

            $scope.primaryContactWTFormat = '+1 201-555-0123';
            $scope.primaryContactWTFormat1 = '+1 201-555-0123';
            $scope.primaryContactMNFormat = '+1 201-555-0123';
            $scope.secondaryContactWTFormat = '+1 201-555-0123';
            $scope.secondaryContactMNFormat = '+1 201-555-0123';
            $scope.primaryContactWTCountryCode = null;
            $scope.primaryContactWTCountryCode1 = null;
            $scope.primaryContactMNCountryCode = null;
            $scope.secondaryContactWTCountryCode = null;
            $scope.secondaryContactMNCountryCode = null;

            $scope.isValidPrimaryMobileEntered = false;
            $scope.isValidPrimaryWorkPhoneEntered = false;
            $scope.isValidSecondaryMobileEntered = false;
            $scope.isValidSecondaryWorkPhoneEntered = false;
            var isAdressValid = false;
            var tempObj = null;
            $scope.errorOcurred = false;
            $scope.isCountyExists = false;
           
            /**
             * This method works for triggerng button clicks on save button
             *  only for on blur validations
             */
            var saveButtonTrigger = function () {
            	console.log('saveButtonTrigger');
            	var savebtnclick = $timeout(function () {
                    angular.element('.btn-save').triggerHandler('click');
                }, 0).then(function () {
                    saveButtonClicked = false;
                    nextButtonClicked = false;
                    $timeout.cancel(savebtnclick);
                });
            };
            
             /**
             * This method works for triggerng button clicks on save button
             *  only for on blur validations
             */
            var nextButtonTrigger = function () {
            	var nextbtnclick = $timeout(function () {
                    angular.element('.btn-next').triggerHandler('click');
                }, 0).then(function () {
                    nextButtonClicked = false;
                    saveButtonClicked = false;
                    $timeout.cancel(nextbtnclick);
                });
            };

            $scope.isPrimaryContactWTValid = function (workPhoneValue) {
                if (workPhoneValue !== "") {
                    if ($("#maskedWorkPhone").intlTelInput("isValidNumber")) {
                        console.log('p WorkTelephone Valid');
                        $scope.isValidPrimaryWorkPhoneEntered = true;
                        $scope.CompanyManagement.contactList[0].workPhone = $filter('phonenumber')(workPhoneValue, $scope.primaryContactWTFormat);
                        $scope.form.companycontactpersondetailsform.maskedWorkPhone.$setValidity('ng-intl-tel-input', true);
                    } else {
                        console.log('p work telephone not valid');
                        $scope.emptyCompanyWorkNumber = false;
                        $scope.invalidCompanyWorkNumber = true;
                        $scope.isValidPrimaryWorkPhoneEntered = false;
                        $scope.primaryWorkTelephoneValueTrim = document.getElementById('maskedWorkPhone').value;
                        $scope.CompanyManagement.contactList[0].workPhone =  $scope.primaryWorkTelephoneValueTrim.replace(/ |-/gm,'');
                        console.log( $scope.CompanyManagement.contactList[0].workPhone);
                        $scope.form.companycontactpersondetailsform.maskedWorkPhone.$setValidity('ng-intl-tel-input', false);
                    }
                } else {
                    $scope.emptyCompanyWorkNumber = true;
                    $scope.invalidCompanyWorkNumber = false;
                    console.log('CompanyWorkNumber empty');
                    $scope.isValidPrimaryWorkPhoneEntered = false;
                }
            }

            $scope.isPrimaryContactWTValid1 = function (mobileNumberValue) {
                if (mobileNumberValue !== "") {
                    $scope.form.companycontactpersondetailsform.maskedWorkPhone1.visited = true;
                    if ($("#maskedWorkPhone1").intlTelInput("isValidNumber")) {
                        console.log('p WorkTelephone Valid');
                        $scope.isValidPrimaryMobileEntered = true;
                        $scope.CompanyManagement.contactList[0].mobilePhone = $filter('phonenumber')(mobileNumberValue, $scope.primaryContactWTFormat1);
                        $scope.form.companycontactpersondetailsform.maskedWorkPhone1.$setValidity('ng-intl-tel-input', true);
                    } else {
                        console.log('p work telephone not valid');
                        $scope.emptyCompanyMobileNumber = false;
                        $scope.invalidCompanyMobileNumber = true;
                        $scope.isValidPrimaryMobileEntered = false;
                        $scope.primaryMobileTelephoneValueTrim = document.getElementById('maskedWorkPhone1').value;
                        $scope.CompanyManagement.contactList[0].mobilePhone =  $scope.primaryMobileTelephoneValueTrim.replace(/ |-/gm,'');
                        console.log( $scope.CompanyManagement.contactList[0].mobilePhone);
                        $scope.form.companycontactpersondetailsform.maskedWorkPhone1.$setValidity('ng-intl-tel-input', false);
                    }
                    console.log('isValidPrimaryMobileEntered '+$scope.isValidPrimaryMobileEntered);
                } else {
                    $scope.emptyCompanyMobileNumber = true;
                    $scope.invalidCompanyMobileNumber = false;
                    console.log('mobileNumberValue empty');
                    $scope.isValidPrimaryMobileEntered = false;
                }
                console.log('isValidPrimaryMobileEntered '+$scope.isValidPrimaryMobileEntered);
            }
            
            $("#maskedWorkPhone").keyup(function () {
                if (document.getElementById('maskedWorkPhone').value.length > 0) {
                    $scope.workTelephoneDirty = true;
                } else {
                    $scope.workTelephoneDirty = false;
                }
            });


	        $("#maskedWorkPhone1").keyup(function () {
	            if (document.getElementById('maskedWorkPhone1').value.length > 0) {
	                $scope.mobileNumberDirty = true;
	            } else {
	                $scope.mobileNumberDirty = false;
	            }
            });

           $scope.setPrimaryContactWTCountryCode = function () {
                console.log('setCountryCodes with $scope.setPrimaryContactWTCountryCode : ' + $scope.primaryContactWTCountryCode);
                $("#maskedWorkPhone").intlTelInput("setCountry", $scope.primaryContactWTCountryCode);
                $scope.primaryContactWTFormat = document.getElementById("maskedWorkPhone").placeholder;
                var maskValidator = [];

                console.log('Length: ' + $scope.primaryContactWTFormat.length);
                for (var index = 0; index < $scope.primaryContactWTFormat.length; index++) {
                    if (angular.isNumber($scope.primaryContactWTFormat[index])) {
                        console.log('Num: ' + $scope.primaryContactWTFormat[index]);
                    } else {
                        console.log('Char: ' + $scope.primaryContactWTFormat[index]);
                    }
                }
                $timeout(function () {
                    $scope.isPrimaryContactWTValid(document.getElementById("maskedWorkPhone").value);
                }, 500);
            }

            $scope.setPrimaryContactWTCountryCode1 = function () {
            	console.log('setCountryCodes with $scope.setPrimaryContactWTCountryCode1 : ' + $scope.primaryContactWTCountryCode1);
                $("#maskedWorkPhone1").intlTelInput("setCountry", $scope.primaryContactWTCountryCode1);
                $scope.primaryContactWTFormat1 = document.getElementById("maskedWorkPhone1").placeholder;
                var maskValidator1 = [];
                for (var index = 0; index < $scope.primaryContactWTFormat1.length; index++) {
                    if (angular.isNumber($scope.primaryContactWTFormat1[index])) {
                        console.log('Num: ' + $scope.primaryContactWTFormat1[index]);
                    } else {
                        console.log('Char: ' + $scope.primaryContactWTFormat1[index]);
                    }
                }
                $timeout(function () {
                    $scope.isPrimaryContactWTValid1(document.getElementById("maskedWorkPhone1").value);
                }, 500);
            }

            $scope.setPrimaryContactNExamples = function () {
        	    console.log('setPrimaryContactNExamples');
                $scope.primaryContactWTFormat = document.getElementById("maskedWorkPhone").placeholder;
                $scope.primaryContactWTFormat1 = document.getElementById("maskedWorkPhone1").placeholder;
                $scope.primaryContactMNFormat = document.getElementById("maskedMobilePhone").placeholder;
            }

            $scope.isSecondaryContactWTValid = function (secondaryWorkNumberValue) {
                if (secondaryWorkNumberValue !== "") {
                	console.log("////////////////////////",secondaryWorkNumberValue);
                    $scope.form.companysecondarycontactpersondetailsform.maskedWorkPhoneSecondary.visited = true;
                    if ($("#maskedWorkPhoneSecondary").intlTelInput("isValidNumber")) {
                    	$scope.isValidSecondaryWorkPhoneEntered = true;
                        console.log('p maskedWorkPhoneSecondary Valid');
                        $scope.CompanyManagement.contactList[1].workPhone = $filter('phonenumber')(secondaryWorkNumberValue, $scope.secondaryContactWTFormat);
                        $scope.form.companysecondarycontactpersondetailsform.maskedWorkPhoneSecondary.$setValidity('ng-intl-tel-input', true);
                    } else {
                        console.log('p work telephone not valid');
                        $scope.emptyCompanySecondaryWorkNumber = false;
                        $scope.invalidCompanySecondaryWorkNumber = true;
                        $scope.isValidSecondaryWorkPhoneEntered = false;
                        $scope.secondaryWorkTelephoneValueTrim = document.getElementById('maskedWorkPhoneSecondary').value;
                        $scope.CompanyManagement.contactList[1].workPhone =  $scope.secondaryWorkTelephoneValueTrim.replace(/ |-/gm,'');
                        $scope.form.companysecondarycontactpersondetailsform.maskedWorkPhoneSecondary.$setValidity('ng-intl-tel-input', false);
                    }
                } else {
                    console.log('maskedWorkPhoneSecondary empty');
                    $scope.emptyCompanySecondaryWorkNumber = true;
                    $scope.invalidCompanySecondaryWorkNumber = false;
                    $scope.isValidSecondaryWorkPhoneEntered = false;
                }
            }

            $scope.isSecondaryContactMNValid = function (secondaryMobileNumberValue) {
                if (secondaryMobileNumberValue !== "") {
                    $scope.form.companysecondarycontactpersondetailsform.maskedMobilePhoneSecondary.visited = true;
                    if ($("#maskedMobilePhoneSecondary").intlTelInput("isValidNumber")) {
                        console.log('p maskedWorkPhoneSecondary Valid');
                        $scope.isValidSecondaryMobileEntered = true;
                        $scope.CompanyManagement.contactList[1].mobilePhone = $filter('phonenumber')(secondaryMobileNumberValue, $scope.secondaryContactMNFormat);
                        $scope.form.companysecondarycontactpersondetailsform.maskedMobilePhoneSecondary.$setValidity('ng-intl-tel-input', true);
                    } else {
                        console.log('p maskedMobilePhoneSecondary not valid');
                        $scope.emptyCompanySecondaryMobilePhone = false;
                        $scope.invalidCompanySecondaryMobilePhone = true;
                        $scope.isValidSecondaryMobileEntered = false;
                        $scope.secondaryMobileTelephoneValueTrim = document.getElementById('maskedMobilePhoneSecondary').value;
                        $scope.CompanyManagement.contactList[1].mobilePhone =  $scope.secondaryMobileTelephoneValueTrim.replace(/ |-/gm,'');
                        $scope.form.companysecondarycontactpersondetailsform.maskedMobilePhoneSecondary.$setValidity('ng-intl-tel-input', false);
                    }
                } else {
                    $scope.emptyCompanySecondaryMobilePhone = true;
                    $scope.invalidCompanySecondaryMobilePhone = false;
                    console.log('maskedMobilePhoneSecondary empty');
                    $scope.isValidSecondaryMobileEntered = false;
                }
            }

            $scope.setSecondaryContactWTCountryCode = function () {
                console.log('setCountryCodes with $scope.secondaryContactWTCountryCode : ' + $scope.secondaryContactWTCountryCode);
                $("#maskedWorkPhoneSecondary").intlTelInput("setCountry", $scope.secondaryContactWTCountryCode);
                $scope.secondaryContactWTFormat = document.getElementById("maskedWorkPhoneSecondary").placeholder;
                $timeout(function () {
                    $scope.isSecondaryContactWTValid(document.getElementById("maskedWorkPhoneSecondary").value);
                }, 500);

            }

            $scope.setSecondaryContactMNCountryCode = function () {
                console.log('setCountryCodes with $scope.secondaryContactMNCountryCode : ' + $scope.secondaryContactMNCountryCode);
                $("#maskedMobilePhoneSecondary").intlTelInput("setCountry", $scope.secondaryContactMNCountryCode);
                $scope.secondaryContactMNFormat = document.getElementById("maskedMobilePhoneSecondary").placeholder;
                $timeout(function () {
                    $scope.isSecondaryContactMNValid(document.getElementById("maskedMobilePhoneSecondary").value);
                }, 500);

            }

            $scope.setSecondaryContactNExamples = function() {
                console.log('setSecondaryContactNExamples');
                $scope.secondaryContactWTFormat = document.getElementById("maskedWorkPhoneSecondary").placeholder;
                console.log($scope.secondaryContactWTFormat);
                $scope.secondaryContactMNFormat = document.getElementById("maskedMobilePhoneSecondary").placeholder;
            }

             $("#maskedWorkPhoneSecondary").keyup(function () {
                console.log("maskedWorkPhoneSecondary");
                if (document.getElementById('maskedWorkPhoneSecondary').value.length > 0) {
                    $scope.workTelephoneDirty = true;
                } else {
                    $scope.workTelephoneDirty = false;
                }
            });

            $("#maskedMobilePhoneSecondary").keyup(function () {
                if (document.getElementById('maskedMobilePhoneSecondary').value.length > 0) {
                    $scope.mobileNumberDirty = true;
                } else {
                    $scope.mobileNumberDirty = false;
                }
            });
            
            var getCurrentStep = function() {
                return $('#myWizard').wizard('selectedItem').step;
            };
            

            $scope.single = function() {
                //console.log('image : '+angular.toJson($scope.profilePic));
            }

            /**
             * This method is called during company update,host company update, client update and if address is changed and cancel is clicked in step 1.
             * This would be sent over HTTPS if the page is served via HTTPS
             */
            $scope.getAddressDetails = function(zipcode, type, callback) {

                if (angular.isDefined($scope.form.companydetailsform.zipcode) && $scope.form.companydetailsform.zipcode.$valid) {
                    $scope.validzipcode = true;
                }else{
                    $scope.validzipcode = false;
                }
                $scope.cities = [];
                $scope.states = [];
                $scope.counties = [];
                $scope.countries = [];
                var locations = [];
                
                if (type != 'copycomanyaddress') {
                    document.getElementById("copyCompanyAddress").checked = false;
                }
                $scope.copyCompanyAddress = false;
                
                var url = 'maps.googleapis.com/maps/api/geocode/json?address=' + zipcode + '&key=' + MESSAGECONSTANTS.GEOCODING.API_KEY;
                 if (angular.isDefined(zipcode) && !angular.equals(zipcode, '')) {
                $.getJSON({
                    url: 'https://maps.googleapis.com/maps/api/geocode/json?address=' + zipcode + '&key=' + MESSAGECONSTANTS.GEOCODING.API_KEY,
                    data: {
                        sensor: false,
                    },
                    success: function(data, textStatus) {
                        var createGeoLocation = function(addressdetails, postalname) {
                            var addressobj = {};
                            for (var p = 0; p < addressdetails.address_components.length; p++) {
                                for (var t = 0; t < addressdetails.address_components[p].types.length; t++) {
                                    switch (addressdetails.address_components[p].types[t]) {
                                        case 'country':
                                            addressobj.country = addressdetails.address_components[p].long_name;
                                            break;
                                        case 'administrative_area_level_2':
                                            addressobj.county = addressdetails.address_components[p].long_name;
                                            break;
                                        case 'administrative_area_level_1':
                                            addressobj.state = addressdetails.address_components[p].long_name;
                                            break;
                                        case 'locality':
                                            if (angular.isDefined(postalname) && postalname !== '') {
                                                addressobj.city = postalname;
                                            } else {
                                                addressobj.city = addressdetails.address_components[p].long_name;
                                            }
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }

                            if (angular.isUndefined(addressobj.city) || angular.isUndefined(addressobj.state) || angular.isUndefined(addressobj.country)) {
                                for (var p = 0; p < addressdetails.address_components.length; p++) {
                                    for (var t = 0; t < addressdetails.address_components[p].types.length; t++) {
                                        switch (addressdetails.address_components[p].types[t]) {
                                            case 'locality':
                                                if (angular.isUndefined(addressobj.state)) {
                                                    addressobj.state = addressdetails.address_components[p].long_name;
                                                }
                                                if (angular.isUndefined(addressobj.country)) {
                                                    addressobj.country = addressdetails.address_components[p].long_name;
                                                }
                                                break;
                                            case 'administrative_area_level_2':
                                                if (angular.isUndefined(addressobj.city)) {
                                                    if (angular.isDefined(postalname) && postalname !== '') {
                                                        addressobj.city = postalname;
                                                    } else {
                                                        addressobj.city = addressdetails.address_components[p].long_name;
                                                    }
                                                }
                                                if (angular.isUndefined(addressobj.state)) {
                                                    addressobj.state = addressdetails.address_components[p].long_name;
                                                }
                                                if (angular.isUndefined(addressobj.country)) {
                                                    addressobj.country = addressdetails.address_components[p].long_name;
                                                }

                                                break;
                                            case 'administrative_area_level_3':
                                                if (angular.isUndefined(addressobj.city)) {
                                                    if (angular.isDefined(postalname) && postalname !== '') {
                                                        addressobj.city = postalname;
                                                    } else {
                                                        addressobj.city = addressdetails.address_components[p].long_name;
                                                    }
                                                }
                                                break;
                                            case 'administrative_area_level_4':
                                                if (angular.isUndefined(addressobj.city)) {
                                                    if (angular.isDefined(postalname) && postalname !== '') {
                                                        addressobj.city = postalname;
                                                    } else {
                                                        addressobj.city = addressdetails.address_components[p].long_name;
                                                    }
                                                }
                                                break;
                                            case 'sublocality' || 'sublocality_level_1' || 'sublocality_level_5':
                                                if (angular.isDefined(postalname) && postalname !== '') {
                                                    addressobj.city = postalname;
                                                } else {
                                                    addressobj.city = addressdetails.address_components[p].long_name;
                                                }
                                                break;
                                            case 'administrative_area_level_1':
                                                if (angular.isUndefined(addressobj.city)) {
                                                    if (angular.isDefined(postalname) && postalname !== '') {
                                                        addressobj.city = postalname;
                                                    } else {
                                                        addressobj.city = addressdetails.address_components[p].long_name;
                                                    }

                                                    if (angular.isUndefined(addressobj.country)) {
                                                        addressobj.country = addressdetails.address_components[p].long_name;
                                                    }
                                                }
                                                break;
                                        }
                                    }
                                }
                            }
                            //console.log('returning : '+ angular.toJson(addressobj))
                            return addressobj;
                        }
                        var checkCountyExists = function(locations,company){
                        	for(var i = 0; i < locations.length; i++){
                        		if(angular.equals(locations[i].city,company.city) &&
                        		angular.isDefined(locations[i].county) && (locations.county!==null) &&
                        		angular.isDefined(company.county) &&
                        		(company.county!==null) && angular.equals(locations[i].county,company.county)){
                        			return true;
                        		}
                        	}
                        	return false;
                        }
                        if (data.status === 'OK') {
                             $scope.validzipcode = true;
                            for (var i = 0; i < data.results.length; i++) {
                                if (angular.isDefined(data.results[i].postcode_localities) && (data.results[i].postcode_localities.length > 0)) {
                                    for (var k = 0; k < data.results[i].postcode_localities.length; k++) {
                                       if((data.results[i].types[0] === 'postal_code')){
                                            console.log('entered zipcode is a postal code');
                                            var location = createGeoLocation(data.results[i], data.results[i].postcode_localities[k]);
                                            locations.push(angular.copy(location));
                                        }else{
                                            console.log('entered zipcode is not a postal code');
                                        }
                                    }
                                } else {
                                    //console.log('Address details fetched : ');
                                   if((data.results[i].types[0] === 'postal_code')){
                                        console.log('entered zipcode is a postal code');
                                        var location = createGeoLocation(data.results[i]);
                                        locations.push(angular.copy(location));
                                    }else{
                                        console.log('entered zipcode is not a postal code');
                                    }
                                }
                            }

                            $scope.temp = locations;
                            if(locations.length){
                                $scope.temp.city = locations[0].city;
                                $scope.temp.addressState = locations[0].state;
                                $scope.temp.county = (angular.isUndefined(locations[0].county))?null:locations[0].county;
                                $scope.temp.country = locations[0].country;
                                $scope.zipcodeCallCompleted = true;
                            }
                            if (type === 'copycomanyaddress' && !cancelClicked && locations.length) {
                                $scope.CompanyManagement.company.city = locations[0].city;
                                $scope.CompanyManagement.company.addressState = locations[0].state;
                                $scope.CompanyManagement.company.county = (angular.isUndefined(locations[0].county))?null:locations[0].county;
                                if(angular.isDefined(locations[0].county) && (locations[0].county!==null)){
                                	$scope.isCountyExists = true;
                                }else{
                                	$scope.isCountyExists = false;
                                }
                                $scope.CompanyManagement.company.country = locations[0].country;
                                $scope.validzipcode = true;
                                $scope.zipcodeCallCompleted = true;
                            } else if (type === 'zipchanged' && !cancelClicked && locations.length) {
                                $scope.CompanyManagement.company.city = locations[0].city;
                                $scope.CompanyManagement.company.addressState = locations[0].state;
                                $scope.CompanyManagement.company.county = (angular.isUndefined(locations[0].county))?null:locations[0].county;
                                if(angular.isDefined(locations[0].county) && (locations[0].county!==null)){
                                	$scope.isCountyExists = true;
                                }else{
                                	$scope.isCountyExists = false;
                                }
                                $scope.CompanyManagement.company.country = locations[0].country;
                                $scope.validzipcode = true;
                                $scope.zipcodeCallCompleted = true;
                            } else if (type === 'updateaddress'&& !cancelClicked && locations.length) {
                                $scope.zipcodeCallCompleted = true;
                            } else if (cancelClicked) {
                                locations = angular.copy($scope.temp);
                                $scope.geoLocations = angular.copy($scope.temp);;
                                $scope.geoLocationscopy = angular.copy($scope.copyOfGeoLocations);;
                                $scope.CompanyManagement.company.city = $scope.originalStepOne.city;
                                $scope.CompanyManagement.company.addressState = $scope.originalStepOne.addressState;
                                $scope.CompanyManagement.company.county = $scope.originalStepOne.county;
                                if(checkCountyExists(angular.copy(locations), angular.copy($scope.originalStepOne))){
                                	$scope.isCountyExists = true;
                                }else{
                                	$scope.isCountyExists = false;
                                }
                                $scope.CompanyManagement.company.country = $scope.originalStepOne.country;
                                $scope.validzipcode = true;
                                $scope.zipcodeCallCompleted = true;
                            } else if(locations.length){
                            	if(checkCountyExists(angular.copy(locations), angular.copy($scope.originalStepOne))){
                                	$scope.isCountyExists = true;
                                }else{
                                	$scope.isCountyExists = false;
                                }
								if (angular.isDefined($scope.originalStepTwo) && angular.isDefined($scope.originalStepTwo[0]) && angular.isDefined($scope.originalStepTwo[0].workPhone)) {
                                    $scope.CompanyManagement.contactList[0].workPhone = angular.copy($scope.originalStepTwo[0].workPhone);
                                    $scope.CompanyManagement.contactList[0].mobilePhone = angular.copy($scope.originalStepTwo[0].mobilePhone);
                                }
                                if (angular.isDefined($scope.originalStepTwo) && angular.isDefined($scope.originalStepTwo[1]) && angular.isDefined($scope.originalStepTwo[1].workPhone)) {
                                    $scope.CompanyManagement.contactList[1].workPhone = angular.copy($scope.originalStepTwo[1].workPhone);
                                    $scope.CompanyManagement.contactList[1].mobilePhone = angular.copy($scope.originalStepTwo[1].mobilePhone);
                                }
                                var elements = angular.element("#maskedMobilePhone");
                                angular.forEach(elements, function (element) {
                                    $compile(element)($scope);
                                });
                                $scope.zipcodeCallCompleted = true;
                            }
                            } else if (data.status === 'ZERO_RESULTS') {
                                $scope.validzipcode = false;
                                if(!cancelClicked){
                                    $scope.validzipcode = false;
                                    if (!$scope.invalidZipcode) {
                                        $scope.invalidZipcode = true;
                                    }
                                    locations = [];
                                    $scope.CompanyManagement.company.city = '';
                                    $scope.CompanyManagement.company.addressState = '';
                                    $scope.CompanyManagement.company.county = '';
                                    $scope.isCountyExists = false;
                                    $scope.CompanyManagement.company.country = '';
                                    $scope.zipcodeCallCompleted = true;
                                }else{
                                    locations = angular.copy($scope.copyOfGeoLocations);;
                                    $scope.geoLocations = angular.copy($scope.copyOfGeoLocations);;
                                    $scope.geoLocationscopy = angular.copy($scope.copyOfGeoLocations);;
                                    $scope.CompanyManagement.company.city = $scope.originalStepOne.city;
                                    $scope.CompanyManagement.company.addressState = $scope.originalStepOne.addressState;
                                    $scope.CompanyManagement.company.county = $scope.originalStepOne.county;
                                    if(checkCountyExists(angular.copy(locations), angular.copy($scope.originalStepOne))){
                                    	$scope.isCountyExists = true;
                                    }else{
                                    	$scope.isCountyExists = false;
                                    }
                                    $scope.CompanyManagement.company.country = $scope.originalStepOne.country;
                                    $scope.zipcodeCallCompleted = true;
                                }
                            }
                            $scope.geoLocations = locations;
                            $scope.geoLocationscopy = locations;
                            if(!$scope.geoLocations.length){
                            	$scope.validzipcode = false;
                            	if (!$scope.invalidZipcode) {
                                    $scope.invalidZipcode = true;
                                }
                            	$scope.CompanyManagement.company.city = '';
                                $scope.CompanyManagement.company.addressState = '';
                                $scope.CompanyManagement.company.county = '';
                                $scope.isCountyExists = false;
                                $scope.CompanyManagement.company.country = '';
                                $scope.zipcodeCallCompleted = true;
                            }
                            $scope.$apply();
                            if(typeof callback != "undefined"){
                            	return callback($scope.geoLocations);
                            }
                        },
                        error: function (data) {
                            console.log('Error: ' + angular.toJson(data));
                            $scope.invalidZipcode = true;
                            $scope.isCountyExists = false;
                            if(typeof callback != "undefined"){
                            	return callback('success');
                            }
                        }
                    });
                } else {
                    console.log('in else');
                    $scope.geoLocations = [];
                    $scope.isCountyExists = false;
                    if(typeof callback != "undefined"){
                    	return callback('success');
                    }
                }
            }


            /**
             * Get Address details.
             */
            $scope.getZipCodeDetails = function (zipcode, type) {
                console.log('zipcode in getZipCodeDetails : ' + zipcode);
                nextButtonClicked = false;
                saveButtonClicked = false;
                if ($scope.form.companydetailsform.zipcode.$valid) {
                    if (angular.isDefined(zipcode)) {
                        cancelClicked = false;
                    }
                    $scope.validzipcode = true;
                    $scope.zipcodeCalling = true;
                    $scope.zipcodeCallCompleted = false;
                    $scope.getAddressDetails(zipcode, type, function(response){
                        if(angular.isDefined($scope.SavedCompanyManagement.company.county) && ($scope.SavedCompanyManagement.company.county !== null)
                    			&& ($scope.CompanyManagement.company.zipcode === $scope.SavedCompanyManagement.company.zipcode)){
                			$scope.CompanyManagement.company.county = $scope.SavedCompanyManagement.company.county;
                			$scope.safeApply();
                        }
                    	if(saveButtonClicked){
                    		saveButtonTrigger();
                    	}else if(nextButtonClicked){
                    		nextButtonTrigger();
                    	}
                    });
                } else {
                    console.log('zipcode invalid');
                    //$scope.validzipcode = false;
                    $scope.cancelNotClicked = false;
                    saveButtonClicked = false;
                    nextButtonClicked = false;
                    $scope.cities = [];
                    $scope.states = [];
                    $scope.counties = [];
                    $scope.countries = [];
                    $scope.geoLocations = [];
                    $scope.geoLocationscopy = [];
                    $scope.CompanyManagement.company.city = '';
                    $scope.CompanyManagement.company.addressState = '';
                    $scope.CompanyManagement.company.county = '';
                    $scope.isCountyExists = false;
                    $scope.CompanyManagement.company.country = '';
                }
            }

               /**
             * Conditions to check whether its Client create or Client update. 
             */
            if (StorageService.get('clientOrBuCreationInProgress') !== null) {
                //Condition is satified if Client Creation is in progress.
                if ($rootScope.userDetails.company.companyType == 'Corporation') {
                    $scope.TypeOfCompany = 'BusinessUnit';
                } else if ($rootScope.userDetails.company.companyType == 'StaffingCompany') {
                    $scope.TypeOfCompany = 'Client';
                }

                $scope.ClientOrBuCreate = true;
                clientOrBuCreationInProgress = true;
                StorageService.remove('clientOrBuCreationInProgress');
            } else if (StorageService.get('clientOrBuToBeUpdated') !== null) {
                //Condition is satified if Client Updation is in progress.
                console.log('Update Client details: ' + angular.toJson(StorageService.get('clientOrBuToBeUpdated')));
                clientOrBuUpdateInProgress = true;
                $scope.TypeOfCompany = 'Client';
                $scope.ClientOrBuUpdate = true;
                $scope.CompanyManagement = StorageService.get('clientOrBuToBeUpdated');
                $scope.CompanyManagementReset = angular.copy($scope.CompanyManagement);
                console.log('$scope.CompanyManagementReset  '+angular.toJson($scope.CompanyManagementReset.fourDotFiveAdmin));
                $scope.SavedCompanyManagement.company = angular.copy($scope.CompanyManagement.company);
                $scope.SavedCompanyManagement.contactList = angular.copy($scope.CompanyManagement.contactList);
                //As its a draft there should not be exception as duplicate client while saving.
                $scope.SavedCompanyManagement.companyState = angular.copy($scope.CompanyManagement.companyState);
                console.log('client : ' + $scope.CompanyManagement);
                $scope.editCompanyName = $scope.CompanyManagement.company.name.substring(0, 57);
                $scope.editCompanyNameOnHover = $scope.CompanyManagement.company.name;
                if ($scope.CompanyManagement.company.name.length > 57) {
                    $scope.editCompanyName = $scope.editCompanyName + '...';
                }
                $scope.getAddressDetails($scope.CompanyManagement.company.zipcode, 'updateaddress');
                StorageService.remove('clientOrBuToBeUpdated');

                $scope.originalStepOne = angular.copy($scope.CompanyManagement.company);
                $scope.originalStepTwo = angular.copy($scope.CompanyManagement.contactList);
                if ($scope.CompanyManagement.contactList.length > 1) {
                    $scope.SecondaryContact = 'Remove';
                    $scope.isValidPrimaryMobileEntered = true;
                    $scope.isValidPrimaryWorkPhoneEntered = true;
                    $scope.isValidSecondaryMobileEntered = true;
                    $scope.isValidSecondaryWorkPhoneEntered = true;
                } else if ($scope.CompanyManagement.contactList.length == 1) {
                    $scope.isValidPrimaryMobileEntered = true;
                    $scope.isValidPrimaryWorkPhoneEntered = true;
                }
            }

            /**
             * This condition seperates whether the module is loaded
             * on account of Company view/update or Host Company view/update.
             * @access for create, view and update
             */
            if ((StorageService.get('companyDetails') != null || $rootScope.userDetails.roleScreenRef.role.name == 'CorporateAdmin' || $rootScope.userDetails.roleScreenRef.role.name == 'StaffingAdmin') && !clientOrBuCreationInProgress && !clientOrBuUpdateInProgress && StorageService.get('SelectedHostCompany') == null) {
                //This condition is satisfied when a company is selected from popup or role is CorporateAdmin or role is StaffingAdmin and user not in creation/updation of client and selected company is not host company.
                //view or update company access
                console.log('Details of Corporate company');
                if (StorageService.get('companyDetails') != null) {
                    if (angular.isDefined(StorageService.get('companyDetails').companyId)) {
                        getDetailsOfCompany = StorageService.get('companyDetails').companyId;
                    } else {
                        getDetailsOfCompany = StorageService.get('companyDetails');
                    }

                } else {
                    console.log('Rootscope details');
                    getDetailsOfCompany = $rootScope.userDetails.company.companyId;
                }

                genericService.getObjects(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.GET_COMPANY + '/' + getDetailsOfCompany).then(function(data) {
                    $scope.CompanyManagement = data;
                    $scope.CompanyManagementReset = angular.copy($scope.CompanyManagement);
                    if (data.contactList.length == 1) {
                        $scope.isValidPrimaryMobileEntered = true;
                        $scope.isValidPrimaryWorkPhoneEntered = true;
                    } else if (data.contactList.length == 2) {
                        $scope.isValidPrimaryMobileEntered = true;
                        $scope.isValidPrimaryWorkPhoneEntered = true;
                        $scope.isValidSecondaryMobileEntered = true;
                        $scope.isValidSecondaryWorkPhoneEntered = true;
                    }
                    console.log('isValidPrimaryMobileEntered '+$scope.isValidPrimaryMobileEntered);
                    $scope.getAddressDetails(data.company.zipcode, 'updateaddress');
                    $scope.originalStepOne = angular.copy($scope.CompanyManagement.company);
                    $scope.SavedCompanyManagement.company = angular.copy($scope.CompanyManagement.company);
                    $scope.SavedCompanyManagement.contactList = angular.copy($scope.CompanyManagement.contactList);
                    $scope.originalStepTwo = angular.copy($scope.CompanyManagement.contactList);
                    //Updating the SavedCompanyManagement object after getting company details.
                    $scope.originalStepThree = angular.copy($scope.CompanyManagement.fourDotFiveAdmin);
                    //Updating the SavedCompanyManagement object after getting company details.
                    $scope.SavedfourDotFiveAdmin = angular.copy($scope.CompanyManagement.fourDotFiveAdmin);

                    if ($scope.CompanyManagement.fourDotFiveAdmin != null) {
                        //This is satisfied when a company has 4dot5 admin assigned. 
                        oldfourdotfiveadminid = $scope.CompanyManagement.fourDotFiveAdmin.userId;
                        $scope.CompanyManagement.fourDotFiveAdmin.ticked = true;
                        $scope.originalStepThree.ticked = true;
                        $scope.fourDotFiveAdminsList = [];
                        genericService.getObjects(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.GET_4DOT5_ADMINS).then(function(data) {
                            if ($rootScope.userDetails.roleScreenRef.role.name == '4dot5SuperUser' || $rootScope.userDetails.roleScreenRef.role.name == '4dot5Admin') {
                                data.forEach(function(element) {
                                    if (element.userId == $scope.CompanyManagement.fourDotFiveAdmin.userId) {
                                        element.ticked = true;
                                    } else {
                                        element.ticked = false;
                                    }
                                    $scope.fourDotFiveAdminsList.push(element);
                                }, this);
                            } else {
                                console.log('Pushing: ');
                                $scope.fourDotFiveAdminsList.push($scope.CompanyManagement.fourDotFiveAdmin);
                            }
                        }, function(error) {
                        	if($rootScope.isOnline){
                            	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                            }
                        });
                    } else if ($rootScope.userDetails.roleScreenRef.role.name == '4dot5SuperUser' && $scope.CompanyManagement.fourDotFiveAdmin == null) {
                        //This is satisfied when a company has is saved as a draft company and hence 4dot5 admin is still not assigned. 
                        get4dot5admins();
                    }
                    if (data.contactList.length > 1) {
                        //This condition is satisfied when the selected company has Secondary Contact assigned and hence it has to be shown.
                        $scope.SecondaryContact = 'Remove';
                        $scope.isValidSecondaryWorkPhoneEntered = true;
                        $scope.isValidSecondaryMobileEntered = true;
                    }
                }, function(error) {
                	if($rootScope.isOnline){
                    	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    }
                });

                $scope.HostCompanySelected = false;
                $scope.CompanyDetailsUpdateMode = false;
                $scope.ShowCreateCompanyHeading = false;

                if ($rootScope.userDetails.roleScreenRef.role.name == '4dot5SuperUser' || $rootScope.userDetails.roleScreenRef.role.name == '4dot5Admin') {
                    //This condition is satisfied when user has access to edit the company details.
                    $scope.CompanyDetailsUpdateMode = true;
                    $scope.ShowCreateCompanyHeading = false;
                    StorageService.remove('wizardoperations');
                    angular.element('.access').attr({ 'readonly': false, 'disabled': false });
                } else {
                    //This condition is satisfied when user doesnot have access to edit the company details and hence all fields are disabled.
                    angular.element('.access').attr({ 'readonly': true, 'disabled': true });
                    angular.element('.hostaccess').attr({ 'readonly': true, 'disabled': true });
                }
                $scope.accessMode = angular.copy($stateParams.accessMode);
            } else if (StorageService.get('SelectedHostCompany') != null) {
                //This condition is satisfied when selected company is a host company.
                $scope.CompanyDetailsUpdateMode = false;
                $scope.HostCompanySelected = true;
                genericService.getObjects(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.GET_COMPANY + '/' + StorageService.get('SelectedHostCompany').companyId).then(function(data) {
                    $scope.CompanyManagement = data;
                    $scope.CompanyManagementReset = angular.copy($scope.CompanyManagement);
                    $scope.SavedCompanyManagement.company = angular.copy($scope.CompanyManagement.company);
                    $scope.SavedCompanyManagement.contactList = angular.copy($scope.CompanyManagement.contactList);
                    $scope.getAddressDetails(data.company.zipcode, 'updateaddress');
                    $scope.originalStepOne = angular.copy($scope.CompanyManagement.company);
                    $scope.originalStepTwo = angular.copy($scope.CompanyManagement.contactList);
                    $scope.originalStepFour = angular.copy($scope.CompanyManagement.licensePreferences);

                    //Updating the SavedCompanyManagement object after getting company details.
                    $scope.SavedAssessmentWorkFlow = angular.copy($scope.CompanyManagement.licensePreferences);
                    $scope.AssessmentWorkFlow = $scope.CompanyManagement.licensePreferences;
                    console.log('$scope.AssessmentWorkFlow  '+angular.toJson($scope.AssessmentWorkFlow));
                    $scope.accessMode = angular.copy($stateParams.accessMode);
                    if (data.contactList.length > 1) {
                        //This condition is satisfied when the selected company has Secondary Contact assigned and hence it has to be shown.
                        $scope.SecondaryContact = 'Remove';
                    }
                    if ($rootScope.userDetails.roleScreenRef.role.name == '4dot5SuperUser' || $rootScope.userDetails.roleScreenRef.role.name == '4dot5Admin') {
                        $scope.CompanyDetailsUpdateMode = true;
                        $scope.ShowCreateCompanyHeading = false;
                        StorageService.remove('wizardoperations');
                        angular.element('.access').attr({ 'readonly': false, 'disabled': false });
                        angular.element('.hostaccess').attr({ 'readonly': true, 'disabled': true });
                    } else {
                        angular.element('.access').attr({ 'readonly': true, 'disabled': true });
                        angular.element('.hostaccess').attr({ 'readonly': true, 'disabled': true });
                    }
                }, function(error) {
                	if($rootScope.isOnline){
                    	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    }
                });
            }

            /**
             * This method is used to delete company details
             */
            $scope.deleteCompanyDetails = function() {
                var loadingModal = $('#deleteconfirm');
                genericService.deleteObject(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.DELETE_COMPANY + '/' + $rootScope.userDetails.company.companyId).then(function(data) {
                    loadingModal.modal("hide");
                    $('body').removeClass('modal-open');
                    $('body').css('padding-right', '0');
                    $('.modal-backdrop').remove();
                    StorageService.remove('companyDetails');
                    StorageService.remove('SelectedHostCompany');
                    $rootScope.userDetails.company.companyId = null;
                    $rootScope.userDetails.company.companyName = null;
                    $rootScope.userDetails.company.companyType = null;
                    $rootScope.userDetails.company.companySelected = false;
                    $rootScope.companyDeletedShowPopup = true;
                    StorageService.setCookie('userDetails', angular.copy($rootScope.userDetails), { 'expires': new Date(Date.now() + $rootScope.tokenvalidity) });
                    companyDeleted = true;
                    $state.go('missioncontrol', null, { reload: true });
                }, function(error) {
                	if($rootScope.isOnline){
                    	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    }
                });
            }

            var uncheckSelected4dot5Admin = function (selectedAdmin) {
                for (var i = 0; i < $scope.fourDotFiveAdminsList.length; i++) {
                    if ($scope.fourDotFiveAdminsList[i].userId === selectedAdmin.userId) {
                        $scope.fourDotFiveAdminsList[i].ticked = false;
                    }
                }
            }
            /**
             * This method is called to get list of 4dot5 Admins companyId
             */
            var get4dot5admins = function() {
                genericService.getObjects(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.GET_4DOT5_ADMINS).then(function(data) {
                    $scope.fourDotFiveAdminsList = data;
                }, function(error) {
                	if($rootScope.isOnline){
                    	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    }
                });
            }

            /**
             * This method will have a track of which forward recipient is selected
             */
            $scope.fourDotFiveAdminChosen = function () {
               if (angular.isDefined($scope.CompanyManagement.fourDotFiveAdmin) && !angular.equals($scope.CompanyManagement.fourDotFiveAdmin, null) && angular.isDefined($scope.selectedFourDotFiveAdmin[0])) {
                    if (angular.equals($scope.CompanyManagement.fourDotFiveAdmin.userId,$scope.selectedFourDotFiveAdmin[0].userId)) {
                    	uncheckSelected4dot5Admin($scope.selectedFourDotFiveAdmin[0]);
                        $scope.CompanyManagement.fourDotFiveAdmin = {};
                        $scope.form.company4dot5admindetailsform.$setDirty();
                    } else {
                        $scope.CompanyManagement.fourDotFiveAdmin = angular.copy($scope.selectedFourDotFiveAdmin[0]);
                        $scope.form.company4dot5admindetailsform.$setDirty();
                    }
                } else {
                    $scope.CompanyManagement.fourDotFiveAdmin = angular.copy($scope.selectedFourDotFiveAdmin[0]);
                    $scope.CompanyManagementReset = angular.copy($scope.CompanyManagement);
                    $scope.form.company4dot5admindetailsform.$setDirty();
                }
            }

            function updatedAssessementFlowChanges(updatedLicencePrefs) {
                for (var index = 0; index < updatedLicencePrefs.length; index++) {
                    console.log('Object: ' + updatedLicencePrefs[index]);
                    if (!(updatedLicencePrefs[index].licensePrefDetailsList[0].disabled == $scope.AssessmentWorkFlow[index].licensePrefDetailsList[0].disabled)) {
                        console.log('There was change in: ' + updatedAssessementFlowChanges[index].name);
                        $scope.AssessmentWorkFlow[index].disabled = updatedLicencePrefs[index].disabled;
                    }
                }
            }

            /**
            * This method is called to get Assesement workflow steps.
            */
            function getAssessmentWorkFlow(companyId) {
                $scope.laststep = true;
                if (!(!$scope.CompanyDetailsUpdateMode && !$scope.ClientOrBuUpdate) || ($scope.CompanyDetailsUpdateMode && !$scope.laststep) || ($scope.ClientOrBuUpdate && !$scope.laststep)) {
                    $scope.dataLastLabel = 'Complete';
                }
                if (StorageService.get('companyDetails') != null && newlyCreatedCompanyId == null) {
                    if (angular.isDefined(StorageService.get('companyDetails').companyId)) {
                        newlyCreatedCompanyId = StorageService.get('companyDetails').companyId;
                    } else {
                        newlyCreatedCompanyId = StorageService.get('companyDetails');
                    }

                }
                if (clientOrBuCreationInProgress) {
                    //This condition is satisfied when client or bu creation is in progress.
                    console.log('Get assessement flow details for client during client creation.');
                    genericService.getObjects(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.GET_ASSESSMENT_WORK_FLOW_FOR_CLIENT_BU + '' + $rootScope.userDetails.company.companyId + '/' + newlyCreatedCompanyId).then(function(data) {
                        //               console.log('GET_ASSESSMENT_WORK_FLOW received is : ' + angular.toJson(data));
                        $scope.originalStepFour = angular.copy(data);
                        $scope.preferences = angular.copy(data);
                        stepFourChanged = !(angular.equals($scope.originalStepFour, $scope.preferences));
                        $scope.licensePreferencesFetched = true;
                        if (!$scope.errorInSavingPrefsCheck) {
                            StorageService.set('wizardoperations', true);
                            $scope.stepTrigger('nextbtnclick');
                        }
                    }, function(error) {
                    	if($rootScope.isOnline){
                        	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                        }
                    });
                } else if ($scope.CompanyDetailsUpdateMode) {
                    //This condition is satisfied when company update is in progress.
                    console.log('Get assessement flow details for company during company updation.');
                    genericService.getObjects(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.GET_ASSESSMENT_WORK_FLOW + '' + $rootScope.userDetails.company.companyType + '/' + $rootScope.userDetails.company.companyId).then(function(data) {
                        $scope.originalStepFour = angular.copy(data);
                        $scope.SavedAssessmentWorkFlow = angular.copy(data);
                        $scope.preferences = angular.copy($scope.SavedAssessmentWorkFlow);
                        $scope.licensePreferencesFetched = true;
                        stepFourChanged = !(angular.equals($scope.originalStepFour, $scope.preferences));
                        if (!$scope.errorInSavingPrefsCheck) {
                            StorageService.set('wizardoperations', true);
                            $scope.stepTrigger('nextbtnclick');
                        }

                    }, function(error) {
                    	if($rootScope.isOnline){
                        	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                        }
                    });
                } else if (!clientOrBuUpdateInProgress) {
                    console.log('1: ' + $rootScope.userDetails.company.companyId);//Creation of company in progress
                    if (newlyCreatedCompanyId == null) {
                        newlyCreatedCompanyId == $rootScope.userDetails.company.companyId;
                    }
                    genericService.getObjects(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.GET_ASSESSMENT_WORK_FLOW + '' + $rootScope.userDetails.company.companyType + '/' + $rootScope.userDetails.company.companyId).then(function(data) {
                        //               console.log('GET_ASSESSMENT_WORK_FLOW received is : ' + angular.toJson(data));
                        $scope.originalStepFour = angular.copy(data);
                        console.log('Assess: ' + $scope.preferences);
                        $scope.SavedAssessmentWorkFlow = angular.copy(data);
                        $scope.preferences = angular.copy($scope.SavedAssessmentWorkFlow);
                        $scope.licensePreferencesFetched = true;
                        stepFourChanged = !(angular.equals($scope.originalStepFour, $scope.preferences));
                        if (!$scope.errorInSavingPrefsCheck) {
                            StorageService.set('wizardoperations', true);
                            $scope.stepTrigger('nextbtnclick');
                        }
                    }, function(error) {
                    	if($rootScope.isOnline){
                        	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                        }
                    });
                } else {
                    console.log('Getting assessement flow details for clientOrBu')
                    if (clientOrBuUpdateInProgress) {
                        //This is satisfied when client or bu update is in progress
                        console.log('Getting assessement flow details for client Or Bu in update mode.')
                        genericService.getObjects(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.GET_ASSESSMENT_WORK_FLOW_FOR_CLIENT_BU + '' + $rootScope.userDetails.company.companyId + '/' + $scope.CompanyManagement.company.id).then(function(data) {
                            $scope.originalStepFour = angular.copy(data);
                            $scope.preferences = angular.copy(data);
                            $scope.licensePreferencesFetched = true;
                            stepFourChanged = !(angular.equals($scope.originalStepFour, $scope.preferences));
                            StorageService.set('wizardoperations', true);
                            $scope.stepTrigger('nextbtnclick');
                        }, function(error) {
                        	if($rootScope.isOnline){
                            	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                            }
                        });
                    } else {
                        console.log('2');
                        genericService.getObjects(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.GET_ASSESSMENT_WORK_FLOW + '' + $rootScope.userDetails.company.companyType + '/' + companyId).then(function(data) {
                            $scope.originalStepFour = angular.copy(data);
                            $scope.preferences = angular.copy(data);
                            $scope.licensePreferencesFetched = true;
                            stepFourChanged = !(angular.equals($scope.originalStepFour, $scope.preferences));
                            console.log(' $scope.CompanyManagement.licensePreferences ' + angular.toJson($scope.CompanyManagement.licensePreferences))
                            StorageService.set('wizardoperations', true);
                            $scope.stepTrigger('nextbtnclick');
                        }, function(error) {
                        	if($rootScope.isOnline){
                            	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                            }
                        });
                    }

                }

            }

            /**
             * This method is called to get list of Company Types.
             */
            $scope.getCompanyTypes = function() {
                companyTypes = ['Corporation', 'StaffingCompany'];
                companyTypes.forEach(function(element) {
                    //Puts in a space before an uppercase letter $scope.CompanyTypes.push(element.replace(/([A-Z])/g, ' $1').trim());
                    $scope.CompanyTypes.push(element);
                }, this);
                if ($rootScope.userDetails.company.companyId == null) {
                    console.log('Getting 4dot5 admins');
                    get4dot5admins();
                } else {
                    console.log('Not Getting 4dot5 admins');
                }
            }


            /**
             * This method works for triggerng
             * button clicks and resetting timeout
             * and remaining variables to normal
             * @access for create, view and update
             */
            $scope.stepTrigger = function(target) {
                if (target === 'nextbtnclick') {
                    var nextbtnclick = $timeout(function() {
                        angular.element('.btn-next').triggerHandler('click');
                    }, 0).then(function() {
                        StorageService.remove('wizardoperations');
                        triggercount = 0;
                        $scope.formsubmitted = false;
                        $timeout.cancel(nextbtnclick);
                    });
                } else if (target === 'cancelflow') {
                    var steponeclick = $timeout(function() {
                        angular.element('#1').trigger('click');
                    }, 500).then(function() {
                        triggercount = 0;
                        $scope.formsubmitted = false;
                        StorageService.remove('wizardoperations');
                        $timeout.cancel(steponeclick);
                    });
                }
            };

            /**
             * Function to save Step 1 Company Details.
             */
            function saveCompanyDetails() {
                if (StorageService.get('clientOrBuCreationInProgress') !== null) {
                    if ($rootScope.userDetails.company.companyType == 'Corporation') {
                        $scope.TypeOfCompany = 'BusinessUnit';
                        $scope.CompanyManagement.company.companyType = 'BusinessUnit';
                    } else if ($rootScope.userDetails.company.companyType == 'StaffingCompany') {
                        $scope.TypeOfCompany = 'Client';
                        $scope.CompanyManagement.company.companyType = 'Client';
                    }
                }
                
                if ((StorageService.get('companyDetails') == null || $scope.CompanyDetailsUpdateMode || clientOrBuCreationInProgress || clientOrBuUpdateInProgress) && stepOneChanged) {
                	savedCompanyManagementCopy.company = angular.copy($scope.SavedCompanyManagement.company);
                	$scope.SavedCompanyManagement.company = $scope.CompanyManagement.company;
                    console.log('$scope.SavedCompanyManagement.company '+angular.toJson($scope.SavedCompanyManagement.company));
                    genericService.addObject(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.SAVE_COMPANY + '/' + loggedInUserDetails.company.companyId, $scope.SavedCompanyManagement).then(function(data) {
                        nextButtonFlag++;
                        $scope.originalStepOne = angular.copy(data.company);
                        $scope.SavedCompanyManagement.company = angular.copy(data.company);
                        console.log('Response: ' + angular.toJson('$scope.SavedCompanyManagement: ' + angular.toJson($scope.SavedCompanyManagement)));
                        $scope.CompanyManagement.company = data.company;
                        $scope.CompanyManagement.company.name = data.company.name;
                        $scope.CompanyManagement.company.id = data.company.id;
                        $scope.CompanyManagement.companyState = data.companyState;
                        $scope.SavedCompanyManagement.company.id = data.company.id;
                        $scope.SavedCompanyManagement.companyState = data.companyState;
                        newlyCreatedCompanyId = data.company.id;
                        $rootScope.newlyCreatedCompanyId = newlyCreatedCompanyId;

                        if ($scope.TypeOfCompany != 'Client' && $scope.TypeOfCompany != 'BusinessUnit') {
                            $rootScope.userDetails.company.companyId = newlyCreatedCompanyId;
                            $rootScope.userDetails.company.companyName = data.company.name;
                            $rootScope.userDetails.company.companyType = data.company.companyType;
                            $rootScope.userDetails.company.companyState = data.companyState;
                            $rootScope.userDetails.company.companySelected = true;
                            StorageService.setCookie('userDetails', angular.copy($rootScope.userDetails), { 'expires': new Date(Date.now() + $rootScope.tokenvalidity) });
                        }
                        if ($scope.TypeOfCompany == 'Client' || $scope.TypeOfCompany == 'BusinessUnit') {
                            console.log('Client details saved, call assignClientToStaffingCompany function');
                            assignClientToStaffingCompany('clickonclientnext');
                        }
                        
                        if (clientOrBuUpdateInProgress) {
                            console.log('Step 1 details changed, so api call made.Updated client details : ');
                            $scope.editCompanyName = data.company.name.substring(0, 55);
                             $scope.editCompanyNameOnHover = data.company.name;
                              if (data.company.name.length > 57) {
                                $scope.editCompanyName = $scope.editCompanyName + '...';
                            }else{
                            	$scope.editCompanyName = $scope.editCompanyName;
                            }
                        }
                        if (!($scope.TypeOfCompany == 'Client') && !($scope.TypeOfCompany == 'BusinessUnit')) {
                            console.log('Saved company Step 1 details successfully.');
                            companyStepOneSaved = true;
                            StorageService.set('wizardoperations', true);
                            $scope.stepTrigger('nextbtnclick');
                        }

                    }, function (error) {
                    	triggercount = 0;
                    	 if(error.statusCode === "409"){
                    		$scope.errorOcurred = true;
                         	if (angular.isDefined($scope.CompanyManagement.companyState)) {
                             	if(error.developerMsg === "101"){
                             		$scope.CompanyManagement.company.companyShortName =$scope.originalStepOne.companyShortName;
                             	}else if(error.developerMsg === "102"){
                             		$scope.CompanyManagement.company.name = $scope.originalStepOne.name;
                             	}else if(error.developerMsg === "100"){
                             		$scope.CompanyManagement.company.companyShortName =$scope.originalStepOne.companyShortName;
                             		$scope.CompanyManagement.company.name = $scope.originalStepOne.name;
                             	}else{
                             		$scope.CompanyManagement.company.companyShortName =$scope.originalStepOne.companyShortName;
                             		$scope.CompanyManagement.company.name = $scope.originalStepOne.name;
                             	}
                         	}else{
                             	if(error.developerMsg === "101"){
                             		$scope.CompanyManagement.company.companyShortName ='';
                             	}else if(error.developerMsg === "102"){
                             		$scope.CompanyManagement.company.name = '';
                             	}else if(error.developerMsg === "100"){
                             		$scope.CompanyManagement.company.companyShortName ='';
                             		$scope.CompanyManagement.company.name = '';
                             	}else{
                             		$scope.CompanyManagement.company.companyShortName ='';
                             		$scope.CompanyManagement.company.name = '';
                             	}
                         	}
                         }
                    	 
                    	 if($rootScope.isOnline){
                    		 alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    	 }
                    	 $scope.SavedCompanyManagement.company = angular.copy(savedCompanyManagementCopy.company);
                    });
                } else if (!stepOneChanged) {
                    console.log('Step one is not changed, so no api call made.');
                    companyStepOneSaved = true;
                    StorageService.set('wizardoperations', true);
                    triggercount = 0;
                }
            }

            /**
             * Function to save Step 2 details(Company Contact Person Details)
             */
            function saveContactDetails() {
                if (newlyCreatedCompanyId == null && clientOrBuUpdateInProgress) {
                    newlyCreatedCompanyId = $scope.CompanyManagement.company.id;
                }
                console.log('In saveContactDetails function: Save for companyid: ' + newlyCreatedCompanyId);
                if (newlyCreatedCompanyId == null && clientOrBuUpdateInProgress) {
                    newlyCreatedCompanyId = $scope.CompanyManagement.company.id;
                }
                if ((StorageService.get('companyDetails') == null || $scope.CompanyDetailsUpdateMode || clientOrBuCreationInProgress || clientOrBuUpdateInProgress) && stepTwoChanged) {
                    //Building the SavedCompanyManagement object before saving. 
                	savedCompanyManagementCopy.contactList = angular.copy($scope.SavedCompanyManagement.contactList);
                	$scope.SavedCompanyManagement.contactList = angular.copy($scope.CompanyManagement.contactList);
                    console.log('$scope.SavedCompanyManagement.contactList '+angular.toJson($scope.SavedCompanyManagement.contactList));
                    genericService.addObject(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.SAVE_COMPANY + '/' + newlyCreatedCompanyId, $scope.SavedCompanyManagement).then(function(data) {
                        $scope.originalStepTwo = angular.copy(data.contactList);
                        $scope.SavedCompanyManagement.contactList = angular.copy(data.contactList);
                        $scope.CompanyManagement.contactList = angular.copy(data.contactList);
                        nextButtonFlag++;
                        
                        if (clientOrBuUpdateInProgress) {
                            console.log('Step 2 details changed, so api call made.Updated client contact details : ');
                            $scope.laststep = true;
                            // alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.UPDATESUCCESS, 'success');
                        }
                        if (clientOrBuCreationInProgress) {
                            console.log('Step 2 details changed, so api call made.Saved client contact details : ');
                            $scope.laststep = true;
                            // alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.CLIENT+" '<b><i class='wrap-word'>"+$scope.SavedCompanyManagement.company.name+"</i></b>' "+MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.CREATEDRAFTSUCCESS, 'success')
                        }
                        if ($rootScope.userDetails.company.companyType == 'Host') {
                            $scope.laststep = true;
                            $scope.dataLastLabel = 'Complete';
                        }

                        if ($scope.TypeOfCompany == 'Client' || $scope.TypeOfCompany == 'BusinessUnit' || clientOrBuUpdateInProgress || clientOrBuCreationInProgress || StorageService.get('SelectedHostCompany') != null) {
                            console.log('Company ID: ' + $rootScope.userDetails.company.companyId);
                            console.log('call to function getAssessmentWorkFlow: ');
                            if (!$scope.licensePreferencesFetched) {
                                getAssessmentWorkFlow($rootScope.userDetails.company.companyId);
                            } else {
                                StorageService.set('wizardoperations', true);
                                $scope.stepTrigger('nextbtnclick');
                            }
                        } else {
                            console.log('Saved Step 2 details successfully.');
                            companyStepTwoSaved = true;
                            StorageService.set('wizardoperations', true);
                            $scope.stepTrigger('nextbtnclick');
                        }
                    }, function(error) {
                    	 if($rootScope.isOnline){
                    		 alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    	 }
                    	 console.log('now assigning original obj to transaction and saved objects');
                    	 $scope.SavedCompanyManagement.contactList = angular.copy(savedCompanyManagementCopy.contactList);
                    });
                } else if (!stepTwoChanged) {
                    console.log('Step 2 is not changed, so no api call made.');
                    if (StorageService.get('SelectedHostCompany') != null) {
                        $scope.laststep = true;
                        $scope.dataLastLabel = 'Complete';
                    }
                    if ($scope.TypeOfCompany == 'Client' || $scope.TypeOfCompany == 'BusinessUnit' || $rootScope.userDetails.company.companyType == 'Host') {
                        $scope.laststep = true;
                        if (!$scope.licensePreferencesFetched) {
                            getAssessmentWorkFlow($rootScope.userDetails.company.companyId);
                        } else {
                            StorageService.set('wizardoperations', true);
                            $scope.stepTrigger('nextbtnclick');
                        }
                    } else {
                        companyStepTwoSaved = true;
                        triggercount = 0;
                        StorageService.set('wizardoperations', true);
                    }
                }
            }

             /**
            * Function to save Step 3 details(Company 4Dot5 Admin Details)
            */
            function saveFourDotFiveAdminDetails() {
                console.log('In function saveFourDotFiveAdminDetails: ');
                if ((StorageService.get('companyDetails') == null || $scope.CompanyDetailsUpdateMode) && stepThreeChanged) {
                	
                    //Building the SavedCompanyManagement object before saving.
                	savedfourDotFiveAdminCopy = angular.copy($scope.SavedfourDotFiveAdmin);
                    $scope.SavedfourDotFiveAdmin = angular.copy(fourDotFiveCompanyAdmin);
                    genericService.addObject(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.UPDATE_COMPANY_FOR_FOURDOTFIVE_ADMIN + '/' + $scope.SavedfourDotFiveAdmin[0].userId + '/' + $rootScope.userDetails.company.companyId + '?oldfourdotfiveadmin=' + oldfourdotfiveadminid, $scope.SavedfourDotFiveAdmin).then(function (data) {
                        //               console.log('Success: ' + angular.toJson(data));
                        nextButtonFlag++;
                        
                        $scope.originalStepThree = angular.copy(data.fourDotFiveAdmin);
                        $scope.SavedfourDotFiveAdmin = angular.copy(data.fourDotFiveAdmin);
                        if ($scope.CompanyDetailsUpdateMode == true) {
                            $scope.CompanyManagement.fourDotFiveAdmin = angular.copy(data.fourDotFiveAdmin);
                            $scope.CompanyManagementReset.fourDotFiveAdmin = angular.copy(data.fourDotFiveAdmin);
                            console.log('Step 3 is changed, so api call made.Four dot five admin updated.');
                        }
                        companyStepThreeSaved = true;
                        if (!$scope.licensePreferencesFetched) {
                            getAssessmentWorkFlow(loggedInUserDetails.company.companyId);
                        } else {
                            StorageService.set('wizardoperations', true);
                            $scope.stepTrigger('nextbtnclick');
                        }
                        $scope.laststep = true;
                    }, function (error) {
                        console.log('error : ' + angular.toJson(error));
                        alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.CREATEERROR, 'danger');
                    });
                } else if (!stepThreeChanged) {
                    $scope.laststep = true;
                    if (!$scope.licensePreferencesFetched) {
                        getAssessmentWorkFlow(loggedInUserDetails.company.companyId);
                    } else {
                        StorageService.set('wizardoperations', true);
                        $scope.stepTrigger('nextbtnclick');
                    }
                }
            }

            /**
             * 
             */

            function assignClientToStaffingCompany(clickfromclientsaveornext) {
                //This condition satisfy when a company from popup is selected and user tries to edit a client/bu for selected company
                console.log('assignClientToStaffingCompany function called.');
                if (clientOrBuUpdateInProgress) {
                    newlyCreatedCompanyId = $scope.CompanyManagement.company.id;
                    console.log('Taking updated client id');
                }
                if (StorageService.get('companyDetails') != null) {
                    if (angular.isDefined(StorageService.get('companyDetails').companyId)) {
                        assignClient2StaffingCompany = StorageService.get('companyDetails').companyId;
                    } else {
                        assignClient2StaffingCompany = StorageService.get('companyDetails');
                    }

                    console.log('StorageService.get != null: ' + angular.toJson(assignClient2StaffingCompany));
                } else {
                    assignClient2StaffingCompany = loggedInUserDetails.company.companyId;
                    console.log('StorageService.get != null: ' + assignClient2StaffingCompany);
                }
                genericService.addObject(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.ASSIGN_CLIENT_TO_STAFFING_COMPANY + '/' + assignClient2StaffingCompany + '/' + newlyCreatedCompanyId).then(function(data) {
                    console.log('Client/BU assigned to staffing/CorporateAdmin company.');
                    if (clientOrBuUpdateInProgress) {
                        alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.BU.BU + " '<b><i class='wrap-word'>" + $scope.SavedCompanyManagement.company.name + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.UPDATESUCCESS, 'success');
                    } else if (clientOrBuCreationInProgress && clickfromclientsaveornext == 'clickonclientsave') {
                        alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.BU.BU + " '<b><i class='wrap-word'>" + $scope.SavedCompanyManagement.company.name + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.BU.CREATEDRAFTSUCCESS, 'success');
                    }
                    companyStepOneSaved = true;
                    if (!(clickfromclientsaveornext == 'clickonclientsave')) {
                        StorageService.set('wizardoperations', true);
                        $scope.stepTrigger('nextbtnclick');
                    }
                }, function(error) {
                	if ((error.status === 500) && $rootScope.isOnline) {
              			alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    }else{
                    	if (clientOrBuUpdateInProgress) {
                    	}  else if (clientOrBuCreationInProgress && clickfromclientsaveornext == 'clickonclientsave') {
                    		alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.BU.BU + " '<b><i class='wrap-word'>" + $scope.SavedCompanyManagement.company.name + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.BU.CREATEDRAFTSUCCESS, 'success');
                    	}
                    	companyStepOneSaved = true;
                    	if (!(clickfromclientsaveornext == 'clickonclientsave')) {
                    		StorageService.set('wizardoperations', true);
                    		$scope.stepTrigger('nextbtnclick');
                    	}
                    }
                    
                });
            }

            /**
             * Change state of Staffing company to 'Active'.
             */
            function changeCompanyState() {
                genericService.addObject(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.CHANGE_COMPANYSTATE + '/' + newlyCreatedCompanyId, $scope.CompanyManagement).then(function(data) {
                    $rootScope.userDetails.company.companyState = data.companyState;
                    StorageService.setCookie('userDetails', angular.copy($rootScope.userDetails), { 'expires': new Date(Date.now() + $rootScope.tokenvalidity) });
                }, function(error) {
                	if($rootScope.isOnline){
                		alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                	}
                });
            }
            /**
            * Function to save Step 4 details(Company assessement Flow Details)
            */
            function saveAssessementFlowDetails() {
                var clientOrCompanyLicensePrefsUrl;
                //This condition satisfy when a company from popup is selected and user tries to create a client/bu for selected company
                if (StorageService.get('companyDetails') != null && newlyCreatedCompanyId == null && !clientOrBuUpdateInProgress) {
                    if (angular.isDefined(StorageService.get('companyDetails').companyId)) {
                        newlyCreatedCompanyId = StorageService.get('companyDetails').companyId;
                    } else {
                        newlyCreatedCompanyId = StorageService.get('companyDetails');
                    }

                }
                //This condition satisfy when a company from popup is selected and user tries to edit a client/bu for selected company
                if (StorageService.get('companyDetails') == null && newlyCreatedCompanyId == null && $scope.CompanyManagement.company.companyType != 'Host') {
                   if(StorageService.get('companyToBeUpdated') == null){
                        newlyCreatedCompanyId = $rootScope.userDetails.company.companyId;
                    }else{
                        newlyCreatedCompanyId = StorageService.get('companyToBeUpdated').company.id;
                    }
                }
                if ($scope.CompanyManagement.company.companyType == 'Host') {
                    newlyCreatedCompanyId = $rootScope.userDetails.company.companyId;
                    console.log('Host: ' + newlyCreatedCompanyId);
                }
                //This condition will satisfy if editing a client/bu
                if (clientOrBuUpdateInProgress) {
                    newlyCreatedCompanyId = $scope.CompanyManagement.company.id;
                    console.log('Update Client set licence preferences: ' + newlyCreatedCompanyId);
                }

                console.log('Save for companyid: ' + newlyCreatedCompanyId);
                if ((StorageService.get('companyDetails') == null || $scope.CompanyDetailsUpdateMode || clientOrBuCreationInProgress || clientOrBuUpdateInProgress) && stepFourChanged) {
                    if (clientOrBuCreationInProgress || clientOrBuUpdateInProgress) {
                        clientOrCompanyLicensePrefsUrl = COMPANYMANAGEMENTCONSTANTS.CONTROLLER.SET_LICENCE_PREFERENCES_FOR_CLIENT_BU + '/' + $rootScope.userDetails.company.companyId + '/' + newlyCreatedCompanyId;
                    } else {
                        clientOrCompanyLicensePrefsUrl = COMPANYMANAGEMENTCONSTANTS.CONTROLLER.SET_LICENCE_PREFERENCES + '/' + $scope.CompanyManagement.company.companyType + '/' + newlyCreatedCompanyId;
                    }
                    //Building the SavedCompanyManagement object before saving. 
                    savedAssessmentWorkFlowCopy = angular.copy($scope.SavedAssessmentWorkFlow);
                    $scope.SavedAssessmentWorkFlow = angular.copy($scope.preferences);
                    tempObj = angular.copy($scope.originalStepFour);

                    genericService.addObject(clientOrCompanyLicensePrefsUrl, $scope.SavedAssessmentWorkFlow).then(function(data) {
                        $scope.CompanyManagement.licensePreferences = angular.copy(data.licensePreferences);
                        $scope.originalStepFour = angular.copy(data.licensePreferences);
                        $scope.SavedAssessmentWorkFlow = angular.copy(data.licensePreferences);
                        
                        if ($scope.CompanyDetailsUpdateMode == true && stepFourChanged) {
                            console.log('Updated step 4 Company Licence Preferences details successfully.')
                            if ($rootScope.userDetails.roleScreenRef.role.name != 'CorporateAdmin' && $rootScope.userDetails.roleScreenRef.role.name != 'StaffingAdmin') {
                                alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.COMPANY + " '<b><i class='wrap-word'>" + delimitedCompanyOrClientName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.UPDATESUCCESS, 'success');
                            }
                        } else if (!clientOrBuUpdateInProgress && $scope.TypeOfCompany == 'Client') {
                            console.log('Saved step 3 Client/BU Licence Preferences details successfully.')
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.BU.BU + " '<b><i class='wrap-word'>" + $scope.SavedCompanyManagement.company.name + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.CREATESUCCESS, 'success');
                        } else if(angular.equals($rootScope.userDetails.roleScreenRef.role.name,'CorporateAdmin') || angular.equals($rootScope.userDetails.roleScreenRef.role.name,'StaffingAdmin') ){
                            console.log('Company updated successfully');
                        } else if (StorageService.get('companyDetails') == null && !$scope.CompanyDetailsUpdateMode) {
                            console.log('Saved step 3 Client/BU Licence Preferences details successfully.')
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.BU.BU + " '<b><i class='wrap-word'>" + $scope.SavedCompanyManagement.company.name + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.CREATESUCCESS, 'success');
                        }

                        companyStepFourSaved = true;
                        $scope.laststep = true;
                        StorageService.set('wizardoperations', true);
                        $scope.stepTrigger('nextbtnclick');

                        if ($scope.CompanyManagement.companyState == 'Draft' || $scope.CompanyManagement.companyState == null) {
                            console.log('Saved Company Object: ' + angular.toJson($scope.SavedCompanyManagement));
                            changeCompanyState();
                        }
                        if ($scope.TypeOfCompany == 'Client' || $scope.TypeOfCompany == 'BusinessUnit') {
                            $scope.lastStepCompleted = true;
                            console.log('Moving out to clientmanagement(List of clients): ' + $scope.lastStepCompleted)
                            if (!$scope.ClientOrBuUpdate) {
                            	$timeout(function() {
                            		alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.BU.BU + " '<b><i class='wrap-word'>" + $scope.SavedCompanyManagement.company.name + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.CREATESUCCESS, 'success');
                            		$state.go('missioncontrol.clientmanagement', null, { reload: false });
                            	}, 2000); //Time out is given to solve draft issue of company.
                            }else{
                                console.log('nextButtonFlag '+nextButtonFlag+' '+saveButtonFlag);
                            	if(!angular.equals(tempObj,$scope.SavedAssessmentWorkFlow) || (nextButtonFlag > 0) || (saveButtonFlag > 0)){
                            		alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.BU.BU + " '<b><i class='wrap-word'>" + $scope.SavedCompanyManagement.company.name + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.UPDATESUCCESS, 'success');
                                    $timeout(function() {
                                        $state.go('missioncontrol.clientmanagement', null, { reload: false });
                                    }, 3000); //Time out is given to solve draft issue of company.
                            	}else{
                            		alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.NOTHING_TO_UPDATED, 'success');
                                    $timeout(function() {
                                        $state.go('missioncontrol.clientmanagement', null, { reload: false });
                                    }, 3000); //Time out is given to solve draft issue of company.
                            	}
                            }
                        } else if ($scope.CompanyManagement.company.companyType == 'Host') {
                            StorageService.set('companyDetails', newlyCreatedCompanyId);
                            $scope.lastStepCompleted = true;
                            $timeout(function () {
                                $state.go('missioncontrol.dashboardsuperusermodule', { companyObject: 'companyObject' }, { reload: false });
                             }, 4500);
                        } else {
                            StorageService.set('companyDetails', newlyCreatedCompanyId);
                            $scope.lastStepCompleted = true;
                            $timeout(function () {
                                $state.go('missioncontrol.dashboard', { companyObject: 'companyObject' }, { reload: false });
                             }, 4000);
                        }

                    }, function(error) {
                    	if($rootScope.isOnline){
 	                   		 alertsAndNotificationsService.showBannerMessage(error.message,'danger');
 	                   	}
                    	
                        if (error.statusCode == "409") {
                            $scope.errorOcurred = true;
                            getAssessmentWorkFlow($rootScope.userDetails.company.companyId)
                        }else{
                        	$scope.SavedAssessmentWorkFlow = angular.copy(savedAssessmentWorkFlowCopy);
                        }
                    });
                } else if (!stepFourChanged) {
                    console.log('Step 4 is not changed, so no api call made.');
                    if ($scope.CompanyManagement.companyState == 'Draft' || $scope.CompanyManagement.companyState == null) {
                        console.log('Saved Company Object: ' + angular.toJson($scope.SavedCompanyManagement));
                        changeCompanyState();
                    }
                    companyStepFourSaved = true;
                    $scope.laststep = true;
                    StorageService.set('wizardoperations', true);
                    if ($scope.TypeOfCompany === 'Client' || $scope.TypeOfCompany === 'BusinessUnit') {
                        $scope.lastStepCompleted = true;
                        console.log('Moving out to clientmanagement: ' + $scope.lastStepCompleted)
                       if (!$scope.ClientOrBuUpdate) {
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.BU.BU + " '<b><i class='wrap-word'>" + $scope.SavedCompanyManagement.company.name + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.CREATESUCCESS, 'success')
                            $timeout(function () {
                                $state.go('missioncontrol.clientmanagement', null, { reload: false });
                            }, 4000);
                        } else {
                            console.log("Update else");
                            //alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.BU.BU + " '<b><i class='wrap-word'>" + $scope.SavedCompanyManagement.company.name + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.UPDATESUCCESS, 'success')
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.NOTHING_TO_UPDATED, 'success');
                            $timeout(function () {
                                $state.go('missioncontrol.clientmanagement', null, { reload: false });
                            }, 4000);
                        }
                    } else if ($scope.CompanyManagement.company.companyType == 'Host') {
                        StorageService.set('companyDetails', newlyCreatedCompanyId);
                        $scope.lastStepCompleted = true;
                        $timeout(function () {
                            $state.go('missioncontrol.dashboardsuperusermodule', { companyObject: 'companyObject' }, { reload: false });
                        }, 4000);
                    } else {
                        StorageService.set('companyDetails', newlyCreatedCompanyId);
                        $scope.lastStepCompleted = true;
                         console.log('company before redirecting...' + $scope.CompanyDetailsUpdateMode);
                         if ($scope.CompanyDetailsUpdateMode) {
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.COMPANY + " '<b><i class='wrap-word'>" + delimitedCompanyOrClientName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.UPDATESUCCESS, 'success')
                            $timeout(function () {
                                $state.go('missioncontrol.dashboard', { companyObject: 'companyObject' }, { reload: false });
                            }, 4000);
                        } else if (!angular.isDefined($scope.CompanyDetailsUpdateMode)) {
                            console.log('Creation of company(Staffing/Corporate)');
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.COMPANY + " '<b><i class='wrap-word'>" + delimitedCompanyOrClientName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.CREATESUCCESS, 'success')
                            $timeout(function () {
                                $state.go('missioncontrol.dashboard', { companyObject: 'companyObject' }, { reload: false });
                            }, 4000);
                        }
                    }
                }else {
                    companyStepFourSaved = true;
                    $scope.laststep = true;
                    StorageService.set('wizardoperations', true);
                    console.log('in else');
                    $state.go('missioncontrol.dashboard', { companyObject: 'companyObject' }, { reload: false });
                }
            }

             $scope.saveStepsDataAfterTimeout = function(Company, ContactList, FourDotFiveAdmin){
                console.log('$timeout 1000');
                 $timeout( function(){ $scope.saveStepsData(Company, ContactList, FourDotFiveAdmin); }, 1000);
            }

            $scope.saveStepsData = function(Company, ContactList, FourDotFiveAdmin) {
                nextButtonClicked = false;
                saveButtonClicked = true;
                var currentStep = getCurrentStep();
                if (currentStep == 3 && ($scope.TypeOfCompany == 'Client' || $scope.TypeOfCompany == 'BusinessUnit' || StorageService.get('SelectedHostCompany') != null)) {
                    currentStep = 4;
                }
                console.log('$scope.TypeOfCompany  '+$scope.TypeOfCompany);
                valid = false;
                switch (currentStep) {
                    case 1:
                        stepOneChanged = !(angular.equals($scope.originalStepOne, $scope.CompanyManagement.company));//Checks if any changes.
                        if($scope.CompanyManagement.company.address2 != null){
                        	isAdressValid = !angular.equals($scope.CompanyManagement.company.address1.toUpperCase(),$scope.CompanyManagement.company.address2.toUpperCase()) && ($scope.CompanyManagement.company.address1 != $scope.CompanyManagement.company.address2);
                        	console.log('if  '+isAdressValid);
                        }else{
                        	console.log('else');
                        	isAdressValid = $scope.CompanyManagement.company.address1 != $scope.CompanyManagement.company.address2;
                        }
                        valid = $scope.form.companydetailsform.$valid && isAdressValid && $scope.validzipcode &&  angular.isDefined($scope.validzipcode) && (!$scope.zipcodeCalling || ($scope.zipcodeCalling && $scope.zipcodeCallCompleted));//Checks validations.;
                        console.log('validations are proper: ' + valid+'  '+$scope.form.companydetailsform.$valid + ' '+$scope.form.companydetailsform.zipcode.$valid);
                        console.log('Step 1 is changed: ' + stepOneChanged+' '+$scope.validzipcode+' CompanyManagement.company.zipcode '+$scope.CompanyManagement.company.zipcode);
                        console.log('id  '+loggedInUserDetails.company.companyId+'while saving obj  '+angular.toJson($scope.SavedCompanyManagement));
                        if (stepOneChanged && valid) {
                            //Building the SavedCompanyManagement object before saving.
                           if (!angular.isDefined($scope.CompanyManagement.company.companyType)) {
                                if ($rootScope.userDetails.company.companyType == 'Corporation') {
                                    $scope.CompanyManagement.company.companyType = 'BusinessUnit';
                                } else if ($rootScope.userDetails.company.companyType == 'StaffingCompany') {
                                    $scope.CompanyManagement.company.companyType = 'Client';
                                }
                            }
                            console.log('Saving: ' + angular.toJson($scope.SavedCompanyManagement.company));
                            savedCompanyManagementCopy.company = angular.copy($scope.SavedCompanyManagement.company);
                            $scope.SavedCompanyManagement.company = angular.copy($scope.CompanyManagement.company);
                            genericService.addObject(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.SAVE_COMPANY + '/' + loggedInUserDetails.company.companyId, $scope.SavedCompanyManagement).then(function(data) {
                                $scope.originalStepOne = angular.copy(data.company);
                                //Updating the SavedCompanyManagement object after saving.
                                $scope.SavedCompanyManagement.company = angular.copy(data.company);
                                saveButtonClicked = false;
                                saveButtonFlag++;
                                
                                console.log('Response: ' + angular.toJson($scope.SavedCompanyManagement));

                                $scope.CompanyManagement.company = data.company;
                                $scope.CompanyManagement.company.name = data.company.name;
                                $scope.CompanyManagement.companyState = data.companyState;
                                $scope.CompanyManagement.companyState = data.companyState;
                                $scope.SavedCompanyManagement.company.id = data.company.id;
                                $scope.SavedCompanyManagement.companyState = data.companyState;
                                newlyCreatedCompanyId = data.company.id;
                                if ($scope.TypeOfCompany != 'Client' && $scope.TypeOfCompany != 'BusinessUnit') {
                                    $rootScope.userDetails.company.companyId = data.company.id;
                                    $rootScope.userDetails.company.companyName = data.company.name;
                                    $rootScope.userDetails.company.companyType = data.company.companyType;
                                    $rootScope.userDetails.company.companyState = data.companyState;
                                    $rootScope.userDetails.company.companySelected = true;
                                    StorageService.setCookie('userDetails', angular.copy($rootScope.userDetails), { 'expires': new Date(Date.now() + $rootScope.tokenvalidity) });
                                }
                                companyStepOneSaved = true;
                                if (($scope.TypeOfCompany == 'Client' || $scope.TypeOfCompany == 'BusinessUnit') && clientOrBuUpdateInProgress) {
                                    console.log('Updated client details')
                                    $scope.editCompanyName = data.company.name.substring(0, 57);
                                    $scope.editCompanyNameOnHover = data.company.name;
                                    if (data.company.name.length > 57) {
                                        $scope.editCompanyName = $scope.editCompanyName + '...';
                                    }else{
                                    	$scope.editCompanyName = $scope.editCompanyName;
                                    }
                                    alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.BU.BU + " '<b><i class='wrap-word'>" + $scope.SavedCompanyManagement.company.name + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.UPDATESUCCESS, 'success');
                                }
                                if (($scope.TypeOfCompany == 'Client' || $scope.TypeOfCompany == 'BusinessUnit') && clientOrBuUpdateInProgress) {
                                    console.log('Updated BU details')
                                    $scope.editCompanyName = data.company.name.substring(0, 57);
                                    $scope.editCompanyNameOnHover = data.company.name;
                                    if (data.company.name.length > 57) {
                                        $scope.editCompanyName = $scope.editCompanyName + '...';
                                    }else{
                                    	$scope.editCompanyName = $scope.editCompanyName;
                                    }
                                    
                                    alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.BU.BU + " '<b><i class='wrap-word'>" + $scope.SavedCompanyManagement.company.name + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.UPDATESUCCESS, 'success');
                                }
                                if (($scope.TypeOfCompany == 'Client' || $scope.TypeOfCompany == 'BusinessUnit') && clientOrBuCreationInProgress) {
                                    console.log('Assigning client or bu to StaffingCompany function call.');
                                    assignClientToStaffingCompany('clickonclientsave');
                                    //alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.CREATESUCCESS, 'success')
                                }
                                 if (!($scope.CompanyDetailsUpdateMode) && $scope.TypeOfCompany == 'Company') {
                                    alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.COMPANY + " '<b><i class='wrap-word'>" + delimitedCompanyOrClientName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.COMPANYDRAFTSUCCESS, 'success')
                                }
                                if ($scope.CompanyDetailsUpdateMode) {
                                    alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.COMPANY + " '<b><i class='wrap-word'>" + delimitedCompanyOrClientName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.UPDATESUCCESS, 'success')
                                }
                               
                            }, function (error) {
                                //Duplicates not allowed.
                                if(error.statusCode === "409"){
                                	$scope.errorOcurred = true;
                                	if (angular.isDefined($scope.CompanyManagement.companyState)) {
	                                	if(error.developerMsg === "101"){
	                                		$scope.CompanyManagement.company.companyShortName =$scope.originalStepOne.companyShortName;
	                                	}else if(error.developerMsg === "102"){
	                                		$scope.CompanyManagement.company.name = $scope.originalStepOne.name;
	                                	}else if(error.developerMsg === "100"){
	                                		$scope.CompanyManagement.company.companyShortName =$scope.originalStepOne.companyShortName;
	                                		$scope.CompanyManagement.company.name = $scope.originalStepOne.name;
	                                	}else{
	                                		$scope.CompanyManagement.company.companyShortName =$scope.originalStepOne.companyShortName;
	                                		$scope.CompanyManagement.company.name = $scope.originalStepOne.name;
                                            console.log('$scope.CompanyManagement.company  '+angular.toJson($scope.CompanyManagement.company));
	                                	}
                                	}else{
	                                	if(error.developerMsg === "101"){
	                                		$scope.CompanyManagement.company.companyShortName ='';
	                                	}else if(error.developerMsg === "102"){
	                                		$scope.CompanyManagement.company.name = '';
	                                	}else if(error.developerMsg === "100"){
	                                		$scope.CompanyManagement.company.companyShortName ='';
	                                		$scope.CompanyManagement.company.name = '';
	                                	}else{
	                                		$scope.CompanyManagement.company.companyShortName ='';
	                                		$scope.CompanyManagement.company.name = '';
	                                	}
                                	}
                                }
                                if($rootScope.isOnline){
	                       		 	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
	                       	 	}
                                $scope.SavedCompanyManagement.company = angular.copy(savedCompanyManagementCopy.company);
                            });
                        } else if (!valid) {
                            console.log('else if .....');
                             $scope.step1formsubmitted = true;
                            triggercount = 0;
                        } else if (valid && (!stepOneChanged)) {
                            console.log('else if ..');
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.NOTHING_TO_SAVE, 'success');
                            saveButtonClicked = false;
                        }
                        break;
                    case 2: console.log('Save clicked in step 2 : ');
                        stepTwoChanged = !(angular.equals($scope.originalStepTwo, $scope.CompanyManagement.contactList));
                        if ($scope.SecondaryContact == 'Remove') {
                        	console.log('in if');
                            $scope.validateSecondaryDetails = true;
                            console.log('isValidPrimaryMobileEntered '+$scope.isValidPrimaryMobileEntered);
                            console.log($scope.form.companydetailsform.$valid+'  '+$scope.form.companycontactpersondetailsform.$valid+' '+$scope.form.companysecondarycontactpersondetailsform.$valid+' '+$scope.isValidPrimaryMobileEntered+' '+$scope.isValidSecondaryMobileEntered+' '+$scope.isValidSecondaryWorkPhoneEntered)
                            valid = $scope.form.companydetailsform.$valid && $scope.form.companycontactpersondetailsform.$valid && $scope.form.companysecondarycontactpersondetailsform.$valid
                                && $scope.isValidPrimaryMobileEntered && $scope.isValidPrimaryWorkPhoneEntered && $scope.isValidSecondaryMobileEntered && $scope.isValidSecondaryWorkPhoneEntered;
                        } else {
                        	console.log('in else');
                        	console.log('isValidPrimaryMobileEntered '+$scope.isValidPrimaryMobileEntered);
                            valid = $scope.form.companydetailsform.$valid && $scope.form.companycontactpersondetailsform.$valid && $scope.isValidPrimaryMobileEntered && $scope.isValidPrimaryWorkPhoneEntered;
                            console.log($scope.form.companydetailsform.$valid+' '+$scope.form.companycontactpersondetailsform.$valid+$scope.isValidPrimaryMobileEntered+' '+$scope.isValidPrimaryWorkPhoneEntered);
                        }
                        console.log('validations are proper: ' + valid);
                        if (stepTwoChanged && valid) {
                            if ($scope.CompanyManagement.contactList.length == 1) {
                                $scope.CompanyManagement.contactList[0].primary = false;
                            }
                            if ($scope.SecondaryContact == 'Remove') {
                                if ($scope.CompanyManagement.contactList.length > 1) {
                                    $scope.CompanyManagement.contactList[0].primary = false;
                                    $scope.CompanyManagement.contactList[1].primary = false;
                                }
                            }

                            if (newlyCreatedCompanyId == null && clientOrBuUpdateInProgress) {
                                newlyCreatedCompanyId = $scope.CompanyManagement.company.id;
                            }
                            console.log('Save for companyid: ' + newlyCreatedCompanyId);

                            //Building the SavedCompanyManagement object before saving. 
                            savedCompanyManagementCopy.contactList = angular.copy($scope.SavedCompanyManagement.contactList);
                            $scope.SavedCompanyManagement.contactList = angular.copy($scope.CompanyManagement.contactList);
                            console.log('Saving: ' + angular.toJson($scope.SavedCompanyManagement.contactList));
                            genericService.addObject(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.SAVE_COMPANY + '/' + newlyCreatedCompanyId, $scope.SavedCompanyManagement).then(function(data) {
                                console.log('Success: ' + angular.toJson(data.contactList));
                                $scope.originalStepTwo = angular.copy(data.contactList);
                                //Updating the SavedCompanyManagement object after saving.
                                saveButtonClicked = false;
                                saveButtonFlag++;
                                
                                $scope.SavedCompanyManagement.contactList = angular.copy(data.contactList);
                                $scope.CompanyManagement.contactList = angular.copy(data.contactList);
                                console.log('Success: ' + angular.toJson($scope.originalStepTwo));
                                if ($scope.TypeOfCompany == 'BusinessUnit' && clientOrBuUpdateInProgress) {
                                    alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.BU.BU + " '<b><i class='wrap-word'>" + $scope.SavedCompanyManagement.company.name + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.UPDATESUCCESS, 'success');
                                }
                                if (($scope.TypeOfCompany == 'Client' || $scope.TypeOfCompany == 'BusinessUnit') && clientOrBuUpdateInProgress) {
                                    alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.BU.BU + " '<b><i class='wrap-word'>" + $scope.SavedCompanyManagement.company.name + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.UPDATESUCCESS, 'success');
                                }
                               
                                if ($scope.TypeOfCompany == 'BusinessUnit' && clientOrBuCreationInProgress) {
                                    console.log('Assigning client to StaffingCompany');
                                    alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.BU.BU + " '<b><i class='wrap-word'>" + $scope.SavedCompanyManagement.company.name + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.BU.CREATEDRAFTSUCCESS, 'success');
                                }
                                
                                companyStepTwoSaved = true;
                            }, function(error) {
                            	 if($rootScope.isOnline){
	                           		 alertsAndNotificationsService.showBannerMessage(error.message,'danger');
	                           	 }
	                           	 console.log('now assigning original obj to transaction and saved objects');
	                           	 $scope.SavedCompanyManagement.contactList = angular.copy(savedCompanyManagementCopy.contactList);
                            });
                        } else if (!valid) {
                            $scope.step2formsubmitted = true;
                            triggercount = 0;
                        } else if (valid && (!stepTwoChanged)) {
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.NOTHING_TO_SAVE, 'success');
                            saveButtonClicked = false;
                        }
                        break;
                    case 3: console.log('Step 3 clicked on save : ');
                        // oldCheckedAdmin = null;
                        if (angular.isDefined($scope.originalStepThree) && $scope.originalStepThree != null && $scope.selectedFourDotFiveAdmin.length > 0) {
                            console.log('Equals of: $scope.originalStepThree.userId' + $scope.originalStepThree.userId + ' and: ' + $scope.selectedFourDotFiveAdmin[0].userId);
                            stepThreeChanged = !(angular.equals($scope.originalStepThree.userId, $scope.selectedFourDotFiveAdmin[0].userId));
                            oldfourdotfiveadminid = $scope.originalStepThree.userId;
                        } else if (!(angular.isDefined($scope.originalStepThree))) {
                            //This is satisfied in create company.
                            stepThreeChanged = true;
                        }

                        console.log('Step 3 is changed: ' + stepThreeChanged);
                        if ($scope.SecondaryContact == 'Remove') {
                            console.log('Secondary contact present');
                            valid = $scope.form.companydetailsform.$valid && $scope.form.companycontactpersondetailsform.$valid && $scope.form.companysecondarycontactpersondetailsform.$valid && $scope.selectedFourDotFiveAdmin.length > 0;
                        } else {
                            valid = $scope.form.companydetailsform.$valid && $scope.form.companycontactpersondetailsform.$valid && $scope.selectedFourDotFiveAdmin.length > 0;
                            console.log('Secondary contact absent: ' + valid);
                        }
                        console.log('validations are proper: ' + valid);
                        if (stepThreeChanged && valid) {
                            if (!angular.isDefined(fourDotFiveCompanyAdmin)) {
                                fourDotFiveCompanyAdmin = $scope.selectedFourDotFiveAdmin;
                            }
                            //Building the SavedCompanyManagement object before saving. 
                            savedfourDotFiveAdminCopy = angular.copy($scope.SavedfourDotFiveAdmin);
                            $scope.SavedfourDotFiveAdmin = angular.copy($scope.selectedFourDotFiveAdmin);
                            genericService.addObject(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.UPDATE_COMPANY_FOR_FOURDOTFIVE_ADMIN + '/' + $scope.SavedfourDotFiveAdmin[0].userId + '/' + $rootScope.userDetails.company.companyId + '?oldfourdotfiveadmin=' + oldfourdotfiveadminid, $scope.SavedfourDotFiveAdmin).then(function(data) {
                                $scope.originalStepThree = angular.copy(data.fourDotFiveAdmin);
                                $scope.SavedfourDotFiveAdmin = angular.copy(data.fourDotFiveAdmin);
                                saveButtonClicked = false;
                                saveButtonFlag++;
                                $scope.CompanyManagement.fourDotFiveAdmin = angular.copy(data.fourDotFiveAdmin);
                                $scope.CompanyManagementReset.fourDotFiveAdmin = angular.copy(data.fourDotFiveAdmin);
                                if ((StorageService.get('companyDetails') != null || $scope.CompanyDetailsUpdateMode)) {
                                    alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.COMPANY + " '<b><i class='wrap-word'>" + delimitedCompanyOrClientName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.UPDATESUCCESS, 'success')
                                } else if (!($scope.CompanyDetailsUpdateMode)) {
                                    alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.COMPANY + " '<b><i class='wrap-word'>" + delimitedCompanyOrClientName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.COMPANYDRAFTSUCCESS, 'success')
                                }
                                companyStepThreeSaved = true;
                            }, function(error) {
                            	 if($rootScope.isOnline){
	                           		 alertsAndNotificationsService.showBannerMessage(error.message,'danger');
	                           	 }
	                           	 console.log('now assigning original obj to transaction and saved objects');
	                           	 $scope.SavedfourDotFiveAdmin = angular.copy(savedfourDotFiveAdminCopy);
                            });
                        } else if (!valid) {
                            console.log('Step 3 mandatory fileds not filled.');
                            $scope.step3formsubmitted = true;
                            triggercount = 0;
                        } else if (valid && (!stepThreeChanged)) {
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.NOTHING_TO_SAVE, 'success');
                             saveButtonClicked = false;
                        }
                        break;
                    case 4: console.log('Step 4 clicked on save :');
                        stepFourChanged = !(angular.equals($scope.originalStepFour, $scope.preferences));
                        if ($scope.TypeOfCompany == 'Client' || StorageService.get('SelectedHostCompany') != null || $scope.TypeOfCompany == 'BusinessUnit') {
                            if ($scope.SecondaryContact == 'Remove') {
                                valid = $scope.form.companydetailsform.$valid && $scope.form.companycontactpersondetailsform.$valid && $scope.form.companysecondarycontactpersondetailsform.$valid;
                            } else {
                                valid = $scope.form.companydetailsform.$valid && $scope.form.companycontactpersondetailsform.$valid;
                            }
                        } else {
                            if ($scope.SecondaryContact == 'Remove') {
                                valid = $scope.form.companydetailsform.$valid && $scope.form.companycontactpersondetailsform.$valid && $scope.form.companysecondarycontactpersondetailsform.$valid && $scope.selectedFourDotFiveAdmin.length > 0;
                            } else {
                                valid = $scope.form.companydetailsform.$valid && $scope.form.companycontactpersondetailsform.$valid && $scope.selectedFourDotFiveAdmin.length > 0;
                            }
                        }
                        if (StorageService.get('SelectedHostCompany') != null) {
                            newlyCreatedCompanyId = $rootScope.userDetails.company.companyId;
                        }
                        //This condition will satisfy if editing a client/bu
                        if (clientOrBuUpdateInProgress) {
                            newlyCreatedCompanyId = $scope.CompanyManagement.company.id;
                            console.log('Update Client set licence preferences: ' + newlyCreatedCompanyId);
                        }
                        console.log('validations are proper: ' + valid);
                        if (valid && stepFourChanged) {
                            //Building the SavedCompanyManagement object before saving.  

                        	savedAssessmentWorkFlowCopy = angular.copy($scope.SavedAssessmentWorkFlow);
                        	$scope.SavedAssessmentWorkFlow = angular.copy($scope.preferences);
                            console.log('Saving :' + angular.toJson($scope.SavedAssessmentWorkFlow));
                            var clientOrCompany;
                            if ($scope.TypeOfCompany == 'Client') {
                                clientOrCompany = COMPANYMANAGEMENTCONSTANTS.CONTROLLER.SET_LICENCE_PREFERENCES_FOR_CLIENT_BU + '/' + $rootScope.userDetails.company.companyId + '/' + newlyCreatedCompanyId;
                            } else {
                                clientOrCompany = COMPANYMANAGEMENTCONSTANTS.CONTROLLER.SET_LICENCE_PREFERENCES + '/' + $rootScope.userDetails.company.companyType + '/' + newlyCreatedCompanyId;
                            }
                            console.log('Url is: ' + clientOrCompany);
                            genericService.addObject(clientOrCompany, $scope.SavedAssessmentWorkFlow).then(function(data) {
                                //                     console.log('Success: ' + angular.toJson(data));
                                $scope.CompanyManagement.licensePreferences = angular.copy(data.licensePreferences);
                                $scope.originalStepFour = angular.copy(data.licensePreferences);
                                //Updating SavedCompanyManagement object after saving.
                                $scope.SavedAssessmentWorkFlow = angular.copy(data.licensePreferences);
                                console.log('equal '+angular.toJson($scope.originalStepFour)+' \n @@@@ '+angular.toJson($scope.AssessmentWorkFlow)+'  '+angular.equals($scope.originalStepFour, $scope.AssessmentWorkFlow));
                                if (clientOrBuUpdateInProgress && !(angular.equals($scope.originalStepFour, $scope.AssessmentWorkFlow))) {
                                    console.log('Working now no msg');
                                    alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.BU.BU + " '<b><i class='wrap-word'>" + $scope.SavedCompanyManagement.company.name + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.UPDATESUCCESS, 'success');
                                    // $state.go('missioncontrol.clientmanagement', null, { reload: false });
                                } else if ((StorageService.get('companyDetails') != null || StorageService.get('SelectedHostCompany') != null) && $scope.CompanyDetailsUpdateMode) {
                                     if ($rootScope.userDetails.roleScreenRef.role.name != 'CorporateAdmin' && $rootScope.userDetails.roleScreenRef.role.name != 'StaffingAdmin') {
                                        alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.COMPANY + " '<b><i class='wrap-word'>" + $scope.SavedCompanyManagement.company.name + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.UPDATESUCCESS, 'success');
                                    }
                                } else if (clientOrBuCreationInProgress ) {
                                    alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.BU.BU + " '<b><i class='wrap-word'>" + $scope.SavedCompanyManagement.company.name + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.BU.CREATEDRAFTSUCCESS, 'success');
                                } 
                                companyStepFourSaved = true;
                                saveButtonClicked = false;
                                saveButtonFlag++;
                            }, function(error) {
                            	if($rootScope.isOnline){
	       	                   		 alertsAndNotificationsService.showBannerMessage(error.message,'danger');
	       	                   	}
                                if (error.statusCode == "409") {
                                    $scope.errorOcurred = true;
                                    $scope.errorInSavingPrefsCheck = true;
                                    getAssessmentWorkFlow($rootScope.userDetails.company.companyId);
                                }
                            });
                        } else if (!valid) {
                            $scope.step4formsubmitted = true;
                            triggercount = 0;
                        } else if (valid && (!stepFourChanged)) {
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.NOTHING_TO_SAVE, 'success');
                            saveButtonClicked = false;
                        }
                        break;
                }
            }


            function updatedLicensePrefsOnSave() {
                genericService.getObjects(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.GET_ASSESSMENT_WORK_FLOW + '' + $rootScope.userDetails.company.companyType + '/' + $rootScope.userDetails.company.companyId).then(function(data) {
                    $scope.SavedAssessmentWorkFlow = angular.copy(data);
                    $scope.preferences = angular.copy($scope.SavedAssessmentWorkFlow);
                    $scope.licensePreferencesFetched = true;
                    stepFourChanged = !(angular.equals($scope.originalStepFour, $scope.preferences));
                }, function(error) {
                	if($rootScope.isOnline){
                    	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    }
                });
            }

             $scope.manageStepDataAfterTimeout = function(Company, ContactList, FourDotFiveAdmin){
                console.log('$timeout 1000');
                 $timeout( function(){ $scope.manageStepData(Company, ContactList, FourDotFiveAdmin); }, 1500);
            }

            /** Function to control wizard flow with validations
             * 
             */
            $scope.manageStepData = function(Company, ContactList, FourDotFiveAdmin) {
                console.log('Trigger value: ' + triggercount);
                if (triggercount === 0) {
                    triggercount = angular.copy(triggercount + 1);
                    nextButtonClicked = true;
                    saveButtonClicked = false;
                    $scope.formsubmitted = true;
                    StorageService.remove('wizardoperations');
                    valid = false;
                    var currentStep = getCurrentStep();
                    console.log('In manageStepData function : saving details of step: ' + currentStep);
                    if (currentStep == 3 && ($scope.TypeOfCompany == 'Client' || StorageService.get('SelectedHostCompany') != null || $scope.TypeOfCompany == 'BusinessUnit')) {
                        currentStep = 4;
                    }
                    switch (currentStep) {
                        case 1:
                            stepOneChanged = !(angular.equals($scope.originalStepOne, $scope.CompanyManagement.company));//Checks if any changes.
                            console.log('Step 1 is changed: ' + stepOneChanged+$scope.CompanyManagement.company.address2);
                            if($scope.CompanyManagement.company.address2 != null){
                            	isAdressValid = !angular.equals($scope.CompanyManagement.company.address1.toUpperCase(),$scope.CompanyManagement.company.address2.toUpperCase()) && ($scope.CompanyManagement.company.address1 != $scope.CompanyManagement.company.address2);
                            	console.log('if  '+isAdressValid);
                            }else{
                            	console.log('else');
                            	isAdressValid = $scope.CompanyManagement.company.address1 != $scope.CompanyManagement.company.address2;
                            }
                            console.log('isAdressValid  '+isAdressValid);
                            valid = $scope.form.companydetailsform.$valid && $scope.validzipcode && isAdressValid && angular.isDefined($scope.validzipcode) && (!$scope.zipcodeCalling || ($scope.zipcodeCalling && $scope.zipcodeCallCompleted));//Checks validations.
                            console.log('$scope.validzipcode '+$scope.validzipcode+' '+$scope.form.companydetailsform.$valid+'  '+(!$scope.zipcodeCalling || ($scope.zipcodeCalling && $scope.zipcodeCallCompleted)));
                            console.log('valid  '+valid);
                            if (valid) {
                                if (!angular.isDefined($scope.CompanyManagement.company.companyType)) {
                                    if ($rootScope.userDetails.company.companyType == 'Corporation') {
                                        $scope.CompanyManagement.company.companyType = 'BusinessUnit';
                                    } else if ($rootScope.userDetails.company.companyType == 'StaffingCompany') {
                                        $scope.CompanyManagement.company.companyType = 'Client';
                                    }
                                }
                                nextButtonClicked = false;
                                saveCompanyDetails();	
                            } else {
                                console.log('Just move no api call')
                                $scope.step1formsubmitted = true;
                                triggercount = 0;
                            }
                            if($scope.SavedCompanyManagement.company.companyType === "Host"){
                            	$scope.isValidPrimaryWorkPhoneEntered = true;
                            	$scope.isValidPrimaryMobileEntered = true;
                            	$scope.isValidSecondaryWorkPhoneEntered = true;
                            	$scope.isValidSecondaryMobileEntered = true;
                            }
                            break;
                        case 2: console.log('Next clicked from step 2 moving to step 3 : ');
                            stepTwoChanged = !(angular.equals($scope.originalStepTwo, $scope.CompanyManagement.contactList));
                            console.log('Step 2 is changed: ' + stepTwoChanged);
                            if ($scope.CompanyManagement.contactList.length == 1) {
                                $scope.CompanyManagement.contactList[0].primary = false;
                            }
                            if ($scope.SecondaryContact == 'Remove') {
                                if ($scope.CompanyManagement.contactList.length > 1) {
                                    $scope.CompanyManagement.contactList[0].primary = false;
                                    $scope.CompanyManagement.contactList[1].primary = false;
                                }
                                $scope.validateSecondaryDetails = true;
                                valid = $scope.form.companydetailsform.$valid && $scope.form.companycontactpersondetailsform.$valid && $scope.form.companysecondarycontactpersondetailsform.$valid && $scope.isValidPrimaryMobileEntered && $scope.isValidPrimaryWorkPhoneEntered && $scope.isValidSecondaryMobileEntered && $scope.isValidSecondaryWorkPhoneEntered;
                            } else {
                                valid = $scope.form.companydetailsform.$valid && $scope.form.companycontactpersondetailsform.$valid && $scope.isValidPrimaryMobileEntered && $scope.isValidPrimaryWorkPhoneEntered; ;
                            }
                            if (valid) {
                                console.log('Step 2 validations are proper, call saveContactDetails function');
                                nextButtonClicked = false;
                                saveContactDetails();
                            } else if (StorageService.get('companyDetails') != null && !($scope.CompanyDetailsUpdateMode) && !clientOrBuCreationInProgress && !clientOrBuUpdateInProgress) {
                                console.log('No api call');
                                StorageService.set('wizardoperations', true);
                                if (clientOrBuUpdateInProgress) {
                                    $scope.laststep = true;
                                }
                                nextButtonClicked = false;
                                triggercount = 0;
                            } else {
                                console.log('validations not proper');
                                $scope.step2formsubmitted = true;
                                triggercount = 0;
                            }
                            break;
                        case 3: console.log('Next clicked from step 3 moving to step 4 : ');
                            oldCheckedAdmin = null;
                            if (angular.isDefined($scope.originalStepThree) && $scope.originalStepThree != null && $scope.selectedFourDotFiveAdmin.length > 0) {
                                stepThreeChanged = !(angular.equals($scope.originalStepThree.userId, $scope.selectedFourDotFiveAdmin[0].userId));
                                oldfourdotfiveadminid = $scope.originalStepThree.userId;
                            } else if (!(angular.isDefined($scope.originalStepThree))) {
                                //This is satisfied in create company.
                                stepThreeChanged = true;
                            }
                            console.log('Step 3 is changed : ' + stepThreeChanged);
                            if ($scope.SecondaryContact == 'Remove') {
                                valid = $scope.form.companydetailsform.$valid && $scope.form.companycontactpersondetailsform.$valid && $scope.form.companysecondarycontactpersondetailsform.$valid && $scope.selectedFourDotFiveAdmin.length > 0;
                            } else {
                                valid = $scope.form.companydetailsform.$valid && $scope.form.companycontactpersondetailsform.$valid && $scope.selectedFourDotFiveAdmin.length > 0;
                            }

                            if (valid) {
                                console.log('Step 3 validations are proper, call saveFourDotFiveAdminDetails function');
                                nextButtonClicked = false;
                                saveFourDotFiveAdminDetails();
                            } else if (StorageService.get('companyDetails') != null && !($scope.CompanyDetailsUpdateMode)) {
                                console.log('No api call');
                                if (!$scope.licensePreferencesFetched) {
                                    getAssessmentWorkFlow(StorageService.get('companyDetails').companyId);
                                } else {
                                    StorageService.set('wizardoperations', true);
                                    $scope.stepTrigger('nextbtnclick');
                                }
                                $scope.laststep = true;
                                nextButtonClicked = false;
                                StorageService.set('wizardoperations', true);
                                triggercount = 0;
                            } else {
                                console.log('Step 3 mandatory fileds not filled.');
                                $scope.step3formsubmitted = true;
                                triggercount = 0;
                            }
                            break;
                        case 4: console.log('Next clicked from step 4 moving to dashboard : ');
                            stepFourChanged = !(angular.equals($scope.CompanyManagement.licensePreferences, $scope.preferences));
                            console.log('Step 4 is changed : ' + stepFourChanged)
                            if ($scope.TypeOfCompany == 'Client' || $scope.TypeOfCompany == 'BusinessUnit' || $scope.CompanyManagement.company.companyType == 'Host') {
                                console.log('Host or client or bu');
                                if ($scope.SecondaryContact == 'Remove') {
                                    valid = $scope.form.companydetailsform.$valid && $scope.form.companycontactpersondetailsform.$valid && $scope.form.companysecondarycontactpersondetailsform.$valid;
                                } else {
                                    valid = $scope.form.companydetailsform.$valid && $scope.form.companycontactpersondetailsform.$valid;
                                }
                            } else {
                                if ($scope.SecondaryContact == 'Remove') {
                                    valid = $scope.form.companydetailsform.$valid && $scope.form.companycontactpersondetailsform.$valid && $scope.form.companysecondarycontactpersondetailsform.$valid && $scope.selectedFourDotFiveAdmin.length > 0;
                                } else {
                                    valid = $scope.form.companydetailsform.$valid && $scope.form.companycontactpersondetailsform.$valid && $scope.selectedFourDotFiveAdmin.length > 0;
                                }
                            }
                            if (valid) {
                                console.log('Step 4 validations are proper, call saveAssessementFlowDetails function')
                                nextButtonClicked = false;
                                saveAssessementFlowDetails();
                            } else if (StorageService.get('companyDetails') != null && !($scope.CompanyDetailsUpdateMode) && !clientOrBuCreationInProgress && !clientOrBuUpdateInProgress) {
                                console.log('No api call');
                                $scope.laststep = true;
                                StorageService.remove('wizardoperations');
                                triggercount = 0;
                                nextButtonClicked = false;
                                $timeout(function () {
                                    $state.go('missioncontrol.dashboard', { companyObject: 'companyObject' }, { reload: false });
                                }, 4000);
                            } else {
                                $scope.step4formsubmitted = true;
                                triggercount = 0;
                            }
                            break;
                        default:
                            console.log('Default');
                    }
                } else {
                    triggercount = 0;
                }
            }

            $scope.toggleAssessmentWorkFlowValues = function(index, workflow) {
                if (workflow.selected) {
                    workflow.selected = false;
                } else {
                    workflow.selected = true;
                }
                stepFourChanged = !(angular.equals($scope.originalStepFour, $scope.preferences));
            }

            /**
             * This function is called on click of 'Add Secondary Contact' button.
             * It allows the user to add secondary contact person for the company/client.
             */
            $scope.isSecondaryContactSelected = function() {
                if ($scope.SecondaryContact == 'Add') {
                	 console.log($scope.CompanyManagement.contactList.length);
                    $scope.SecondaryContact = 'Remove';
                } else {
                    $scope.SecondaryContact = 'Add';
                    if ($scope.CompanyManagement.contactList.length > 1) {
                        $scope.CompanyManagement.contactList.splice(1, 1);
                    }
                    $scope.validateSecondaryDetails = false;
                    console.log('Dont check secondary details');
                    $scope.form.companysecondarycontactpersondetailsform.$setPristine();
                }
            }

            /**
             * This method will be called when user clicks on 'Prev' button of wizard
             */
            $scope.prevbtnclicked = function() {
                $scope.form.companycontactpersondetailsform.$setPristine();
                console.log('prevbtnclicked: ');
                $scope.step2formsubmitted = false;
                saveButtonClicked = false;
            	nextButtonClicked = false;
                $scope.laststep = false;
                if (count === 0 && StorageService.get('companyDetails') == null && StorageService.get('SelectedHostCompany') == null && $scope.CompanyManagement.companyState != 'Draft') {
                    console.log('In if prevbtnclicked');
                    count = count + 1;
                    var action = $timeout(function() {
                        StorageService.set('wizardoperations', true);
                        angular.element('.btn-prev').triggerHandler('click');
                    }, 0).then(function() {
                        console.log('Then prevbtnclicked');
                        StorageService.remove('wizardoperations');
                        count = 0;
                        $timeout.cancel(action);
                    });
                } else {
                    if (StorageService.get('SelectedHostCompany') == null) {
                        $scope.AssessmentWorkFlow = [];
                    }
                    if ($scope.CompanyDetailsUpdateMode || $scope.ClientOrBuCreate || $scope.ClientOrBuUpdate || StorageService.get('SelectedHostCompany') != null || $scope.CompanyManagement.companyState == 'Draft') {
                        StorageService.set('wizardoperations', true);
                        triggercount = 0;
                    } else if (angular.equals($rootScope.userDetails.roleScreenRef.role.name,'StaffingAdmin') || angular.equals($rootScope.userDetails.roleScreenRef.role.name,'CorporateAdmin')){
                        StorageService.set('wizardoperations', true);
                        triggercount = 0;
                    }
                }
            }

            $scope.$watchCollection(
                "form.companydetailsform",
                function (newValue, oldValue) {
                    if ($scope.form.companydetailsform.$dirty) {
                        $scope.cancelNotClicked = true;
                    }
                }
            );
            
            /**
             * Clear data on the form 
             */
            $scope.cancel = function() {
                $scope.cancelNotClicked = false;
                console.log('@@In cancel function from step: ' + getCurrentStep()+'$scope.geoLocations '+angular.toJson($scope.geoLocations));
                cancelClicked = false;
                $scope.copyOfGeoLocations = angular.copy($scope.geoLocations);
                console.log('@@@$scope.copyOfGeoLocations  '+angular.toJson($scope.copyOfGeoLocations));
                $scope.copyOfGeoLocationsCopy = angular.copy($scope.geoLocationsCopy);
                showConfirmationPopup = false;
                var cancelCurrentStep = getCurrentStep();
                if (cancelCurrentStep == 3 && ($scope.TypeOfCompany == 'Client' || StorageService.get('SelectedHostCompany') != null || $scope.TypeOfCompany == 'BusinessUnit')) {
                    cancelCurrentStep = 4;
                }
                switch (cancelCurrentStep) {
                    case 1:
                         console.log('Step one: ' + angular.toJson($scope.originalStepOne));
                        if (angular.isDefined($scope.CompanyManagement.companyState)) {
                            console.log('inside company state defined');
                            cancelClicked = true;
                            $scope.step1formsubmitted = false;
                            if (!angular.equals($scope.CompanyManagement.company.zipcode, $scope.originalStepOne.zipcode)) {
                                console.log('both are equal');
                                $scope.getAddressDetails($scope.originalStepOne.zipcode, 'zipchanged');
                            }else{
                                console.log('both are not equal');
                                $scope.getAddressDetails($scope.originalStepOne.zipcode, 'updateaddress');
                            }
                            $scope.step1formsubmitted = false;
                            $scope.CompanyManagement.company = angular.copy($scope.originalStepOne);
                            $scope.SavedCompanyManagement.company = angular.copy($scope.originalStepOne);
                            $scope.form.companydetailsform.$setPristine();
                        } else {
                            console.log('inside company state un defined');
                            $scope.form.companydetailsform.$setPristine();
                            console.log("Step one create state click on cancel: " + $scope.CompanyManagement.company.zipcode);
                            $scope.geoLocations = [];
                            $scope.CompanyManagement.company = {};
                            cancelClicked = true;
                            $scope.getAddressDetails($scope.CompanyManagement.company.zipcode, 'zipchanged');
                            $scope.step1formsubmitted = false;
                        }
                        angular.element('.companyaddresscopyenabled').attr({ 'readonly': false, 'disabled': false });
                        break;
                    case 2:
                        if ($scope.originalStepTwo != undefined && $scope.originalStepTwo.length != 0) {
                            $scope.CompanyManagement.contactList = angular.copy($scope.originalStepTwo);
                            $scope.isValidPrimaryMobileEntered = true;
                            $scope.isValidPrimaryWorkPhoneEntered = true;
                            $scope.form.companycontactpersondetailsform.maskedWorkPhone.$setValidity('ng-intl-tel-input', true);
                            $scope.form.companycontactpersondetailsform.maskedWorkPhone1.$setValidity('ng-intl-tel-input', true);
                            if ($scope.originalStepTwo.length > 1) {
                                console.log('Secondary contact present.');
                                $scope.SecondaryContact = 'Remove';
                                $scope.isValidSecondaryMobileEntered = true;
                                $scope.isValidSecondaryWorkPhoneEntered = true;
                                $scope.form.companysecondarycontactpersondetailsform.maskedWorkPhoneSecondary.$setValidity('ng-intl-tel-input', true);
                                $scope.form.companysecondarycontactpersondetailsform.maskedMobilePhoneSecondary.$setValidity('ng-intl-tel-input', true);
                                $timeout(function() {
                                    $scope.form.companysecondarycontactpersondetailsform.$setPristine();
                                }, 1000);
                            }else{
                                console.log('in else');
                                $scope.invalidCompanySecondaryWorkNumber = false;
                                $scope.invalidCompanySecondaryMobilePhone = false;
                            }
                            $timeout(function() {
                                $scope.form.companycontactpersondetailsform.$setPristine();
                            }, 1000);
                             $scope.step2formsubmitted = false;
                            console.log('isValidPrimaryMobileEntered '+$scope.isValidPrimaryMobileEntered);
                        } else {
                            console.log("Step Two create state click on cancel: ");
                            $scope.CompanyManagement.contactList = [];
                            document.getElementById('maskedWorkPhone').value = '';
                            document.getElementById('maskedWorkPhone1').value = '';
                            if ($scope.SecondaryContact == 'Remove') {
                                $scope.invalidCompanySecondaryMobilePhone = false;
                                $scope.invalidCompanySecondaryWorkNumber = false;
                                document.getElementById('maskedWorkPhoneSecondary').value = '';
                                document.getElementById('maskedMobilePhoneSecondary').value = '';
                            }
                            $scope.step2formsubmitted = false;
                            $scope.form.companycontactpersondetailsform.$setPristine();
                        }
                        break;
                    case 3:
                        console.log('Step three: ' + angular.toJson($scope.originalStepThree) + ' Role: ' + $rootScope.userDetails.roleScreenRef.role.name);
                        if ($scope.originalStepThree != undefined) {
                            if (!($rootScope.userDetails.roleScreenRef.role.name == '4dot5Admin')) {
                                $scope.fourDotFiveAdminsList.forEach(function(element) {
                                    if (element.userId != $scope.originalStepThree.userId) {
                                        element.ticked = false;
                                        console.log('Making false of: ' + angular.toJson(element));
                                    } else {
                                        element.ticked = true;
                                    }
                                }, this);
                                if (oldCheckedAdmin != null) {
                                    oldCheckedAdmin[0].ticked = false;
                                }
                                $scope.CompanyManagement = angular.copy($scope.CompanyManagementReset);
                            }
                        } else {
                            $scope.CompanyManagement.fourDotFiveAdmin = {};
                            $scope.fourDotFiveAdminsList.forEach(function(element) {
                                if (element.ticked) {
                                    element.ticked = false;
                                }
                            }, this);
                            $scope.step3formsubmitted = false;
                        }
                        $scope.form.company4dot5admindetailsform.$setPristine();
                        break;
                    case 4:
                        if ($scope.ClientOrBuCreate) {
                            $scope.AssessmentWorkFlow = [];
                            console.log('Its client create');
                            $scope.preferences = angular.copy($scope.originalStepFour);
                        } else if ($scope.ClientOrBuUpdate) {
                            $scope.AssessmentWorkFlow = [];
                            console.log('Its client update');
                            $scope.preferences = angular.copy($scope.originalStepFour);
                        } else {
                            console.log('Its company create or update: ');
                            if ($rootScope.userDetails.company.companyType != 'Host') {
                                console.log('Original: ' + angular.toJson($scope.originalStepFour));
                                $scope.preferences = angular.copy($scope.originalStepFour);
                                console.log('Preferences: ' + angular.toJson($scope.originalStepFour));
                            } else {
                                $scope.preferences = angular.copy($scope.originalStepFour);
                            }
                        }
                         $scope.step4formsubmitted = false;
                        break;
                }
            }

            $scope.cancelSteps = function () {
                $scope.cancel();
            }

            /**
             * Delete extra characters
             */
            function deleteExtraCharacters(element) {
                for (var key in element) {
                    if (angular.isNumber(element[key])) {
                        console.log('Key is: ' + key);
                        delete element[key];
                    }
                }
            }

            var unbindStateChangeEvent = $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
                console.log('Preventing'+$scope.form.companydetailsform.$invalid);
                event.preventDefault();
                confirmationAtStep = getCurrentStep();
                if (!(angular.equals($scope.SavedCompanyManagement.company, $scope.CompanyManagement.company)) && confirmationAtStep == 1) {
                    console.log('Unsaved changes in step 1');
                    showConfirmationPopup = true;
                } else if (confirmationAtStep == 2 && !(angular.equals($scope.SavedCompanyManagement.contactList, $scope.CompanyManagement.contactList))) {
                    console.log('Unsaved changes in step 2');
                    showConfirmationPopup = true;
                } else if (angular.isDefined($scope.originalStepThree) && confirmationAtStep == 3) {
                    console.log('Unsaved changes in step 3 :');
                    if (!(angular.isDefined($scope.selectedFourDotFiveAdmin[0]))) {
                        showConfirmationPopup = true;
                    } else if (!(angular.equals($scope.originalStepThree.userId, $scope.selectedFourDotFiveAdmin[0].userId))) {
                        showConfirmationPopup = true;
                    }
                } else if ($scope.SecondaryContact == 'Remove' && confirmationAtStep == 2) {
                    console.log('Secondary selected:');
                    if ($scope.form.companysecondarycontactpersondetailsform.$dirty && confirmationAtStep == 2 && !companyStepTwoSaved && !companyDeleted) {
                        console.log('Unsaved changes in step 2 secondary contact');
                        showConfirmationPopup = true;
                    }
                } else if (confirmationAtStep == 4 && !(angular.equals($scope.CompanyManagement.licensePreferences, $scope.preferences)) && !(angular.equals($scope.originalStepFour, $scope.preferences))) {
                    console.log('Unsaved changes in step 4 staffing company');
                    console.log('Staffing company update step 4 changed: ' + angular.equals($scope.originalStepFour, $scope.AssessmentWorkFlow));
                    if (!(angular.equals($scope.originalStepFour, $scope.AssessmentWorkFlow))) {
                        showConfirmationPopup = true;
                    }
                } else if(!(angular.equals($scope.preferences,$scope.originalStepFour)) && confirmationAtStep == 3){
                    console.log('in else if');
                    showConfirmationPopup = true;
                }else if( $scope.errorOcurred && (!angular.equals($scope.originalStepOne, $scope.CompanyManagement.company)) && confirmationAtStep == 1){
                    showConfirmationPopup = true;
                }else{
                    console.log('$scope.CompanyManagement.company  '+angular.toJson($scope.CompanyManagement.company));
                    console.log('in else');
                }
                

                if (showConfirmationPopup) {
                    bootbox.confirm({
                        closeButton: false,
                        title: "<div class='alert alert-warning' style='margin-bottom: 0px;'><i class='fa fa-warning fa-fw fa-lg'></i><strong>Warning!</strong></div>",
                        message: "Unsaved data on the page will be lost. Do you still want to continue?",
                        buttons: {
                            confirm: {
                                label: 'Yes',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'Cancel',
                                className: 'btn-danger'
                            }
                        },
                        callback: function(result) {
                            console.log('This was logged in the callback: ' + result);
                            if (result) {
                                if ($rootScope.switchClicked) {
                                    $scope.cancel();
                                    $scope.errorOcurred = false;
                                    $rootScope.switchClicked = false;
                                    $scope.getCompaniesForLoggedinUser(function(response){
                                    	$('#myModal').modal('show');
                                    });
                                } else {
                                    $scope.errorOcurred = false;
                                    unbindStateChangeEvent();
                                    if (toState.name === 'login') {
                                        $rootScope.$emit("CallLogoutMethod", { logoutnow: true });
                                    } else {
                                        $state.go(toState.name, toParams, { reload: true });
                                    }
                                }
                            } else {
                                showConfirmationPopup = false;
                                $rootScope.switchClicked = false;
                                console.log("Stay in same page");
                            }

                        }
                    });
                } else {
                    // console.log("No changes in this page yet so moving");
                    if (StorageService.get('clientOrBuToBeUpdated') != null) {
                        StorageService.remove('clientOrBuToBeUpdated');
                    }
                    if ($rootScope.switchClicked) {
                       $rootScope.switchClicked = false;
                    	showConfirmationPopup = false;
                     
                        $scope.getCompaniesForLoggedinUser(function(response){
                        	$('#myModal').modal('show');
                        });
                    } else {
                        unbindStateChangeEvent();
                        $rootScope.switchClicked = false;
                        if (toState.name === 'login') {
                            $rootScope.$emit("CallLogoutMethod", { logoutnow: true });
                        } else {
                            $state.go(toState.name, toParams, { reload: true });
                        }
                    }
                }
            });

            /**
             * Step changes
             */
            $scope.getStepChanges = function(stepclicked) {
                saveButtonClicked = false;
            	nextButtonClicked = false;
                console.log('Step clicked: ' + stepclicked);
                if (stepclicked != 3 && ($scope.TypeOfCompany == 'Client' || $scope.TypeOfCompany == 'BusinessUnit')) {
                    $scope.laststep = false;
                } else if (stepclicked != 4 && ($scope.CompanyManagement.company.companyType == 'StaffingCompany' || $scope.CompanyManagement.company.companyType == 'Corporation')) {
                    $scope.laststep = false;
                }
            }

            $scope.changeAddressObject = function(type, obj) {
                  console.log('............................................'+type);
                saveButtonClicked = false;
            	nextButtonClicked = false;
                switch (type) {
                    case 'city':
                        console.log('type : ' + type + ' obj : ' + obj);
                        for (var p = 0; p < $scope.geoLocationscopy.length; p++) {
                            if ($scope.geoLocationscopy[p].city === obj) {
                                $scope.CompanyManagement.company.addressState = $scope.geoLocationscopy[p].state;
                                $scope.CompanyManagement.company.country = $scope.geoLocationscopy[p].country;
                                $scope.CompanyManagement.company.county = $scope.geoLocationscopy[p].county;
                                if(angular.isDefined($scope.geoLocationscopy[p].county) && ($scope.geoLocationscopy[p].county!==null)){
                                	$scope.isCountyExists = true;
                                }else{
                                	$scope.isCountyExists = false;
                                }
                            }
                        }
                        break;
                    case 'state':
                        console.log('type : ' + type + ' obj : ' + obj);
                        for (var p = 0; p < $scope.geoLocationscopy.length; p++) {
                            if ($scope.geoLocationscopy[p].state === obj) {
                                $scope.CompanyManagement.company.city = $scope.geoLocationscopy[p].city;
                                $scope.CompanyManagement.company.country = $scope.geoLocationscopy[p].country;
                                $scope.CompanyManagement.company.county = $scope.geoLocationscopy[p].county;
                                if(angular.isDefined($scope.geoLocationscopy[p].county) && ($scope.geoLocationscopy[p].county!==null)){
                                	$scope.isCountyExists = true;
                                }else{
                                	$scope.isCountyExists = false;
                                }
                            }
                        }
                        break;
                    case 'country':
                        console.log('type : ' + type + ' obj : ' + obj);
                        for (var p = 0; p < $scope.geoLocationscopy.length; p++) {
                            if ($scope.geoLocationscopy[p].country === obj) {
                                $scope.CompanyManagement.company.city = $scope.geoLocationscopy[p].city;
                                $scope.CompanyManagement.company.addressState = $scope.geoLocationscopy[p].state;
                                $scope.CompanyManagement.company.county = $scope.geoLocationscopy[p].county;
                                if(angular.isDefined($scope.geoLocationscopy[p].county) && ($scope.geoLocationscopy[p].county!==null)){
                                	$scope.isCountyExists = true;
                                }else{
                                	$scope.isCountyExists = false;
                                }
                            }
                        }
                        break;
                    case 'county':
                        for (var p = 0; p < $scope.geoLocationscopy.length; p++) {
                            if ($scope.geoLocationscopy[p].county === obj) {
                                $scope.CompanyManagement.company.city = $scope.geoLocationscopy[p].city;
                                $scope.CompanyManagement.company.addressState = $scope.geoLocationscopy[p].state;
                                $scope.CompanyManagement.company.country = $scope.geoLocationscopy[p].country;
                            }
                        }
                        break;
                }
            }
            
            /**
             * Toggle company address to user checkbox.
             */
            $scope.toggleCopyCompanyAddress = function () {
                $scope.copyCompanyAddress = document.getElementById("copyCompanyAddress").checked;
                var companyAddressZipcode;
                console.log($rootScope.userDetails.company.companyId);
                if ($scope.copyCompanyAddress) {
                    genericService.getObjects(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.GET_COMPANY_DETAILS + '/' + $rootScope.userDetails.company.companyId).then(function (data) {
                        companyAddressZipcode = data.company.zipcode;
                        $scope.CompanyManagement.company.address1 = data.company.address1;
                        $scope.CompanyManagement.company.address2 = data.company.address2;
                        $scope.CompanyManagement.company.zipcode = companyAddressZipcode;
                        cancelClicked = false;
                        $scope.getAddressDetails(companyAddressZipcode, 'copycomanyaddress',function(response){
                        	angular.element('.companyaddresscopyenabled').attr({ 'readonly': true, 'disabled': true });
                        });
                    }, function (error) {
                    	if($rootScope.isOnline){
                        	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                        }
                    });
                } else {
                    $scope.copyCompanyAddress = false;
                    angular.element('.companyaddresscopyenabled').attr({ 'readonly': false, 'disabled': false });
                    if ($scope.accessMode == MESSAGECONSTANTS.ACCESS_MODES.EDIT) {
                        $scope.CompanyManagement.company.address1 = $scope.SavedCompanyManagement.company.address1;
                        $scope.CompanyManagement.company.address2 = $scope.SavedCompanyManagement.company.address2;
                        $scope.CompanyManagement.company.zipcode = $scope.SavedCompanyManagement.company.zipcode;
                        $scope.getAddressDetails( $scope.CompanyManagement.company.zipcode, 'zipchanged',function(response){
                        	if(angular.isDefined($scope.SavedCompanyManagement.company.county) && ($scope.SavedCompanyManagement.company.county !== null)
                        			&& ($scope.CompanyManagement.company.zipcode === $scope.SavedCompanyManagement.company.zipcode)){
                    			$scope.CompanyManagement.company.county = $scope.SavedCompanyManagement.company.county;
                    			$scope.safeApply();
                            }
                        });
                    } else {
                        $scope.CompanyManagement.company.address1 = $scope.SavedCompanyManagement.company.address1;
                        $scope.CompanyManagement.company.address2 = $scope.SavedCompanyManagement.company.address2;
                        $scope.CompanyManagement.company.zipcode = $scope.SavedCompanyManagement.company.zipcode;
                        $scope.getAddressDetails( $scope.CompanyManagement.company.zipcode, 'zipchanged',function(response){
                        	if(angular.isDefined($scope.SavedCompanyManagement.company.county) && ($scope.SavedCompanyManagement.company.county !== null)
                        			&& ($scope.CompanyManagement.company.zipcode === $scope.SavedCompanyManagement.company.zipcode)){
                    			$scope.CompanyManagement.company.county = $scope.SavedCompanyManagement.company.county;
                    			$scope.safeApply();
                            }
                        });
                    }
                }
            }

            $("#maskedWorkPhone").on("countrychange", function(e, countryData) {
                $scope.primaryContactWTCountryCode = countryData.iso2;
                $timeout(function () {
                    $scope.setPrimaryContactWTCountryCode();
                }, 200);
            });
            

            $("#maskedWorkPhone1").on("countrychange", function(e, countryData) {
                $scope.primaryContactWTCountryCode1 = countryData.iso2;
                $timeout(function () {
                	$scope.setPrimaryContactWTCountryCode1();
                }, 200);
                
            });

            angular.element("#maskedWorkPhoneSecondary").on("countrychange", function(e, countryData) {
                 $scope.secondaryContactWTCountryCode = countryData.iso2;
                 $timeout(function () {
                    $scope.setSecondaryContactWTCountryCode();
                }, 200);
            });

            angular.element("#maskedMobilePhoneSecondary").on("countrychange", function (e, countryData) {
                $scope.secondaryContactMNCountryCode = countryData.iso2;
                $timeout(function () {
                    $scope.setSecondaryContactMNCountryCode();
                }, 200);
            });
            $timeout(function(){
                console.log('now entered');
                $scope.form.companycontactpersondetailsform.$setPristine();
              //  $scope.form.companysecondarycontactpersondetailsform.$setPristine();
            },500);

            //Get these information for UI Elements
            $scope.getCompanyTypes();
        }
    ]);