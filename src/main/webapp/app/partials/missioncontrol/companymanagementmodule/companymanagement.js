var companymanagementmodule = angular.module('4dot5.missioncontrolmodule.companymanagementmodule',
    []);

companymanagementmodule.constant('COMPANYMANAGEMENTCONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.companymanagement',
        URL: '/companymanagement',
        CONTROLLER: 'CompanyManagementController',
        TEMPLATEURL: 'app/partials/missioncontrol/companymanagementmodule/companymanagement.html',
    },
    CONTROLLER: {
        ASSIGN_CLIENT_TO_STAFFING_COMPANY: 'api/company/assignclientorbu',
        GET_INDUSTRY_SUBLIST: 'api/company/getindustrysublist/',
        GET_INDUSTRY_GROUPS: 'api/company/getindustrylist',
        GET_4DOT5_ADMINS: 'api/company/get4dot5admins',
        GET_ENTITY_SCOPES: 'api/company/getentityscope',
        GET_COMPANY_TYPE_DETAILS: 'api/company/getcompanytype',
        GET_ASSESSMENT_WORK_FLOW: 'api/licensepref/getlicensepreferences/',
        GET_ASSESSMENT_WORK_FLOW_FOR_CLIENT_BU: 'api/licensepref/getclientorbulicensepreferences/',
        SET_LICENCE_PREFERENCES: 'api/licensepref/setlicensepreferences',
        SET_LICENCE_PREFERENCES_FOR_CLIENT_BU: 'api/licensepref/setclientorbulicensepreferences',
        GET_COMPANY: 'api/company/getcompanydetails',
        SAVE_COMPANY: 'api/company/savecompany',
        // DELETE_COMPANY: 'api/company/deletecompanyorclientorbu',
        // GET_DELETE_COMPANY_INFO: 'api/company/companyorclientorbudeleteinfos',
        UPDATE_COMPANY_FOR_FOURDOTFIVE_ADMIN: 'api/user/updatecompanyfor4dot5admin',
        CHANGE_COMPANYSTATE: 'api/company/changecompanystate',
        GET_ALL_SOURCE_TYPES: 'api/resume/resumesourcetypes'
    }
});


companymanagementmodule.filter('unique', function () {
    return function (collection, keyname) {
        var output = [],
            keys = [];
        angular.forEach(collection, function (item) {
            var key = item[keyname];
            if (keys.indexOf(key) === -1) {
                keys.push(key);
                output.push(item);
            }
        });
        return output;
    };
});

companymanagementmodule.config(
    ['$stateProvider',
        'COMPANYMANAGEMENTCONSTANTS',
        function ($stateProvider, COMPANYMANAGEMENTCONSTANTS) {

            $stateProvider.state(COMPANYMANAGEMENTCONSTANTS.CONFIG.STATE, {
                url: COMPANYMANAGEMENTCONSTANTS.CONFIG.URL,
                templateUrl: COMPANYMANAGEMENTCONSTANTS.CONFIG.TEMPLATEURL,
                controller: COMPANYMANAGEMENTCONSTANTS.CONFIG.CONTROLLER,
                data: {
                    requireLogin: true
                },
                params: {
                    companyObject: null,
                    userId: null,
                    accessMode: null,
                    addClient: null,
                    updateClient: null,
                    bu: null,
                    selectedCompanyId: null
                }
            });
        }
    ]);

companymanagementmodule.controller('CompanyManagementController',
    ['$scope',
        '$state',
        '$uibModal',
        '$stateParams',
        '$rootScope',
        'genericService',
        'StorageService',
        'companyService',
        'planService',
        'dateTimeUtilityService',
        'COMPANYMANAGEMENTCONSTANTS',
        'MESSAGECONSTANTS',
        'usSpinnerService',
        '$timeout',
        '$http',
        '$compile',
        'ngToast',
        '$filter',
        'userRoleService',
        'alertsAndNotificationsService',
        function ($scope, $state, $uibModal, $stateParams, $rootScope, genericService, StorageService, companyService, planService, dateTimeUtilityService, COMPANYMANAGEMENTCONSTANTS, MESSAGECONSTANTS, usSpinnerService, $timeout, $http, $compile, ngToast, $filter, userRoleService, alertsAndNotificationsService) {
            var loggedInUserDetails = StorageService.getCookie('userDetails');
            var newlyCreatedCompanyId = null;
            var fourDotFiveCompanyAdmin;
            var assignClient2StaffingCompany;  //NP
            var industrySublist = [];          //NP
            var count = 0;
            var newlyCreatedCompanyId;
            var confirmationAtStep;
            var showConfirmationPopup = false;
            var updatedAssessementFlow;
            var companyStepOneSaved = false;
            var companyStepTwoSaved = false;
            var companyStepThreeSaved = false;
            var companyStepFourSaved = false;
            var companyStepFiveSaved = false;
            var stepOneChanged = false;
            var stepTwoChanged = false;
            var stepThreeChanged = false;
            var stepFourChanged = false;
            var stepFiveChanged = false;
            var getDetailsOfCompany;
            var fourDotFiveAdminObject = null;
            var oldfourdotfiveadminid = null;
            $scope.clientOrBuUpdateInProgress = false;
            $scope.clientOrBuCreationInProgress = false;
            $scope.resumePullCounts = [5, 10, 15, 20, 25, 30, 40, 50, 75, 100];
            $scope.isCompanyDeleted = false;
            $scope.isActivePlanExists = true;
            $scope.isPlanChosen = false;


            var maskedMobilePhoneError;
            var maskedWorkPhoneError;
            var maskedWorkPhoneSecondaryError;
            var maskedMobilePhoneSecondaryError;
            var countryForContactDetails;
            var industryListOfCompany = 0;
            var oldCheckedAdmin = null;
            var selectedSubIndustries = [];
            var cancelClicked = false;
            var newCompanyCreation = false;

            $scope.zipcodeCalling = false;
            $scope.zipcodeCallCompleted = false;
            var saveButtonClicked = false;
            var nextButtonClicked = false;
            var nextButtonFlag = 0;
            var saveButtonFlag = 0;

            $scope.usernamejob;
            $scope.keyjob;
            $scope.buttonWorkFlowText = 'Add';

            $scope.form = {};
            $scope.form.companydetailsform = {};
            $scope.form.companycontactpersondetailsform = {};
            $scope.form.companysecondarycontactpersondetailsform = {};
            $scope.form.company4dot5admindetailsform = {};
            $scope.form.companyassessementflowdetailsform = {};

            $scope.maskedMobilePhoneSecondarycursorPosition = true;

            //This is the UI object (Transient object)
            $scope.CompanyManagement = {};
            $scope.CompanyManagementReset = {};
            $scope.CompanyManagement.company = {};//Step 1 object
            $scope.CompanyManagement.contactList = [];//Step 2 object
            $scope.fourDotFiveAdminsList = [];//Step 3 object
            $scope.selectedPlan = {};//Step 4 object
            $scope.AssessmentWorkFlow = [];//Step 5 object

            //This is the Saved object (Saved object),the object that will be sent while saving the step details.
            $scope.SavedCompanyManagement = {};
            $scope.SavedCompanyManagement.company = {};//Step 1 object
            $scope.SavedCompanyManagement.contactList = [];//Step 2 object
            $scope.SavedfourDotFiveAdmin = [];//Step 3 object
            $scope.savedCurrentActivePlan = {};//Step 4 object
            $scope.SavedAssessmentWorkFlow = [];//Step 5 object

            var savedCompanyManagementCopy = {};
            savedCompanyManagementCopy.company = {};
            savedCompanyManagementCopy.contactList = [];
            var savedfourDotFiveAdminCopy = [];
            var savedCurrentActivePlanCopy = {};
            var savedAssessmentWorkFlowCopy = [];

            $scope.preferences = null;
            $scope.errorInSavingPrefsCheck = false;
            $scope.cancelNotClicked = true;
            $scope.invalidZipcode = false;
            console.log('validzipcode 144' + $scope.validzipcode);

            $scope.licensePreferencesFetched = false;
            $scope.planDetailsFetched = false;

            $scope.selectedIndustryGroup = null;
            $scope.laststep = false;
            $scope.TypeOfCompany = 'Company';
            $scope.SecondaryContact = 'Add';
            $stateParams.accessMode = 'view';
            $scope.IndustrySubList = [];
            $scope.SelectedIndustrySubList = [];
            $scope.typeaheadNotSelected = true;
            var valid = false;
            var companyTypes = null;
            var triggercount = 0;
            var delimitedCompanyOrClientName = null;
            $scope.CompanyTypes = [];
            $scope.lastStepCompleted = false;
            $scope.ShowCreateCompanyHeading = true;
            $scope.ClientOrBuUpdate = false;
            $scope.ClientOrBuCreate = false;
            $scope.IndustryGroupList = [];
            $scope.IndustrySubGroupList = [];
            $scope.dataLastLabel = 'Complete';
            $scope.selectedSubGroup = false;
            $scope.industriesInPanel = [];
            $scope.temp = [];

            $scope.primaryContactWTFormat = '+1 201-555-0123';
            $scope.primaryContactWTFormat1 = '+1 201-555-0123';
            $scope.primaryContactMNFormat = '+1 201-555-0123';
            $scope.secondaryContactWTFormat = '+1 201-555-0123';
            $scope.secondaryContactMNFormat = '+1 201-555-0123';
            $scope.primaryContactWTCountryCode = 'us';
            $scope.primaryContactWTCountryCode1 = 'us';
            $scope.primaryContactMNCountryCode = 'us';
            $scope.secondaryContactWTCountryCode = 'us';
            $scope.secondaryContactMNCountryCode = 'us';

            $scope.isValidPrimaryMobileEntered = false;
            $scope.isValidPrimaryWorkPhoneEntered = false;
            $scope.isValidSecondaryMobileEntered = false;
            $scope.isValidSecondaryWorkPhoneEntered = false;


            $scope.jobSourceType;

            var isAdressValid = false;
            var tempObj = null;
            $scope.errorOcurred = false;
            $scope.isCountyExists = false;

            $scope.JobBoardCredential = {};
            $scope.JobBoardCredential.sourceType;
            $scope.JobBoardCredential.userName;
            $scope.JobBoardCredential.key = "";
            $scope.selectedAccessTime = [];

            $scope.keyaccess;
            $scope.usernameaccess;
            $scope.buttonAssessment = "Add";
            $scope.selectedAssesmentIndex;
            $scope.assessmentTypeList = ["Interview Mocha", "Codility"];
            $scope.useAccountCredDetailsList = ["4dot5", "My Own"];
            $scope.useAccountCredDetailsList1 = ["4dot5", "My Own"];
            $scope.matchCountList = [
                {
                    name: "1",
                    value: 1
                },
                {
                    name: "2",
                    value: 2
                },
                {
                    name: "3",
                    value: 3
                },
                {
                    name: "5",
                    value: 5
                },
                {
                    name: "Any",
                    value: -1
                }
            ];
            $scope.isUserSuperUserOr4dot5Admin = userRoleService.isLoggedInUserSuperUserOr4dot5Admin();

            /**
             * This method works for triggerng button clicks on save button
             *  only for on blur validations
             */
            var saveButtonTrigger = function () {
                console.log('saveButtonTrigger');
                var savebtnclick = $timeout(function () {
                    angular.element('.btn-save').triggerHandler('click');
                }, 0).then(function () {
                    saveButtonClicked = false;
                    nextButtonClicked = false;
                    $timeout.cancel(savebtnclick);
                });
            };

            /**
             * This method works for triggerng button clicks on save button
             *  only for on blur validations
             */
            var nextButtonTrigger = function () {
                var nextbtnclick = $timeout(function () {
                    console.log('triggering next button click');
                    angular.element('.btn-next').triggerHandler('click');
                }, 0).then(function () {
                    console.log('in nextButtonTrigger callback');
                    $scope.zipcodeCalling = false;
                    $scope.zipcodeCallCompleted = false;
                    nextButtonClicked = false;
                    saveButtonClicked = false;
                    triggercount = 0;
                    $timeout.cancel(nextbtnclick);
                });
            };

            $scope.isPrimaryContactWTValid = function (workPhoneValue) {
                if (workPhoneValue !== "") {
                    if ($("#maskedWorkPhone").intlTelInput("isValidNumber")) {
                        console.log('p WorkTelephone Valid');
                        $scope.isValidPrimaryWorkPhoneEntered = true;
                        $scope.CompanyManagement.contactList[0].workPhone = $filter('phonenumber')(workPhoneValue, $scope.primaryContactWTFormat);
                        $scope.form.companycontactpersondetailsform.maskedWorkPhone.$setValidity('ng-intl-tel-input', true);
                    } else {
                        console.log('p work telephone not valid');
                        $scope.emptyCompanyWorkNumber = false;
                        $scope.invalidCompanyWorkNumber = true;
                        $scope.isValidPrimaryWorkPhoneEntered = false;
                        $scope.primaryWorkTelephoneValueTrim = document.getElementById('maskedWorkPhone').value;
                        $scope.CompanyManagement.contactList[0].workPhone = $scope.primaryWorkTelephoneValueTrim.replace(/ |-/gm, '');
                        console.log($scope.CompanyManagement.contactList[0].workPhone);
                        $scope.form.companycontactpersondetailsform.maskedWorkPhone.$setValidity('ng-intl-tel-input', false);
                    }
                } else {
                    $scope.emptyCompanyWorkNumber = true;
                    $scope.invalidCompanyWorkNumber = false;
                    console.log('CompanyWorkNumber empty');
                    $scope.isValidPrimaryWorkPhoneEntered = false;
                }
            };

            $scope.isPrimaryContactWTValid1 = function (mobileNumberValue) {
                if (mobileNumberValue !== "") {
                    $scope.form.companycontactpersondetailsform.maskedWorkPhone1.visited = true;
                    if ($("#maskedWorkPhone1").intlTelInput("isValidNumber")) {
                        console.log('p WorkTelephone1 Valid');
                        $scope.isValidPrimaryMobileEntered = true;
                        $scope.CompanyManagement.contactList[0].mobilePhone = $filter('phonenumber')(mobileNumberValue, $scope.primaryContactWTFormat1);
                        $scope.form.companycontactpersondetailsform.maskedWorkPhone1.$setValidity('ng-intl-tel-input', true);
                    } else {
                        console.log('p work telephone1 not valid');
                        $scope.emptyCompanyMobileNumber = false;
                        $scope.invalidCompanyMobileNumber = true;
                        $scope.primaryMobileTelephoneValueTrim = document.getElementById('maskedWorkPhone1').value;
                        $scope.CompanyManagement.contactList[0].mobilePhone = $scope.primaryMobileTelephoneValueTrim.replace(/ |-/gm, '');
                        console.log($scope.CompanyManagement.contactList[0].mobilePhone);
                        $scope.form.companycontactpersondetailsform.maskedWorkPhone1.$setValidity('ng-intl-tel-input', false);
                        $scope.isValidPrimaryMobileEntered = false;
                    }
                } else {
                    $scope.emptyCompanyMobileNumber = true;
                    $scope.invalidCompanyMobileNumber = false;
                    console.log('mobileNumberValue empty');
                    $scope.isValidPrimaryMobileEntered = false;
                }
            };

            $("#maskedWorkPhone").keyup(function () {
                if (document.getElementById('maskedWorkPhone').value.length > 0) {
                    $scope.workTelephoneDirty = true;
                } else {
                    $scope.workTelephoneDirty = false;
                }
            });


            $("#maskedWorkPhone1").keyup(function () {
                if (document.getElementById('maskedWorkPhone1').value.length > 0) {
                    $scope.mobileNumberDirty = true;
                } else {
                    $scope.mobileNumberDirty = false;
                }
            });

            /**
             *  This is to handle long string
             */
            var get4Dot5AdminFullName = function () {
                for (var p = 0; p < $scope.fourDotFiveAdminsList.length; p++) {
                    if ($scope.fourDotFiveAdminsList[p].lastName != null) {
                        var output = $scope.fourDotFiveAdminsList[p].firstName + " " + $scope.fourDotFiveAdminsList[p].lastName;
                    } else {
                        var output = $scope.fourDotFiveAdminsList[p].firstName;
                    }
                    if (output.length > 57) {
                        $scope.fourDotFiveAdminsList[p]["fullName"] = output.substring(0, 57) + "...";
                    } else {
                        $scope.fourDotFiveAdminsList[p]["fullName"] = output;
                    }
                }
            };

            $scope.setPrimaryContactWTCountryCode = function () {
                $("#maskedWorkPhone").intlTelInput("setCountry", $scope.primaryContactWTCountryCode);
                $scope.primaryContactWTFormat = document.getElementById("maskedWorkPhone").placeholder;
                var maskValidator = [];

                for (var index = 0; index < $scope.primaryContactWTFormat.length; index++) {
                    if (angular.isNumber($scope.primaryContactWTFormat[index])) {
                        console.log('Num: ' + $scope.primaryContactWTFormat[index]);
                    } else {
                        console.log('Char: ' + $scope.primaryContactWTFormat[index]);
                    }
                }
                $timeout(function () {
                    $scope.isPrimaryContactWTValid($scope.CompanyManagement.contactList[0].workPhone);
                }, 500);

            };

            $scope.setPrimaryContactWTCountryCode1 = function () {
                $("#maskedWorkPhone1").intlTelInput("setCountry", $scope.primaryContactWTCountryCode1);
                $scope.primaryContactWTFormat1 = document.getElementById("maskedWorkPhone1").placeholder;
                $scope.isPrimaryContactWTValid1($scope.CompanyManagement.contactList[0].mobilePhone);
            };

            $scope.setPrimaryContactMNCountryCode = function () {
                $("#maskedMobilePhone").intlTelInput("setCountry", $scope.primaryContactMNCountryCode);
                $scope.primaryContactMNFormat = document.getElementById("maskedMobilePhone").placeholder;
                $timeout(function () {
                    $scope.isPrimaryContactMNValid();
                }, 500);
            };

            $scope.setPrimaryContactNExamples = function () {
                $scope.primaryContactWTFormat = document.getElementById("maskedWorkPhone").placeholder;
                $scope.primaryContactWTFormat1 = document.getElementById("maskedWorkPhone1").placeholder;
                $scope.primaryContactMNFormat = document.getElementById("maskedMobilePhone").placeholder;
            };

            $scope.isSecondaryContactWTValid = function (secondaryWorkNumberValue) {
                if (secondaryWorkNumberValue !== "") {
                    $scope.form.companysecondarycontactpersondetailsform.maskedWorkPhoneSecondary.visited = true;
                    if ($("#maskedWorkPhoneSecondary").intlTelInput("isValidNumber")) {
                        console.log('p maskedWorkPhoneSecondary Valid');
                        $scope.CompanyManagement.contactList[1].workPhone = $filter('phonenumber')(secondaryWorkNumberValue, $scope.secondaryContactWTFormat);
                        $scope.form.companysecondarycontactpersondetailsform.maskedWorkPhoneSecondary.$setValidity('ng-intl-tel-input', true);
                        $scope.isValidSecondaryWorkPhoneEntered = true;
                    } else {
                        $scope.emptyCompanySecondaryWorkNumber = false;
                        $scope.invalidCompanySecondaryWorkNumber = true;
                        $scope.isValidSecondaryWorkPhoneEntered = false;
                        $scope.secondaryWorkTelephoneValueTrim = document.getElementById('maskedWorkPhoneSecondary').value;
                        $scope.CompanyManagement.contactList[1].workPhone = $scope.secondaryWorkTelephoneValueTrim.replace(/ |-/gm, '');
                        $scope.form.companysecondarycontactpersondetailsform.maskedWorkPhoneSecondary.$setValidity('ng-intl-tel-input', false);
                    }
                } else {
                    $scope.emptyCompanySecondaryWorkNumber = true;
                    $scope.invalidCompanySecondaryWorkNumber = false;
                    $scope.isValidSecondaryWorkPhoneEntered = false;
                }
            };

            $scope.isSecondaryContactMNValid = function (secondaryMobileNumberValue) {
                if (secondaryMobileNumberValue !== "") {
                    $scope.form.companysecondarycontactpersondetailsform.maskedMobilePhoneSecondary.visited = true;
                    if ($("#maskedMobilePhoneSecondary").intlTelInput("isValidNumber")) {
                        $scope.isValidSecondaryMobileEntered = true;
                        $scope.CompanyManagement.contactList[1].mobilePhone = $filter('phonenumber')(secondaryMobileNumberValue, $scope.secondaryContactMNFormat);
                        $scope.form.companysecondarycontactpersondetailsform.maskedMobilePhoneSecondary.$setValidity('ng-intl-tel-input', true);
                    } else {
                        $scope.emptyCompanySecondaryMobilePhone = false;
                        $scope.invalidCompanySecondaryMobilePhone = true;
                        $scope.isValidSecondaryMobileEntered = false;
                        $scope.secondaryMobileTelephoneValueTrim = document.getElementById('maskedMobilePhoneSecondary').value;
                        $scope.CompanyManagement.contactList[1].mobilePhone = $scope.secondaryMobileTelephoneValueTrim.replace(/ |-/gm, '');
                        $scope.form.companysecondarycontactpersondetailsform.maskedMobilePhoneSecondary.$setValidity('ng-intl-tel-input', false);
                    }
                } else {
                    $scope.emptyCompanySecondaryMobilePhone = true;
                    $scope.invalidCompanySecondaryMobilePhone = false;
                    $scope.isValidSecondaryMobileEntered = false;
                }
            };

            $scope.setSecondaryContactWTCountryCode = function () {
                $("#maskedWorkPhoneSecondary").intlTelInput("setCountry", $scope.secondaryContactWTCountryCode);
                $scope.secondaryContactWTFormat = document.getElementById("maskedWorkPhoneSecondary").placeholder;
                $timeout(function () {
                    $scope.isSecondaryContactWTValid(document.getElementById("maskedWorkPhoneSecondary").value);
                }, 500);

            };

            $scope.setSecondaryContactMNCountryCode = function () {
                $("#maskedMobilePhoneSecondary").intlTelInput("setCountry", $scope.secondaryContactMNCountryCode);
                $scope.secondaryContactMNFormat = document.getElementById("maskedMobilePhoneSecondary").placeholder;
                $timeout(function () {
                    $scope.isSecondaryContactMNValid(document.getElementById("maskedMobilePhoneSecondary").value);
                }, 500);

            };

            $scope.setSecondaryContactNExamples = function () {
                $scope.secondaryContactWTFormat = document.getElementById("maskedWorkPhoneSecondary").placeholder;
                $scope.secondaryContactMNFormat = document.getElementById("maskedMobilePhoneSecondary").placeholder;
            };

            $("#maskedWorkPhoneSecondary").keyup(function () {
                console.log("maskedWorkPhoneSecondary");
                if (document.getElementById('maskedWorkPhoneSecondary').value.length > 0) {
                    $scope.workTelephoneDirty = true;
                } else {
                    $scope.workTelephoneDirty = false;
                }
            });

            $("#maskedMobilePhoneSecondary").keyup(function () {
                if (document.getElementById('maskedMobilePhoneSecondary').value.length > 0) {
                    $scope.mobileNumberDirty = true;
                } else {
                    $scope.mobileNumberDirty = false;
                }
            });

            var getCurrentStep = function () {
                return $('#myWizard').wizard('selectedItem').step;
            };

            $scope.single = function () {
                //console.log('image : '+angular.toJson($scope.profilePic));
            };


            /**
             * This method is called during company update,host company update, client update and if address is changed and cancel is clicked in step 1.
             * This would be sent over HTTPS if the page is served via HTTPS
             */
            $scope.getAddressDetails = function (zipcode, type, callback) {
                if (angular.isDefined($scope.form.companydetailsform.zipcode) && $scope.form.companydetailsform.zipcode.$valid) {
                    $scope.validzipcode = true;
                } else {
                    $scope.validzipcode = false;
                }

                $scope.cities = [];
                $scope.states = [];
                $scope.counties = [];
                $scope.countries = [];
                var locations = [];

                if (type != 'copycomanyaddress') {
                    document.getElementById("copyCompanyAddress").checked = false;
                }

                $scope.copyCompanyAddress = false;

                var url = 'maps.googleapis.com/maps/api/geocode/json?address=' + zipcode + '&key=' + MESSAGECONSTANTS.GEOCODING.API_KEY;

                if (angular.isDefined(zipcode) && !angular.equals(zipcode, '')) {
                    console.log('validzipcode calling google api');
                    $.getJSON({
                        url: 'https://maps.googleapis.com/maps/api/geocode/json?address=' + zipcode + '&key=' + MESSAGECONSTANTS.GEOCODING.API_KEY,
                        data: {
                            sensor: false,
                        },
                        success: function (data, textStatus) {
                            console.log('Data: ' + data.status + ' textStatus: ' + textStatus);
                            var createGeoLocation = function (addressdetails, postalname) {
                                console.log(' addressdetails ' + angular.toJson(addressdetails) + 'postalname ' + angular.toJson(postalname));
                                var addressobj = {};
                                for (var p = 0; p < addressdetails.address_components.length; p++) {
                                    for (var t = 0; t < addressdetails.address_components[p].types.length; t++) {
                                        switch (addressdetails.address_components[p].types[t]) {
                                            case 'country':
                                                addressobj.country = addressdetails.address_components[p].long_name;
                                                break;
                                            case 'administrative_area_level_2':
                                                addressobj.county = addressdetails.address_components[p].long_name;
                                                break;
                                            case 'administrative_area_level_1':
                                                addressobj.state = addressdetails.address_components[p].long_name;
                                                break;
                                            case 'locality':
                                                if (angular.isDefined(postalname) && postalname !== '') {
                                                    addressobj.city = postalname;
                                                } else {
                                                    addressobj.city = addressdetails.address_components[p].long_name;
                                                }
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                }

                                if (angular.isUndefined(addressobj.city) || angular.isUndefined(addressobj.state) || angular.isUndefined(addressobj.country)) {
                                    for (var p = 0; p < addressdetails.address_components.length; p++) {
                                        for (var t = 0; t < addressdetails.address_components[p].types.length; t++) {
                                            switch (addressdetails.address_components[p].types[t]) {
                                                case 'locality':
                                                    if (angular.isUndefined(addressobj.state)) {
                                                        addressobj.state = addressdetails.address_components[p].long_name;
                                                    }
                                                    if (angular.isUndefined(addressobj.country)) {
                                                        addressobj.country = addressdetails.address_components[p].long_name;
                                                    }
                                                    break;
                                                case 'administrative_area_level_2':
                                                    if (angular.isUndefined(addressobj.city)) {
                                                        if (angular.isDefined(postalname) && postalname !== '') {
                                                            addressobj.city = postalname;
                                                        } else {
                                                            addressobj.city = addressdetails.address_components[p].long_name;
                                                        }
                                                    }
                                                    if (angular.isUndefined(addressobj.state)) {
                                                        addressobj.state = addressdetails.address_components[p].long_name;
                                                    }
                                                    if (angular.isUndefined(addressobj.country)) {
                                                        addressobj.country = addressdetails.address_components[p].long_name;
                                                    }

                                                    break;
                                                case 'administrative_area_level_3':
                                                    if (angular.isUndefined(addressobj.city)) {
                                                        if (angular.isDefined(postalname) && postalname !== '') {
                                                            addressobj.city = postalname;
                                                        } else {
                                                            addressobj.city = addressdetails.address_components[p].long_name;
                                                        }
                                                    }
                                                    break;
                                                case 'administrative_area_level_4':
                                                    if (angular.isUndefined(addressobj.city)) {
                                                        if (angular.isDefined(postalname) && postalname !== '') {
                                                            addressobj.city = postalname;
                                                        } else {
                                                            addressobj.city = addressdetails.address_components[p].long_name;
                                                        }
                                                    }
                                                    break;
                                                case 'sublocality' || 'sublocality_level_1' || 'sublocality_level_5':
                                                    if (angular.isDefined(postalname) && postalname !== '') {
                                                        addressobj.city = postalname;
                                                    } else {
                                                        addressobj.city = addressdetails.address_components[p].long_name;
                                                    }
                                                    break;
                                                case 'administrative_area_level_1':
                                                    if (angular.isUndefined(addressobj.city)) {
                                                        if (angular.isDefined(postalname) && postalname !== '') {
                                                            addressobj.city = postalname;
                                                        } else {
                                                            addressobj.city = addressdetails.address_components[p].long_name;
                                                        }

                                                        if (angular.isUndefined(addressobj.country)) {
                                                            addressobj.country = addressdetails.address_components[p].long_name;
                                                        }
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                }
                                return addressobj;
                            };
                            var checkCountyExists = function (locations, company) {
                                for (var i = 0; i < locations.length; i++) {
                                    if (angular.equals(locations[i].city, company.city) &&
                                        angular.isDefined(locations[i].county) && (locations.county !== null) &&
                                        angular.isDefined(company.county) &&
                                        (company.county !== null) && angular.equals(locations[i].county, company.county)) {
                                        return true;
                                    }
                                }
                                return false;
                            };
                            console.log('data : ' + angular.toJson(data));
                            if (data.status === 'OK') {
                                console.log('OK....');
                                $scope.validzipcode = true;
                                console.log('validzipcode 699' + $scope.validzipcode);
                                for (var i = 0; i < data.results.length; i++) {
                                    if (angular.isDefined(data.results[i].postcode_localities) && (data.results[i].postcode_localities.length > 0)) {
                                        for (var k = 0; k < data.results[i].postcode_localities.length; k++) {
                                            if ((data.results[i].types[0] === 'postal_code')) {
                                                console.log('entered zipcode is a postal code');
                                                var location = createGeoLocation(data.results[i], data.results[i].postcode_localities[k]);
                                                locations.push(angular.copy(location));
                                            } else {
                                                console.log('entered zipcode is not a postal code');
                                            }
                                        }
                                    } else {
                                        if ((data.results[i].types[0] === 'postal_code')) {
                                            console.log('entered zipcode is a postal code');
                                            var location = createGeoLocation(data.results[i]);
                                            locations.push(angular.copy(location));
                                        } else {
                                            console.log('entered zipcode is not a postal code');
                                        }
                                    }
                                }
                                $scope.temp = locations;
                                if (locations.length) {
                                    $scope.temp.city = locations[0].city;
                                    $scope.temp.addressState = locations[0].state;
                                    console.log('locations[0].county  ' + locations[0].county);
                                    $scope.temp.county = (angular.isUndefined(locations[0].county)) ? null : locations[0].county;
                                    if (angular.isDefined(locations[0].county) && (locations[0].county !== null)) {
                                        $scope.isCountyExists = true;
                                    } else {
                                        $scope.isCountyExists = false;
                                    }
                                    $scope.temp.country = locations[0].country;
                                    console.log('temp  ' + angular.toJson(location));
                                    $scope.zipcodeCallCompleted = true;
                                }
                                console.log('cancelClicked : ' + cancelClicked + ' type is: ' + type + '  $scope.temp' + angular.toJson($scope.temp));

                                if ((type === 'createaddress' || type === 'copycomanyaddress') && !cancelClicked && locations.length) {
                                    console.log('in createaddress');
                                    $scope.CompanyManagement.company.city = locations[0].city;
                                    $scope.CompanyManagement.company.addressState = locations[0].state;
                                    $scope.CompanyManagement.company.county = (angular.isUndefined(locations[0].county)) ? null : locations[0].county;
                                    if (angular.isDefined(locations[0].county) && (locations[0].county !== null)) {
                                        $scope.isCountyExists = true;
                                    } else {
                                        $scope.isCountyExists = false;
                                    }
                                    $scope.CompanyManagement.company.country = locations[0].country;
                                    $scope.validzipcode = true;
                                    $scope.zipcodeCallCompleted = true;
                                } else if (type === 'zipchanged' && !cancelClicked && locations.length) {
                                    console.log('in zipchanged............');
                                    $scope.CompanyManagement.company.city = locations[0].city;
                                    $scope.CompanyManagement.company.addressState = locations[0].state;
                                    $scope.CompanyManagement.company.county = (angular.isUndefined(locations[0].county)) ? null : locations[0].county;
                                    if (angular.isDefined(locations[0].county) && (locations[0].county !== null)) {
                                        $scope.isCountyExists = true;
                                    } else {
                                        $scope.isCountyExists = false;
                                    }
                                    $scope.CompanyManagement.company.country = locations[0].country;
                                    $scope.validzipcode = true;
                                    $scope.zipcodeCallCompleted = true;
                                } else if (type === 'updateaddress' && !cancelClicked && locations.length) {
                                    $scope.zipcodeCallCompleted = true;
                                } else if (cancelClicked) {
                                    locations = angular.copy($scope.temp);
                                    $scope.geoLocations = angular.copy($scope.temp);
                                    $scope.geoLocationscopy = angular.copy($scope.copyOfGeoLocations);
                                    $scope.CompanyManagement.company.city = $scope.originalStepOne.city;
                                    $scope.CompanyManagement.company.addressState = $scope.originalStepOne.addressState;
                                    $scope.CompanyManagement.company.county = $scope.originalStepOne.county;
                                    if (checkCountyExists(angular.copy(locations), angular.copy($scope.originalStepOne))) {
                                        $scope.isCountyExists = true;
                                    } else {
                                        $scope.isCountyExists = false;
                                    }
                                    $scope.CompanyManagement.company.country = $scope.originalStepOne.country;
                                    $scope.validzipcode = true;
                                    $scope.zipcodeCallCompleted = true;
                                    console.log('after setting location ' + angular.toJson($scope.geoLocations));
                                    console.log('after setting geo loc ' + angular.toJson($scope.copyOfGeoLocations));
                                } else if (locations.length) {
                                    if (checkCountyExists(angular.copy(locations), angular.copy($scope.originalStepOne))) {
                                        $scope.isCountyExists = true;
                                    } else {
                                        $scope.isCountyExists = false;
                                    }
                                    if (angular.isDefined($scope.originalStepTwo[0]) && angular.isDefined($scope.originalStepTwo[0].workPhone)) {
                                        $scope.CompanyManagement.contactList[0].workPhone = angular.copy($scope.originalStepTwo[0].workPhone);
                                        $scope.CompanyManagement.contactList[0].mobilePhone = angular.copy($scope.originalStepTwo[0].mobilePhone);
                                    }
                                    if (angular.isDefined($scope.originalStepTwo[1]) && angular.isDefined($scope.originalStepTwo[1].workPhone)) {
                                        $scope.CompanyManagement.contactList[1].workPhone = angular.copy($scope.originalStepTwo[1].workPhone);
                                        $scope.CompanyManagement.contactList[1].mobilePhone = angular.copy($scope.originalStepTwo[1].mobilePhone);
                                    }
                                    var elements = angular.element("#maskedMobilePhone");
                                    angular.forEach(elements, function (element) {
                                        $compile(element)($scope);
                                    });
                                    $scope.zipcodeCallCompleted = true;
                                }
                            } else if (data.status === 'ZERO_RESULTS') {
                                $scope.validzipcode = false;
                                console.log('Setting false: ' + $scope.CompanyManagement.company.zipcode);
                                if (!cancelClicked) {
                                    console.log('if ');
                                    $scope.validzipcode = false;
                                    if (!$scope.invalidZipcode) {
                                        $scope.invalidZipcode = true;
                                    }
                                    locations = [];
                                    $scope.CompanyManagement.company.city = '';
                                    $scope.CompanyManagement.company.addressState = '';
                                    $scope.CompanyManagement.company.county = '';
                                    $scope.isCountyExists = false;
                                    $scope.CompanyManagement.company.country = '';
                                    $scope.zipcodeCallCompleted = true;
                                } else {
                                    locations = angular.copy($scope.copyOfGeoLocations);
                                    $scope.geoLocations = angular.copy($scope.copyOfGeoLocations);
                                    $scope.geoLocationscopy = angular.copy($scope.copyOfGeoLocations);
                                    $scope.CompanyManagement.company.city = $scope.originalStepOne.city;
                                    $scope.CompanyManagement.company.addressState = $scope.originalStepOne.addressState;
                                    $scope.CompanyManagement.company.county = $scope.originalStepOne.county;
                                    if (checkCountyExists(angular.copy(locations), angular.copy($scope.originalStepOne))) {
                                        $scope.isCountyExists = true;
                                    } else {
                                        $scope.isCountyExists = false;
                                    }
                                    $scope.CompanyManagement.company.country = $scope.originalStepOne.country;
                                    $scope.zipcodeCallCompleted = true;
                                }
                            }
                            $scope.geoLocations = locations;
                            $scope.geoLocationscopy = locations;
                            if (!$scope.geoLocations.length) {
                                $scope.validzipcode = false;
                                if (!$scope.invalidZipcode) {
                                    $scope.invalidZipcode = true;
                                }
                                $scope.CompanyManagement.company.city = '';
                                $scope.CompanyManagement.company.addressState = '';
                                $scope.CompanyManagement.company.county = '';
                                $scope.isCountyExists = false;
                                $scope.CompanyManagement.company.country = '';
                                $scope.zipcodeCallCompleted = true;
                            }
                            $scope.$apply();
                            if (typeof callback != "undefined") {
                                return callback($scope.geoLocations);
                            }
                        },
                        error: function (data) {
                            console.log('Error: ' + angular.toJson(data));
                            $scope.invalidZipcode = true;
                            $scope.isCountyExists = false;
                            if (typeof callback != "undefined") {
                                return callback('success');
                            }
                        }
                    });
                } else {
                    console.log('in else');
                    $scope.geoLocations = [];
                    $scope.isCountyExists = false;
                    if (typeof callback != "undefined") {
                        return callback('success');
                    }
                }
            };


            /**
             * Get Address details.
             */
            $scope.getZipCodeDetails = function (zipcode, type) {
                nextButtonClicked = false;
                saveButtonClicked = false;
                if ($scope.form.companydetailsform.zipcode.$valid) {
                    if (angular.isDefined(zipcode)) {
                        cancelClicked = false;
                    }
                    $scope.validzipcode = true;
                    $scope.zipcodeCalling = true;
                    $scope.zipcodeCallCompleted = false;
                    $scope.getAddressDetails(zipcode, type, function (response) {
                        if (angular.isDefined($scope.SavedCompanyManagement.company.county) && ($scope.SavedCompanyManagement.company.county !== null)
                            && ($scope.CompanyManagement.company.zipcode === $scope.SavedCompanyManagement.company.zipcode)) {
                            $scope.CompanyManagement.company.county = $scope.SavedCompanyManagement.company.county;
                            $scope.safeApply();
                        }
                        if (saveButtonClicked) {
                            saveButtonTrigger();
                        } else if (nextButtonClicked) {
                            nextButtonTrigger();
                        }
                    });
                } else {
                    $scope.cancelNotClicked = false;
                    saveButtonClicked = false;
                    nextButtonClicked = false;
                    $scope.cities = [];
                    $scope.states = [];
                    $scope.counties = [];
                    $scope.countries = [];
                    $scope.geoLocations = [];
                    $scope.geoLocationscopy = [];
                    $scope.CompanyManagement.company.city = '';
                    $scope.CompanyManagement.company.addressState = '';
                    $scope.CompanyManagement.company.county = '';
                    $scope.isCountyExists = false;
                    $scope.CompanyManagement.company.country = '';
                }
            };

            /**
             * Conditions to check whether its Client create or Client update.
             */
            if (StorageService.get('clientOrBuCreationInProgress') !== null) {
                //Condition is satified if Client Creation is in progress.
                if ($rootScope.userDetails.company.companyType == 'Corporation') {
                    $scope.TypeOfCompany = 'BusinessUnit';
                    //$scope.CompanyManagement.company.companyType = 'BusinessUnit';
                } else if ($rootScope.userDetails.company.companyType == 'StaffingCompany') {
                    $scope.TypeOfCompany = 'Client';
                    //$scope.CompanyManagement.company.companyType = 'Client';
                }
                $scope.ClientOrBuCreate = true;
                $scope.clientOrBuCreationInProgress = true;
                StorageService.remove('clientOrBuCreationInProgress');
                // INDUSTRY CODE                getIndustryGroups();
            } else if (StorageService.get('clientOrBuToBeUpdated') !== null) {
                //Condition is satified if Client Updation is in progress.

                $scope.clientOrBuUpdateInProgress = true;
                if ($rootScope.userDetails.company.companyType == 'Corporation') {
                    $scope.TypeOfCompany = 'BusinessUnit';
                    //$scope.CompanyManagement.company.companyType = 'BusinessUnit';
                } else if ($rootScope.userDetails.company.companyType == 'StaffingCompany') {
                    $scope.TypeOfCompany = 'Client';
                    //$scope.CompanyManagement.company.companyType = 'Client';
                }
                $scope.ClientOrBuUpdate = true;
                $scope.CompanyManagement = StorageService.get('clientOrBuToBeUpdated');
                $scope.CompanyManagementReset = angular.copy($scope.CompanyManagement);
                console.log('$scope.CompanyManagementReset  ' + angular.toJson($scope.CompanyManagementReset.fourDotFiveAdmin));
                $scope.SavedCompanyManagement.company = angular.copy($scope.CompanyManagement.company);
                $scope.SavedCompanyManagement.contactList = angular.copy($scope.CompanyManagement.contactList);
                //As its a draft there should not be exception as duplicate client while saving.
                $scope.SavedCompanyManagement.companyState = angular.copy($scope.CompanyManagement.companyState);
                console.log('client : ' + $scope.CompanyManagement);
                $scope.editCompanyName = $scope.CompanyManagement.company.name.substring(0, 57);
                $scope.editCompanyNameOnHover = $scope.CompanyManagement.company.name;
                if ($scope.CompanyManagement.company.name.length > 57) {
                    $scope.editCompanyName = $scope.editCompanyName + '...';
                }
                $scope.getAddressDetails($scope.CompanyManagement.company.zipcode, 'updateaddress');
                StorageService.remove('clientOrBuToBeUpdated');

                $scope.originalStepOne = angular.copy($scope.CompanyManagement.company);
                $scope.originalStepTwo = angular.copy($scope.CompanyManagement.contactList);
                if ($scope.CompanyManagement.contactList.length > 1) {
                    $scope.SecondaryContact = 'Remove';
                    $scope.isValidPrimaryMobileEntered = true;
                    $scope.isValidPrimaryWorkPhoneEntered = true;
                    $scope.isValidSecondaryMobileEntered = true;
                    $scope.isValidSecondaryWorkPhoneEntered = true;
                } else if ($scope.CompanyManagement.contactList.length == 1) {
                    $scope.isValidPrimaryMobileEntered = true;
                    $scope.isValidPrimaryWorkPhoneEntered = true;
                }
            }

            /**
             * This condition seperates whether the module is loaded
             * on account of Company view/update or Host Company view/update.
             * @access for create, view and update
             */
            if ((StorageService.get('companyDetails') != null || $rootScope.userDetails.roleScreenRef.role.name == 'CorporateAdmin' || $rootScope.userDetails.roleScreenRef.role.name == 'StaffingAdmin') && !$scope.clientOrBuCreationInProgress && !$scope.clientOrBuUpdateInProgress && StorageService.get('SelectedHostCompany') == null) {
                //This condition is satisfied when a company is selected from popup or
                // Role is CorporateAdmin or role is StaffingAdmin and user not in creation/updation of client and selected company is not host company.
                //view or update company access
                if (StorageService.get('companyDetails') != null) {
                    if (angular.isDefined(StorageService.get('companyDetails').companyId)) {
                        getDetailsOfCompany = StorageService.get('companyDetails').companyId;
                    } else {
                        getDetailsOfCompany = StorageService.get('companyDetails');
                    }
                } else {
                    console.log('Rootscope details');
                    getDetailsOfCompany = $rootScope.userDetails.company.companyId;
                }

                genericService.getObjects(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.GET_COMPANY + '/' + getDetailsOfCompany).then(function (data) {
                    $scope.CompanyManagement = data;
                    $scope.CompanyManagementReset = angular.copy($scope.CompanyManagement);
                    console.log('$scope.CompanyManagementReset  ' + angular.toJson($scope.CompanyManagementReset.fourDotFiveAdmin));
                    //Setting the phone number values to be true
                    if (data.contactList.length == 1) {
                        $scope.isValidPrimaryMobileEntered = true;
                        $scope.isValidPrimaryWorkPhoneEntered = true;
                    } else if (data.contactList.length == 2) {
                        $scope.isValidPrimaryMobileEntered = true;
                        $scope.isValidPrimaryWorkPhoneEntered = true;
                        $scope.isValidSecondaryMobileEntered = true;
                        $scope.isValidSecondaryWorkPhoneEntered = true;
                    }

                    $scope.getAddressDetails(data.company.zipcode, 'updateaddress');

                    $scope.originalStepOne = angular.copy($scope.CompanyManagement.company);
                    $scope.SavedCompanyManagement.company = angular.copy($scope.CompanyManagement.company);
                    console.log('$scope.originalStepOne  ' + angular.toJson($scope.originalStepOne));
                    $scope.originalStepTwo = angular.copy($scope.CompanyManagement.contactList);
                    $scope.SavedCompanyManagement.contactList = angular.copy($scope.CompanyManagement.contactList);
                    $scope.originalStepThree = angular.copy($scope.CompanyManagement.fourDotFiveAdmin);
                    $scope.SavedfourDotFiveAdmin = angular.copy($scope.CompanyManagement.fourDotFiveAdmin);
                    console.log('...................................' + angular.toJson($scope.SavedfourDotFiveAdmin));


                    //  INDUSTRY CODE                 $scope.SelectedIndustrySubList = angular.copy($scope.CompanyManagement.company.industryList);

                    if ($scope.CompanyManagement.fourDotFiveAdmin !== null) {
                        //This is satisfied when a company has 4dot5 admin assigned.
                        oldfourdotfiveadminid = $scope.CompanyManagement.fourDotFiveAdmin.userId;
                        $scope.CompanyManagement.fourDotFiveAdmin.ticked = true;
                        $scope.originalStepThree.ticked = true;
                        $scope.fourDotFiveAdminsList = [];
                        genericService.getObjects(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.GET_4DOT5_ADMINS).then(function (data) {
                            if ($rootScope.userDetails.roleScreenRef.role.name == '4dot5SuperUser' || $rootScope.userDetails.roleScreenRef.role.name == '4dot5Admin') {
                                data.forEach(function (element) {
                                    if (element.userId == $scope.CompanyManagement.fourDotFiveAdmin.userId) {
                                        element.ticked = true;
                                    } else {
                                        element.ticked = false;
                                    }
                                    $scope.fourDotFiveAdminsList.push(element);
                                }, this);
                            } else {
                                console.log('Pushing: ');
                                $scope.fourDotFiveAdminsList.push($scope.CompanyManagement.fourDotFiveAdmin);
                            }
                            get4Dot5AdminFullName();
                        }, function (error) {
                            if ($rootScope.isOnline) {
                                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                            }

                            //INDUSTRY CODE                            getIndustryGroups();
                        });
                    } else if ($scope.isSuperUser($rootScope.userDetails.roleScreenRef.role) && ($scope.CompanyManagement.fourDotFiveAdmin == null)) {
                        //This is satisfied when a company has is saved as a draft company and hence 4dot5 admin is still not assigned.
                        get4dot5admins();
                    }
                    if (data.contactList.length > 1) {
                        //This condition is satisfied when the selected company has Secondary Contact assigned and hence it has to be shown.
                        $scope.SecondaryContact = 'Remove';
                        //Setting phone number values to be true.
                        $scope.isValidSecondaryWorkPhoneEntered = true;
                        $scope.isValidSecondaryMobileEntered = true;
                    }
                }, function (error) {
                    if ($rootScope.isOnline) {
                        alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                    }
                    });
                
                $scope.HostCompanySelected = false;
                $scope.CompanyDetailsUpdateMode = false;
                $scope.ShowCreateCompanyHeading = false;

                if ($rootScope.userDetails.roleScreenRef.role.name == '4dot5SuperUser' || $rootScope.userDetails.roleScreenRef.role.name == '4dot5Admin') {
                    //This condition is satisfied when user has access to edit the company details.
                    console.log('Enabling all');
                    $scope.CompanyDetailsUpdateMode = true;
                    $scope.ShowCreateCompanyHeading = false;
                    StorageService.remove('wizardoperations');
                    $timeout(function(){
                        angular.element('.access').attr({'readonly': false, 'disabled': false});
                    }, 500);
                } else {
                    //This condition is satisfied when user doesnot have access to edit the company details and hence all fields are disabled.
                    $timeout(function(){
                        angular.element('.access').attr({'readonly': true, 'disabled': true});
                        angular.element('.hostaccess').attr({'readonly': true, 'disabled': true});
                    }, 500);
                }
                $scope.accessMode = angular.copy($stateParams.accessMode);
            } else if (StorageService.get('SelectedHostCompany') != null) {
                //This condition is satisfied when selected company is a host company.
                console.log('Details of Host company');
                $scope.CompanyDetailsUpdateMode = false;
                $scope.HostCompanySelected = true;
                $scope.TypeOfCompany = 'Host';
                genericService.getObjects(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.GET_COMPANY + '/' + StorageService.get('SelectedHostCompany').companyId).then(function (data) {
                    $scope.CompanyManagement = data;
                    $scope.CompanyManagementReset = angular.copy($scope.CompanyManagement);
                    console.log('$scope.CompanyManagementReset  ' + angular.toJson($scope.CompanyManagementReset.fourDotFiveAdmin));
                    $scope.SavedCompanyManagement.company = angular.copy($scope.CompanyManagement.company);
                    $scope.SavedCompanyManagement.contactList = angular.copy($scope.CompanyManagement.contactList);
                    $scope.getAddressDetails(data.company.zipcode, 'updateaddress');
                    $scope.originalStepOne = angular.copy($scope.CompanyManagement.company);
                    console.log('$scope.originalStepOne  ' + angular.toJson($scope.originalStepOne));

                    // INDUSTRY CODE                 delete $scope.originalStepOne['industryGroup'];
                    // INDUSTRY CODE               delete $scope.originalStepOne['industryList'];
                    $scope.originalStepTwo = angular.copy($scope.CompanyManagement.contactList);
                    $scope.originalStepFive = angular.copy($scope.CompanyManagement.licensePreferences);

                    //Updating the SavedCompanyManagement object after getting company details.
                    $scope.SavedAssessmentWorkFlow = angular.copy($scope.CompanyManagement.licensePreferences);
                    //  INDUSTRY CODE    $scope.SelectedIndustrySubList = angular.copy($scope.CompanyManagement.company.industryList);

                    $scope.AssessmentWorkFlow = $scope.CompanyManagement.licensePreferences;
                    $scope.accessMode = angular.copy($stateParams.accessMode);

                    if (data.contactList.length > 1) {
                        //This condition is satisfied when the selected company has Secondary Contact assigned and hence it has to be shown.
                        $scope.SecondaryContact = 'Remove';
                    }
                    if ($rootScope.userDetails.roleScreenRef.role.name == '4dot5SuperUser' || $rootScope.userDetails.roleScreenRef.role.name == '4dot5Admin') {
                        console.log('Enabling all');
                        $scope.CompanyDetailsUpdateMode = true;
                        $scope.ShowCreateCompanyHeading = false;
                        StorageService.remove('wizardoperations');
                        $timeout(function(){
                            angular.element('.access').attr({'readonly': true, 'disabled': true});
                            angular.element('.hostaccess').attr({'readonly': true, 'disabled': true});
                        }, 500);
                    } else {
                        $timeout(function(){
                            angular.element('.access').attr({'readonly': true, 'disabled': true});
                            angular.element('.hostaccess').attr({'readonly': true, 'disabled': true});
                        }, 500);
                    }
                    // INDUSTRY CODE     getIndustryGroups();
                }, function (error) {
                    if ($rootScope.isOnline) {
                        alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                    }
                });
            }

            /**
             * This method is used to delete company details
             */
            $scope.deleteCompanyDetails = function () {
                $scope.getDeleteCompanyDetails();
            };

            $scope.getDeleteCompanyDetails = function () {
                companyService.getDeleteCompanyInfo($rootScope.userDetails.company.companyId, function (data) {
                   $scope.deleteCompanyObject = {
                        companyId: $rootScope.userDetails.company.companyId,
                        companyName:  $rootScope.userDetails.company.companyName,
                        candidatesCount: data.numberOfCandiddates,
                        requisitionsCount: data.numberOfRequisitions,
                        usersCount: data.numberOfUsers,
                        clientOrBuCount: data.numberOfClientOrBUs
                    }  
                    $scope.launchDeleteModal();
                }, function (error) {
                    if ($rootScope.isOnline) {
                        alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                    }
                });
            }

            $scope.launchDeleteModal = function () {
                $scope.deleteCompanyModal = $uibModal.open({
                    animation: false,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/partials/missioncontrol/companymanagementmodule/delete-company-modal.html',
                    controller: ['$uibModalInstance','companyInfo',function ($uibModalInstance, companyInfo) {
                        var vm = this;
                        vm.companyInfo = companyInfo;
                        vm.closeModal = function () {
                            $uibModalInstance.close('cancel');
                        };
                        vm.onCompanyDelete = function () {
                            $uibModalInstance.close('cancel');
                            $scope.deleteCompanyDetailsLocally();
                        }
                    }],
                    controllerAs: 'deleteCompanyModal',
                    size: 'md',
                    resolve:{
                        companyInfo: function () {
                            return $scope.deleteCompanyObject;
                        }
                    }
                });
            }

            $scope.deleteCompanyDetailsLocally = function () {
                    StorageService.remove('companyDetails');
                    StorageService.remove('SelectedHostCompany');
                    $rootScope.userDetails.company.companyId = null;
                    $rootScope.userDetails.company.companyName = null;
                    $rootScope.userDetails.company.companyType = null;
                    $rootScope.userDetails.company.companySelected = false;
                    $rootScope.companyDeletedShowPopup = true;
                    console.log('$rootScope.companyDeletedShowPopup : ' + $rootScope.companyDeletedShowPopup);
                    StorageService.setCookie('userDetails', angular.copy($rootScope.userDetails), {'expires': new Date(Date.now() + $rootScope.tokenvalidity)});
                    $scope.isCompanyDeleted = true;
                    $state.go('missioncontrol', null, {reload: true});
            }

            var uncheckSelected4dot5Admin = function (selectedAdmin) {
                for (var i = 0; i < $scope.fourDotFiveAdminsList.length; i++) {
                    if ($scope.fourDotFiveAdminsList[i].userId === selectedAdmin.userId) {
                        $scope.fourDotFiveAdminsList[i].ticked = false;
                    }
                }
            };

            /**
             * This method is called to get list of 4dot5 Admins companyId
             */
            var get4dot5admins = function () {
                genericService.getObjects(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.GET_4DOT5_ADMINS).then(function (data) {
                    $scope.fourDotFiveAdminsList = data;
                    console.log('Get4dot5admins received is : ' + angular.toJson($scope.CompanyManagement.fourDotFiveAdmin));
                    console.log('Original step 3 is : ' + angular.toJson($scope.originalStepThree));
                    get4Dot5AdminFullName();

                    //INDUSTRY CODE         getIndustryGroups();
                }, function (error) {
                    if ($rootScope.isOnline) {
                        alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                    }
                    //INDUSTRY CODE         getIndustryGroups();
                });
            };

            var getAllPlans = function() {
                planService.getAllPlans(function(data) {
                    $scope.plans = data;
                    var priority = 0;
                    angular.forEach($scope.plans, function(value, key) {
                        priority++;
                        value.enabled = false;
                        value.planActionText = "Choose Plan";
                        value.priority = priority;
                    });
                    console.log('plans');
                    console.log($scope.plans);
                    $scope.plansCopy = angular.copy($scope.plans);
                 }, function (error) {
                     if ($rootScope.isOnline) {
                         alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                     }
                 });
            }

            $scope.setPlanPriorities = function() {
                var priority = 0;
                //assign chosen plan
                angular.forEach($scope.plans, function(plan, key) {
                    priority++;
                    if (plan.id == $scope.selectedPlan.planHeaderDto.planFeaturesCapabilityId) {
                        plan.enabled = true;
                        plan.priority = priority;
                        plan.planActionText = "Modify Plan";
                        $scope.savedCurrentActivePlanPriority = priority;
                        $scope.savedCurrentActivePlanName = plan.plan;
                    } else {
                        plan.enabled = false;
                        plan.priority = priority;
                        plan.planActionText = "Choose Plan";
                    }
                });
            }

            /**
             * This method will have a track of which forward recipient is selected
             */
            $scope.fourDotFiveAdminChosen = function () {
                if (angular.isDefined($scope.CompanyManagement.fourDotFiveAdmin) && !angular.equals($scope.CompanyManagement.fourDotFiveAdmin, null) && angular.isDefined($scope.selectedFourDotFiveAdmin[0])) {
                    console.log('$scope.CompanyManagement.fourDotFiveAdmin.userId  ' + $scope.CompanyManagement.fourDotFiveAdmin.userId + $scope.CompanyManagement.fourDotFiveAdmin.firstName + '  ' + $scope.selectedFourDotFiveAdmin[0].userId + '  ' + $scope.selectedFourDotFiveAdmin[0].firstName);
                    if (angular.equals($scope.CompanyManagement.fourDotFiveAdmin.userId, $scope.selectedFourDotFiveAdmin[0].userId)) {
                        uncheckSelected4dot5Admin($scope.selectedFourDotFiveAdmin[0]);
                        $scope.CompanyManagement.fourDotFiveAdmin = {};
                        $scope.form.company4dot5admindetailsform.$setDirty();
                    } else {
                        $scope.CompanyManagement.fourDotFiveAdmin = angular.copy($scope.selectedFourDotFiveAdmin[0]);
                        console.log('******************************************* ' + angular.toJson($scope.CompanyManagement.fourDotFiveAdmin));
                        $scope.form.company4dot5admindetailsform.$setDirty();
                    }
                } else {
                    $scope.CompanyManagement.fourDotFiveAdmin = angular.copy($scope.selectedFourDotFiveAdmin[0]);
                    $scope.CompanyManagementReset = angular.copy($scope.CompanyManagement);
                    console.log('new 4dot5 admin selected : ' + angular.toJson($scope.CompanyManagementReset.fourDotFiveAdmin));
                    $scope.form.company4dot5admindetailsform.$setDirty();
                }
            };

            function updatedAssessementFlowChanges(updatedLicencePrefs) {
                console.log('Original updated licence prefs: ' + updatedLicencePrefs.length);
                console.log('Original updated licence prefs: ' + angular.toJson(updatedLicencePrefs));
                for (var index = 0; index < updatedLicencePrefs.length; index++) {
                    console.log('Object: ' + updatedLicencePrefs[index]);
                    if (!(updatedLicencePrefs[index].licensePrefDetailsList[0].disabled == $scope.AssessmentWorkFlow[index].licensePrefDetailsList[0].disabled)) {
                        console.log('There was change in: ' + updatedAssessementFlowChanges[index].name);
                        $scope.AssessmentWorkFlow[index].disabled = updatedLicencePrefs[index].disabled;
                    }
                }
                console.log('UI Assesement Flow: ' + angular.toJson($scope.AssessmentWorkFlow));
            }

            /**
             * This method is called to get Assesement workflow steps.
             */
            function getAssessmentWorkFlow(companyId) {
                $scope.laststep = true;
                if (!(!$scope.CompanyDetailsUpdateMode && !$scope.ClientOrBuUpdate) || ($scope.CompanyDetailsUpdateMode && !$scope.laststep) || ($scope.ClientOrBuUpdate && !$scope.laststep)) {
                    $scope.dataLastLabel = 'Complete';
                }
                if (StorageService.get('companyDetails') != null && newlyCreatedCompanyId == null) {
                    if (angular.isDefined(StorageService.get('companyDetails').companyId)) {
                        newlyCreatedCompanyId = StorageService.get('companyDetails').companyId;
                    } else {
                        newlyCreatedCompanyId = StorageService.get('companyDetails');
                    }

                }
                if ($scope.clientOrBuCreationInProgress) {
                    //This condition is satisfied when client or bu creation is in progress.
                    console.log('Get assessement flow details for client during client creation.');
                    genericService.getObjects(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.GET_ASSESSMENT_WORK_FLOW_FOR_CLIENT_BU + '' + $rootScope.userDetails.company.companyId + '/' + newlyCreatedCompanyId).then(function (data) {
                        //               console.log('GET_ASSESSMENT_WORK_FLOW received is : ' + angular.toJson(data));
                        $scope.originalStepFive = angular.copy(data);
                        $scope.preferences = angular.copy(data);
                        stepFiveChanged = !(angular.equals($scope.originalStepFive, $scope.preferences));
                        $scope.licensePreferencesFetched = true;
                        if (!$scope.errorInSavingPrefsCheck) {
                            StorageService.set('wizardoperations', true);
                            $scope.stepTrigger('nextbtnclick');
                        }
                        //_setAccessDisabledForWorkflowSteps();
                    }, function (error) {
                        if ($rootScope.isOnline) {
                            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                        }
                    });
                } else if ($scope.CompanyDetailsUpdateMode) {
                    //This condition is satisfied when company update is in progress.
                    console.log('Get assessement flow details for company during company updation.');
                    genericService.getObjects(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.GET_ASSESSMENT_WORK_FLOW + '' + $rootScope.userDetails.company.companyType + '/' + $rootScope.userDetails.company.companyId).then(function (data) {
                        $scope.originalStepFive = angular.copy(data);
                        $scope.SavedAssessmentWorkFlow = angular.copy(data);
                        $scope.preferences = angular.copy($scope.SavedAssessmentWorkFlow);
                        $scope.licensePreferencesFetched = true;
                        stepFiveChanged = !(angular.equals($scope.originalStepFive, $scope.preferences));
                        if (!$scope.errorInSavingPrefsCheck) {
                            StorageService.set('wizardoperations', true);
                            $scope.stepTrigger('nextbtnclick');
                        }
                        //_setAccessDisabledForWorkflowSteps();
                    }, function (error) {
                        if ($rootScope.isOnline) {
                            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                        }
                    });
                } else if (!$scope.clientOrBuUpdateInProgress) {
                    console.log('1: ' + $rootScope.userDetails.company.companyId);//Creation of company in progress
                    if (newlyCreatedCompanyId == null) {
                        newlyCreatedCompanyId == $rootScope.userDetails.company.companyId;
                    }
                    genericService.getObjects(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.GET_ASSESSMENT_WORK_FLOW + '' + $rootScope.userDetails.company.companyType + '/' + $rootScope.userDetails.company.companyId).then(function (data) {
                        //               console.log('GET_ASSESSMENT_WORK_FLOW received is : ' + angular.toJson(data));
                        $scope.originalStepFive = angular.copy(data);
                        console.log('Assess: ' + $scope.preferences);
                        $scope.SavedAssessmentWorkFlow = angular.copy(data);
                        $scope.preferences = angular.copy($scope.SavedAssessmentWorkFlow);

                        $scope.licensePreferencesFetched = true;
                        stepFiveChanged = !(angular.equals($scope.originalStepFive, $scope.preferences));

                        if (!$scope.errorInSavingPrefsCheck) {
                            StorageService.set('wizardoperations', true);
                            $scope.stepTrigger('nextbtnclick');
                        }
                        //_setAccessDisabledForWorkflowSteps();
                    }, function (error) {
                        if ($rootScope.isOnline) {
                            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                        }
                    });
                } else {
                    console.log('Getting assessement flow details for clientOrBu');
                    if ($scope.clientOrBuUpdateInProgress) {
                        //This is satisfied when client or bu update is in progress
                        console.log('Getting assessement flow details for client Or Bu in update mode.');
                        genericService.getObjects(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.GET_ASSESSMENT_WORK_FLOW_FOR_CLIENT_BU + '' + $rootScope.userDetails.company.companyId + '/' + $scope.CompanyManagement.company.id).then(function (data) {
                            //           console.log('GET_ASSESSMENT_WORK_FLOW received is : ' + angular.toJson(data));
                            $scope.originalStepFive = angular.copy(data);
                            $scope.preferences = angular.copy(data);
                            console.log('$scope.preferences : ' + angular.toJson($scope.preferences));
                            $scope.licensePreferencesFetched = true;
                            stepFiveChanged = !(angular.equals($scope.originalStepFive, $scope.preferences));
                            console.log('stepFiveChanged starting : ' + angular.toJson(stepFiveChanged));
                            StorageService.set('wizardoperations', true);
                            $scope.stepTrigger('nextbtnclick');
                            //_setAccessDisabledForWorkflowSteps();
                        }, function (error) {
                            if ($rootScope.isOnline) {
                                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                            }
                        });
                    } else {
                        console.log('2');
                        genericService.getObjects(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.GET_ASSESSMENT_WORK_FLOW + '' + $rootScope.userDetails.company.companyType + '/' + companyId).then(function (data) {
                            $scope.originalStepFive = angular.copy(data);
                            $scope.preferences = angular.copy(data);
                            $scope.licensePreferencesFetched = true;
                            stepFiveChanged = !(angular.equals($scope.originalStepFive, $scope.preferences));
                            console.log(' $scope.CompanyManagement.licensePreferences ' + angular.toJson($scope.CompanyManagement.licensePreferences));
                            StorageService.set('wizardoperations', true);
                            $scope.stepTrigger('nextbtnclick');
                            //_setAccessDisabledForWorkflowSteps();
                        }, function (error) {
                            if ($rootScope.isOnline) {
                                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                            }
                        });
                    }

                }

                // Get all job source type

                genericService.getObjects('api/resume/resumesourcetypes').then(function (data) {
                    $scope.jobSourceType = angular.copy(data);
                }, function (error) {

                });

                function _setAccessDisabledForWorkflowSteps(){
                    if($rootScope.userDetails.roleScreenRef.role.name == 'CorporateAdmin' || $rootScope.userDetails.roleScreenRef.role.name == 'StaffingAdmin'){
                        $timeout(function(){
                            angular.element('.access').attr({'readonly': true, 'disabled': true});
                        }, 300);
                    }
                }

            }

            /**
             * This method is called to get Assesement workflow steps.
             */
            function getCurrentPlanDetails(companyId)  {
                  if ($scope.CompanyDetailsUpdateMode) {
                    //This condition is satisfied when company update is in progress.
                    console.log('Get current plan details for company during company updation.');
                     planService.getCurrentPlan(companyId, function(data) {
                         console.log('current plan');
                         console.log(data);
                         $scope.selectedPlan = data;
                         $scope.selectedPlan.planHeaderDto.planStartDateDisplay = $filter('fourdotfiveDateFormat')($scope.selectedPlan.planHeaderDto.planStartDate);
                         $scope.selectedPlan.planHeaderDto.planEndDateDisplay = $filter('fourdotfiveDateFormat')($scope.selectedPlan.planHeaderDto.planEndDate);
                         $scope.selectedPlan.planHeaderDto.unformattedStartDate = moment($scope.selectedPlan.planHeaderDto.planStartDate).toDate();
                         $scope.selectedPlan.planHeaderDto.unformattedEndDate = moment($scope.selectedPlan.planHeaderDto.planEndDate).toDate();
                         $scope.originalStepFour = angular.copy($scope.selectedPlan);
                         $scope.savedCurrentActivePlan = angular.copy($scope.selectedPlan);
                         $scope.planDetailsFetched = true;
                         $scope.setPlanPriorities();
                         $scope.isActivePlanExists = true;
                         $scope.isPlanChosen = true;
                         StorageService.set('wizardoperations', true);
                         $scope.stepTrigger('nextbtnclick');
                    }, function (error) {
                        $scope.isActivePlanExists = false; 
                        StorageService.set('wizardoperations', true);
                        $scope.stepTrigger('nextbtnclick');
                    });
                 }
                 else if (!$scope.clientOrBuUpdateInProgress) {
                    console.log('1: ' + $rootScope.userDetails.company.companyId);//Creation of company in progress
                    if (newlyCreatedCompanyId == null) {
                        newlyCreatedCompanyId == $rootScope.userDetails.company.companyId;
                    }
                    planService.getCurrentPlan(newlyCreatedCompanyId, function(data) {
                        console.log('current plan');
                        console.log(data);
                        $scope.selectedPlan = data;
                        $scope.selectedPlan.planHeaderDto.planStartDateDisplay = $filter('fourdotfiveDateFormat')($scope.selectedPlan.planHeaderDto.planStartDate);
                        $scope.selectedPlan.planHeaderDto.planEndDateDisplay = $filter('fourdotfiveDateFormat')($scope.selectedPlan.planHeaderDto.planEndDate);
                        $scope.selectedPlan.planHeaderDto.unformattedStartDate = moment($scope.selectedPlan.planHeaderDto.planStartDate).toDate();
                        $scope.selectedPlan.planHeaderDto.unformattedEndDate = moment($scope.selectedPlan.planHeaderDto.planEndDate).toDate();
                        $scope.originalStepFour = angular.copy($scope.selectedPlan);
                        $scope.savedCurrentActivePlan = angular.copy($scope.selectedPlan);
                        $scope.setPlanPriorities();
                         $scope.isActivePlanExists = true;
                        $scope.isPlanChosen = true;
                        StorageService.set('wizardoperations', true);
                        $scope.stepTrigger('nextbtnclick');
                   }, function (error) {
                       $scope.isActivePlanExists = false; 
                       StorageService.set('wizardoperations', true);
                       $scope.stepTrigger('nextbtnclick');
                   });
                }   
            }

            /**
             * This method is called to get list of Company Types.
             */
            $scope.getCompanyTypes = function () {
                companyTypes = ['Corporation', 'StaffingCompany'];
                companyTypes.forEach(function (element) {
                    //Puts in a space before an uppercase letter                    $scope.CompanyTypes.push(element.replace(/([A-Z])/g, ' $1').trim());
                    $scope.CompanyTypes.push(element);
                }, this);
                if ($rootScope.userDetails.company.companyId == null) {
                    console.log('Getting 4dot5 admins');
                    get4dot5admins();
                    getAllPlans();
                } else {
                    getAllPlans();
                    console.log('Not Getting 4dot5 admins');
                }
            };

            /**
             * This function is called to the list on industry group list
             */
            /*   INDUSTRY CODE         function getIndustryGroups() {
                            var clientIdToGetIndustryList = 0;
                            if ($scope.ClientOrBuUpdate) {
                                clientIdToGetIndustryList = $scope.CompanyManagement.company.id;
                                industryListOfCompany = $rootScope.userDetails.company.companyId;
                            } else if ($rootScope.userDetails.company.companyId != null) {
                                console.log('Update staffing company');
                                industryListOfCompany = $rootScope.userDetails.company.companyId;
                            }
                            console.log('Get industries for company: ' + industryListOfCompany);
                            if ($scope.TypeOfCompany == 'BusinessUnit' || $scope.TypeOfCompany == 'Client' || $scope.ClientOrBuUpdate) {
                                genericService.getObjects(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.GET_INDUSTRY_GROUPS + '/' + industryListOfCompany + '/' + clientIdToGetIndustryList).then(function (data) {
                                    console.log('GET_INDUSTRY_GROUPS Length of Data received is : ' + data.length);
                                    $scope.IndustryGroupList = [];
                                    industryMasterList = data;
                                    data.forEach(function (element) {
                                        $scope.IndustryGroupList.push(element.name);
                                        console.log('Element: ' + element.name + ' status: ' + element.selected);
                                        if (element.selected) {
                                            $scope.industryGroup = element.name;
                                            $scope.IndustrySubList = element.industryDtos;
                                        }
                                        element.industryDtos.forEach(function (elem) {
                                            if (elem.selected == true) {
                                                $scope.industriesInPanel.push(elem);
                                                elem.selected = false;
                                            }
                                        }, this);
                                    }, this);
                                    $scope.originalIndustriesPanel = angular.copy($scope.industriesInPanel);
                                    console.log('Industries panel: ' + angular.toJson($scope.industriesInPanel));
                                }, function (error) {
                                if (error.status === 500) {
                            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                        }
                                    console.log('error : ' + angular.toJson(error));
                                });
                            } else {//Get industries during staffing company update and create.
                                console.log('Get industries during staffing company update and create.');
                                genericService.getObjects(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.GET_INDUSTRY_GROUPS + '/' + industryListOfCompany).then(function (data) {
                                    console.log('GET_INDUSTRY_GROUPS Length of Data received is : ' + data.length);
                                    $scope.IndustryGroupList = [];
                                    industryMasterList = data;
                                    $scope.CompanyManagement.company.industryList = [];
                                    data.forEach(function (element) {
                                        $scope.IndustryGroupList.push(element.name);
                                        console.log('Element: ' + element.name + ' status: ' + element.selected);
                                        if (element.selected) {
                                            $scope.industryGroup = element.name;
                                            $scope.IndustrySubList = element.industryDtos;
                                        }
                                        element.industryDtos.forEach(function (elem) {
                                            if (elem.selected == true) {
                                                $scope.industriesInPanel.push(elem);
                                                elem.selected = false;
                                            }
                                        }, this);
                                    }, this);
                                    $scope.originalIndustriesPanel = angular.copy($scope.industriesInPanel);
                                    console.log('Industries panel: ' + angular.toJson($scope.industriesInPanel));
                                }, function (error) {
                                if (error.status === 500) {
                            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                        }
                                    console.log('error : ' + angular.toJson(error));
                                });
                            }
                        }*/

            /**
             * Change as per selection,get sublist of industries belonging to the group.
             */
            /*   INDUSTRY CODE         $scope.getIndustrySublist = function (industrygroup) {
                            if ($rootScope.userDetails.company.companyId != null && !$scope.ClientOrBuCreate) {
                                if ($scope.originalStepOne.industryGroup == industrygroup) {
                                    console.log('Setting api values');
                                }
                            }
                            console.log('Industry Group: ' + industrygroup);
                            if (!angular.equals($scope.selectedIndustryGroup, industrygroup)) {
                                industryMasterList.forEach(function (element) {
                                    if (element.name == industrygroup) {
                                        element.selected = true;
                                        $scope.IndustrySubList = element.industryDtos;
                                        console.log('Inside for each');
                                        $scope.selectedSubGroup = true;
                                    } else {
                                        if (angular.equals(element.name, $scope.selectedIndustryGroup)) {
                                            element.selected = false;
                                            element.industryDtos.forEach(function (industryDto) {
                                                if (industryDto.selected === true) {
                                                    industryDto.selected = false;
                                                }
                                            }, this);
                                        }
                                    }
                                }, this);
                            }

                            if ($scope.ClientOrBuCreate) {
                                industryMasterList.forEach(function (element) {
                                    console.log("5");
                                    if (angular.equals(element.name, industrygroup)) {
                                        console.log("6");
                                        element.selected = true;
                                        $scope.IndustrySubList = element.industryDtos;
                                        $scope.IndustrySubList.forEach(function (elem) {
                                            //console.log('Element is: ' + angular.toJson(elem));
                                            elem.selected = false;
                                        }, this);
                                        $scope.selectedSubGroup = true;
                                    }
                                }, this);
                            }

                            $scope.selectedIndustryGroup = angular.copy(industrygroup);
                        }*/


            /**
             * This method works for triggerng
             * button clicks and resetting timeout
             * and remaining variables to normal
             * @access for create, view and update
             */
            $scope.stepTrigger = function (target) {
                if (target === 'nextbtnclick') {
                    var nextbtnclick = $timeout(function () {
                        console.log('in stepTrigger nextbtnclick');
                        angular.element('.btn-next').triggerHandler('click');
                    }, 0).then(function () {
                        console.log('in stepTrigger nextbtnclick callback');
                        StorageService.remove('wizardoperations');

                        triggercount = 0;
                        $scope.formsubmitted = false;
                        $timeout.cancel(nextbtnclick);
                    });
                } else if (target === 'cancelflow') {
                    var steponeclick = $timeout(function () {
                        angular.element('#1').trigger('click');
                    }, 500).then(function () {
                        triggercount = 0;
                        $scope.formsubmitted = false;
                        StorageService.remove('wizardoperations');
                        $timeout.cancel(steponeclick);
                    });
                }
            };

            /**
             * Function to save Step 1 Company Details.
             */
            function saveCompanyDetails() {
                console.log('................' + $scope.CompanyManagement.company.companyType);
                if (StorageService.get('clientOrBuCreationInProgress') !== null) {
                    if ($rootScope.userDetails.company.companyType == 'Corporation') {
                        $scope.TypeOfCompany = 'BusinessUnit';
                        $scope.CompanyManagement.company.companyType = 'BusinessUnit';
                    } else if ($rootScope.userDetails.company.companyType == 'StaffingCompany') {
                        $scope.TypeOfCompany = 'Client';
                        $scope.CompanyManagement.company.companyType = 'Client';
                    }
                }

                console.log(' @@In saveCompanyDetails function: ' + stepOneChanged);
                if ((StorageService.get('companyDetails') == null || $scope.CompanyDetailsUpdateMode || $scope.clientOrBuCreationInProgress || $scope.clientOrBuUpdateInProgress) && stepOneChanged) {
                    //Building the SavedCompanyManagement object before saving.
                    savedCompanyManagementCopy.company = angular.copy($scope.SavedCompanyManagement.company);
                    console.log('Before Api CompanyManagement.company : ' + angular.toJson($scope.CompanyManagement.company));
                    $scope.SavedCompanyManagement.company = angular.copy($scope.CompanyManagement.company);
                    limitCharactersInName();
                    genericService.addObject(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.SAVE_COMPANY + '/' + loggedInUserDetails.company.companyId, $scope.SavedCompanyManagement).then(function (data) {
                        $scope.originalStepOne = angular.copy(data.company);
                        console.log('$scope.originalStepOne  ' + angular.toJson($scope.originalStepOne));
                        nextButtonFlag++;
                        $scope.SavedCompanyManagement.company = angular.copy(data.company);
                        console.log('Response: ' + angular.toJson('$scope.SavedCompanyManagement: ' + angular.toJson($scope.SavedCompanyManagement)));
                        $scope.CompanyManagement.company = data.company;
                        // INDUSTRY CODE                      $scope.originalIndustriesPanel = angular.copy($scope.originalStepOne.industryList);
                        // INDUSTRY CODE                     delete $scope.originalStepOne['industryGroup'];
                        // INDUSTRY CODE                   delete $scope.originalStepOne['industryList'];

                        $scope.CompanyManagement.company.name = data.company.name;
                        $scope.CompanyManagement.company.id = data.company.id;
                        $scope.CompanyManagement.companyState = data.companyState;
                        $scope.SavedCompanyManagement.company.id = data.company.id;
                        $scope.SavedCompanyManagement.companyState = data.companyState;
                        newlyCreatedCompanyId = data.company.id;
                        $rootScope.newlyCreatedCompanyId = newlyCreatedCompanyId;

                        if ($scope.TypeOfCompany != 'Client' && $scope.TypeOfCompany != 'BusinessUnit') {
                            $rootScope.userDetails.company.companyId = newlyCreatedCompanyId;
                            $rootScope.userDetails.company.companyName = data.company.name;
                            $rootScope.userDetails.company.companyType = data.company.companyType;
                            $rootScope.userDetails.company.companyState = data.companyState;
                            $rootScope.userDetails.company.companySelected = true;
                            StorageService.setCookie('userDetails', angular.copy($rootScope.userDetails), {'expires': new Date(Date.now() + $rootScope.tokenvalidity)});
                        }
                        if ($scope.TypeOfCompany == 'Client' || $scope.TypeOfCompany == 'BusinessUnit') {
                            console.log('Client details saved, call assignClientToStaffingCompany function');
                            assignClientToStaffingCompany('clickonclientnext');
                        }
                        if ($scope.clientOrBuUpdateInProgress) {
                            console.log('Step 1 details changed, so api call made.Updated client details : ');
                            $scope.editCompanyName = data.company.name.substring(0, 57);
                            $scope.editCompanyNameOnHover = data.company.name;
                            if (data.company.name.length > 57) {
                                $scope.editCompanyName = $scope.editCompanyName + '...';
                            }
                        }
                        if (!($scope.TypeOfCompany == 'Client') && !($scope.TypeOfCompany == 'BusinessUnit')) {
                            console.log('Saved company Step 1 details successfully.');
                            companyStepOneSaved = true;
                            StorageService.set('wizardoperations', true);

                            $scope.stepTrigger('nextbtnclick');
                        }

                    }, function (error) {
                        triggercount = 0;
                        if (error.statusCode === "409") {
                            $scope.errorOcurred = true;
                            if (angular.isDefined($scope.CompanyManagement.companyState)) {
                                if (error.developerMsg === "101") {
                                    $scope.CompanyManagement.company.companyShortName = $scope.originalStepOne.companyShortName;
                                } else if (error.developerMsg === "102") {
                                    $scope.CompanyManagement.company.name = $scope.originalStepOne.name;
                                } else if (error.developerMsg === "100") {
                                    $scope.CompanyManagement.company.companyShortName = $scope.originalStepOne.companyShortName;
                                    $scope.CompanyManagement.company.name = $scope.originalStepOne.name;
                                } else {
                                    $scope.CompanyManagement.company.companyShortName = $scope.originalStepOne.companyShortName;
                                    $scope.CompanyManagement.company.name = $scope.originalStepOne.name;
                                }
                            } else {
                                if (error.developerMsg === "101") {
                                    $scope.CompanyManagement.company.companyShortName = '';
                                } else if (error.developerMsg === "102") {
                                    $scope.CompanyManagement.company.name = '';
                                } else if (error.developerMsg === "100") {
                                    $scope.CompanyManagement.company.companyShortName = '';
                                    $scope.CompanyManagement.company.name = '';
                                } 
                            }
                        }
                        if ($rootScope.isOnline) {
                            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                        }
                        $scope.SavedCompanyManagement.company = angular.copy(savedCompanyManagementCopy.company);
                    });
                } else if (!stepOneChanged) {
                    companyStepOneSaved = true;
                    console.log('companyStepOneSaved');
                    StorageService.set('wizardoperations', true);
                    triggercount = 0;
                }
            }

            /**
             * Function to save Step 2 details(Company Contact Person Details)
             */
            function saveContactDetails() {
                if (newlyCreatedCompanyId == null && $scope.clientOrBuUpdateInProgress) {
                    newlyCreatedCompanyId = $scope.CompanyManagement.company.id;
                }
                if ((StorageService.get('companyDetails') == null || $scope.CompanyDetailsUpdateMode || $scope.clientOrBuCreationInProgress || $scope.clientOrBuUpdateInProgress) && stepTwoChanged) {
                    //Building the SavedCompanyManagement object before saving.
                    savedCompanyManagementCopy.contactList = angular.copy($scope.SavedCompanyManagement.contactList);
                    $scope.SavedCompanyManagement.contactList = angular.copy($scope.CompanyManagement.contactList);
                    limitCharactersInName();
                    genericService.addObject(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.SAVE_COMPANY + '/' + newlyCreatedCompanyId, $scope.SavedCompanyManagement).then(function (data) {
                        nextButtonFlag++;
                        $scope.originalStepTwo = angular.copy(data.contactList);
                        $scope.SavedCompanyManagement.contactList = angular.copy(data.contactList);
                        $scope.CompanyManagement.contactList = angular.copy(data.contactList);
                        if ($scope.clientOrBuUpdateInProgress) {
                            console.log('Step 2 details changed, so api call made.Updated client contact details : ');
                            $scope.laststep = true;
                            // alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.UPDATESUCCESS, 'success');
                        }
                        if ($scope.clientOrBuCreationInProgress) {
                            console.log('Step 2 details changed, so api call made.Saved client contact details : ');
                            $scope.laststep = true;
                            // alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.CLIENT+" '<b><i class='wrap-word'>"+$scope.SavedCompanyManagement.company.name+"</i></b>' "+MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.CREATEDRAFTSUCCESS, 'success')
                        }
                        if ($rootScope.userDetails.company.companyType == 'Host') {
                            $scope.laststep = true;
                            $scope.dataLastLabel = 'Complete';
                        }
                        if ($scope.TypeOfCompany == 'Client' || $scope.TypeOfCompany == 'BusinessUnit' || $scope.clientOrBuUpdateInProgress || $scope.clientOrBuCreationInProgress || StorageService.get('SelectedHostCompany') != null) {
                            if (!$scope.licensePreferencesFetched) {
                                getAssessmentWorkFlow($rootScope.userDetails.company.companyId);
                            } else {
                                StorageService.set('wizardoperations', true);
                                $scope.stepTrigger('nextbtnclick');
                            }
                        } else {
                            console.log('Saved Step 2 details successfully.');
                            // alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.CREATEDRAFTSUCCESS, 'success');
                            companyStepTwoSaved = true;
                            StorageService.set('wizardoperations', true);
                            $scope.stepTrigger('nextbtnclick');
                        }
                    }, function (error) {
                        if ($rootScope.isOnline) {
                            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                        }
                        console.log('now assigning original obj to transaction and saved objects');
                        $scope.SavedCompanyManagement.contactList = angular.copy(savedCompanyManagementCopy.contactList);
                    });
                } else if (!stepTwoChanged) {
                    console.log('Step 2 is not changed, so no api call made.');
                    if (StorageService.get('SelectedHostCompany') != null) {
                        $scope.laststep = true;
                        $scope.dataLastLabel = 'Complete';
                    }
                    if ($scope.TypeOfCompany == 'Client' || $scope.TypeOfCompany == 'BusinessUnit' || $rootScope.userDetails.company.companyType == 'Host') {
                        $scope.laststep = true;
                        getAssessmentWorkFlow($rootScope.userDetails.company.companyId);
                    } else {
                        companyStepTwoSaved = true;
                        triggercount = 0;
                        StorageService.set('wizardoperations', true);
                    }
                }
            }

            /**
             * Function to save Step 3 details(Company 4Dot5 Admin Details)
             */
            function saveFourDotFiveAdminDetails() {
                console.log('In function saveFourDotFiveAdminDetails: ');
                if ((StorageService.get('companyDetails') == null || $scope.CompanyDetailsUpdateMode) && stepThreeChanged) {

                    //Building the SavedCompanyManagement object before saving.
                    savedfourDotFiveAdminCopy = angular.copy($scope.SavedfourDotFiveAdmin);
                    $scope.SavedfourDotFiveAdmin = angular.copy($scope.selectedFourDotFiveAdmin);
                    console.log('before saving ' + angular.toJson($scope.SavedfourDotFiveAdmin));
                    limitCharactersInName();
                    genericService.addObject(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.UPDATE_COMPANY_FOR_FOURDOTFIVE_ADMIN + '/' + $scope.SavedfourDotFiveAdmin[0].userId + '/' + $rootScope.userDetails.company.companyId + '?oldfourdotfiveadmin=' + oldfourdotfiveadminid, $scope.SavedfourDotFiveAdmin).then(function (data) {
                        //               console.log('Success: ' + angular.toJson(data));
                        nextButtonFlag++;
                        $scope.originalStepThree = angular.copy(data.fourDotFiveAdmin);
                        $scope.SavedfourDotFiveAdmin = angular.copy(data.fourDotFiveAdmin);

                        if ($scope.CompanyDetailsUpdateMode == true) {
                            $scope.CompanyManagement.fourDotFiveAdmin = angular.copy(data.fourDotFiveAdmin);
                            $scope.CompanyManagementReset.fourDotFiveAdmin = angular.copy(data.fourDotFiveAdmin);
                            console.log('Step 3 is changed, so api call made.Four dot five admin updated.' + angular.toJson($scope.CompanyManagement.fourDotFiveAdmin));
                            // alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.UPDATESUCCESS, 'success')
                        } else {
                            console.log('Step 3 is changed, so api call made.Four dot five admin saved successfully.');
                            //alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.CREATEDRAFTSUCCESS, 'success');
                        }
                        companyStepThreeSaved = true;
                        getCurrentPlanDetails(loggedInUserDetails.company.companyId);

                        $scope.laststep = true;
                    }, function (error) {
                        if ($rootScope.isOnline) {
                            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                        }
                        console.log('now assigning original obj to transaction and saved objects');
                        $scope.SavedfourDotFiveAdmin = angular.copy(savedfourDotFiveAdminCopy);
                    });
                } else if (!stepThreeChanged) {
                    console.log('Step 3 is not changed, so no api call made.');
                    $scope.laststep = true;
                    if (!$scope.licensePreferencesFetched) {
                        console.log('Fetching company plan details:');
                        getCurrentPlanDetails(loggedInUserDetails.company.companyId);
                    } else {
                        StorageService.set('wizardoperations', true);
                        $scope.stepTrigger('nextbtnclick');
                    }
                }
            }
            
            /**
             * Function to save Step 4 details(Company Plan)
             */
            function savePlanDetails() {
                console.log('In function save Plan Details: ');
                if ((StorageService.get('companyDetails') == null || $scope.CompanyDetailsUpdateMode) && stepFourChanged) {

                    savedCurrentActivePlanCopy = angular.copy($scope.selectedPlan);
                    $scope.savedCurrentActivePlan = angular.copy($scope.selectedPlan);
                    planService.savePlan($scope.selectedPlan, function (data) {
                        nextButtonFlag++;
                        data.planHeaderDto.planStartDateDisplay = $filter('fourdotfiveDateFormat')(data.planHeaderDto.planStartDate);
                        data.planHeaderDto.planEndDateDisplay = $filter('fourdotfiveDateFormat')(data.planHeaderDto.planEndDate);
                        data.planHeaderDto.unformattedStartDate = moment(data.planHeaderDto.planStartDate).toDate();
                        data.planHeaderDto.unformattedEndDate = moment(data.planHeaderDto.planEndDate).toDate();
                        $scope.originalStepFour = angular.copy(data);
                        $scope.savedCurrentActivePlan = angular.copy(data);
                        $scope.selectedPlan = angular.copy(data);
                        $scope.isActivePlanExists = true;
                        companyStepFourSaved = true;
                        getAssessmentWorkFlow(loggedInUserDetails.company.companyId);
                        $scope.laststep = true;
                    }, function (error) {
                        if ($rootScope.isOnline) {
                            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                        }
                        // console.log('now assigning original obj to transaction and saved objects');
                        // $scope.SavedfourDotFiveAdmin = angular.copy(savedfourDotFiveAdminCopy);
                    });
                } else if (!stepFourChanged) {
                    console.log('Step 4 is not changed, so no api call made.');
                    $scope.laststep = true;
                    if (!$scope.licensePreferencesFetched) {
                        console.log('Fetching licence prefs:');
                        getAssessmentWorkFlow(loggedInUserDetails.company.companyId);
                    } else {
                        StorageService.set('wizardoperations', true);
                        $scope.stepTrigger('nextbtnclick');
                    }
                }
            }

            /**
             * Function to limit characters in name.
             */
            function limitCharactersInName() {
                delimitedCompanyOrClientName = $scope.SavedCompanyManagement.company.name;
                //                if ($scope.SavedCompanyManagement.company.name.length > 15) {
                //                    delimitedCompanyOrClientName = delimitedCompanyOrClientName + '....';
                //                }
            }

            /**
             *
             */
            function assignClientToStaffingCompany(clickfromclientsaveornext) {
                //This condition satisfy when a company from popup is selected and user tries to edit a client/bu for selected company
                console.log('assignClientToStaffingCompany function called.');
                if ($scope.clientOrBuUpdateInProgress) {
                    newlyCreatedCompanyId = $scope.CompanyManagement.company.id;
                    console.log('Taking updated client id');
                }
                if (StorageService.get('companyDetails') != null) {
                    if (angular.isDefined(StorageService.get('companyDetails').companyId)) {
                        assignClient2StaffingCompany = StorageService.get('companyDetails').companyId;
                    } else {
                        assignClient2StaffingCompany = StorageService.get('companyDetails');
                    }

                    console.log('StorageService.get != null: ' + assignClient2StaffingCompany);
                } else {
                    assignClient2StaffingCompany = loggedInUserDetails.company.companyId;
                    console.log('StorageService.get != null: ' + assignClient2StaffingCompany);
                }
                genericService.addObject(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.ASSIGN_CLIENT_TO_STAFFING_COMPANY + '/' + assignClient2StaffingCompany + '/' + newlyCreatedCompanyId).then(function (data) {
                    console.log('Client assigned to staffing company.');
                    nextButtonFlag++;
                    if ($scope.clientOrBuUpdateInProgress) {
                        //  alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.UPDATESUCCESS, 'success')
                    } else if ($scope.clientOrBuCreationInProgress && clickfromclientsaveornext == 'clickonclientsave') {
                        alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.CLIENT + " '<b><i class='wrap-word'>" + delimitedCompanyOrClientName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.CREATEDRAFTSUCCESS, 'success')
                    }
                    companyStepOneSaved = true;
                    if (!(clickfromclientsaveornext == 'clickonclientsave')) {
                        StorageService.set('wizardoperations', true);
                        $scope.stepTrigger('nextbtnclick');
                    }
                }, function (error) {
                    console.log('assignClientToStaffingCompany error with trigger count : ' + triggercount);
                    if ((error.status === 500) && $rootScope.isOnline) {
                        alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                    } else {
                        if ($scope.clientOrBuUpdateInProgress) {
                            // alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.UPDATESUCCESS, 'success')
                        } else if ($scope.clientOrBuCreationInProgress && clickfromclientsaveornext == 'clickonclientsave') {
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.CLIENT + " '<b><i class='wrap-word'>" + delimitedCompanyOrClientName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.CREATEDRAFTSUCCESS, 'success')
                        }
                        companyStepOneSaved = true;
                        if (!(clickfromclientsaveornext == 'clickonclientsave')) {
                            StorageService.set('wizardoperations', true);
                            $scope.stepTrigger('nextbtnclick');
                        }
                    }
                });
            }

            /**
             * Change state of Staffing company to 'Active'.
             */
            function changeCompanyState() {
                console.log('changeCompanyState function called.');
                genericService.addObject(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.CHANGE_COMPANYSTATE + '/' + newlyCreatedCompanyId, $scope.CompanyManagement).then(function (data) {
                    nextButtonFlag++;
                    console.log('Selected Company ClientOrBus Data received is : ' + angular.toJson(data));
                    $rootScope.userDetails.company.companyState = data.companyState;
                    StorageService.setCookie('userDetails', angular.copy($rootScope.userDetails), {'expires': new Date(Date.now() + $rootScope.tokenvalidity)});
                }, function (error) {
                    if ($rootScope.isOnline) {
                        alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                    }
                });
            }

            /**
             * Function to save Step 5 details(Company assessement Flow Details)
             */
            function saveAssessementFlowDetails() {
                var clientOrCompanyLicensePrefsUrl;
                console.log('saveAssessementFlowDetails function called.');
                //This condition satisfy when a company from popup is selected and user tries to create a client/bu for selected company
                if (StorageService.get('companyDetails') != null && newlyCreatedCompanyId == null && !$scope.clientOrBuUpdateInProgress) {
                    if (angular.isDefined(StorageService.get('companyDetails').companyId)) {
                        newlyCreatedCompanyId = StorageService.get('companyDetails').companyId;
                    } else {
                        newlyCreatedCompanyId = StorageService.get('companyDetails');
                    }
                }

                //This condition satisfy when a company from popup is selected and user tries to edit a client/bu for selected company
                if (StorageService.get('companyDetails') == null && newlyCreatedCompanyId == null && $scope.CompanyManagement.company.companyType != 'Host') {
                    if (StorageService.get('companyToBeUpdated') == null) {
                        newlyCreatedCompanyId = $rootScope.userDetails.company.companyId;
                    } else {
                        newlyCreatedCompanyId = StorageService.get('companyToBeUpdated').company.id;
                    }
                    // console.log('Update Client set licence preferences');
                }

                //This condition will satisfy if editing host company
                if ($scope.CompanyManagement.company.companyType == 'Host') {
                    newlyCreatedCompanyId = $rootScope.userDetails.company.companyId;
                    console.log('Host: ' + newlyCreatedCompanyId);
                }

                //This condition will satisfy if editing a client/bu
                if ($scope.clientOrBuUpdateInProgress) {
                    newlyCreatedCompanyId = $scope.CompanyManagement.company.id;
                    console.log('Update Client set licence preferences: ' + newlyCreatedCompanyId);
                }

                console.log('Save for companyid: ' + newlyCreatedCompanyId);

                if ((StorageService.get('companyDetails') == null && !$scope.clientOrBuCreationInProgress && !$scope.clientOrBuUpdateInProgress )) {
                    newCompanyCreation = true;
                }

                if ((StorageService.get('companyDetails') == null || $scope.CompanyDetailsUpdateMode || $scope.clientOrBuCreationInProgress || $scope.clientOrBuUpdateInProgress) && stepFiveChanged) {
                    if ($scope.clientOrBuCreationInProgress || $scope.clientOrBuUpdateInProgress) {
                        clientOrCompanyLicensePrefsUrl = COMPANYMANAGEMENTCONSTANTS.CONTROLLER.SET_LICENCE_PREFERENCES_FOR_CLIENT_BU + '/' + $rootScope.userDetails.company.companyId + '/' + newlyCreatedCompanyId;
                    } else {
                        clientOrCompanyLicensePrefsUrl = COMPANYMANAGEMENTCONSTANTS.CONTROLLER.SET_LICENCE_PREFERENCES + '/' + $scope.CompanyManagement.company.companyType + '/' + newlyCreatedCompanyId;
                    }
                    //Building the SavedCompanyManagement object before saving.
                    savedAssessmentWorkFlowCopy = angular.copy($scope.SavedAssessmentWorkFlow);
                    $scope.SavedAssessmentWorkFlow = angular.copy($scope.preferences);
                    tempObj = angular.copy($scope.originalStepFive);
                    limitCharactersInName();
                    genericService.addObject(clientOrCompanyLicensePrefsUrl, $scope.SavedAssessmentWorkFlow).then(function (data) {
                        //              console.log('Success: ' + angular.toJson(data));
//                        nextButtonFlag++;
                        $scope.CompanyManagement.licensePreferences = angular.copy(data.licensePreferences);
                        $scope.originalStepFive = angular.copy(data.licensePreferences);
                        $scope.SavedAssessmentWorkFlow = angular.copy(data.licensePreferences);

                        if ($scope.CompanyDetailsUpdateMode == true && stepFiveChanged) {
                            console.log('Updated step 5 Company Licence Preferences details successfully.');
                            if ($rootScope.userDetails.roleScreenRef.role.name != 'CorporateAdmin' && $rootScope.userDetails.roleScreenRef.role.name != 'StaffingAdmin') {
                                console.log('1');
                                if (!angular.equals(tempObj, $scope.SavedAssessmentWorkFlow) || (nextButtonFlag > 0) || (saveButtonFlag > 0)) {
                                    alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.COMPANY + " '<b><i class='wrap-word'>" + delimitedCompanyOrClientName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.UPDATESUCCESS, 'success');
                                } else {
                                    alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.NOTHING_TO_UPDATED, 'success');
                                }
                            }
                        } else if (!$scope.clientOrBuUpdateInProgress && $scope.TypeOfCompany == 'Client') {
                            console.log('2');
                            console.log('Saved step 3 Client/BU Licence Preferences details successfully.');
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.CLIENT + " '<b><i class='wrap-word'>" + delimitedCompanyOrClientName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.CREATESUCCESS, 'success')
                        }  else if (!$scope.clientOrBuUpdateInProgress && $scope.TypeOfCompany == 'BusinessUnit') {
                            console.log('Saved step 3 Client/BU Licence Preferences details successfully.');
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.BU.BU + " '<b><i class='wrap-word'>" + delimitedCompanyOrClientName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.BU.CREATESUCCESS, 'success')
                        } else if (angular.equals($rootScope.userDetails.roleScreenRef.role.name, 'CorporateAdmin') || angular.equals($rootScope.userDetails.roleScreenRef.role.name, 'StaffingAdmin')) {
                            console.log('Company updated successfully');
                        } else if (StorageService.get('companyDetails') == null && !$scope.CompanyDetailsUpdateMode) {
                            console.log('3');
                            console.log('Saved step 5 Company Licence Preferences details successfully.');
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.COMPANY + " '<b><i class='wrap-word'>" + delimitedCompanyOrClientName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.CREATESUCCESS, 'success')
                        }
                        if ($scope.clientOrBuUpdateInProgress && stepFiveChanged) {
                            console.log('4');
                            console.log('Updated step 3 Client/BU Licence Preferences details successfully.');
                            if (!angular.equals(tempObj, $scope.SavedAssessmentWorkFlow) || (nextButtonFlag > 0) || (saveButtonFlag > 0)) {
                                alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.CLIENT + " '<b><i class='wrap-word'>" + delimitedCompanyOrClientName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.UPDATESUCCESS, 'success')
                            } else {
                                alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.NOTHING_TO_UPDATED, 'success');
                            }
                        }


                        companyStepFiveSaved = true;
                        $scope.laststep = true;
                        StorageService.set('wizardoperations', true);
                        $scope.stepTrigger('nextbtnclick');

                        if ($scope.CompanyManagement.companyState == 'Draft' || $scope.CompanyManagement.companyState == null) {
                            console.log('Saved Company Object: ' + angular.toJson($scope.SavedCompanyManagement));
                            changeCompanyState();
                        }
                        if ($scope.TypeOfCompany == 'Client' || $scope.TypeOfCompany == 'BusinessUnit') {
                            $scope.lastStepCompleted = true;
                            console.log('Moving out to clientmanagement(List of clients): ' + $scope.lastStepCompleted);
                            $timeout(function () {
                                $state.go('missioncontrol.clientmanagement', null, {reload: false});
                            }, 300);
                        } else if ($scope.CompanyManagement.company.companyType == 'Host') {
                            StorageService.set('companyDetails', newlyCreatedCompanyId);
                            $scope.lastStepCompleted = true;
                            $timeout(function () {
                                $state.go('missioncontrol.dashboardsuperusermodule', {companyObject: 'companyObject'}, {reload: false});
                            }, 300);
                        } else {
                            StorageService.set('companyDetails', newlyCreatedCompanyId);
                            $scope.lastStepCompleted = true;
                            $timeout(function () {
                                $state.go('missioncontrol.dashboard', {companyObject: 'companyObject'}, {reload: false});
                            }, 300);
                            // $state.go('missioncontrol.dashboard', { companyObject: 'companyObject' }, { reload: false });
                        }

                    }, function (error) {
                        if ($rootScope.isOnline) {
                            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                        }

                        if (error.statusCode == "409") {
                            getAssessmentWorkFlow($rootScope.userDetails.company.companyId);
                        } else {
                            $scope.SavedAssessmentWorkFlow = angular.copy(savedAssessmentWorkFlowCopy);
                        }
                    });
                } else if (!stepFiveChanged) {
                    console.log('Step 5 is not changed, so no api call made.');
                    if ($scope.CompanyManagement.companyState == 'Draft' || $scope.CompanyManagement.companyState == null) {
                        console.log('Saved Company Object: ' + angular.toJson($scope.SavedCompanyManagement));
                        changeCompanyState();
                    }
                    companyStepFiveSaved = true;
                    $scope.laststep = true;
                    StorageService.set('wizardoperations', true);
                    if ($scope.TypeOfCompany == 'Client' || $scope.TypeOfCompany == 'BusinessUnit') {
                        $scope.lastStepCompleted = true;
                        console.log('Moving out to clientmanagement: ' + $scope.lastStepCompleted);
                        console.log('client before redirecting...' + $scope.ClientOrBuUpdate);
                        if (!$scope.ClientOrBuUpdate) {
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.CLIENT + " '<b><i class='wrap-word'>" + $scope.SavedCompanyManagement.company.name + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.CREATESUCCESS, 'success');
                            $timeout(function () {
                                $state.go('missioncontrol.clientmanagement', null, {reload: false});
                            }, 300);
                        } else {
                            console.log("Update else");
                            // alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.CLIENT + " '<b><i class='wrap-word'>" + $scope.SavedCompanyManagement.company.name + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.UPDATESUCCESS, 'success')
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.NOTHING_TO_UPDATED, 'success');
                            $timeout(function () {
                                $state.go('missioncontrol.clientmanagement', null, {reload: false});
                            }, 300);
                        }
                    } else if ($scope.CompanyManagement.company.companyType == 'Host') {
                        console.log('for host company on complete when nothing changed showing message');
                        console.log('nextButtonFlag : ' + nextButtonFlag);
                        console.log('saveButtonFlag : ' + saveButtonFlag);
                        if ((nextButtonFlag > 0) || (saveButtonFlag > 0)) {
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.COMPANY + " '<b><i class='wrap-word'>" + delimitedCompanyOrClientName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.UPDATESUCCESS, 'success');
                        } else {
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.NOTHING_TO_UPDATED, 'success')
                        }
                        StorageService.set('companyDetails', newlyCreatedCompanyId);
                        $scope.lastStepCompleted = true;
                        $timeout(function () {
                            $state.go('missioncontrol.dashboardsuperusermodule', {companyObject: 'companyObject'}, {reload: false});
                        }, 300);
                    } else {
                        StorageService.set('companyDetails', newlyCreatedCompanyId);
                        $scope.lastStepCompleted = true;
                        console.log('company before redirecting...' + $scope.CompanyDetailsUpdateMode);
                        if ($scope.CompanyDetailsUpdateMode) {
                            //alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.COMPANY + " '<b><i class='wrap-word'>" + delimitedCompanyOrClientName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.UPDATESUCCESS, 'success')
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.NOTHING_TO_UPDATED, 'success');
                            $timeout(function () {
                                $state.go('missioncontrol.dashboard', {companyObject: 'companyObject'}, {reload: false});
                            }, 300);
                        } else if (!angular.isDefined($scope.CompanyDetailsUpdateMode)) {
                            console.log('Creation of company(Staffing/Corporate)');
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.COMPANY + " '<b><i class='wrap-word'>" + delimitedCompanyOrClientName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.CREATESUCCESS, 'success');
                            $timeout(function () {
                                $state.go('missioncontrol.dashboard', {companyObject: 'companyObject'}, {reload: false});
                            }, 300);
                        }
                    }
                } else {
                    companyStepFiveSaved = true;
                    $scope.laststep = true;
                    StorageService.set('wizardoperations', true);
                    console.log('in else');
                    $state.go('missioncontrol.dashboard', {companyObject: 'companyObject'}, {reload: false});
                }

            }


            $scope.saveStepsDataAfterTimeout = function (Company, ContactList, FourDotFiveAdmin) {
                console.log('$timeout 1000');
                $timeout(function () {
                    $scope.saveStepsData(Company, ContactList, FourDotFiveAdmin);
                }, 1000);
            };

            $scope.saveStepsData = function (Company, ContactList, FourDotFiveAdmin) {
                console.log('$scope.SavedCompanyManagement ' + angular.toJson($scope.SavedCompanyManagement));
                nextButtonClicked = false;
                saveButtonClicked = true;
                // INDUSTRY CODE console.log('save value: ' + $scope.industriesInPanel.length);
                var currentStep = getCurrentStep();
                if (currentStep == 3 && ($scope.TypeOfCompany == 'Client' || $scope.TypeOfCompany == 'BusinessUnit' || StorageService.get('SelectedHostCompany') != null)) {
                    currentStep = 5;
                }

                valid = false;
                switch (currentStep) {
                    case 1:
                        stepOneChanged = !(angular.equals($scope.originalStepOne, $scope.CompanyManagement.company));//Checks if any changes.
                        console.log(' originalStepOne ' + angular.toJson($scope.originalStepOne));
                        console.log(' CompanyManagement.company ' + angular.toJson($scope.CompanyManagement.company));
                        if ($scope.CompanyManagement.company.address2 != null) {
                            isAdressValid = !angular.equals($scope.CompanyManagement.company.address1.toUpperCase(), $scope.CompanyManagement.company.address2.toUpperCase()) && ($scope.CompanyManagement.company.address1 != $scope.CompanyManagement.company.address2);
                            console.log('if  ' + isAdressValid);
                        } else {
                            console.log('else');
                            isAdressValid = $scope.CompanyManagement.company.address1 != $scope.CompanyManagement.company.address2;
                        }
                        valid = $scope.form.companydetailsform.$valid && isAdressValid && $scope.validzipcode && angular.isDefined($scope.validzipcode) && (!$scope.zipcodeCalling || ($scope.zipcodeCalling && $scope.zipcodeCallCompleted));//Checks validations.;
                        console.log('validations are proper: ' + valid + '  ' + $scope.form.companydetailsform.$valid + ' ' + $scope.form.companydetailsform.zipcode.$valid);
                        console.log('Step 1 is changed: ' + stepOneChanged + ' ' + $scope.validzipcode + ' ' + $scope.CompanyManagement.company.zipcode + '  ' + angular.equals($scope.originalStepOne, $scope.CompanyManagement.company));
                        if (stepOneChanged && valid) {
                            if (!angular.isDefined($scope.CompanyManagement.company.companyType)) {
                                if ($rootScope.userDetails.company.companyType == 'Corporation') {
                                    $scope.CompanyManagement.company.companyType = 'BusinessUnit';
                                } else if ($rootScope.userDetails.company.companyType == 'StaffingCompany') {
                                    $scope.CompanyManagement.company.companyType = 'Client';
                                }
                            }
                            console.log('Creating: ' + $scope.CompanyManagement.company.companyType);
                            //Building the SavedCompanyManagement object before saving.
                            savedCompanyManagementCopy.company = angular.copy($scope.SavedCompanyManagement.company);
                            $scope.SavedCompanyManagement.company = angular.copy($scope.CompanyManagement.company);
                            console.log('Saving: ' + angular.toJson($scope.SavedCompanyManagement));
                            genericService.addObject(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.SAVE_COMPANY + '/' + loggedInUserDetails.company.companyId, $scope.SavedCompanyManagement).then(function (data) {
                                $scope.originalStepOne = angular.copy(data.company);
                                saveButtonFlag++;
                                console.log('$scope.originalStepOne  ' + angular.toJson($scope.originalStepOne));

                                //Updating the SavedCompanyManagement object after saving.
                                $scope.SavedCompanyManagement.company = angular.copy(data.company);
                                limitCharactersInName();
                                saveButtonClicked = false;
                                console.log('Response: ' + angular.toJson($scope.SavedCompanyManagement));

                                $scope.CompanyManagement.company = data.company;
                                $scope.CompanyManagement.company.name = data.company.name;
                                $scope.CompanyManagement.companyState = data.companyState;
                                $scope.CompanyManagement.companyState = data.companyState;
                                $scope.SavedCompanyManagement.company.id = data.company.id;
                                $scope.SavedCompanyManagement.companyState = data.companyState;
                                newlyCreatedCompanyId = data.company.id;
                                if ($scope.TypeOfCompany != 'Client' && $scope.TypeOfCompany != 'BusinessUnit') {
                                    $rootScope.userDetails.company.companyId = data.company.id;
                                    $rootScope.userDetails.company.companyName = data.company.name;
                                    $rootScope.userDetails.company.companyType = data.company.companyType;
                                    $rootScope.userDetails.company.companyState = data.companyState;
                                    $rootScope.userDetails.company.companySelected = true;
                                    StorageService.setCookie('userDetails', angular.copy($rootScope.userDetails), {'expires': new Date(Date.now() + $rootScope.tokenvalidity)});
                                }
                                companyStepOneSaved = true;
                                if (($scope.TypeOfCompany == 'Client' || $scope.TypeOfCompany == 'BusinessUnit') && $scope.clientOrBuUpdateInProgress) {
                                    console.log('Updated client details');
                                    $scope.editCompanyName = data.company.name.substring(0, 57);
                                    $scope.editCompanyNameOnHover = data.company.name;
                                    if (data.company.name.length > 57) {
                                        $scope.editCompanyName = $scope.editCompanyName + '...';
                                    }
                                    alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.CLIENT + " '<b><i class='wrap-word'>" + delimitedCompanyOrClientName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.UPDATESUCCESS, 'success')
                                }
                                if (($scope.TypeOfCompany == 'Client' || $scope.TypeOfCompany == 'BusinessUnit') && $scope.clientOrBuCreationInProgress) {
                                    console.log('Assigning client to StaffingCompany function call.');
                                    assignClientToStaffingCompany('clickonclientsave');
                                    //alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.CREATESUCCESS, 'success')
                                }

                                if (!($scope.CompanyDetailsUpdateMode) && $scope.TypeOfCompany == 'Company') {
                                    alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.COMPANY + " '<b><i class='wrap-word'>" + delimitedCompanyOrClientName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.COMPANYDRAFTSUCCESS, 'success')
                                }
                                if ($scope.CompanyDetailsUpdateMode) {
                                    alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.COMPANY + " '<b><i class='wrap-word'>" + delimitedCompanyOrClientName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.UPDATESUCCESS, 'success')
                                }
                            }, function (error) {
                                if (error.statusCode === "409") {
                                    $scope.errorOcurred = true;
                                    if (angular.isDefined($scope.CompanyManagement.companyState)) {
                                        if (error.developerMsg === "101") {
                                            $scope.CompanyManagement.company.companyShortName = $scope.originalStepOne.companyShortName;
                                        } else if (error.developerMsg === "102") {
                                            $scope.CompanyManagement.company.name = $scope.originalStepOne.name;
                                        } else if (error.developerMsg === "100") {
                                            $scope.CompanyManagement.company.companyShortName = $scope.originalStepOne.companyShortName;
                                            $scope.CompanyManagement.company.name = $scope.originalStepOne.name;
                                        } else {
                                            $scope.CompanyManagement.company.companyShortName = $scope.originalStepOne.companyShortName;
                                            $scope.CompanyManagement.company.name = $scope.originalStepOne.name;
                                        }
                                    } else {
                                        if (error.developerMsg === "101") {
                                            $scope.CompanyManagement.company.companyShortName = '';
                                        } else if (error.developerMsg === "102") {
                                            $scope.CompanyManagement.company.name = '';
                                        } else if (error.developerMsg === "100") {
                                            $scope.CompanyManagement.company.companyShortName = '';
                                            $scope.CompanyManagement.company.name = '';
                                        }
                                    }
                                }
                                if ($rootScope.isOnline) {
                                    alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                                }
                                $scope.SavedCompanyManagement.company = angular.copy(savedCompanyManagementCopy.company);
                            });
                        } else if (!valid) {
                            console.log('else if .....');
                            $scope.step1formsubmitted = true;
                            triggercount = 0;
                        } else if (valid && (!stepOneChanged)) {
                            console.log('else if ..');
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.NOTHING_TO_SAVE, 'success');
                            saveButtonClicked = false;
                        }
                        break;
                    case 2:
                        console.log('Save clicked in step 2 : ');
                        $scope.errorOcurred = false;
                        stepTwoChanged = !(angular.equals($scope.originalStepTwo, $scope.CompanyManagement.contactList));
                        console.log('Step 2 is changed: ' + stepTwoChanged);
                        if ($scope.SecondaryContact == 'Remove') {
                            $scope.validateSecondaryDetails = true;
                            valid = $scope.form.companydetailsform.$valid && $scope.form.companycontactpersondetailsform.$valid && $scope.form.companysecondarycontactpersondetailsform.$valid
                                && $scope.isValidPrimaryMobileEntered && $scope.isValidPrimaryWorkPhoneEntered && $scope.isValidSecondaryMobileEntered && $scope.isValidSecondaryWorkPhoneEntered;
                        } else {
                            valid = $scope.form.companydetailsform.$valid && $scope.form.companycontactpersondetailsform.$valid && $scope.isValidPrimaryMobileEntered && $scope.isValidPrimaryWorkPhoneEntered;
                        }
                        console.log('validations are proper: ' + valid);
                        console.log('1: ' + $scope.form.companydetailsform.$valid + ' 2: ' + $scope.form.companycontactpersondetailsform.$valid + ' 3: ' + $scope.isValidPrimaryMobileEntered + ' 4: ' + $scope.isValidPrimaryWorkPhoneEntered);
                        if (stepTwoChanged && valid) {
                            if ($scope.CompanyManagement.contactList.length == 1) {
                                $scope.CompanyManagement.contactList[0].primary = false;
                            }
                            if ($scope.SecondaryContact == 'Remove') {
                                if ($scope.CompanyManagement.contactList.length > 1) {
                                    $scope.CompanyManagement.contactList[0].primary = false;
                                    $scope.CompanyManagement.contactList[1].primary = false;
                                }
                            }

                            if (newlyCreatedCompanyId == null && $scope.clientOrBuUpdateInProgress) {
                                newlyCreatedCompanyId = $scope.CompanyManagement.company.id;
                            }
                            console.log('Save for companyid: ' + newlyCreatedCompanyId);

                            //Building the SavedCompanyManagement object before saving.
                            savedCompanyManagementCopy.contactList = angular.copy($scope.SavedCompanyManagement.contactList);
                            $scope.SavedCompanyManagement.contactList = angular.copy($scope.CompanyManagement.contactList);
                            limitCharactersInName();
                            console.log('Saving: ' + angular.toJson($scope.SavedCompanyManagement.contactList));
                            genericService.addObject(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.SAVE_COMPANY + '/' + newlyCreatedCompanyId, $scope.SavedCompanyManagement).then(function (data) {
                                console.log('Success: ' + angular.toJson(data.contactList));
                                saveButtonFlag++;
                                $scope.originalStepTwo = angular.copy(data.contactList);
                                saveButtonClicked = false;
                                //Updating the SavedCompanyManagement object after saving.
                                $scope.SavedCompanyManagement.contactList = angular.copy(data.contactList);

                                $scope.CompanyManagement.contactList = angular.copy(data.contactList);
                                console.log('Success: ' + angular.toJson($scope.originalStepTwo));
                                if ($scope.TypeOfCompany == 'Client' || $scope.TypeOfCompany == 'BusinessUnit') {
                                    //getAssessmentWorkFlow(loggedInUserDetails.company.companyId);
                                }
                                if ($scope.TypeOfCompany == 'BusinessUnit' && $scope.clientOrBuUpdateInProgress) {
                                    alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.BU.BU + " '<b><i class='wrap-word'>" + $scope.SavedCompanyManagement.company.name + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.UPDATESUCCESS, 'success');
                                }
                                if ($scope.CompanyManagement.company.companyType == 'Client' && $scope.clientOrBuUpdateInProgress) {
                                    alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.CLIENT + " '<b><i class='wrap-word'>" + delimitedCompanyOrClientName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.UPDATESUCCESS, 'success')
                                } else if (!$scope.clientOrBuCreationInProgress && $scope.CompanyDetailsUpdateMode) {
                                    alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.COMPANY + " '<b><i class='wrap-word'>" + delimitedCompanyOrClientName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.UPDATESUCCESS, 'success')
                                }
                                if (($scope.TypeOfCompany == 'Client' || $scope.TypeOfCompany == 'BusinessUnit') && $scope.clientOrBuCreationInProgress) {
                                    console.log('Assigning client to StaffingCompany');
                                    alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.CLIENT + " '<b><i class='wrap-word'>" + delimitedCompanyOrClientName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.CREATEDRAFTSUCCESS, 'success')
                                }
                                if (!($scope.CompanyDetailsUpdateMode) && $scope.TypeOfCompany == 'Company') {
                                    alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.COMPANY + " '<b><i class='wrap-word'>" + delimitedCompanyOrClientName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.COMPANYDRAFTSUCCESS, 'success')
                                }
                                companyStepTwoSaved = true;
                            }, function (error) {
                                if ($rootScope.isOnline) {
                                    alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                                }
                                console.log('now assigning original obj to transaction and saved objects');
                                $scope.SavedCompanyManagement.contactList = angular.copy(savedCompanyManagementCopy.contactList);
                            });
                        } else if (!valid) {
                            $scope.step2formsubmitted = true;
                            triggercount = 0;
                        } else if (valid && (!stepTwoChanged)) {
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.NOTHING_TO_SAVE, 'success');
                            saveButtonClicked = false;
                        }
                        break;
                    case 3:
                        console.log('Step 3 clicked on save : ');
                        // oldCheckedAdmin = null;
                        if (angular.isDefined($scope.originalStepThree) && $scope.originalStepThree != null && $scope.selectedFourDotFiveAdmin.length > 0) {
                            console.log('Equals of: $scope.originalStepThree.userId' + $scope.originalStepThree.userId + ' and: ' + $scope.selectedFourDotFiveAdmin[0].userId);
                            stepThreeChanged = !(angular.equals($scope.originalStepThree.userId, $scope.selectedFourDotFiveAdmin[0].userId));
                            oldfourdotfiveadminid = $scope.originalStepThree.userId;
                        } else if (!angular.isDefined($scope.originalStepThree) || ($scope.originalStepThree === null)) {
                            /**
                             * First condition is satisfied in create company.
                             * Second condition is satisfied when creating a company in
                             * Draft state( without selecting 4dot5admin) and coming to update for the next time later
                             */
                            stepThreeChanged = true;
                        }

                        console.log('Step 3 is changed: ' + stepThreeChanged);
                        if ($scope.SecondaryContact == 'Remove') {
                            console.log('Secondary contact present');
                            valid = $scope.form.companydetailsform.$valid && $scope.form.companycontactpersondetailsform.$valid && $scope.form.companysecondarycontactpersondetailsform.$valid && $scope.selectedFourDotFiveAdmin.length > 0;
                        } else {
                            valid = $scope.form.companydetailsform.$valid && $scope.form.companycontactpersondetailsform.$valid && $scope.selectedFourDotFiveAdmin.length > 0;
                            console.log('Secondary contact absent: ' + valid);
                        }
                        console.log('validations are proper: ' + valid);
                        if (stepThreeChanged && valid) {
                            if (!angular.isDefined(fourDotFiveCompanyAdmin)) {
                                fourDotFiveCompanyAdmin = $scope.selectedFourDotFiveAdmin;
                            }
                            console.log('Save for companyid: ' + newlyCreatedCompanyId);

                            //Building the SavedCompanyManagement object before saving.
                            savedfourDotFiveAdminCopy = angular.copy($scope.SavedfourDotFiveAdmin);
                            $scope.SavedfourDotFiveAdmin = angular.copy($scope.selectedFourDotFiveAdmin);
                            console.log('$$$$$$$$$$$$$$' + angular.toJson($scope.SavedfourDotFiveAdmin));
                            limitCharactersInName();
                            console.log('Saving :' + angular.toJson($scope.SavedfourDotFiveAdmin));
                            genericService.addObject(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.UPDATE_COMPANY_FOR_FOURDOTFIVE_ADMIN + '/' + $scope.SavedfourDotFiveAdmin[0].userId + '/' + $rootScope.userDetails.company.companyId + '?oldfourdotfiveadmin=' + oldfourdotfiveadminid, $scope.SavedfourDotFiveAdmin).then(function (data) {
                                //                        console.log('Success: ' + angular.toJson(data));
                                saveButtonFlag++;
                                $scope.originalStepThree = angular.copy(data.fourDotFiveAdmin);
                                //Updating the SavedCompanyManagement object after saving.
                                $scope.SavedfourDotFiveAdmin = angular.copy(data.fourDotFiveAdmin);
                                saveButtonClicked = false;

                                $scope.CompanyManagement.fourDotFiveAdmin = angular.copy(data.fourDotFiveAdmin);
                                $scope.CompanyManagementReset.fourDotFiveAdmin = angular.copy(data.fourDotFiveAdmin);
                                console.log(' $scope.CompanyManagementReset.fourDotFiveAdmin  ' + angular.toJson($scope.CompanyManagementReset.fourDotFiveAdmin));
                                //getAssessmentWorkFlow(loggedInUserDetails.company.companyId);
                                if ((StorageService.get('companyDetails') != null || $scope.CompanyDetailsUpdateMode)) {
                                    alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.COMPANY + " '<b><i class='wrap-word'>" + delimitedCompanyOrClientName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.UPDATESUCCESS, 'success')
                                } else if (!($scope.CompanyDetailsUpdateMode)) {
                                    alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.COMPANY + " '<b><i class='wrap-word'>" + delimitedCompanyOrClientName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.COMPANYDRAFTSUCCESS, 'success')
                                }
                                companyStepThreeSaved = true;
                            }, function (error) {
                                if ($rootScope.isOnline) {
                                    alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                                }
                                console.log('now assigning original obj to transaction and saved objects');
                                $scope.SavedfourDotFiveAdmin = angular.copy(savedfourDotFiveAdminCopy);
                            });
                        } else if (!valid) {
                            console.log('Step 3 mandatory fileds not filled.');
                            $scope.step3formsubmitted = true;
                            triggercount = 0;
                        } else if (valid && (!stepThreeChanged)) {
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.NOTHING_TO_SAVE, 'success');
                            saveButtonClicked = false;
                        }
                        break;
                    case 4:
                        console.log('Step 4 clicked on save :');
                        console.log('$scope.CompanyManagement.licensePreferences: ' + angular.toJson($scope.CompanyManagement.licensePreferences));
                        console.log('angular.equals($scope.CompanyManagement.licensePreferences, $scope.preferences: ' + angular.equals($scope.CompanyManagement.licensePreferences, $scope.preferences));
                        // stepFiveChanged = angular.equals($scope.originalStepFive, $scope.CompanyManagement.licensePreferences);
                        if ($scope.isPlanChosen && $scope.isActivePlanExists) {
                            stepFourChanged = !(angular.equals($scope.originalStepFour, $scope.selectedPlan));
                        console.log('Step 4 is changed: ' + stepFourChanged);
                        console.log('Save for companyid: ' + newlyCreatedCompanyId);
                        }
                        if ($scope.isPlanChosen && stepFourChanged) {
                             planService.savePlan($scope.selectedPlan, function (data) {
                                saveButtonFlag++;
                                data.planHeaderDto.planStartDateDisplay = $filter('fourdotfiveDateFormat')(data.planHeaderDto.planStartDate);
                                data.planHeaderDto.planEndDateDisplay = $filter('fourdotfiveDateFormat')(data.planHeaderDto.planEndDate);
                                data.planHeaderDto.unformattedStartDate = moment(data.planHeaderDto.planStartDate).toDate();
                                data.planHeaderDto.unformattedEndDate = moment(data.planHeaderDto.planEndDate).toDate();
                                $scope.originalStepFour = angular.copy(data);
                                $scope.savedCurrentActivePlan = angular.copy(data);
                                $scope.selectedPlan = angular.copy(data);
                                $scope.isActivePlanExists = true;
                                alertsAndNotificationsService.showBannerMessage('Company plan details saved successfully', 'success');
                                companyStepFourSaved = true;
                                saveButtonClicked = false;
                             }, function (error) {
                                if ($rootScope.isOnline) {
                                    alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                                }
                             });
                        } else if (!stepFourChanged) {
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.NOTHING_TO_SAVE, 'success');
                            saveButtonClicked = false;
                        } else if (!$scope.isPlanChosen) {
                            $scope.step4formsubmitted = true;
                            triggercount = 0;
                        }
                        break;
                    case 5:
                        console.log('Step 5 clicked on save :');
                        console.log('$scope.CompanyManagement.licensePreferences: ' + angular.toJson($scope.CompanyManagement.licensePreferences));
                        console.log('angular.equals($scope.CompanyManagement.licensePreferences, $scope.preferences: ' + angular.equals($scope.CompanyManagement.licensePreferences, $scope.preferences));
                        // stepFiveChanged = angular.equals($scope.originalStepFive, $scope.CompanyManagement.licensePreferences);
                        stepFiveChanged = !(angular.equals($scope.preferences, $scope.originalStepFive));
                        console.log('Step 5 is changed: ' + stepFiveChanged);
                        console.log('Save for companyid: ' + newlyCreatedCompanyId);
                        if ($scope.TypeOfCompany == 'Client' || StorageService.get('SelectedHostCompany') != null || $scope.TypeOfCompany == 'BusinessUnit') {
                            if ($scope.SecondaryContact == 'Remove') {
                                valid = $scope.form.companydetailsform.$valid && $scope.form.companycontactpersondetailsform.$valid && $scope.form.companysecondarycontactpersondetailsform.$valid;
                            } else {
                                valid = $scope.form.companydetailsform.$valid && $scope.form.companycontactpersondetailsform.$valid;
                            }
                        } else {
                            if ($scope.SecondaryContact == 'Remove') {
                                valid = $scope.form.companydetailsform.$valid && $scope.form.companycontactpersondetailsform.$valid && $scope.form.companysecondarycontactpersondetailsform.$valid && $scope.selectedFourDotFiveAdmin.length > 0;
                            } else {
                                valid = $scope.form.companydetailsform.$valid && $scope.form.companycontactpersondetailsform.$valid && $scope.selectedFourDotFiveAdmin.length > 0;
                            }
                        }
                        if (StorageService.get('SelectedHostCompany') != null) {
                            newlyCreatedCompanyId = $rootScope.userDetails.company.companyId;
                        }
                        //This condition will satisfy if editing a client/bu
                        if ($scope.clientOrBuUpdateInProgress) {
                            newlyCreatedCompanyId = $scope.CompanyManagement.company.id;
                            console.log('Update Client set licence preferences: ' + newlyCreatedCompanyId);
                        }
                        console.log('validations are proper: ' + valid);
                        if (valid && stepFiveChanged) {
                            //Building the SavedCompanyManagement object before saving.

                            savedAssessmentWorkFlowCopy = angular.copy($scope.SavedAssessmentWorkFlow);
                            $scope.SavedAssessmentWorkFlow = angular.copy($scope.preferences);
                            limitCharactersInName();
                            console.log('Saving :' + angular.toJson($scope.SavedAssessmentWorkFlow));
                            var clientOrCompany;
                            if ($scope.TypeOfCompany == 'Client') {
                                clientOrCompany = COMPANYMANAGEMENTCONSTANTS.CONTROLLER.SET_LICENCE_PREFERENCES_FOR_CLIENT_BU + '/' + $rootScope.userDetails.company.companyId + '/' + newlyCreatedCompanyId;
                            } else {
                                clientOrCompany = COMPANYMANAGEMENTCONSTANTS.CONTROLLER.SET_LICENCE_PREFERENCES + '/' + $rootScope.userDetails.company.companyType + '/' + newlyCreatedCompanyId;
                            }
                            console.log('Url is: ' + clientOrCompany);
                            genericService.addObject(clientOrCompany, $scope.SavedAssessmentWorkFlow).then(function (data) {
                                //                     console.log('Success: ' + angular.toJson(data));
                                saveButtonFlag++;
                                $scope.CompanyManagement.licensePreferences = angular.copy(data.licensePreferences);
                                $scope.originalStepFive = angular.copy(data.licensePreferences);
                                //Updating SavedCompanyManagement object after saving.
                                $scope.SavedAssessmentWorkFlow = angular.copy(data.licensePreferences);
                                if ($scope.clientOrBuUpdateInProgress && !(angular.equals($scope.originalStepFive, $scope.AssessmentWorkFlow))) {
                                    console.log('Working now no msg');
                                    alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.CLIENT + " '<b><i class='wrap-word'>" + delimitedCompanyOrClientName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.UPDATESUCCESS, 'success');
                                    // $state.go('missioncontrol.clientmanagement', null, { reload: false });
                                } else if ((StorageService.get('companyDetails') != null || StorageService.get('SelectedHostCompany') != null) && $scope.CompanyDetailsUpdateMode) {
                                    if ($rootScope.userDetails.roleScreenRef.role.name != 'CorporateAdmin' && $rootScope.userDetails.roleScreenRef.role.name != 'StaffingAdmin') {
                                        alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.COMPANY + " '<b><i class='wrap-word'>" + delimitedCompanyOrClientName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.UPDATESUCCESS, 'success');
                                    }
                                } else if ($scope.clientOrBuCreationInProgress) {
                                    alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.CLIENT + " '<b><i class='wrap-word'>" + delimitedCompanyOrClientName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.CLIENT.CREATEDRAFTSUCCESS, 'success');
                                } else if (!$scope.CompanyDetailsUpdateMode) {
                                    alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.COMPANY + " '<b><i class='wrap-word'>" + delimitedCompanyOrClientName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.COMPANYDRAFTSUCCESS, 'success');
                                }
                                companyStepFiveSaved = true;
                                saveButtonClicked = false;
                                //No update of company to active state from draft state on click of save.
                                /*if ($scope.CompanyManagement.company.companyType != 'Host') {
                                    genericService.addObject(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.CHANGE_COMPANYSTATE + '/' + newlyCreatedCompanyId, $scope.CompanyManagement).then(function (data) {
                                        console.log('Selected Company ClientOrBus Data received is : ' + angular.toJson(data));
                                    }, function (error) {
                                        console.log('Selected Company ClientOrBus Data error : ' + angular.toJson(error));
                                    });
                                }*/
                            }, function (error) {
                                if ($rootScope.isOnline) {
                                    alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                                }

                                if (error.statusCode == "409") {
                                    $scope.errorInSavingPrefsCheck = true;
                                    getAssessmentWorkFlow($rootScope.userDetails.company.companyId)
                                } else {
                                    $scope.SavedAssessmentWorkFlow = angular.copy(savedAssessmentWorkFlowCopy);
                                }
                            });
                        } else if (valid && !stepFiveChanged) {
                            console.log("clientOrBuUpdateInProgress");
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.NOTHING_TO_SAVE, 'success');
                            saveButtonClicked = false;
                            //$state.go('missioncontrol.clientmanagement', null, { reload: false });
                        } else if (!valid) {
                            $scope.step5formsubmitted = true;
                            triggercount = 0;
                        }
                        break;
                }
            };


            /**
             * Toggle company address to user checkbox.
             */
            $scope.toggleCopyCompanyAddress = function () {
                $scope.copyCompanyAddress = document.getElementById("copyCompanyAddress").checked;
                var companyAddressZipcode;
                console.log($rootScope.userDetails.company.companyId);
                if ($scope.copyCompanyAddress) {
                    genericService.getObjects(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.GET_COMPANY_DETAILS + '/' + $rootScope.userDetails.company.companyId).then(function (data) {
                        companyAddressZipcode = data.company.zipcode;
                        $scope.CompanyManagement.company.address1 = data.company.address1;
                        $scope.CompanyManagement.company.address2 = data.company.address2;
                        $scope.CompanyManagement.company.zipcode = companyAddressZipcode;
                        cancelClicked = false;
                        $scope.getAddressDetails(companyAddressZipcode, 'copycomanyaddress', function (response) {
                            angular.element('.companyaddresscopyenabled').attr({'readonly': true, 'disabled': true});
                        });
                    }, function (error) {
                        if ($rootScope.isOnline) {
                            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                        }
                    });
                } else {
                    $scope.copyCompanyAddress = false;
                    angular.element('.companyaddresscopyenabled').attr({'readonly': false, 'disabled': false});
                    if ($scope.accessMode == MESSAGECONSTANTS.ACCESS_MODES.EDIT) {
                        $scope.CompanyManagement.company.address1 = $scope.SavedCompanyManagement.company.address1;
                        $scope.CompanyManagement.company.address2 = $scope.SavedCompanyManagement.company.address2;
                        $scope.CompanyManagement.company.zipcode = $scope.SavedCompanyManagement.company.zipcode;
                        $scope.getAddressDetails($scope.CompanyManagement.company.zipcode, 'zipchanged', function (response) {
                            if (angular.isDefined($scope.SavedCompanyManagement.company.county) && ($scope.SavedCompanyManagement.company.county !== null)
                                && ($scope.CompanyManagement.company.zipcode === $scope.SavedCompanyManagement.company.zipcode)) {
                                $scope.CompanyManagement.company.county = $scope.SavedCompanyManagement.company.county;
                                $scope.safeApply();
                            }
                        });
                    } else {
                        $scope.CompanyManagement.company.address1 = $scope.SavedCompanyManagement.company.address1;
                        $scope.CompanyManagement.company.address2 = $scope.SavedCompanyManagement.company.address2;
                        $scope.CompanyManagement.company.zipcode = $scope.SavedCompanyManagement.company.zipcode;
                        $scope.getAddressDetails($scope.CompanyManagement.company.zipcode, 'zipchanged', function (response) {
                            if (angular.isDefined($scope.SavedCompanyManagement.company.county) && ($scope.SavedCompanyManagement.company.county !== null)
                                && ($scope.CompanyManagement.company.zipcode === $scope.SavedCompanyManagement.company.zipcode)) {
                                $scope.CompanyManagement.company.county = $scope.SavedCompanyManagement.company.county;
                                $scope.safeApply();
                            }
                        });
                    }
                }
            };

            function updatedLicensePrefsOnSave() {
                genericService.getObjects(COMPANYMANAGEMENTCONSTANTS.CONTROLLER.GET_ASSESSMENT_WORK_FLOW + '' + $rootScope.userDetails.company.companyType + '/' + $rootScope.userDetails.company.companyId).then(function (data) {
                    //$scope.originalStepFive = angular.copy(data);
                    $scope.SavedAssessmentWorkFlow = angular.copy(data);
                    $scope.preferences = angular.copy($scope.SavedAssessmentWorkFlow);
                    $scope.licensePreferencesFetched = true;
                    stepFiveChanged = !(angular.equals($scope.originalStepFive, $scope.preferences));
                }, function (error) {
                    if ($rootScope.isOnline) {
                        alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                    }
                });
            }


            $scope.manageStepDataAfterTimeout = function (Company, ContactList, FourDotFiveAdmin) {
                console.log('$timeout 1000');
                $timeout(function () {
                    $scope.manageStepData(Company, ContactList, FourDotFiveAdmin);
                }, 1000);
            };

            /** Function to control wizard flow with validations
             *
             */
            $scope.manageStepData = function (Company, ContactList, FourDotFiveAdmin) {
                console.log('clicked on next with triggercount : ' + triggercount);
                //         console.log('Company Management Details: ' + angular.toJson($scope.CompanyManagement) + 'Contact Person: ' + angular.toJson(ContactList) + 'FourDotFiveAdmin: ' + angular.toJson(FourDotFiveAdmin));
                //alert(triggercount);

                if (triggercount === 0) {
                    triggercount = angular.copy(triggercount + 1);
                    $scope.formsubmitted = true;
                    nextButtonClicked = true;
                    saveButtonClicked = false;
                    StorageService.remove('wizardoperations');
                    valid = false;
                    var currentStep = getCurrentStep();
                    //alert(currentStep);
                    console.log('In manageStepData function : saving details of step: ' + currentStep);
                    if (currentStep == 3 && ($scope.TypeOfCompany == 'Client' || StorageService.get('SelectedHostCompany') != null || $scope.TypeOfCompany == 'BusinessUnit')) {
                        currentStep = 5;
                    }

                    switch (currentStep) {
                        case 1:
                            stepOneChanged = !(angular.equals($scope.originalStepOne, $scope.CompanyManagement.company));//Checks if any changes.
                            console.log('Step 1 is changed: ' + stepOneChanged);
                            if ($scope.CompanyManagement.company.address1 !== null && $scope.CompanyManagement.company.address1 !== undefined) {
                                if ($scope.CompanyManagement.company.address2 != null) {
                                    isAdressValid = !angular.equals($scope.CompanyManagement.company.address1.toUpperCase(), $scope.CompanyManagement.company.address2.toUpperCase()) && ($scope.CompanyManagement.company.address1 != $scope.CompanyManagement.company.address2);
                                    console.log('if  ' + isAdressValid);
                                } else {
                                    console.log('else');
                                    isAdressValid = $scope.CompanyManagement.company.address1 != $scope.CompanyManagement.company.address2;
                                }
                          }
                            valid = $scope.form.companydetailsform.$valid && isAdressValid && $scope.validzipcode && angular.isDefined($scope.validzipcode) && (!$scope.zipcodeCalling || ($scope.zipcodeCalling && $scope.zipcodeCallCompleted));//Checks validations.
                            console.log('validations are proper  ' + valid + ' ' + $scope.validzipcode + ' ' + $scope.form.companydetailsform.$valid);
                            if (valid) {
                                console.log('Step 1 validations are proper, call saveCompanyDetails function');
                                if (!angular.isDefined($scope.CompanyManagement.company.companyType)) {
                                    if ($rootScope.userDetails.company.companyType == 'Corporation') {
                                        $scope.CompanyManagement.company.companyType = 'BusinessUnit';
                                    } else if ($rootScope.userDetails.company.companyType == 'StaffingCompany') {
                                        $scope.CompanyManagement.company.companyType = 'Client';
                                    }
                                }
                                nextButtonClicked = false;
                                saveCompanyDetails();
                            } else {
                                console.log('Just move no api call');
                                $scope.step1formsubmitted = true;
                                triggercount = 0;
                            }
                            if ($scope.SavedCompanyManagement.company.companyType === "Host") {
                                $scope.isValidPrimaryWorkPhoneEntered = true;
                                $scope.isValidPrimaryMobileEntered = true;
                                $scope.isValidSecondaryWorkPhoneEntered = true;
                                $scope.isValidSecondaryMobileEntered = true;
                            }
                            break;
                        case 2:
                            console.log('Next clicked from step 2 moving to step 3 : ');

                            console.log("$scope.originalStepTwo" + angular.toJson($scope.originalStepTwo));
                            console.log("$scope.CompanyManagement.contactList" + $scope.CompanyManagement.contactList);
                            $scope.errorOcurred = false;
                            stepTwoChanged = !(angular.equals($scope.originalStepTwo, $scope.CompanyManagement.contactList));
                            console.log('Step 2 is changed: ' + stepTwoChanged);
                            if ($scope.CompanyManagement.contactList.length == 1) {
                                $scope.CompanyManagement.contactList[0].primary = false;
                            }
                            if ($scope.SecondaryContact == 'Remove') {
                                if ($scope.CompanyManagement.contactList.length > 1) {
                                    $scope.CompanyManagement.contactList[0].primary = false;
                                    $scope.CompanyManagement.contactList[1].primary = false;
                                }
                                $scope.validateSecondaryDetails = true;
                                valid = $scope.form.companydetailsform.$valid && $scope.form.companycontactpersondetailsform.$valid && $scope.form.companysecondarycontactpersondetailsform.$valid;
                            } else {
                                console.log('$scope.form.companydetailsform.$valid : ' + $scope.form.companydetailsform.$valid);
                                console.log('$scope.form.companycontactpersondetailsform.$valid : ' + $scope.form.companycontactpersondetailsform.$valid);
                                valid = $scope.form.companydetailsform.$valid && $scope.form.companycontactpersondetailsform.$valid;
                            }
                            if (valid) {
                                console.log('Step 2 validations are proper, call saveContactDetails function');
                                nextButtonClicked = false;
                                saveContactDetails();
                            } else if (StorageService.get('companyDetails') != null && !($scope.CompanyDetailsUpdateMode) && !$scope.clientOrBuCreationInProgress && !$scope.clientOrBuUpdateInProgress) {
                                console.log('No api call');
                                StorageService.set('wizardoperations', true);
                                if ($scope.clientOrBuUpdateInProgress) {
                                    $scope.laststep = true;
                                }
                                nextButtonClicked = false;
                                triggercount = 0;
                            } else {
                                console.log('validations not proper');
                                $scope.step2formsubmitted = true;
                                triggercount = 0;
                            }
                            break;
                        case 3:
                            console.log('Next clicked from step 3 moving to step 4 : ');

                            oldCheckedAdmin = null;
                            console.log('$scope.originalStepThree: ' + angular.toJson($scope.originalStepThree));
                            console.log('angular.isDefined($scope.originalStepThree): ' + angular.isDefined($scope.originalStepThree));
                            console.log('$scope.selectedFourDotFiveAdmin.length: ' + $scope.selectedFourDotFiveAdmin.length);
                            if (angular.isDefined($scope.originalStepThree) && $scope.originalStepThree != null && $scope.selectedFourDotFiveAdmin.length > 0) {
                                console.log('Equals of: $scope.originalStepThree.userId' + $scope.originalStepThree.userId + ' and: ' + $scope.selectedFourDotFiveAdmin[0].userId);
                                stepThreeChanged = !(angular.equals($scope.originalStepThree.userId, $scope.selectedFourDotFiveAdmin[0].userId));
                                oldfourdotfiveadminid = $scope.originalStepThree.userId;
                            } else if (!angular.isDefined($scope.originalStepThree) || ($scope.originalStepThree === null)) {
                                /**
                                 * First condition is satisfied in create company.
                                 * Second condition is satisfied when creating a company in
                                 * Draft state( without selecting 4dot5admin) and coming to update for the next time later
                                 */
                                stepThreeChanged = true;
                            }
                            console.log('Four dot five admin selected: ' + $scope.selectedFourDotFiveAdmin.length == 1);
                            console.log('Step 3 is changed : ' + stepThreeChanged);
                            if ($scope.SecondaryContact == 'Remove') {
                                valid = $scope.form.companydetailsform.$valid && $scope.form.companycontactpersondetailsform.$valid && $scope.form.companysecondarycontactpersondetailsform.$valid && $scope.selectedFourDotFiveAdmin.length > 0;
                            } else {
                                valid = $scope.form.companydetailsform.$valid && $scope.form.companycontactpersondetailsform.$valid && $scope.selectedFourDotFiveAdmin.length > 0;
                            }

                            if (valid) {
                                console.log('Step 3 validations are proper, call saveFourDotFiveAdminDetails function');
                                nextButtonClicked = false;
                                saveFourDotFiveAdminDetails();
                            } else if (StorageService.get('companyDetails') != null && !($scope.CompanyDetailsUpdateMode)) {
                                console.log('No api call');
                                if (!$scope.licensePreferencesFetched) {
                                    getAssessmentWorkFlow(StorageService.get('companyDetails').companyId);
                                } else {
                                    StorageService.set('wizardoperations', true);
                                    $scope.stepTrigger('nextbtnclick');
                                }
                                $scope.laststep = true;
                                nextButtonClicked = false;
                                StorageService.set('wizardoperations', true);
                                triggercount = 0;
                            } else {
                                console.log('Step 3 mandatory fileds not filled.');
                                $scope.step3formsubmitted = true;
                                triggercount = 0;
                            }
                            break;
                        case 4:
                            $scope.stepFourNextClicked = true;
                            console.log('Next clicked from step 4 moving to dashboard : ');

                            console.log('$scope.selectedPlan : ' + angular.toJson($scope.selectedPlan));
                            // if ($scope.isPlanChosen && $scope.isActivePlanExists) {
                            //     stepFourChanged = !(angular.equals($scope.originalStepFour, $scope.selectedPlan));
                            // console.log('Step 4 is changed: ' + stepFourChanged);
                            // console.log('Save for companyid: ' + newlyCreatedCompanyId);
                            // }
                            // else if($scope.isPlanChosen && !$scope.isActivePlanExists) {
                            //     stepFourChanged = true;
                            //     console.log('Plan chosen for first time');
                            // }
                            console.log('Step 4 is changed : ' + stepFourChanged);
                             if ($scope.isPlanChosen && $scope.isActivePlanExists) {
                                console.log('Step 4 validations are proper, call save plan function');
                                nextButtonClicked = false;
                                savePlanDetails();

                            }
                            else if (StorageService.get('companyDetails') != null && !($scope.CompanyDetailsUpdateMode)) {
                                console.log('No api call');
                                $scope.laststep = true;
                                StorageService.remove('wizardoperations');
                                triggercount = 0;
                                nextButtonClicked = false;
                                $scope.laststep = true;
                            } else {
                                $scope.step4formsubmitted = true;
                                triggercount = 0;
                            }

                            break;
                        case 5:
                            console.log('Next clicked from step 5 moving to dashboard : ');

                            console.log('$scope.CompanyManagement.licensePreferences : ' + angular.toJson($scope.CompanyManagement.licensePreferences));
                            console.log('$scope.preferences : ' + angular.toJson($scope.preferences));
                            stepFiveChanged = !(angular.equals($scope.CompanyManagement.licensePreferences, $scope.preferences));
                            console.log('Step 5 is changed : ' + stepFiveChanged);
                            if ($scope.TypeOfCompany == 'Client' || $scope.TypeOfCompany == 'BusinessUnit' || $scope.CompanyManagement.company.companyType == 'Host') {
                                console.log('Host or client or bu');
                                if ($scope.SecondaryContact == 'Remove') {
                                    valid = $scope.form.companydetailsform.$valid && $scope.form.companycontactpersondetailsform.$valid && $scope.form.companysecondarycontactpersondetailsform.$valid;
                                } else {
                                    valid = $scope.form.companydetailsform.$valid && $scope.form.companycontactpersondetailsform.$valid;
                                }
                            } else {
                                console.log('Staffing or corporate');
                                if ($scope.SecondaryContact == 'Remove') {
                                    valid = $scope.form.companydetailsform.$valid && $scope.form.companycontactpersondetailsform.$valid && $scope.form.companysecondarycontactpersondetailsform.$valid && $scope.selectedFourDotFiveAdmin.length > 0;
                                } else {
                                    valid = $scope.form.companydetailsform.$valid && $scope.form.companycontactpersondetailsform.$valid && $scope.selectedFourDotFiveAdmin.length > 0;
                                }
                            }
                            if (valid) {
                                console.log('Step 5 validations are proper, call saveAssessementFlowDetails function');
                                nextButtonClicked = false;
                                saveAssessementFlowDetails();

                            } else if (StorageService.get('companyDetails') != null && !($scope.CompanyDetailsUpdateMode) && !$scope.clientOrBuCreationInProgress && !$scope.clientOrBuUpdateInProgress) {
                                console.log('No api call');
                                $scope.laststep = true;
                                StorageService.remove('wizardoperations');
                                triggercount = 0;
                                nextButtonClicked = false;
                                /*  $timeout(function () {
                                      $state.go('missioncontrol.dashboard', { companyObject: 'companyObject' }, { reload: false });
                                  }, 4000);*/
                            } else {
                                $scope.step5formsubmitted = true;
                                triggercount = 0;
                            }

                            break;
                        default:
                            console.log('Default');
                    }
                } else {
                    triggercount = 0;
                }
            };


            $scope.toggleAssessmentWorkFlowValues = function (index, workflow) {
                if (workflow.selected) {
                    workflow.selected = false;
                    if (workflow.name == 'Auto Match') {
                        $scope.preferences.matchCount = null;
                    }
                } else {
                    workflow.selected = true;
                    if (workflow.name == 'Auto Match') {
                        $scope.preferences.matchCount = 2;
                    }
                }
                // $scope.CompanyManagement.licensePreferences = $scope.preferences;
                stepFiveChanged = !(angular.equals($scope.originalStepFive, $scope.preferences));
            };

            /**
             * This function is called on click of 'Add Secondary Contact' button.
             * It allows the user to add secondary contact person for the company/client.
             */
            $scope.isSecondaryContactSelected = function () {
                if ($scope.SecondaryContact == 'Add') {
                    $scope.SecondaryContact = 'Remove';
                    console.log('Validate secondary details');
                } else {
                    $scope.SecondaryContact = 'Add';
                    if ($scope.CompanyManagement.contactList.length > 1) {
                        $scope.CompanyManagement.contactList.splice(1, 1);
                    }
                    $scope.validateSecondaryDetails = false;
                    console.log('Dont check secondary details');
                    $scope.form.companysecondarycontactpersondetailsform.$setPristine();
                }
            };

            /**
             * This method will be called when user clicks on 'Prev' button of wizard
             */
            $scope.prevbtnclicked = function () {
                $scope.form.companycontactpersondetailsform.$setPristine();
                console.log('prevbtnclicked: ');
                $scope.step2formsubmitted = false;
                saveButtonClicked = false;
                nextButtonClicked = false;
                console.log('Count: ' + count);
                console.log('comp details: ' + StorageService.get('companyDetails'));
                $scope.laststep = false;
                if (count === 0 && StorageService.get('companyDetails') == null && StorageService.get('SelectedHostCompany') == null && $scope.CompanyManagement.companyState != 'Draft') {
                    console.log('In if prevbtnclicked');
                    count = count + 1;
                    var action = $timeout(function () {
                        StorageService.set('wizardoperations', true);
                        angular.element('.btn-prev').triggerHandler('click');
                    }, 0).then(function () {
                        console.log('Then prevbtnclicked');
                        StorageService.remove('wizardoperations');
                        count = 0;
                        $timeout.cancel(action);
                    });
                } else {
                    console.log('Else prevbtnclicked: ' + angular.toJson($rootScope.userDetails.roleScreenRef.role));
                    if (StorageService.get('SelectedHostCompany') == null) {
                        $scope.AssessmentWorkFlow = [];
                    }
                    if ($scope.CompanyDetailsUpdateMode || $scope.ClientOrBuCreate || $scope.ClientOrBuUpdate || StorageService.get('SelectedHostCompany') != null || $scope.CompanyManagement.companyState == 'Draft') {
                        console.log('company update mode');
                        StorageService.set('wizardoperations', true);
                        triggercount = 0;
                    } else if (angular.equals($rootScope.userDetails.roleScreenRef.role.name, 'StaffingAdmin') || angular.equals($rootScope.userDetails.roleScreenRef.role.name, 'CorporateAdmin')) {
                        console.log('company view mode for Staffing Admin or Corporate Admin');
                        StorageService.set('wizardoperations', true);
                        triggercount = 0;
                    }
                }
            };

            $scope.$watchCollection(
                "form.companydetailsform",
                function (newValue, oldValue) {
                    if ($scope.form.companydetailsform.$dirty) {
                        $scope.cancelNotClicked = true;
                    }
                }
            );

            /**
             * Clear data on the form
             */
            $scope.cancel = function () {
                showConfirmationPopup = true;
                if (showConfirmationPopup && !$scope.isCompanyDeleted) {
                    bootbox.confirm({
                        closeButton: false,
                        title: "<div class='alert alert-warning' style='margin-bottom: 0px;'><i class='fa fa-warning fa-fw fa-lg'></i><strong>Warning!</strong></div>",
                        message: "Unsaved data on the page will be lost. Do you still want to continue?",
                        buttons: {
                            confirm: {
                                label: 'Yes',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'Cancel',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            console.log('This was logged in the callback: ' + result);
                            if (result) {
                                $scope.clearData();
                            } else {
                                showConfirmationPopup = false;
                                console.log("Stay in same page");
                            }

                        }
                    });
                }
             };

            $scope.cancelSteps = function () {
                $scope.cancel();
            };

            $scope.clearData = function() {

                $scope.cancelNotClicked = false;
                console.log('@@In cancel function from step: ' + getCurrentStep() + '$scope.geoLocations ' + angular.toJson($scope.geoLocations));
                cancelClicked = false;
                console.log('$scope.CompanyManagement.fourDotFiveAdmin  ' + angular.toJson($scope.CompanyManagement.fourDotFiveAdmin));
                $scope.copyOfGeoLocations = angular.copy($scope.geoLocations);
                console.log('@@@$scope.copyOfGeoLocations  ' + angular.toJson($scope.copyOfGeoLocations));
                $scope.copyOfGeoLocationsCopy = angular.copy($scope.geoLocationsCopy);
                var cancelCurrentStep = getCurrentStep();
                if (cancelCurrentStep == 3 && ($scope.TypeOfCompany == 'Client' || StorageService.get('SelectedHostCompany') != null || $scope.TypeOfCompany == 'BusinessUnit')) {
                    cancelCurrentStep = 4;
                }
                console.log('$scope.CompanyManagement.fourDotFiveAdmin  ' + angular.toJson($scope.CompanyManagement.fourDotFiveAdmin));
                // $scope.CompanyManagementReset.fourDotFiveAdmin = angular.copy($scope.CompanyManagement.fourDotFiveAdmin);
                showConfirmationPopup = false;

                switch (cancelCurrentStep) {
                    case 1:
                        console.log('Step one: ' + angular.toJson($scope.originalStepOne));
                        console.log('Step one $scope.CompanyManagement.company.zipcode: ' + $scope.CompanyManagement.company.zipcode);
                        if (angular.isDefined($scope.CompanyManagement.companyState)) {
                            console.log('inside company state defined');
                            cancelClicked = true;
                            if (!angular.equals($scope.CompanyManagement.company.zipcode, $scope.originalStepOne.zipcode)) {
                                console.log('both are equal');
                                $scope.getAddressDetails($scope.originalStepOne.zipcode, 'zipchanged');
                            } else {
                                console.log('both are not equal');
                                $scope.getAddressDetails($scope.originalStepOne.zipcode, 'updateaddress');
                            }
                            $scope.step1formsubmitted = false;
                            $scope.CompanyManagement.company = angular.copy($scope.originalStepOne);
                            $scope.SavedCompanyManagement.company = angular.copy($scope.originalStepOne);
                            $scope.form.companydetailsform.$setPristine();
                        } else {
                            console.log('inside company state un defined');
                            $scope.form.companydetailsform.$setPristine();
                            console.log("Step one create state click on cancel: " + $scope.CompanyManagement.company.zipcode);
                            $scope.geoLocations = [];
                            $scope.CompanyManagement.company = {};
                            console.log('Object: ' + angular.toJson($scope.CompanyManagement.company));
                            cancelClicked = true;
                            $scope.getAddressDetails($scope.CompanyManagement.company.zipcode, 'zipchanged');
                            $scope.step1formsubmitted = false;
                        }
                        break;
                    case 2:
                        console.log('Step two: ' + angular.toJson($scope.originalStepTwo));
                        if ($scope.originalStepTwo != undefined && $scope.originalStepTwo.length != 0) {
                            console.log('in if');
                            $scope.CompanyManagement.contactList = angular.copy($scope.originalStepTwo);
                            $scope.isValidPrimaryMobileEntered = true;
                            $scope.isValidPrimaryWorkPhoneEntered = true;
                            $scope.form.companycontactpersondetailsform.maskedWorkPhone.$setValidity('ng-intl-tel-input', true);
                            $scope.form.companycontactpersondetailsform.maskedWorkPhone1.$setValidity('ng-intl-tel-input', true);
                            if ($scope.originalStepTwo.length > 1) {
                                console.log('Secondary contact present.');
                                $scope.SecondaryContact = 'Remove';
                                $scope.isValidSecondaryMobileEntered = true;
                                $scope.isValidSecondaryWorkPhoneEntered = true;
                                $scope.form.companysecondarycontactpersondetailsform.maskedWorkPhoneSecondary.$setValidity('ng-intl-tel-input', true);
                                $scope.form.companysecondarycontactpersondetailsform.maskedMobilePhoneSecondary.$setValidity('ng-intl-tel-input', true);
                                $timeout(function () {
                                    $scope.form.companysecondarycontactpersondetailsform.$setPristine();
                                }, 1000);
                            } else {
                                console.log('in else');
                                $scope.invalidCompanySecondaryWorkNumber = false;
                                $scope.invalidCompanySecondaryMobilePhone = false;
                            }

                            $timeout(function () {
                                $scope.form.companycontactpersondetailsform.$setPristine();
                            }, 1000);
                            $scope.step2formsubmitted = false;
                        } else {
                            console.log("Step Two create state click on cancel: ");
                            $scope.CompanyManagement.contactList = [];
                            document.getElementById('maskedWorkPhone').value = '';
                            document.getElementById('maskedWorkPhone1').value = '';
                            if ($scope.SecondaryContact == 'Remove') {
                                $scope.invalidCompanySecondaryMobilePhone = false;
                                $scope.invalidCompanySecondaryWorkNumber = false;
                                document.getElementById('maskedWorkPhoneSecondary').value = '';
                                document.getElementById('maskedMobilePhoneSecondary').value = '';
                            }
                            $scope.step2formsubmitted = false;
                            $scope.form.companycontactpersondetailsform.$setPristine();
                        }
                        break;
                    case 3:
                        console.log('Step three: ' + angular.toJson($scope.originalStepThree) + ' Role: ' + $rootScope.userDetails.roleScreenRef.role.name);
                        if ($scope.originalStepThree != undefined) {
                            if (!($rootScope.userDetails.roleScreenRef.role.name == '4dot5Admin')) {
                                $scope.fourDotFiveAdminsList.forEach(function (element) {
                                    if (element.userId != $scope.originalStepThree.userId) {
                                        element.ticked = false;
                                    } else {
                                        element.ticked = true;
                                    }
                                }, this);
                                if (oldCheckedAdmin != null) {
                                    oldCheckedAdmin[0].ticked = false;
                                }
                                $scope.CompanyManagement = angular.copy($scope.CompanyManagementReset);
                                console.log('$scope.CompanyManagement.fourDotFiveAdmin  ' + angular.toJson($scope.CompanyManagement.fourDotFiveAdmin));
                            }
                        } else {
                            console.log('$scope.selectedFourDotFiveAdmin[0].userId ' + $scope.CompanyManagement.fourDotFiveAdmin);
                            $scope.CompanyManagement.fourDotFiveAdmin = {};
                            console.log("Step Three create state click on cancel");
                            $scope.fourDotFiveAdminsList.forEach(function (element) {
                                if (element.ticked) {
                                    element.ticked = false;
                                }
                            }, this);
                            $scope.step3formsubmitted = false;
                        }
                        $scope.form.company4dot5admindetailsform.$setPristine();
                        console.log('$scope.selectedFourDotFiveAdmin[0].userId ' + $scope.selectedFourDotFiveAdmin);
                        break;
                    case 4:
                        console.log("Step Four create state click on cancel");
                        if(!$scope.ClientOrBuUpdate && !$scope.ClientOrBuCreate) {
                            console.log('Its company create or update: ');
                            if ($rootScope.userDetails.company.companyType != 'Host') {
                                console.log('Original: ' + angular.toJson($scope.originalStepFour));
                                $scope.selectedPlan = angular.copy($scope.originalStepFour);
                                console.log('Preferences: ' + angular.toJson($scope.originalStepFour));
                                $scope.setPlanPriorities();
                            }
                        }
                        $scope.step4formsubmitted = false;
                        break;
                    case 5:
                        console.log('$scope.CompanyManagement.licensePreferences: ' + angular.toJson($scope.CompanyManagement.licensePreferences));
                        console.log('Step five: ' + angular.toJson($scope.originalStepFive));
                        console.log("Step five create state click on cancel");
                        if ($scope.ClientOrBuCreate) {
                            $scope.AssessmentWorkFlow = [];
                            console.log('Its client create');
                            $scope.preferences = angular.copy($scope.originalStepFive);
                        } else if ($scope.ClientOrBuUpdate) {
                            $scope.AssessmentWorkFlow = [];
                            console.log('Its client update');
                            $scope.preferences = angular.copy($scope.originalStepFive);
                        } else {
                            console.log('Its company create or update: ');
                            if ($rootScope.userDetails.company.companyType != 'Host') {
                                console.log('Original: ' + angular.toJson($scope.originalStepFive));
                                $scope.preferences = angular.copy($scope.originalStepFive);
                                console.log('Preferences: ' + angular.toJson($scope.originalStepFive));
                            } else {
                                $scope.preferences = angular.copy($scope.originalStepFive);
                            }
                        }
                        $scope.step5formsubmitted = false;
                        break;

                }
            
            }

            /**
             * Deselect industry subgroup on cancel
             */
            /*      INDUSTRY CODE        $scope.deselectIndustrySubGroup = function () {
                            //   var matching = false;
                            industryMasterList.forEach(function (element) {
                                if (element.selected == true) {
                                    element.industryDtos.forEach(function (elem) {
                                        for (var key in elem) {
                                            if (angular.isNumber(elem[key])) {
                                                delete elem[key];
                                            }
                                        }
                                        if (elem.selected == true) {
                                            elem.selected = false;
                                        }
                                    }, this);
                                }
                            }, this);
                        }*/

            /**
             * Remove from panel
             */

            /*  INDUSTRY CODE         $scope.removeFromSelectedList = function (index, subList) {
                           console.log('Clicked on: ' + angular.toJson(subList));
                           $scope.industriesInPanel.splice(index, 1);
                           $scope.IndustrySubList.forEach(function (element) {
                               console.log('Element name: ' + element.name + ' subList name: ' + subList.name);
                               if (subList.name == element.name) {
                                   element.selected = false;
                               }
                           }, this);
                       }*/

            /**
             * Delete extra characters
             */
            function deleteExtraCharacters(element) {
                for (var key in element) {
                    if (angular.isNumber(element[key])) {
                        console.log('Key is: ' + key);
                        delete element[key];
                    }
                }
            }

            /**
             * Industry chosen
             */
            /*   INDUSTRY CODE         $scope.industrySubGroupChosen = function (selectedValues) {
                            var newElementToPanel = true;
                            console.log('Selected here: ' + angular.toJson(selectedValues));
                            selectedValues.forEach(function (element) {
                                //deleteExtraCharacters(element);
                                if (element.selected == true) {
                                    for (var index = 0; index < $scope.industriesInPanel.length; index++) {
                                        if (element.name == $scope.industriesInPanel[index].name) {
                                            newElementToPanel = false;
                                            break;
                                        } else {
                                            newElementToPanel = true;
                                        }
                                    }
                                    if (newElementToPanel) {
                                        $scope.industriesInPanel.push(element);
                                    }
                                }
                            }, this);
                            console.log('Industries panel: ' + angular.toJson($scope.industriesInPanel));
                            //$scope.CompanyManagement.company.industryList = $scope.SelectedIndustrySubList;
                            //console.log('Last value: ' + angular.toJson($scope.CompanyManagement.company.industryList));
                        }*/

            var unbindStateChangeEvent = $scope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
                console.log('Preventing');
                event.preventDefault();
                confirmationAtStep = getCurrentStep();
                if (confirmationAtStep == 3 && ($scope.TypeOfCompany == 'Client' || StorageService.get('SelectedHostCompany') != null || $scope.TypeOfCompany == 'BusinessUnit')) {
                    confirmationAtStep = 4;
                }
                if (!(angular.equals($scope.SavedCompanyManagement.company, $scope.CompanyManagement.company)) && confirmationAtStep == 1) {
                    console.log('Unsaved changes in step 1');
                    showConfirmationPopup = true;
                } else if (confirmationAtStep == 2 && !(angular.equals($scope.SavedCompanyManagement.contactList, $scope.CompanyManagement.contactList))) {
                    console.log('Unsaved changes in step 2');
                    showConfirmationPopup = true;
                } else if (angular.isDefined($scope.originalStepThree) && confirmationAtStep == 3) {
                    console.log('Unsaved changes in step 3 :');
                    if (!(angular.isDefined($scope.selectedFourDotFiveAdmin[0]))) {
                        showConfirmationPopup = true;
                    } else if (!(angular.equals($scope.originalStepThree.userId, $scope.selectedFourDotFiveAdmin[0].userId))) {
                        showConfirmationPopup = true;
                    }
                } else if (angular.isDefined($scope.selectedFourDotFiveAdmin[0]) && confirmationAtStep == 3) {
                    showConfirmationPopup = true;
                } else if ($scope.SecondaryContact == 'Remove') {
                    if ($scope.form.companysecondarycontactpersondetailsform.$dirty && confirmationAtStep == 2 && !companyStepTwoSaved && !companyDeleted) {
                        console.log('Unsaved changes in step 2 secondary contact');
                        showConfirmationPopup = true;
                    }
                    // } else if (confirmationAtStep == 2 && !companyStepTwoSaved) {
                    //     console.log('Success 4.2');
                    //     showConfirmationPopup = true;
                    // }
                } else if (confirmationAtStep == 4 && !(angular.equals($scope.CompanyManagement.licensePreferences, $scope.preferences)) && !(angular.equals($scope.originalStepFive, $scope.preferences))) {
                    console.log('Unsaved changes in step 5 staffing company');
                    console.log('Staffing company update step 5 changed: ' + angular.equals($scope.originalStepFive, $scope.SavedAssessmentWorkFlow));
                    if (!(angular.equals($scope.originalStepFive, $scope.SavedAssessmentWorkFlow))) {
                        showConfirmationPopup = true;
                    }
                } else if (confirmationAtStep == 3 && !(angular.equals($scope.CompanyManagement.licensePreferences, $scope.preferences)) && $rootScope.userDetails.company.companyType != 'StaffingCompany' && ($rootScope.userDetails.company.companyType != 'Corporation') && StorageService.get('companyDetails') === null) {
                    console.log('Unsaved changes in step 3 Host company. ' + angular.equals($scope.originalStepFive, $scope.AssessmentWorkFlow));
                    showConfirmationPopup = true;
                } else if ($scope.errorOcurred && (!angular.equals($scope.originalStepOne, $scope.CompanyManagement.company)) && confirmationAtStep == 1) {
                    showConfirmationPopup = true;
                }

                if (showConfirmationPopup && !$scope.isCompanyDeleted) {
                    bootbox.confirm({
                        closeButton: false,
                        title: "<div class='alert alert-warning' style='margin-bottom: 0px;'><i class='fa fa-warning fa-fw fa-lg'></i><strong>Warning!</strong></div>",
                        message: "Unsaved data on the page will be lost. Do you still want to continue?",
                        buttons: {
                            confirm: {
                                label: 'Yes',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'Cancel',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            console.log('This was logged in the callback: ' + result);
                            if (result) {
                                if ($rootScope.switchClicked) {
                                    $scope.clearData();
                                    //$('#myModal').modal('show');
                                    $rootScope.switchClicked = false;
                                    $scope.errorOcurred = false;
                                    $scope.getCompaniesForLoggedinUser(function (response) {
                                        $('#myModal').modal('show');
                                    });
                                } else {
                                    $scope.errorOcurred = false;
                                    unbindStateChangeEvent();
                                    if (toState.name === 'login') {
                                        $rootScope.$emit("CallLogoutMethod", {logoutnow: true});
                                    } else {
                                        $state.go(toState.name, toParams, {reload: true});
                                    }
                                }
                            } else {
                                showConfirmationPopup = false;
                                $rootScope.switchClicked = false;
                                console.log("Stay in same page");
                            }

                        }
                    });
                } else {
                    //console.log("No changes in this page yet so moving");
                    if (StorageService.get('clientOrBuToBeUpdated') != null) {
                        StorageService.remove('clientOrBuToBeUpdated');
                    }
                    if ($rootScope.switchClicked) {
                        //$('#myModal').modal('show');
                        $rootScope.switchClicked = false;
                        showConfirmationPopup = false;
                        $scope.getCompaniesForLoggedinUser(function (response) {
                            $('#myModal').modal('show');
                        });
                    } else {
                        unbindStateChangeEvent();
                        $rootScope.switchClicked = false;
                        if (toState.name === 'login') {
                            $rootScope.$emit("CallLogoutMethod", {logoutnow: true});
                        } else {
                            $state.go(toState.name, toParams, {reload: true});
                        }
                    }
                }
            });


            $scope.deleteJobBoardDetails = function (index) {
                $scope.preferences.jobBoardCredentialList.splice(index, 1);
            };


            /**
             * Step changes
             */
            $scope.getStepChanges = function (stepclicked) {
                saveButtonClicked = false;
                nextButtonClicked = false;
                console.log('Step clicked: ' + stepclicked);
                if (stepclicked != 3 && ($scope.CompanyManagement.company.companyType == 'Client' || $scope.CompanyManagement.company.companyType == 'BusinessUnit')) {
                    $scope.laststep = false;
                } else if (stepclicked != 4 && ($scope.CompanyManagement.company.companyType == 'StaffingCompany' || $scope.CompanyManagement.company.companyType == 'Corporation')) {
                    $scope.laststep = false;
                }
            };

            $scope.changeAddressObject = function (type, obj) {
                console.log('............................................' + type);
                saveButtonClicked = false;
                nextButtonClicked = false;
                switch (type) {
                    case 'city':
                        console.log('type : ' + type + ' obj : ' + obj);
                        for (var p = 0; p < $scope.geoLocationscopy.length; p++) {
                            if ($scope.geoLocationscopy[p].city === obj) {
                                $scope.CompanyManagement.company.addressState = $scope.geoLocationscopy[p].state;
                                $scope.CompanyManagement.company.country = $scope.geoLocationscopy[p].country;
                                $scope.CompanyManagement.company.county = $scope.geoLocationscopy[p].county;
                                if (angular.isDefined($scope.geoLocationscopy[p].county) && ($scope.geoLocationscopy[p].county !== null)) {
                                    $scope.isCountyExists = true;
                                } else {
                                    $scope.isCountyExists = false;
                                }
                            }
                        }
                        break;
                    case 'state':
                        console.log('type : ' + type + ' obj : ' + obj);
                        for (var p = 0; p < $scope.geoLocationscopy.length; p++) {
                            if ($scope.geoLocationscopy[p].state === obj) {
                                $scope.CompanyManagement.company.city = $scope.geoLocationscopy[p].city;
                                $scope.CompanyManagement.company.country = $scope.geoLocationscopy[p].country;
                                $scope.CompanyManagement.company.county = $scope.geoLocationscopy[p].county;
                                if (angular.isDefined($scope.geoLocationscopy[p].county) && ($scope.geoLocationscopy[p].county !== null)) {
                                    $scope.isCountyExists = true;
                                } else {
                                    $scope.isCountyExists = false;
                                }
                            }
                        }
                        break;
                    case 'country':
                        console.log('type : ' + type + ' obj : ' + obj);
                        for (var p = 0; p < $scope.geoLocationscopy.length; p++) {
                            if ($scope.geoLocationscopy[p].country === obj) {
                                $scope.CompanyManagement.company.city = $scope.geoLocationscopy[p].city;
                                $scope.CompanyManagement.company.addressState = $scope.geoLocationscopy[p].state;
                                $scope.CompanyManagement.company.county = $scope.geoLocationscopy[p].county;
                                if (angular.isDefined($scope.geoLocationscopy[p].county) && ($scope.geoLocationscopy[p].county !== null)) {
                                    $scope.isCountyExists = true;
                                } else {
                                    $scope.isCountyExists = false;
                                }
                            }
                        }
                        break;
                    case 'county':
                        for (var p = 0; p < $scope.geoLocationscopy.length; p++) {
                            if ($scope.geoLocationscopy[p].county === obj) {
                                $scope.CompanyManagement.company.city = $scope.geoLocationscopy[p].city;
                                $scope.CompanyManagement.company.addressState = $scope.geoLocationscopy[p].state;
                                $scope.CompanyManagement.company.country = $scope.geoLocationscopy[p].country;
                            }
                        }
                        break;
                }
            };

            $("#maskedWorkPhone").on("countrychange", function (e, countryData) {
                //console.log('e : ' + angular.toJson(e));
                console.log('countryData p workTelephone change: ' + angular.toJson(countryData));
                $scope.primaryContactWTCountryCode = countryData.iso2;
                $timeout(function () {
                    $scope.setPrimaryContactWTCountryCode();
                }, 200);
            });

            $("#maskedWorkPhone1").on("countrychange", function (e, countryData) {
                //console.log('e : ' + angular.toJson(e));
                console.log('countryData p workTelephone change: ' + angular.toJson(countryData));
                $scope.primaryContactWTCountryCode1 = countryData.iso2;
                $scope.setPrimaryContactWTCountryCode1();
            });

            $("#maskedMobilePhone").on("countrychange", function (e, countryData) {
                //console.log('e : ' + angular.toJson(e));
                console.log('countryData p mobileNumber change: ' + angular.toJson(countryData));
                $scope.primaryContactMNCountryCode = countryData.iso2;
                $timeout(function () {
                    $scope.setPrimaryContactMNCountryCode();
                }, 200);
            });

            angular.element("#maskedWorkPhoneSecondary").on("countrychange", function (e, countryData) {
                //console.log('e : ' + angular.toJson(e));
                console.log('countryData s workTelephone change: ' + angular.toJson(countryData));
                $scope.secondaryContactWTCountryCode = countryData.iso2;
                $timeout(function () {
                    $scope.setSecondaryContactWTCountryCode();
                }, 200);
            });

            angular.element("#maskedMobilePhoneSecondary").on("countrychange", function (e, countryData) {
                //console.log('e : ' + angular.toJson(e));
                console.log('countryData s mobileNumber change: ' + angular.toJson(countryData));
                $scope.secondaryContactMNCountryCode = countryData.iso2;
                $timeout(function () {
                    $scope.setSecondaryContactMNCountryCode();
                }, 200);
            });


            $timeout(function () {
                console.log('now entered');
                if (angular.isDefined($scope.form.companycontactpersondetailsform)) {
                    $scope.form.companycontactpersondetailsform.$setPristine();
                }
            }, 5000);

            //Get these information for UI Elements
            $scope.getCompanyTypes();


            $scope.addAssessmentKey = function () {
                var alreadyPresent = false;
                var username = document.getElementById("username").value;
                var key = document.getElementById("idval").value;
                var indexval = 0;
                var use4dot5Account;
                var use4dot5Credential;
                if ($scope.TypeOfCompany == 'Host') {
                    use4dot5Account = true;
                    use4dot5Credential = false;
                } else {
                    if ($scope.useAccountDetails == '4dot5')
                        use4dot5Account = true;
                    else
                        use4dot5Account = false;

                    if ($scope.useAccountCredDetails == '4dot5')
                        use4dot5Credential = true;
                    else
                        use4dot5Credential = false;

                }

                if (checkIfAdd(username, key, use4dot5Credential)) {
                    if ($scope.preferences.licenseAssessmentPreferences.assessmentCredentials == null) {
                        $scope.preferences.licenseAssessmentPreferences.assessmentCredentials = [];
                    }
                    for (var index in $scope.preferences.licenseAssessmentPreferences.assessmentCredentials) {
                        var assessmentCredential = $scope.preferences.licenseAssessmentPreferences.assessmentCredentials[index];
                        if (username == assessmentCredential.userName && $scope.assessmentType == assessmentCredential.assessmentType) {
                            alreadyPresent = true;
                            alertsAndNotificationsService.showBannerMessage("Username already exists", 'success');
                        }

                    }
                    if (!alreadyPresent) {
                        if ($scope.buttonAssessment == "Modify") {
                            // The key at index 2
                            var assessmentCredentials = $scope.preferences.licenseAssessmentPreferences.assessmentCredentials[$scope.selectedAssesmentIndex];
                            assessmentCredentials.userName = username;
                            assessmentCredentials.key = key;
                            assessmentCredentials.assessmentType = $scope.assessmentType;
                            assessmentCredentials.use4Dot5Account = use4dot5Account;
                            assessmentCredentials.use4Dot5Credential = use4dot5Credential;
                            $scope.preferences.licenseAssessmentPreferences.assessmentCredentials[$scope.selectedAssesmentIndex] = assessmentCredentials;
                            $scope.buttonAssessment = "Add"

                        } else {
                            $scope.AssessmentCredential = {};
                            $scope.AssessmentCredential.assessmentType = $scope.assessmentType;
                            $scope.AssessmentCredential.userName = username;
                            $scope.AssessmentCredential.key = key;
                            $scope.AssessmentCredential.use4Dot5Account = use4dot5Account;
                            $scope.AssessmentCredential.use4Dot5Credential = use4dot5Credential;
                            $scope.preferences.licenseAssessmentPreferences.assessmentCredentials.push($scope.AssessmentCredential);


                        }
                    }
                }

                document.getElementById("username").value = "";
                document.getElementById("idval").value = "";


            };

            function isBlank(str) {
                return (!str || /^\s*$/.test(str));
            }

            function checkIfAdd(username, key, use4dot5Credential) {

                if (!isBlank(username) && !isBlank(key) && !use4dot5Credential) {
                    return true;
                } else if (isBlank(username) && isBlank(key) && use4dot5Credential) {
                    return true;
                } else {
                    return false;
                }

            }


            $scope.changecredential = function () {
                var use4dot5Account = $scope.useAccountDetails;
                if (use4dot5Account == 'My Own') {
                    $scope.useAccountCredDetails = 'My Own';
                    document.getElementById("useAccountCredDetails").disabled = true;

                } else {

                    $scope.useAccountCredDetails = '';
                    document.getElementById("useAccountCredDetails").disabled = false;

                }
            };

            $scope.displayUse4dot5account = function (use4dot5account) {


            };

            $scope.addJobBoardKey = function () {
                var alreadyPresent = false;
                var username = document.getElementById("usernamejob").value;
                var key = document.getElementById("keyjob").value;
                if (username != null && key != null) {
                    if ($scope.preferences.jobBoardCredentialList == null) {
                        $scope.preferences.jobBoardCredentialList = [];
                    }

                    for (var index in $scope.preferences.jobBoardCredentialList) {
                        var jobBoard = $scope.preferences.jobBoardCredentialList[index];
                        if (username == jobBoard.userName && jobBoard.sourceType == $scope.source.sourceType) {
                            alreadyPresent = true;
                            alertsAndNotificationsService.showBannerMessage("Username already exists", 'success');
                        }
                    }

                    if (!alreadyPresent) {
                        if ($scope.buttonWorkFlowText == "Modify") {
                            var jobBoard = $scope.preferences.jobBoardCredentialList[$scope.selectedjobWorkFlowQuestion];
                            jobBoard.userName = username;
                            jobBoard.key = key;
                            jobBoard.sourceType = $scope.source.sourceType;
                            jobBoard.jobBoardId = $scope.source.id;
                            $scope.buttonWorkFlowText = "Add";
                            $scope.preferences.jobBoardCredentialList[$scope.selectedjobWorkFlowQuestion] = jobBoard;
                        } else {
                            $scope.JobBoardCredential = {};
                            $scope.JobBoardCredential.sourceType = $scope.source.sourceType;
                            $scope.JobBoardCredential.jobBoardId = $scope.source.id;
                            $scope.JobBoardCredential.userName = username;
                            $scope.JobBoardCredential.key = key;
                            $scope.preferences.jobBoardCredentialList.push($scope.JobBoardCredential);
                        }

                    }
                }
                document.getElementById("usernamejob").value = "";
                document.getElementById("keyjob").value = "";
            };

            $scope.toggleAssessmentWorkFlowValues1 = function (index, workflow) {
                if (workflow.selected) {
                    workflow.selected = false;
                } else {
                    workflow.selected = true;
                }
            };

            $scope.imageProctoringValueChanged = function () {
                if (!$scope.preferences.licenseAssessmentPreferences.imageProctoring) {
                    $scope.preferences.licenseAssessmentPreferences.addProctoringImagesToReport = false;
                }
            };

            $scope.checkifSelected = function (optionval) {
                if ($scope.preferences != null) {
                    if ($scope.preferences.licenseAssessmentPreferences.accessTime == null) {
                        return false;
                    }

                    if ($scope.preferences.licenseAssessmentPreferences.accessTime.indexOf(optionval.trim()) == -1) {
                        return false
                    } else {

                        return true;
                    }
                }
            };

            $scope.checkifDisabled = function (optionval) {
                if ($scope.preferences != null) {
                    if ($scope.preferences.licenseAssessmentPreferences.accessTimeDisabledValues == null) {
                        return false;
                    }

                    if ($scope.preferences.licenseAssessmentPreferences.accessTimeDisabledValues.indexOf(optionval.trim()) == -1) {
                        return false
                    } else {
                        return true;
                    }
                }
            };

            $scope.deleteAssesmentUsername = function (index) {
                $scope.preferences.licenseAssessmentPreferences.assessmentCredentials.splice(index, 1);
            };

            $scope.modifyJobBoard = function (jobBoardCred, idVal) {
                document.getElementById("usernamejob").value = jobBoardCred.userName;
                document.getElementById("keyjob").value = jobBoardCred.key;
                $scope.selectedjobWorkFlowQuestion = idVal;
                for (var source in $scope.jobSourceType) {
                    if ($scope.jobSourceType[source].sourceType == jobBoardCred.sourceType) {
                        $scope.source = $scope.jobSourceType[source];
                    }
                }

                $scope.buttonWorkFlowText = "Modify";
            };

            $scope.modifyAssessmentPreferences = function (assementCredential, idval) {


                document.getElementById("username").value = assementCredential.userName;
                document.getElementById("idval").value = assementCredential.key;
                $scope.buttonAssessment = "Modify";
                for (var assessmentType in $scope.assessmentTypeList) {
                    if ($scope.assessmentTypeList[assessmentType] == assementCredential.assessmentType) {
                        $scope.assessmentType = $scope.assessmentTypeList[assessmentType];
                    }
                }

                if (assementCredential.use4Dot5Account == true) {
                    $scope.useAccountDetails = "4dot5";
                } else {

                    $scope.useAccountDetails = "My Own";
                }
                if (assementCredential.use4Dot5Credential == true) {
                    $scope.useAccountCredDetails = "4dot5";
                } else {
                    $scope.useAccountCredDetails = "My Own";
                }
                $scope.selectedAssesmentIndex = idval;
            }

            $scope.choosePlan = function(planIndex) {
                var planChangeStatus = "";
                $scope.isPlanChosen = true;
                $scope.selectedPlanName= $scope.plans[planIndex].plan;
                $scope.selectedPlanTypeList = angular.copy($scope.plans[planIndex].planTypeList);
                if (!$scope.isActivePlanExists) {
                    $scope.selectedPlan = $scope.getParsedSelectedPlan($scope.plans[planIndex], planChangeStatus);
                } else if ($scope.savedCurrentActivePlan.planHeaderDto.planFeaturesCapabilityId !== $scope.plans[planIndex].id) {
                    if ($scope.plans[planIndex].priority > $scope.savedCurrentActivePlanPriority) {
                        planChangeStatus = "upgrade";
                    }
                    else {
                        planChangeStatus = "downgrade";
                    }
                    $scope.selectedPlan = $scope.getParsedSelectedPlan($scope.plans[planIndex], planChangeStatus);
                    $scope.selectedPlan.planHeaderDto.id = $scope.savedCurrentActivePlan.planHeaderDto.id;
                    $scope.selectedPlan.planDetailDto.id = $scope.savedCurrentActivePlan.planDetailDto.id;
                    $scope.selectedPlan.planDetailDto.planHeaderId = $scope.savedCurrentActivePlan.planDetailDto.planHeaderId;
                     
                } else {
                    $scope.selectedPlan = angular.copy($scope.savedCurrentActivePlan);
                }
                if ($scope.plans[planIndex].enabled == true) {
                    $scope.modifyPlan(true);
                } else {
                    $scope.modifyPlan(false);
                }
            }

            $scope.getParsedSelectedPlan = function(selectedPlan, planChangeStatus){
                var taAllocated = selectedPlan.features.filter(function(feature) {
                    return feature.name == "Tech Assessments"
                })[0].limitValue;

                var jbiAllocated = selectedPlan.features.filter(function(feature) {
                    return feature.name == "Job Board Integration"
                })[0].limitValue;

                var planStartDate = new Date();
                var planEndDate = new Date();
                var taUsed = 0;
                var jbiUsed = 0;

                if (planChangeStatus == "upgrade") {
                    planStartDate = moment(planStartDate).add(1, 'd').toDate();
                    taUsed = $scope.savedCurrentActivePlan.planDetailDto.taUsed;
                    jbiUsed = $scope.savedCurrentActivePlan.planDetailDto.jbiUsed;
                }
                else if (planChangeStatus == "downgrade") {
                    planStartDate = moment($scope.savedCurrentActivePlan.planHeaderDto.unformattedEndDate).add(1, 'd').toDate();
                    taUsed = $scope.savedCurrentActivePlan.planDetailDto.taUsed;
                    jbiUsed = $scope.savedCurrentActivePlan.planDetailDto.jbiUsed;
                }
                var planType = "";
                if ($scope.selectedPlanName.toLowerCase() == "trial") {
                    planType = "Month to Month"
                    planStartDate = $filter('fourdotfiveDateFormat')(planStartDate);
                    planEndDate = $filter('fourdotfiveDateFormat') (moment(planStartDate).add(1, 'M').toDate());
                } else {
                    planType = "6 Months"
                    planStartDate = $filter('fourdotfiveDateFormat')(planStartDate);
                    planEndDate = $filter('fourdotfiveDateFormat') (moment(planStartDate).add(6, 'M').toDate()); 
                }
                return {
                    "planHeaderDto": {
                        "id": "",
                        "planFeaturesCapabilityId": selectedPlan.id,
                        "companyId": $rootScope.userDetails.company.companyId,
                        "planType": planType,
                        "planStartDate": dateTimeUtilityService.getDateTimeInAPIFormat(planStartDate),
                        "planEndDate": dateTimeUtilityService.getDateTimeInAPIFormat(new Date(planEndDate)),
                        "planStartDateDisplay": planStartDate,
                        "planEndDateDisplay": planEndDate,
                        "unformattedStartDate": moment(planStartDate).toDate(),
                        "unformattedEndDate": moment(planEndDate).toDate()
                    },
                    "planDetailDto": {
                        "id": "",
                        "planHeaderId": "",
                        "taAllocated": taAllocated,
                        "taUsed": taUsed,
                        "taAdded": 0,
                        "jbiAllocated": jbiAllocated,
                        "jbiUsed": 0,
                        "jbiAdded": jbiUsed
                    }
                }
            }

            $scope.modifyPlan = function(isModify) {
                $scope.modifyPlanModal = $uibModal.open({
                    animation: false,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/partials/missioncontrol/companymanagementmodule/modify-plan-modal.html',
                    controller: ['$uibModalInstance','selectedPlanName','planTypeList','selectedPlan','isModifyPlan',function ($uibModalInstance, selectedPlanName, planTypeList, selectedPlan, isModifyPlan) {
                        var vm = this;  
                        vm.selectedPlanName = selectedPlanName;
                        vm.planTypeList = planTypeList;    
                        vm.selectedPlan = selectedPlan;
                        vm.isModifyPlan = isModifyPlan
                        vm.closeModal = function() {
                            console.log('unmodified plan');
                            $scope.selectedPlan = angular.copy($scope.savedCurrentActivePlan);
                            console.log($scope.selectedPlan)
                            $uibModalInstance.close('cancel');
                        };
                        vm.savePlan = function () {
                            $uibModalInstance.close('cancel');
                            $scope.selectedPlan = angular.copy(vm.selectedPlan);
                            $scope.selectedPlan.planHeaderDto.planStartDateDisplay = $filter('fourdotfiveDateFormat')($scope.selectedPlan.planHeaderDto.planStartDate);
                            $scope.selectedPlan.planHeaderDto.planEndDateDisplay = $filter('fourdotfiveDateFormat')($scope.selectedPlan.planHeaderDto.planEndDate);
                            $scope.selectedPlan.planHeaderDto.unformattedStartDate = moment($scope.selectedPlan.planHeaderDto.planStartDate).toDate();
                            $scope.selectedPlan.planHeaderDto.unformattedEndDate = moment($scope.selectedPlan.planHeaderDto.planEndDate).toDate();
                            $scope.originalStepFour = angular.copy($scope.selectedPlan);
                            $scope.savedCurrentActivePlan = angular.copy($scope.selectedPlan);
                            $scope.planDetailsFetched = true;
                            $scope.setPlanPriorities();
                            $scope.isActivePlanExists = true;
                            $scope.isPlanChosen = true;
                            console.log('modified plan');
                            console.log($scope.selectedPlan);
                        }
                    }],
                    controllerAs: 'modifyPlanModal',
                    size: 'lg',
                    resolve: {
                        selectedPlanName: function() {
                            return $scope.selectedPlanName;
                        },
                        planTypeList: function(){
                            return $scope.selectedPlanTypeList;
                        },
                        selectedPlan: function () {
                            return $scope.selectedPlan;
                        },
                        isModifyPlan: function() {
                            return isModify;
                        }
                    }
                });
            } 
        }
    ]);