/**
 * MODULE FOR DASHBOARD OF 4DOT5 APPLICATION
 */

var dashboardmodule = angular.module('4dot5.missioncontrolmodule.dashboardmodule',
	[]);

dashboardmodule.constant('DASBBOARDCONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.dashboard',
        URL: '/dashboard',
        CONTROLLER: 'DashboardController',
		CONTROLLERAS: 'dashboard',
        TEMPLATEURL: 'app/partials/missioncontrol/dashboard/dashboard.html',
    }
});

dashboardmodule.config(
    ['$stateProvider',
        'DASBBOARDCONSTANTS',
        function ($stateProvider, DASBBOARDCONSTANTS) {
            $stateProvider.state(DASBBOARDCONSTANTS.CONFIG.STATE, {
                url: DASBBOARDCONSTANTS.CONFIG.URL,
                templateUrl: DASBBOARDCONSTANTS.CONFIG.TEMPLATEURL,
                controller: DASBBOARDCONSTANTS.CONFIG.CONTROLLER,
				controllerAs: DASBBOARDCONSTANTS.CONFIG.CONTROLLERAS,
                data: {
                    requireLogin: true
                },
				resolve: {

				}
            });
        }
    ]);

dashboardmodule.controller('DashboardController',
	['$rootScope', '$state', 'candidateService', 'jobService','alertsAndNotificationsService',
		function ($rootScope, $state, candidateService, jobService, alertsAndNotificationsService) {

			var vm = this;

			vm.init = init;
			vm.requisitionCount = '';
            vm.candidateCounts = {};
            vm.visibilityFilters = {
                unassigned: '',
                assignedToMe: '',
                assignedToOthers: ''
            };
            vm.countsVisibilityFilter = '';
            // methods
			vm.getCandidateCounts = getCandidateCounts;
			vm.getRequisitionsCount = getRequisitionsCount;
			vm.visibilityFilterChanged = visibilityFilterChanged;
			vm.redirectToCandidates = redirectToCandidates;
			vm.redirectToRequisitions = redirectToRequisitions;
			//flags
			vm.candidateCountsLoadingFlag = false;
			vm.requisitionCountsLoadingFlag = false;

            vm.init();

			function init(){
                vm.userId = $rootScope.userDetails.id;
                vm.companyId = $rootScope.userDetails.company.companyId;
                vm.candidateCountsLoadingFlag = true;
                vm.requisitionCountsLoadingFlag = true;
                // visibility filters - all are checked by default
                vm.visibilityFilters.unassigned = true;
                vm.visibilityFilters.assignedToOthers = true;
                vm.visibilityFilters.assignedToMe = true;
                vm.countsVisibilityFilter = 'unassigned,assignedToMe,assignedToOthers';
                vm.getCandidateCounts(function () {
					vm.candidateCountsLoadingFlag = false;
                });
                vm.getRequisitionsCount(function () {
					vm.requisitionCountsLoadingFlag = false;
                });
			}

			function getCandidateCounts(successCallback){
                candidateService.getCandidateCounts(vm.userId, vm.companyId, vm.countsVisibilityFilter, function(data){
                	vm.candidateCounts = data;
                	if(successCallback){
                        successCallback();
					}
				}, function(error){
                    if ($rootScope.isOnline) {
                        alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                    }
				});
			}

			function getRequisitionsCount(successCallback){
				jobService.getJobCountByStatus(vm.userId, vm.companyId, function(data){
                    vm.requisitionCount = data;
                    if(successCallback){
                        successCallback();
                    }
                }, function(error){
                    if ($rootScope.isOnline) {
                        alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                    }
                });
			}

			function _appendToCountsFilter(filterString, appendString){
				if(_.isEmpty(filterString)){
					return appendString;
				}else{
					return filterString + ',' + appendString;
				}
			}

            function visibilityFilterChanged(){
                vm.countsVisibilityFilter = '';
                if(vm.visibilityFilters.unassigned){
                    vm.countsVisibilityFilter = _appendToCountsFilter(vm.countsVisibilityFilter, 'unassigned');
                }
                if(vm.visibilityFilters.assignedToMe){
                    vm.countsVisibilityFilter = _appendToCountsFilter(vm.countsVisibilityFilter, 'assignedToMe');
                }
                if(vm.visibilityFilters.assignedToOthers){
                    vm.countsVisibilityFilter = _appendToCountsFilter(vm.countsVisibilityFilter, 'assignedToOthers');
                }
                vm.candidateCountsLoadingFlag = true;
                vm.getCandidateCounts(function () {
                    vm.candidateCountsLoadingFlag = false;
                });
            }
            
            function redirectToCandidates(candidateStatus) {
                var stateObject = {};
                stateObject.candidateStatuses = [];
                stateObject.candidateStatuses.push(candidateStatus);
                // do not pass visibility filters as they are no longer displayed on the screen.
                // stateObject.visibilityFilters = {
                //     unassigned: '',
                //     assignedToMe: '',
                //     allPermissible: ''
                // };
                // if(vm.visibilityFilters.unassigned){
                //     stateObject.visibilityFilters.unassigned = 'unassigned';
                // }
                // if(vm.visibilityFilters.assignedToMe){
                //     stateObject.visibilityFilters.assignedToMe = 'assignedToMe';
                // }
                // if(vm.visibilityFilters.assignedToOthers){
                //     stateObject.visibilityFilters.assignedToOthers = 'assignedToOthers';
                // }
                $state.go('missioncontrol.candidates',stateObject);
            }

            function redirectToRequisitions(){
                var stateObject = {};
                stateObject.requisitionStatesArray = ['Open','Sourced','Filled','Joined','Paused'];
                $state.go('missioncontrol.requisitions',stateObject);
            }
		}
	]);