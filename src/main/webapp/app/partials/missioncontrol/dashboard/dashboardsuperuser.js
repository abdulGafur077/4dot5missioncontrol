
/**
 * 
 */

var dashboardsuperusermodule = angular.module('4dot5.missioncontrolmodule.dashboardsuperusermodule',
  []);

dashboardsuperusermodule.constant('DASHBOARDSUPERUSERCONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.dashboardsuperusermodule',
        URL: '/dashboardsuperuser',
        CONTROLLER: 'DashboardsuperuserController',
        TEMPLATEURL: 'app/partials/missioncontrol/dashboard/dashboardsuperuser.html',
    },
    CONTROLLER: {
    	
    }
});

dashboardsuperusermodule.config(
    ['$stateProvider',
        'DASHBOARDSUPERUSERCONSTANTS',
        function($stateProvider, DASHBOARDSUPERUSERCONSTANTS) {
            $stateProvider.state(DASHBOARDSUPERUSERCONSTANTS.CONFIG.STATE, {
                url: DASHBOARDSUPERUSERCONSTANTS.CONFIG.URL,
                templateUrl: DASHBOARDSUPERUSERCONSTANTS.CONFIG.TEMPLATEURL,
                controller: DASHBOARDSUPERUSERCONSTANTS.CONFIG.CONTROLLER,
                data: {
                    requireLogin: true
                }
            });
        }
    ]);

dashboardsuperusermodule.controller('DashboardsuperuserController', 
 ['$scope',
  '$http',
  '$state',
  '$timeout',
  '$rootScope',
  '$stateParams',
  'genericService',
  'DASHBOARDSUPERUSERCONSTANTS',
  function($scope, $http, $state,$timeout,$rootScope,$stateParams, genericService, DASHBOARDSUPERUSERCONSTANTS) {
	 	console.log('DashboardSuperUserController');

    }
]);