var feedbackformsmodule = angular.module('4dot5.missioncontrolmodule.feedbackformsmodule',
    []);

feedbackformsmodule.constant('FEEDBACKFORMSCONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.feedbackforms',
        URL: '/feedbackforms',
        CONTROLLER: 'FeedBackFormsController',
        TEMPLATEURL: 'app/partials/missioncontrol/feedbackformsmodule/feedbackforms.html',
    },
      CONTROLLER: {

        }
    });


   feedbackformsmodule.config(
    ['$stateProvider',
        'FEEDBACKFORMSCONSTANTS',
        function ($stateProvider, FEEDBACKFORMSCONSTANTS) {

            $stateProvider.state(FEEDBACKFORMSCONSTANTS.CONFIG.STATE, {
                url: FEEDBACKFORMSCONSTANTS.CONFIG.URL,
                templateUrl: FEEDBACKFORMSCONSTANTS.CONFIG.TEMPLATEURL,
                controller: FEEDBACKFORMSCONSTANTS.CONFIG.CONTROLLER,
                data: {
                    requireLogin: true
                },
                params: {
                    companyObject: null,
                    userId: null,
                    accessMode: null,
                    addClient: null,
                    updateClient: null,
                    bu: null,
                    selectedCompanyId: null,
                    feedbacktype:null
                }
            });
        }
    ]);



    feedbackformsmodule.controller('FeedBackFormsController',
        ['$scope',
            '$state',
            '$window',
            '$compile',
            '$rootScope',
            '$stateParams',
            'StorageService',
            'genericService',
            'FEEDBACKFORMSCONSTANTS',
            'MESSAGECONSTANTS',
            '$timeout',
            '$filter',
            'alertsAndNotificationsService',
            function ($scope, $state, $window, $compile, $rootScope, $stateParams, StorageService, genericService, FEEDBACKFORMSCONSTANTS, MESSAGECONSTANTS, $timeout,$filter, alertsAndNotificationsService) {

            $scope.buttonText = 'Add';

            $scope.parentquestions = [];
            $scope.questionList = [];
            $scope.parentQuestionList =[];
            $scope.savedQuestionList= [];
            $scope.feedbackList=[];
            $scope.feedbacktype = $stateParams.feedbacktype;
            $scope.QnsPreview = 'Show';
            $scope.sameQuestionAdded = false;
            $scope.feedbacktype = $stateParams.feedbacktype
            $scope.companyName;
            $scope.selectedClient = {};
            $scope.allClientsOrBus = [];
            $scope.fourdot5questionList = [];
            $scope.company;
            $scope.selectedDropdown;
            console.log('In feedback controller');
            var companyid  = $rootScope.userDetails.company.companyId;
            var companyType = $rootScope.userDetails.company.companyType;
            $scope.companyName  = $rootScope.userDetails.company.companyName;
            $scope.company =  $rootScope.userDetails.company;
            $scope.selectedDropdown = $scope.companyName;
            var showConfirmationPopup = false;


            if ($scope.feedbacktype == null) {
                $scope.feedbacktype = 'R';
            }




             genericService.getObjects('api/company/gethostcompanydetails').then(function (data) {
                 if (data.id != companyid) {
                       genericService.getObjects("api/feedback/getfeedback/"+ data.id +"/" +  $scope.feedbacktype).then(function (data) {

                                    var fourdot5feedback = angular.copy(data);
                                    if(fourdot5feedback.questionList != null && fourdot5feedback.questionList.length != 0) {
                                          $scope.fourdot5questionList = angular.copy(fourdot5feedback.questionList);

                                    }
                                    }, function (error) {
                                        if($rootScope.isOnline){
                                            alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                                        }
                                    });

                    }
                }, function (error) {
                    if($rootScope.isOnline){
                        alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    }
                });


           genericService.getObjects('api/user/getuserclientsorbu/' +  $rootScope.userDetails.id + '/'+ $rootScope.userDetails.company.companyId).then(function (data) {
               for (curr in data.clientOrBUList)
                   {
                       //alert(curr.companyId);
                       //alert(curr.companyName);

                   }
                	$scope.allClientsOrBus = angular.copy(data.clientOrBUList);
                    if($scope.allClientsOrBus.length > 0){
                       	console.log('assigned first client : ' + angular.toJson($scope.client));
                        //$scope.allClientsOrBus.push(null);
                    }else{
                    	$scope.isStillProcessing = false;
                    }
                }, function (error) {
                	if($rootScope.isOnline){
                    	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    }
                });


            genericService.getObjects("api/feedback/getfeedback/"+ companyid +"/" +  $scope.feedbacktype).then(function (data) {
                     var companyfeedback = angular.copy(data);
                if (companyfeedback == "") {
                    $scope.questionList = [];
                    $scope.savedQuestionList = [];
                }
                if (companyfeedback.questionList != null) {
                    $scope.questionList = angular.copy(companyfeedback.questionList);
                    $scope.savedQuestionList = angular.copy(companyfeedback.questionList);
                }


             }, function (error) {
                if($rootScope.isOnline){
                        alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    }
            });



         $scope.getClientQuestions = function() {
                if($scope.selectedClient)
                 {
                  genericService.getObjects("api/feedback/getfeedback/"+  $scope.company.companyId +"/" +  $scope.feedbacktype).then(function (data) {
                          var companyFeedBack = angular.copy(data);
                           if (companyFeedBack.questionList != null && companyFeedBack.questionList.length != 0)
                               $scope.parentQuestionList = companyFeedBack.questionList;
                           else
                                $scope.parentQuestionList = [];
                     }, function (error) {
                        if($rootScope.isOnline){
                                alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                            }
                 });

                 genericService.getObjects("api/feedback/getfeedback/"+  $scope.selectedClient.companyId +"/" +  $scope.feedbacktype).then(function (data) {
                  var clientfeedback = angular.copy(data);
                  if (clientfeedback.questionList != null && clientfeedback.questionList.length != 0) {
                      $scope.questionList = angular.copy(clientfeedback.questionList);
                      $scope.savedQuestionList = angular.copy(clientfeedback.questionList);
                  }
                   else
                       $scope.questionList = [];
             }, function (error) {
                if($rootScope.isOnline){
                        alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    }
            });
                 }
             else
             {
                 $scope.questionList = [];
                 $scope.savedQuestionList = [];
             }


         }

        $scope.onSelectClick = function(selText){
               $scope.selectedDropdown = selText;

                if (!angular.equals($scope.savedQuestionList,$scope.questionList)){
                    showConfirmationPopup = true;
                }

                if (showConfirmationPopup) {
                    bootbox.confirm({
                        closeButton: false,
                        title: "<div class='alert alert-warning' style='margin-bottom: 0px;'><i class='fa fa-warning fa-fw fa-lg'></i><strong>Warning!</strong></div>",
                        message: "Unsaved data on the page will be lost. Do you still want to continue?",
                        buttons: {
                            confirm: {
                                label: 'Yes',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'Cancel',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            console.log('This was logged in the callback: ' + result);
                            if (result) {
                                  if ($scope.selectedDropdown == $rootScope.userDetails.company.companyName) {
                                        $scope.getParentsQuestions();
                                    } else {
                                        //$scope.selectedClient = {};
                                        $scope.questionList = angular.copy($scope.savedQuestionList);
                                        $scope.parentQuestionList = $scope.questionList;
                                        $scope.questionList = [];
                                        $scope.savedQuestionList = [];
                                    }
                            } else {
                                var compName = $('#hiddencompanyname').val();

                                if(selText != compName)
                                {
                                    $('.dropdown-menu li a').parents('.btn-group').find('.dropdown-toggle').html(compName + ' <span class="caret"></span>');
                                    $("#select-client").hide();
                                    $scope.selectedDropdown = compName;
                                }
                                else
                                {
                                    $('.dropdown-menu li a').parents('.btn-group').find('.dropdown-toggle').html('Select Client' + ' <span class="caret"></span>');
                                    $("#select-client").show();
                                    $scope.selectedDropdown = 'Select Client';
                                }
                            }
``
                        }
                    });
                }

         else {
            if ($scope.selectedDropdown == $rootScope.userDetails.company.companyName) {
                $scope.getParentsQuestions();
                $scope.questionList = [];
                $scope.savedQuestionList = [];
            } else {
                $scope.selectedClient = {};
                $scope.parentQuestionList = $scope.questionList;
                $scope.questionList = [];
                $scope.savedQuestionList = [];
            }
         }
        }

        $scope.getParentsQuestions = function(){

            genericService.getObjects("api/feedback/getfeedback/"+ $scope.company.companyId +"/" +  $scope.feedbacktype).then(function (data) {
                     var companyfeedback = angular.copy(data);
                        $scope.parentQuestionList = [];
                if (companyfeedback == "") {
                    $scope.questionList = [];
                    $scope.savedQuestionList = [];
                }
                if (companyfeedback.questionList != null) {
                    $scope.questionList = angular.copy(companyfeedback.questionList);
                    $scope.savedQuestionList = angular.copy(companyfeedback.questionList);
                }

             }, function (error) {
                if($rootScope.isOnline){
                        alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    }
            });


        }

         $scope.saveQuestions = function() {
            if($scope.selectedDropdown !=  $rootScope.userDetails.company.companyName && $scope.selectedClient.companyId == undefined)
            {
                return;
            }
           var idval  = $rootScope.userDetails.company.companyId;
            if ($scope.selectedDropdown !=  $rootScope.userDetails.company.companyName) {
                idval = $scope.selectedClient.companyId;
            }
              genericService.addObject('api/feedback/savefeedBack/' + idval +"/" + $scope.feedbacktype, $scope.questionList).then(function (data) {
                  $scope.savedQuestionList = angular.copy($scope.questionList);
               if ($scope.feedbacktype == 'R')
                  alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.FEEDBACK.RECRUITERQUESTIONS, 'success');
               else if($scope.feedbacktype == 'P')
                    alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.FEEDBACK.PHONEQUESTIONS, 'success');
               else
                    alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.FEEDBACK.INTERVIEWQUESTIONS, 'success');

            }, function (error) {
                if($rootScope.isOnline){
                    alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                }
                $scope.originalStepFive = angular.copy($scope.CompanyManagement.company.questionList);
            });
         }


          $scope.addQuestions = function() {
            if($scope.selectedDropdown !=  $rootScope.userDetails.company.companyName && $scope.selectedClient.companyId == undefined)
            {
                return;
            }

            $scope.step5clickedAddModify = true;
            $scope.sameQuestionAdded=false;
            if ($scope.questiontoAdd.length != 0 ) {
                if ($scope.buttonText == "Modify") {
                    if ($scope.questionList.indexOf($scope.questiontoAdd.trim()) == -1) {
                        var intValue = parseInt($scope.selectedQuestion);
                        $scope.questionList[intValue] = $scope.questiontoAdd;
                        $scope.buttonText = "Add";
                        $scope.questiontoAdd = ''
                        $scope.step5clickedAddModify = false;
                    } else  {
                        $scope.sameQuestionAdded=true;
                    }

                } else {
                    if ($scope.questionList.indexOf($scope.questiontoAdd.trim()) == -1) {
                        var idPos =   $scope.questionList.length + 1;
                        $scope.questionList.push($scope.questiontoAdd);
                        $scope.questiontoAdd = ''
                        $scope.step5clickedAddModify = false;
                    } else  {
                        $scope.sameQuestionAdded=true;
                    }
                }
            }
            $scope.QnsPreview = 'Show';
            initializeSliders();
        }


            $scope.displayQuestion = function(questVal,idVal) {
                $scope.selectedQuestion = idVal;
                $scope.questiontoAdd = questVal;
                $scope.buttonText = "Modify";
            }


            $scope.isQnsPreviewShown = function () {
                initializeSliders();
                if ($scope.QnsPreview == 'Show') {
                    $scope.QnsPreview = 'Close';
                } else {
                    $scope.QnsPreview = 'Show';
                }
            }

            $scope.hideshowpreview = function () {
                if ($scope.questionList.length >= 1 || $scope.parentQuestionList.length  >= 1 || $scope.fourdot5questionList.length >= 1 ) {
                    return true;
                }
            }


              $scope.deleteQuestion = function(index) {
                $scope.questionList.splice(index, 1);
                $scope.questiontoAdd = '';
                $scope.buttonText = "Add";
                initializeSliders();
            }


                $scope.checkIfFourDot5ToDisplay = function() {


                     if ($scope.selectedDropdown != $rootScope.userDetails.company.companyName)
                        {
                         if($scope.parentQuestionList != null && $scope.parentQuestionList.length  >= 1) {
                             return false;
                         } else {
                             return true;
                     }

                 }else {
                      if($scope.questionList != null && $scope.questionList.length >= 1) {
                         return false;
                     } else {
                         return true;
                     }
                 }

                }

                $scope.cancelChanges = function(){
                    showConfirmationPopup = false;
                    if (!angular.equals($scope.savedQuestionList, $scope.questionList)) {
                        $scope.questionList =  angular.copy($scope.savedQuestionList);
                        $scope.form.screeiningquestions.$setPristine();
                    }
                }
                
                var unbindStateChangeEvent = $scope.$on('$stateChangeStart', function (event, toState , toParams, fromState, fromParams) {
                    console.log('preventing');
                    event.preventDefault();
                    if (!angular.equals($scope.savedQuestionList, $scope.questionList)) {
                        console.log('Unsaved changes');
                        showConfirmationPopup = true;
                    }
                    
                    if (showConfirmationPopup) {
                        bootbox.confirm({
                            closeButton: false,
                            title: "<div class='alert alert-warning' style='margin-bottom: 0px;'><i class='fa fa-warning fa-fw fa-lg'></i><strong>Warning!</strong></div>",
                            message: "Unsaved data on the page will be lost. Do you still want to continue?",
                            buttons: {
                                confirm: {
                                    label: 'Yes',
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: 'Cancel',
                                    className: 'btn-danger'
                                }
                            },
                            callback: function (result) {
                                if (result) {
                                    if ($rootScope.switchClicked) {
                                        $scope.getCompaniesForLoggedinUser(function(response){
                                            $scope.cancelChanges();
                                            $('#myModal').modal('show');
                                        });
                                    } else {
                                        unbindStateChangeEvent();
                                        $scope.cancelChanges();
                                        if (toState.name === 'login') {
                                            $rootScope.$emit("CallLogoutMethod", { logoutnow: true });
                                        } else {
                                            $state.go(toState.name, toParams, { reload: true });
                                        }
                                    }
                                } else {
                                    showConfirmationPopup = false;
                                    $rootScope.switchClicked = false;
                                }
                            }
                        });
                    } else {
                        if ($rootScope.switchClicked) {
                            $scope.getCompaniesForLoggedinUser(function(response){
                                $('#myModal').modal('show');
                            });
                        } else {
                            unbindStateChangeEvent();
                            $rootScope.switchClicked = false;
                            if (toState.name === 'login') {
                                $rootScope.$emit("CallLogoutMethod", { logoutnow: true });
                            } else {
                                $state.go(toState.name, toParams, { reload: true });
                            }
                        }
                    }
                });
 }]);
