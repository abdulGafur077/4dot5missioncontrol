/**
 * 
 */
var jobcandidatecomparisonmodule = angular.module('4dot5.missioncontrolmodule.jobcandidatecomparisonmodule', []);

jobcandidatecomparisonmodule.constant('JOBCANDIDATECOMPARISONCONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.candidateJobComparison',
        URL: '/candidateJobComparison',
        CONTROLLER: 'CandidateJobComparisonViewController',
        CONTROLLERAS: 'candidateJobComparisonView',
        TEMPLATEURL: 'app/partials/missioncontrol/jobcandidatecomparison/jobcandidatecomparison.html'
    }
});

jobcandidatecomparisonmodule.config(
    ['$stateProvider',
        'JOBCANDIDATECOMPARISONCONSTANTS',
        function($stateProvider, JOBCANDIDATECOMPARISONCONSTANTS) {
            $stateProvider.state(JOBCANDIDATECOMPARISONCONSTANTS.CONFIG.STATE, {
                url: JOBCANDIDATECOMPARISONCONSTANTS.CONFIG.URL,
                templateUrl: JOBCANDIDATECOMPARISONCONSTANTS.CONFIG.TEMPLATEURL,
                controller: JOBCANDIDATECOMPARISONCONSTANTS.CONFIG.CONTROLLER,
                controllerAs: JOBCANDIDATECOMPARISONCONSTANTS.CONFIG.CONTROLLERAS,
                data: {
                    requireLogin: true
                },
                params: {
                    jobMatchId: null
                }
            });
        }
    ]);

jobcandidatecomparisonmodule.controller('CandidateJobComparisonViewController',CandidateJobComparisonViewController);

jobcandidatecomparisonmodule.$inject = ['$stateParams'];

function CandidateJobComparisonViewController($stateParams) {
    var vm = this;
    vm.jobMatchId = $stateParams.jobMatchId;
}