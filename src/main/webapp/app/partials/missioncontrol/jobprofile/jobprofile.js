/**
 * 
 */

var jobprofilemodule = angular.module('4dot5.missioncontrolmodule.jobprofilemodule', []);

jobprofilemodule.constant('JOBPROFILECONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.jobprofile',
        URL: '/jobprofile',
        CONTROLLER: 'JobProfileViewController',
        CONTROLLERAS: 'jobProfileView',
        TEMPLATEURL: 'app/partials/missioncontrol/jobprofile/jobprofile.html'
    }
});

jobprofilemodule.config(
    ['$stateProvider', 'JOBPROFILECONSTANTS',
        function($stateProvider, JOBPROFILECONSTANTS) {
            $stateProvider.state(JOBPROFILECONSTANTS.CONFIG.STATE, {
                url: JOBPROFILECONSTANTS.CONFIG.URL,
                templateUrl: JOBPROFILECONSTANTS.CONFIG.TEMPLATEURL,
                controller: JOBPROFILECONSTANTS.CONFIG.CONTROLLER,
                controllerAs: JOBPROFILECONSTANTS.CONFIG.CONTROLLERAS,
                data: {
                    requireLogin: true
                },
                params: {
                    jobId: null
                }
            });
        }
    ]);

jobprofilemodule.controller('JobProfileViewController', JobProfileViewController);

jobprofilemodule.$inject = ['$stateParams','$rootScope'];

function JobProfileViewController($stateParams, $rootScope){
    var vm = this;
    //variables
    vm.breadCrumbLabel = 'Requisitions';
    vm.breadCrumbStateAddress = 'missioncontrol.requisitions';
    // functions
    vm.init = init;

    function init(){
        vm.jobId = $stateParams.jobId;
        if(!_.isNull($rootScope.fromState) && $rootScope.fromState.name == 'missioncontrol.candidates'){
            vm.breadCrumbLabel = 'Candidates';
            vm.breadCrumbStateAddress = 'missioncontrol.candidates';
        }
    }

    // call the initialize function
    vm.init();
}
