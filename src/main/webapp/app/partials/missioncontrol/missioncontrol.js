/**
 * This module is a Parent for all Mission Control Functionalities
 * (or Pages).This Module is maintained just for maintaining
 * Separation of Concern. Gives Easy maintenance if in future
 * 4dot5 has another major Responsibilities.
 */

var missioncontrolmodule = angular.module('4dot5.missioncontrolmodule',
    [
        '4dot5.missioncontrolmodule.dashboardmodule',
        '4dot5.missioncontrolmodule.changepasswordmodule',
        '4dot5.missioncontrolmodule.usermanagementmodule',
        '4dot5.missioncontrolmodule.teammembersmodule',
        '4dot5.missioncontrolmodule.roleentityscopemodule',
        '4dot5.missioncontrolmodule.companymanagementmodule',
        '4dot5.missioncontrolmodule.clientmanagementmodule',
        '4dot5.missioncontrolmodule.clientorbumanagement',
        '4dot5.missioncontrolmodule.uploadresumemodule',
        '4dot5.missioncontrolmodule.addCandidatesModule',
        '4dot5.missioncontrolmodule.uploadreqmodule',
        '4dot5.missioncontrolmodule.candidatemodule',
        '4dot5.missioncontrolmodule.candidateJobMatchesWorkflowModule',
        '4dot5.missioncontrolmodule.candidateprofilemodule',
        '4dot5.missioncontrolmodule.jobprofilemodule',
        '4dot5.missioncontrolmodule.passivecandidatesmodule',
        '4dot5.missioncontrolmodule.activecandidatesprofilemodule',
        '4dot5.missioncontrolmodule.roleentityscopemodule',
        '4dot5.missioncontrolmodule.notificationmodule',
        '4dot5.missioncontrolmodule.auditlogsmodule',
        '4dot5.missioncontrolmodule.requisitionsmodule',
        '4dot5.missioncontrolmodule.candidatecomparemodule',
        '4dot5.missioncontrolmodule.jobcandidatecomparisonmodule',
        '4dot5.missioncontrolmodule.reportsmodule',
        '4dot5.missioncontrolmodule.candidateperformancemodule',
        '4dot5.missioncontrolmodule.teammembercandidatesmodule',
        '4dot5.missioncontrolmodule.dashboardsuperusermodule',
        '4dot5.missioncontrolmodule.reportsmodule.corporateorclientreportmodule',
        '4dot5.missioncontrolmodule.notprocessedcandidatesmodule',
        '4dot5.missioncontrolmodule.transactionsmodule',
        '4dot5.missioncontrolmodule.notificationpreferencemodule',
        '4dot5.missioncontrolmodule.settingsmodule',
        '4dot5.missioncontrol.modelsModule',
        '4dot5.missioncontrol.servicesmodule',
        '4dot5.missioncontrolmodule.feedbackformsmodule',
        '4dot5.missioncontrolmodule.assessmentManagementModule'
    ]);

missioncontrolmodule.constant('MISSIONCONTROLCONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol',
        URL: '/missioncontrol',
        CONTROLLER: 'MissionControlController',
        TEMPLATEURL: 'app/partials/missioncontrol/missioncontrol.html',
    },
    CONTROLLER: {
        GET_COMPANYIES_FOR_LOGGEDINUSER: 'api/company/getallcompanies'
    }
});

missioncontrolmodule.config(
    ['$stateProvider',
        'MISSIONCONTROLCONSTANTS',
        function ($stateProvider, MISSIONCONTROLCONSTANTS) {
            $stateProvider.state(MISSIONCONTROLCONSTANTS.CONFIG.STATE, {
                url: MISSIONCONTROLCONSTANTS.CONFIG.URL,
                templateUrl: MISSIONCONTROLCONSTANTS.CONFIG.TEMPLATEURL,
                controller: MISSIONCONTROLCONSTANTS.CONFIG.CONTROLLER,
                data: {
                    requireLogin: true,
                    companyObject: null,
                    companyDeletedShowPopup: null
                }
            });

        }
    ]);

missioncontrolmodule.controller('MissionControlController',
    ['$scope',
        '$state',
        '$filter',
        '$stateParams',
        '$rootScope',
        'StorageService',
        'genericService',
        '$timeout',
        'ngToast',
        'ngNotify',
        'MISSIONCONTROLCONSTANTS',
        'MESSAGECONSTANTS',
        function ($scope, $state, $filter, $stateParams, $rootScope, StorageService, genericService, $timeout, ngToast, ngNotify, MISSIONCONTROLCONSTANTS, MESSAGECONSTANTS) {
            var page = 0;
            var size = 500;
            var sort = 'name';
            var sortDir = 'asc';
            $scope.searchTerm = '';
            
            var switchCompanyRepeat = 0;
            $rootScope.switchClicked = false;
            $scope.companyListLoaded = false;
            var companiesForLoggedInUser = [];
            $scope.companies = [];
            $scope.showLogoutButtonInPopup = false;
            $rootScope.CompanySelectedFromPopup = false;
            
            $scope.NormalUser = false;
            $rootScope.usersEmptyMessage = ''
            
            $scope.filterValuesToActiveCandidates = [];
            $scope.filterValuesToPassiveCandidates = [];

            if ($rootScope.userDetails.roleScreenRef.role.name == 'CorporateAdmin') {
                $rootScope.SelectedCompanyId = $rootScope.userDetails.company.companyId;
            }

            if (angular.isDefined($rootScope.newlyCreatedCompanyId)) {
                $rootScope.SelectedCompanyId = $rootScope.newlyCreatedCompanyId;
            }

            $scope.modelOptions = {
                updateOn: 'blur'
            };

            /**
             * This method is used to do $scope.$apply()
             * with out breaking any digest cycles
             */
            $scope.safeApply = function(fn) {
                var phase = this.$root.$$phase;
                if(phase == '$apply' || phase == '$digest') {
                    if(fn && (typeof(fn) === 'function')) {
                    fn();
                    }
                } else {
                    this.$apply(fn);
                }
            };
            
            /**
             * This is a helper method which checks for 
             * element existance in $scope.companies array
             */
            var checkCompanyExists = function (element) {
                for (var i = 0; i < $scope.companies.length; i++) {
                    if (($scope.companies[i].companyId === element.companyId) && ($scope.companies[i].companyName === element.companyName)) {
                        return true;
                    }
                }
                return false;
            };
            
            /**
             * This method is used to get all companies, to show in companies pop up
             */
            $scope.getCompaniesForLoggedinUser = function (callback) {
                genericService.getObjects(MISSIONCONTROLCONSTANTS.CONTROLLER.GET_COMPANYIES_FOR_LOGGEDINUSER + '/' + $rootScope.userDetails.roleScreenRef.role.id + '/' + $rootScope.userDetails.id + '?page=' + page + '&size=' + size + '&sort=' + sort + '&sortDir=' + sortDir).then(function (data) {
                    companiesForLoggedInUser = data;
                    if (data.length == 0) {
                        $scope.showLogoutButtonInPopup = true;
                    }
                    $scope.companies = [];
                    data.forEach(function (element) {
                        if (!checkCompanyExists(element)) {
                            $scope.companies.push(element);
                        }
                    });
                    $scope.companyListLoaded = true;
                    $scope.companytype = $rootScope.userDetails.company.companyType;
                    
                    if(typeof callback != "undefined"){
                    	return callback($scope.companies);
                    }
                    
                }, function (error) {
                    $rootScope.throwError(error);
                    if(typeof callback != "undefined"){
                    	return callback("done");
                    }
                });
            };

            $rootScope.$on("switchnow", function (event, data) {
                console.log('switchnow in MC : '+ switchCompanyRepeat);
                //companiesForLoggedInUser = [];
                //$scope.companyListLoaded = false;
                if ((switchCompanyRepeat == 0)) {
                	console.log('invoking modal');
                	//$('#myModal').modal('show');
                	switchCompanyRepeat++;
                    $scope.getCompaniesForLoggedinUser(function(response){
                    	$('#myModal').modal('show');
                    });
                    
                }
            });

            if (angular.isUndefined($rootScope.userDetails) || ($rootScope.userDetails === null)) {
                $state.go('login', null, { reload: true });
            }
            
            /**
             * This method is used to show or hide functionalities based
             * on role screen ref provided by login api.
             */
            $scope.hasAccess = function (screenId) {
                var accessGranted = false;
                for (var i = 0; i < $rootScope.userDetails.roleScreenRef.screen.length; i++) {
                    if ($rootScope.userDetails.roleScreenRef.screen[i].id === screenId) {
                        accessGranted = true;
                        break;
                    }
                }
                return accessGranted;
            };

            /**
             * This method checks whether the user is superuser or not
             * @param user - to which check has to be happen for role
             */
            $scope.isLogedinUser = function(userdetails){
                if(angular.equals(userdetails.userId,$rootScope.userDetails.id)){
                    return true;
                }
                return false;
            };

            /**
             * This method checks whether the user is superuser or not
             * @param user - to which check has to be happen for role
             */
            $scope.isinHost = function(){
                if(angular.equals($rootScope.userDetails.company.companyType,MESSAGECONSTANTS.COMPANY_TYPES.FOURDOTFIVE)){
                    return true;
                }
                return false;
            };

            /**
             * This method checks whether the user is superuser or not
             * @param user - to which check has to be happen for role
             */
            $scope.isinStaffing = function(){
                if(angular.equals($rootScope.userDetails.company.companyType,MESSAGECONSTANTS.COMPANY_TYPES.STAFFING)){
                    return true;
                }
                return false;
            };

            /**
             * This method checks whether the user is superuser or not
             * @param user - to which check has to be happen for role
             */
            $scope.isinCorporate = function(){
                if(angular.equals($rootScope.userDetails.company.companyType,MESSAGECONSTANTS.COMPANY_TYPES.CORPORATE)){
                    return true;
                }
                return false;
            };

            /**
             * This method checks whether the user is superuser or not
             * @param user - to which check has to be happen for role
             */
            $scope.isSuperUser = function(role){
                if(angular.equals(role.id,MESSAGECONSTANTS.ROLEIDS.SUPERUSER)){
                    return true;
                }
                return false;
            };

            /**
             * This method checks whether the user is 4dot5Admin or not
             * @param user - to which check has to be happen for role
             */
            $scope.isFourDotFiveAdmin = function(role){
                if(angular.equals(role.id,MESSAGECONSTANTS.ROLEIDS.FOURDOTFIVE_ADMIN)){
                    return true;
                }
                return false;
            };

            /**
             * This method checks whether the user is CompanyAdmin or not
             * @param user - to which check has to be happen for role
             */
            $scope.isCompanyAdmin = function(role){
                if(angular.equals(role.id,MESSAGECONSTANTS.ROLEIDS.STAFFING_ADMIN) || angular.equals(role.id,MESSAGECONSTANTS.ROLEIDS.CORPORATE_ADMIN)){
                    return true;
                }
                return false;
            };

            /**
             * This method checks whether the user is RecruitingManager or not
             * @param user - to which check has to be happen for role
             */
            $scope.isRecruitingManager = function(role){
                if(angular.equals(role.id,MESSAGECONSTANTS.ROLEIDS.STAFFING_REC_MANAGER) || angular.equals(role.id,MESSAGECONSTANTS.ROLEIDS.CORPORATE_REC_MANAGER)){
                    return true;
                }
                return false;
            };

            /**
             * This method checks whether the user is SeniorRecruiter or not
             * @param user - to which check has to be happen for role
             */
            $scope.isSeniorRecruiter = function(role){
                if(angular.equals(role.id,MESSAGECONSTANTS.ROLEIDS.STAFFING_SR_RECRUITER) || angular.equals(role.id,MESSAGECONSTANTS.ROLEIDS.CORPORATE_SR_RECRUITER)){
                    return true;
                }
                return false;
            };

            /**
             * This method checks whether the user is Recruiter or not
             * @param user - to which check has to be happen for role
             */
            $scope.isRecruiter = function(role){
                if(angular.equals(role.id,MESSAGECONSTANTS.ROLEIDS.STAFFING_RECRUITER) || angular.equals(role.id,MESSAGECONSTANTS.ROLEIDS.CORPORATE_RECRUITER)){
                	return true;
                }
                return false;
            };

            /**
             * This method is used to make limit operations on name with extensions as '...'
             * @param longname - full name for which limit to be applied
             * @param limittomax - for number of characters
             */
            $scope.handleLongName = function(longname,limittomax){
                if(longname.length > limittomax){
                    longname = angular.copy($filter('limitTo')(longname,(limittomax-3),0).concat('...'));
                }
                return longname;
            };

            /**
             * This method is used to check all names 
             * to avoid showing null on UI
             */
            $scope.getFullName = function(firstName,lastName){
            	if(lastName != null){
            		return firstName + " " + lastName;
            	}else{
            		return firstName;
            	}
            };

            $scope.isContentinObjSame = function(wordone, wordtwo){
                if(angular.isDefined(wordtwo) && (wordtwo !== null) && angular.equals(wordone.toLowerCase(),wordtwo.toLowerCase())){
                    return true
                }
                return false;
            };
            
            /**
             * This method is called when user selects any company
             * from select company pop up.
             */
            $scope.getCompany = function (company) {
                $scope.searchTerm = '';
                $rootScope.companyDeletedShowPopup = false;
                $rootScope.SelectedCompanyId = company.companyId;
                $rootScope.CompanySelectedFromPopup = true;
                if (StorageService.get('SelectedHostCompany') != null) {
                    StorageService.set('SelectedHostCompany', null);
                }
                if (company.companyType == 'Host') {
                    StorageService.set('SelectedHostCompany', company);
                } else {
                    StorageService.set('companyDetails', company);
                }
                $rootScope.userDetails.company.companyId = company.companyId;
                $rootScope.userDetails.company.companyName = company.companyName;
                $rootScope.userDetails.company.companyType = company.companyType;
                $rootScope.userDetails.company.companyState = company.companyState;
                $rootScope.userDetails.company.companySelected = true;

                StorageService.setCookie('userDetails', $rootScope.userDetails, { 'expires': new Date(Date.now() + $rootScope.tokenvalidity) });
                $scope.companytype = company.companyType;
                $rootScope.switchClicked = false;
                destroyModal();
                if ($rootScope.userDetails.roleScreenRef.role.name === '4dot5SuperUser' && $rootScope.userDetails.company.companyType === 'Host') {
                    $state.go('missioncontrol.dashboardsuperusermodule');
                } else {
                    if (StorageService.get('companyDetails') != null && $rootScope.userDetails.company.companyState == 'Active') {
                        $state.go('missioncontrol.dashboard', null, { reload: true });
                    }else{
                        $state.go('missioncontrol.companymanagement', null, { reload: true });
                    }
                }
            };
            
            /**
             * This function is called when user clicks on 'ADD COMPANY'
             * from select company pop up. 
             */
            $scope.addCompany = function () {
                destroyModal();
                $rootScope.companyDeletedShowPopup = false;
                StorageService.remove('companyDetails');
                StorageService.remove('SelectedHostCompany');
                $rootScope.userDetails.company.companyId = null;
                $rootScope.userDetails.company.companyName = null;
                $rootScope.userDetails.company.companyType = null;
                $rootScope.userDetails.company.companyState = 'Draft'
                $rootScope.userDetails.company.companySelected = false;
                StorageService.setCookie('userDetails', angular.copy($rootScope.userDetails), { 'expires': new Date(Date.now() + $rootScope.tokenvalidity) });
                $state.go('missioncontrol.companymanagement', null, { reload: true });
            };

            /**
             * Function to destroy modal
             */
            function destroyModal() {
            	switchCompanyRepeat = 0;
            	$rootScope.switchClicked = false;
                var loadingModal = $('#myModal');
                loadingModal.modal("hide");
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
            }

            $scope.logoutAsNoCompaniesRelated = function () {
                destroyModal();
                $rootScope.$emit("CallLogoutMethod");
                // $timeout(function () { $rootScope.$emit("CallLogoutMethod"); }, 200).then( function() {
                //     console.log("I'm called only after the timeout.");
                //     destroyModal();
                // });
            };

            $scope.dismissModalAndDestroy = function () {
            	$rootScope.switchClicked = false;
                destroyModal();
            };

            /**
             * This condition shows popup when entering to missioncontrol application based on different variables
             * ($rootScope.userDetails.company.companyId != null) ---> true ; only when user is not in create company flow( 1st step)
             * $rootScope.companyDeletedShowPopup ---> true ; only at the point of company deletion untill getting all companies in popup again.
             * $rootScope.userDetails.shouldPopupBeDisplayed ---> true ; only if server side api after login returns true;
             * $rootScope.userDetails.company.companySelected ---> true ; only if a company is selected from popup.  
             * angular.equals(StorageService.get('companyDetails'), null) only if a company is not selected from popup.---> true;
             */
            if ($rootScope.userDetails.company.companyId != null || $rootScope.companyDeletedShowPopup) {
                $rootScope.userDetails.shouldPopupBeDisplayed = true;
                if ($rootScope.userDetails.shouldPopupBeDisplayed && !$rootScope.userDetails.company.companySelected && angular.equals(StorageService.get('companyDetails'), null)) {
                    $scope.NormalUser = true;
                    $('#myModal').modal('show');
                    $scope.getCompaniesForLoggedinUser();
                }
            }

            /**
             * This method is used for showing app level user messages
             * This method is used for showing messages irrespective of time interval
             */
            // $scope.showNotification = function (messagedata, messagetype) {
            //     switch (messagetype) {
            //         case 'success':
            //             ngNotify.set('&nbsp;' + messagedata, {
            //                 type: 'success',
            //                 html: true,
            //                 sticky: true
            //             });
            //             break;
            //         case 'info':
            //             ngNotify.set('&nbsp;' + messagedata, {
            //                 type: 'info',
            //                 html: true,
            //                 sticky: true
            //             });
            //             break;
            //         case 'warning':
            //             ngNotify.set('&nbsp;' + messagedata, {
            //                 type: 'warn',
            //                 html: true,
            //                 sticky: true
            //             });
            //             break;
            //         default:
            //             ngNotify.set(" ")
            //     }
            // };
            
            /**
             * This method is used for showing app level user messages
             * This method is used for showing messages based on time interval provided as a param
             */
            //  $scope.notify = function (messagedata, messagetype) {
            //      console.log('making disableScreen true');
            //      $rootScope.disableScreen = true;
            //      switch (messagetype) {
	        //          case 'warning':
	        //         //  <i class="fa fa-warning fa-fw fa-lg"></i> 
	        //              ngNotify.set('&nbsp;' + messagedata, {
	        //                  type: 'warn',
	        //                  html: true,
	        //                 //  duration : duration,
	        //                   sticky: true
	        //              });
	        //              break;
	        //          case 'danger':
	        //         //  <i class="fa fa-times-circle fa-fw fa-lg"></i>
	        //              ngNotify.set('&nbsp;' + messagedata, {
	        //                  type: 'error',
	        //                  html: true,
	        //                 //  duration : duration,
	        //                  sticky: true
	        //              });
	        //              break;
	        //          default:
	        //              ngNotify.set(" ")
            //      }
            //  };
             
            /**
             * This condition executes at the starting of the page
             * This will maintain same height for both leftnav and the rightside loaded page.
             */
            $(document).ready(function () {
                $("#nav-col").addClass("vertilizeheight");
                $('#menubars').click(function () {
                    $("#nav-col").toggleClass("vertilizeheight", 200, "linear");
                });
            });

             /**
             * This function is used to hide and show admin menu icon in left nav
             * @ current implementation is if no child menu items available
             * 	 for admin menu icon, then it will be hidden
             */
            $timeout(function () {
                var myList = document.getElementById('adminmenulist');
                if (!(myList.children.length > 0)) {
                    angular.element(document.getElementById('adminmenu')).css("display", "none");
                }
            }, 800)
        }
    ]);
