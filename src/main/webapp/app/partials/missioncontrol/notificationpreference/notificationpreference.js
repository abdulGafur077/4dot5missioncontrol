/**
 * 
 */
var notificationpreferencemodule = angular.module('4dot5.missioncontrolmodule.notificationpreferencemodule', []);

notificationpreferencemodule.constant('NOTIFICATIONPREFERENCECONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.notificationpreference',
        URL: '/notificationpreference',
        CONTROLLER: 'NotificationPreferenceController',
        TEMPLATEURL: 'app/partials/missioncontrol/notificationpreference/notificationpreference.html',
    },
    CONTROLLER: {
    	GET_NOTIFICATION_PREFERENCE :'api/notification/getnotificationprefs'
    }
});

notificationpreferencemodule.config(
    ['$stateProvider',
        'NOTIFICATIONPREFERENCECONSTANTS',
        function ($stateProvider, NOTIFICATIONPREFERENCECONSTANTS) {
            $stateProvider.state(NOTIFICATIONPREFERENCECONSTANTS.CONFIG.STATE, {
                url: NOTIFICATIONPREFERENCECONSTANTS.CONFIG.URL,
                templateUrl: NOTIFICATIONPREFERENCECONSTANTS.CONFIG.TEMPLATEURL,
                controller: NOTIFICATIONPREFERENCECONSTANTS.CONFIG.CONTROLLER,
                data: {
                    requireLogin: true
                },
                params: {
                    userId: null,
                },
                resolve: {
                    MyNotificationPreferenceData: function ($rootScope, $stateParams, genericService, StorageService) {
                        return genericService.getObjects(StorageService.get('baseurl') + NOTIFICATIONPREFERENCECONSTANTS.CONTROLLER.GET_NOTIFICATION_PREFERENCE + '/' + angular.copy($stateParams.userId) + '/' + $rootScope.userDetails.company.companyId);
                    }
                }
            });
        }
    ]);

notificationpreferencemodule.controller('NotificationPreferenceController',
    ['$scope',
        '$state',
        '$timeout',
        '$rootScope',
        '$stateParams',
        'genericService',
        'StorageService',
        'utilityService',
        'MyNotificationPreferenceData',
        'MESSAGECONSTANTS',
        'NOTIFICATIONPREFERENCECONSTANTS',
        'alertsAndNotificationsService',
        function ($scope, $state, $timeout, $rootScope, $stateParams, genericService, StorageService, utilityService, MyNotificationPreferenceData, MESSAGECONSTANTS, NOTIFICATIONPREFERENCECONSTANTS, alertsAndNotificationsService) {
            
	    	$scope.notificationPrefsForm = {};
	    	var showConfirmationPopup = false;
	    	
    		$scope.TransactionNotificationPreferences = angular.copy(MyNotificationPreferenceData);
            $scope.notificationPreferencesSavedObj = angular.copy(MyNotificationPreferenceData);

            var isPreferencesChanged = function(){
                return !angular.equals($scope.TransactionNotificationPreferences,$scope.notificationPreferencesSavedObj);
            }
          
            $scope.saveNotificationPreferences = function(){
                if(isPreferencesChanged()){
                    //$scope.notificationPreferencesSavedObj = angular.copy($scope.TransactionNotificationPreferences);
                    utilityService.saveNotificationPrefs(angular.copy($stateParams.userId),$rootScope.userDetails.company.companyId,$scope.TransactionNotificationPreferences).then(function(data){
                        console.log('success : ' + angular.toJson(data));
                        if($scope.isLogedinUser(data.user.userDetails)){
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.NOTIFICATION_PREFS.OWN_NP_UPDATE,'success');
                        }else{
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.NOTIFICATION_PREFS.NP_FOR_USER +
                             " '<b><i class='wrap-word'>" +
                              $scope.handleLongName($scope.getFullName(data.user.userDetails.firstName,data.user.userDetails.lastName),50) +
                               "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.NOTIFICATION_PREFS.UPDATESUCCESS,'success');
                        }
                        $scope.notificationPreferencesSavedObj = angular.copy(data);
                        $scope.TransactionNotificationPreferences = angular.copy(data);
                        $scope.notificationPrefsForm.$setPristine();
                    },function(error){
                    	if($rootScope.isOnline){
                        	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                        }
                    	$scope.TransactionNotificationPreferences = angular.copy($scope.notificationPreferencesSavedObj);
                    });
                }else {
                	alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.NOTHING_TO_UPDATE,'success');
                }
            }

            $scope.cancelChanges = function(){
                showConfirmationPopup = false;
                if(isPreferencesChanged()){
                    $scope.TransactionNotificationPreferences =  angular.copy($scope.notificationPreferencesSavedObj);
                    $scope.notificationPrefsForm.$setPristine();
                }
            }

            var unbindStateChangeEvent = $scope.$on('$stateChangeStart', function (event, toState , toParams, fromState, fromParams) {
                console.log('preventing');
                event.preventDefault();
                if (isPreferencesChanged()) {
                    console.log('Unsaved changes');
                    showConfirmationPopup = true;
                } 
                
                if (showConfirmationPopup) {
                    bootbox.confirm({
                        closeButton: false,
                        title: "<div class='alert alert-warning' style='margin-bottom: 0px;'><i class='fa fa-warning fa-fw fa-lg'></i><strong>Warning!</strong></div>",
                        message: "Unsaved data on the page will be lost. Do you still want to continue?",
                        buttons: {
                            confirm: {
                                label: 'Yes',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'Cancel',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            if (result) {
                                if ($rootScope.switchClicked) {
                                    $scope.getCompaniesForLoggedinUser(function(response){
                                        $scope.cancelChanges();
                                    	$('#myModal').modal('show');
                                    });
                                } else {
                                    unbindStateChangeEvent();
                                    $scope.cancelChanges();
                                    if (toState.name === 'login') {
                                        $rootScope.$emit("CallLogoutMethod", { logoutnow: true });
                                    } else {
                                        $state.go(toState.name, toParams, { reload: true });
                                    }
                                }
                            } else {
                                showConfirmationPopup = false;
                                $rootScope.switchClicked = false;
                            }
                        }
                    });
                } else {
                    if ($rootScope.switchClicked) {
                        $scope.getCompaniesForLoggedinUser(function(response){
                        	$('#myModal').modal('show');
                        });
                    } else {
                        unbindStateChangeEvent();
                        $rootScope.switchClicked = false;
                        if (toState.name === 'login') {
                            $rootScope.$emit("CallLogoutMethod", { logoutnow: true });
                        } else {
                            $state.go(toState.name, toParams, { reload: true });
                        }
                    }
                }
            });
        }
    ]);