/**
 * 
 */
var notificationmodule = angular.module('4dot5.missioncontrolmodule.notificationmodule',[]);

notificationmodule.constant('NOTIFICATIONCONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.notifications',
        URL: '/notifications',
        CONTROLLER: 'NotificationController',
        TEMPLATEURL: 'app/partials/missioncontrol/notifications/notifications.html'
    },
    CONTROLLER: {

    }
});

notificationmodule.config(
	    ['$stateProvider',
	        'NOTIFICATIONCONSTANTS',
	        function ($stateProvider, NOTIFICATIONCONSTANTS) {
	            $stateProvider.state(NOTIFICATIONCONSTANTS.CONFIG.STATE, {
	                url: NOTIFICATIONCONSTANTS.CONFIG.URL,
	                templateUrl: NOTIFICATIONCONSTANTS.CONFIG.TEMPLATEURL,
	                controller: NOTIFICATIONCONSTANTS.CONFIG.CONTROLLER,
	                data: {
	                    requireLogin: true
	                },
                    params: {
                        searchObject: null,
                        context: null
                    }
	            });
	        }
	    ]);

notificationmodule.controller('NotificationController',
    ['$scope','$rootScope','$stateParams', function ($scope, $rootScope, $stateParams) {
        // do nothing
        if(_.isNull($stateParams.searchObject)){
            $scope.searchObject = {};
            $scope.searchObject.page = 1;
            $scope.searchObject.size = 20;
            $scope.searchObject.companyId = $rootScope.userDetails.company.companyId;
        }else{
            $scope.searchObject = $stateParams.searchObject;
            $scope.searchObject.page = 1;
            $scope.searchObject.size = 20;
        }
        if(_.isNull($stateParams.context)){
            $scope.context = 'global';
        }else{
            $scope.context = $stateParams.context;
        }
        $scope.contextLabel = '';
        if($scope.context == 'candidateCard'){
            $scope.contextLabel = 'Candidate Card Notifications';
        }else if($scope.context == 'candidateJobMatchCard'){
            $scope.contextLabel = 'Candidate Job Card Notifications';
        }else if($scope.context == 'requisitionCard'){
            $scope.contextLabel = 'Requisition Card Notifications';
        }
        $scope.userNotificationControls = {};
        $scope.userNotificationControls.setNewNotificationsAsViewed = null;
        $scope.viewAllNotifications = false;
        $scope.numberOfNewNotifications = 0;
        $scope.showFiltersFlag = true;
    }
]);