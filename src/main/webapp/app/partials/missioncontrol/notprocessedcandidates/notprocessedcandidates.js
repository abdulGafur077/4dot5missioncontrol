/**
 * 
 */
var notprocessedcandidatesmodule = angular.module('4dot5.missioncontrolmodule.notprocessedcandidatesmodule',['datatables','ngResource']);

notprocessedcandidatesmodule.constant('NOTPROCESSEDCANDIDATESCONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.notprocessedcandidates',
        URL: '/notprocessedcandidates',
        CONTROLLER: 'NotprocessedcandidatesController',
        TEMPLATEURL: 'app/partials/missioncontrol/notprocessedcandidates/notprocessedcandidates.html',
    },
    CONTROLLER: {
        RESUME_DETAILS: 'data/resumeData.json'
    }
});

notprocessedcandidatesmodule.config(
	    ['$stateProvider',
	        'NOTPROCESSEDCANDIDATESCONSTANTS',
	        function ($stateProvider, NOTPROCESSEDCANDIDATESCONSTANTS) {
	            $stateProvider.state(NOTPROCESSEDCANDIDATESCONSTANTS.CONFIG.STATE, {
	                url: NOTPROCESSEDCANDIDATESCONSTANTS.CONFIG.URL,
	                templateUrl: NOTPROCESSEDCANDIDATESCONSTANTS.CONFIG.TEMPLATEURL,
	                controller: NOTPROCESSEDCANDIDATESCONSTANTS.CONFIG.CONTROLLER,
	                data: {
	                    requireLogin: true
	                }
	            });
	        }
	    ]);

notprocessedcandidatesmodule.controller('NotprocessedcandidatesController',
		['$scope',
         '$state',
         '$timeout',
         '$rootScope',
         '$stateParams',
         'genericService',
         'NOTPROCESSEDCANDIDATESCONSTANTS',
         'StorageService',
         'DTOptionsBuilder',
         '$resource',
         function ($scope, $state, $timeout, $rootScope, $stateParams, genericService, NOTPROCESSEDCANDIDATESCONSTANTS, StorageService, DTOptionsBuilder,$resource) {
			console.log('NotprocessedcandidatesController');

            $scope.query = {
                    searchString : ""
                };
                
            $scope.resumeDetails = [];
            var cloudPath='http://martinfowler.com/ieeeSoftware/';
            $scope.resume=function(){               
                var uri = cloudPath + 'whenType.pdf';
                var link = angular.element('<a href="' + uri + '" target="_blank"></a>');

                angular.element(document.body).append(link);

                link[0].click();
                link.remove();
            }

            //  genericService.getObjects(StorageService.get('baseurl') + NOTPROCESSEDCANDIDATESCONSTANTS.CONTROLLER.RESUME_DETAILS).then(function (data) {
            //     $scope.resumeDetails = data;
            //     var pdfvalue = $scope.resumeDetails.Recruiter;
            //     console.log('$scope.resumeDetailsList' + angular.toJson($scope.resumeDetails));
            // }, function (error) {
            //     console.log('error : ' + angular.toJson(error));
            // });

            $resource('data/resumeData.json').query().$promise.then(function(resumeDetails) {
                    $scope.resumeDetails = resumeDetails;
                });               
                
                // $scope.dtOptions = DTOptionsBuilder.newOptions()
                //     .withPaginationType('full_numbers')
                //     .withOption('bInfo',true)
                //     .withOption('searching',false)
                //     .withOption('lengthChange',false);

                $scope.dtOptions = DTOptionsBuilder.newOptions()
                    .withPaginationType('full_numbers')
                    .withOption('bInfo',true)
                    .withOption('searching',true)
                    .withOption('lengthChange',false)
                    .withOption('bFilter', true)
                    .withOption('language', {zeroRecords: 'No records to display'})
                    .withBootstrap(); 


               $scope.search = function (row) {
                return ((angular.lowercase(row.FirstName) || '').indexOf(angular.lowercase($scope.query.searchString) || '') !== -1 ||
                        (angular.lowercase(row.LastName) || '').indexOf(angular.lowercase($scope.query.searchString) || '') !== -1 ||
                        (angular.lowercase(row.Email) || '').indexOf(angular.lowercase($scope.query.searchString) || '') !== -1 ||
                        (angular.lowercase(row.Ph) || '').indexOf(angular.lowercase($scope.query.searchString) || '') !== -1 );
            };     
		}
]);