/**
 *
 */

var passivecandidatesmodule = angular.module('4dot5.missioncontrolmodule.passivecandidatesmodule', []);

passivecandidatesmodule.constant('PASSIVECANDIDATESCONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.passivecandidates',
        URL: '/passivecandidates',
        CONTROLLER: 'PassiveCandidatesController',
        TEMPLATEURL: 'app/partials/missioncontrol/passivecandidates/passivecandidates.html',
    },
    CONTROLLER: {

    }
});

passivecandidatesmodule.config(
    ['$stateProvider',
        'PASSIVECANDIDATESCONSTANTS',
        function($stateProvider, PASSIVECANDIDATESCONSTANTS) {
            $stateProvider.state(PASSIVECANDIDATESCONSTANTS.CONFIG.STATE, {
                url: PASSIVECANDIDATESCONSTANTS.CONFIG.URL,
                templateUrl: PASSIVECANDIDATESCONSTANTS.CONFIG.TEMPLATEURL,
                controller: PASSIVECANDIDATESCONSTANTS.CONFIG.CONTROLLER,
                params: {
                    candidatesType: null,
                    filterValues: null
                },
                data: {
                    requireLogin: true
                },

            });
        }
    ]);

passivecandidatesmodule.controller('PassiveCandidatesController', ['$scope',
    '$state',
    '$stateParams',
    '$rootScope',
    'StorageService',
    'genericService',
    '$http',
    'alertsAndNotificationsService',
    function($scope, $state, $stateParams, $rootScope, StorageService, genericService, $http, alertsAndNotificationsService) {
        var loggedInUserDetails = StorageService.getCookie('userDetails');
        console.log('PassiveCandidatesController: ' + $stateParams.candidatesType);

        $scope.companyType = $rootScope.userDetails.company.companyType;
        $scope.inputTextbox = false;
        $scope.toggleInputBox = function() {
            $scope.inputTextbox = !$scope.inputTextbox;
        };
        /* var $scope = this;*/

        $scope.searchTerm = '';
        $scope.candidatesType = $stateParams.candidatesType;
        $scope.candidatesTypeLabel = 'Active Candidates Job View';
        // console.log('Filter Values are: ' + $stateParams.filterValues)
        //filters
        $scope.filterValues = $stateParams.filterValues;
        $scope.filterType = '';
        $scope.requisitionRoleFilter = '';
        $scope.requistionCompanyFilter = '';
        $scope.teamMemberFilter = '';


        $scope.candidateNamesList = [];
        $scope.filteredCandidateList = [];
        $scope.filteredCandidateNamesList = [];
        $scope.compareArray = [];
        $scope.jobMatchDetails;
        $scope.companyQuestionsResults;
        $scope.clientQuestionsResults;
        $scope.jobQuestionsResults;
        $scope.jobclientMap;
        $scope.companyQnsComplete = false;

        $scope.totalClQnsCount = 0;

        $scope.countClientQns = function() {
            return $scope.totalClQnsCount++;
        }

        $scope.totalCount = 0;
        $scope.countJobQns = function() {
            return $scope.totalCount++;
        }

        $scope.init = function() {

            // active candadtes list
            $scope.qualifiedCandidatesData = [];
            $scope.techAssCandidatesData = [];
            $scope.valueAssCandidatesData = [];
            $scope.verificationCandidatesData = [];
            $scope.goldenStandardCandidatesData = [];
            $scope.likelyCandidatesData = [];
            $scope.unlikelyCandidatesData = [];

            //passive candiadates list
            $scope.softQualifiedCandidatesData = [];
            $scope.outReachCandidatesData = [];
            $scope.respondedCandidatesData = [];
            $scope.notInterestedCandidatesData = [];
            $scope.activatedCandidatesData = [];

            //filters
            /*$scope.filterValues = $scope.filterValues.split('|');*/
            // console.log('filterValues - ', $scope.filterValues);
            /* if($scope.filterValues.length){
            	 $scope.filterType = $scope.filterValues[0];
            	 if($scope.filterType === 'requisition'){
            		 $scope.requisitionRoleFilter = $scope.filterValues[1];
            		 $scope.requistionCompanyFilter = $scope.filterValues[2];
            	 }else if($scope.filterType === 'teamMember'){
            		 $scope.teamMemberFilter = $scope.filterValues[1];
            	 }else{
            		 //
            	 }
             }*/

            if ($scope.candidatesType === 'active') {
                $http.get("app-content/jsonobjects/candidates.json")
                    .success(function(data) {
                        angular.forEach(data, function(val, key) {
                            if ($scope.candidateToBeIncluded(val)) {
                                if (val.State === 'Qualified') {
                                    $scope.qualifiedCandidatesData.push(val);
                                } else if (val.State === 'Tech. Assessment') {
                                    $scope.techAssCandidatesData.push(val);
                                } else if (val.State === 'Value Assessment') {
                                    $scope.valueAssCandidatesData.push(val);
                                } else if (val.State === 'Verification') {
                                    $scope.verificationCandidatesData.push(val);
                                } else if (val.State === 'Golden Standard') {
                                    $scope.goldenStandardCandidatesData.push(val);
                                } else if (val.State === 'Likely') {
                                    $scope.likelyCandidatesData.push(val);
                                } else if (val.State === 'Unlikely') {
                                    $scope.unlikelyCandidatesData.push(val);
                                } else {
                                    // do nothing
                                }
                                $scope.candidateNamesList.push({ name: val.Name, ticked: true });
                            }
                        });
                    });
            } else {
                $scope.candidatesTypeLabel = 'Passive Candidates Job View';
                $http.get("app-content/jsonobjects/passiveCandidates.json")
                    .success(function(data) {
                        angular.forEach(data, function(val, key) {
                            if ($scope.candidateToBeIncluded(val)) {
                                if (val.State === 'Soft Qualified') {
                                    $scope.softQualifiedCandidatesData.push(val);
                                } else if (val.State === 'OutReach') {
                                    $scope.outReachCandidatesData.push(val);
                                } else if (val.State === 'Responded') {
                                    $scope.respondedCandidatesData.push(val);
                                } else if (val.State === 'Not Interested') {
                                    $scope.notInterestedCandidatesData.push(val);
                                } else if (val.State === 'Activated') {
                                    $scope.activatedCandidatesData.push(val);
                                } else {
                                    // do nothing
                                }
                            }
                        });
                    });
            }
        }

        $scope.candidateToBeIncluded = function(candidate) {
            if ($scope.filterType === 'requisition') {
                return (candidate.Role === $scope.requisitionRoleFilter && candidate.Company === $scope.requistionCompanyFilter);
            } else if ($scope.filterType === 'teamMember') {
                return (candidate.Recruiter === $scope.teamMemberFilter);
            } else {
                return true;
            }
        };

        $scope.copyAllCandidatesToFiltered = function() {
            $scope.filteredCandidateNamesList = $scope.candidateNamesList;
        };

        $scope.removeItemFromList = function(list, item) {
            //console.log('list - ', list);
            //console.log('item - ', item);
            for (i = 0; i < list.length; i++) {
                if (angular.equals(list[i], item)) {
                    // console.log('match found - ',list[i]);
                    // console.log('old list',list);
                    list.splice(i, 1);
                    // console.log('new list',list);
                }
            }
        };

        $scope.searchCandidates = function(candidate) {
            // console.log('called -',candidate);
            return (angular.lowercase(candidate.Name).indexOf(angular.lowercase($scope.searchTerm) || '') !== -1 ||
                angular.lowercase(candidate.Company).indexOf(angular.lowercase($scope.searchTerm) || '') !== -1 ||
                angular.lowercase(candidate.Role).indexOf(angular.lowercase($scope.searchTerm) || '') !== -1);
        };

        $scope.toggleView = function(columnType) {
            var column = angular.element(document.getElementById(columnType + '-candidates-column'));
            var toggleIcon = angular.element(document.getElementById(columnType + '-candidates-column-toogle-icon'));
            console.log('columnType - ',columnType);
            console.log('has class - ', column.hasClass('candidate-state-column'));
            if (column.hasClass('candidate-state-column')) {
                column.removeClass('candidate-state-column');
                column.addClass('candidate-state-column-collapse');
                toggleIcon.removeClass('fa-chevron-circle-left');
                toggleIcon.addClass('fa-chevron-circle-right');
                toggleIcon.attr('title', 'Expand Column');
            } else {
                column.removeClass('candidate-state-column-collapse');
                column.addClass('candidate-state-column');
                toggleIcon.removeClass('fa-chevron-circle-right');
                toggleIcon.addClass('fa-chevron-circle-left');
                toggleIcon.attr('title', 'Collapse Column');
            }
        };

        $scope.compareCandidates = function() {
            if ($scope.compareArray.length <= 1) {
                alert('Please choose at least two candidates to compare');
            } else {
                jobType = $scope.compareArray[0].Role;
                company = $scope.compareArray[0].Company;
                candidate1 = $scope.compareArray[0].Name;
                candidate2 = '';
                candidate3 = '';
                breadcrumb = 'Passive Candidates';
                if (angular.isDefined($scope.compareArray[1])) {
                    candidate2 = $scope.compareArray[1].Name;
                    if (angular.isDefined($scope.compareArray[2])) {
                        candidate3 = $scope.compareArray[2].Name;
                    }
                }
                $state.go('missioncontrol.candidatecompare', { jobType: jobType, companyName: company, candidate1: candidate1, candidate2: candidate2, candidate3: candidate3, breadcrumb: breadcrumb });
            }
        };

        // $scope.addOrRemoveFromCompare = function(candidate, event) {

        //     if ($scope.compareArray.length >= 1) {
        //         console.log('lets start validation');
        //         console.log('seee' + angular.toJson($scope.compareArray[0].Role) + ' ' + angular.toJson($scope.compareArray[0]));
        //         console.log('candidate.Company of selection' + angular.toJson(candidate));



        //         if (($scope.compareArray[0].Company) === (candidate.Company) & ($scope.compareArray[0].Role) === (candidate.Role)) {
        //             console.log('validating company');
        //         } else {
        //             console.log('child inner else');
        //             alert('Candiadtes of same Company and Role can only be compared');
        //             var removed = false;
        //             angular.forEach($scope.compareArray, function(val, key) {
        //                 if (val.Role === candidate.Role && val.Name === candidate.Name && val.Company === candidate.Company) {
        //                     _removeCandidate(key, image);
        //                     removed = true;
        //                 }
        //             });
        //             if (!removed) {
        //                 _addCandidate(candidate, image);
        //             }
        //         }
        //     } else {
        //         console.log('parent outer else');
        //     }

        //     var image = angular.element(event.target);
        //     if ($scope.compareArray.length === 0) {
        //         _addCandidate(candidate, image);
        //     } else {
        //         var removed = false;
        //         angular.forEach($scope.compareArray, function(val, key) {
        //             if (val.Role === candidate.Role && val.Name === candidate.Name && val.Company === candidate.Company) {
        //                 _removeCandidate(key, image);
        //                 removed = true;
        //             }
        //         });
        //         if (!removed) {
        //             _addCandidate(candidate, image);
        //         }
        //     }

        //     function _removeCandidate(key, image) {
        //         $scope.compareArray.splice(key, 1);
        //         image.toggleClass('profile-img-selected');
        //     }

        //     function _addCandidate(obj) {
        //         if ($scope.compareArray.length === 3) {
        //             alert('A maximum of 3 candidates can be compared at once. Please unselect another candidate to add ' + candidate.Name + ' to compare');
        //         } else {
        //             $scope.compareArray.push(obj);
        //             image.toggleClass('profile-img-selected');
        //         }
        //     }

        //     console.log('compare array', $scope.compareArray);
        // };

        $scope.addOrRemoveFromCompare = function(candidate, event) { //add or remove from compare
            console.log('entering here successfully' + angular.toJson(candidate.Role) + ' ' + angular.toJson(candidate.Company)); //need to be worked

            // console.log('candidate.Role of selection' + angular.toJson(candidate.Role));
            // console.log('candidate.Company of selection' + angular.toJson(candidate.Company));
            // console.log('seee' + angular.toJson($scope.compareArray[0]));

            if ($scope.compareArray.length >= 1) {
                // console.log('lets start validation');
                // console.log('seee' + angular.toJson($scope.compareArray[0].Role) + ' ' + angular.toJson($scope.compareArray[0]));
                // console.log('candidate.Company of selection' + angular.toJson(candidate));



                if (($scope.compareArray[0].Company) === (candidate.Company) & ($scope.compareArray[0].Role) === (candidate.Role)) {
                    // console.log('validating company');
                } else {
                    // console.log('child inner else');
                    alert('Candiadtes of same Company and Role can only be compared');
                    $scope.compareArray.splice(key, 1);
                    var removed = false;
                    angular.forEach($scope.compareArray, function(val, key) {
                        if (val.Role === candidate.Role && val.Name === candidate.Name && val.Company === candidate.Company) {
                            _removeCandidate(key, image);
                            removed = true;
                        }
                    });
                    if (!removed) {
                        _addCandidate(candidate, image);
                    }

                }
            } else {
                // console.log('parent outer else');
            }

            var image = angular.element(event.target);
            if ($scope.compareArray.length === 0) {
                _addCandidate(candidate, image);
            } else {
                var removed = false;
                angular.forEach($scope.compareArray, function(val, key) {
                    if (val.Role === candidate.Role && val.Name === candidate.Name && val.Company === candidate.Company) {
                        _removeCandidate(key, image);
                        removed = true;
                    }
                });
                if (!removed) {
                    _addCandidate(candidate, image);
                }
            }

            function _removeCandidate(key, image) { //remove candidate
                $scope.compareArray.splice(key, 1);
                image.toggleClass('profile-img-selected');
            }

            function _addCandidate(obj) { //add candidate
                if ($scope.compareArray.length === 3) {

                    alert('A maximum of 3 candidates can be compared at once. Please unselect another candidate to add ' + candidate.Name + ' ' + candidate.Company + candidate.Role + ' to compare');
                } else {
                    $scope.compareArray.push(obj);
                    image.toggleClass('profile-img-selected');
                    // console.log('compareArray' + angular.toJson($scope.compareArray));
                }
            }
            // console.log('compare array', $scope.compareArray);

        };


        $scope.searchBasedOnFilters = function(filter) {
            $scope.SearchFilters = filter;
            // console.log('SearchBasedOnFilters function : ' + angular.toJson(filter));
            //  $("#search-panel").hide();
            $(document).off("click", "#filterSearch").on("click", "#filterSearch",
                function() {
                    $("#search-panel").animate({
                        height: 'toggle'
                    });
                });
        }

        $scope.showrecruiterquestions = function() {
            var candidateId  = "595c90bae4b0120fc758eed2";
            genericService.getObjects("api/recruiterscreeningquestions/getrecruiterscreeningquestions/"+ candidateId+"?questionType=R").then(function (data) {
                $scope.RecruiterScreeningQuestionsDto = angular.copy(data);
                $scope.jobMatchDetails = $scope.RecruiterScreeningQuestionsDto.jobMatchDetails;
                $scope.companyName = $scope.RecruiterScreeningQuestionsDto.companyName;
                $scope.clientDetails =    $scope.RecruiterScreeningQuestionsDto.clientDetails;
                $scope.jobDetails =    $scope.RecruiterScreeningQuestionsDto.jobDetails;
                $scope.jobclientMap =   $scope.RecruiterScreeningQuestionsDto.clientJobMatchDetails;
                $scope.clientQuestionsResults =   $scope.RecruiterScreeningQuestionsDto.clientQuestionsResults;
                $scope.companyQuestionsResults =   $scope.RecruiterScreeningQuestionsDto.companyQuestionsResults;
                $scope.jobQuestionsResults =   $scope.RecruiterScreeningQuestionsDto.jobQuestionsResults;
                $scope.candidate =   $scope.RecruiterScreeningQuestionsDto.candidate;
                $scope.sortedClientList =  $scope.RecruiterScreeningQuestionsDto.sortedClientList;
                $scope.clientListJobMatchDetails = $scope.RecruiterScreeningQuestionsDto.clientListJobMatchDetails;
                $scope.totalClQnsCount = 0;
                $scope.totalCount = 0;
                $scope.checkifCompanyCompleted();

                //$scope.jobDetails["595c90bae4b0120fc758eed4"]["interested"] = false;
                //$scope.jobDetails["596d8f4de4b042814c07c87d"]["interested"] = false;

            }, function (error) {
                //Have to do something
            });
        }

        $scope.checkifCompanyCompleted = function() {
             if($scope.RecruiterScreeningQuestionsDto === undefined || $scope.RecruiterScreeningQuestionsDto == '') {
                $scope.companyQnsComplete = true;
                 return true;
             }

            var companykey;
             for (var key in $scope.RecruiterScreeningQuestionsDto.companyQuestionsResults) {
                 companykey = key
             }
             var companyQuestion =  $scope.RecruiterScreeningQuestionsDto.companyQuestionsResults[companykey];

            for (var i=0;i<companyQuestion.length;i++) {
                     var recruiterComment = companyQuestion[i].recruiterComment;
                       var overallQuestion = companyQuestion[i].question;
                       var sliderId = companyQuestion[i].rating;
                        if (overallQuestion != 'Overall Comment') {
                            if(isEmpty(recruiterComment) && sliderId == "0"){
                                $scope.companyQnsComplete = false;
                                return false;
                        }
                     }
            }
            $scope.companyQnsComplete = true;
            return true;
        }


        $scope.checkifAllClientCompleted = function() {
                for (var key in $scope.RecruiterScreeningQuestionsDto.clientQuestionsResults) {
                    var clientQuestion =  $scope.RecruiterScreeningQuestionsDto.clientQuestionsResults[key];
                    for (var i=0;i<clientQuestion.length;i++) {
                     var recruiterComment = clientQuestion[i].recruiterComment
                     var sliderId = clientQuestion[i].rating
                     if(isEmpty(recruiterComment)&& sliderId == "0"){
                        return false;
                    }
                  }
                }
              return true;

        }


        $scope.checkifClientCompleted = function(key) {
                    var clientQuestion =  $scope.RecruiterScreeningQuestionsDto.clientQuestionsResults[key];
                    for (var i=0;i<clientQuestion.length;i++) {
                     var recruiterComment = clientQuestion[i].recruiterComment
                     var sliderId = clientQuestion[i].rating
                     if(isEmpty(recruiterComment) && sliderId == "0"){
                        return false;
                    }
               }
              return true;

        }


         $scope.checkifJobCompleted = function(key) {
                    var jobQuestion =  $scope.RecruiterScreeningQuestionsDto.jobQuestionsResults[key];
                     for (var i=0;i<jobQuestion.length;i++) {
                     var recruiterComment = jobQuestion[i].recruiterComment;
                     var sliderId = jobQuestion[i].rating;
                     if(isEmpty(recruiterComment) && sliderId == "0"){
                        return false;
                    }
               }
              return true;

        }


         $scope.checkifAllJobCompleted = function() {
                for (var key in $scope.RecruiterScreeningQuestionsDto.jobQuestionsResults) {
                     var jobQuestion =  $scope.RecruiterScreeningQuestionsDto.jobQuestionsResults[key];
                     for (var i=0;i<jobQuestion.length;i++) {
                     var recruiterComment = jobQuestion[i].recruiterComment
                     var sliderId = jobQuestion[i].rating
                     if(isEmpty(recruiterComment) && sliderId == "0"){
                        return false;
                    }
               }
            }
              return true;

        }



         $scope.checkifCompanyOverallCompleted = function() {
             if($scope.RecruiterScreeningQuestionsDto === undefined || $scope.RecruiterScreeningQuestionsDto == '')
                 return true;

            var companykey;
             for (var key in $scope.RecruiterScreeningQuestionsDto.companyQuestionsResults) {
                 companykey = key
             }

             var companyQuestion =  $scope.RecruiterScreeningQuestionsDto.companyQuestionsResults[companykey];

              for (var i=0;i<companyQuestion.length;i++) {
                     var recruiterComment = companyQuestion[i].recruiterComment;
                       var overallQuestion = companyQuestion[i].question;
                       var sliderId = companyQuestion[i].rating;
                       if (overallQuestion == 'Overall Comment') {
                         if(isEmpty(recruiterComment)  && sliderId == "0"){
                            return false;
                        }
                     }
               }
              return true;
        }


        function isEmpty(str) {
           return (!str || 0 === str.length);
        }

        $scope.initializeSliders = function()
        {
            initCoSliders();
            initClSliders();
            initJobSliders();

            $("div[id='overall-qn-slider']").each(function(i,obj) {
                var labelId = "#overall-qn-rating";
                $(obj).noUiSlider({
                    range: [0,10], start: [0], step: 1, handles: 1, connect: 'lower',
                    slide: function(){
                        var val = $(this).val();
                        $(labelId).text(Math.round(val));
                    },
                    set: function() {
                        var val = $(this).val();
                        $(labelId).text(Math.round(val));
                    }
                });
            });

            var companyQuestionIndex;
            for (var key in $scope.RecruiterScreeningQuestionsDto.companyQuestionsResults) {
                var l = $scope.RecruiterScreeningQuestionsDto.companyQuestionsResults[key].length;
                var overallRating = $scope.RecruiterScreeningQuestionsDto.companyQuestionsResults[key][l-1]["rating"];
                $("#overall-qn-slider").val(overallRating,true);
            }
        }

        function initCoSliders()
        {
            $("div[id='co-qn-slider']").each(function(i,obj) {
                var labelId = "#co-qn-rating" + ++i;
                $(obj).noUiSlider({
                    range: [0,10], start: [0], step: 1, handles: 1, connect: 'lower',
                    slide: function(){
                        var val = $(this).val();
                        $(labelId).text(Math.round(val));
                    },
                    set: function() {
                        var val = $(this).val();
                        $(labelId).text(Math.round(val));
                    }
                });
                var qnRating = $(labelId).text();
                $(obj).val(qnRating,true);
            });
        }

        function initClSliders()
        {
            $("div[id='cl-qn-slider']").each(function(i,obj) {
                var labelId = "#cl-qn-rating" + ++i;
                $(obj).noUiSlider({
                    range: [0,10], start: [0], step: 1, handles: 1, connect: 'lower',
                    slide: function(){
                        var val = $(this).val();
                        $(labelId).text(Math.round(val));
                    },
                    set: function() {
                        var val = $(this).val();
                        $(labelId).text(Math.round(val));
                    }
                });
                var qnRating = $(labelId).text();
                $(obj).val(qnRating,true);
            });
        }

        function initJobSliders()
        {
            $("div[id='job-qn-slider']").each(function(i,obj) {
                var labelId = "#job-qn-rating" + ++i;
                $(obj).noUiSlider({
                    range: [0,10], start: [0], step: 1, handles: 1, connect: 'lower',
                    slide: function(){
                        var val = $(this).val();
                        $(labelId).text(Math.round(val));
                    },
                    set: function() {
                        var val = $(this).val();
                        $(labelId).text(Math.round(val));
                    }
                });
                var qnRating = $(labelId).text();
                $(obj).val(qnRating,true);
            });
        }

        $scope.getFile = function(jobMatchId) {
            alert($scope.jobDetails[jobMatchId].jobRawFileLocation);
            var fileName="58b792dfe4b0c7dcd9b73879-1497205993068-Java_Full Stack Web Developer";
            var fileLocation="58b72f13e4b0c7dcd9b73874";
            var subDir = "";
           genericService.getObjects("api/recruiterscreeningquestions/getjobfile/" + fileLocation + "/" +  fileName + "/" + subDir).then(function (data) {
           }, function (error) {

                       });
           }



        $scope.clearObjects = function() {
            $scope.RecruiterScreeningQuestionsDto = '';
            $scope.jobMatchDetails = '';
            $scope.companyName = '';
            $scope.clientDetails = '';
            $scope.jobDetails = '';
            $scope.jobclientMap = '';
            $scope.clientQuestionsResults = '';
            $scope.companyQuestionsResults = '';
            $scope.jobQuestionsResults = '';
            $scope.candidate = '';
            $scope.sortedClientList = '';
            $scope.clientListJobMatchDetails = '';
            $scope.totalClQnsCount = 0;
            $scope.totalCount = 0;
        }

        $scope.saveResults = function() {
            var listofCompanyQuestions;
            var listofClientQuestions;
            var listofJobQuestions;
            var companyQuestionIndex;

            for (var key in $scope.RecruiterScreeningQuestionsDto.companyQuestionsResults) {
                $("span[id='co-qn']").each(function(i,obj) {
                    var suffix = i + 1;
                    var sliderId = "#co-qn-rating" + suffix;
                    var textAreaId = "#co-qn-comments" + suffix;
                    $scope.RecruiterScreeningQuestionsDto.companyQuestionsResults[key][i]["rating"] = $(sliderId).text();
                    $scope.RecruiterScreeningQuestionsDto.companyQuestionsResults[key][i]["recruiterComment"] = $(textAreaId).val();
                    $scope.RecruiterScreeningQuestionsDto.companyQuestionsResults[key][i]["recruiterId"] =  loggedInUserDetails.id;
                    $scope.RecruiterScreeningQuestionsDto.companyQuestionsResults[key][i]["questionareDateTime"] =  new Date();
                    companyQuestionIndex = i;
                });
                companyQuestionIndex = companyQuestionIndex +  1;
                var overallsliderId = "#overall-qn-rating";
                var overalltextAreaId = "#overall-qn-comments";
                $scope.RecruiterScreeningQuestionsDto.companyQuestionsResults[key][companyQuestionIndex]["question"] = "Overall Comment"
                $scope.RecruiterScreeningQuestionsDto.companyQuestionsResults[key][companyQuestionIndex]["rating"] = $(overallsliderId).text();
                $scope.RecruiterScreeningQuestionsDto.companyQuestionsResults[key][companyQuestionIndex]["recruiterComment"] = $(overalltextAreaId).val();
                $scope.RecruiterScreeningQuestionsDto.companyQuestionsResults[key][companyQuestionIndex]["recruiterId"] =  loggedInUserDetails.id;
                $scope.RecruiterScreeningQuestionsDto.companyQuestionsResults[key][companyQuestionIndex]["questionareDateTime"] =  new Date();
                listofCompanyQuestions = $scope.RecruiterScreeningQuestionsDto.companyQuestionsResults[key];
            }



            for (var key in $scope.RecruiterScreeningQuestionsDto.clientQuestionsResults) {
                var spanId = "cl-qn" + key;
                $("span[id=" + spanId + "]").each(function(i,obj) {
                    var clQnNoFldId = "cl-qn-no" + key;
                    var clQnNo = $("input[id=" + clQnNoFldId + "]")[i].value;
                    var sliderId = "#cl-qn-rating" + clQnNo;
                    var textAreaId = "#cl-qn-comments" + clQnNo;
                    $scope.RecruiterScreeningQuestionsDto.clientQuestionsResults[key][i]["rating"] = $(sliderId).text();
                    $scope.RecruiterScreeningQuestionsDto.clientQuestionsResults[key][i]["recruiterComment"] = $(textAreaId).val();
                    $scope.RecruiterScreeningQuestionsDto.clientQuestionsResults[key][i]["recruiterId"] =  loggedInUserDetails.id;
                    $scope.RecruiterScreeningQuestionsDto.clientQuestionsResults[key][i]["questionareDateTime"] =  new Date();
                });

                if(listofClientQuestions != null)
                {
                    listofClientQuestions = listofClientQuestions.concat($scope.RecruiterScreeningQuestionsDto.clientQuestionsResults[key]);
                }
                else
                {
                    listofClientQuestions = $scope.RecruiterScreeningQuestionsDto.clientQuestionsResults[key];
                }
            }

            for (var key in $scope.RecruiterScreeningQuestionsDto.jobQuestionsResults) {
                var spanId = "job-qn" + key;
                $("span[id=" + spanId + "]").each(function(i,obj) {
                    var jobQnNoFldId = "job-qn-no" + key;
                    var jobQnNo = $("input[id=" + jobQnNoFldId + "]")[i].value;
                    var sliderId = "#job-qn-rating" + jobQnNo;
                    var textAreaId = "#job-qn-comments" + jobQnNo;
                    $scope.RecruiterScreeningQuestionsDto.jobQuestionsResults[key][i]["rating"] = $(sliderId).text();
                    $scope.RecruiterScreeningQuestionsDto.jobQuestionsResults[key][i]["recruiterComment"] = $(textAreaId).val();
                    $scope.RecruiterScreeningQuestionsDto.jobQuestionsResults[key][i]["recruiterId"] =  loggedInUserDetails.id;
                    $scope.RecruiterScreeningQuestionsDto.jobQuestionsResults[key][i]["questionareDateTime"] =  new Date();
                });

                if(listofJobQuestions != null)
                {
                    listofJobQuestions = listofJobQuestions.concat($scope.RecruiterScreeningQuestionsDto.jobQuestionsResults[key]);
                }
                else
                {
                    listofJobQuestions = $scope.RecruiterScreeningQuestionsDto.jobQuestionsResults[key];
                }
            }

            var listOfQuestionsResults = [];
            var listOfCompanyquest = listofCompanyQuestions.concat(listofClientQuestions);
            var listOfQuestionsResults = listOfCompanyquest.concat(listofJobQuestions);

              genericService.addObject("api/recruiterscreeningquestions/saverecruiterscreeningquestions/",  angular.toJson(listOfQuestionsResults)).then(function (data) {
                  alertsAndNotificationsService.showBannerMessage("Recruiter Screening Questions Saved Successfully", 'success');
                }, function (error) {
                  alertsAndNotificationsService.showBannerMessage("There was an error while saving Recruiter Screening Questions",'danger');
            });

            if ($scope.checkifCompanyCompleted() && $scope.checkifCompanyOverallCompleted() && $scope.checkifAllJobCompleted() && $scope.checkifAllClientCompleted) {

                  var listOfjobdetails =[];
                  var statusOfSave = 'completed'

                    for (var key in $scope.jobDetails) {
                        listOfjobdetails.push($scope.jobDetails[key]);
                    }

                 genericService.addObject("api/recruiterscreeningquestions/savejobmatchstate/"+statusOfSave,  angular.toJson(listOfjobdetails)).then(function (data) {

                }, function (error) {

            });
            } else {
               var listOfjobdetails =[];
                var statusOfSave = 'notcompleted'
                 for (var key in $scope.jobDetails) {
                      listOfjobdetails.push($scope.jobDetails[key]);
                    }
                 genericService.addObject("api/recruiterscreeningquestions/savejobmatchstate/"+statusOfSave,  angular.toJson(listOfjobdetails)).then(function (data) {

                }, function (error) {

            });

            }
        }

        $scope.filterByInterested = function() {
            for (var clientId in $scope.clientListJobMatchDetails) {
                var showClient = false;
                var jobArray = $scope.clientListJobMatchDetails[clientId];
                for (var i = 0; i < jobArray.length; i++) {
                    var jobId = jobArray[i];
                    var x = document.getElementById(jobId);
                    if ($scope.jobDetails[jobId].interested) {
                        x.style.display = "block";
                        showClient = true;
                    } else {
                        x.style.display = "none";
                    }
                }

                var y = document.getElementById(clientId);
                if (showClient) {
                    y.style.display = "block";
                } else {
                    y.style.display = "none";
                }
            }
        }

        $scope.filterFunction = function(element) {
              return element.question.match(/^Overall/) ? false : true;
        };

         $scope.filterFunctionForOverall = function(element) {
              return element.question.match(/^Overall/) ? true : false;
            };


         $scope.getCompanyClass = function( ){
          if ($scope.companyQnsComplete)
               return "panel-collapse collapse";
          else
              return "panel-collapse collapse in";
        }

         $scope.getClientClass = function(keyval){
          if ($scope.checkifClientCompleted(keyval))
               return "panel-collapse collapse";
          else
              return "panel-collapse collapse in";
        }


         $scope.getJobClass = function(keyval){
          if ($scope.checkifJobCompleted(keyval))
               return "panel-collapse collapse";
          else
              return "panel-collapse collapse in";
        }


          $scope.getOverallClass = function(){

          if ($scope.checkifCompanyOverallCompleted())
               return "panel-collapse collapse";
            else
              return "panel-collapse collapse in";

        }

        //Implementaion of filters
        $(document).ready(function() {
            $("#search-panel").hide();
            $(document).off("click", "#open-filter").on("click", "#open-filter",
                function() {
                    $("#search-panel").animate({
                        height: 'toggle'
                    });
                });
            $('response1').click(function() {
               var element = $(this)
               setTimeout(
                function() {
               var number = element.attr('name').split("_")[1];
                        alert(number);
                        var divId = 'quest' + number;
                        var y = document.getElementById(divId);
                        y.style.display = "none";

                        var nextdivId = 'quest' + (parseInt(number)+1);
                                alert(nextdivId);
                        var x = document.getElementById(nextdivId);
                        x.style.display = "block";

                },
                1000);


	       });
         $(document).off("click", "#response1").on("click", "#response1",
            function() {
             var element = $(this);
               var number = element.attr('name').split("_")[1];
             if (number != $scope.bigfivequestions.length) {
              setTimeout(function(){

                  var divId = 'quest' + number;
                  var y = document.getElementById(divId);
                  y.style.display = "none";
                  var nextdivId = 'quest' + (parseInt(number)+1);
                  var x = document.getElementById(nextdivId);
                  x.style.display = "block";
            }, 800);
           }
            });
    });
        $scope.init();

    }
]);