/**
 * All the Functionalities Related to Reports
 * Module is included here
 */

var candidateperformancemodule = angular.module('4dot5.missioncontrolmodule.candidateperformancemodule',
    []);

candidateperformancemodule.constant('CANDIDATEPERFORMANCECONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.candidateperformancemodule',
        URL: '/candidateperformance',
        CONTROLLER: 'CandidatePerformanceController',
        TEMPLATEURL: 'app/partials/missioncontrol/reports/candidateperformance/candidateperformance.html',
    }
});

candidateperformancemodule.config(
    ['$stateProvider',
        'CANDIDATEPERFORMANCECONSTANTS',
        function ($stateProvider, CANDIDATEPERFORMANCECONSTANTS) {
            $stateProvider.state(CANDIDATEPERFORMANCECONSTANTS.CONFIG.STATE, {
                url: CANDIDATEPERFORMANCECONSTANTS.CONFIG.URL,
                templateUrl: CANDIDATEPERFORMANCECONSTANTS.CONFIG.TEMPLATEURL,
                controller: CANDIDATEPERFORMANCECONSTANTS.CONFIG.CONTROLLER,
                data: {
                    requireLogin: true
                },
                params: {
                    reportType: null
                }
            });
        }
    ]);


candidateperformancemodule.controller('CandidatePerformanceController',
    ['$scope',
        '$state',
        '$window',
        '$location',
        '$stateParams',
        '$rootScope',
        '$timeout',
        '$http',
        'StorageService',
        'genericService',
        'CANDIDATEPERFORMANCECONSTANTS',
        function ($scope, $state, $window, $location, $stateParams, $rootScope, $timeout, $http, StorageService, genericService, CANDIDATEPERFORMANCECONSTANTS) {

            console.log('CandidatePerformanceController');
            $scope.reportTypeLabel = 'Candidates Performance';
            $scope.CandidatePerformance = {};
            $scope.types = ['Me', 'My team'];
            $scope.CandidatePerformance.type = 'Me';


            //CHARTS
            function getMonday(d) {
                d = new Date(d);
                var day = d.getDay(),
                    diff = d.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday
                //d.setDate(diff);
                return new Date(d.setDate(diff));
            }

            function getLastMondayTimeBySubtractingWeeksFromNow(weeks) {
                var d = new Date();
                d.setHours(0);
                d.setMinutes(0);
                return getMonday(new Date(d.getTime() - (weeks * 7 * 24 * 60 * 60 * 1000)).getTime()).getTime();
            }


            function getQuarter(d) {
                d = d || new Date();
                return m = Math.floor(d.getMonth() / 3);
                //return m > 4? m - 4 : m;
            }

            function getLastMonthOfQuarter(quarter) {
                return (quarter * 3) + 2;
            }

            function getTimeBySubtractingMonthsFromNow(months) {
                var d = new Date();
                return new Date(d.setMonth(d.getMonth() - months)).getTime();
            }

            function getTimeBySubtractingQuartersFromNow(quarters) {
                var d = new Date();
                d.setMonth(d.getMonth() - (quarters * 3));
                return new Date(d.setMonth(getLastMonthOfQuarter(getQuarter(d)))).getTime();
            }

            $scope.requisitionRole = 'All';
            $scope.bu = function (selectedbu) {
                console.log("selected bu " + angular.toJson(selectedbu));
                $scope.requisitionfilters[0].bulist = selectedbu;
            }
            $scope.client = function (selectedclient) {
                console.log("selected client " + angular.toJson(selectedclient));
                $scope.requisitionfilters[0].clientList = selectedclient;
            }
            $scope.roleSelected = function (roleSelected) {
                console.log("roleSelected  " + roleSelected);
                $scope.requisitionfilters[0].Rolelist = roleSelected;
            }
            $scope.recruiterselected = function (selectedrecruitersList) {
                console.log("rec selected!! " + selectedrecruitersList);
                console.log("rec ");
                $scope.requisitionfilters[0].recruiterList = selectedrecruitersList;

                console.log("$scope.requisitionfilters[0].recruiterList " + angular.toJson($scope.requisitionfilters[0].recruiterList));
            }
            $scope.search = function () {
                console.log("$scope.requisitionfilters " + angular.toJson($scope.requisitionfilters));
                $scope.getGraphData($scope.requisitionfilters);
            }
            $scope.plotFlotGraph = function (containerId, graph1Data, graph1Label, graph2Data, graph2Label, graph3Data, graph3Label, timeType, id) {

                console.log("coming")
                if ($('#' + containerId).length) {
                    if (id === 2) {
                        var passiveCandidatesData = graph1Data;
                        var passiveCandidates = [];

                        var timeToConvertCandidatesData = graph3Data;
                        var timeToConvertCandidates = [];

                        var convertedCandidatesData = graph2Data;
                        var convertedCandidates = [];

                        var ticks = [];

                        for (var i = 0; i < passiveCandidatesData.length; i++) {
                            var arr = [];
                            var tempTime = '';
                            if (timeType === 'weekly') {
                                tempTime = getLastMondayTimeBySubtractingWeeksFromNow(passiveCandidatesData.length - i - 1);
                            } else if (timeType === 'monthly') {
                                tempTime = getTimeBySubtractingMonthsFromNow(passiveCandidatesData.length - i - 1);
                            } else if (timeType === 'quarterly') {
                                tempTime = getTimeBySubtractingQuartersFromNow(passiveCandidatesData.length - i - 1);
                            } else {
                                //
                            }

                            arr.push(tempTime);
                            arr.push(passiveCandidatesData[i]);
                            passiveCandidates.push(arr);

                            arr = [];
                            arr.push(tempTime);
                            arr.push(timeToConvertCandidatesData[i]);
                            timeToConvertCandidates.push(arr);

                            arr = [];
                            arr.push(tempTime);
                            arr.push(convertedCandidatesData[i]);
                            convertedCandidates.push(arr);

                            ticks.push(tempTime);
                        }

                        var xAxisMin = '';
                        var xAxisMax = '';
                        var xAsisBarWidth = '';
                        var xAxisLabel = '';
                        var xAxisTimeFormat = '';
                        if (timeType === 'weekly') {
                            xAxisMin = (new Date()).getTime() - ((passiveCandidatesData.length) * 7 * 24 * 60 * 60 * 1000);
                            xAxisMax = (new Date()).getTime() + (7 * 24 * 60 * 60 * 1000);
                            xAxisBarWidth = 24 * 60 * 60 * 800;
                            xAxisLabel = 'Past six weeks';
                            xAxisTimeFormat = "%m/%d";
                        } else if (timeType === 'monthly') {
                            xAxisMin = (new Date()).getTime() - ((passiveCandidatesData.length) * 31 * 24 * 60 * 60 * 1000);
                            xAxisMax = (new Date()).getTime() + (31 * 24 * 60 * 60 * 1000);
                            xAxisBarWidth = 24 * 60 * 60 * 3000;
                            xAxisLabel = 'Past six months';
                            xAxisTimeFormat = "%b '%y";
                        } else if (timeType === 'quarterly') {
                            xAxisMin = (new Date()).getTime() - (3 * (passiveCandidatesData.length) * 31 * 24 * 60 * 60 * 1000);
                            xAxisMax = (new Date()).getTime() + (6 * 31 * 24 * 60 * 60 * 1000);
                            xAxisBarWidth = 24 * 60 * 60 * 8000;
                            xAxisLabel = 'Past six quarters';
                            xAxisTimeFormat = "%b '%y";
                        } else {
                            //
                        }

                        var series = new Array();

                        series.push({
                            data: passiveCandidates,
                            color: '#e84e40',
                            lines: {
                                show: true,
                                lineWidth: 3,
                            },
                            points: {
                                fillColor: '#ffffff',
                                pointWidth: 1,
                                show: true
                            },
                            label: graph1Label,
                            yaxis: 2
                        });


                        series.push({
                            data: convertedCandidates,
                            color: '#8bc34a',
                            lines: {
                                show: true,
                                lineWidth: 3,
                            },
                            points: {
                                fillColor: '#ffffff',
                                pointWidth: 1,
                                show: true
                            },
                            label: graph2Label,
                            yaxis: 2
                        });
                        series.push({
                            data: timeToConvertCandidates,
                            color: '#03a9f4',
                            bars: {
                                show: true,
                                barWidth: xAxisBarWidth,
                                lineWidth: 1,
                                order: 1,
                                fill: 1,
                                align: 'center'
                            },
                            label: graph3Label,
                            yaxis: 1
                        });


                    } else {
                        var activeCandidatesData = graph1Data;
                        var activeCandidates = [];

                        var timeToPlaceCandidatesData = graph3Data;
                        var timeToPlaceCandidates = [];

                        var placedCandidatesData = graph2Data;
                        var placedCandidates = [];

                        var ticks = [];

                        for (var i = 0; i < activeCandidatesData.length; i++) {
                            var arr = [];
                            var tempTime = '';
                            if (timeType === 'weekly') {
                                tempTime = getLastMondayTimeBySubtractingWeeksFromNow(activeCandidatesData.length - i - 1);
                            } else if (timeType === 'monthly') {
                                tempTime = getTimeBySubtractingMonthsFromNow(activeCandidatesData.length - i - 1);
                            } else if (timeType === 'quarterly') {
                                tempTime = getTimeBySubtractingQuartersFromNow(activeCandidatesData.length - i - 1);
                            } else {
                                //
                            }

                            arr.push(tempTime);
                            arr.push(activeCandidatesData[i]);
                            activeCandidates.push(arr);

                            arr = [];
                            arr.push(tempTime);
                            arr.push(timeToPlaceCandidatesData[i]);
                            timeToPlaceCandidates.push(arr);

                            arr = [];
                            arr.push(tempTime);
                            arr.push(placedCandidatesData[i]);
                            placedCandidates.push(arr);

                            ticks.push(tempTime);
                        }


                        var xAxisMin = '';
                        var xAxisMax = '';
                        var xAsisBarWidth = '';
                        var xAxisLabel = '';
                        var xAxisTimeFormat = '';

                        if (timeType === 'weekly') {
                            xAxisMin = (new Date()).getTime() - ((activeCandidatesData.length) * 7 * 24 * 60 * 60 * 1000);
                            xAxisMax = (new Date()).getTime() + (7 * 24 * 60 * 60 * 1000);
                            xAxisBarWidth = 24 * 60 * 60 * 800;
                            xAxisLabel = 'Past six weeks';
                            xAxisTimeFormat = "%m/%d";
                        } else if (timeType === 'monthly') {
                            xAxisMin = (new Date()).getTime() - ((activeCandidatesData.length) * 31 * 24 * 60 * 60 * 1000);
                            xAxisMax = (new Date()).getTime() + (31 * 24 * 60 * 60 * 1000);
                            xAxisBarWidth = 24 * 60 * 60 * 3000;
                            xAxisLabel = 'Past six months';
                            xAxisTimeFormat = "%b '%y";
                        } else if (timeType === 'quarterly') {
                            xAxisMin = (new Date()).getTime() - (3 * (activeCandidatesData.length) * 31 * 24 * 60 * 60 * 1000);
                            xAxisMax = (new Date()).getTime() + (6 * 31 * 24 * 60 * 60 * 1000);
                            xAxisBarWidth = 24 * 60 * 60 * 8000;
                            xAxisLabel = 'Past six quarters';
                            xAxisTimeFormat = "%b '%y";
                        } else {
                            //
                        }

                        var series = new Array();

                        series.push({
                            data: activeCandidates,
                            color: '#e84e40',
                            lines: {
                                show: true,
                                lineWidth: 3,
                            },
                            points: {
                                fillColor: '#ffffff',
                                pointWidth: 1,
                                show: true
                            },
                            label: graph1Label,
                            yaxis: 2
                        });

                        series.push({
                            data: placedCandidates,
                            color: '#8bc34a',
                            lines: {
                                show: true,
                                lineWidth: 3,
                            },
                            points: {
                                fillColor: '#ffffff',
                                pointWidth: 1,
                                show: true
                            },
                            label: graph2Label,
                            yaxis: 2
                        });

                        series.push({
                            data: timeToPlaceCandidates,
                            color: '#03a9f4',
                            bars: {
                                show: true,
                                barWidth: xAxisBarWidth,
                                lineWidth: 1,
                                order: 2,
                                fill: 1,
                                align: 'center'
                            },
                            label: graph3Label,
                            yaxis: 1
                        });

                    }


                    //plotiing graph
                    $.plot("#" + containerId, series, {
                        colors: ['#03a9f4', '#f1c40f', '#2ecc71', '#3498db', '#9b59b6', '#95a5a6'],
                        grid: {
                            tickColor: "#f2f2f2",
                            borderWidth: 0,
                            hoverable: true,
                            clickable: true
                        },
                        legend: {
                            noColumns: 1,
                            labelBoxBorderColor: "#000000",
                            position: "ne"
                        },
                        shadowSize: 0,
                        xaxis: {
                            mode: "time",
                            ticks: ticks,
                            timezone: "browser",
                            //tickSize: [1, "day"],
                            timeformat: xAxisTimeFormat,
                            //tickLength: 0,
                            axisLabel: xAxisLabel,
                            min: xAxisMin,
                            max: xAxisMax,
                            axisLabelUseCanvas: true,
                            axisLabelFontSizePixels: 12,
                            axisLabelFontFamily: 'Open Sans, sans-serif',
                            axisLabelPadding: 10
                        },
                        yaxes: [
                            {
                                // tickFormatter: function (val, axis) {
                                //     return val;
                                // },	
                                position: "left",
                                axisLabel: "Time (in days)",
                                axisLabelUseCanvas: true,
                                axisLabelFontSizePixels: 12,
                                axisLabelFontFamily: "Verdana, Arial, Helvetica, Tahoma, sans-serif",
                                axisLabelPadding: 5
                            },
                            {
                                position: "right",
                                axisLabel: "Number of candidates",
                                axisLabelUseCanvas: true,
                                axisLabelFontSizePixels: 12,
                                axisLabelFontFamily: "Verdana, Arial, Helvetica, Tahoma, sans-serif",
                                axisLabelPadding: 5
                            }
                        ]
                    });

                    var previousPoint = null;
                    $("#" + containerId).bind("plothover", function (event, pos, item) {
                        if (item) {
                            if (previousPoint != item.dataIndex) {

                                previousPoint = item.dataIndex;

                                $("#flot-tooltip").remove();
                                var x = item.datapoint[0],
                                    y = item.datapoint[1];

                                showTooltip(item.pageX, item.pageY, item.series.label, y);
                            }
                        }
                        else {
                            $("#flot-tooltip").remove();
                            previousPoint = [0, 0, 0];
                        }
                    });

                    function showTooltip(x, y, label, data) {
                        $('<div id="flot-tooltip">' + '<b>' + label + ': </b><i>' + data + '</i>' + '</div>').css({
                            top: y + 5,
                            left: x + 20
                        }).appendTo("body").fadeIn(200);
                    }
                };
            }

            $scope.drawGraph = function () {
                console.log("cominnnn 1");
                for (var i = 0; i < $scope.candidateList.length; i++) {
                    console.log("candidateList id " + $scope.candidateList[i].id);
                    console.log("cominnnn 2");
                    console.log(angular.equals($scope.candidateList[i].id, 2))
                    if (angular.equals($scope.candidateList[i].id, 2)) {
                        console.log("cominnnn 21");
                        if ($scope.candidateList[i].monthlyshow) {
                            console.log("cominnnn 211");
                            $scope.plotFlotGraph($scope.candidateList[i].id + '-graph-bar', $scope.candidateList[i].passive.monthly, 'Passive Canditates', $scope.candidateList[i].converted.monthly, 'Converted Canditates', $scope.candidateList[i].timeToConvert.monthly, 'Time to Convert', 'monthly', $scope.candidateList[i].id);
                        }
                        if ($scope.candidateList[i].quarterlyshow) {
                            console.log("cominnnn 212");
                            $scope.plotFlotGraph($scope.candidateList[i].id + '-graph-bar', $scope.candidateList[i].passive.quarterly, 'Passive Canditates', $scope.candidateList[i].converted.quarterly, 'Converted Canditates', $scope.candidateList[i].timeToConvert.quarterly, 'Time to Convert', 'quarterly', $scope.candidateList[i].id);
                        }
                        if ($scope.candidateList[i].weeklyshow) {
                            console.log("cominnnn 212");
                            $scope.plotFlotGraph($scope.candidateList[i].id + '-graph-bar', $scope.candidateList[i].passive.weekly, 'Passive Canditates', $scope.candidateList[i].converted.weekly, 'Converted Canditates', $scope.candidateList[i].timeToConvert.weekly, 'Time to Convert', 'weekly', $scope.candidateList[i].id);
                        }
                    } else if (angular.equals($scope.candidateList[i].id, 1)) {
                        console.log("cominnnn 22");
                        if ($scope.candidateList[i].monthlyshow) {
                            console.log("cominnnn 221");
                            $scope.plotFlotGraph($scope.candidateList[i].id + '-graph-bar', $scope.candidateList[i].active.monthly, 'Active Canditates', $scope.candidateList[i].placed.monthly, 'Placed Canditates', $scope.candidateList[i].timeToPlace.monthly, 'Time to Place', 'monthly', $scope.candidateList[i].id);
                        }
                        if ($scope.candidateList[i].quarterlyshow) {
                            console.log("cominnnn 222");
                            $scope.plotFlotGraph($scope.candidateList[i].id + '-graph-bar', $scope.candidateList[i].active.quarterly, 'Active Canditates', $scope.candidateList[i].placed.quarterly, 'Placed Canditates', $scope.candidateList[i].timeToPlace.quarterly, 'Time to Place', 'quarterly', $scope.candidateList[i].id);
                        }
                        if ($scope.candidateList[i].weeklyshow) {
                            console.log("cominnnn 222");
                            $scope.plotFlotGraph($scope.candidateList[i].id + '-graph-bar', $scope.candidateList[i].active.weekly, 'Active Canditates', $scope.candidateList[i].placed.weekly, 'Placed Canditates', $scope.candidateList[i].timeToPlace.weekly, 'Time to Place', 'weekly', $scope.candidateList[i].id);
                        }

                    }

                }
            }

            $scope.toggleGraph = function (type, id) {
                for (var i = 0; i < $scope.candidateList.length; i++) {
                    if (id === $scope.candidateList[i].id) {
                        if (type === 'monthly') {
                            $scope.candidateList[i].weeklyshow = false;
                            $scope.candidateList[i].monthlyshow = true;
                            $scope.candidateList[i].quarterlyshow = false;
                        } else if (type === 'weekly') {
                            $scope.candidateList[i].weeklyshow = true;
                            $scope.candidateList[i].monthlyshow = false;
                            $scope.candidateList[i].quarterlyshow = false;
                        } else if (type === 'quarterly') {
                            $scope.candidateList[i].weeklyshow = false;
                            $scope.candidateList[i].monthlyshow = false;
                            $scope.candidateList[i].quarterlyshow = true;
                        } else {
                            //
                        }
                    }

                }
                $scope.drawGraph();
            }

            $scope.getGraphData = function (requisitionfilters) {
                $http.get("data/candidatereport.json")
                    .success(function (data) {
                        $scope.candidateList = data;
                        console.log("candidateList " + angular.toJson($scope.candidateList))
                        $timeout(function () {
                            $scope.drawGraph();
                        }, 100)
                    });
            }
            $scope.getFilterList = function () {
                $http.get("data/requisitionReportList.json")
                    .success(function (data) {
                        $scope.requisitionfilters = data;
                        $scope.clientlist = $scope.requisitionfilters[0].clientList;
                        console.log("requisitionList " + angular.toJson($scope.requisitionList))
                        $scope.bulist = $scope.requisitionfilters[0].bulist;
                        console.log("requisitionList " + angular.toJson($scope.requisitionList))
                        $scope.recruitersList = $scope.requisitionfilters[0].recruiterList;
                        console.log("requisitionList " + angular.toJson($scope.requisitionList))
                        $scope.RequisitonRoles = $scope.requisitionfilters[0].Rolelist;
                        console.log("requisitionList " + angular.toJson($scope.requisitionList))
                        $scope.getGraphData($scope.requisitionfilters);

                    });
            }
            $scope.getFilterList();

            //pdf download
            $scope.download = function () {
                html2canvas(document.getElementById('content'), {
                    onrendered: function (canvas) {
                        var data = canvas.toDataURL();
                        var docDefinition = {
                            content: [{
                                image: data,
                                width: 500,
                            }]
                        };
                        pdfMake.createPdf(docDefinition).download("candidate_performance_report.pdf");
                    }
                });
            }

        }]);
