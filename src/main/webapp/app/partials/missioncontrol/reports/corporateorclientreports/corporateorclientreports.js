/**
 * All the Functionalities Related to Reports
 * Module is included here
 */

var clientreportmodule = angular.module('4dot5.missioncontrolmodule.reportsmodule.corporateorclientreportmodule',
    []);

clientreportmodule.constant('CORPORATEORCLIENTREPORTCONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.reports.corporateorclientreports',
        URL: '/corporateorclient',
        CONTROLLER: 'CorporateOrClientReportController',
        TEMPLATEURL: 'app/partials/missioncontrol/reports/corporateorclientreports/corporateorclientreports.html',
    },
    CONTROLLER: {
         GET_CLIENTS_OR_BU: 'api/company/getclientsorbu',
         GET_REPORTS: 'data/reportData.json',
         LOGGED_IN_DETAILS: 'api/user/getloggedinuserdetails'
    }
});

clientreportmodule.config(
    ['$stateProvider',
        'CORPORATEORCLIENTREPORTCONSTANTS',
        function ($stateProvider, CORPORATEORCLIENTREPORTCONSTANTS) {
            $stateProvider.state(CORPORATEORCLIENTREPORTCONSTANTS.CONFIG.STATE, {
                url: CORPORATEORCLIENTREPORTCONSTANTS.CONFIG.URL,
                templateUrl: CORPORATEORCLIENTREPORTCONSTANTS.CONFIG.TEMPLATEURL,
                controller: CORPORATEORCLIENTREPORTCONSTANTS.CONFIG.CONTROLLER,
                data: {
                    requireLogin: true
                },
                params: {
                    reportType: null
                }
            });
        }
    ]);


clientreportmodule.controller('CorporateOrClientReportController',
    ['$scope',
        '$state',
        '$window',
        '$location',
        '$stateParams',
        '$rootScope',
        '$http',
        'StorageService',
        'genericService',
        'CORPORATEORCLIENTREPORTCONSTANTS',
        function ($scope, $state, $window, $location, $stateParams, $rootScope, $http, StorageService, genericService, CORPORATEORCLIENTREPORTCONSTANTS) {

            // console.log('ClientReportController +'+$rootScope.companyId);
            // console.log('ClientReportController +'+$rootScope.userDetails.company.companyId);
            // console.log('ClientReportController +'+$rootScope.userDetails.company.companyType);
            // console.log('ClientReportController +'+angular.toJson($rootScope.userDetails.company));
            // getclientsorbu/{companyid}

            //pdf download
            $scope.download=function (){
                html2canvas(document.getElementById('print-area'), {
                                onrendered: function (canvas) {
                                    var data = canvas.toDataURL();
                                    var docDefinition = {
                                        content: [{
                                            image: data,
                                            width: 500,
                                        }]
                                    };
                            pdfMake.createPdf(docDefinition).download("report.pdf");
                        }
                    });
            }

            $scope.clientBuList = [];
            $scope.reportList = [];
            $scope.clientorbu = {};
            $scope.clientorbu.company = {};
            $scope.companyType = $rootScope.userDetails.company.companyType;
            // console.log('entering'+ $scope.companyType);

            $scope.reportData={};  

            $scope.requisitionRoles = [
                                        {		name: "Software Engineer",ticked: true	},
                                        {		name: "UI Developer",ticked: true	},
                                        {		name: "Test Engineer",ticked: true	}
                                    ];  
            $scope.selectedRequisitionRoles = [];
            $scope.testValue="";
            // $scope.change = function(){
            //     alert('testData');
            //     console.log('hiiiii');
            // }

            $scope.clientDetails = true;
            $scope.reportPanel = true;
            $scope.showBelowDivision = function (){
                // console.log('entering cleanly');
                if($scope.selectedRequisitionRoles.length>0 && document.getElementById('reportrange')!= null ){
                    // alert('testData');
                    // console.log('success');
                    $scope.reportPanel = false;
                }
            }

            $scope.details = function(clientorbuid){
                if($scope.clientBuList.length>0){
                    $scope.clientDetails = false;
                }
                // console.log('id : '+ angular.toJson(clientorbuid));
                for(var i = 0; i < $scope.clientBuList.length; i++){
                    if($scope.clientBuList[i].company.id === clientorbuid){
                        $scope.clientorbu = $scope.clientBuList[i];
                    }
                }
                // console.log('client Object is : '+ angular.toJson($scope.clientorbu));
            }

            // $scope.reportPanelDetails = function(){
            //     $scope.reportPanel = false;
            // }


            genericService.getObjects(StorageService.get('baseurl') + CORPORATEORCLIENTREPORTCONSTANTS.CONTROLLER.GET_CLIENTS_OR_BU + '/' + $rootScope.userDetails.company.companyId).then(function (data) {
                $scope.clientBuList = data;
                // console.log('$scope.clientBuList ++++++' + angular.toJson($scope.clientBuList));
                
            }, function (error) {
                // console.log('error : ' + angular.toJson(error));
            });

             genericService.getObjects(StorageService.get('baseurl') + CORPORATEORCLIENTREPORTCONSTANTS.CONTROLLER.GET_REPORTS).then(function (data) {
                $scope.reportList = data;
                // console.log('$scope.reportList' + angular.toJson($scope.reportList));
            }, function (error) {
                // console.log('error : ' + angular.toJson(error));
            });

            genericService.getObjects(StorageService.get('baseurl') + CORPORATEORCLIENTREPORTCONSTANTS.CONTROLLER.LOGGED_IN_DETAILS).then(function (data) {
                // $scope.userDetailsCopy = angular.copy(data);
                // $scope.managers = [];
                // $scope.userDetails = data;
                // console.log('data'+angular.toJson(data));
            }, function (error) {
                // console.log('error : ' + angular.toJson(error));
            });

            
        }]);









