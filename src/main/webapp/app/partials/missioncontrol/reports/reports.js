/**
 * All the Functionalities Related to Reports
 * Module is included here
 */

var reportsmanagementmodule = angular.module('4dot5.missioncontrolmodule.reportsmodule',
    ['4dot5.missioncontrolmodule.reportsmodule.sourcingperformancemodule',
     '4dot5.missioncontrolmodule.reportsmodule.requisitionperformancemodule',
     '4dot5.missioncontrolmodule.reportsmodule.teammemberperformancemodule',
     '4dot5.missioncontrolmodule.reportsmodule.corporateorclientreportmodule'
     
    ]);

reportsmanagementmodule.constant('REPORTSCONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.reports',
        URL: '/reports',
        CONTROLLER: 'ReportsController',
        TEMPLATEURL: 'app/partials/missioncontrol/reports/reports.html',
    }
});

reportsmanagementmodule.config(
    ['$stateProvider',
        'REPORTSCONSTANTS',
        function ($stateProvider, REPORTSCONSTANTS) {
            $stateProvider.state(REPORTSCONSTANTS.CONFIG.STATE, {
                url: REPORTSCONSTANTS.CONFIG.URL,
                templateUrl: REPORTSCONSTANTS.CONFIG.TEMPLATEURL,
                controller: REPORTSCONSTANTS.CONFIG.CONTROLLER,
                data: {
                    requireLogin: true
                },
                params: {
                    reportType: null
                }
            });
        }
    ]);


reportsmanagementmodule.controller('ReportsController',
    ['$scope',
        '$state',
        '$window',
        '$location',
        '$stateParams',
        '$rootScope',
        '$http',
        'StorageService',
        'genericService',
        'REPORTSCONSTANTS',
        function ($scope, $state, $window, $location, $stateParams, $rootScope, $http, StorageService, genericService, REPORTSCONSTANTS) {
            console.log('ReportsManagementController');
        }
    ]);
