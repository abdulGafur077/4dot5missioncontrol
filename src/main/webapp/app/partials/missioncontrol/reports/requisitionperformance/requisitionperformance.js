/**
 * All the Functionalities Related to Reports
 * Module is included here
 */

var requisitionperformancemodule = angular.module('4dot5.missioncontrolmodule.reportsmodule.requisitionperformancemodule',
    []);

requisitionperformancemodule.constant('REQUISITIONPERFORMANCECONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.reports.requisitionperformance',
        URL: '/requisitionperformance',
        CONTROLLER: 'RequisitionPerformanceController',
        TEMPLATEURL: 'app/partials/missioncontrol/reports/requisitionperformance/requisitionperformance.html',
    }
});

requisitionperformancemodule.config(
    ['$stateProvider',
        'REQUISITIONPERFORMANCECONSTANTS',
        function ($stateProvider, REQUISITIONPERFORMANCECONSTANTS) {
            $stateProvider.state(REQUISITIONPERFORMANCECONSTANTS.CONFIG.STATE, {
                url: REQUISITIONPERFORMANCECONSTANTS.CONFIG.URL,
                templateUrl: REQUISITIONPERFORMANCECONSTANTS.CONFIG.TEMPLATEURL,
                controller: REQUISITIONPERFORMANCECONSTANTS.CONFIG.CONTROLLER,
                data: {
                    requireLogin: true
                },
                params: {
                    reportType: null
                }
            });
        }
    ]);


requisitionperformancemodule.controller('RequisitionPerformanceController',
    ['$scope',
        '$state',
        '$window',
        '$location',
        '$stateParams',
        '$rootScope',
        '$timeout',
        '$http',
        'StorageService',
        'genericService',
        'REQUISITIONPERFORMANCECONSTANTS',
        function ($scope, $state, $window, $location, $stateParams, $rootScope, $timeout, $http, StorageService, genericService, REQUISITIONPERFORMANCECONSTANTS) {
            console.log('RequisitionPerformanceController');



            $scope.RequisitionPerformance = {};
            selectedrecruitersList = [];
            $scope.types = ['Me', 'My team'];
            $scope.RequisitionPerformance.type = 'Me';

            $scope.reportType = 'requisitions';
            $scope.reportTypeLabel = 'Requisition Fill Performance';

            /**
             * Graph configurations
             */


            function getMonday(d) {
                d = new Date(d);
                var day = d.getDay(),
                    diff = d.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday
                //d.setDate(diff);
                return new Date(d.setDate(diff));
            }

            function getLastMondayTimeBySubtractingWeeksFromNow(weeks) {
                var d = new Date();
                d.setHours(0);
                d.setMinutes(0);
                return getMonday(new Date(d.getTime() - (weeks * 7 * 24 * 60 * 60 * 1000)).getTime()).getTime();
            }


            function getQuarter(d) {
                d = d || new Date();
                return m = Math.floor(d.getMonth() / 3);
                //return m > 4? m - 4 : m;
            }

            function getLastMonthOfQuarter(quarter) {
                return (quarter * 3) + 2;
            }

            function getTimeBySubtractingMonthsFromNow(months) {
                var d = new Date();
                return new Date(d.setMonth(d.getMonth() - months)).getTime();
            }

            function getTimeBySubtractingQuartersFromNow(quarters) {
                var d = new Date();
                d.setMonth(d.getMonth() - (quarters * 3));
                return new Date(d.setMonth(getLastMonthOfQuarter(getQuarter(d)))).getTime();
            }

            // $scope.RequisitonRoles = ['Requisition role 1', 'Requisition role 2', 'Requisition role 3', 'Requisition role 4', 'Requisition role 5', 'Requisition role 6'];
            $scope.requisitionRole = 'All';
             $scope.bu = function (selectedbu) {
                console.log("selected bu " + angular.toJson(selectedbu));
                $scope.requisitionfilters[0].bulist = selectedbu;
            }
            $scope.client = function (selectedclient) {
                console.log("selected client " + angular.toJson(selectedclient));
                $scope.requisitionfilters[0].clientList = selectedclient;
            }
            $scope.roleSelected = function (roleSelected) {
                console.log("roleSelected  " + roleSelected);
                $scope.requisitionfilters[0].Rolelist = roleSelected;
            }
            $scope.recruiterselected = function (selectedrecruitersList) {
                console.log("rec selected!! " + selectedrecruitersList);
                console.log("rec ");
                $scope.requisitionfilters[0].recruiterList = selectedrecruitersList;

                console.log("$scope.requisitionfilters[0].recruiterList " + angular.toJson($scope.requisitionfilters[0].recruiterList));
            }
            $scope.search = function () {
                console.log("$scope.requisitionfilters " + angular.toJson($scope.requisitionfilters));
                $scope.getGraphData($scope.requisitionfilters);
            }
            function plotFlotGraph(containerId, graph1Data, graph1Label, graph2Data, graph2Label, bar1Data, bar1Label, bar2Data, bar2Label, timeType) {
                console.log("coming.........")
                if ($('#' + containerId).length) {
                    var timeToSourceCandidatesData = bar1Data;
                    var timeToSourceCandidates = [];

                    var timeToFillCandidatesData = bar2Data;
                    var timeToFillCandidates = [];


                    var newreqCandidatesData = graph1Data;
                    var newreqCandidates = [];

                    var fulfilledCandidatesData = graph2Data;
                    var fulfilledCandidates = [];


                    var ticks = [];

                    for (var i = 0; i < timeToFillCandidatesData.length; i++) {
                        var arr = [];
                        var tempTime = '';
                        if (timeType === 'weekly') {
                            tempTime = getLastMondayTimeBySubtractingWeeksFromNow(timeToFillCandidatesData.length - i - 1);
                        } else if (timeType === 'monthly') {
                            tempTime = getTimeBySubtractingMonthsFromNow(timeToFillCandidatesData.length - i - 1);
                        } else if (timeType === 'quarterly') {
                            tempTime = getTimeBySubtractingQuartersFromNow(timeToFillCandidatesData.length - i - 1);
                        } else {
                            //
                        }

                        arr.push(tempTime);
                        arr.push(timeToSourceCandidatesData[i]);
                        timeToSourceCandidates.push(arr);

                        arr = [];
                        arr.push(tempTime);
                        arr.push(timeToFillCandidatesData[i]);
                        timeToFillCandidates.push(arr);

                        arr = [];
                        arr.push(tempTime);
                        arr.push(newreqCandidatesData[i]);
                        newreqCandidates.push(arr);

                        arr = [];
                        arr.push(tempTime);
                        arr.push(fulfilledCandidatesData[i]);
                        fulfilledCandidates.push(arr);


                        ticks.push(tempTime);
                    }

                    var xAxisMin = '';
                    var xAxisMax = '';
                    var xAsisBarWidth = '';
                    var xAxisLabel = '';
                    var xAxisTimeFormat = '';

                    if (timeType === 'weekly') {
                        xAxisMin = (new Date()).getTime() - ((timeToFillCandidatesData.length) * 7 * 24 * 60 * 60 * 1000);
                        xAxisMax = (new Date()).getTime() + (7 * 24 * 60 * 60 * 1000);
                        xAxisBarWidth = 24 * 60 * 60 * 800;
                        xAxisLabel = 'Past six weeks';
                        xAxisTimeFormat = "%m/%d";
                    } else if (timeType === 'monthly') {
                        xAxisMin = (new Date()).getTime() - ((timeToFillCandidatesData.length) * 31 * 24 * 60 * 60 * 1000);
                        xAxisMax = (new Date()).getTime() + (31 * 24 * 60 * 60 * 1000);
                        xAxisBarWidth = 24 * 60 * 60 * 3000;
                        xAxisLabel = 'Past six months';
                        xAxisTimeFormat = "%b '%y";
                    } else if (timeType === 'quarterly') {
                        xAxisMin = (new Date()).getTime() - (3 * (timeToFillCandidatesData.length) * 31 * 24 * 60 * 60 * 1000);
                        xAxisMax = (new Date()).getTime() + (6 * 31 * 24 * 60 * 60 * 1000);
                        xAxisBarWidth = 24 * 60 * 60 * 8000;
                        xAxisLabel = 'Past six quarters';
                        xAxisTimeFormat = "%b '%y";
                    } else {
                        //
                    }

                    var series = new Array();

                    series.push({
                        data: timeToSourceCandidates,
                        color: '#03a9f4',
                        bars: {
                            show: true,
                            barWidth: xAxisBarWidth,
                            lineWidth: 1,
                            order: 1,
                            fill: 1,
                            align: 'center'
                        },
                        label: bar1Label,
                        yaxis: 1
                    });
                    series.push({
                        data: timeToFillCandidates,
                        color: '#dfd188',
                        bars: {
                            show: true,
                            barWidth: xAxisBarWidth,
                            lineWidth: 1,
                            order: 2,
                            fill: 1,
                            align: 'center'
                        },
                        label: bar2Label,
                        yaxis: 1
                    });
                    series.push({
                        data: newreqCandidates,
                        color: '#8bc34a',
                        lines: {
                            show: true,
                            lineWidth: 3,
                        },
                        points: {
                            fillColor: '#ffffff',
                            pointWidth: 1,
                            show: true
                        },
                        label: graph1Label,
                        yaxis: 2
                    });
                    series.push({
                        data: fulfilledCandidates,
                        color: '#e84e40',
                        lines: {
                            show: true,
                            lineWidth: 3,
                        },
                        points: {
                            fillColor: '#ffffff',
                            pointWidth: 1,
                            show: true
                        },
                        label: graph2Label,
                        yaxis: 2
                    });

                    //plotiing graph

                    $.plot("#" + containerId, series, {
                        colors: ['#03a9f4', '#f1c40f', '#2ecc71', '#3498db', '#9b59b6', '#95a5a6'],
                        grid: {
                            tickColor: "#f2f2f2",
                            borderWidth: 0,
                            hoverable: true,
                            clickable: true
                        },
                        legend: {
                            noColumns: 1,
                            labelBoxBorderColor: "#000000",
                            position: "ne"
                        },
                        shadowSize: 0,
                        xaxis: {
                            mode: "time",
                            ticks: ticks,
                            timezone: "browser",
                            //tickSize: [1, "day"],
                            timeformat: xAxisTimeFormat,
                            //tickLength: 0,
                            axisLabel: xAxisLabel,
                            min: xAxisMin,
                            max: xAxisMax,
                            axisLabelUseCanvas: true,
                            axisLabelFontSizePixels: 12,
                            axisLabelFontFamily: 'Open Sans, sans-serif',
                            axisLabelPadding: 10
                        },
                        yaxes: [
                            {
                                // tickFormatter: function (val, axis) {
                                //     return val;
                                // },	
                                position: "left",
                                axisLabel: "Time (in days)",
                                axisLabelUseCanvas: true,
                                axisLabelFontSizePixels: 12,
                                axisLabelFontFamily: "Verdana, Arial, Helvetica, Tahoma, sans-serif",
                                axisLabelPadding: 5
                            },
                            {
                                position: "right",
                                axisLabel: "Number of candidates",
                                axisLabelUseCanvas: true,
                                axisLabelFontSizePixels: 12,
                                axisLabelFontFamily: "Verdana, Arial, Helvetica, Tahoma, sans-serif",
                                axisLabelPadding: 5
                            }
                        ]
                    });

                    var previousPoint = null;
                    $("#" + containerId).bind("plothover", function (event, pos, item) {
                        if (item) {
                            if (previousPoint != item.dataIndex) {

                                previousPoint = item.dataIndex;

                                $("#flot-tooltip").remove();
                                var x = item.datapoint[0],
                                    y = item.datapoint[1];

                                showTooltip(item.pageX, item.pageY, item.series.label, y);
                            }
                        }
                        else {
                            $("#flot-tooltip").remove();
                            previousPoint = [0, 0, 0];
                        }
                    });

                    function showTooltip(x, y, label, data) {
                        $('<div id="flot-tooltip">' + '<b>' + label + ': </b><i>' + data + '</i>' + '</div>').css({
                            top: y + 5,
                            left: x + 20
                        }).appendTo("body").fadeIn(200);
                    }
                };
            }

            $scope.drawGraph = function () {
                console.log("cominnnn 1");
                for (var i = 0; i < $scope.requisitionList.length; i++) {
                    if ($scope.requisitionList[i].weeklyshow) {
                        plotFlotGraph($scope.requisitionList[i].id + '-graph-bar', $scope.requisitionList[i].newreq.weekly, 'New Candidates', $scope.requisitionList[i].fulfilled.weekly, 'Fulfilled Candidates', $scope.requisitionList[i].timeToSource.weekly, 'Time to Source', $scope.requisitionList[i].timeToFill.weekly, 'Time To Fill', 'weekly');
                    }
                    if ($scope.requisitionList[i].monthlyshow) {
                        plotFlotGraph($scope.requisitionList[i].id + '-graph-bar', $scope.requisitionList[i].newreq.monthly, 'New Candidates', $scope.requisitionList[i].fulfilled.monthly, 'Fulfilled Candidates', $scope.requisitionList[i].timeToSource.monthly, 'Time to Source', $scope.requisitionList[i].timeToFill.monthly, 'Time To Fill', 'monthly');
                    }
                    if ($scope.requisitionList[i].quarterlyshow) {
                        plotFlotGraph($scope.requisitionList[i].id + '-graph-bar', $scope.requisitionList[i].newreq.quarterly, 'New Candidates', $scope.requisitionList[i].fulfilled.quarterly, 'Fulfilled Candidates', $scope.requisitionList[i].timeToSource.quarterly, 'Time to Source', $scope.requisitionList[i].timeToFill.quarterly, 'Time To Fill', 'quarterly');
                    }
                }
            }

            $scope.toggleGraph = function (type, id) {
                for (var i = 0; i < $scope.requisitionList.length; i++) {
                    if (id === $scope.requisitionList[i].id) {
                        if (type === 'monthly') {
                            $scope.requisitionList[i].weeklyshow = false;
                            $scope.requisitionList[i].monthlyshow = true;
                            $scope.requisitionList[i].quarterlyshow = false;
                        } else if (type === 'weekly') {
                            $scope.requisitionList[i].weeklyshow = true;
                            $scope.requisitionList[i].monthlyshow = false;
                            $scope.requisitionList[i].quarterlyshow = false;
                        } else if (type === 'quarterly') {
                            $scope.requisitionList[i].weeklyshow = false;
                            $scope.requisitionList[i].monthlyshow = false;
                            $scope.requisitionList[i].quarterlyshow = true;
                        } else {
                            //
                        }
                    }

                }
                $scope.drawGraph();
            }
            $scope.getGraphData = function (datalist) {
                console.log("datalist " + angular.toJson(datalist))
                $http.get("data/requisitionReport.json")
                    .success(function (data) {
                        $scope.requisitionList = data;
                        console.log("requisitionList " + angular.toJson($scope.requisitionList))
                        $timeout(function () {
                            $scope.drawGraph();
                        }, 100)
                    });
            }
            $scope.getFilterList = function () {
                $http.get("data/requisitionReportList.json")
                    .success(function (data) {
                        $scope.requisitionfilters = data;
                        $scope.clientlist = $scope.requisitionfilters[0].clientList;
                        console.log("requisitionList " + angular.toJson($scope.requisitionList))
                        $scope.bulist = $scope.requisitionfilters[0].bulist;
                        console.log("requisitionList " + angular.toJson($scope.requisitionList))
                        $scope.recruitersList = $scope.requisitionfilters[0].recruiterList;
                        console.log("requisitionList " + angular.toJson($scope.requisitionList))
                        $scope.RequisitonRoles = $scope.requisitionfilters[0].Rolelist;
                        console.log("requisitionList " + angular.toJson($scope.requisitionList))
                        $scope.getGraphData($scope.requisitionfilters);

                    });
            }
            $scope.getFilterList();

            //pdf download
            $scope.download = function () {
                html2canvas(document.getElementById('content'), {
                    onrendered: function (canvas) {
                        var data = canvas.toDataURL();
                        var docDefinition = {
                            content: [{
                                image: data,
                                width: 500,
                            }]
                        };
                        pdfMake.createPdf(docDefinition).download("requisition_performance_report.pdf");
                    }
                });
            }

        }
    ]);
