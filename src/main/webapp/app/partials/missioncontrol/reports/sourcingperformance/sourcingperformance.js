/**
 * All the Functionalities Related to Reports
 * Module is included here
 */

var sourcingperformancemodule = angular.module('4dot5.missioncontrolmodule.reportsmodule.sourcingperformancemodule',
    []);

sourcingperformancemodule.constant('SOURCINGPERFORMANCECONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.reports.sourcingperformance',
        URL: '/sourcingperformance',
        CONTROLLER: 'SourcingPerformanceController',
        TEMPLATEURL: 'app/partials/missioncontrol/reports/sourcingperformance/sourcingperformance.html',
    }
});

sourcingperformancemodule.config(
    ['$stateProvider',
        'SOURCINGPERFORMANCECONSTANTS',
        function ($stateProvider, SOURCINGPERFORMANCECONSTANTS) {
            $stateProvider.state(SOURCINGPERFORMANCECONSTANTS.CONFIG.STATE, {
                url: SOURCINGPERFORMANCECONSTANTS.CONFIG.URL,
                templateUrl: SOURCINGPERFORMANCECONSTANTS.CONFIG.TEMPLATEURL,
                controller: SOURCINGPERFORMANCECONSTANTS.CONFIG.CONTROLLER,
                data: {
                    requireLogin: true
                },
                params: {

                }
            });
        }
    ]);


sourcingperformancemodule.controller('SourcingPerformanceController',
    ['$scope',
        '$state',
        '$window',
        '$location',
        '$stateParams',
        '$rootScope',
        '$timeout',
        '$http',
        'StorageService',
        'genericService',
        'SOURCINGPERFORMANCECONSTANTS',
        function ($scope, $state, $window, $location, $stateParams, $rootScope, $timeout, $http, StorageService, genericService, SOURCINGPERFORMANCECONSTANTS) {

            console.log('Entered into SourcingPerformanceController');

            $scope.sources = [];
            $scope.reportType = 'sourcing';
            $scope.reportTypeLabel = 'Sourcing Performance';
            $scope.requisitionRole = 'All';
            $scope.bu = function (selectedbu) {
                console.log("selected bu " + angular.toJson(selectedbu));
                $scope.requisitionfilters[0].bulist = selectedbu;
            }
            $scope.client = function (selectedclient) {
                console.log("selected client " + angular.toJson(selectedclient));
                $scope.requisitionfilters[0].clientList = selectedclient;
            }
            $scope.roleSelected = function (roleSelected) {
                console.log("roleSelected  " + roleSelected);
                $scope.requisitionfilters[0].Rolelist = roleSelected;
            }
            $scope.selectedsourceTypeList = function (selectedsourcetypeList) {
                $scope.selectedsourcetypeList='';
                console.log("selectedsourceList " + angular.toJson(selectedsourcetypeList));
                $scope.selectedsourcetypeList = angular.copy(selectedsourcetypeList);
                // $scope.sourceTypeSelected = selectedsourceList;
                //  console.log("rec selected!! " + selectedrecruitersList);
                // console.log("rec ");
                //  $scope.requisitionfilters[0].selectedsourceList = selectedsourceList;

                //  console.log("$scope.requisitionfilters[0].recruiterList " + angular.toJson($scope.requisitionfilters[0].selectedsourceList));
            }
            $scope.search = function () {
                console.log("$scope.requisitionfilters " + angular.toJson($scope.requisitionfilters));
                $scope.selectedsourceList = angular.copy($scope.selectedsourcetypeList);
                $timeout(function () {
                    $scope.draw();
                }, 100)
                //$scope.getGraphData($scope.requisitionfilters);
            }



            function plotStackedGraph(containerId, d1, d2, d3, d4, d5) {
                console.log('Entered into plotStackedGraph');

                if ($('#' + containerId).length) {
                    //var d1 = [3, 4, 4, 3, 4, 4];
                    var d1Data = [];

                    //var d2 = [1, 2, 2, 1, 2, 1];
                    var d2Data = [];

                    //var d3 = [0, 1, 1, 0, 1, 0];
                    var d3Data = [];

                    //var d4 = [0, 0, 1, 0, 0, 0];
                    var d4Data = [];

                    //var d5 = [0, 0, 1, 0, 0, 0];
                    var d5Data = [];

                    var ticks = [];

                    function getMonday(d) {
                        d = new Date(d);
                        var day = d.getDay(),
                            diff = d.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday
                        //d.setDate(diff);
                        //console.log(d.getDate());
                        return new Date(d.setDate(diff));
                    }

                    function getLastMondayTimeBySubtractingWeeksFromNow(weeks) {
                        var d = new Date();
                        d.setHours(0);
                        d.setMinutes(0);
                        return getMonday(new Date(d.getTime() - (weeks * 7 * 24 * 60 * 60 * 1000)).getTime()).getTime();
                    }

                    for (var i = 0; i <= d1.length; i += 1) {
                        var tempTime = getLastMondayTimeBySubtractingWeeksFromNow(d1.length - i - 1);
                        d1Data.push([tempTime, d1[i]]);
                        d2Data.push([tempTime, d2[i]]);
                        d3Data.push([tempTime, d3[i]]);
                        d4Data.push([tempTime, d4[i]]);
                        d5Data.push([tempTime, d5[i]]);
                        ticks.push(tempTime);
                    }
                    var d1SeriesData = {
                        label: 'Sourced',
                        data: d1Data
                    };
                    // for (var i = 0; i <= d2.length; i += 1) {
                    // 	d2Data.push([i, d1[i]]);
                    // }
                    var d2SeriesData = {
                        label: 'Qualified',
                        data: d2Data
                    };
                    // for (var i = 0; i <= d3.length; i += 1) {
                    // 	d3Data.push([i, d3[i]]);
                    // }
                    var d3SeriesData = {
                        label: 'Tech Assessment',
                        data: d3Data
                    };
                    // for (var i = 0; i <= d4.length; i += 1) {
                    // 	d4Data.push([i, d4[i]]);
                    // }
                    var d4SeriesData = {
                        label: 'Gold Standard',
                        data: d4Data
                    };
                    // for (var i = 0; i <= d5.length; i += 1) {
                    // 	d5Data.push([i, d5[i]]);
                    // }
                    var d5SeriesData = {
                        label: 'Hired',
                        data: d5Data
                    };
                    var series = [d1SeriesData, d2SeriesData, d3SeriesData, d4SeriesData, d5SeriesData];
                    var stack = 0,
                        bars = true,
                        lines = false,
                        steps = false;
                    function plotGraphWithOptions() {
                        $.plot("#" + containerId, series, {
                            series: {
                                stack: stack,
                                lines: {
                                    show: lines,
                                    //fill: true,
                                    steps: steps,
                                    lineWidth: 1,
                                    fill: 1
                                },
                                bars: {
                                    show: bars,
                                    barWidth: 24 * 60 * 60 * 800,
                                    lineWidth: 1,
                                    fill: 1
                                }
                            },
                            colors: ['#e84e40', '#ffc107', '#03a9f4', '#9c27b0', '#8bc34a', '#90a4ae'],
                            grid: {
                                tickColor: "#ddd",
                                borderWidth: 0
                            },
                            legend: {
                                noColumns: 1,
                                labelBoxBorderColor: "#000000",
                                position: "ne"
                            },
                            shadowSize: 0,
                            xaxis: {
                                mode: "time",
                                ticks: ticks,
                                timezone: "browser",
                                //tickSize: [1, "day"],
                                timeformat: "%m/%d",
                                //tickLength: 0,
                                axisLabel: "Past six weeks",
                                min: (new Date()).getTime() - ((d1.length) * 7 * 24 * 60 * 60 * 1000),
                                max: (new Date()).getTime() + (7 * 24 * 60 * 60 * 1000),
                                axisLabelUseCanvas: true,
                                axisLabelFontSizePixels: 12,
                                axisLabelFontFamily: 'Open Sans, sans-serif',
                                axisLabelPadding: 10
                            }
                        });
                        var previousPoint = null;
                        $("#" + containerId).on("plothover", function (event, pos, item) {
                            alert('hi');
                            console.log('item', item);
                            if (item) {
                                if (previousPoint != item.dataIndex) {

                                    previousPoint = item.dataIndex;

                                    $("#flot-tooltip").remove();
                                    var x = item.datapoint[0],
                                        y = item.datapoint[1];

                                    showTooltip(item.pageX, item.pageY, item.series.label, y);
                                }
                            }
                            else {
                                $("#flot-tooltip").remove();
                                previousPoint = [0, 0, 0];
                            }
                        });

                        function showTooltip(x, y, label, data) {
                            $('<div id="flot-tooltip">' + '<b>' + label + ': </b><i>' + data + '</i>' + '</div>').css({
                                top: y + 5,
                                left: x + 20
                            }).appendTo("body").fadeIn(200);
                        }
                    }
                    plotGraphWithOptions();
                }
            }

            $scope.draw = function () {
                console.log("ddddd");
                for (var i = 0; i < $scope.selectedsourceList.length; i++) {
                    plotStackedGraph($scope.selectedsourceList[i].id + '-graph-bar', $scope.selectedsourceList[i].Sourcedweek, $scope.selectedsourceList[i].Qualified, $scope.selectedsourceList[i].Techassessment, $scope.selectedsourceList[i].Gold_Candidatesweek, $scope.selectedsourceList[i].Hiredweek);
                }
            }
            $scope.getGraphData = function (Datalist) {
                $http.get("app-content/jsonobjects/sourcing.json").success(function (data) {
                    angular.forEach(data, function (val, key) {
                        val.numberSourced = val['# Sourced'];
                        val.percentGoldCandidates = val['% Gold Candidates'];
                        val.percentHired = val['% Hired'];
                        val.numberHired = val['# Hired'];
                    });
                    $scope.sources = data;
                    $scope.selectedsourceList = $scope.sources;
                    $timeout(function () {
                        $scope.draw();
                    }, 100)
                });
            }
            $scope.getFilterList = function () {
                $http.get("data/requisitionReportList.json")
                    .success(function (data) {
                        $scope.requisitionfilters = data;
                        $scope.clientlist = $scope.requisitionfilters[0].clientList;
                        console.log("requisitionList " + angular.toJson($scope.clientlist))
                        $scope.bulist = $scope.requisitionfilters[0].bulist;
                        console.log("requisitionList " + angular.toJson($scope.bulist))
                        $scope.recruitersList = $scope.requisitionfilters[0].recruiterList;
                        console.log("requisitionList " + angular.toJson($scope.requisitionList))
                        $scope.RequisitonRoles = $scope.requisitionfilters[0].Rolelist;
                        console.log("requisitionList " + angular.toJson($scope.requisitionList))
                        $scope.getGraphData($scope.requisitionfilters);

                    });
            }
            $scope.getFilterList();

            //pdf download
            $scope.download = function () {
                html2canvas(document.getElementById('content'), {
                    onrendered: function (canvas) {
                        var data = canvas.toDataURL();
                        var docDefinition = {
                            content: [{
                                image: data,
                                width: 500,
                            }]
                        };
                        pdfMake.createPdf(docDefinition).download("sourcing_performance_report.pdf");
                    }
                });
            }


        }]);