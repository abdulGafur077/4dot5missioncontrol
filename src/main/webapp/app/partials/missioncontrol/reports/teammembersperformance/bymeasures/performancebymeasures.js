/**
 * All the Functionalities Related to Reports
 * Module is included here
 */

var performancebymeasuresmodule = angular.module('4dot5.missioncontrolmodule.reportsmodule.teammemberperformancemodule.performancebymeasuresmodule',
    []);

performancebymeasuresmodule.constant('PERFORMANCEBYMEASURECONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.reports.teammembersperformance.performancebymeasure',
        URL: '/teammembersperformance',
        CONTROLLER: 'TeamMembersPerformanceByMeasureController',
        TEMPLATEURL: 'app/partials/missioncontrol/reports/teammembersperformance/bymeasures/performancebymeasure.html',
    }
});

performancebymeasuresmodule.config(
    ['$stateProvider',
        'PERFORMANCEBYMEASURECONSTANTS',
        function ($stateProvider, PERFORMANCEBYMEASURECONSTANTS) {
            $stateProvider.state(PERFORMANCEBYMEASURECONSTANTS.CONFIG.STATE, {
                url: PERFORMANCEBYMEASURECONSTANTS.CONFIG.URL,
                templateUrl: PERFORMANCEBYMEASURECONSTANTS.CONFIG.TEMPLATEURL,
                controller: PERFORMANCEBYMEASURECONSTANTS.CONFIG.CONTROLLER,
                data: {
                    requireLogin: true
                },
                params: {
                    reportType: null
                }
            });
        }
    ]);


performancebymeasuresmodule.controller('TeamMembersPerformanceByMeasureController',
    ['$scope',
        '$state',
        '$window',
        '$location',
        '$stateParams',
        '$rootScope',
        '$http',
        'StorageService',
        'genericService',
        'PERFORMANCEBYMEASURECONSTANTS',
        function ($scope, $state, $window, $location, $stateParams, $rootScope, $http, StorageService, genericService, PERFORMANCEBYMEASURECONSTANTS) {

            $scope.reportTypeLabel = 'Team Member Performance by Measure';
            $scope.PerformanceByMeasure = {};
            //Dropdowns
            $scope.CompanyTypes = ['Client', 'Bu'];
            $scope.RequisitonRoles = ['Requisition role 1', 'Requisition role 2', 'Requisition role 3', 'Requisition role 4', 'Requisition role 5', 'Requisition role 6'];
            $scope.PerformanceByMeasure.companyType = 'Client';
            $scope.PerformanceByMeasure.requisitonRole = 'Requisition role 1';

            //Measure related variable
            $scope.showTeamMemberGraphByMeasure = true;

            //Measure related variable
            $scope.showTimeToConvertPerformanceWeeklyGraph = true;
            $scope.showTimeToConvertPerformanceMonthlyGraph = false;
            $scope.showTimeToConvertPerformanceQuarterlyGraph = false;

            $scope.showTimeToPlacePerformanceWeeklyGraph = true;
            $scope.showTimeToPlacePerformanceMonthlyGraph = false;
            $scope.showTimeToPlacePerformanceQuarterlyGraph = false;

            $scope.showTimeToSourcePerformanceWeeklyGraph = true;
            $scope.showTimeToSourcePerformanceMonthlyGraph = false;
            $scope.showTimeToSourcePerformanceQuarterlyGraph = false;

            $scope.showTimeToFillPerformanceWeeklyGraph = true;
            $scope.showTimeToFillPerformanceMonthlyGraph = false;
            $scope.showTimeToFillPerformanceQuarterlyGraph = false;

            $scope.showActivePerformanceWeeklyGraph = true;
            $scope.showActivePerformanceMonthlyGraph = false;
            $scope.showActivePerformanceQuarterlyGraph = false;

            $scope.showPlacedCandidatesWeeklyGraph = true;
            $scope.showPlacedCandidatesMonthlyGraph = false;
            $scope.showPlacedCandidatesQuarterlyGraph = false;

            $scope.showConvertedCandidatesWeeklyGraph = true;
            $scope.showConvertedCandidatesMonthlyGraph = false;
            $scope.showConvertedCandidatesQuarterlyGraph = false;

            $scope.showPassiveConversionPerformanceWeeklyGraph = true;
            $scope.showPassiveConversionPerformanceMonthlyGraph = false;
            $scope.showPassiveConversionPerformanceQuarterlyGraph = false;

            function getMonday(d) {
                d = new Date(d);
                var day = d.getDay(),
                    diff = d.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday
                //d.setDate(diff);
                return new Date(d.setDate(diff));
            }

            function getLastMondayTimeBySubtractingWeeksFromNow(weeks) {
                var d = new Date();
                d.setHours(0);
                d.setMinutes(0);
                return getMonday(new Date(d.getTime() - (weeks * 7 * 24 * 60 * 60 * 1000)).getTime()).getTime();
            }

            function getQuarter(d) {
                d = d || new Date();
                return m = Math.floor(d.getMonth() / 3);
                //return m > 4? m - 4 : m;
            }

            function getLastMonthOfQuarter(quarter) {
                return (quarter * 3) + 2;
            }

            function getTimeBySubtractingMonthsFromNow(months) {
                var d = new Date();
                return new Date(d.setMonth(d.getMonth() - months)).getTime();
            }

            function getTimeBySubtractingQuartersFromNow(quarters) {
                var d = new Date();
                d.setMonth(d.getMonth() - (quarters * 3));
                return new Date(d.setMonth(getLastMonthOfQuarter(getQuarter(d)))).getTime();
            }
            $scope.bu = function (selectedbu) {
                console.log("selected bu " + angular.toJson(selectedbu));
                $scope.requisitionfilters[0].bulist = selectedbu;
            }
            $scope.client = function (selectedclient) {
                console.log("selected client " + angular.toJson(selectedclient));
                $scope.requisitionfilters[0].clientList = selectedclient;
            }
            $scope.roleSelected = function (roleSelected) {
                console.log("roleSelected  " + roleSelected);
                $scope.requisitionfilters[0].Rolelist = roleSelected;
            }
            $scope.selectedmeasureteamMembers = function (selectedmeasureteamMembersList) {
                console.log("selectedsourceList " + angular.toJson(selectedmeasureteamMembersList));

                // $scope.sourceTypeSelected = selectedsourceList;
                //  console.log("rec selected!! " + selectedrecruitersList);
                // console.log("rec ");
                //  $scope.requisitionfilters[0].selectedsourceList = selectedsourceList;

                //  console.log("$scope.requisitionfilters[0].recruiterList " + angular.toJson($scope.requisitionfilters[0].selectedsourceList));
            }
            $scope.search = function () {
                console.log("$scope.requisitionfilters " + angular.toJson($scope.requisitionfilters));
                $scope.renderTeamMembersMeasureTypesGraph();
                //$scope.getGraphData($scope.requisitionfilters);
            }


            //Measure related variable
            $scope.undefinedTeamMember = {
                name: undefined,
                timeToConvert: {
                    weekly: undefined,
                    monthly: undefined,
                    quarterly: undefined
                },
                timeToPlace: {
                    weekly: undefined,
                    monthly: undefined,
                    quarterly: undefined
                },
                timeToSource: {
                    weekly: undefined,
                    monthly: undefined,
                    quarterly: undefined
                },
                timeToFill: {
                    weekly: undefined,
                    monthly: undefined,
                    quarterly: undefined
                },
                active: {
                    weekly: undefined,
                    monthly: undefined,
                    quarterly: undefined
                },
                passiveConversion: {
                    weekly: undefined,
                    monthly: undefined,
                    quarterly: undefined
                },
                placed: {
                    weekly: undefined,
                    monthly: undefined,
                    quarterly: undefined
                },
                converted: {
                    weekly: undefined,
                    monthly: undefined,
                    quarterly: undefined
                }
            };


            $scope.plotFlotGraph = function (containerId, graphType, setlist, timeType, yAxisLabel) {
                if ($('#' + containerId).length) {
                    //var set1Data = set3Data;
                    var set = [];
                    var setData = [];
                    var setLabel = [];
                    for (var k = 0; k < setlist.length; k++) {
                        set[k] = [];
                        setData[k] = setlist[k].list;
                        setLabel[k] = setlist[k].name;
                    }
                    var ticks = [];
                    for (var i = 0; i < setData[0].length; i++) {
                        var arr = [];
                        var tempTime = '';
                        if (timeType === 'weekly') {
                            tempTime = getLastMondayTimeBySubtractingWeeksFromNow(setData[0].length - i - 1);
                        } else if (timeType === 'monthly') {
                            tempTime = getTimeBySubtractingMonthsFromNow(setData[0].length - i - 1);
                        } else if (timeType === 'quarterly') {
                            tempTime = getTimeBySubtractingQuartersFromNow(setData[0].length - i - 1);
                        } else {
                            //
                        }
                        // arr.push(tempTime);
                        // arr.push(setData[0][i]);
                        // set[0].push(arr);
                        for (var j = 0; j < setData.length; j++) {
                            if (angular.isDefined(setData[j])) {
                                arr = [];
                                arr.push(tempTime);
                                arr.push(setData[j][i]);
                                set[j].push(arr);
                            }
                        }
                        ticks.push(tempTime);
                    }
                    var xAxisMin = '';
                    var xAxisMax = '';
                    var xAsisBarWidth = '';
                    var xAxisLabel = '';
                    var xAxisTimeFormat = '';
                    if (timeType === 'weekly') {
                        xAxisMin = (new Date()).getTime() - ((setData[0].length) * 7 * 24 * 60 * 60 * 1000);
                        xAxisMax = (new Date()).getTime() + (7 * 24 * 60 * 60 * 1000);
                        xAxisBarWidth = 24 * 60 * 60 * 800;
                        xAxisLabel = 'Past six weeks';
                        xAxisTimeFormat = "%m/%d";
                    } else if (timeType === 'monthly') {
                        xAxisMin = (new Date()).getTime() - ((setData[0].length) * 31 * 24 * 60 * 60 * 1000);
                        xAxisMax = (new Date()).getTime() + (31 * 24 * 60 * 60 * 1000);
                        xAxisBarWidth = 24 * 60 * 60 * 3500;
                        xAxisLabel = 'Past six months';
                        xAxisTimeFormat = "%b '%y";
                    } else if (timeType === 'quarterly') {
                        xAxisMin = (new Date()).getTime() - (3 * (setData[0].length) * 31 * 24 * 60 * 60 * 1000);
                        xAxisMax = (new Date()).getTime() + (6 * 31 * 24 * 60 * 60 * 1000);
                        xAxisBarWidth = 24 * 60 * 60 * 9500;
                        xAxisLabel = 'Past six quarters';
                        xAxisTimeFormat = "%b '%y";
                    } else {
                        //
                    }
                    var colors = ["#a2d4ee", "#dfd188", "#f781bf", "#8bc34a", "#f79981", "#03a9f4", "#e84e40", "#ff0000", "#008000", "#ffc0cb", "#00008b", "#00ffff", "#f0ffff", "#f5f5dc", "#000000", "#0000ff", "#a52a2a", "#00ffff", "#008b8b", "#a9a9a9", "#006400", "#bdb76b", "#8b008b", "#556b2f", "#ff8c00", "#9932cc", "#8b0000", "#e9967a", "#9400d3", "#ff00ff", "#ffd700", "#4b0082", "#f0e68c", "#add8e6", "#e0ffff", "#90ee90", "#d3d3d3", "#ffb6c1", "#ffffe0", "#00ff00", "#ff00ff", "#800000", "#000080", "#808000", "#ffa500", "#800080", "#800080", "#c0c0c0", "#ffffff", "#ffff00"];

                    var series = new Array();
                    for (var i = 0; i < set.length; i++) {
                        if (graphType === 'allLines') {
                            if (angular.isDefined(setData[i])) {
                                series.push({
                                    data: set[i],
                                    color: colors[i],
                                    lines: {
                                        show: true,
                                        lineWidth: 3,
                                    },
                                    points: {
                                        fillColor: '#ffffff',
                                        pointWidth: 1,
                                        show: true
                                    },
                                    label: setLabel[i],
                                    yaxis: 1
                                });
                            }
                        } else if (graphType === 'allBars') {
                            if (angular.isDefined(setData[i])) {
                                series.push({
                                    data: set[i],
                                    color: colors[i],
                                    bars: {
                                        show: true,
                                        barWidth: xAxisBarWidth,
                                        lineWidth: 1,
                                        order: i + 1,
                                        fill: 1,
                                        align: 'left'
                                    },
                                    label: setLabel[i],
                                    yaxis: 1
                                });
                            }
                        }
                    }


                    $.plot("#" + containerId, series, {
                        colors: ['#03a9f4', '#f1c40f', '#2ecc71', '#3498db', '#9b59b6', '#95a5a6'],
                        grid: {
                            tickColor: "#f2f2f2",
                            borderWidth: 0,
                            hoverable: true,
                            clickable: true
                        },
                        legend: {
                            noColumns: 1,
                            labelBoxBorderColor: "#000000",
                            position: "ne"
                        },
                        shadowSize: 0,
                        xaxis: {
                            mode: "time",
                            ticks: ticks,
                            timezone: "browser",
                            //tickSize: [1, "day"],
                            timeformat: xAxisTimeFormat,
                            //tickLength: 0,
                            axisLabel: xAxisLabel,
                            min: xAxisMin,
                            max: xAxisMax,
                            axisLabelUseCanvas: true,
                            axisLabelFontSizePixels: 12,
                            axisLabelFontFamily: 'Open Sans, sans-serif',
                            axisLabelPadding: 10
                        },
                        yaxes: [
                            {
                                position: "left",
                                axisLabel: yAxisLabel,
                                axisLabelUseCanvas: true,
                                axisLabelFontSizePixels: 12,
                                axisLabelFontFamily: "Verdana, Arial, Helvetica, Tahoma, sans-serif",
                                axisLabelPadding: 5
                            }
                        ]
                    });
                    var previousPoint = null;
                    $("#" + containerId).bind("plothover", function (event, pos, item) {
                        if (item) {
                            if (previousPoint != item.dataIndex) {

                                previousPoint = item.dataIndex;

                                $("#flot-tooltip").remove();
                                var x = item.datapoint[0],
                                    y = item.datapoint[1];

                                showTooltip(item.pageX, item.pageY, item.series.label, y);
                            }
                        }
                        else {
                            $("#flot-tooltip").remove();
                            previousPoint = [0, 0, 0];
                        }
                    });

                    function showTooltip(x, y, label, data) {
                        $('<div id="flot-tooltip">' + '<b>' + label + ': </b><i>' + data + '</i>' + '</div>').css({
                            top: y + 5,
                            left: x + 20
                        }).appendTo("body").fadeIn(200);
                    }
                };
            }
            $scope.renderTeamMembersMeasureTypesGraph = function () {
                var i = 0;
                $scope.selectedteamMembersCopy = [];
                angular.copy($scope.selectedmeasureteamMembersList, $scope.selectedteamMembersCopy);
                for (i = 0; i < $scope.selectedmeasureteamMembersList.length; i++) {
                    if (!angular.isDefined($scope.selectedteamMembersCopy[i])) {
                        $scope.selectedteamMembersCopy.push({});
                        angular.copy($scope.undefinedTeamMember, $scope.selectedteamMembersCopy[i]);
                    }
                }
                $scope.ActivePerformanceWeekly = [];
                $scope.ActivePerformanceMonthly = [];
                $scope.ActivePerformanceQuarterly = [];

                $scope.PlacedCandidatesWeekly = [];
                $scope.PlacedCandidatesMonthly = [];
                $scope.PlacedCandidatesQuarterly = [];

                $scope.ConvertedCandidatesWeekly = [];
                $scope.ConvertedCandidatesMonthly = [];
                $scope.ConvertedCandidatesQuarterly = [];

                $scope.PassiveConversionPerformanceWeekly = [];
                $scope.PassiveConversionPerformanceMonthly = [];
                $scope.PassiveConversionPerformanceQuarterly = [];

                $scope.TimeToSourcePerformanceWeekly = [];
                $scope.TimeToSourcePerformanceMonthly = [];
                $scope.TimeToSourcePerformanceQuarterly = [];

                $scope.TimeToConvertPerformanceWeekly = [];
                $scope.TimeToConvertPerformanceMonthly = [];
                $scope.TimeToConvertPerformanceQuarterly = [];

                $scope.TimeToFillPerformanceWeekly = [];
                $scope.TimeToFillPerformanceMonthly = [];
                $scope.TimeToFillPerformanceQuarterly = [];

                $scope.TimeToPlacePerformanceWeekly = [];
                $scope.TimeToPlacePerformanceMonthly = [];
                $scope.TimeToPlacePerformanceQuarterly = [];


                for (var i = 0; i < $scope.selectedteamMembersCopy.length; i++) {
                    $scope.ActivePerformanceWeekly.push({ list: $scope.selectedteamMembersCopy[i].active.weekly, name: $scope.selectedteamMembersCopy[i].name });
                    $scope.ActivePerformanceMonthly.push({ list: $scope.selectedteamMembersCopy[i].active.monthly, name: $scope.selectedteamMembersCopy[i].name });
                    $scope.ActivePerformanceQuarterly.push({ list: $scope.selectedteamMembersCopy[i].active.quarterly, name: $scope.selectedteamMembersCopy[i].name });

                    $scope.PlacedCandidatesWeekly.push({ list: $scope.selectedteamMembersCopy[i].placed.weekly, name: $scope.selectedteamMembersCopy[i].name });
                    $scope.PlacedCandidatesMonthly.push({ list: $scope.selectedteamMembersCopy[i].placed.monthly, name: $scope.selectedteamMembersCopy[i].name });
                    $scope.PlacedCandidatesQuarterly.push({ list: $scope.selectedteamMembersCopy[i].placed.quarterly, name: $scope.selectedteamMembersCopy[i].name });

                    $scope.ConvertedCandidatesWeekly.push({ list: $scope.selectedteamMembersCopy[i].converted.weekly, name: $scope.selectedteamMembersCopy[i].name });
                    $scope.ConvertedCandidatesMonthly.push({ list: $scope.selectedteamMembersCopy[i].converted.monthly, name: $scope.selectedteamMembersCopy[i].name });
                    $scope.ConvertedCandidatesQuarterly.push({ list: $scope.selectedteamMembersCopy[i].converted.quarterly, name: $scope.selectedteamMembersCopy[i].name });

                    $scope.PassiveConversionPerformanceWeekly.push({ list: $scope.selectedteamMembersCopy[i].passiveConversion.weekly, name: $scope.selectedteamMembersCopy[i].name });
                    $scope.PassiveConversionPerformanceMonthly.push({ list: $scope.selectedteamMembersCopy[i].passiveConversion.monthly, name: $scope.selectedteamMembersCopy[i].name });
                    $scope.PassiveConversionPerformanceQuarterly.push({ list: $scope.selectedteamMembersCopy[i].passiveConversion.quarterly, name: $scope.selectedteamMembersCopy[i].name });

                    $scope.TimeToSourcePerformanceWeekly.push({ list: $scope.selectedteamMembersCopy[i].timeToSource.weekly, name: $scope.selectedteamMembersCopy[i].name });
                    $scope.TimeToSourcePerformanceMonthly.push({ list: $scope.selectedteamMembersCopy[i].timeToSource.monthly, name: $scope.selectedteamMembersCopy[i].name });
                    $scope.TimeToSourcePerformanceQuarterly.push({ list: $scope.selectedteamMembersCopy[i].timeToSource.quarterly, name: $scope.selectedteamMembersCopy[i].name });

                    $scope.TimeToConvertPerformanceWeekly.push({ list: $scope.selectedteamMembersCopy[i].timeToConvert.weekly, name: $scope.selectedteamMembersCopy[i].name });
                    $scope.TimeToConvertPerformanceMonthly.push({ list: $scope.selectedteamMembersCopy[i].timeToConvert.monthly, name: $scope.selectedteamMembersCopy[i].name });
                    $scope.TimeToConvertPerformanceQuarterly.push({ list: $scope.selectedteamMembersCopy[i].timeToConvert.quarterly, name: $scope.selectedteamMembersCopy[i].name });

                    $scope.TimeToFillPerformanceWeekly.push({ list: $scope.selectedteamMembersCopy[i].timeToFill.weekly, name: $scope.selectedteamMembersCopy[i].name });
                    $scope.TimeToFillPerformanceMonthly.push({ list: $scope.selectedteamMembersCopy[i].timeToFill.monthly, name: $scope.selectedteamMembersCopy[i].name });
                    $scope.TimeToFillPerformanceQuarterly.push({ list: $scope.selectedteamMembersCopy[i].timeToFill.quarterly, name: $scope.selectedteamMembersCopy[i].name });

                    $scope.TimeToPlacePerformanceWeekly.push({ list: $scope.selectedteamMembersCopy[i].timeToPlace.weekly, name: $scope.selectedteamMembersCopy[i].name });
                    $scope.TimeToPlacePerformanceMonthly.push({ list: $scope.selectedteamMembersCopy[i].timeToPlace.monthly, name: $scope.selectedteamMembersCopy[i].name });
                    $scope.TimeToPlacePerformanceQuarterly.push({ list: $scope.selectedteamMembersCopy[i].timeToPlace.quarterly, name: $scope.selectedteamMembersCopy[i].name });
                }


                if ($scope.showActivePerformanceWeeklyGraph) {
                    $scope.plotFlotGraph('active-performance-graph-bar', 'allLines', $scope.ActivePerformanceWeekly, 'weekly', 'Number of candidates');
                }
                if ($scope.showActivePerformanceMonthlyGraph) {
                    $scope.plotFlotGraph('active-performance-graph-bar', 'allLines', $scope.ActivePerformanceMonthly, 'monthly', 'Number of candidates');
                }
                if ($scope.showActivePerformanceQuarterlyGraph) {
                    $scope.plotFlotGraph('active-performance-graph-bar', 'allLines', $scope.ActivePerformanceQuarterly, 'quarterly', 'Number of candidates');
                }

                if ($scope.showPlacedCandidatesWeeklyGraph) {
                    $scope.plotFlotGraph('placed-candidates-graph-bar', 'allLines', $scope.PlacedCandidatesWeekly, 'weekly', 'Number of candidates');
                }
                if ($scope.showPlacedCandidatesMonthlyGraph) {
                    $scope.plotFlotGraph('placed-candidates-graph-bar', 'allLines', $scope.PlacedCandidatesMonthly, 'monthly', 'Number of candidates');
                }
                if ($scope.showPlacedCandidatesQuarterlyGraph) {
                    $scope.plotFlotGraph('placed-candidates-graph-bar', 'allLines', $scope.PlacedCandidatesQuarterly, 'quarterly', 'Number of candidates');
                }

                if ($scope.showConvertedCandidatesWeeklyGraph) {
                    $scope.plotFlotGraph('converted-candidates-graph-bar', 'allLines', $scope.ConvertedCandidatesWeekly, 'weekly', 'Number of candidates');
                }
                if ($scope.showConvertedCandidatesMonthlyGraph) {
                    $scope.plotFlotGraph('converted-candidates-graph-bar', 'allLines', $scope.ConvertedCandidatesMonthly, 'monthly', 'Number of candidates');
                }
                if ($scope.showConvertedCandidatesQuarterlyGraph) {
                    $scope.plotFlotGraph('converted-candidates-graph-bar', 'allLines', $scope.ConvertedCandidatesQuarterly, 'quarterly', 'Number of candidates');
                }

                if ($scope.showPassiveConversionPerformanceWeeklyGraph) {
                    $scope.plotFlotGraph('passive-conversion-performance-graph-bar', 'allLines', $scope.PassiveConversionPerformanceWeekly, 'weekly', 'Number of candidates');
                }
                if ($scope.showPassiveConversionPerformanceMonthlyGraph) {
                    $scope.plotFlotGraph('passive-conversion-performance-graph-bar', 'allLines', $scope.PassiveConversionPerformanceMonthly, 'monthly', 'Number of candidates');
                }
                if ($scope.showPassiveConversionPerformanceQuarterlyGraph) {
                    $scope.plotFlotGraph('passive-conversion-performance-graph-bar', 'allLines', $scope.PassiveConversionPerformanceQuarterly, 'quarterly', 'Number of candidates');
                }

                if ($scope.showTimeToSourcePerformanceWeeklyGraph) {
                    $scope.plotFlotGraph('time-to-source-performance-graph-bar', 'allBars', $scope.TimeToSourcePerformanceWeekly, 'weekly', 'Number of candidates');
                }
                if ($scope.showTimeToSourcePerformanceMonthlyGraph) {
                    $scope.plotFlotGraph('time-to-source-performance-graph-bar', 'allBars', $scope.TimeToSourcePerformanceMonthly, 'monthly', 'Number of candidates');
                }
                if ($scope.showTimeToSourcePerformanceQuarterlyGraph) {
                    $scope.plotFlotGraph('time-to-source-performance-graph-bar', 'allBars', $scope.TimeToSourcePerformanceQuarterly, 'quarterly', 'Number of candidates');
                }

                if ($scope.showTimeToConvertPerformanceWeeklyGraph) {
                    $scope.plotFlotGraph('time-to-convert-performance-graph-bar', 'allBars', $scope.TimeToConvertPerformanceWeekly, 'weekly', 'Number of candidates');
                }
                if ($scope.showTimeToConvertPerformanceMonthlyGraph) {
                    $scope.plotFlotGraph('time-to-convert-performance-graph-bar', 'allBars', $scope.TimeToConvertPerformanceMonthly, 'monthly', 'Number of candidates');
                }
                if ($scope.showTimeToConvertPerformanceQuarterlyGraph) {
                    $scope.plotFlotGraph('time-to-convert-performance-graph-bar', 'allBars', $scope.TimeToConvertPerformanceQuarterly, 'quarterly', 'Number of candidates');
                }

                if ($scope.showTimeToFillPerformanceWeeklyGraph) {
                    $scope.plotFlotGraph('time-to-fill-performance-graph-bar', 'allBars', $scope.TimeToFillPerformanceWeekly, 'weekly', 'Number of candidates');
                }
                if ($scope.showTimeToFillPerformanceMonthlyGraph) {
                    $scope.plotFlotGraph('time-to-fill-performance-graph-bar', 'allBars', $scope.TimeToFillPerformanceMonthly, 'monthly', 'Number of candidates');
                }
                if ($scope.showTimeToFillPerformanceQuarterlyGraph) {
                    $scope.plotFlotGraph('time-to-fill-performance-graph-bar', 'allBars', $scope.TimeToFillPerformanceQuarterly, 'quarterly', 'Number of candidates');
                }

                if ($scope.showTimeToPlacePerformanceWeeklyGraph) {
                    $scope.plotFlotGraph('time-to-place-performance-graph-bar', 'allBars', $scope.TimeToPlacePerformanceWeekly, 'weekly', 'Number of candidates');
                }
                if ($scope.showTimeToPlacePerformanceMonthlyGraph) {
                    $scope.plotFlotGraph('time-to-place-performance-graph-bar', 'allBars', $scope.TimeToPlacePerformanceMonthly, 'monthly', 'Number of candidates');
                }
                if ($scope.showTimeToPlacePerformanceQuarterlyGraph) {
                    $scope.plotFlotGraph('time-to-place-performance-graph-bar', 'allBars', $scope.TimeToPlacePerformanceQuarterly, 'quarterly', 'Number of candidates');
                }


            }

            $scope.toggleTimeToConvertPerformanceGraph = function (type) {
                if (type === 'weekly') {
                    $scope.showTimeToConvertPerformanceWeeklyGraph = true;
                    $scope.showTimeToConvertPerformanceMonthlyGraph = false;
                    $scope.showTimeToConvertPerformanceQuarterlyGraph = false;
                } else if (type === 'monthly') {
                    $scope.showTimeToConvertPerformanceWeeklyGraph = false;
                    $scope.showTimeToConvertPerformanceMonthlyGraph = true;
                    $scope.showTimeToConvertPerformanceQuarterlyGraph = false;
                } else if (type === 'quarterly') {
                    $scope.showTimeToConvertPerformanceWeeklyGraph = false;
                    $scope.showTimeToConvertPerformanceMonthlyGraph = false;
                    $scope.showTimeToConvertPerformanceQuarterlyGraph = true;
                } else {
                    //
                }
                $scope.renderTeamMembersMeasureTypesGraph();
            }
            $scope.toggleTimeToPlacePerformanceGraph = function (type) {
                if (type === 'weekly') {
                    $scope.showTimeToPlacePerformanceWeeklyGraph = true;
                    $scope.showTimeToPlacePerformanceMonthlyGraph = false;
                    $scope.showTimeToPlacePerformanceQuarterlyGraph = false;
                } else if (type === 'monthly') {
                    $scope.showTimeToPlacePerformanceWeeklyGraph = false;
                    $scope.showTimeToPlacePerformanceMonthlyGraph = true;
                    $scope.showTimeToPlacePerformanceQuarterlyGraph = false;
                } else if (type === 'quarterly') {
                    $scope.showTimeToPlacePerformanceWeeklyGraph = false;
                    $scope.showTimeToPlacePerformanceMonthlyGraph = false;
                    $scope.showTimeToPlacePerformanceQuarterlyGraph = true;
                } else {
                    //
                }
                $scope.renderTeamMembersMeasureTypesGraph();
            }
            $scope.toggleTimeToSourcePerformanceGraph = function (type) {
                if (type === 'weekly') {
                    $scope.showTimeToSourcePerformanceWeeklyGraph = true;
                    $scope.showTimeToSourcePerformanceMonthlyGraph = false;
                    $scope.showTimeToSourcePerformanceQuarterlyGraph = false;
                } else if (type === 'monthly') {
                    $scope.showTimeToSourcePerformanceWeeklyGraph = false;
                    $scope.showTimeToSourcePerformanceMonthlyGraph = true;
                    $scope.showTimeToSourcePerformanceQuarterlyGraph = false;
                } else if (type === 'quarterly') {
                    $scope.showTimeToSourcePerformanceWeeklyGraph = false;
                    $scope.showTimeToSourcePerformanceMonthlyGraph = false;
                    $scope.showTimeToSourcePerformanceQuarterlyGraph = true;
                } else {
                    //
                }
                $scope.renderTeamMembersMeasureTypesGraph();
            }
            $scope.toggleTimeToFillPerformanceGraph = function (type) {
                if (type === 'weekly') {
                    $scope.showTimeToFillPerformanceWeeklyGraph = true;
                    $scope.showTimeToFillPerformanceMonthlyGraph = false;
                    $scope.showTimeToFillPerformanceQuarterlyGraph = false;
                } else if (type === 'monthly') {
                    $scope.showTimeToFillPerformanceWeeklyGraph = false;
                    $scope.showTimeToFillPerformanceMonthlyGraph = true;
                    $scope.showTimeToFillPerformanceQuarterlyGraph = false;
                } else if (type === 'quarterly') {
                    $scope.showTimeToFillPerformanceWeeklyGraph = false;
                    $scope.showTimeToFillPerformanceMonthlyGraph = false;
                    $scope.showTimeToFillPerformanceQuarterlyGraph = true;
                } else {
                    //
                }
                $scope.renderTeamMembersMeasureTypesGraph();
            }

            $scope.toggleActivePerformanceGraph = function (type) {
                if (type === 'weekly') {
                    $scope.showActivePerformanceWeeklyGraph = true;
                    $scope.showActivePerformanceMonthlyGraph = false;
                    $scope.showActivePerformanceQuarterlyGraph = false;
                } else if (type === 'monthly') {
                    $scope.showActivePerformanceWeeklyGraph = false;
                    $scope.showActivePerformanceMonthlyGraph = true;
                    $scope.showActivePerformanceQuarterlyGraph = false;

                } else if (type === 'quarterly') {
                    $scope.showActivePerformanceWeeklyGraph = false;
                    $scope.showActivePerformanceMonthlyGraph = false;
                    $scope.showActivePerformanceQuarterlyGraph = true;
                } else {
                    //
                }
                $scope.renderTeamMembersMeasureTypesGraph();
            }

            $scope.togglePlacedCandidatesGraph = function (type) {
                if (type === 'weekly') {
                    $scope.showPlacedCandidatesWeeklyGraph = true;
                    $scope.showPlacedCandidatesMonthlyGraph = false;
                    $scope.showPlacedCandidatesQuarterlyGraph = false;
                } else if (type === 'monthly') {
                    $scope.showPlacedCandidatesWeeklyGraph = false;
                    $scope.showPlacedCandidatesMonthlyGraph = true;
                    $scope.showPlacedCandidatesQuarterlyGraph = false;
                } else if (type === 'quarterly') {
                    $scope.showPlacedCandidatesWeeklyGraph = false;
                    $scope.showPlacedCandidatesMonthlyGraph = false;
                    $scope.showPlacedCandidatesQuarterlyGraph = true;
                } else {
                    //
                }
                $scope.renderTeamMembersMeasureTypesGraph();
            }

            $scope.toggleConvertedCandidatesGraph = function (type) {
                if (type === 'weekly') {
                    $scope.showConvertedCandidatesWeeklyGraph = true;
                    $scope.showConvertedCandidatesMonthlyGraph = false;
                    $scope.showConvertedCandidatesQuarterlyGraph = false;
                } else if (type === 'monthly') {
                    $scope.showConvertedCandidatesWeeklyGraph = false;
                    $scope.showConvertedCandidatesMonthlyGraph = true;
                    $scope.showConvertedCandidatesQuarterlyGraph = false;
                } else if (type === 'quarterly') {
                    $scope.showConvertedCandidatesWeeklyGraph = false;
                    $scope.showConvertedCandidatesMonthlyGraph = false;
                    $scope.showConvertedCandidatesQuarterlyGraph = true;
                } else {
                    //
                }
                $scope.renderTeamMembersMeasureTypesGraph();
            }

            $scope.togglePassiveConversionPerformanceGraph = function (type) {
                if (type === 'weekly') {
                    $scope.showPassiveConversionPerformanceWeeklyGraph = true;
                    $scope.showPassiveConversionPerformanceMonthlyGraph = false;
                    $scope.showPassiveConversionPerformanceQuarterlyGraph = false;
                } else if (type === 'monthly') {
                    $scope.showPassiveConversionPerformanceWeeklyGraph = false;
                    $scope.showPassiveConversionPerformanceMonthlyGraph = true;
                    $scope.showPassiveConversionPerformanceQuarterlyGraph = false;
                } else if (type === 'quarterly') {
                    $scope.showPassiveConversionPerformanceWeeklyGraph = false;
                    $scope.showPassiveConversionPerformanceMonthlyGraph = false;
                    $scope.showPassiveConversionPerformanceQuarterlyGraph = true;
                } else {
                    //
                }
                $scope.renderTeamMembersMeasureTypesGraph();
            }

            $scope.toggleTeamMemberGraphType = function (type) {
                if (type === 'measure') {
                    $scope.showTeamMemberGraphByMeasure = true;
                } else {
                    $scope.showTeamMemberGraphByMeasure = false;
                }
            }

            $scope.getGraphData = function (GraphData) {
                $http.get("data/ReportByTeamMemberList.json")
                    .success(function (data) {
                        $scope.measureteamMembersList = data;
                        $scope.selectedmeasureteamMembersList = $scope.measureteamMembersList;
                        $scope.renderTeamMembersMeasureTypesGraph();
                    });
            }
            $scope.getFilterList = function () {
                $http.get("data/requisitionReportList.json")
                    .success(function (data) {
                        $scope.requisitionfilters = data;
                        $scope.clientlist = $scope.requisitionfilters[0].clientList;
                        console.log("requisitionList " + angular.toJson($scope.requisitionList))
                        $scope.bulist = $scope.requisitionfilters[0].bulist;
                        console.log("requisitionList " + angular.toJson($scope.requisitionList))
                        $scope.recruitersList = $scope.requisitionfilters[0].recruiterList;
                        console.log("requisitionList " + angular.toJson($scope.requisitionList))
                        $scope.RequisitonRoles = $scope.requisitionfilters[0].Rolelist;
                        console.log("requisitionList " + angular.toJson($scope.requisitionList))
                        $scope.getGraphData($scope.requisitionfilters);

                    });
            }
            $scope.getFilterList();

            //pdf download
            $scope.download = function () {
                html2canvas(document.getElementById('content'), {
                    onrendered: function (canvas) {
                        var data = canvas.toDataURL();
                        var docDefinition = {
                            content: [{
                                image: data,
                                width: 500,
                            }]
                        };
                        pdfMake.createPdf(docDefinition).download("performance_by_measures_report.pdf");
                    }
                });
            }

        }]);


