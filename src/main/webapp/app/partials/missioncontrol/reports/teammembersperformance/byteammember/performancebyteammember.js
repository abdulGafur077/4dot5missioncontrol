/**
 * All the Functionalities Related to Reports
 * Module is included here
 */

var performancebyteammembermodule = angular.module('4dot5.missioncontrolmodule.reportsmodule.teammemberperformancemodule.performancebyteammembermodule',
    []);

performancebyteammembermodule.constant('PERFORMANCEBYTEAMMEMBERCONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.reports.teammembersperformance.performancebyteammember',
        URL: '/performancebyteammember',
        CONTROLLER: 'PerformanceByTeamMemberController',
        TEMPLATEURL: 'app/partials/missioncontrol/reports/teammembersperformance/byteammember/performancebyteammember.html',
    }
});

performancebyteammembermodule.config(
    ['$stateProvider',
        'PERFORMANCEBYTEAMMEMBERCONSTANTS',
        function ($stateProvider, PERFORMANCEBYTEAMMEMBERCONSTANTS) {
            $stateProvider.state(PERFORMANCEBYTEAMMEMBERCONSTANTS.CONFIG.STATE, {
                url: PERFORMANCEBYTEAMMEMBERCONSTANTS.CONFIG.URL,
                templateUrl: PERFORMANCEBYTEAMMEMBERCONSTANTS.CONFIG.TEMPLATEURL,
                controller: PERFORMANCEBYTEAMMEMBERCONSTANTS.CONFIG.CONTROLLER,
                data: {
                    requireLogin: true
                },
                params: {
                    reportType: null
                }
            });
        }
    ]);


performancebyteammembermodule.controller('PerformanceByTeamMemberController',
    ['$scope',
        '$state',
        '$window',
        '$location',
        '$stateParams',
        '$rootScope',
        '$timeout',
        '$http',
        'StorageService',
        'genericService',
        'PERFORMANCEBYTEAMMEMBERCONSTANTS',
        function ($scope, $state, $window, $location, $stateParams, $rootScope, $timeout, $http, StorageService, genericService, PERFORMANCEBYTEAMMEMBERCONSTANTS) {
            $scope.PerformanceByMeasure = {};
            //Dropdowns
            $scope.RequisitonRoles = ['All','Requisition role 1', 'Requisition role 2', 'Requisition role 3', 'Requisition role 4', 'Requisition role 5', 'Requisition role 6'];
            $scope.PerformanceByMeasure.companyType = 'Client';
            $scope.PerformanceByMeasure.requisitonRole = 'Requisition role 1';
            $scope.reportTypeLabel = 'Team Member Performance by Team Member';
            $scope.FilterParam={
                SelectedClientBu : [],
                SelectedRole : 'All',
                SelectedMemmberList : []
            }

            function getMonday(d) {
                d = new Date(d);
                var day = d.getDay(),
                    diff = d.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday
                //d.setDate(diff);
                return new Date(d.setDate(diff));
            }

            function getLastMondayTimeBySubtractingWeeksFromNow(weeks) {
                var d = new Date();
                d.setHours(0);
                d.setMinutes(0);
                return getMonday(new Date(d.getTime() - (weeks * 7 * 24 * 60 * 60 * 1000)).getTime()).getTime();
            }


            function getQuarter(d) {
                d = d || new Date();
                return m = Math.floor(d.getMonth() / 3);
                //return m > 4? m - 4 : m;
            }

            function getLastMonthOfQuarter(quarter) {
                return (quarter * 3) + 2;
            }

            function getTimeBySubtractingMonthsFromNow(months) {
                var d = new Date();
                return new Date(d.setMonth(d.getMonth() - months)).getTime();
            }

            function getTimeBySubtractingQuartersFromNow(quarters) {
                var d = new Date();
                d.setMonth(d.getMonth() - (quarters * 3));
                return new Date(d.setMonth(getLastMonthOfQuarter(getQuarter(d)))).getTime();
            }


            $scope.getFilterData= function(){
                 $http.get("data/ReportByTeamMemberList.json")
                    .success(function (data) {
                         $scope.bulist = [
                            {
                                "id": 1,
                                "name": "BL 1",
                            },
                            {
                                "id": 2,
                                "name": "BL 2",
                            },
                            {
                                "id": 3,
                                "name": "BL 3",
                            }

                        ];
                         $scope.clientlist = [
                            {
                                "id": 1,
                                "name": "CL 1",
                            },
                            {
                                "id": 2,
                                "name": "CL 2",
                            },
                            {
                                "id": 3,
                                "name": "CL 3",
                            }

                        ];

                        $scope.teamMembersList = data;
                        $scope.selectedteamMembersList = $scope.teamMembersList;
                        $timeout(function () {
                            $scope.drawGraph();
                        }, 100)
                    });
                    if($rootScope.userDetails.company.companyType == 'staffing')
                        $scope.FilterParam.SelectedClientBu = $scope.clientlist;
                    else
                        $scope.FilterParam.SelectedClientBu = $scope.bulist;

                    $scope.FilterParam.SelectedMemmberList = data;
            }

             $scope.getFilterData();
           
           //set bulist in filter param
            $scope.bu = function (selectedbu) {
                console.log("selected bu " + angular.toJson(selectedbu));
                $scope.FilterParam.SelectedClientBu = selectedbu;
            }
           
           //set clientlist in filter param
            $scope.client = function (selectedclient) {
                console.log("selected client " + angular.toJson(selectedclient));
                $scope.FilterParam.SelectedClientBu = selectedclient;
            }

            //set clientlist in filter param
            $scope.setSelectedMember = function () {
                //console.log("selected client " + angular.toJson($scope.selectedteamMembersList));
                $scope.FilterParam.SelectedMemmberList = $scope.selectedteamMembersList;
            }
            //get reports through filter params
            $scope.getFilteredReportData = function(){
                 console.log("selected Filter " + angular.toJson( $scope.FilterParam));
            }
            $scope.getFilteredReportData();

            //This function is for teamByTeamMembers graphs
            function plotFlotGraph(containerId, graph1Data, graph1Label, graph2Data, graph2Label, graph3Data, graph3Label, graph4Data, graph4Label, bar1Data, bar1Label, bar2Data, bar2Label, bar3Data, bar3Label, bar4Data, bar4Label, timeType) {
                if ($('#' + containerId).length) {
                    var timeToConvertCandidatesData = bar1Data;
                    var timeToConvertCandidates = [];

                    var timeToPlaceCandidatesData = bar2Data;
                    var timeToPlaceCandidates = [];

                    var timeToSourceCandidatesData = bar3Data;
                    var timeToSourceCandidates = [];

                    var timeToFillCandidatesData = bar4Data;
                    var timeToFillCandidates = [];

                    var activeCandidatesData = graph1Data;
                    var activeCandidates = [];

                    var passiveCandidatesData = graph2Data;
                    var passiveCandidates = [];

                    var placedCandidatesData = graph3Data;
                    var placedCandidates = [];

                    var convertedCandidatesData = graph4Data;
                    var convertedCandidates = [];

                    var ticks = [];

                    for (var i = 0; i < timeToPlaceCandidatesData.length; i++) {
                        var arr = [];
                        var tempTime = '';
                        if (timeType === 'weekly') {
                            tempTime = getLastMondayTimeBySubtractingWeeksFromNow(timeToPlaceCandidatesData.length - i - 1);
                        } else if (timeType === 'monthly') {
                            tempTime = getTimeBySubtractingMonthsFromNow(timeToPlaceCandidatesData.length - i - 1);
                        } else if (timeType === 'quarterly') {
                            tempTime = getTimeBySubtractingQuartersFromNow(timeToPlaceCandidatesData.length - i - 1);
                        } else {
                            //
                        }

                        arr.push(tempTime);
                        arr.push(timeToConvertCandidatesData[i]);
                        timeToConvertCandidates.push(arr);

                        arr = [];
                        arr.push(tempTime);
                        arr.push(timeToPlaceCandidatesData[i]);
                        timeToPlaceCandidates.push(arr);

                        arr = [];
                        arr.push(tempTime);
                        arr.push(timeToSourceCandidatesData[i]);
                        timeToSourceCandidates.push(arr);

                        arr = [];
                        arr.push(tempTime);
                        arr.push(timeToFillCandidatesData[i]);
                        timeToFillCandidates.push(arr);

                        arr = [];
                        arr.push(tempTime);
                        arr.push(activeCandidatesData[i]);
                        activeCandidates.push(arr);

                        arr = [];
                        arr.push(tempTime);
                        arr.push(passiveCandidatesData[i]);
                        passiveCandidates.push(arr);

                        arr = [];
                        arr.push(tempTime);
                        arr.push(placedCandidatesData[i]);
                        placedCandidates.push(arr);

                        arr = [];
                        arr.push(tempTime);
                        arr.push(convertedCandidatesData[i]);
                        convertedCandidates.push(arr);

                        ticks.push(tempTime);
                    }

                    var xAxisMin = '';
                    var xAxisMax = '';
                    var xAsisBarWidth = '';
                    var xAxisLabel = '';
                    var xAxisTimeFormat = '';

                    if (timeType === 'weekly') {
                        xAxisMin = (new Date()).getTime() - ((timeToPlaceCandidatesData.length) * 7 * 24 * 60 * 60 * 1000);
                        xAxisMax = (new Date()).getTime() + (7 * 24 * 60 * 60 * 1000);
                        xAxisBarWidth = 24 * 60 * 60 * 800;
                        xAxisLabel = 'Past six weeks';
                        xAxisTimeFormat = "%m/%d";
                    } else if (timeType === 'monthly') {
                        xAxisMin = (new Date()).getTime() - ((timeToPlaceCandidatesData.length) * 31 * 24 * 60 * 60 * 1000);
                        xAxisMax = (new Date()).getTime() + (31 * 24 * 60 * 60 * 1000);
                        xAxisBarWidth = 24 * 60 * 60 * 3000;
                        xAxisLabel = 'Past six months';
                        xAxisTimeFormat = "%b '%y";
                    } else if (timeType === 'quarterly') {
                        xAxisMin = (new Date()).getTime() - (3 * (timeToPlaceCandidatesData.length) * 31 * 24 * 60 * 60 * 1000);
                        xAxisMax = (new Date()).getTime() + (6 * 31 * 24 * 60 * 60 * 1000);
                        xAxisBarWidth = 24 * 60 * 60 * 8000;
                        xAxisLabel = 'Past six quarters';
                        xAxisTimeFormat = "%b '%y";
                    } else {
                        //
                    }


                    var series = new Array();

                    series.push({
                        data: timeToConvertCandidates,
                        color: '#a2d4ee',
                        bars: {
                            show: true,
                            barWidth: xAxisBarWidth,
                            lineWidth: 1,
                            order: 1,
                            fill: 1,
                            align: 'center'
                        },
                        label: bar1Label,
                        yaxis: 1
                    });
                    series.push({
                        data: timeToPlaceCandidates,
                        color: '#dfd188',
                        bars: {
                            show: true,
                            barWidth: xAxisBarWidth,
                            lineWidth: 1,
                            order: 2,
                            fill: 1,
                            align: 'center'
                        },
                        label: bar2Label,
                        yaxis: 1
                    });
                    series.push({
                        data: timeToSourceCandidates,
                        color: '#f79981',
                        bars: {
                            show: true,
                            barWidth: xAxisBarWidth,
                            lineWidth: 1,
                            order: 3,
                            fill: 1,
                            align: 'center'
                        },
                        label: bar3Label,
                        yaxis: 1
                    });
                    series.push({
                        data: timeToFillCandidates,
                        color: '#f781bf',
                        bars: {
                            show: true,
                            barWidth: xAxisBarWidth,
                            lineWidth: 1,
                            order: 4,
                            fill: 1,
                            align: 'center'
                        },
                        label: bar4Label,
                        yaxis: 1
                    });
                    series.push({
                        data: activeCandidates,
                        color: '#8bc34a',
                        lines: {
                            show: true,
                            lineWidth: 3,
                        },
                        points: {
                            fillColor: '#ffffff',
                            pointWidth: 1,
                            show: true
                        },
                        label: graph1Label,
                        yaxis: 2
                    });
                    series.push({
                        data: passiveCandidates,
                        color: '#e84e40',
                        lines: {
                            show: true,
                            lineWidth: 3,
                        },
                        points: {
                            fillColor: '#ffffff',
                            pointWidth: 1,
                            show: true
                        },
                        label: graph2Label,
                        yaxis: 2
                    });
                    series.push({
                        data: placedCandidates,
                        color: '#ff7f00',
                        lines: {
                            show: true,
                            lineWidth: 3,
                        },
                        points: {
                            fillColor: '#ffffff',
                            pointWidth: 1,
                            show: true
                        },
                        label: graph3Label,
                        yaxis: 2
                    });
                    series.push({
                        data: convertedCandidates,
                        color: '#984ea3',
                        lines: {
                            show: true,
                            lineWidth: 3,
                        },
                        points: {
                            fillColor: '#ffffff',
                            pointWidth: 1,
                            show: true
                        },
                        label: graph4Label,
                        yaxis: 2
                    });

                    //plotiing graph

                    $.plot("#" + containerId, series, {
                        colors: ['#03a9f4', '#f1c40f', '#2ecc71', '#3498db', '#9b59b6', '#95a5a6'],
                        grid: {
                            tickColor: "#f2f2f2",
                            borderWidth: 0,
                            hoverable: true,
                            clickable: true
                        },
                        legend: {
                            noColumns: 1,
                            labelBoxBorderColor: "#000000",
                            position: "ne"
                        },
                        shadowSize: 0,
                        xaxis: {
                            mode: "time",
                            ticks: ticks,
                            timezone: "browser",
                            //tickSize: [1, "day"],
                            timeformat: xAxisTimeFormat,
                            //tickLength: 0,
                            axisLabel: xAxisLabel,
                            min: xAxisMin,
                            max: xAxisMax,
                            axisLabelUseCanvas: true,
                            axisLabelFontSizePixels: 12,
                            axisLabelFontFamily: 'Open Sans, sans-serif',
                            axisLabelPadding: 10
                        },
                        yaxes: [
                            {
                                // tickFormatter: function (val, axis) {
                                //     return val;
                                // },	
                                position: "left",
                                axisLabel: "Time (in days)",
                                axisLabelUseCanvas: true,
                                axisLabelFontSizePixels: 12,
                                axisLabelFontFamily: "Verdana, Arial, Helvetica, Tahoma, sans-serif",
                                axisLabelPadding: 5
                            },
                            {
                                position: "right",
                                axisLabel: "Number of candidates",
                                axisLabelUseCanvas: true,
                                axisLabelFontSizePixels: 12,
                                axisLabelFontFamily: "Verdana, Arial, Helvetica, Tahoma, sans-serif",
                                axisLabelPadding: 5
                            }
                        ]
                    });

                    var previousPoint = null;
                    $("#" + containerId).bind("plothover", function (event, pos, item) {
                        if (item) {
                            if (previousPoint != item.dataIndex) {

                                previousPoint = item.dataIndex;

                                $("#flot-tooltip").remove();
                                var x = item.datapoint[0],
                                    y = item.datapoint[1];

                                showTooltip(item.pageX, item.pageY, item.series.label, y);
                            }
                        }
                        else {
                            $("#flot-tooltip").remove();
                            previousPoint = [0, 0, 0];
                        }
                    });

                    function showTooltip(x, y, label, data) {
                        $('<div id="flot-tooltip">' + '<b>' + label + ': </b><i>' + data + '</i>' + '</div>').css({
                            top: y + 5,
                            left: x + 20
                        }).appendTo("body").fadeIn(200);
                    }
                };
            }


            $scope.drawGraph = function () {
                for (var i = 0; i < $scope.selectedteamMembersList.length; i++) {
                    if ($scope.selectedteamMembersList[i].weeklyshow) {
                        plotFlotGraph($scope.selectedteamMembersList[i].id + '-graph-bar', $scope.selectedteamMembersList[i].active.weekly, 'Active Canditates', $scope.selectedteamMembersList[i].passiveConversion.weekly, 'Passive Canditates', $scope.selectedteamMembersList[i].placed.weekly, 'Placed Canditates', $scope.selectedteamMembersList[i].converted.weekly, 'Converted Canditates', $scope.selectedteamMembersList[i].timeToConvert.weekly, 'Time to Convert', $scope.selectedteamMembersList[i].timeToPlace.weekly, 'Time To Place', $scope.selectedteamMembersList[i].timeToSource.weekly, 'Time To Source', $scope.selectedteamMembersList[i].timeToFill.weekly, 'Time To Fill', 'weekly');
                    }
                    if ($scope.selectedteamMembersList[i].monthlyshow) {
                        plotFlotGraph($scope.selectedteamMembersList[i].id + '-graph-bar', $scope.selectedteamMembersList[i].active.monthly, 'Active Canditates', $scope.selectedteamMembersList[i].passiveConversion.monthly, 'Passive Canditates', $scope.selectedteamMembersList[i].placed.monthly, 'Placed Canditates', $scope.selectedteamMembersList[i].converted.monthly, 'Converted Canditates', $scope.selectedteamMembersList[i].timeToConvert.monthly, 'Time to Convert', $scope.selectedteamMembersList[i].timeToPlace.monthly, 'Time To Place', $scope.selectedteamMembersList[i].timeToSource.monthly, 'Time To Source', $scope.selectedteamMembersList[i].timeToFill.monthly, 'Time To Fill', 'monthly');
                    }
                    if ($scope.selectedteamMembersList[i].quarterlyshow) {
                        plotFlotGraph($scope.selectedteamMembersList[i].id + '-graph-bar', $scope.selectedteamMembersList[i].active.quarterly, 'Active Canditates', $scope.selectedteamMembersList[i].passiveConversion.quarterly, 'Passive Canditates', $scope.selectedteamMembersList[i].placed.quarterly, 'Placed Canditates', $scope.selectedteamMembersList[i].converted.quarterly, 'Converted Canditates', $scope.selectedteamMembersList[i].timeToConvert.quarterly, 'Time to Convert', $scope.selectedteamMembersList[i].timeToPlace.quarterly, 'Time To Place', $scope.selectedteamMembersList[i].timeToSource.quarterly, 'Time To Source', $scope.selectedteamMembersList[i].timeToFill.quarterly, 'Time To Fill', 'quarterly');
                    }
                }
            }

            $scope.toggleGraph = function (type, member) {
                for (var i = 0; i < $scope.selectedteamMembersList.length; i++) {
                    if (member === $scope.selectedteamMembersList[i].name) {
                        if (type === 'weekly') {
                            $scope.selectedteamMembersList[i].weeklyshow = true;
                            $scope.selectedteamMembersList[i].monthlyshow = false;
                            $scope.selectedteamMembersList[i].quarterlyshow = false;
                        } else if (type === 'monthly') {
                            $scope.selectedteamMembersList[i].weeklyshow = false;
                            $scope.selectedteamMembersList[i].monthlyshow = true;
                            $scope.selectedteamMembersList[i].quarterlyshow = false;
                        } else if (type === 'quarterly') {
                            $scope.selectedteamMembersList[i].weeklyshow = false;
                            $scope.selectedteamMembersList[i].monthlyshow = false;
                            $scope.selectedteamMembersList[i].quarterlyshow = true;
                        } else {
                            //
                        }
                    }

                }
                $scope.drawGraph();
            }
            // //get team member list
            // $scope.getMemberList = function () {
            //     $http.get("data/ReportByTeamMemberList.json")
            //         .success(function (data) {
            //             $scope.teamMembersList = data;
            //             $scope.selectedteamMembersList = $scope.teamMembersList;
            //             $timeout(function () {
            //                 $scope.drawGraph();
            //             }, 100)
            //         });
            // }
            // $scope.getMemberList();



            //pdf download
            $scope.download = function () {
                html2canvas(document.getElementById('content'), {
                    onrendered: function (canvas) {
                        var data = canvas.toDataURL();
                        var docDefinition = {
                            content: [{
                                image: data,
                                width: 500,
                            }]
                        };
                        pdfMake.createPdf(docDefinition).download("performance_by_teamember_report.pdf");
                    }
                });
            }


        }]);