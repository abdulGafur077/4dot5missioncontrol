/**
 * All the Functionalities Related to Reports
 * Module is included here
 */

var teammemberperformancemodule = angular.module('4dot5.missioncontrolmodule.reportsmodule.teammemberperformancemodule',
    ['4dot5.missioncontrolmodule.reportsmodule.teammemberperformancemodule.performancebyteammembermodule',
     '4dot5.missioncontrolmodule.reportsmodule.teammemberperformancemodule.performancebymeasuresmodule'
    ]);
     
teammemberperformancemodule.constant('TEAMMEMBERPERFORMANCECONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.reports.teammembersperformance',
        URL: '/teammembersperformance',
        CONTROLLER: 'TeamMemberPerformanceController',
        TEMPLATEURL: 'app/partials/missioncontrol/reports/teammembersperformance/teammemberperformance.html',
    }
});

teammemberperformancemodule.config(
    ['$stateProvider',
        'TEAMMEMBERPERFORMANCECONSTANTS',
        function ($stateProvider, TEAMMEMBERPERFORMANCECONSTANTS) {
            $stateProvider.state(TEAMMEMBERPERFORMANCECONSTANTS.CONFIG.STATE, {
                url: TEAMMEMBERPERFORMANCECONSTANTS.CONFIG.URL,
                templateUrl: TEAMMEMBERPERFORMANCECONSTANTS.CONFIG.TEMPLATEURL,
                controller: TEAMMEMBERPERFORMANCECONSTANTS.CONFIG.CONTROLLER,
                data: {
                    requireLogin: true
                },
                params: {
                    reportType: null
                }
            });
        }
    ]);


teammemberperformancemodule.controller('TeamMemberPerformanceController',
    ['$scope',
        '$state',
        '$window',
        '$location',
        '$stateParams',
        '$rootScope',
        '$http',
        'StorageService',
        'genericService',
        'TEAMMEMBERPERFORMANCECONSTANTS',
        function ($scope, $state, $window, $location, $stateParams, $rootScope, $http, StorageService, genericService, TEAMMEMBERPERFORMANCECONSTANTS) {
            console.log('TeamMemberPerformanceController');
        }
    ]);
