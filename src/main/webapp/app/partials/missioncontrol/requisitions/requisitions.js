/**
 * This module deals with Requisition management operations
 * 
 */

var requisitionsmodule = angular.module('4dot5.missioncontrolmodule.requisitionsmodule',
    []);

requisitionsmodule.constant('REQUISITIONSCONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.requisitions',
        URL: '/requisitions',
        CONTROLLER: 'RequisitionsController',
        TEMPLATEURL: 'app/partials/missioncontrol/requisitions/requisitions.html'
    },
    CONTROLLER: {
        GET_ALL_JOBS: 'api/job/userJobs',
        UPDATE_JOB_STATUS: 'api/job/status',
        GET_CLIENTS_RECRUITERS_BY_ROLE: 'api/job/clientandrecruitersbyroles',
        GET_CLIENT_ROLE_BY_RECRUITER: 'api/job/clientandrolebyrecruiters',
        GET_ROLE_RECRUITER_BY_CLIENT: 'api/job/roleandrecruitersbyclients'
    }
});

requisitionsmodule.config(
    ['$stateProvider',
        'REQUISITIONSCONSTANTS',
        function ($stateProvider, REQUISITIONSCONSTANTS) {
            $stateProvider.state(REQUISITIONSCONSTANTS.CONFIG.STATE, {
                url: REQUISITIONSCONSTANTS.CONFIG.URL,
                templateUrl: REQUISITIONSCONSTANTS.CONFIG.TEMPLATEURL,
                controller: REQUISITIONSCONSTANTS.CONFIG.CONTROLLER,
                resolve: {
                	RequisitionList: function ($rootScope, $stateParams, StorageService, genericService,jobService) {
                        var userId = $rootScope.userDetails.id;
                        var companyId = $rootScope.userDetails.company.companyId;
                        var filterRequisitionObject = {};
                        filterRequisitionObject.clientOrBU = [];
                        filterRequisitionObject.roleList = [];
                        filterRequisitionObject.requisitionNums = [];
                        if(!_.isNull($stateParams.requisitionStatesArray)){
                            filterRequisitionObject.requisitionStates = [];
                            angular.forEach($stateParams.requisitionStatesArray, function(val, key){
                                filterRequisitionObject.requisitionStates.push(angular.uppercase(val));
                            });
                        }
                        filterRequisitionObject.recruiters = [];
                        filterRequisitionObject.searchCheck = false;
                        filterRequisitionObject.size = 6;
                        filterRequisitionObject.pageNum = 1;
                        filterRequisitionObject.fromDate = null;
                        filterRequisitionObject.toDate = null;
                        filterRequisitionObject.sortColumn = "";
                        filterRequisitionObject.sortDirection = "";
                        filterRequisitionObject.isAssignedToMe = true;
                        filterRequisitionObject.isAssignedToOthers = true;
                        filterRequisitionObject.isUnassigned = true;
                        filterRequisitionObject.searchText = "";
                        return genericService.addObject(StorageService.get('baseurl') + REQUISITIONSCONSTANTS.CONTROLLER.GET_ALL_JOBS + '/' + userId + '/' +companyId, filterRequisitionObject);
                        //return genericService.getObjects("app-content/jsonobjects/requisitions-copy.json");
                    } 
                },
                data: {
                    requireLogin: true
                },
                params: {
                    requisitionStatesArray: null
                }
            });
        }
    ]);

 





requisitionsmodule.controller('RequisitionsController',
    ['$scope',
        '$state',
        '$timeout',
        '$interval',
        '$stateParams',
        '$uibModal',
        'StorageService',
        'requisitionService',
        'jobService',
        '$rootScope',
        '$http',
        '$compile',
        'RequisitionList',
        'genericService',
        'REQUISITIONSCONSTANTS',
        'MESSAGECONSTANTS',
        '$filter',
        'moment',
        'alertsAndNotificationsService',
            function ($scope, $state, $timeout, $interval, $stateParams, $uibModal, StorageService, requisitionService, jobService, $rootScope, $http,$compile, RequisitionList, genericService, REQUISITIONSCONSTANTS, MESSAGECONSTANTS, $filter, moment, alertsAndNotificationsService) {
            var ctrl = this;
            $scope.userId = $rootScope.userDetails.id;
            $scope.companyId = $rootScope.userDetails.company.companyId;
            // $scope.candidateInfoContainerId = null;
            // $scope.infoOfCandidateInContext = null;
            $scope.queryRequisitions = queryRequisitions;
            
            $scope.getAllClientsOrBu = getAllClientsOrBu;
            $scope.getAllStatusTypes = getAllStatusTypes;
            $scope.getAllRecruiters = getAllRecruiters;
            $scope.getAllActivityTypes = getAllActivityTypes;
            $scope.getReqNums = getReqNums;
            $scope.multiSelectClients = multiSelectClients;
            $scope.multiSelectRoles = multiSelectRoles;
            $scope.multiSelectStatus = multiSelectStatus;
            $scope.multiSelectRecruiters = multiSelectRecruiters;
            $scope.multiSelectReqNum = multiSelectReqNum;
            $scope.filterJobs = filterJobs;
            $scope.selectRemoveAll = selectRemoveAll;
            $scope.resetFilterData = resetFilterData;
            $scope.searchJobs = searchJobs;
            $scope.sortRequisitions = sortRequisitions;
            $scope.sortByDate = sortByDate;
            $scope.showAllActivity = showAllActivity;
            // $scope.setRequisitionInfo = setRequisitionInfo;
            // $scope.closeCandidateInfoRow = closeCandidateInfoRow;
            $scope.convertToBoolean = _convertToBoolean;
            var count = 2;

            function _initialize() {
                $scope.companyType = $rootScope.userDetails.company.companyType;
                $scope.userId = $rootScope.userDetails.id;
                $scope.companyId = $rootScope.userDetails.company.companyId;
                $scope.inputTextbox = false;
                $scope.showpopup = false;
                $scope.reqNumFlag = false;
                $scope.selectedClients = [];
                $scope.selectedRoles = [];
                $scope.selectedStatus = [];
                $scope.selectedRecruiters = [];
                $scope.selectedSortByList = [];
                $scope.selectedReqNum = [];
                $scope.requisitionNumbersList = [];
                $scope.sortByList = [];
                $scope.requisitionsContainer = null;
//                $scope.multiSelectDropdownSettings = {
//                    scrollableHeight: '200px',
//                    scrollable: true,
//                    enableSearch: true,
//                    displayProp: 'name',
//                    buttonClasses: 'multiSelect-button',
//                    idProperty: 'name',
//                    selectedToTop: true
//                };

                $scope.requisitionStatesEvents = {
                     onInitDone: function(item) {
                        _setFilterRequisitionStates(item);
                     },
                     onItemSelect: function(item) {
                        _setFilterRequisitionStates(item);
                     },
                     onItemDeselect: function(item) {
                        _setFilterRequisitionStates(item);
                     },
                     onSelectAll: function() {
                        _setFilterRequisitionStates();
                     },
                     onDeselectAll: function() {
                        _setFilterRequisitionStates();
                     }
                 };

                $scope.toggleInputBox = function () {
                    $scope.inputTextbox = !$scope.inputTextbox;
                };
                $scope.searchTerm = '';
                $scope.requisitions = [];
                $scope.filterRequisitionObject = {};
                $scope.filterRequisitionObject.clientOrBU = [];
                $scope.filterRequisitionObject.roleList = [];
                $scope.filterRequisitionObject.requisitionNums = [];
                $scope.filterRequisitionObject.recruiters = [];
                if(!_.isNull($stateParams.requisitionStatesArray)){
                    $scope.filterRequisitionObject.requisitionStates = [];
                    angular.forEach($stateParams.requisitionStatesArray, function(val, key){
                        $scope.filterRequisitionObject.requisitionStates.push(angular.uppercase(val));
                    });
                }
                $scope.filterRequisitionObject.searchCheck = false;
                $scope.filterRequisitionObject.size = 9;
                $scope.filterRequisitionObject.pageNum = 1;
                $scope.filterRequisitionObject.fromDate = "";
                $scope.filterRequisitionObject.toDate = "";
                $scope.filterRequisitionObject.sortColumn = "";
                $scope.filterRequisitionObject.sortDirection = "";
                $scope.filterRequisitionObject.isAssignedToMe = true;
                $scope.filterRequisitionObject.isAssignedToOthers = true;
                $scope.filterRequisitionObject.isUnassigned = true;
                $scope.selectedDateRange = "";
                $scope.options = {
                    barColor: '#03a9f4',
                    trackColor: '#f2f2f2',
                    scaleColor: false,
                    lineWidth: 8,
                    size: 130,
                    animate: 1500,
                    onStep: function (from, to, percent) {
                        $(this.el).find('.percent').text(Math.round(percent));
                    }
                };

                // get clients or bus
                $scope.getAllClientsOrBu();

                // get all job status types
                $scope.getAllStatusTypes();

                //get all recruiters
                $scope.getAllRecruiters();

                //get top 10 req nums
                $scope.getReqNums("all");

                //get all activity types
                $scope.getAllActivityTypes();

                $scope.requisitions = angular.copy(RequisitionList);

                _setRolesAndRecruiters(RequisitionList);
                $scope.requisitionsContainer = new requisitionsContainer();
                $scope.requisitionsContainer.requisitionsQueryFilter = angular.copy($scope.filterRequisitionObject);
            }

            //initialize the variables and call the default methods
            _initialize();

//            $timeout(function () {
//                $scope.selectedDateRange = '';
//            }, 2000);
            
             function queryRequisitions() {
                // clear any existing query requests
                // check for an existing promise and see if it has not been fulfilled yet. If it has not been fulfilled yet, cancel the promise.
                if(!(_.isNull($scope.requisitionsContainer.currentPromise)|| $scope.requisitionsContainer.currentPromise === undefined ) && $scope.requisitionsContainer.currentPromise.$$state.status == 0) {
                    genericService.cancelRequest($scope.requisitionsContainer.currentPromise);
                }
                // reset the initial value of requisitions exist flag to true
                $scope.requisitionsContainer.requisitionsExistFlag = true;
                // reset the page number to 1 as this is a new query
                $scope.requisitionsContainer.requisitionsQueryFilter.pageNum = 1;
                // clear the existing requisitions
                $scope.requisitionsContainer.requisitions = [];
                $scope.requisitionsContainer.disableInfiniteScroll = false;
                // wait for the cancel promise to be fulfilled, if it was called.
                $timeout(function () {
                    $scope.requisitionsContainer.getNext();
                }, 200);
            }           
            
function requisitionsContainer(){
    this.requisitions = [];
    this.busy = false;
    this.requisitionsExistFlag = true;
    this.currentPromise = null;
    this.disableInfiniteScroll = false;

    this.getNext = function () {
        var parent = this;
        if (this.busy) return;
        this.busy = true;

        if (this.requisitionsQueryFilter.pageNum == '') {
            this.requisitionsQueryFilter.pageNum = 1;
        }

        $scope.filterRequisitionObject.size = 3;
        this.currentPromise = jobService.searchJobsByText($scope.userId, $scope.companyId, $scope.searchTerm, this.requisitionsQueryFilter, function (data) {
            if (angular.isDefined(data) && data != 'user cancellation') {
                $scope.requisitionCount = data.totalCount;
                if (data.jobMinDetailsDtos.length > 0) {
                    angular.forEach(data.jobMinDetailsDtos, function (val, key) {
                        val.requisitionIndex = (parent.requisitions.length + 1);
                        val.requisitionNumber = val.id;
                        parent.requisitions.push(val);
                    });
                    if (data.jobMinDetailsDtos.length < parent.requisitionsQueryFilter.pageSize) {
                        parent.disableInfiniteScroll = true;
                    } else {
                        parent.requisitionsQueryFilter.pageNum++;
                        $scope.filterRequisitionObject.pageNum++;
                    }
                } else { 
                    parent.disableInfiniteScroll = true;
                }
                if (parent.requisitions.length > 0) {
                    parent.requisitionsExistFlag = true;
                } else {
                    parent.requisitionsExistFlag = false;
                }
            }
            parent.busy = false;

        }, function (error) {
            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
        });
    }
}

            function _convertToBoolean(value) {
                return value === "true" || value === true;
            }

            function _setRolesAndRecruiters(RequisitionList) {
                if(RequisitionList.length > 0){
                    $scope.fullRolesList = [];
                    $scope.fullRecruitersList = [];
                    angular.forEach($scope.requisitions, function (val, key) {

                        var roleObj = {"name": val.requisitionRole};
                        if (!_containsObject(roleObj, $scope.fullRolesList,"name")) {
                            $scope.fullRolesList.push(roleObj);
                        }

//                        val.recruiterObjectList = JSON.parse(val.recruiterObjectList);
//                        for (index in val.recruiterObjectList) {
//                            var recObj = val.recruiterObjectList[index];
//                            if (!_containsObject(recObj, $scope.fullRecruitersList,"name")) {
//                                $scope.fullRecruitersList.push(recObj);
//                            }
//                        }

                        $scope.fullRolesListCopy = angular.copy($scope.fullRolesList);
                        //$scope.fullRecruitersListCopy = angular.copy($scope.fullRecruitersList);
                        //show next status base
                    });
                }
            }

            function _containsObject(obj, list, key) {
               for (var index in list) {
                      var currentObject = list[index];
                      for (var prop in currentObject) {
                          if( prop === key && currentObject.hasOwnProperty(prop)
                          && currentObject[prop] === obj[prop] ){
                              return true;
                          }
                      }
                }
                return false;
            }

            function getAllClientsOrBu () {
                requisitionService.getAllClientsOrBu($rootScope.userDetails, function (data) {
                    $scope.fullClientsList = data;
                    $scope.fullClientsListCopy = angular.copy($scope.fullClientsList);
                }, function (error) {
                    if ($rootScope.isOnline) {
                        alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                    }
                });
            }

            function getAllStatusTypes () {
                jobService.getAllStatusTypes(function (data) {
                    $scope.fullStatusList = data;
                    $scope.fullStatusListCopy = angular.copy($scope.fullStatusList);
                    if(!_.isNull($stateParams.requisitionStatesArray)){
                        for (var i = 0; i < $scope.fullStatusList.length; i++) {
                            for (var j = 0; j < $stateParams.requisitionStatesArray.length; j++) {
                                if ($scope.fullStatusList[i].name == $stateParams.requisitionStatesArray[j]) {
                                    $scope.fullStatusList[i]['ticked'] = true;
                                    break;
                                }
                            }
                        }
                    }
                }, function (error) {
                    if ($rootScope.isOnline) {
                        alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                    }
                });
            }

            function getAllRecruiters () {
                jobService.getAllRecruiters($rootScope.userDetails, $rootScope.userDetails.company, function (data) {
                    var newRecruiterArray = [];
                    var recruiters = data;
                    for (i = 0; i < recruiters.length; i ++) {
                        var name;
                        if(recruiters[i].lastName !== null){
                            name = recruiters[i].firstName+" "+recruiters[i].lastName;
                        }else{
                            name = recruiters[i].firstName;
                        }
                        var recObj = {"name": name, "id": recruiters[i].userId};
                        if (!_containsObject(recObj, newRecruiterArray, "name")) {
                            newRecruiterArray.push(recObj);
                        }

                    }
                    $scope.fullRecruitersList = angular.copy(newRecruiterArray);
                    $scope.fullRecruitersListCopy = angular.copy($scope.fullRecruitersList);
                }, function (error) {
                    if ($rootScope.isOnline) {
                        alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                    }
                });
            }

            function getAllActivityTypes(){
                jobService.getActivityTypes(function (data) {
                    $scope.fullActivityList = data;
                    $scope.activityMap = [];
                    angular.forEach(data, function (val, key) {
                        $scope.activityMap[val.value] = val.name;
                    });
                }, function (error) {
                    if ($rootScope.isOnline) {
                        alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                    }
                });
            }

            function showAllActivity(req){
                $uibModal.open({
                    animation: false,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/partials/missioncontrol/requisitions/requisition-activity-modal.html',
                    controller: ['$uibModalInstance','jobId','role','requisitionNumber','clientName',function ($uibModalInstance, jobId, role, requisitionNumber, clientName) {
                        var vm = this;
                        vm.jobId = jobId;
                        vm.role = role;
                        vm.requisitionNumber = requisitionNumber;
                        vm.clientName = clientName;
                        vm.closeModal = function () {
                            $uibModalInstance.dismiss('cancel');
                        };
                    }],
                    controllerAs: 'activityModal',
                    size: 'lg',
                    resolve:{
                        jobId: function () {
                            return req.requisitionNumber;
                        },
                        role: function () {
                            return req.requisitionRole;
                        },
                        requisitionNumber: function(){
                            return req.requisitionActualNumber;
                        },
                        clientName: function () {
                            return req.companyName;
                        }
                    }
                });
            }

            function getReqNums(searchReqText) {
                if (searchReqText == "") {
                    searchReqText = "all";
                }
                $scope.searchReqText = searchReqText;
                jobService.searchByReqNum($scope.userId, $scope.companyId, $scope.searchReqText, function (data) {
                    _setReqNums(data);
                    if (searchReqText == "all") {
                       $scope.fullReqNumListCopy = angular.copy($scope.fullReqNumList);
                    }
                }, function (error) {
                    if ($rootScope.isOnline) {
                        alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                    }
                });
            }

            function _setReqNums(reqNums) {
                newReqNumsArray = [];
                $selectedReqNum = angular.copy($scope.selectedReqNum);

                for (i = 0; i < reqNums.length; i++){
                    var obj = {"name": reqNums[i]};
                    if (!_containsObject(obj, newReqNumsArray, "name")) {
                       newReqNumsArray.push(obj);
                    }

                }

                for (i = 0; i < newReqNumsArray.length; i++) {
                    for (j = 0; j < $selectedReqNum.length; j++) {
                        if (newReqNumsArray[i].name == $selectedReqNum[j].name) {
                            newReqNumsArray[i]['ticked'] = true;
                            break;
                        }
                    }
                }
                $scope.fullReqNumList = angular.copy(newReqNumsArray);
            }

            function _setRoles(roles){
                newRolesArray = [];

                if(roles.length == 0) {
                    $scope.selectedRoles = [];
                    $scope.filterRequisitionObject.roleList = [];
                }

                $selectedRoles = angular.copy($scope.selectedRoles);

                for (i = 0; i < roles.length; i++){
                    var roleObj = {"name": roles[i]};
                    if (!_containsObject(roleObj, newRolesArray, "name")) {
                       newRolesArray.push(roleObj);
                    }

                }

                for (i = 0; i < newRolesArray.length; i++) {
                    for (j = 0; j < $selectedRoles.length; j++) {
                        if (newRolesArray[i].name == $selectedRoles[j].name) {
                            newRolesArray[i]['ticked'] = true;
                            break;
                        }
                    }
                }
                $scope.fullRolesList = angular.copy(newRolesArray);

            }

            function _setRecruiters(recruiters) {
                newRecruiterArray = [];

                if(recruiters.length == 0) {
                    $scope.selectedRecruiters = [];
                    $scope.filterRequisitionObject.recruiters = [];
                }

                $selectedRecruiters = angular.copy($scope.selectedRecruiters);
                for (i = 0; i < recruiters.length; i ++) {
                    var name;
                    if(recruiters[i].lastname !== null){
                        name = recruiters[i].firstname+" "+recruiters[i].lastname;
                    }else{
                        name = recruiters[i].firstname;
                    }
                    var recObj = {"name": name, "id": recruiters[i].id};
                    if (!_containsObject(recObj, newRecruiterArray, "name")) {
                        newRecruiterArray.push(recObj);
                    }

                }

                for (i = 0; i < newRecruiterArray.length; i++) {
                    for (j = 0; j < $selectedRecruiters.length; j++) {
                        if (newRecruiterArray[i].id == $selectedRecruiters[j].id) {
                            newRecruiterArray[i]['ticked'] = true;
                            break;
                        }
                    }
                }
                $scope.fullRecruitersList = angular.copy(newRecruiterArray);
            }

            function _setClients(clients) {
                newClientArray = [];

                if(clients.length == 0) {
                    $scope.selectedClients = [];
                    $scope.filterRequisitionObject.clientOrBU = [];
                }

                $selectedClients = angular.copy($scope.selectedClients);

                for (i = 0; i < clients.length; i ++) {
                    if (angular.isDefined(clients[i]) && clients[i] != null && angular.isDefined(clients[i].id)) {
                        var recObj = {"name": clients[i].name, "id": clients[i].id};
                        if (!_containsObject(recObj, newClientArray, "name")) {
                            newClientArray.push(recObj);
                        }
                    }
                }
                for (i = 0; i < newClientArray.length; i++) {
                    for (j = 0; j < $selectedClients.length; j++) {
                        if (newClientArray[i].id == $selectedClients[j].id) {
                            newClientArray[i]['ticked'] = true;
                            break;
                        }
                    }
                }

                $scope.fullClientsList = angular.copy(newClientArray);
            }


            function multiSelectClients(selectedClientOrBU) {
                $scope.filterRequisitionObject.clientOrBU = [];
                for (index in selectedClientOrBU) {
                    var obj = selectedClientOrBU[index];
                    if ($scope.filterRequisitionObject.clientOrBU.indexOf(obj.id) < 0) {
                        $scope.filterRequisitionObject.clientOrBU.push(obj.id);
                    }
                }

                $scope.requisitionsContainer.requisitionsQueryFilter = angular.copy($scope.filterRequisitionObject);
                $scope.filterJobs(function(data){
                   $scope.selectedClientObj = {};
                   $scope.selectedClientObj.clientOrBU = [];
                   $scope.selectedClientObj.clientOrBU = angular.copy($scope.filterRequisitionObject.clientOrBU);
                   genericService.addObject(StorageService.get('baseurl') + REQUISITIONSCONSTANTS.CONTROLLER.GET_ROLE_RECRUITER_BY_CLIENT+ '/' +$scope.userId+ '/' +$scope.companyId, $scope.selectedClientObj).then(function (data) {
                        roles = data.roleList;
                        roleKeys = Object.keys(roles);

                        recruiters = data.recruiters;
                        recruiterKeys = Object.keys(recruiters);

                        _setRoles(roles);
                        _setRecruiters(recruiters)

                    }, function myError(response) {
                        $scope.fullRolesList = angular.copy($scope.fullRolesListCopy);
                        $scope.fullRecruitersList = angular.copy($scope.fullRecruitersListCopy);
                    });
                });
            }

            function multiSelectRoles(selectedRoles) {
                $scope.filterRequisitionObject.roleList = [];
                for (index in selectedRoles) {
                    var obj = selectedRoles[index];
                    if ($scope.filterRequisitionObject.roleList.indexOf(obj.name) < 0) {
                        $scope.filterRequisitionObject.roleList.push(obj.name);
                    }
                }
                $scope.filterJobs(function(data) {
                    $scope.selectedRoleObj = {};
                    $scope.selectedRoleObj.roleList = [];
                    $scope.selectedRoleObj.roleList = angular.copy($scope.filterRequisitionObject.roleList);
                    genericService.addObject(StorageService.get('baseurl') + REQUISITIONSCONSTANTS.CONTROLLER.GET_CLIENTS_RECRUITERS_BY_ROLE+ '/' +$scope.userId+ '/' +$scope.companyId, $scope.selectedRoleObj).then(function (data) {
                        clients = data.clientOrBU;
                        clientKeys = Object.keys(clients);

                        recruiters = data.recruiters;
                        recruiterKeys = Object.keys(recruiters);

                        _setClients(clients);
                        _setRecruiters(recruiters);


                    }, function myError(response) {
                        $scope.fullClientsList = angular.copy($scope.fullClientsListCopy);
                        $scope.fullRecruitersList = angular.copy($scope.fullRecruitersListCopy);
                    });
                });
            }

            function multiSelectStatus(selectedStatus) {
                $scope.filterRequisitionObject.requisitionStates = [];
                for (index in selectedStatus) {
                    var obj = selectedStatus[index];
                    if ($scope.filterRequisitionObject.requisitionStates.indexOf(angular.uppercase(obj.name)) < 0) {
                        $scope.filterRequisitionObject.requisitionStates.push(angular.uppercase(obj.name));
                    }
                }
                $scope.filterJobs(function(data){
                    $selectedStatus = angular.copy($scope.selectedStatus);
                    for (i = 0; i < $scope.fullStatusList.length; i++) {
                        for (j = 0; j < $selectedStatus.length; j++) {
                            if ($scope.fullStatusList[i].name == $selectedStatus[j].name) {
                                $scope.fullStatusList[i]['ticked'] = true;
                                break;
                            }
                        }
                    }
                });
            }

            function multiSelectReqNum(selectedReqNum) {
                $scope.filterRequisitionObject.requisitionNums = [];
                for (index in selectedReqNum) {
                    var obj = selectedReqNum[index];
                    if ($scope.filterRequisitionObject.requisitionNums.indexOf(obj.name) < 0) {
                        $scope.filterRequisitionObject.requisitionNums.push(obj.name);
                    }
                }
                $scope.filterJobs(function(data){
                    $selectedReqNum = angular.copy($scope.selectedReqNum);
                    for (i = 0; i < $scope.fullReqNumList.length; i++) {
                        for (j = 0; j < $selectedReqNum.length; j++) {
                            if ($scope.fullReqNumList[i].name == $selectedReqNum[j].name) {
                                $scope.fullReqNumList[i]['ticked'] = true;
                                break;
                            }
                        }
                    }
                });
            }

            function multiSelectRecruiters(selectedRecruiters) {
                $scope.filterRequisitionObject.recruiters = [];
                for (index in selectedRecruiters) {
                    var obj = selectedRecruiters[index];
                    if ($scope.filterRequisitionObject.recruiters.indexOf(obj.id) < 0) {
                        $scope.filterRequisitionObject.recruiters.push(obj.id);
                    }
                }
                $scope.filterJobs(function(data){
                   $scope.selectedRecruitertObj = {};
                   $scope.selectedRecruitertObj.recruiters = [];
                   $scope.selectedRecruitertObj.recruiters = angular.copy($scope.filterRequisitionObject.recruiters);
                   genericService.addObject(StorageService.get('baseurl') + REQUISITIONSCONSTANTS.CONTROLLER.GET_CLIENT_ROLE_BY_RECRUITER+ '/' +$scope.userId+ '/' +$scope.companyId, $scope.selectedRecruitertObj).then(function (data) {
                        clients = data.clientOrBU;
                        clientKeys = Object.keys(clients);


                        roles = data.roleList;
                        roleKeys = Object.keys(roles);

                        _setRoles(roles);
                        _setClients(clients);

                    }, function myError(response) {
                        $scope.fullClientsList = angular.copy($scope.fullClientsListCopy);
                        $scope.fullRolesList = angular.copy($scope.fullRolesListCopy);
                    });
                });
            }

            var showAlert = function (message) {
                bootbox.alert({
                    closeButton: false,
                    title: '<div class="alert alert-danger" ><i class="fa fa-times-circle fa-fw fa-lg"></i><strong>Oh snap!</strong></div>',
                    message: message
                });
            };

            $scope.setCandidate = function (candidate, candidateFlag) {
                $scope.requisitionSPOC.candidateId = candidate[0].id;
                if (candidateFlag) {
                    $scope.requisitionSPOC.openingNumber = candidate[0].requisitionOpeningNumber;
                }
            };
 
//            $scope.searchRequisitions = function (requisition) {
//                return (angular.lowercase(requisition.companyName).indexOf(angular.lowercase($scope.searchTerm) || '') !== -1 ||
//                    angular.lowercase(requisition.requisitionRole).indexOf(angular.lowercase($scope.searchTerm) || '') !== -1);
//            };

            function searchJobs() {
                $scope.queryRequisitions();

            }

            function searchBasedOnFilters () {
                $scope.filterRequisitionObject = angular.copy($scope.selectedfilterObjCopy);
                $scope.SearchFilters = filter;
                //  $("#search-panel").hide();
                $(document).off("click", "#filterSearch").on("click", "#filterSearch",
                    function () {
                        $("#search-panel").animate({
                            height: 'toggle'
                        });
                    });
            }

            function filterJobs(callback) {
                $scope.filterRequisitionObject.pageNum = 1;
                count = 2;
                $scope.selectedfilterObjCopy = angular.copy($scope.filterRequisitionObject);
                $scope.requisitionsContainer.requisitionsQueryFilter = angular.copy($scope.filterRequisitionObject);
                $scope.queryRequisitions();
            }
            
          
            /**
             * On click of delete this method will get called
             * we are assigning the object for delete the requisition on click of OK on confirmation pop up
             */
            $scope.assignRequisition =function(req){
            	$scope.deleteRequistion = req;
            };
            
            /**
             *  This method is for delete the requisition
             */
            $scope.deleteRequisition = function(requisition){
            	genericService.deleteObject('').then(function (data) {

                }, function (error) {
                    //Once the real API provides this can be removed
                    for (var int = 0; int <  $scope.requisitions.length; int++) {
                    	if($scope.requisitions[int].RequisitionId === requisition.RequisitionId){
                    		$scope.requisitions.splice(requisition.RequisitionId,1);
                    	}
                    }
                });
            };

             function selectRemoveAll(label, type) {
                var selectAllFlag = false;
                if (type === "selectAll") {
                    selectAllFlag = true;
                }
                if (label === MESSAGECONSTANTS.LABLES.CLIENT || label === MESSAGECONSTANTS.LABLES.BU) {
                   $scope.filterRequisitionObject.clientOrBU = [];
                } else if (label === MESSAGECONSTANTS.LABLES.USER_MANAGEMENT.ROLE) {
                    $scope.filterRequisitionObject.roleList = [];
                } else if (label === MESSAGECONSTANTS.LABLES.REQUISITION_STATUS) {
                    $scope.filterRequisitionObject.requisitionStates = [];
                } else if (label === MESSAGECONSTANTS.LABLES.RECRUITER) {
                     $scope.filterRequisitionObject.recruiters = [];
                } else if (label === MESSAGECONSTANTS.LABLES.REQ_NUMBER) {
                    $scope.filterRequisitionObject.requisitionNums = [];
                }
                //set the dropdown value to the selected value when selectAll
                if (selectAllFlag) {
                    $scope.filterJobs(function(data) {
                        if (label === MESSAGECONSTANTS.LABLES.CLIENT || label === MESSAGECONSTANTS.LABLES.BU) {
                            for (i = 0; i < $scope.fullClientsList.length; i++) {
                                $scope.fullClientsList[i]['ticked'] = true;
                            }
                        } else if (label === MESSAGECONSTANTS.LABLES.USER_MANAGEMENT.ROLE) {
                            for (i = 0; i < $scope.fullRolesList.length; i++) {
                                $scope.fullRolesList[i]['ticked'] = true;
                            }

                        } else if (label === MESSAGECONSTANTS.LABLES.REQUISITION_STATUS) {
                            for (i = 0; i < $scope.fullStatusList.length; i++) {
                                 $scope.fullStatusList[i]['ticked'] = true;
                            }

                        } else if (label === MESSAGECONSTANTS.LABLES.RECRUITER) {
                            for (i = 0; i < $scope.fullRecruitersList.length; i++) {
                                 $scope.fullRecruitersList[i]['ticked'] = true;
                            }
                        }
                        else if (label === MESSAGECONSTANTS.LABLES.RECRUITER) {
                            for (i = 0; i < $scope.fullReqNumList.length; i++) {
                                 $scope.fullReqNumList[i]['ticked'] = true;
                            }
                        }
                    });
                } else {
                    $scope.filterJobs();
                }

            }

            function sortRequisitions(sortColumn) {
                 $scope.filterRequisitionObject.sortColumn = "";
                 $scope.filterRequisitionObject.sortDirection = "";
                if (angular.isDefined(sortColumn) && sortColumn != "") {
                    var array = sortColumn.split(" ");
                    $scope.filterRequisitionObject.sortColumn = array[0];
                    $scope.filterRequisitionObject.sortDirection = array[1];
                    $scope.filterJobs();
                } else {
                    $scope.filterJobs();
                }

            }

            $('input[name="daterange"]').daterangepicker({
                locale: {
                  cancelLabel: 'Clear'
                },
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'This Week': [moment().startOf('week'), moment().endOf('week')],
                    'Last Week': [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    'This Quarter': [moment().startOf('quarter'), moment().endOf('quarter')],
                    'Last Quarter': [moment().subtract(1, 'quarter').startOf('quarter'), moment().subtract(1, 'quarter').endOf('quarter')],
                    'Past 6 Weeks': [moment().subtract(5, 'week').startOf('week'), moment().endOf('week')],
                    'Past 6 Months': [moment().subtract(5, 'month').startOf('month'), moment().endOf('month')],
                    'Past 6 Quarter': [moment().subtract(5, 'quarter').startOf('quarter'), moment().endOf('quarter')],
                }
            });

            $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val("");
                $scope.filterRequisitionObject.fromDate = "";
                $scope.filterRequisitionObject.toDate = "";
                $scope.filterJobs(function() {

                });
            });



            function sortByDate(daterange) {
                $scope.filterRequisitionObject.fromDate = "";
                $scope.filterRequisitionObject.toDate = "";
                $scope.selectedDateRangeCopy = angular.copy($scope.selectedDateRange);
                if (angular.isDefined( $scope.selectedDateRangeCopy) &&  $scope.selectedDateRangeCopy !== "") {
                    var array =  $scope.selectedDateRangeCopy.split("-");
                    $scope.filterRequisitionObject.fromDate = array[0];
                    $scope.filterRequisitionObject.toDate = array[1];
                    $scope.filterJobs();
                } else {
                    $scope.selectedDateRange = "";
                    $scope.filterJobs();
                }
            }

            function resetFilterData() {
                $scope.filterRequisitionObject = {};
                $scope.filterRequisitionObject.clientOrBU = [];
                $scope.filterRequisitionObject.roleList = [];
                $scope.filterRequisitionObject.requisitionNums = [];
                $scope.filterRequisitionObject.recruiters = [];
                $scope.filterRequisitionObject.searchCheck = false;
                $scope.filterRequisitionObject.size = 9;
                $scope.filterRequisitionObject.pageNum = 1;
                $scope.filterRequisitionObject.fromDate = "";
                $scope.filterRequisitionObject.toDate = "";
                $scope.filterRequisitionObject.sortColumn = "";
                $scope.filterRequisitionObject.sortDirection = "";
                $scope.filterRequisitionObject.isAssignedToMe = true;
                $scope.filterRequisitionObject.isAssignedToOthers = true;
                $scope.filterRequisitionObject.isUnassigned = true;
                $scope.requisitionsContainer.requisitionsQueryFilter = angular.copy($scope.filterRequisitionObject);
                $scope.selectedSort = "Please Select";
                //$scope.selectedCandidate = {name:"Please Select"};

                $scope.filterJobs(function(data){
                    $scope.fullClientsList = angular.copy($scope.fullClientsListCopy);
                    $scope.fullRecruitersList = angular.copy($scope.fullRecruitersListCopy);
                    $scope.fullRolesList = angular.copy($scope.fullRolesListCopy);
                    $scope.fullStatusList = angular.copy($scope.fullStatusListCopy);
                    $scope.fullReqNumList = angular.copy($scope.fullReqNumListCopy);
                });
            }

            //Implementaion of filters 
            $(document).ready(function () {
                $("#search-panel").hide();
                $(document).off("click", "#open-filter").on("click", "#open-filter",
                    function () {
                        $("#search-panel").animate({
                            height: 'toggle'
                        });
                    });
            });

            function determineDropDirection() {
                $(".dropdown-menu").each(function () {

                    // Invisibly expand the dropdown menu so its true height can be calculated
                    $(this).css({
                        visibility: "hidden",
                        display: "block"
                    });

                    // Necessary to remove class each time so we don't unwantedly use dropup's offset top
                    $(this).parent().removeClass("dropup");

                    // Determine whether bottom of menu will be below window at current scroll position
                    if ($(this).offset().top + $(this).outerHeight() > $(window).innerHeight() + $(window).scrollTop()) {
                        $(this).parent().addClass("dropup");
                    }

                    // Return dropdown menu to fully hidden state
                    $(this).removeAttr("style");
                });
            } 

            $('html').on('mouseup', function(e) {
                if(!$(e.target).closest('.popover').length) {
                    $('.popover').each(function(){
                        $(this.previousSibling).popover('hide');
                    });
                }
            });
        }]);