/**
 * This module deals with Role entity scope management operations
 * i.e. create,view and update Role entity scope of particular company. 
 * @author Rajendra
 */
var roleentityscopemodule = angular.module('4dot5.missioncontrolmodule.roleentityscopemodule', []);

roleentityscopemodule.constant('ROLEENTITYSCOPECONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.roleentityscope',
        URL: '/roleentityscope',
        CONTROLLER: 'RoleentityscopeController',
        TEMPLATEURL: 'app/partials/missioncontrol/roleentityscope/roleentityscope.html',
    },
    CONTROLLER: {
        ROLE_ENTITY: 'api/company/getroleentityscope',
        LOGGED_IN_DETAILS: 'api/user/getloggedinuserdetails',
        CREATE_ROLE_ENTITY: 'api/company/createroleentityscope',
        UPDATE_ROLE_ENTITY: 'api/company/updateroleentityscope'

    }
});

roleentityscopemodule.config(
    ['$stateProvider',
        'ROLEENTITYSCOPECONSTANTS',
        function ($stateProvider, ROLEENTITYSCOPECONSTANTS) {
            $stateProvider.state(ROLEENTITYSCOPECONSTANTS.CONFIG.STATE, {
                url: ROLEENTITYSCOPECONSTANTS.CONFIG.URL,
                templateUrl: ROLEENTITYSCOPECONSTANTS.CONFIG.TEMPLATEURL,
                controller: ROLEENTITYSCOPECONSTANTS.CONFIG.CONTROLLER,
                data: {
                    requireLogin: true
                },
                resolve: {
                    companyRoleScopeData: function (utilityService) {
                        return utilityService.getCompanyRoleEntityScope();
                    }
                }
            });
        }
    ]);

roleentityscopemodule.controller('RoleentityscopeController', ['$scope',
    '$http',
    '$state',
    '$timeout',
    '$rootScope',
    '$stateParams',
    'genericService',
    'StorageService',
    'companyRoleScopeData',
    'ROLEENTITYSCOPECONSTANTS',
    'MESSAGECONSTANTS',
    'alertsAndNotificationsService',
    function ($scope, $http, $state, $timeout, $rootScope, $stateParams, genericService, StorageService, companyRoleScopeData, ROLEENTITYSCOPECONSTANTS, MESSAGECONSTANTS, alertsAndNotificationsService) {
        console.log('ROLE ENTITY SCOPE DATA $scope.roleEntityScope : ' + angular.toJson(companyRoleScopeData));
        
        $scope.RoleEntityForm = {};
        var showConfirmationPopup = false;
        
        $scope.TransactionroleEntityScope = angular.copy(companyRoleScopeData);
        var savedRoleEntityScope = angular.copy(companyRoleScopeData);
        var scopeList = angular.copy(companyRoleScopeData.scopeList);

        var isScopeChanged = function(){
            return !angular.equals($scope.TransactionroleEntityScope, savedRoleEntityScope);
        }

        /**
         * this function is called on click of All radio button
         * makes all as selected and list as unselected 
         */
        $scope.changeAll = function(index,obj,value){
        	$scope.TransactionroleEntityScope.scopeList[index].allClientsOrBU = true;
        	$scope.TransactionroleEntityScope.scopeList[index].clientOrBUList = false;
        	allClientsOrBU_index = true;
        	allClientsOrBU_index = false;
        }
        
        /**
         * this function is called on click of list radio button
         * makes list as selected and all as unselected 
         */
        $scope.changelist = function(index,obj,value){
        	$scope.TransactionroleEntityScope.scopeList[index].clientOrBUList = true;
        	$scope.TransactionroleEntityScope.scopeList[index].allClientsOrBU = false;
        	obj.clientOrBUList = true;
        	obj.allClientsOrBU = false;
        }

        var setCompanyRes = function(url){
            //savedRoleEntityScope = angular.copy($scope.TransactionroleEntityScope);
            genericService.addObject(url, $scope.TransactionroleEntityScope).then(function (data) {
                console.log('setCompanyRes success with :  ' + angular.toJson(data));
                $scope.TransactionroleEntityScope = angular.copy(data);
                savedRoleEntityScope = angular.copy(data);
                $scope.RoleEntityForm.$setPristine();
                var message = MESSAGECONSTANTS.NOTIFICATIONS.COMPANY_RES.SET_SUCCESS1 + 
                              $rootScope.userDetails.company.companyName +
                              MESSAGECONSTANTS.NOTIFICATIONS.COMPANY_RES.SET_SUCCESS2;
                alertsAndNotificationsService.showBannerMessage(message, 'success');
            }, function (error) {
            	if($rootScope.isOnline){
                	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                }
            	$scope.TransactionroleEntityScope = angular.copy(savedRoleEntityScope);
            });
        }

        var updateCompanyRes = function(url){
            //savedRoleEntityScope = angular.copy($scope.TransactionroleEntityScope);
            genericService.addObject(url, $scope.TransactionroleEntityScope).then(function (data) {
                console.log('updateCompanyRes success with :  ' + angular.toJson(data));
                $scope.TransactionroleEntityScope = angular.copy(data);
                savedRoleEntityScope = angular.copy(data);
                $scope.RoleEntityForm.$setPristine();
                var message = MESSAGECONSTANTS.NOTIFICATIONS.COMPANY_RES.UPDATE_SUCCESS1 + 
                              $rootScope.userDetails.company.companyName +
                              MESSAGECONSTANTS.NOTIFICATIONS.COMPANY_RES.UPDATE_SUCCESS2;
                alertsAndNotificationsService.showBannerMessage(message, 'success');
            }, function (error) {
            	if($rootScope.isOnline){
                	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                }
            	$scope.TransactionroleEntityScope = angular.copy(savedRoleEntityScope);
            });
        }

        $scope.saveCompanyRES = function(){
            console.log('save clicked or form submitted : ' + angular.toJson($scope.TransactionroleEntityScope));
             if (angular.copy(savedRoleEntityScope.newScope)) {
                 if(isScopeChanged()){
                     var seturl = ROLEENTITYSCOPECONSTANTS.CONTROLLER.CREATE_ROLE_ENTITY + "/" + $rootScope.userDetails.company.companyId;
                     setCompanyRes(seturl);
                 }else{
                     alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.NOTHING_TO_SAVE, 'success');
                 }
             }else{
                 if(isScopeChanged()){
                     var updateurl = ROLEENTITYSCOPECONSTANTS.CONTROLLER.UPDATE_ROLE_ENTITY + "/" + $rootScope.userDetails.company.companyId;
                     updateCompanyRes(updateurl);
                 }else{
                     alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.NOTHING_TO_SAVE, 'success');
                 }
                 
             }
        }
   
        /**
         * function to reset the changes when clicked on cancel 
         */
        $scope.cancelChanges = function () {
            if (isScopeChanged()) {
                $scope.TransactionroleEntityScope = angular.copy(savedRoleEntityScope);
                $scope.RoleEntityForm.$setPristine();
            }else {

            }
        }

        /**
         * this function is called when state is changed
         * i.e user clicks somewhere outside the page
         * checks some changes are there or not and gives warning popup.
         */
        var unbindStateChangeEvent = $scope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            event.preventDefault();
            if (isScopeChanged()) {
                showConfirmationPopup = true;
            }
            console.log('in unbindStateChangeEvent with showConfirmationPopup : ' + showConfirmationPopup);
            if (showConfirmationPopup) {
                bootbox.confirm({
                    closeButton: false,
                    title: "<div class='alert alert-warning' style='margin-bottom: 0px;'><i class='fa fa-warning fa-fw fa-lg'></i><strong>Warning!</strong></div>",
                    message: "Unsaved data on the page will be lost. Do you still want to continue?",
                    buttons: {
                        confirm: {
                            label: 'Yes',
                            className: 'btn-success'
                        },
                        cancel: {
                            label: 'Cancel',
                            className: 'btn-danger'
                        }
                    },
                    callback: function (result) {
                        if (result) {
                            $scope.cancelChanges();
                            if ($rootScope.switchClicked) {
                            	showConfirmationPopup = false;
                                $scope.getCompaniesForLoggedinUser(function(response){
                                	$('#myModal').modal('show');
                                });
                            } else {
                                unbindStateChangeEvent();
                                if (toState.name === 'login') {
                                    $rootScope.$emit("CallLogoutMethod", { logoutnow: true });
                                } else {
                                	$scope.RoleEntityForm.$setPristine();
                                    $state.go(toState.name, toParams, { reload: true });
                                }
                            }
                        } else {
                            showConfirmationPopup = false;
                            $rootScope.switchClicked = false;
                        }
                    }
                });
            } else {
                //console.log("No changes in this page yet so moving");
                if ($rootScope.switchClicked) {
                    console.log('nothing changed and switchClicked');
                    $scope.getCompaniesForLoggedinUser(function(response){
                    	$('#myModal').modal('show');
                    });
                } else {
                    unbindStateChangeEvent();
                    $rootScope.switchClicked = false;
                    if (toState.name === 'login') {
                        $rootScope.$emit("CallLogoutMethod", { logoutnow: true });
                    } else {
                        $state.go(toState.name, toParams, { reload: true });
                    }
                }
            }
        });
    }
]);
