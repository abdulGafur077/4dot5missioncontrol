/**
 * THIS IS THE MODULE FOR SETTINGS OF 4DOT5 APPLICATION
 */

var settingsmodule = angular.module('4dot5.missioncontrolmodule.settingsmodule',
    []);

settingsmodule.constant('SETTINGSCONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.settings',
        URL: '/settings',
        CONTROLLER: 'SettingsController',
        TEMPLATEURL: 'app/partials/missioncontrol/settings/settings.html',
    },


    /*Get 4dot5 settings: http://localhost:8080/4dot5-missioncontrol-apiserver/api/setting/getfordotfivesettings - GET
    Get Staffing or corporate settings: http://localhost:8080/4dot5-missioncontrol-apiserver/api/setting/getstaffingorcorpsettings/{companyID} - GET
    Get client or BU settings: http://localhost:8080/4dot5-missioncontrol-apiserver/api/setting/getclientorbusettings/{parentCompanyId/{childCompanyId} - GET
    Delete company settings: http://localhost:8080/4dot5-missioncontrol-apiserver/api/setting/deletesettings/{companyId} – DELETE
    Set company settings: http://localhost:8080/4dot5-missioncontrol-apiserver/api/setting/savesettings/{companyId} – PUT getclientsorbuwithoutglobalsetting
    alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.COMPANY + " '<b><i class='wrap-word'>" + delimitedCompanyOrClientName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.COMPANY.COMPANYDRAFTSUCCESS, 'success')
    
    
    */

    CONTROLLER: {
        GET_FOURDOTFIVE_SETTINGS: 'api/setting/getfordotfivesettings',
        GET_CORPORATE_OR_STAFFING_SETTINGS: 'api/setting/getstaffingorcorpsettings/',
        GET_CLIENT_OR_BU_SETTINGS: 'api/setting/getclientorbusettings/',
        DELETE_SETTINGS: 'api/setting/deleteclientorbusettings',
        DELETE_SETTINGS_STAFFING_OR_CORP: 'api/setting/deletestaffingorcorpsettings',
        SET_SETTINGS: 'api/setting/savesettings/',
        GET_PARENT_SETTINGS: 'api/setting/getparentsettings/',

        GET_CLIENTS_OR_BUS: 'api/user/getuserclientsorbu/',
        GET_SETTINGS_TO_COPY: 'api/setting/getclientorbusettings/',
        GET_CLIENTS_OR_BUS_HAVING_CUSTOM_SETTINGS: 'api/company/getclientsorbuwithoutglobalsetting/'
    }
});

settingsmodule.config(
    ['$stateProvider',
        'SETTINGSCONSTANTS',
        function ($stateProvider, SETTINGSCONSTANTS) {
            $stateProvider.state(SETTINGSCONSTANTS.CONFIG.STATE, {
                url: SETTINGSCONSTANTS.CONFIG.URL,
                templateUrl: SETTINGSCONSTANTS.CONFIG.TEMPLATEURL,
                controller: SETTINGSCONSTANTS.CONFIG.CONTROLLER,
                data: {
                    requireLogin: true
                },
                params:{
                	accessMode : null
                }
            });
        }
    ]);

settingsmodule.controller('SettingsController',
    ['$scope',
        '$state',
        '$window',
        '$compile',
        '$rootScope',
        '$stateParams',
        'StorageService',
        'genericService',
        'SETTINGSCONSTANTS',
        'MESSAGECONSTANTS',
        '$timeout',
        '$filter',
        'alertsAndNotificationsService',
        function ($scope, $state, $window, $compile, $rootScope, $stateParams, StorageService, genericService, SETTINGSCONSTANTS, MESSAGECONSTANTS, $timeout,$filter, alertsAndNotificationsService) {

            console.log('In settings controller');
            $scope.form = {};
            $scope.form.settingsTypeform = {};
            $scope.form.performanceSetting = {};
            $scope.globalSettings = null; //Global Settings
            $scope.settings = {} //UI Settings
            $scope.settings.licensePreferenceSetting = {}
            $scope.settings.performanceSetting = {}
            $scope.savedSettings = {}; //Saved Settings Object
            $scope.allClientsOrBus = [];
            $scope.allClientsOrBusForCopy = null;
            $scope.allClientsOrBusForCopyHavingCustomSettings = null;
            $scope.years = [0, 1, 2];
            $scope.months = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
            $scope.days = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];
            $scope.copyChecked = false;
            $scope.copyClientCheckBox = false;
            $scope.selectEnable = false;
            $scope.checkValuesArray = [];
            $scope.availableClientsToCopy = [];
            $scope.clientCopy = null;
            var isCustomSettingsSelected = false;
            var delimitedCompanyOrClientName = null;
            var deleteSettingCounter = 0;
            var showConfirmationPopup = false;
            $scope.isClientGlobalCopy = true;
            
            $scope.modifySettingsCompanyModel=false;
            $scope.globalSettingsCompanyModel=false;
            $scope.modifySettingsClientModel=false;
            $scope.globalSettingsClientModel=false;
            
            $scope.toggleClientValue= {};
            $scope.toggleClientValueCopy= {};
            $scope.companySelectionActive = true;
            
            $scope.isClientCopyObject = false;
            $scope.clientCopyMandatoryMessage = false;
            
            $scope.isStillProcessing = true;
            $scope.isStillProcessingModified = true;
            $scope.currentCompany = $rootScope.userDetails.company;
            var selectedClient = null;
            
            if($stateParams.accessMode === null){
            	$scope.viewonlymode = false;
            }else{
            	$scope.viewonlymode = true;
            }
            
            $scope.isSectionActive = function(){
	            if($scope.isinHost()){
	            	return true;
	            }else{
	            	return (($scope.modifySettingsCompanyModel) || ($scope.modifySettingsClientModel && !$scope.isClientCopyObject));
	            }
            }
            
            $scope.isModifyClientSelected = function(){
                return !$scope.isinHost() && !$scope.companySelectionActive && $scope.modifySettingsClientModel;
            } 
            
            /**
             * Toggle company , client or bu radio button
             */
            
            $scope.manageCompanySelection = function () {
            	console.log('Show Staffing/Corporate settings');
            	$scope.companySelectionActive = true;
                $scope.modifySettingsCompanyModel=false;
                $scope.globalSettingsCompanyModel= false;
                $scope.modifySettingsClientModel=false;
                $scope.globalSettingsClientModel=false;
                $scope.isClientCopyObject = false;
                $scope.clientCopyMandatoryMessage = false;
                $scope.allClientsOrBus = [];
                document.getElementById('modifySettingsCompany').checked = false;
                document.getElementById('globalSettingsCompany').checked = false;
                
                $scope.isClientGlobalCopy = true;
                
                $scope.safeApply();
            	$scope.client = null;
                $scope.copyClientCheckBox = false;
        		$scope.copyChecked = false;
        		$scope.toggleClientValue= {};
                $scope.showClientsOrBusList = false;
                $scope.settings = angular.copy($scope.settingsCopyForCompany);
                //$scope.settings = $scope.settingsCopyForCompany;
                isGlobalOrCustomSettings();
                updateSettingsInView();
            }
            
            $scope.manageClientOrBuSelection = function () {
                if($scope.modifySettingsClientModel || $scope.globalSettingsClientModel){
                	$scope.isStillProcessing = false;
                }else{
                	$scope.isStillProcessing = true;
                }
            	$scope.companySelectionActive = false;
                $scope.modifySettingsCompanyModel=false;
                $scope.globalSettingsCompanyModel=false;
                $scope.modifySettingsClientModel=false;
                $scope.globalSettingsClientModel=false;
                $scope.showClientsOrBusList = true;
                $scope.isClientCopyObject = false;
                $scope.clientCopyMandatoryMessage = false;
                document.getElementById('modifySettingsClient').checked = false;
                document.getElementById('globalSettingsClient').checked = false;
                
                if($scope.copyChecked){
            		document.getElementById("checkbox-inl-1").checked = false;
            		$scope.copyChecked = false;
            	}
                
                //$scope.client = null;
                if($scope.allClientsOrBus.length === 0){
                	getClientsOrBus();
                	$timeout(function () {
                    	if($scope.allClientsOrBus.length > 0){
                        	if ($scope.allClientsOrBus == null) {
                        		getClientsOrBus();
                        	}else if($scope.toggleClientValue && $scope.toggleClientValue.company){
                        		if($scope.toggleClientValue.company.companyType === "Client"){
                        			$scope.selectedClientNameForMessage = $scope.toggleClientValue.company.name;
                        		}else if($scope.toggleClientValue.company.companyType === "BusinessUnit"){
                        			$scope.selectedBUNameForMessage = $scope.toggleClientValue.company.name;
                        		}
                        		if($scope.showClientsOrBusList && $scope.client != null && $scope.copyChecked && $scope.toggleClientValueCopy.company){
                        			if($scope.toggleClientValueCopy.company.companyType === "Client"){
                        				$scope.selectedClientNameForMessageCopy = $scope.toggleClientValueCopy.company.name;
                        			}else if($scope.toggleClientValueCopy.company.companyType === "BusinessUnit"){
                        				$scope.selectedBUNameForMessageCopy = $scope.toggleClientValueCopy.company.name;
                        			}        
                        		}
                        	}
                        }
                    }, 500)
                }else{
                	$scope.getSettingsForClient($scope.client);
                }
            }
            
            /**
             * UNbind state for Warning message pop  
             */
            var isSettingsChanged = function(){
            	if($scope.companySelectionActive){
            		 $scope.changeCompanySettingsBeforeSave();
            		if ((angular.equals($scope.savedSettings, $scope.settingsCopyForCompany)) || (!$scope.isinHost() && $scope.globalSettingsCompanyModel && !angular.equals($scope.savedSettings, $scope.settingsCopyForCompany))) {
            			return false;
            		}else{
            			return true;
            		}
            	}else{
            		$scope.changeClientSettingsBeforeSave();
            		if($scope.allClientsOrBus.length > 0){
            			if((angular.equals($scope.savedSettings, $scope.settingsClient)) || ($scope.globalSettingsClientModel && !angular.equals($scope.savedSettings, $scope.settingsClient))){
            				return false;
            			}else{
            				return true;
            			}
            		}else{
            			return false;
            		}
            	}
            }
            
            $window.addEventListener("beforeunload", function () {
                if (angular.equals($state.$current.name, 'missioncontrol.settings')) {
                	StorageService.remove('removeSliderId');
                	console.log('now removeSliderId and refreshing');
                }
            });
            
            var unbindStateChangeEvent = $scope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
                event.preventDefault();
                if (isSettingsChanged()) {
                    console.log('Unsaved changes');
                    showConfirmationPopup = true;
                } 
                if (showConfirmationPopup) {
                    bootbox.confirm({
                    	closeButton: false,
                        title: "<div class='alert alert-warning' style='margin-bottom: 0px;'><i class='fa fa-warning fa-fw fa-lg'></i><strong>Warning!</strong></div>",
                        message: "Unsaved data on the page will be lost. Do you still want to continue?",
                        buttons: {
                            confirm: {
                                label: 'Yes',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'Cancel',
                                className: 'btn-danger'
                            }
                        },

                        callback: function (result) {
                            if (result) {
                                StorageService.remove('removeSliderId');
                                StorageService.set('outOfPage', 'movingOut');
                                if ($rootScope.switchClicked) {
                                    //$('#myModal').modal('show');
                                	$scope.cancel();
                                    $scope.getCompaniesForLoggedinUser(function(response){
                                    	$('#myModal').modal('show');
                                    	showConfirmationPopup = false;
                                        $rootScope.switchClicked = false;
                                    });
                                } else {
                                    unbindStateChangeEvent();
                                    if (toState.name === 'login') {
                                        $rootScope.$emit("CallLogoutMethod", { logoutnow: true });
                                    } else {
                                        StorageService.remove('wizardoperations');
                                        $state.go(toState.name, toParams, { reload: true });
                                    }
                                }
                            } else {
                                showConfirmationPopup = false;
                                $rootScope.switchClicked = false;
                            }
                        }
                    });
                } else {
                	StorageService.remove('removeSliderId');
                    StorageService.set('outOfPage', 'movingOut');
                    if ($rootScope.switchClicked) {
                        //$('#myModal').modal('show');
                        $scope.getCompaniesForLoggedinUser(function(response){
                        	$('#myModal').modal('show');
                        });
                    } else {
                        unbindStateChangeEvent();
                        $rootScope.switchClicked = false;
                        if (toState.name === 'login') {
                            $rootScope.$emit("CallLogoutMethod", { logoutnow: true });
                        } else {
                            StorageService.remove('wizardoperations');
                            $state.go(toState.name, toParams, { reload: true });
                        }
                    }
                }

            });
            /**
             * Change company settings type.
             */
            $scope.changeCompanySettings = function(){
            	$scope.settings = angular.copy($scope.settingsCopyForCompany);
            	$scope.savedSettings = angular.copy($scope.settingsCopyForCompany);
            	//console.log(angular.equals($scope.settings,$scope.savedSettings));
                isGlobalOrCustomSettings();
                updateSettingsInView();
            	$scope.globalSettingsCompanyModel = false;
            	$scope.modifySettingsCompanyModel = true;
            	//$scope.settings.global = false;
                for (var index = 0; index < $scope.settings.licensePreferenceSetting.length; index++) {
                    if ($scope.settings.licensePreferenceSetting[index].isEnabled) {
                       // console.log('Enabling: ' + $scope.settings.licensePreferenceSetting[index].name);
                        angular.element('.slider-setting-' + index).attr({ 'disabled': false });
                        angular.element('.slider-setting-timeToComplete-' + index).attr({ 'disabled': false });
                    }
                }
                angular.element('.slider-range').attr({ 'disabled': false });
                angular.element('.onoffswitch').removeClass('avoid-clicks');
                $scope.selectEnable = false;
                isCustomSettingsSelected = true;
                
            }
            /**
             * Change client settings type.
             */
            $scope.changeClientSettings = function(){
            	$scope.isClientCopyObject = false;
            	$scope.clientCopyMandatoryMessage = false;
            	$scope.settings = angular.copy($scope.settingsClient);
            	$scope.savedSettings = angular.copy($scope.settingsClient);
            	//console.log(angular.equals($scope.settings,$scope.savedSettings));
	        	 isGlobalOrCustomSettings();
	             updateSettingsInView();
            	
            	$scope.modifySettingsClientModel=true;
            	$scope.globalSettingsClientModel=false;
            	//$scope.settings.global = false;
            	if($scope.copyChecked){
            		document.getElementById("checkbox-inl-1").checked = false;
            		$scope.copyChecked = false;
            	}
            	for (var index = 0; index < $scope.settings.licensePreferenceSetting.length; index++) {
            		if ($scope.settings.licensePreferenceSetting[index].isEnabled) {
            			angular.element('.slider-setting-' + index).attr({ 'disabled': false });
            			angular.element('.slider-setting-timeToComplete-' + index).attr({ 'disabled': false });
            		}
            	}
                angular.element('.slider-range').attr({ 'disabled': false });
                angular.element('.onoffswitch').removeClass('avoid-clicks');
                $scope.selectEnable = false;
                isCustomSettingsSelected = true;
            }
            /**
             * Global settings at both company and client level.
             */
            $scope.changeGlobalSettings = function(){
            	$scope.isClientCopyObject = false;
            	$scope.clientCopyMandatoryMessage = false;
            	//console.log($scope.copyChecked);
            	if($scope.copyChecked){
            		document.getElementById("checkbox-inl-1").checked = false;
            		$scope.copyChecked = false;
            	}
            	//$scope.settings.global = true;
                angular.element('.slider-range').attr({ 'disabled': true });
                angular.element('.noUi-target').attr({ 'disabled': true });
                angular.element('.onofsfswitch').addClass('avoid-clicks');
                isCustomSettingsSelected = false;
                $scope.selectEnable = true;
                if (!$scope.companySelectionActive) {
                	console.log("Gets settings for Staffing/Corpoarte Company with reference to client.");
                	$scope.modifySettingsClientModel=false;
                    $scope.globalSettingsClientModel=true;
                    $scope.getParentSettings(); //Gets settings for Staffing/Corpoarte Company with reference to client.
                } else {
                	console.log("Gets settings for FourDotFive Company.");
                	console.log(angular.equals($scope.savedSettings, $scope.settingsCopyForCompany));
                	$scope.globalSettingsCompanyModel = true;
                	$scope.modifySettingsCompanyModel = false;
                    getFourDotFiveSettings();  //Gets settings for FourDotFive Company.
                }
            }
            
            /**
             * Change settings type.
             */
            /*$scope.changeSettingsType = function (settingsType) {
                console.log('Setting type: ' + settingsType);
                if (settingsType === 'globalsettings') {
                	$scope.selectedClientSettings=false;
                	$scope.selectedClientSettingsCopy=false;
                	$scope.selectedCompanySettings=false;
                	$scope.selected4dot5Settings=true;
                	$scope.selectedBUSettings=false;
                    $scope.selectedBUSettingsCopy=false;
                    angular.element('.slider-range').attr({ 'disabled': true });
                    angular.element('.noUi-target').attr({ 'disabled': true });
                    // angular.element('.access').attr({ 'disabled': true });

                    angular.element('.onoffswitch').addClass('avoid-clicks');
                    isCustomSettingsSelected = false;
                    $scope.selectEnable = true;
                    if ($scope.showClientsOrBusList) {
                        $scope.getParentSettings(); //Gets settings for Staffing/Corpoarte Company with reference to client.
                        console.log("Gets settings for Staffing/Corpoarte Company with reference to client.");
                        $scope.selected4dot5Settings = false;
                        $scope.selectedCompanySettings = true;
                    } else {
                    	console.log("Gets settings for FourDotFive Company.");
                        getFourDotFiveSettings();  //Gets settings for FourDotFive Company.
                    }
                } else {
//                	$scope.selectedClientSettings=false;
//                	$scope.selectedCompanySettings=true;
//                	$scope.selected4dot5Settings=false;
                	$scope.onClickCompanyModifySettings = true;
                	console.log("$scope.globalSettingsCompanyModel"+$scope.globalSettingsCompanyModel);
                    for (var index = 0; index < $scope.settings.licensePreferenceSetting.length; index++) {
                        if ($scope.settings.licensePreferenceSetting[index].isEnabled) {
                            console.log('Enabling: ' + $scope.settings.licensePreferenceSetting[index].name);
                            angular.element('.slider-setting-' + index).attr({ 'disabled': false });
                            angular.element('.slider-setting-timeToComplete-' + index).attr({ 'disabled': false });
                            // angular.element('.dropdowns-for-' + index).attr({ 'disabled': false });
                        }
                    }
                    
                    angular.element('.noUi-target').attr({ 'disabled': false });
                    angular.element('.access').attr({ 'disabled': false });

                    angular.element('.slider-range').attr({ 'disabled': false });
                    angular.element('.onoffswitch').removeClass('avoid-clicks');
                    $scope.selectEnable = false;
                    isCustomSettingsSelected = true;
                }
                console.log('isCustomSettingsSelected: ' + isCustomSettingsSelected);
            }
*/
            /**
             * Setting value and render sliders in UI
             */
            $scope.setValues = function () {
            	//console.log("setvalues method");
                var sliderValues = $scope.settings.licensePreferenceSetting;
                for (var i = 0; i < $scope.settings.licensePreferenceSetting.length; i++) {
                    //console.log('Score is: ' + sliderValues[i].score);
                    if (sliderValues[i].score != null) {
                        $('.slider-setting-' + i).noUiSlider({
                            range: [0, 100],
                            start: [sliderValues[i].score],
                            handles: 1,
                            step: 0.5,
                            connect: 'lower',
                            slide: function () {
                                var val = $(this).val();
                                $(this).next('span').text(val).addClass('sliderBold');
                            },
                            set: function () {
                                var val = $(this).val();
                                $(this).next('span').text(val).removeClass('sliderBold');
                            }

                        });
                        $('.slider-setting-' + i).val(sliderValues[i].score, true);
                    }
                }

                for (var i = 0; i < $scope.settings.licensePreferenceSetting.length; i++) {
                    //console.log('Score is: ' + sliderValues[i].timeToComplete);
                    $('.slider-setting-timeToComplete-' + i).noUiSlider({
                        range: [0, 10],
                        start: [sliderValues[i].timeToComplete],
                        handles: 1,
                        step: 1,
                        connect: 'lower',
                        slide: function () {
                            var val = $(this).val();
                            $(this).next('span').text(val).addClass('sliderBold');
                        },
                        set: function () {
                            var val = $(this).val();
                            $(this).next('span').text(val).removeClass('sliderBold');
                        }

                    });
                    $('.slider-setting-timeToComplete-' + i).val(sliderValues[i].timeToComplete, true);
                }

                var performanceSliderValues = $scope.settings.performanceSetting;
                //console.log('performanceSliderValues: ' + angular.toJson(performanceSliderValues));
                for (var j = 0; j < $scope.settings.performanceSetting.length; j++) {
                    $('.slider-range-' + j).noUiSlider({
                        range: [0, performanceSliderValues[j].maxLimit],
                        start: [performanceSliderValues[j].minValue, performanceSliderValues[j].maxValue],
                        step: 1,
                        connect: true,
                        slide: function () {
                            var values = $(this).val();
                            var fromLabel = $(this).next('p').find('.fromLabel').text();
                            var toLabel = $(this).next('p').find('.toLabel').text()

                            if (fromLabel != '' && fromLabel != values[0]) {
                                $(this).next('p').find('.fromLabel').text(values[0]).addClass('sliderBold');
                            } else if (toLabel != '' && toLabel != values[1]) {
                                $(this).next('p').find('.toLabel').text(values[1]).addClass('sliderBold');
                            }
                        },
                        set: function () {
                            var values = $(this).val();
                            $(this).next('p').find('.fromLabel').text(values[0]).removeClass('sliderBold');
                            $(this).next('p').find('.toLabel').text(values[1]).removeClass('sliderBold');
                        }
                    });
                    $('.slider-range-' + j).val([performanceSliderValues[j].minValue, performanceSliderValues[j].maxValue], true);
                }
            }

            $('#disable-checkbox').click(function () {
                if (this.checked) {
                    // Disable the slider
                    disableSlider.attr("disabled", "disabled");
                } else {
                    // Enabled the slider
                    disableSlider.removeAttr("disabled");
                }
            });

            /**
             * This function is called on click of cancel to reset scope variables.
             */
            $scope.cancel = function () {
	            if($scope.companySelectionActive){
	            	// $scope.changeCompanySettingsBeforeSave();
	            	// if (!angular.equals($scope.savedSettings, $scope.settingsCopyForCompany)) {
	            		 $scope.settings = angular.copy($scope.settingsCopyForCompany);
	            		 updateSettingsInView();
	            		 if(!$scope.isinHost()){
	            		 $scope.modifySettingsCompanyModel=false;
                         $scope.globalSettingsCompanyModel=false;
                         document.getElementById('modifySettingsCompany').checked = false;
                       	 document.getElementById('globalSettingsCompany').checked = false;
	            		 }
	            	// }
	           	}else if(!$scope.companySelectionActive){
	           		//$scope.changeClientSettingsBeforeSave();
            		//if(!angular.equals($scope.savedSettings, $scope.settingsClient)) {
	            		 $scope.settings = angular.copy($scope.settingsClient);
	            		 updateSettingsInView();
	            		 $scope.modifySettingsClientModel=false;
	                        $scope.globalSettingsClientModel=false;
	                        document.getElementById('modifySettingsClient').checked = false;
	                        document.getElementById('globalSettingsClient').checked = false;
	                        $scope.copyChecked = false;
            		//}
	           		
	           	}
            }

            /**
             * Function Re-Render Sliders values on change of settings;
             */
            function updateSettingsInView() {
               // console.log('Updating Setings in view');
                for (var i = 0; i < $scope.settings.licensePreferenceSetting.length; i++) {
                    $('.slider-setting-' + i).val($scope.settings.licensePreferenceSetting[i].score, true);
                    $('.slider-setting-timeToComplete-' + i).val($scope.settings.licensePreferenceSetting[i].timeToComplete, true);
                    if (!$scope.settings.licensePreferenceSetting[i].isEnabled) {
                        //console.log('Disabling: ' + $scope.settings.licensePreferenceSetting[i].name);
                        angular.element('.slider-setting-' + i).attr({ 'disabled': true });
                    }
                    //$('.slider-setting-' + i).next('span').text($('.slider-setting-' + i).val());
                }
                for (var i = 0; i < $scope.settings.performanceSetting.length; i++) {
                    $('.slider-range-' + i).val([$scope.settings.performanceSetting[i].minValue, $scope.settings.performanceSetting[i].maxValue], true);
                    $scope.settings.performanceSetting[i].maxLimit =  $scope.settings.performanceSetting[i].maxLimit;
                }
            }

            /**
             * Function to get FourDotFive Settings
             */
            function getFourDotFiveSettings() {
               // console.log('Getting FourDotFive Settings')
                genericService.getObjects(SETTINGSCONSTANTS.CONTROLLER.GET_FOURDOTFIVE_SETTINGS).then(function (data) {
                   // console.log('Succes FourDotFive Settings fetched');
                    $scope.settings = data;
                    $scope.settingsCopy = data;
                    if($scope.isinHost()){
                    	$scope.settingsCopyForCompany = angular.copy(data);
                    }
                    //  StorageService.set('removeSliderId', $scope.settings.performanceSetting.length +1);
                    $scope.globalSettings = angular.copy($scope.settings);
                    $scope.savedSettings = angular.copy($scope.settings);
                    if ($rootScope.userDetails.company.companyType === 'Host') {
                        $timeout(function () {
                        	//console.log("setvalues click");
                            /*angular.element('#setvalues').trigger('click');*/
                        	$scope.setValues();
                            isGlobalOrCustomSettings();
                        }, 500)
                    } else {
                    	 for(var i=0;i<$scope.settings.performanceSetting.length;i++){
                             $scope.updateMaxLimitForSliders(i,$scope.settings.performanceSetting[i].maxLimit);
                         }
                        updateSettingsInView();
	                     
                    }
                    $scope.isStillProcessing = false;
                }, function (error) {
                	if($rootScope.isOnline){
                    	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    }
                });
            }

            /**
             * 
             */
            $scope.getParentSettings = function () {
                genericService.getObjects(SETTINGSCONSTANTS.CONTROLLER.GET_PARENT_SETTINGS + $rootScope.userDetails.company.companyId).then(function (data) {
            	$scope.isStillProcessing = true;
                    $scope.settings = data;
                    for (i = 0; i < $scope.settings.performanceSetting.length; i++) {
                        $scope.settings.performanceSetting[i].duplicateMaxLImit = $scope.settings.performanceSetting[i].maxLimit;
                        $scope.settings.performanceSetting[i].isSelected = false;
                        $scope.settings.performanceSetting[i].isError = false;
                    }
                    $scope.toggleParentAndChildSettings = angular.copy($scope.settings);
                    $scope.savedSettings = angular.copy($scope.settings);
                    isGlobalOrCustomSettings();
                    updateSettingsInView();
                    $scope.isStillProcessing = false;

                }, function (error) {
                	if($rootScope.isOnline){
                    	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    }
                });
            }

            /**
           * This function is to get Settings for the Staffing/Corporate company or Client/Bu.
           */
            $scope.getSettingsForStaffingOrCorporateCompany = function () {
            	$scope.isStillProcessingModified = true;
                genericService.getObjects(SETTINGSCONSTANTS.CONTROLLER.GET_CORPORATE_OR_STAFFING_SETTINGS + $rootScope.userDetails.company.companyId).then(function (data) {
                    $scope.settings = data;
                    $scope.settingsCopyForCompany = angular.copy(data);
                    for (i = 0; i < $scope.settings.performanceSetting.length; i++) {
                        $scope.settings.performanceSetting[i].duplicateMaxLImit = $scope.settings.performanceSetting[i].maxLimit;
                        $scope.settings.performanceSetting[i].isSelected = false;
                        $scope.settings.performanceSetting[i].isError = false;
                    }
                    $scope.toggleParentAndChildSettings = angular.copy($scope.settings);
                    $scope.savedSettings = angular.copy($scope.settings);
                    $scope.savedSettings123 = angular.copy($scope.settings);
                    if (!($scope.showClientsOrBusList)) {
                        $timeout(function () {
                            angular.element('#setvalues').trigger('click');
                            isGlobalOrCustomSettings();
                        }, 500)
                    }
                    $scope.isStillProcessingModified = false;
                }, function (error) {
                	if($rootScope.isOnline){
                    	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    }
                });
            }

            function isGlobalOrCustomSettings() {
            	$scope.isStillProcessing = true;
                if ($scope.settings.global && $rootScope.userDetails.company.companyType != 'Host') {
                    angular.element('.slider-range').attr({ 'disabled': true }); //Disabling double headed sliders.
                    angular.element('.noUi-target').attr({ 'disabled': true }); //Disabling single headed sliders.
                    angular.element('.onoffswitch').addClass('avoid-clicks'); //Disabling switches.
                } else {
                    for (var index = 0; index < $scope.settings.licensePreferenceSetting.length; index++) {
                        if ($scope.settings.licensePreferenceSetting[index].isEnabled) {
                            angular.element('.slider-setting-' + index).attr({ 'disabled': false });
                            angular.element('.slider-setting-timeToComplete-' + index).attr({ 'disabled': false });
                        }else{
                            angular.element('.slider-setting-' + index).attr({ 'disabled': true });
                            angular.element('.slider-setting-timeToComplete-' + index).attr({ 'disabled': true });
                        }
                    }
                }
            	$scope.isStillProcessing = false;
            }

            /**
             * Function to get Setting for Client/Bu
             */
            $scope.getSettingsForClient = function (client) {
            	$scope.isStillProcessing = true;
            	$scope.toggleClientValue = client;
            	if(client.companyType === "Client"){
            		$scope.selectedClientNameForMessage = client.companyName;
            	}else if(client.companyType === "BusinessUnit"){
            		$scope.selectedBUNameForMessage = client.companyName;
            	}
            	//if(!$scope.isClientGlobalCopy){
            		//$filter('clientorbuModified')(client.companyName, $scope.isClientGlobalCopy);
            	//}
            	$scope.ischeckboxValueNullorNot = document.getElementById("checkbox-inl-1");
        	    $scope.modifySettingsClientModel=false;
                $scope.globalSettingsClientModel=false;
                document.getElementById('modifySettingsClient').checked = false;
                document.getElementById('globalSettingsClient').checked = false;
                  
                $scope.client = client;
                if ($scope.availableClientsToCopy.length != 0) {
                    $scope.availableClientsToCopy = [];
                    $scope.allClientsOrBus.forEach(function (element) {
                        if (element.companyId != client.companyId) {
                            $scope.availableClientsToCopy.push(element);
                        } else {
                            console.log('Duplicate : ' + element.companyName + ' not added');
                        }
                    }, this);
                }
                genericService.getObjects(SETTINGSCONSTANTS.CONTROLLER.GET_CLIENT_OR_BU_SETTINGS + $rootScope.userDetails.company.companyId + '/' + client.companyId).then(function (data) {
                    $scope.settings = data;
                    $scope.isClientGlobal = data.global;
                    $scope.isClientGlobalCopy = $scope.isClientGlobal;
                    $scope.settingsClient = angular.copy($scope.settings);
                    $scope.savedSettings = angular.copy($scope.settings);
                    $scope.copyChecked = false;
                    $scope.isClientCopyObject = false;
                    $scope.clientCopyMandatoryMessage = false;
                    /*for(i=0; i < $scope.allClientsOrBus.length; i++){
                    	$scope.allClientsOrBus[i].clientCompanyNameCopy = $scope.allClientsOrBus[i].companyName;
                    }*/
                    //$scope.client.clientCompanyNameCopy = $filter('clientorbuModified')($scope.client.companyName, $scope.isClientGlobalCopy);
                    isGlobalOrCustomSettings();
                    updateSettingsInView();
                    if(!$scope.viewonlymode){
                    	getClientsOrBusHavingCustomSettings();
                    }
                    //$scope.isStillProcessing = false;
                }, function (error) {
                    if($rootScope.isOnline){
	                	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
	                }
                });
            }
            
            $scope.getClientSettings = function(selectedclient){
            	if(angular.isUndefined(selectedclient)){
            		return;
            	}else {
            		$scope.getSettingsForClient(selectedclient);
            	}
            };

            /**
             * This function is to get setting from the client from which settings are to be copied.
             */
            $scope.getSettingsForClientToCopy = function (clientCopy) {
            	$scope.isStillProcessing = true;
            	$scope.toggleClientValueCopy = clientCopy;
            	$scope.isClientCopyObject = true;
            	$scope.clientCopyMandatoryMessage = false;
            	if(clientCopy.company.companyType === "Client"){
            		$scope.selectedClientNameForMessageCopy = clientCopy.company.name;
            	}else if(clientCopy.company.companyType === "BusinessUnit"){
            		$scope.selectedBUNameForMessageCopy = clientCopy.company.name;
            	}        
  //              console.log('Selected Client: ' + angular.toJson(clientCopy));
                $scope.clientCopy = clientCopy;
                if (angular.isDefined(clientCopy)) {
                    genericService.getObjects(SETTINGSCONSTANTS.CONTROLLER.GET_CLIENT_OR_BU_SETTINGS + $rootScope.userDetails.company.companyId + '/' + clientCopy.company.id).then(function (data) {
                        $scope.settings = data;
                        $scope.savedSettings = data;
                        isGlobalOrCustomSettings();
                        updateSettingsInView();
                    }, function (error) {
                    	if($rootScope.isOnline){
                        	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                        }
                    });
                }
                //$scope.isStillProcessing = false;
            }

            /**
             * Function to get Setting for Client/Bu
             *//*
            $scope.getSettingForClient = function (clientCopy) {
                $scope.clientCopy = clientCopy;
            }*/

            /**
             * Function to limit characters in name.
             */
            function limitCharactersInName(nameToBeDelimited) {
                delimitedCompanyOrClientName = nameToBeDelimited;
                if (nameToBeDelimited.length > 15) {
                    delimitedCompanyOrClientName = nameToBeDelimited.substring(0, 15) + "...";
                }
            }
            
            $scope.changeClientSettingsBeforeSave = function(){
            	$scope.savedSettings = angular.copy($scope.settings);
                for (i = 0; i < $scope.settings.performanceSetting.length; i++) {
                    delete $scope.settings.performanceSetting[i].duplicateMaxLImit;
                    delete $scope.settings.performanceSetting[i].isSelected;
                    delete $scope.settings.performanceSetting[i].isError;

                    delete $scope.savedSettings.performanceSetting[i].duplicateMaxLImit;
                    delete $scope.savedSettings.performanceSetting[i].isSelected;
                    delete $scope.savedSettings.performanceSetting[i].isError;
                }
                for (var j = 0; j < $scope.settings.performanceSetting.length; j++) {
                    minMaxValues = $('.slider-range-' + j).val();
                    $scope.savedSettings.performanceSetting[j].minValue = Number(minMaxValues[0]);
                    $scope.savedSettings.performanceSetting[j].maxValue = Number(minMaxValues[1]);
                }
            	 //Updating the score values from view.
                for (var j = 0; j < $scope.settings.licensePreferenceSetting.length; j++) {
                	if($scope.settings.licensePreferenceSetting[j].score != null){
                		$scope.savedSettings.licensePreferenceSetting[j].score = Number($('.slider-setting-' + j).val());
                	}
                   // console.log( $scope.savedSettings.licensePreferenceSetting[j].score, $scope.settings.licensePreferenceSetting[j].score);
                }
              //Updating the time to complete days from view.
                for (var j = 0; j < $scope.settings.licensePreferenceSetting.length; j++) {
                	if($scope.settings.licensePreferenceSetting[j].timeToComplete != null){
                    $scope.savedSettings.licensePreferenceSetting[j].timeToComplete = Number($('.slider-setting-timeToComplete-' + j).val());
                	}
                }
            }
            
            
            var changeClientName = function(){
            	genericService.getObjects(SETTINGSCONSTANTS.CONTROLLER.GET_CLIENTS_OR_BUS +  $rootScope.userDetails.id + '/'+ $rootScope.userDetails.company.companyId).then(function (data) {
            		for(var i = 0; i < data.clientOrBUList.length; i++){
            			for(var k = 0; k < $scope.allClientsOrBus.length; k++){
            				if($scope.allClientsOrBus[k].companyId === data.clientOrBUList[i].companyId){
            					$scope.allClientsOrBus[k].aliasName = data.clientOrBUList[i].aliasName;
            					if($scope.allClientsOrBus[k].companyId === $scope.client.companyId){
                    				$scope.client.aliasName = $scope.allClientsOrBus[k].aliasName;
                    			}
            				}
            			}
            		}
            	}, function (error) {
                	if($rootScope.isOnline){
                    	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    }
                });
            }
            
            /**
             * This function is to set Settings for the Staffing/Corporate company or Client/Bu.
             */
            $scope.setSettingsForClientorBU = function () {
                if(!(($scope.client != null) && $scope.copyChecked && ($scope.clientCopy === null))){
                	$scope.changeClientSettingsBeforeSave();
                	$scope.clientCopyMandatoryMessage = false;
                if (!$scope.settingsClient.global && angular.equals($scope.savedSettings, $scope.settingsClient)) {
                    //Nothing to be saved.
                    alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.NOTHING_TO_SAVE, 'success');
                } else if ($scope.globalSettingsClientModel) {
                    //Delete global for client and save new settings for client.
                    genericService.deleteObject(SETTINGSCONSTANTS.CONTROLLER.DELETE_SETTINGS + '/' + $rootScope.userDetails.company.companyId + '/' + $scope.client.companyId ).then(function (data) {
                        console.log('Deleted Custom Settings : ' + angular.toJson(data));
                        console.log('Deleted Custom Settings : ' + angular.toJson($scope.client));
                        $scope.modifySettingsClientModel=false;
                        $scope.globalSettingsClientModel=false;
                        document.getElementById('modifySettingsClient').checked = false;
                        document.getElementById('globalSettingsClient').checked = false;
                        changeClientName();
                        $scope.getSettingsForClient($scope.client);
                        alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.SETTINGS.SETTINGS_FOR + '' + $scope.client.companyType + " '<b><i class='wrap-word'>" + $scope.client.companyName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.SETTINGS.UPDATE_SUCCESS, 'success')
                    }, function (error) {
                    	if($rootScope.isOnline){
                        	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                        }
                    	$scope.cancel();
                    });
                } else if ($scope.modifySettingsClientModel && ($scope.settingsClient.global || !angular.equals($scope.savedSettings, $scope.settingsClient))) {
                    //Update custom settings.
                    console.log('Update custom settings for client.');
                    genericService.putObject(SETTINGSCONSTANTS.CONTROLLER.SET_SETTINGS + $scope.client.companyId, $scope.savedSettings).then(function (data) {
                        console.log('Update Custom Settings : ' + angular.toJson(data));
//                        $scope.modifySettingsClientModel=false;
//                        $scope.globalSettingsClientModel=false;
//                        document.getElementById('modifySettingsClient').checked = false;
//                        document.getElementById('globalSettingsClient').checked = false;
                        if($scope.copyChecked){
                    		document.getElementById("checkbox-inl-1").checked = false;
                    		$scope.copyChecked = false;
                    	}
                        $scope.settings = data;
                        $scope.isClientGlobal = data.global;
                        $scope.isClientGlobalCopy = $scope.isClientGlobal;
                        $scope.settingsClient = angular.copy($scope.settings);
                        $scope.savedSettings = angular.copy($scope.settings);

                        $scope.isClientCopyObject = false;
                        $scope.clientCopyMandatoryMessage = false;
                        
                        changeClientName();
                        alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.SETTINGS.SETTINGS_FOR + '' + $scope.client.companyType + " '<b><i class='wrap-word'>" + $scope.client.companyName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.SETTINGS.UPDATE_SUCCESS, 'success')
                    }, function (error) {
                    	if($rootScope.isOnline){
                        	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                        }
                    	$scope.cancel();
                    });
                }
                }else{
                	showCopyClientValidationAlert();
                	//$scope.clientCopyMandatoryMessage = true;
                }
            }
            
            
            $scope.changeCompanySettingsBeforeSave = function(){
            	 $scope.savedSettings = angular.copy($scope.settings);
             	//Update custom settings.
                  for (i = 0; i < $scope.settings.performanceSetting.length; i++) {
                      delete $scope.settings.performanceSetting[i].duplicateMaxLImit;
                      delete $scope.settings.performanceSetting[i].isSelected;
                      delete $scope.settings.performanceSetting[i].isError;
                      delete $scope.settings.performanceSetting[i].isErrorForMax;

                      delete $scope.savedSettings.performanceSetting[i].duplicateMaxLImit;
                      delete $scope.savedSettings.performanceSetting[i].isSelected;
                      delete $scope.savedSettings.performanceSetting[i].isError;
                      delete $scope.savedSettings.performanceSetting[i].isErrorForMax;
                  }
                  for (var j = 0; j < $scope.settings.performanceSetting.length; j++) {
                      minMaxValues = $('.slider-range-' + j).val();
                      $scope.savedSettings.performanceSetting[j].minValue = Number(minMaxValues[0]);
                      $scope.savedSettings.performanceSetting[j].maxValue = Number(minMaxValues[1]);
                  }
              	 //Updating the score values from view.
                  for (var j = 0; j < $scope.settings.licensePreferenceSetting.length; j++) {
                  	if($scope.settings.licensePreferenceSetting[j].score != null){
                  		$scope.savedSettings.licensePreferenceSetting[j].score = Number($('.slider-setting-' + j).val());
                  	}
                     // console.log( $scope.savedSettings.licensePreferenceSetting[j].score, $scope.settings.licensePreferenceSetting[j].score);
                  }
                //Updating the time to complete days from view.
                  for (var j = 0; j < $scope.settings.licensePreferenceSetting.length; j++) {
                  	if($scope.settings.licensePreferenceSetting[j].timeToComplete != null){
                      $scope.savedSettings.licensePreferenceSetting[j].timeToComplete = Number($('.slider-setting-timeToComplete-' + j).val());
                  	}
                  }
                  if($scope.isinHost()){
                	  console.log("Entered license preference host");
                	  var minMaxValues;
                  	//Updating the range values from view.
                      for (var j = 0; j < $scope.settings.performanceSetting.length; j++) {
                    	  console.log("Entered license preference host");
                          minMaxValues = $('.slider-range-' + j).val();
                          $scope.savedSettings.performanceSetting[j].minValue = Number(minMaxValues[0]);
                          $scope.savedSettings.performanceSetting[j].maxValue = Number(minMaxValues[1]);
                      }
                  }
            }

            $scope.setSettingsForCompany = function () {
            	console.log("ENtered set settings for company");
            	 $scope.changeCompanySettingsBeforeSave();
            	 if (!$scope.settingsCopyForCompany.global && angular.equals($scope.savedSettings, $scope.settingsCopyForCompany)) {
            		   //Nothing to be saved.
	                   console.log('Nothing to be saved for company.');
	                   alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.NOTHING_TO_SAVE, 'success');
            	 }else if(!$scope.isinHost() && $scope.globalSettingsCompanyModel){
            		 console.log('Delete global for company and save new settings for company.'+angular.toJson($rootScope.userDetails.company));
            		 genericService.deleteObject(SETTINGSCONSTANTS.CONTROLLER.DELETE_SETTINGS_STAFFING_OR_CORP + '/' + $rootScope.userDetails.company.companyId).then(function (data) {
                         console.log('Deleted Custom Settings company: ' + angular.toJson(data));
                         $scope.modifySettingsCompanyModel=false;
                         $scope.globalSettingsCompanyModel=false;
                         document.getElementById('modifySettingsCompany').checked = false;
                       	 document.getElementById('globalSettingsCompany').checked = false;
                         alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.SETTINGS.SETTINGS_FOR_COMPANY + " '<b><i class='wrap-word'>" + $rootScope.userDetails.company.companyName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.SETTINGS.UPDATE_SUCCESS, 'success');
                         genericService.getObjects(SETTINGSCONSTANTS.CONTROLLER.GET_CORPORATE_OR_STAFFING_SETTINGS + $rootScope.userDetails.company.companyId).then(function (data) {
                             $scope.settings = data;
                             $scope.settingsCopyForCompany = angular.copy(data);
                             for (i = 0; i < $scope.settings.performanceSetting.length; i++) {
                                 $scope.settings.performanceSetting[i].duplicateMaxLImit = $scope.settings.performanceSetting[i].maxLimit;
                                 $scope.settings.performanceSetting[i].isSelected = false;
                                 $scope.settings.performanceSetting[i].isError = false;
                             }
                             $scope.toggleParentAndChildSettings = angular.copy($scope.settings);
                             $scope.savedSettings = angular.copy($scope.settings);
                             $scope.savedSettings123 = angular.copy($scope.settings);
                         }, function (error) {
                        	 if($rootScope.isOnline){
                             	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                             }
                        	 $state.go('missioncontrol.dashboard', null , { reload: true });
                         });
            		 
            		 }, function (error) {
            			 if($rootScope.isOnline){
                         	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                         }
                     	$scope.cancel();
            		 });
            	 } else if ($scope.modifySettingsCompanyModel && ($scope.settingsCopyForCompany.global || !angular.equals($scope.savedSettings, $scope.settingsCopyForCompany))) {
                   console.log('Update custom settings for company.');
                   genericService.putObject(SETTINGSCONSTANTS.CONTROLLER.SET_SETTINGS + $rootScope.userDetails.company.companyId, $scope.savedSettings).then(function (data) {
                       console.log('Update Custom Settings : ' + angular.toJson(data));
                       $scope.settings = data;
                       $scope.settingsCopyForCompany = angular.copy($scope.settings);
                       $scope.savedSettings = angular.copy($scope.settings);
                       if(!$scope.isinHost()){
//                    	     $scope.modifySettingsCompanyModel=false;
//                           $scope.globalSettingsCompanyModel=false;
//                           document.getElementById('modifySettingsCompany').checked = false;
//                           document.getElementById('globalSettingsCompany').checked = false;
                           $scope.toggleParentAndChildSettings = angular.copy($scope.settings);
                       }else{
                    	   $scope.modifySettingsCompanyModel=true;
                    	   $scope.settingsCopy = data;
                    	   $scope.globalSettings = angular.copy($scope.settings);
                       }
                       alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.SETTINGS.SETTINGS_FOR_COMPANY + " '<b><i class='wrap-word'>" + $rootScope.userDetails.company.companyName + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.SETTINGS.UPDATE_SUCCESS, 'success');
                   }, function (error) {
                	   if($rootScope.isOnline){
                       		alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                       }
                   	   $scope.cancel();
                   });
            	 }
          }
            
            $scope.setSettings = function(){
            	if($scope.companySelectionActive){
            		$scope.setSettingsForCompany();
            	}else {
            		$scope.setSettingsForClientorBU();
            	}
            }

            /**
             * This function is to get all the clients/bus for the Staffing/Corporate company.
             */
            function getClientsOrBus() {
            	$scope.isStillProcessing = true;
                console.log('Getting Clients/Bus');
                genericService.getObjects(SETTINGSCONSTANTS.CONTROLLER.GET_CLIENTS_OR_BUS +  $rootScope.userDetails.id + '/'+ $rootScope.userDetails.company.companyId).then(function (data) {
                    $scope.isClientExists = !data.disabled;
                	$scope.allClientsOrBus = angular.copy(data.clientOrBUList);
                    if($scope.allClientsOrBus.length > 0){
                    	$scope.client = $scope.allClientsOrBus[0];
                    	console.log('assigned first client : ' + angular.toJson($scope.client));
                    	$scope.getSettingsForClient($scope.client);
                    	$scope.checkValuesClientsOrBus = data;
                    	$scope.allClientsOrBusForCopy = data.clientOrBUList;
                    }else{
                    	$scope.isStillProcessing = false;
                    }
                }, function (error) {
                	if($rootScope.isOnline){
                    	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    }
                });
            }

            /**
             * This function is to get all the clients/bus for the Staffing/Corporate company.
             */
            function getClientsOrBusHavingCustomSettings() {
            	$scope.isStillProcessing = true;
                console.log('Getting Clients/Bus with custom settings');
                genericService.getObjects(SETTINGSCONSTANTS.CONTROLLER.GET_CLIENTS_OR_BUS_HAVING_CUSTOM_SETTINGS + $rootScope.userDetails.company.companyId).then(function (data) {
                	console.log('Getting Clients/Bus with custom settings api success');
                	data.length == 0 ? $scope.copyClientCheckBox = false : '';
                	$scope.availableClientsToCopy = [];
                    data.forEach(function (element) {
                        if (element.company.id != $scope.client.companyId) {
                            $scope.availableClientsToCopy.push(element);
                            //console.log($scope.availableClientsToCopy.length)
                            $scope.copyClientCheckBox = true;
                        } else {
//                            console.log('Duplicate : ' + element.company.name + ' not added');
                        }
                    }, this);
                    if($scope.availableClientsToCopy.length < 1){
                    	 $scope.copyClientCheckBox = false;
                    }
                    $scope.isStillProcessing = false;
                    console.log('loaded available clients to copy dropdown');
                }, function (error) {
                	if($rootScope.isOnline){
                    	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    }
                });
            }

			/**
			 * Function to update maxlimit for sliders.
             * Range slider is $index starts from 0
			 */
            $scope.updateMaxLimitForSliders = function (rangeSlider, maxLimit) {
           //     console.log(rangeSlider + 'New maxLimit: ' + maxLimit);
           //     console.log('Presently: ' + $scope.settings.performanceSetting[rangeSlider].maxLimit);
                if ((maxLimit >= $scope.settings.performanceSetting[rangeSlider].maxValue) && (maxLimit <= 200) && ($scope.settings.performanceSetting[rangeSlider].maxValue !== 0)) {
                    $scope.settings.performanceSetting[rangeSlider].isSelected = false;
                    $scope.settings.performanceSetting[rangeSlider].maxLimit = maxLimit;

                    var sliderIdInUI = $scope.settings.performanceSetting.length + rangeSlider + 3;
                    console.log('slider changing is : ' + sliderIdInUI);
                    StorageService.set('removeSliderId', sliderIdInUI);
                    /*if($scope.isinHost()){
                    }*/
                   // console.log('Removing Slider with Id: ' + sliderIdInUI);
                    $('#' + sliderIdInUI).remove();
                    var performanceSliderValues = $scope.settings.performanceSetting;
                    //performanceSliderValues[0].maxLimit = maxLimit;
                    $('.slider-range-' + rangeSlider).noUiSlider({
                        range: [0, maxLimit],
                        start: [performanceSliderValues[rangeSlider].minValue, performanceSliderValues[rangeSlider].maxValue],
                        step: 1,
                        connect: true,
                        /*slide: function () {
                            var values = $(this).val();
                            $(this).next('span').text(values[0] + ' - ' + values[1]).addClass('sliderBold');
                        },
                        set: function () {
                            var values = $(this).val();
                            $(this).next('span').text(values[0] + ' - ' + values[1]).removeClass('sliderBold');
                            //console.log('Set PerformanceSetting: ' + values[0] + ' - ' + values[1]);
                        }*/
                        slide: function () {
                            var values = $(this).val();
                            var fromLabel = $(this).next('p').find('.fromLabel').text();
                            var toLabel = $(this).next('p').find('.toLabel').text()

                            if (fromLabel != '' && fromLabel != values[0]) {
                                $(this).next('p').find('.fromLabel').text(values[0]).addClass('sliderBold');
                            } else if (toLabel != '' && toLabel != values[1]) {
                                $(this).next('p').find('.toLabel').text(values[1]).addClass('sliderBold');
                            }
                        },
                        set: function () {
                            var values = $(this).val();
                            $(this).next('p').find('.fromLabel').text(values[0]).removeClass('sliderBold');
                            $(this).next('p').find('.toLabel').text(values[1]).removeClass('sliderBold');
                        }
                    });
                   // console.log('Updating values for slider: ' + rangeSlider);
                    $('.slider-range-' + rangeSlider).val([performanceSliderValues[rangeSlider].minValue, performanceSliderValues[rangeSlider].maxValue], true);
                    $scope.settings.performanceSetting[rangeSlider].isError = false;
                    $scope.settings.performanceSetting[rangeSlider].isErrorForMax = false;
                } else {
                	if(maxLimit > 200){
                		$scope.settings.performanceSetting[rangeSlider].isErrorForMax = true;
                		$scope.settings.performanceSetting[rangeSlider].isError = false;
                	}else{
                		$scope.settings.performanceSetting[rangeSlider].isError = true;
                		$scope.settings.performanceSetting[rangeSlider].isErrorForMax = false;
                	}
                }
            }

			/**
			 * Function to cancel(hide the save and cancel buttons that appear on edit of maxlimit).
			 */
            $scope.cancelTimeForSliders = function (rangeSlider) {
                $scope.settings.performanceSetting[rangeSlider].isSelected = false;
                $scope.settings.performanceSetting[rangeSlider].isError = false;
                $scope.settings.performanceSetting[rangeSlider].isErrorForMax = false
                $scope.settings.performanceSetting[rangeSlider].duplicateMaxLImit = $scope.settings.performanceSetting[rangeSlider].maxLimit;
            }

			/**
			 * Function to toggle edit options for range sliders.
			 */
            $scope.toggleEditOptionForSliders = function (rangeSliderIndex) {
             //   console.log('Edit Range maxlimit: ' + rangeSliderIndex);
                $scope.settings.performanceSetting[rangeSliderIndex].duplicateMaxLImit = $scope.settings.performanceSetting[rangeSliderIndex].maxLimit;
                $scope.settings.performanceSetting[rangeSliderIndex].isSelected = true;
            }

            $scope.isCopyChecked = function () {
            	//$scope.modifySettingsClientModel=false;
                $scope.globalSettingsClientModel=false;
                $scope.isClientCopyObject = false;
                $scope.clientCopyMandatoryMessage = false;
                $scope.availableClientsToCopy = [];
                $scope.clientCopy = null;
                $scope.copyChecked = document.getElementById("checkbox-inl-1").checked;
                if($scope.copyChecked){
                	getClientsOrBusHavingCustomSettings();
                }else{
                	$scope.settings = angular.copy($scope.settingsClient);
                	$scope.savedSettings = angular.copy($scope.settingsClient);
                	 isGlobalOrCustomSettings();
                     updateSettingsInView();
                }
                /*$scope.allClientsOrBus.forEach(function (element) {
                    if (element.company.id != $scope.client.company.id) {
                        $scope.availableClientsToCopy.push(element);
                    } else {
                        console.log('Duplicate : ' + element.company.name + ' not added');
                    }
                }, this);*/
//                $timeout(function () {
//                   // console.log('Items to copy array: ' + angular.toJson($scope.availableClientsToCopy));
//                    if($scope.copyChecked){
//                    	$scope.clientCopy = $scope.availableClientsToCopy[0];
//                    	$scope.getSettingsForClientToCopy($scope.clientCopy);
//                    }else{
//                    	$scope.getSettingsForClient($scope.client);
//                    }
//                }, 50)
            }

//            $scope.$on('$stateChangeStart', function (event, toState, fromState, toParams) {
//                console.log('Moving out.................');
//                StorageService.remove('removeSliderId');
//                StorageService.set('outOfPage', 'movingOut');
//            });
            
            

            $scope.getSettings = function () {
                if ($scope.isinHost()) {
                    console.log('Get 4dot5 Settings');
                    $scope.modifySettingsCompanyModel = true;
                    getFourDotFiveSettings();
                } else {
                    console.log('Get Staffing or corporate company Settings');
                    $scope.getSettingsForStaffingOrCorporateCompany();
                }
            }
            
            var showCopyClientValidationAlert = function () {
            	if($scope.isinStaffing()){
            		var copyMessage = 'Please select a Client to copy from.';
            	}else{
            		var copyMessage = 'Please select a BU to copy from.';
            	}
                bootbox.alert({ 
                    closeButton: false,
                    title: '<div class="alert alert-danger"><i class="fa fa-times-circle fa-fw fa-lg"></i><strong>Oh snap!</strong></div>',
                    message: copyMessage,
                    callback: function () {
                    }
                });
            }

            $scope.getSettings();
        }
    ]);