
var teammembercandidatesmodule = angular.module('4dot5.missioncontrolmodule.teammembercandidatesmodule', []);

teammembercandidatesmodule.constant('TEAMMEMBERCANDIDATESCONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.teammembercandidates',
        URL: '/teammembercandidates',
        CONTROLLER: 'TeammembercandidatesController',
        TEMPLATEURL: 'app/partials/missioncontrol/teammembercandidates/teammembercandidates.html',
    },
    CONTROLLER: {

    }
});
teammembercandidatesmodule.config(
    ['$stateProvider',
        'TEAMMEMBERCANDIDATESCONSTANTS',
        function($stateProvider, TEAMMEMBERCANDIDATESCONSTANTS) {
            $stateProvider.state(TEAMMEMBERCANDIDATESCONSTANTS.CONFIG.STATE, {
                url: TEAMMEMBERCANDIDATESCONSTANTS.CONFIG.URL,
                templateUrl: TEAMMEMBERCANDIDATESCONSTANTS.CONFIG.TEMPLATEURL,
                controller: TEAMMEMBERCANDIDATESCONSTANTS.CONFIG.CONTROLLER,
                data: {
                    requireLogin: true
                },
                params: {
                	teamMemberName: null,
                	candidateType: null

                }
            });
        }
    ]);

teammembercandidatesmodule.controller('TeammembercandidatesController', ['$scope',
    '$http',
    '$state',
    '$timeout',
    '$rootScope',
    '$stateParams',
    'genericService',
    'StorageService',
    'TEAMMEMBERCANDIDATESCONSTANTS',
    function($scope, $http, $state, $timeout, $rootScope, $stateParams, genericService, StorageService, TEAMMEMBERCANDIDATESCONSTANTS) {

    
	 $scope.teamMemberName = $stateParams.teamMemberName;
	 $scope.candidateType = $stateParams.candidateType;
	 
	 console.log('***'+angular.toJson($scope.teamMemberName));
	 console.log('###'+angular.toJson($scope.candidateType));
	
	 $scope.searchTerm = '';
	    $scope.teamMemberTitle = "Candidates";
	    $scope.candidatesTypeLabel = 'Active';

	    //filters
	    $scope.filterValues = $stateParams.filterValues;
	    $scope.filterType = '';
	    $scope.requisitionRoleFilter = '';
	    $scope.requistionCompanyFilter = '';
	    $scope.teamMemberFilter = '';


	    $scope.candidateNamesList = [];
	    $scope.filteredCandidateList = [];
	    $scope.filteredCandidateNamesList = [];


	    $scope.candidateJobMatchesTemplate = 'app/partials/directivetemplates/candidate-job-matches.html';
	    $scope.candidateNotificationsTemplate = 'app/partials/directivetemplates/candidate-all-notifications.html';
	    $scope.candidateActivityTemplate = 'app/partials/directivetemplates/candidate-activity.html';
	    $scope.currentInfoTemplate = 'app/partials/directivetemplates/candidate-activity-full.html';
	    $scope.currentInfoCandidateName = '';
	    $scope.today = new Date();
	    var oneDayTimeStamp = 1000 * 60 * 60 * 24; // Milliseconds in a day
	    var diff = $scope.today - oneDayTimeStamp;
	    $scope.yesterday = new Date(diff);

	    $scope.softwareDeveloperJobMatches = [
	        {title: 'Software Developer', company: 'The Green Orange Company', score: '87', status: 'Tech Assessment Completed' },
	        {title: 'Software Developer', company: 'The Red Tree Company', score: '83', status: 'Qualified'}
	    ];

	    $scope.managerJobMatches = [
	        {title: 'Manager', company: 'The Red Tree Company', score: '81', status: 'Qualified'}
	    ];

	    $scope.init = function(){

	        $scope.teamMemberCandidatesData = [];
	       /* $scope.teamMemberName = $stateParams.teamMemberName;
	        $scope.candidatesType = $stateParams.candidateType;*/
	        
	        console.log('***'+angular.toJson($scope.teamMemberName));
	   	 	console.log('###'+angular.toJson($scope.candidateType));
	   	

	        // //filters
	        // ctrl.filterValues = ctrl.filterValues.split('|');
	        // //console.log('filterValues - ',ctrl.filterValues);
	        // if(ctrl.filterValues.length){
	        //     ctrl.filterType = ctrl.filterValues[0];
	        //     if(ctrl.filterType === 'requisition'){
	        //         ctrl.requisitionRoleFilter = ctrl.filterValues[1];
	        //         ctrl.requistionCompanyFilter = ctrl.filterValues[2];
	        //     }else if(ctrl.filterType === 'teamMember'){
	        //         ctrl.teamMemberFilter = ctrl.filterValues[1];
	        //     }else{
	        //         //
	        //     }
	        // }

	        if($scope.teamMemberName === 'All'){
	            if($scope.candidatesType === 'Active'){
	                $scope.teamMemberTitle = "Active Candidates";
	            }else{
	                $scope.teamMemberTitle = "Passive Candidates";
	            }
	        }else{
	            $scope.teamMemberTitle = $scope.teamMemberName + "'s Candidates";
	        }
	        
	        if($scope.candidatesType === 'All' || $scope.candidatesType === 'Active'){
	            console.log('in get active');
	            $http.get("data/candidates.json")
	            .success(function(data) {
	                angular.forEach(data,function(val,key){
	                    if($scope.candidateToBeIncluded(val)){
	                        val.ParentState = 'Active';
	                        val.activityPopOverIsOpen = false;
	                        if(val.Role === 'Software Developer'){
	                            val.jobMatchCount = $scope.softwareDeveloperJobMatches.length;
	                            val.jobMatches = $scope.softwareDeveloperJobMatches;
	                        }else{
	                            val.jobMatchCount = $scope.managerJobMatches.length;
	                            val.jobMatches = $scope.managerJobMatches;
	                        }
	                        $scope.teamMemberCandidatesData.push(val);
	                        //$scope.candidateNamesList.push({name: val.Name, ticked: true});
	                    }
	                });
	            });
	        }

	        if($scope.candidatesType === 'Passive' || $scope.candidatesType === 'All'){
	            console.log('in get passive');
	            $scope.candidatesTypeLabel = 'Passive';
	            $http.get("data/passiveCandidates.json")
	            .success(function(data) {
	                 angular.forEach(data,function(val,key){
	                    if($scope.candidateToBeIncluded(val)){
	                        val.ParentState = 'Passive';
	                        val.activityPopOverIsOpen = false;
	                        if(val.Role === 'Software Developer'){
	                            val.jobMatchCount = $scope.softwareDeveloperJobMatches.length;
	                            val.jobMatches = $scope.softwareDeveloperJobMatches;
	                        }else{
	                            val.jobMatchCount = $scope.managerJobMatches.length;
	                            val.jobMatches = $scope.managerJobMatches;
	                        }
	                        $scope.teamMemberCandidatesData.push(val);
	                        //$scope.candidateNamesList.push({name: val.Name, ticked: true});
	                    }
	                });
	            });
	        }
	    }
	    
	    $scope.candidateToBeIncluded = function(candidate){
	        if($scope.teamMemberName === 'All'){
	            return true;
	        }else{
	            return candidate.Recruiter === $scope.teamMemberName;
	        }
	    };

	    $scope.copyAllCandidatesToFiltered = function(){
	        $scope.filteredCandidateNamesList = $scope.candidateNamesList;
	    };

	    $scope.removeItemFromList = function(list, item){
	        for(i=0; i < list.length; i++){
	            if(angular.equals(list[i], item)){
	                list.splice(i,1);
	            }
	        }
	    };

	    $scope.searchCandidates = function (candidate) {
	        // console.log('called -',candidate);
	        return (angular.lowercase(candidate.Name).indexOf(angular.lowercase($scope.searchTerm) || '') !== -1 ||
	                angular.lowercase(candidate.Company).indexOf(angular.lowercase($scope.searchTerm) || '') !== -1 ||
	                angular.lowercase(candidate.Role).indexOf(angular.lowercase($scope.searchTerm) || '') !== -1 || 
	                angular.lowercase(candidate.State).indexOf(angular.lowercase($scope.searchTerm) || '') !== -1);
	    };

	    $scope.showFullActivity = function (event, index, candidate) {
	        $("div[id^='info-row']").hide();
	        $scope.currentInfoTemplate = 'app/partials/directivetemplates/candidate-activity-full.html';
	        $scope.currentInfoCandidate = candidate;
	        $scope.currentInfoCandidateRowId = "info-row-"+Math.ceil(index/4);
	        candidate.activityPopOverIsOpen = false;
	        $("#info-row-"+Math.ceil(index/4)).slideToggle();
	    };

	    $scope.showFullJobMatches = function (event, index, candidate) {
	        $("div[id^='info-row']").hide();
	        $scope.currentInfoTemplate = 'app/partials/directivetemplates/candidate-job-matches-full.html';
	        $scope.currentInfoCandidate = candidate;
	        $scope.currentInfoCandidateRowId = "info-row-"+Math.ceil(index/4);
	        candidate.jobPopOverIsOpen = false;
	        $("#info-row-"+Math.ceil(index/4)).slideToggle();
	    };

	    $scope.showAllNotifications = function (event, index, candidate) {
	        $("div[id^='info-row']").hide();
	        $scope.currentInfoTemplate = 'app/partials/directivetemplates/candidate-all-notifications.html';
	        $scope.currentInfoCandidate = candidate;
	        $scope.currentInfoCandidateRowId = "info-row-"+Math.ceil(index/4);
	        candidate.notificationPopOverIsOpen = false;
	        $("#info-row-"+Math.ceil(index/4)).slideToggle();
	    };

	    $scope.closeInfoRow = function(rowId){
	        $("#" + rowId).slideToggle();
	    };
	        
	    $scope.init();
    }
]);

