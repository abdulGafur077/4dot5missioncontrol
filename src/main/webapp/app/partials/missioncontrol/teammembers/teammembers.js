/**
 * 
 */

var teammembersmodule = angular.module('4dot5.missioncontrolmodule.teammembersmodule', []);

teammembersmodule.constant('TEAMMEMBERSCONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.teammembers',
        URL: '/teammembers',
        CONTROLLER: 'TeamMembersController',
        TEMPLATEURL: 'app/partials/missioncontrol/teammembers/teammembers.html',
    },
    CONTROLLER: {
        VIEW_ALL_USERS: 'api/user/getteammembers',
        VIEW_USER_PROFILE: 'api/user/getuserdetails',
        DELETE_USER: 'api/user/deleteuser',
        REPORTIES: 'api/user/getreportees',
        LOGGED_IN_DETAILS: 'api/user/getloggedinuserdetails',
        CHECK_SCOPE : 'api/user/checkroleentityscope'
    }
});

teammembersmodule.config(
    ['$stateProvider',
        'TEAMMEMBERSCONSTANTS',
        function ($stateProvider, TEAMMEMBERSCONSTANTS) {
            $stateProvider.state(TEAMMEMBERSCONSTANTS.CONFIG.STATE, {
                url: TEAMMEMBERSCONSTANTS.CONFIG.URL,
                templateUrl: TEAMMEMBERSCONSTANTS.CONFIG.TEMPLATEURL,
                controller: TEAMMEMBERSCONSTANTS.CONFIG.CONTROLLER,
                data: {
                    requireLogin: true
                },
                params: {
                    managerObj: null,
                    iconClicked : null
                }
            });
        }
    ]);

teammembersmodule.controller('TeamMembersController', ['$scope',
    '$http',
    '$state',
    '$timeout',
    '$rootScope',
    '$stateParams',
    'genericService',
    'userService',
    'utilityService',
    'TEAMMEMBERSCONSTANTS',
    'MESSAGECONSTANTS',
    'StorageService',
    'teamMembersService',
    'alertsAndNotificationsService',
    '$uibModal',
    function ($scope, $http, $state, $timeout, $rootScope, $stateParams, genericService, userService, utilityService, TEAMMEMBERSCONSTANTS, MESSAGECONSTANTS, StorageService, teamMembersService, alertsAndNotificationsService, $uibModal) {

        $scope.userDetails = {};
        $scope.teamMemberData = [];
        $scope.teamMemberLoaded = false;
        $scope.fromusermgmt = false;
        $scope.managers = [];
        $scope.deletinguser = '';
        
        $scope.listData = function () { //VIEW ALL USERS
            $scope.teamMemberLoaded = false;
            genericService.getObjects(TEAMMEMBERSCONSTANTS.CONTROLLER.LOGGED_IN_DETAILS).then(function (data) {
                $scope.userDetailsCopy = angular.copy(data);
                $scope.managers = [];
                $scope.userDetails = data;
            }, function (error) {
                if($rootScope.isOnline){
                	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                }
            });

            genericService.getObjects(TEAMMEMBERSCONSTANTS.CONTROLLER.VIEW_ALL_USERS + '/' + angular.copy($rootScope.userDetails.company.companyId)).then(function (data) {
                $scope.teamMemberLoaded = true;
                $scope.teamMemberData = data;
            }, function (error) {
            	if($rootScope.isOnline){
                	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                }
            });
        };

        $scope.viewReporties = function (teammember) {
            $scope.teamMemberLoaded = false;
            if (teammember.canDrillDown) {
                genericService.getObjectById(TEAMMEMBERSCONSTANTS.CONTROLLER.REPORTIES + '/' + teammember.userId).then(function (data) {
                    $scope.currentmanager = teammember;
                    if (($scope.managers !== null)) {
                        if(!$scope.fromusermgmt){
                            $scope.managers.push(teammember);
                        }else {
                            $scope.fromusermgmt = false;
                        }
                    } else {
                        $scope.listData();
                    }
                    $scope.teamMemberLoaded = true;
                    $scope.teamMemberData = data;
                    $scope.userDetails = teammember;
                }, function (error) {
                	if($rootScope.isOnline){
                    	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    }
                });
            } else {
                //do nothing
            }
        }
        
        /**
         * This condition executes first in this state
         * This differentiates between the different invocation points of this state(i.e, whether from leftnav icons or from usermanagement state)
         * ($stateParams.iconClicked !== null) only when clicked on leftnav icons
         * ($stateParams.managerObj !== null) only when returned from usermanagement state.
         */
        if (($stateParams.iconClicked !== null) || (angular.equals($stateParams.managerObj, null) || angular.equals($stateParams.managerObj, {}))) {
        	$scope.listData(); // loading initial content
        } else {
            $scope.managerObj = angular.copy($stateParams.managerObj);
            if (($scope.managerObj.roleName === $rootScope.MESSAGECONSTANTS.ROLES.FOURDOTFIVEADMIN) || ($scope.managerObj.roleName === $rootScope.MESSAGECONSTANTS.ROLES.SUPERUSER)) {
            	$scope.listData();
            } else {
                $scope.managers = StorageService.extract('managers', $scope.managers);
                if(!$scope.managers.length){
                    $scope.listData();
                }else {
                    $scope.currentmanager = $scope.managerObj;
                    genericService.getObjects(TEAMMEMBERSCONSTANTS.CONTROLLER.LOGGED_IN_DETAILS).then(function (data) {
                        $scope.userDetailsCopy = angular.copy(data);
                        $scope.userDetails = data;
                        $scope.managerObj.canDrillDown = true;
                        $scope.fromusermgmt = true;
                        $scope.viewReporties($scope.managerObj);
                        $stateParams.managerObj = null;
                    }, function (error) {
                    	if($rootScope.isOnline){
                        	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                        }
                    });
                }
            }
        }


        $scope.roleCheck = function (id) { // for disabling the drill down based on the roles
            return !(id === 'Staffing Sr.Recruiter' || id === 'Staffing Recruiter' || id === 'Corporate Recruiter' || id === 'Corporate Sr.Recruiter');
        }

        $scope.viewProfile = function (user, accessMode) { //view user profile
            utilityService.getUserProfile(user).then(function (data) {
                StorageService.set('managers', $scope.managers);
                $state.go('missioncontrol.usermanagement', { userObject: data, accessMode: accessMode }, { reload: false });
            }, function (error) {
            	if($rootScope.isOnline){
                	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                }
            });
        }

        $scope.deleteSelectedUser = function (user) {
            $scope.deletingUser = user;
            $scope.getDeleteUserInfo(user.userId);
        }

        $scope.getDeleteUserInfo = function (userId) {
            userService.getDeleteUserInfo(userId, function (data) {
                // $scope.deleteUserInfo = data;
                $scope.deletingUser.numberOfCandidates = data.numberOfCandidates;
                $scope.deletingUser.numberOfRequisitions = data.numberOfRequisitions;
                $scope.deletingUser.requisitionDetails = data.requisitionDetails;
                $scope.launchDeleteUserModal();
            },
                function (error) {
                    if($rootScope.isOnline){
                        alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    }
            });
        }

        $scope.launchDeleteUserModal = function () {
            $scope.deleteUserModal = $uibModal.open({
                animation: false,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/partials/missioncontrol/teammembers/delete-user-modal.html',
                controller: ['$uibModalInstance','userInfo',function ($uibModalInstance, userInfo) {
                    var vm = this;
                    vm.userInfo = userInfo;
                    vm.closeModal = function () {
                        $uibModalInstance.close('cancel');
                    };
                }],
                controllerAs: 'deleteUserModal',
                size: 'lg',
                resolve:{
                    userInfo: function () {
                        return $scope.deletingUser;
                    }
                }
            });
        } 

        $scope.addUser = function () {
            genericService.getObjects(TEAMMEMBERSCONSTANTS.CONTROLLER.CHECK_SCOPE + '/' + $rootScope.userDetails.company.companyId).then(function (data) {
                console.log('data : ' + angular.toJson(data));
                if(($scope.managers.length === 0) || ($scope.managers && data)){
                    StorageService.set('managers', $scope.managers);
                    $state.go('missioncontrol.usermanagement', { manager: $scope.userDetails, accessMode: 'create' }, { reload: true });
                }else {
                	alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.USER.DEFINE_SCOPE,'warning');
                }
            }, function (error) {
            	if($rootScope.isOnline){
                	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                }
            });
        }

        var getManagerindex = function (manager) {
            for (var i = 0; i < $scope.managers.length; i++) {
                if ($scope.managers[i].userId === manager.userId) {
                    return i;
                }
            }
            return -1;
        }

        $scope.viewManagerReporties = function (manager) { 
            genericService.getObjectById(TEAMMEMBERSCONSTANTS.CONTROLLER.REPORTIES + '/' + manager.userId).then(function (data) {
                $scope.currentmanager = manager;
                var managerpositioninarray = getManagerindex(manager);
                if (managerpositioninarray !== -1) {
                    $scope.managers.splice((managerpositioninarray + 1), ($scope.managers.length - 1));
                }
                $scope.userDetails = manager;
                $scope.teamMemberData = data;
            }, function (error) {
            	if($rootScope.isOnline){
                	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                }
            });
        }

        $scope.searchUser = function(item){
            if ($scope.searchTerm == undefined) {
                return true;
            }
            else {
                if (item.fullName.toLowerCase().indexOf($scope.searchTerm.toLowerCase()) != -1 ||
                    item.roleDescription.toLowerCase().indexOf($scope.searchTerm.toLowerCase()) != -1) {
                    return true;
                }
            }
            return false;
        }
    }
]);

teammembersmodule.factory('teamMembersService', ['$q',
    '$http',
    'StorageService',
    '$state',
    'TEAMMEMBERSCONSTANTS',
    function ($q, $http, StorageService, $state, TEAMMEMBERSCONSTANTS) {

        var factory = {};
        /**
         * This will allow to login by calling backend API
         */
        factory.deleteUser = function (user) {
            var deferred = $q.defer();
            $http.get(TEAMMEMBERSCONSTANTS.CONTROLLER.DELETE_USER + '/' + user.userId).success(function (data, status, headers, config) {
                if (angular.isDefined(data.statusCode) && ((status !== 500) || (status !== 404))) {
                    if (angular.equals(data.statusCode, "200")) {
                        deferred.resolve(data);
                    } else {
                        deferred.reject(data);
                    }
                } else {
                    if (status === 200) {
                        deferred.resolve(data);
                    } else {
                        deferred.reject(data);
                    }
                }
            }).error(function (data, status, headers, config) {
                console.log('service error : ' + angular.toJson(data));
                deferred.reject(data);
            });
            return deferred.promise;
        };
        return factory;
    }
]);
