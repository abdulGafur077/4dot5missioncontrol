/**
 * 
 */
var transactionsmodule = angular.module('4dot5.missioncontrolmodule.transactionsmodule', ['datatables','ngResource']);

transactionsmodule.constant('TRANSACTIONSCONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.transactions',
        URL: '/transactions',
        CONTROLLER: 'TransactionsController',
        TEMPLATEURL: 'app/partials/missioncontrol/transactions/transactions.html',
    },
    CONTROLLER: {
        TRANSACTION_DETAILS: 'data/transaction.json'
    }
});

transactionsmodule.config(
    ['$stateProvider',
        'TRANSACTIONSCONSTANTS',
        function ($stateProvider, TRANSACTIONSCONSTANTS) {
            $stateProvider.state(TRANSACTIONSCONSTANTS.CONFIG.STATE, {
                url: TRANSACTIONSCONSTANTS.CONFIG.URL,
                templateUrl: TRANSACTIONSCONSTANTS.CONFIG.TEMPLATEURL,
                controller: TRANSACTIONSCONSTANTS.CONFIG.CONTROLLER,
                data: {
                    requireLogin: true
                }
            });
        }
    ]);

transactionsmodule.controller('TransactionsController',
    ['$scope',
        '$state',
        '$timeout',
        '$rootScope',
        '$stateParams',
        'genericService',
        'DTOptionsBuilder',
        'TRANSACTIONSCONSTANTS',
        'StorageService',
        '$resource',
        function ($scope, $state, $timeout, $rootScope, $stateParams, genericService,DTOptionsBuilder, TRANSACTIONSCONSTANTS, StorageService, $resource) {
                console.log('TransactionsController');

                $scope.companyType = $rootScope.userDetails.company.companyType;
                $scope.query = {
                    searchString : ""
                };

                $scope.inputTextbox = false;
                
                //toggle search box
                $scope.toggleInputBox = function () { 
                    $scope.inputTextbox = !$scope.inputTextbox;
                };
            
                //toggle filter box
                $(document).ready(function () { 
                    $("#search-panel").hide();
                    $(document).off("click", "#open-filter").on("click", "#open-filter",
                        function () {
                            $("#search-panel").animate({
                                height: 'toggle'
                            });
                        });
                });
                
                //api call
                $resource('data/transaction.json').query().$promise.then(function(transactions) {
                    $scope.transactions = transactions;
                });               
                
                //data table configuration
                $scope.dtOptions = DTOptionsBuilder.newOptions()
                    .withPaginationType('full_numbers')
                    .withOption('bInfo',true)
                    .withOption('searching',true)
                    .withOption('lengthChange',false)
                    .withOption('bFilter', true)
                    .withOption('language', {zeroRecords: 'No transactions to display'})
                    .withBootstrap();  

            //     $scope.dtOptions = DTOptionsBuilder.newOptions()
            // .withPaginationType('full_numbers')
            
            // .withOption('bLengthChange', false)
            // .withOption('bInfo',true)
              


                // sorting queries    
                $scope.search = function (row) {
                return ((angular.lowercase(row.TransactionId)|| '').indexOf(angular.lowercase($scope.query.searchString) || '') !== -1 ||
                        (angular.lowercase(row.Requisitions)|| '').indexOf(angular.lowercase($scope.query.searchString) || '') !== -1 ||
                        (angular.lowercase(row.ActiveCandidateCount)|| '').indexOf(angular.lowercase($scope.query.searchString) || '') !== -1 ||
                        (angular.lowercase(row.PassiveCandidateCount)|| '').indexOf(angular.lowercase($scope.query.searchString) || '') !== -1 ||
                        (angular.lowercase(row.MatchedCandidateCount)|| '').indexOf(angular.lowercase($scope.query.searchString) || '') !== -1 ||
                        (angular.lowercase(row.NotMatchedCandidateCount)|| '').indexOf(angular.lowercase($scope.query.searchString) || '') !== -1 ||
                        (angular.lowercase(row.NotProcessedCount)|| '').indexOf(angular.lowercase($scope.query.searchString) || '') !== -1);
            };    
        }
    ]);