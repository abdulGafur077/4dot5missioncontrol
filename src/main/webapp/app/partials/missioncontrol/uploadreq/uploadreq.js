var uploadreqmodule = angular.module('4dot5.missioncontrolmodule.uploadreqmodule', []);

uploadreqmodule.constant('UPLOADREQCONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.uploadreq',
        URL: '/uploadreq',
        CONTROLLER: 'UploadReqController',
        CONTROLLERAS: 'uploadReq',
        TEMPLATEURL: 'app/partials/missioncontrol/uploadreq/uploadreq.html'
    }
});

uploadreqmodule.config(['$stateProvider', 'UPLOADREQCONSTANTS',
    function ($stateProvider, UPLOADREQCONSTANTS) {
        $stateProvider.state(UPLOADREQCONSTANTS.CONFIG.STATE, {
            url: UPLOADREQCONSTANTS.CONFIG.URL,
            templateUrl: UPLOADREQCONSTANTS.CONFIG.TEMPLATEURL,
            controller: UPLOADREQCONSTANTS.CONFIG.CONTROLLER,
            controllerAs: UPLOADREQCONSTANTS.CONFIG.CONTROLLERAS,
            data: {
                requireLogin: true
            },
            params: {
                jobId: null
            }
        });
    }
]);

uploadreqmodule.controller('UploadReqController',UploadReqController);

uploadreqmodule.$inject = ['$scope', '$state', '$stateParams', '$rootScope', '$uibModal', '$sce', '$timeout', 'Requisition', 'requisitionService', 'resumeService',
    'Job', 'jobService', 'assessmentService','planService','alertsAndNotificationsService', 'UiError','MESSAGECONSTANTS', 'UPLOADREQCONSTANTS'];

function UploadReqController ($scope, $state, $stateParams, $rootScope, $uibModal, $sce, $timeout, Requisition, requisitionService, resumeService,
    Job, jobService, assessmentService, planService, alertsAndNotificationsService, UiError, MESSAGECONSTANTS, UPLOADREQCONSTANTS) {

    var vm = this;
    vm.MESSAGECONSTANTS = MESSAGECONSTANTS;
    vm.UPLOADREQCONSTANTS = UPLOADREQCONSTANTS;
    vm.clientorbulist = {};
    vm.recruiterList = [];
    vm.selectedRecruiters = [];
    vm.jobTypeList = [];
    vm.selectedJobTypes = [];
    vm.sponsorshipList = [];
    vm.selectedSponsorships = [];
    vm.techAssessmentList = [];
    vm.valueAssessmentList = [];
    vm.jobBoardDetails = [];
    vm.jobBoardDetailsCopy = [];
    vm.resumesToPullPerJobBoardCountCopy = 0;
    vm.currentScoreCandidateCounts = {};
    vm.adjustedScoreCandidateCounts = {};
    vm.candidatesBasedOnRequisitionTransaction = [];
    vm.req = {};
    vm.job = {};
    vm.reqUploadDropzone = {};
    vm.uploadReqWizard = {};
    vm.currentJobStatus = null;
    vm.matchTypesArray = [];
    vm.resumePullCounts = [5, 10, 15, 20, 25, 30, 40, 50, 75, 100];
    vm.radiusOptions = [];
    vm.recencyOptions = [];
    vm.jobBoardParameters = {};
    vm.jobBoardParametersMatrix = [];
    //preference objects
    vm.intelligencePreferences = {};
    vm.techAssessmentPreferences = {};
    vm.valueAssessmentPreferences = {};
    vm.verificationPreferences = {};
    // flags
    vm.editModeFlag = false;
    vm.clientOrBUIsRequiredFlag = true;
    vm.disableUploadButtonFlag = true;
    vm.fileExistsFlag = false;
    vm.savingFlag = false;
    vm.step2DetailsSavedFlag = false;
    vm.workflowStepsSavedFlag = false;
    vm.jobBoardsSettingsSavedFlag = false;
    vm.disablePrevButtonFlag = false;
    vm.disableSaveButtonFlag = true;
    vm.disableNextButtonFlag = true;
    vm.disableCancelButtonFlag = false;
    vm.disableAllButtonsFlag = false;
    vm.fileUploadInProgressFlag = false;
    vm.showSponsorshipDetailsFlag = false;
    vm.loadingAdjustedCandidatesCountFlag = false;
    vm.step2RequestWasInProgressFlag = false;
    vm.step3RequestWasInProgressFlag = false;
    vm.step4RequestWasInProgressFlag = false;
    vm.step5RequestWasInProgressFlag = false;
    vm.editTitleFlag = false;
    vm.editJobLocationFlag = false;
    vm.editBUFlag = false;
    vm.isResumePullEnabled = false;
    vm.isJobBoardSelected = false;
    vm.isJobIntegrationEnabledByPlan = false;
    // temp objects for comparison
    vm.step2DetailsTempObject = {};
    vm.workflowStepsTempObject = {};
    vm.jobBoardsSettingsTempObject = {};
    //error objects
    vm.step1Error = null;
    vm.step2Error = null;
    vm.step3Error = null;
    vm.step4Error = null;
    vm.step5Error = null;
    //controller methods
    vm.initialize = initialize;
    vm.createReqUploadDropzone = createReqUploadDropzone;
        // get data methods
    vm.getPlanDetails = getPlanDetails;
    vm.getAllClientsOrBu = getAllClientsOrBu;
    vm.getAllJobTypes = getAllJobTypes;
    vm.getAllSponsorshipTypes = getAllSponsorshipTypes;
    vm.getAllTechAssessments = getAllTechAssessments;
    vm.getAllValueAssessments = getAllValueAssessments;
    vm.getAllJobBoards = getAllJobBoards;
    vm.getRecruiters = getRecruiters;
    vm.getLicensePreferences = getLicensePreferences;
    vm.getJob = getJob;
    vm.getJobBoardsSearchJobStatus = getJobBoardsSearchJobStatus;
    vm.getResumesFromJobBoards = getResumesFromJobBoards;
    vm.getJobBoardParameters = getJobBoardParameters;
    vm.parseJobBoardParameters = parseJobBoardParameters;
    vm.getJobBoardSearchInterface = getJobBoardSearchInterface;
    vm.launchJobBoardSearchHistory = launchJobBoardSearchHistory;
    vm.getRadiusAndRecencyOptions = getRadiusAndRecencyOptions;
        // save / update methods
    vm.updateActionButtons = updateActionButtons;
    vm.uploadRequisition = uploadRequisition;
    vm.setJobStatusToOpen = setJobStatusToOpen;
    vm.runJobForScoring = runJobForScoring;
    vm.saveJob = saveJob;
    vm.saveStep2Details = saveStep2Details;
    vm.saveWorkflowSteps = saveWorkflowSteps;
    vm.saveJobBoardsSettings = saveJobBoardsSettings;
    vm.saveAdjusted4dot5Score = saveAdjusted4dot5Score;
    vm.getCandidateCount = getCandidateCount;
    vm.getCandidatesByRequisitionTransaction = getCandidatesByRequisitionTransaction;
    vm.launchAdvancedJobBoardSearch = launchAdvancedJobBoardSearch;
    vm.updateLocation = updateLocation;
    vm.updateLocationCallback = updateLocationCallback;
        // on change methods
    vm.onClientChange = onClientChange;
    vm.onRecruitersSelected = onRecruitersSelected;
    vm.onJobTypeSelected = onJobTypeSelected;
    vm.onSponsorshipSelected = onSponsorshipSelected;
    vm.onSponsorshipCheckboxClick = onSponsorshipCheckboxClick;
    vm.onTechAssessmentCheckboxClick = onTechAssessmentCheckboxClick;
    vm.onImageProctoringCheckboxClick = onImageProctoringCheckboxClick;
    vm.onJobBoardSelectionChange = onJobBoardSelectionChange;
    vm.useJobBoardChange = useJobBoardChange;
        // wizard steps
    vm.wizardSaveStepData = wizardSaveStepData;
    vm.wizardCancel = wizardCancel;
    vm.resetStepData = resetStepData;

    vm.initialize();

    function initialize() {
        // set add or edit mode
        if($stateParams.jobId != null) {
            vm.editModeFlag = true;
        }
        // if corporate, set the BU required flag as false.
        if($rootScope.userDetails.company.companyType === $rootScope.MESSAGECONSTANTS.COMPANY_TYPES.CORPORATE){
            vm.clientOrBUIsRequiredFlag = false;
        }
        //create the wizard
        vm.uploadReqWizard = _createUploadReqWizard();
        //initialize the drop zone configuration
        vm.reqUploadDropzone = vm.createReqUploadDropzone();
        vm.req = new Requisition();
        // set the values from session
        vm.companyId = $rootScope.userDetails.company.companyId;
        vm.userId = $rootScope.userDetails.id;
        // error objects
        vm.step1Error = new UiError();
        vm.step2Error = new UiError();
        vm.step3Error = new UiError();
        vm.step4Error = new UiError();
        vm.step5Error = new UiError();
        vm.getPlanDetails();
        // if not edit mode
        if(!vm.editModeFlag){
            //initialize the variables
            vm.job = new Job();
            vm.currentJobStatus = 'DRAFT';
            // get all the required values.
            // get clients or bus
            vm.getAllClientsOrBu();
            // get recruiters
            vm.getRecruiters();
            // get job types
            vm.getAllJobTypes();
            // get sponsorship types
            vm.getAllSponsorshipTypes();
            // get tech assessments
            vm.getAllTechAssessments();
            // get value assessments
            vm.getAllValueAssessments();
            // get job boards
            vm.getAllJobBoards();
        }else{
            // edit mode
            vm.getJob($stateParams.jobId, function (data) {
                vm.currentJobStatus = 'OPEN';
                if(data.jobCurrentState == 'DRAFT'){
                    vm.currentJobStatus = 'DRAFT';
                }
                vm.req.title = data.title;
                var fileName = data.jobRequisitionList[0].fileName;
                var fileSize = data.jobRequisitionList[0].fileSize;
                // get clients or bus
                vm.getAllClientsOrBu(function () {
                    _setClientOrBu(data.clientorbu);
                });
                // get recruiters
                vm.getRecruiters(function () {
                    _setRecruiters();
                });
                // get job types
                vm.getAllJobTypes(function () {
                    _setJobType();
                });
                // get sponsorship types
                vm.getAllSponsorshipTypes(function () {
                    if(vm.job.sponsorships.length > 0){
                        vm.showSponsorshipDetailsFlag = true;
                        _setSponsorshipType();
                    }
                });
                // get tech assessments
                vm.getAllTechAssessments();
                // get value assessments
                vm.getAllValueAssessments();
                // get job boards
                vm.getAllJobBoards(function(){
                    _setJobBoardDetails();
                });
                // get license preferences as job id is available
                vm.getLicensePreferences(function(){
                    _createWorkflowSliders();
                });
                _setFileInUploadDropZone(fileName, fileSize);
                //set the flags for edit mode
                vm.disableUploadButtonFlag = true;
                vm.fileExistsFlag = true;
                vm.disableNextButtonFlag = false;
                vm.step2DetailsSavedFlag = true;
                vm.workflowStepsSavedFlag = true;
                vm.jobBoardsSettingsSavedFlag = true;
            });
        }
    }

    function _createUploadReqWizard(){
        var uploadReqWizard = $("#uploadReqWizard").wizard();
        $('#uploadReqWizard').on('actionclicked.fu.wizard', function (evt, data) {
            if(data.direction == "next"){
                switch(data.step){
                    case 1:
                        //set the temp object for the next step
                        _createStepTempObject(data.step + 1);
                        break;
                    case 2:
                        if(vm.step2RequestWasInProgressFlag){
                            // save or request was in progress and it is now completed. The success has has triggered a next event.
                            // set the flag, do nothing and move to next step.
                            _enableStepActionsOnWizard(data.step);
                        }else{
                            // there was no step2 request or save in progress. the event was triggered by user click of next button
                            if(_isStepDataValid(data.step)){
                                if(_hasStepDataChanged(data.step)){
                                    vm.step2RequestWasInProgressFlag = true;
                                    vm.saveStep2Details(function () {
                                        // if this is not edit mode and if workflow steps were not already saved, get license preferences
                                        if(!(vm.editModeFlag) && !(vm.workflowStepsSavedFlag)){
                                            vm.getLicensePreferences(function () {
                                                _createStepTempObject(data.step + 1);
                                                _createStepTempObject(data.step + 2);
                                                _createWorkflowSliders();
                                                // move to next step as the saves and gets are done.
                                                $("#uploadReqWizard").wizard('next');
                                            }, function () {
                                                _enableStepActionsOnWizard(data.step);
                                                //do nothing. error message will show up. no move to next step.
                                            });
                                        }else{
                                            // move to next step as the save is done.
                                            $("#uploadReqWizard").wizard('next');
                                        }
                                    }, function (error) {
                                        _enableStepActionsOnWizard(data.step);
                                        // show error messages
                                        if ($rootScope.isOnline) {
                                            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                                        }
                                        // no move to next step.
                                    });
                                }else{
                                    // if this is not edit mode and if workflow steps were not already saved, get license preferences
                                    if(!(vm.editModeFlag) && !(vm.workflowStepsSavedFlag)){
                                        vm.step2RequestWasInProgressFlag = true;
                                        vm.getLicensePreferences(function () {
                                            _createStepTempObject(data.step + 1);
                                            _createStepTempObject(data.step + 2);
                                            _createWorkflowSliders();
                                            // move to next step as the gets are done.
                                            $("#uploadReqWizard").wizard('next');
                                        }, function(){
                                            _enableStepActionsOnWizard(data.step);
                                            //do nothing. error message will show up. no move to next step.
                                        });
                                    }else{
                                        // step data valid and not add mode
                                        _createStepTempObject(data.step + 1);
                                        _createStepTempObject(data.step + 2);
                                    }
                                }
                            }else{
                                // prevent the move. Validation errors will show up.
                                evt.preventDefault();
                            }
                            // if a request was triggered, prevent the default behaviour of moving to next, till the requests are completed.
                            if(vm.step2RequestWasInProgressFlag){
                                // prevent default move.
                                evt.preventDefault();
                                vm.disableAllButtonsFlag = true;
                            }
                        }
                        break;
                    case 3:
                        if(vm.step3RequestWasInProgressFlag){
                            // save or request was in progress and it is now completed. The success has has triggered a next event.
                            // set the flag, do nothing and move to next step.
                            // vm.step3RequestWasInProgressFlag = false;
                            // vm.disableAllButtonsFlag = false;
                            _enableStepActionsOnWizard(data.step);
                        }else{
                            if(_isStepDataValid(data.step)){
                                if(_hasStepDataChanged(data.step)){
                                    vm.step3RequestWasInProgressFlag = true;
                                    vm.saveWorkflowSteps(function(){
                                        // if (vm.currentJobStatus === "DRAFT") {
                                        //             $("#uploadReqWizard").wizard('next');
                                        // }else{
                                        vm.getJobBoardParameters(function () {
                                            vm.getRadiusAndRecencyOptions(function () {
                                                _createStepTempObject(data.step+1);
                                                $("#uploadReqWizard").wizard('next');
                                            },
                                            function(error){
                                                _enableStepActionsOnWizard(data.step);
                                                // show error messages
                                                if ($rootScope.isOnline) {
                                                    alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                                                }
                                                // no move to next step
                                            }
                                            );
                                         },
                                        function(error){
                                            _enableStepActionsOnWizard(data.step);
                                            // show error messages
                                            if ($rootScope.isOnline) {
                                                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                                            }
                                            // no move to next step
                                        }
                                        );
                                        //  }
                                    }, function(error){
                                        _enableStepActionsOnWizard(data.step);
                                        // show error messages
                                        if ($rootScope.isOnline) {
                                            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                                        }
                                        // no move to next step
                                    });
                                }else{
                                    vm.step3RequestWasInProgressFlag = true;
                                    // the current job might not have been saved in Open state yet (if it is an edit of a draft job).
                                    if(vm.currentJobStatus === "DRAFT"){
                                        _getCandidateCountBeforeStep5(function () {
                                            _createCountSliders();
                                            getJobBoardParameters(function () { 
                                                getRadiusAndRecencyOptions(function () {
                                                    _createStepTempObject(data.step + 1);
                                                    $("#uploadReqWizard").wizard('next');
                                                }, function (error) {
                                                    _enableStepActionsOnWizard(data.step);
                                                    //do nothing. error message will show up. no move to next step.
                                                });
                                            },
                                            function () {
                                                _enableStepActionsOnWizard(data.step);
                                                //do nothing. error message will show up. no move to next step.
                                            });
                                          
                                        }, function () {
                                            _enableStepActionsOnWizard(data.step);
                                            //do nothing. error message will show up. no move to next step.
                                        });

                                    }else{
                                        _getCandidateCountBeforeStep5(function () {
                                            _createCountSliders();
                                            getJobBoardParameters(function () {
                                                getRadiusAndRecencyOptions(function () {
                                                    _createStepTempObject(data.step + 1);
                                                    $("#uploadReqWizard").wizard('next');
                                                }, function (error) {
                                                    _enableStepActionsOnWizard(data.step);
                                                    //do nothing. error message will show up. no move to next step.
                                                });
                                            },function () {
                                                _enableStepActionsOnWizard(data.step);
                                                //do nothing. error message will show up. no move to next step.
                                            });
                                        }, function () {
                                            _enableStepActionsOnWizard(data.step);
                                            //do nothing. error message will show up. no move to next step.
                                        });
                                    }
                                }
                            }else{
                                // prevent the move. Validation errors will show up.
                                evt.preventDefault();
                            }
                            // if a request was triggered, prevent the default behaviour of moving to next, till the requests are completed.
                            if(vm.step3RequestWasInProgressFlag){
                                // prevent default move.
                                evt.preventDefault();
                                vm.disableAllButtonsFlag = true;
                            }
                        }
                        break;
                    case 4:
                        if (vm.step4RequestWasInProgressFlag) {
                            // save or request was in progress and it is now completed. The success has has triggered a next event.
                            // set the flag, do nothing and move to next step.
                            _enableStepActionsOnWizard(data.step);
                        } else {
                            if (_isStepDataValid(data.step)) {
                                if (_hasStepDataChanged(data.step)) {
                                    vm.step4RequestWasInProgressFlag = true;
                                    vm.saveJobBoardsSettings('NEXT', function () {
                                        if (vm.currentJobStatus === "DRAFT") {
                                            _setJobStateToOpenAndRunForScoring(function () {
                                                var successMessage = "Requisition is now in OPEN Status. ";
                                                if (vm.job.autoMatch) {
                                                    successMessage = successMessage + "Matching candidates from system ";
                                                    if (vm.job.isResumePullEnabled && vm.isJobBoardSelected) {
                                                        successMessage = successMessage + "and pulling and matching candidates from job boards";
                                                    }
                                                } else {
                                                    if (vm.job.isResumePullEnabled && vm.isJobBoardSelected) {
                                                        successMessage = successMessage + "Pulling candidates from job board";
                                                    }
                                                }
                                                alertsAndNotificationsService.showBannerMessage(successMessage, "info");
                                                _pullResumesAndGetCandidateCountInDraft();
                                            }, function () {
                                                _enableStepActionsOnWizard(data.step);
                                            });
                                        } else {
                                            var successMessage = "";
                                            // if (vm.job.autoMatch) {
                                            //     successMessage = successMessage + "Matching candidates from system ";
                                            //     if (vm.job.isResumePullEnabled && vm.isJobBoardSelected) {
                                            //         successMessage = successMessage + "and pulling and matching candidates from job boards";
                                            //     }
                                            // }
                                            // // else {
                                                if (vm.job.isResumePullEnabled && vm.isJobBoardSelected) {
                                                    successMessage = successMessage + "Pulling and matching candidates from job boards";
                                                    alertsAndNotificationsService.showBannerMessage(successMessage, 'info');
                                                }
                                            // }
                                            _pullResumesAndGetCandidateCountInOpen();
                                        }
                                    }, function (error) {
                                        _enableStepActionsOnWizard(data.step);
                                        // show error messages
                                        if ($rootScope.isOnline) {
                                            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                                        }
                                        // no move to next step
                                    });
                                } else {
                                    vm.step4RequestWasInProgressFlag = true;
                                    // the current job might not have been saved in Open state yet (if it is an edit of a draft job).
                                    if (vm.currentJobStatus === "DRAFT") {
                                        _setJobStateToOpenAndRunForScoring(function () {
                                            _getCandidateCountBeforeStep5(function () {
                                                _createCountSliders();
                                                // find the remove button in the drop zone and delete it.
                                                $('.dz-remove').remove();
                                                // when a move from step 3 to step 4 changes the job status from DRAFT to OPEN, show a message saying the requisition is now in OPEN status.
                                                var successMessage = "Requisition is now in OPEN Status. ";
                                                if (vm.job.autoMatch) {
                                                    successMessage = successMessage + "Matching candidates from system ";
                                                    if (vm.job.isResumePullEnabled && vm.isJobBoardSelected) {
                                                        successMessage = successMessage + "and pulling and matching candidates from job boards";
                                                    }
                                                } else {
                                                    if (vm.job.isResumePullEnabled && vm.isJobBoardSelected) {
                                                        successMessage = "Pulling and matching candidates from job boards";
                                                    }
                                                }
                                                alertsAndNotificationsService.showBannerMessage(successMessage, 'info');
                                                _pullResumesAndGetCandidateCountInDraft();
                                                vm.currentJobStatus = "OPEN";
                                                    // move to next step as the save is done.
                                                    $("#uploadReqWizard").wizard('next');
                                            }, function () {
                                                _enableStepActionsOnWizard(data.step);
                                                //do nothing. error message will show up. no move to next step.
                                            });
                                        }, function () {
                                            _enableStepActionsOnWizard(data.step);
                                            // do nothing. error message will show up. no move to next step.
                                        });
                                    } else {
                                        var successMessage = "";
                                        if (vm.job.isResumePullEnabled && vm.isJobBoardSelected) {
                                            successMessage = successMessage + "Pulling and matching candidates from job boards";
                                            alertsAndNotificationsService.showBannerMessage(successMessage, 'info');
                                        }
                                        _pullResumesAndGetCandidateCountInOpen();
                                    }
                                }
                            } else {
                                // prevent the move. Validation errors will show up.
                                evt.preventDefault();
                            }
                            // if a request was triggered, prevent the default behaviour of moving to next, till the requests are completed.
                            if (vm.step4RequestWasInProgressFlag) {
                                // prevent default move.
                                evt.preventDefault();
                                vm.disableAllButtonsFlag = true;
                            }
                        }
                        break;
                    case 5:
                        if(vm.step5RequestWasInProgressFlag){
                            // save or request was in progress and it is now completed. The success has has triggered a next event.
                            // set the flag, do nothing and move to next step.
                            // vm.step5RequestWasInProgressFlag = false;
                            // vm.disableAllButtonsFlag = false;
                            _enableStepActionsOnWizard(data.step);
                        }else{
                            if(_hasStepDataChanged(data.step)){
                                vm.step5RequestWasInProgressFlag = true;
                                var adjustedScore = $('.slider-adjust-4dot5-score').val();
                                // need to save adjusted score as the current 4dot5 intelligence score
                                vm.saveAdjusted4dot5Score(adjustedScore, function () {
                                    // move to next step as the save is done.
                                    $("#uploadReqWizard").wizard('next');
                                }, function(error){

                                });
                            }
                            // if a request was triggered, prevent the default behaviour of moving to next, till the requests are completed.
                            if(vm.step5RequestWasInProgressFlag){
                                // prevent default move.
                                evt.preventDefault();
                                vm.disableAllButtonsFlag = true;
                            }
                        }
                        break;
                    default:
                        break;
                }
            }else if(data.direction == "previous"){
                switch(data.step){
                    case 1:
                        // not possible
                        break;
                    case 2:
                        // do nothing
                        break;
                    case 3:
                        _createStepTempObject(data.step - 1);
                        break;
                    case 4:
                        _createStepTempObject(data.step - 1);
                        break;
                    case 5:
                        _createStepTempObject(data.step - 1);
                        break;
                    default:
                        break;
                }
            }
        });

        $('#uploadReqWizard').on('finished.fu.wizard', function (evt, data) {
            $state.go('missioncontrol.requisitions');
        });

        $('#uploadReqWizard').on('changed.fu.wizard', function (evt, data) {
           switch(data.step){
               case 1:
               case 2:
               case 3:
               case 4:
               case 5:
                   updateActionButtons();
                   break;
           }
        });

        return uploadReqWizard;
    }

    function getPlanDetails(){
        planService.getCompanyPlanDetails(vm.companyId, function(data){
            if(!_.isNull(data.features) && data.features.length){
                var jobIntegrationDetails = _.find(data.features, { 'name': "Job Board Integration", 'available': true });
                vm.isJobIntegrationEnabledByPlan = _.isUndefined(jobIntegrationDetails) ? false : true;
                if(!vm.isJobIntegrationEnabledByPlan){
                    vm.isResumePullEnabled = false;
                }
            }
        }, function(error){
            alertsAndNotificationsService.showBannerMessage(error.message,'danger');
        });
    }

    function getJobBoardParameters(successCallback, errorCallback) {
        jobService.getJobBoardParameters(function(data){
            vm.jobBoardParameters = data;
            vm.parseJobBoardParameters();
            successCallback(data);
        }, function (error) {
            errorCallback();
        });
    }

    function getJobBoardsSearchJobStatus(successCallback, errorCallback) {
        jobService.getGetMoreResumesJobStatus(vm.job.id, function (jobInProgress) {
            successCallback(jobInProgress);
        }, function (error) {
            errorCallback(error);
        });
    }

    function getJobBoardSearchInterface(){
        var jobBoardCodes = '';
        angular.forEach(vm.job.jobBoardDetails, function(val, key){
            if(val.enabled){
                jobBoardCodes = jobBoardCodes == '' ? val.daxtraCode : jobBoardCodes + ',' + val.daxtraCode;
            }
        });
        if(jobBoardCodes == ''){
            bootbox.alert({
                closeButton: false,
                title: "<div class='alert alert-warning' style='margin-bottom: 0px;'><i class='fa fa-warning fa-fw fa-lg'></i><strong>No Job Boards Selected!</strong></div>",
                message: "Please select at least one job board to perform this action.",
                className: "zIndex1060",
                buttons: {
                    ok: {
                        label: 'Ok',
                        className: 'btn-info'
                    }
                }
            });
        }else{
            $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                backdrop: 'static',
                templateUrl: 'app/partials/missioncontrol/uploadreq/upload-req-job-board-search-interface-modal.html',
                controller: ['$uibModalInstance','$sce','$timeout','jobId','jobBoardCodes','jobService','modalService','alertsAndNotificationsService',function ($uibModalInstance, $sce, $timeout, jobId, jobBoardCodes, jobService, modalService, alertsAndNotificationsService) {
                    var vm = this;
                    vm.loadingSearchInterfaceFlag = true;
                    vm.savingFlag = false;
                    vm.jobId = jobId;
                    vm.location = location;
                    vm.searchHtml = '';

                    var searchObject = {};
                    searchObject.dbCode = 'jw_fourdotfive';
                    searchObject.jobBoardCodes = jobBoardCodes;
                    searchObject.jsLib = 'jquery';
                    searchObject.libs = 'css,jb_params_jq,jquery-ui';
                    searchObject.ajaxAutoCompleter = 'http://jbws-az-4dot5.daxtra.com/rtb/spider_params_completer.pl';
                    searchObject.ajaxUpdater = 'http://jbws-az-4dot5.daxtra.com/rtb/spider_params_ajax.pl';
                    searchObject.jsonp = true;
                    searchObject.relayParam = 'daxtra.com';

                    jobService.getJobBoardSearchInterface(searchObject, function(data){
                        vm.searchHtml = $sce.trustAsHtml(data.t.htmlContent);

                        var searchJobParamsJS = "<script>" + data.t.jbParamsJq + "</script>";
                        var searchJS = "<script>" + data.t.jsContent + "</script>";
                        var searchCSS = "<style>" + data.t.cssContent + "</style>";
                        $('#dynamicContentForJobBoardSearch').append(searchCSS);
                        $('#dynamicContentForJobBoardSearch').append(searchJobParamsJS);
                        $('#dynamicContentForJobBoardSearch').append(searchJS);
                        vm.loadingSearchInterfaceFlag = false;
                        $timeout(function(){
                            // remove the 'max results' labels and selection boxes.
                            // remove labels
                            $("label:contains('Max results')").remove();
                            // remove select controls
                            $( "select[name$='_max_results']" ).remove();
                        }, 500);
                    }, function(error){
                        vm.loadingSearchInterfaceFlag = false;
                        alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    });

                    vm.closeModal = function () {
                        $uibModalInstance.close('cancel');
                    };

                    vm.saveCallback = function(locationObject){
                        vm.closeModal();
                        updateLocationCallback(locationObject);
                    };

                    vm.saveAdvancedSearchParams = function(){
                        vm.savingFlag = true;
                        var advancedSearchArray = $("#jobBoardSearchForm").serializeArray();
                        var advancedSearchObject = {};
                        angular.forEach(advancedSearchArray, function(val, key){
                            advancedSearchObject[val['name']] = val['value'];
                        });
                        jobService.saveDaxtraParameters(vm.jobId, advancedSearchObject, function (data) {
                            vm.savingFlag = false;
                            bootbox.alert({
                                closeButton: false,
                                title: "<div class='alert alert-success' style='margin-bottom: 0px;'><i class='fa fa-check fa-fw fa-lg'></i><strong>Success!</strong></div>",
                                message: "Advanced Search Parameters saved successfully. Hit 'Next' to initiate search and pull candidates.",
                                className: "zIndex1060",
                                buttons: {
                                    ok: {
                                        label: 'Ok',
                                        className: 'btn-info'
                                    }
                                },
                                callback: function(){
                                    // modalService.addModalOpenClassToBodyIfAnyModalIsOpen();
                                    vm.closeModal();
                                }
                            });
                        }, function(error){
                            vm.savingFlag = false;
                            alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                        });
                    };
                }],
                controllerAs: 'searchModal',
                size: 'lg',
                resolve:{
                    jobId: function () {
                        return vm.job.id;
                    },
                    jobBoardCodes: function (){
                        return jobBoardCodes;
                    }
                }
            });
        }
    }

    function launchJobBoardSearchHistory() {
        $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            backdrop: 'static',
            templateUrl: 'app/partials/missioncontrol/uploadreq/job-boards-search-history-modal.html',
            controller: ['$uibModalInstance', 'jobId', 'clientOrBuName', 'jobTitle', function($uibModalInstance, jobId, clientOrBuName, jobTitle) {
                var vm = this;
                vm.jobId = jobId;
                vm.clientOrBuName = clientOrBuName;
                vm.jobTitle = jobTitle;

                vm.closeModal = function() {
                    $uibModalInstance.close('cancel');
                };
                vm.saveCallback = function() {
                    vm.closeModal();
                };
            }],
            controllerAs: 'searchHistoryModal',
            windowClass: 'job-boards-search-history-modal-window',
            resolve: {
                jobId: function() {
                    return vm.job.id;
                },
                clientOrBuName: function() {
                    return vm.job.clientorbu.name;
                },
                jobTitle: function() {
                    return vm.job.title;
                }
            }
        });
    }

    function parseJobBoardParameters() {
        angular.forEach(vm.job.jobBoardDetails, function (val, key) {
            var flags = [];
            var jobBoardsParams = vm.jobBoardParameters.jobBoardParameterMap[val.daxtraCode];
            
            flags.push(true); // location supported by all job boards
            flags.push(_isParameterSupported(jobBoardsParams, "postcode"));
            flags.push(_isParameterSupported(jobBoardsParams, "radius"));
            flags.push(_isParameterSupported(jobBoardsParams, "jobType"));
            flags.push(_isParameterSupported(jobBoardsParams, "recency"));
            flags.push(_isParameterSupported(jobBoardsParams, "education"));
            flags.push(_isParameterSupported(jobBoardsParams, "industry"));
            flags.push(_isParameterSupported(jobBoardsParams, "salary"));
            val.flags = flags;
        });
        vm.jobBoardDetailsCopy = angular.copy(vm.job.jobBoardDetails);
        vm.jobBoardsSettingsTempObject.jobBoardDetails = angular.copy(vm.job.jobBoardDetails);
    }

    function _isParameterSupported(jobBoardParameters,parameter){
        return (jobBoardParameters.indexOf(parameter) == -1) ? false : true; 
    }

    function getRadiusAndRecencyOptions(successCallback, errorCallback) {
        jobService.getRadiusAndRecencyOptions(function(data){
            console.log('options');
            console.log(data);
            vm.radiusOptions = data.radius;
            vm.recencyOptions = data.recency;
            
        if (_.isNull(vm.job.radius)) {
            vm.job.radius = data.defaultRadius[0];
        }

        if (_.isNull(vm.job.recency)) {
            vm.job.recency = data.defaultRecency[0];
        }
            successCallback(data);
        }, function (error) {
            errorCallback();
        });
    }

    /**
     * creates the requisition upload drop zone.
     */
    function createReqUploadDropzone(){
        var reqUploadDropzone = new Dropzone("form#reqUploadDropzone", {
            url: requisitionService.getUploadUrl(),
            headers: {
                Authorization: 'Bearer ' + $rootScope.userDetails.accesstoken
            },
            paramName: "file",
            acceptedFiles: ".pdf,.xlsx,.docx",
            maxFiles: 1,
            maxFilesize: 10,
            maxThumbnailFilesize: 10,
            addRemoveLinks: true,
            autoProcessQueue: false
        });

        // add event handlers for the drop zone
        reqUploadDropzone.on('addedfile', function (file, response) {
            vm.fileExistsFlag = true;
            vm.updateActionButtons();
            // set the progress bar so that it is properly visible
            $('.dz-progress').css({top: '70%'});
        });

        reqUploadDropzone.on('removedfile', function (file, response) {
            vm.fileExistsFlag = false;
            //reset the job object
            vm.job = new Job();
            vm.updateActionButtons();
        });

        reqUploadDropzone.on('sending', function (file, xhr, formData) {
            // disable the upload button to avoid multiple clicks
            vm.disableUploadButtonFlag = true;
            // indicate that file upload is in progress
            vm.fileUploadInProgressFlag = true;
            formData.append('companyId', $rootScope.userDetails.company.companyId);
            if (vm.req.clientOrBU !== null) {
                formData.append('buId', vm.req.clientOrBU.id);
            }
            formData.append('userId', $rootScope.userDetails.id);
        });

        reqUploadDropzone.on('success', function (file, response) {
            // remove file upload in progress flag
            vm.fileUploadInProgressFlag = false;
            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.UPLOAD.UPLOAD_SUCCESS, 'success');
            // merge the response into the job object
            vm.job = _parseJobResponse(response);
            vm.updateActionButtons();
        });

        return reqUploadDropzone;
    }

    function getJob(jobId, successCallback) {
        jobService.getJob(jobId, function(data){
            vm.job = _parseJobResponse(data);
            successCallback(data);
        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }

    function _parseJobResponse(data){
        var tempJob = new Job();
        tempJob.id = data.id;
        tempJob.title = data.title;
        tempJob.company = {
            name: data.company.name,
            id: data.company.id,
            shortName: data.company.shortName
        };
        if(_.isNull(data.clientorbu)){
            tempJob.clientorbu = null;
        }else{
            tempJob.clientorbu = {
                name: data.clientorbu.name,
                id: data.clientorbu.id,
                shortName: data.clientorbu.shortName
            };
        }
        tempJob.requisitionNumber = data.requisitionNumber;
        tempJob.jobType = data.jobType;
        tempJob.location = data.location;
        tempJob.locationLabel = '-';
        if(!_.isNull(tempJob.location)){
            if(!_.isNull(data.location.city)){
                tempJob.locationLabel = data.location.city.name;
            }
            if(_.isNull(tempJob.locationLabel) || tempJob.locationLabel == '-'){
                tempJob.locationLabel = data.location.state;
                if(_.isNull(tempJob.locationLabel)){
                    tempJob.locationLabel = '-';
                }
            }else{
                if(!_.isNull(data.location.state)){
                    tempJob.locationLabel += ', ' + data.location.state;
                }
            }
        }
        // set the transaction id of the last requisition in the requisition list as job's transaction id
        tempJob.transactionId = data.jobRequisitionList[(data.jobRequisitionList.length - 1)].transactionId;
        tempJob.openings = data.numberOfOpenings;
        tempJob.sponsorships = data.sponsorships;
        tempJob.recruiters = data.recruiters.map(function (recruiter) {
            return recruiter.id;
        });
        tempJob.educationList = data.educationList;
        tempJob.score = data.fourDotFiveIntelligenceScore;
        tempJob.licensePreferences = data.licensePreferences;
        tempJob.radius = data.radius;
        tempJob.recency = data.recency;
        tempJob.salary = data.salary;
        tempJob.industry = data.industry;
        if(!_.isNull(data.techAssessmentId)){
            tempJob.techAssessmentId = data.techAssessmentId;
            tempJob.techAssessmentScore = data.techAssessmentScore;
        }
        if(!_.isNull(data.valueAssessmentId)) {
            tempJob.valueAssessmentId = data.valueAssessmentId;
        }
        tempJob.jobBoardDetails = angular.copy(vm.jobBoardDetails);
        if (!_.isNull(tempJob.licensePreferences) && !_.isNull(tempJob.licensePreferences.jobBoardDetails)) {
            angular.forEach(tempJob.jobBoardDetails, function (val, key) {
                if (_.findIndex(tempJob.licensePreferences.jobBoardDetails, { 'boardId': val.boardId, 'enabled': true }) == -1) {
                    val.enabled = false;
                }
            });
        }
        vm.jobBoardDetailsCopy = angular.copy(tempJob.jobBoardDetails);
        if (!_.isNull(tempJob.licensePreferences) && !_.isNull(tempJob.licensePreferences.resumesToPullPerJobBoardCount)) {
            tempJob.resumesToPullPerJobBoardCount = data.licensePreferences.resumesToPullPerJobBoardCount;
            vm.resumesToPullPerJobBoardCountCopy = tempJob.resumesToPullPerJobBoardCount;
            tempJob.isResumePullEnabled = false;
        } 
        return tempJob;
    }

    function _setJobStateToOpenAndRunForScoring(successCallback, errorCallback){
        vm.setJobStatusToOpen(function () {
            vm.currentJobStatus = "OPEN";
            // delete the DOM for 'remove button' option from the file in drop zone since job is now in 'open' status
            // find the remove button in the drop zone and delete it.
            $('.dz-remove').remove();
            vm.runJobForScoring(function () {
                if(successCallback){
                    successCallback();
                }
            }, function(){
                if(errorCallback){
                    errorCallback();
                }
            });
        }
            , function () {
            if(errorCallback){
                errorCallback();
            }
        });
    }

    function _pullResumesAndGetCandidateCountInDraft() {
        if (vm.job.isResumePullEnabled && vm.isJobBoardSelected) {
            vm.getJobBoardsSearchJobStatus(function (jobStatus) {
                if (jobStatus == true) {
                    alertsAndNotificationsService.showBannerMessage("Candidates Fetch from Job Boards already <b>'INPROGRESS'</b>. Please try after sometime.", 'info');
                    _enableStepActionsOnWizard(data.step);
                }
                else {
                    vm.getResumesFromJobBoards(function () {
                        _getCandidateCountBeforeStep5(function () {
                           _createCountSliders();
                           vm.currentJobStatus = "OPEN";
                           // find the remove button in the drop zone and delete it.
                           $('.dz-remove').remove();
                           // when a move from step 4 to step 5 changes the job status from DRAFT to OPEN, show a message saying the requisition is now in OPEN status.
                           var successMessage = "";
                           if (vm.job.autoMatch) {
                               successMessage = successMessage + "Matched candidates from system ";
                               if (vm.job.isResumePullEnabled && vm.isJobBoardSelected) {
                                   successMessage = successMessage + "and pulled and matched candidates from job boards";
                               }
                           } else {
                               if (vm.job.isResumePullEnabled && vm.isJobBoardSelected) {
                                   successMessage = successMessage + "Pulled candidates from job board";
                               }
                           }
        
                           alertsAndNotificationsService.showBannerMessage(successMessage, 'success');
                           // move to next step as the save is done.
                           $("#uploadReqWizard").wizard('next');
                       }, function () {
                            _enableStepActionsOnWizard(data.step);
                           //do nothing. error message will show up. no move to next step.
                       });
                   },
                   function (error) {
                       //end of resume pull
                       alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                       _enableStepActionsOnWizard(data.step);
                   });
                }
            }, function (error) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            });
        }
        else {
            _getCandidateCountBeforeStep5(function () {
               _createCountSliders();
               vm.currentJobStatus = "OPEN";
               // find the remove button in the drop zone and delete it.
               $('.dz-remove').remove();
               // when a move from step 3 to step 4 changes the job status from DRAFT to OPEN, show a message saying the requisition is now in OPEN status.
               var successMessage = "Requisition is now in OPEN Status. ";
               if (vm.job.autoMatch) {
                   successMessage = successMessage + "Matching candidates from system ";
                   if (vm.job.isResumePullEnabled && vm.isJobBoardSelected) {
                       successMessage = successMessage + "and pulling and matching candidates from job boards";
                   }
               } else {
                   if (vm.job.isResumePullEnabled && vm.isJobBoardSelected) {
                       successMessage = successMessage + "Pulling candidates from job board";
                   }
               }
               alertsAndNotificationsService.showBannerMessage(successMessage, 'info');
               // move to next step as the save is done.
               $("#uploadReqWizard").wizard('next');
           }, function () {
                _enableStepActionsOnWizard(data.step);
                //do nothing. error message will show up. no move to next step.
           });
        }
    }

    function _pullResumesAndGetCandidateCountInOpen() {
        if (vm.job.isResumePullEnabled && vm.isJobBoardSelected) {
            vm.getJobBoardsSearchJobStatus(function (jobStatus) {
                if (jobStatus == true) { //job in progress
                    alertsAndNotificationsService.showBannerMessage("Candidates Fetch from Job Boards already <b>'INPROGRESS'</b>. Please try after sometime.", 'info');
                    _enableStepActionsOnWizard(data.step);
                } else {
                    vm.getResumesFromJobBoards(function () {
                        _getCandidateCountBeforeStep5(function () {
                            _createCountSliders();
                            var successMessage = "";
                            if (vm.job.autoMatch) {
                                successMessage = successMessage + "Matched candidates from system ";
                                if (vm.job.isResumePullEnabled && vm.isJobBoardSelected) {
                                    successMessage = successMessage + "and pulled and matched candidates from job boards";
                                }
                                alertsAndNotificationsService.showBannerMessage(successMessage, 'success');
                            }else {
                                if (vm.job.isResumePullEnabled && vm.isJobBoardSelected) {
                                    successMessage = successMessage + "Pulled and matched candidates from job boards";
                                    alertsAndNotificationsService.showBannerMessage(successMessage, 'success');
                                }
                            }
                           // move to next step as the save is done.
                           $("#uploadReqWizard").wizard('next');
                       }, function () {
                            _enableStepActionsOnWizard(data.step);
                            //do nothing. error message will show up. no move to next step.
                       });
                   },
                   function (error) {
                       //end of resume pull
                       alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                       _enableStepActionsOnWizard(data.step);
                   });
                }
            }, function (error) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                _enableStepActionsOnWizard(data.step);
            });
            
        }else {
            _getCandidateCountBeforeStep5(function () {
               _createCountSliders();
               $("#uploadReqWizard").wizard('next');
           }, function () {
                _enableStepActionsOnWizard(data.step);
               //do nothing. error message will show up. no move to next step.
           });
        }
    }

    function _getCandidateCountBeforeStep5(successCallback, errorCallback) {
        vm.getCandidateCount(vm.job.score, 'currentScore', function () {
            if(successCallback){
                successCallback();
            }
        }, function () {
            if(errorCallback){
                errorCallback();
            }
        });
    }

    function _createStepTempObject(currentStep) {
        switch(currentStep){
            case 1:
                break;
            case 2:
                var clientOrBUId = _.isNull(vm.job.clientorbu) ? null : vm.job.clientorbu.id;
                vm.step2DetailsTempObject = {
                    jobId: vm.job.id,
                    title: vm.job.title,
                    clientOrBUId: clientOrBUId,
                    requistionNumber: vm.job.requisitionNumber,
                    openings: vm.job.openings,
                    jobType: vm.job.jobType,
                    sponsorships: vm.job.sponsorships,
                    recruiters: vm.job.recruiters,
                    salary: vm.job.salary,
                    tab: "ASSIGN_RECRUITERS"
                };
                break;
            case 3:
                vm.workflowStepsTempObject = {
                    jobId: vm.job.id,
                    score: vm.job.score,
                    autoMatch: vm.job.autoMatch,
                    recruiterScreening: vm.job.recruiterScreening,
                    techAssessmentId: vm.job.techAssessmentId,
                    techAssessmentScore: vm.job.techAssessmentScore,
                    techAssessment: vm.job.techAssessment,
                    valueAssessmentId: vm.job.valueAssessmentId,
                    valueAssessment: vm.job.valueAssessment,
                    eduVerification: vm.job.eduVerification,
                    expVeification: vm.job.expVeification,
                    phoneScreen: vm.job.phoneScreen,
                    interview: vm.job.interview,
                    imageProctoring: vm.job.imageProctoring,
                    addProctoringImagesToReport: vm.job.addProctoringImagesToReport,
                    // jobBoardDetails: angular.copy(vm.job.jobBoardDetails),
                    // resumesToPullPerJobBoardCount: vm.job.resumesToPullPerJobBoardCount,
                    tab: "WORKFLOW_STEPS"
                };
                break;
            case 4:
                vm.jobBoardsSettingsTempObject = {
                    // jobId: vm.job.id,
                    // autoMatch: vm.job.autoMatch,
                    jobBoardDetails: angular.copy(vm.job.jobBoardDetails),
                    resumesToPullPerJobBoardCount: vm.job.resumesToPullPerJobBoardCount,
                    isResumePullEnabled: vm.job.isResumePullEnabled,
                    radius: vm.job.radius,
                    recency: vm.job.recency,
                    tab: "JOB_BOARDS"
                };
                break;
            case 5:
                break;
            default:
                break;
        }
    }

    function _isStepDataValid(currentStep) {
        var stepDataValidityFlag = true;
        switch(currentStep){
            case 1:
                //do nothing
                break;
            case 2:
                // set the form to submitted
                vm.forms.step2Form.$submitted = true;
                //check all the values one by one.
                if(vm.forms.step2Form.$invalid || (vm.job.jobType == null) || (vm.job.recruiters.length == 0) || (vm.showSponsorshipDetailsFlag && vm.job.sponsorships.length == 0)){
                    vm.step2Error.hasErrorFlag = true;
                    vm.step2Error.errorMessage = 'Please enter all mandatory fields.';
                    stepDataValidityFlag = false;
                }else{
                    vm.step2Error.hasErrorFlag = false;
                    vm.step2Error.errorMessage = '';
                }
                break;
            case 3:
                // set the form to submitted
                vm.forms.step3Form.$submitted = true;
                //check all the values one by one.
                if(vm.forms.step3Form.$invalid){
                    vm.step3Error.hasErrorFlag = true;
                    vm.step3Error.errorMessage = 'Please enter all mandatory fields.';
                    stepDataValidityFlag = false;
                }else{
                    vm.step3Error.hasErrorFlag = false;
                    vm.step3Error.errorMessage = '';
                }
                break;
            case 4:
                break;
            default:
                break;
        }
        return stepDataValidityFlag;
    }

    function _hasStepDataChanged(currentStep) {
        var stepDataHasChangedFlag = false;
        switch(currentStep){
            case 1:
                //do nothing
                break;
            case 2:
                var clientOrBUId = _.isNull(vm.job.clientorbu) ? null : vm.job.clientorbu.id;
                var currentStep2DataObject = {
                    jobId: vm.job.id,
                    title: vm.job.title,
                    clientOrBUId: clientOrBUId,
                    requistionNumber: vm.job.requisitionNumber,
                    openings: vm.job.openings,
                    jobType: vm.job.jobType,
                    sponsorships: vm.job.sponsorships,
                    recruiters: vm.job.recruiters,
                    salary: vm.job.salary,
                    tab: "ASSIGN_RECRUITERS"
                };
                stepDataHasChangedFlag = !(_.isEqual(vm.step2DetailsTempObject,currentStep2DataObject));
                break;
            case 3:
                var currentWorkflowStepsObject = {
                    jobId: vm.job.id,
                    score: vm.job.score,
                    autoMatch: vm.job.autoMatch,
                    recruiterScreening: vm.job.recruiterScreening,
                    techAssessmentId: vm.job.techAssessmentId,
                    techAssessmentScore: vm.job.techAssessmentScore,
                    techAssessment: vm.job.techAssessment,
                    valueAssessmentId: vm.job.valueAssessmentId,
                    valueAssessment: vm.job.valueAssessment,
                    eduVerification: vm.job.eduVerification,
                    expVeification: vm.job.expVeification,
                    phoneScreen: vm.job.phoneScreen,
                    interview: vm.job.interview,
                    imageProctoring: vm.job.imageProctoring,
                    addProctoringImagesToReport: vm.job.addProctoringImagesToReport,
                    // jobBoardDetails: angular.copy(vm.job.jobBoardDetails),
                    // resumesToPullPerJobBoardCount: vm.job.resumesToPullPerJobBoardCount,
                    tab: "WORKFLOW_STEPS"
                };
                stepDataHasChangedFlag = !(angular.equals(vm.workflowStepsTempObject,currentWorkflowStepsObject));
                break;
            case 4:
                var currentJobBoardsSettingsObject = {
                    // jobId: vm.job.id,
                    // autoMatch: vm.job.autoMatch,
                    jobBoardDetails: angular.copy(vm.job.jobBoardDetails),
                    resumesToPullPerJobBoardCount: vm.job.resumesToPullPerJobBoardCount,
                    isResumePullEnabled: vm.job.isResumePullEnabled,
                    radius: vm.job.radius,
                    recency: vm.job.recency,
                    tab: "JOB_BOARDS"
                };
                stepDataHasChangedFlag = !(angular.equals(vm.jobBoardsSettingsTempObject, currentJobBoardsSettingsObject)) || (vm.job.isResumePullEnabled);
                if (vm.currentJobStatus == "DRAFT")
                    stepDataHasChangedFlag = true;    
                break;
            case 5:
                var adjustedScore = $('.slider-adjust-4dot5-score').val();
                if(adjustedScore != vm.job.score){
                    stepDataHasChangedFlag = true;
                }
                break;
            default:
                break;
        }
        return stepDataHasChangedFlag;
    }

    function _enableStepActionsOnWizard(currentStep) {
        switch (currentStep) {
            case 1:
                //do nothing
                break;
            case 2:
                vm.step2RequestWasInProgressFlag = false;
                break;
            case 3:
                vm.step3RequestWasInProgressFlag = false;
                break;
            case 4:
                vm.step4RequestWasInProgressFlag = false;
                break;
            case 5:
                vm.step5RequestWasInProgressFlag = false;
                break;
            default:
                break;
        }
        vm.disableAllButtonsFlag = false;
    }

    function wizardSaveStepData(){
        var currentStep = $('#uploadReqWizard').wizard('selectedItem').step;
        if(_isStepDataValid(currentStep)){
            if(_hasStepDataChanged(currentStep)) {
                switch(currentStep){
                    case 1:
                        //do nothing
                        break;
                    case 2:
                        vm.disableAllButtonsFlag = true;
                        vm.savingFlag = true;
                        vm.saveStep2Details(function(){
                            vm.disableAllButtonsFlag = false;
                            vm.savingFlag = false;
                            if ($rootScope.isOnline) {
                                var successMessage = 'The Requisition details have been saved in Draft Status.';
                                if(vm.currentJobStatus == 'OPEN'){
                                    successMessage = 'The Requisition details have been saved.';
                                }
                                alertsAndNotificationsService.showBannerMessage(successMessage, 'success');
                            }
                            _createStepTempObject(currentStep);
                        }, function (error) {
                            vm.disableAllButtonsFlag = false;
                            vm.savingFlag = false;
                            if ($rootScope.isOnline) {
                                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                            }
                        });
                        break;
                    case 3:
                        vm.disableAllButtonsFlag = true;
                        vm.savingFlag = true;
                        vm.saveWorkflowSteps(function(){
                            vm.disableAllButtonsFlag = false;
                            vm.savingFlag = false;
                            // show the message that the job is saved state
                            if ($rootScope.isOnline) {
                                var successMessage = 'The Requisition details have been saved in Draft Status.';
                                if(vm.currentJobStatus == 'OPEN'){
                                    successMessage = 'The Requisition details have been saved.';
                                }
                                alertsAndNotificationsService.showBannerMessage(successMessage, 'success');
                            }
                            _createStepTempObject(currentStep);
                        }, function (error) {
                            vm.disableAllButtonsFlag = false;
                            vm.savingFlag = false;
                            if ($rootScope.isOnline) {
                                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                            }
                        });
                        break;
                    case 4:
                        vm.disableAllButtonsFlag = true;
                        vm.savingFlag = true;
                        vm.saveJobBoardsSettings('SAVE',function(){
                            vm.disableAllButtonsFlag = false;
                            vm.savingFlag = false;
                            var successMessage = "";
                            // show the message that the job is saved state
                            if ($rootScope.isOnline) {
                                if (vm.currentJobStatus == 'OPEN') {
                                    successMessage = "All the parameters have been saved. ";
                                    if (vm.job.isResumePullEnabled) {
                                        successMessage = successMessage + "Only when you click ‘Next’ will more resumes be pulled ";
                                        if (vm.job.autoMatch) {
                                            successMessage = successMessage + "and matched from job boards.";
                                        }
                                    }
                                    else {
                                        if (vm.job.autoMatch) {
                                            successMessage = successMessage + "Only when you click ‘Next’ candidates will be matched.";
                                        }  
                                    }
                                }
                                if (vm.currentJobStatus == 'DRAFT') {
                                    successMessage = "All the parameters have been saved. Requisition is in Draft Status. Only when you hit 'Next' will the status be changed to 'Open'. ";
                                    if (vm.job.autoMatch) {
                                        successMessage = successMessage + "Matching of candidates to requisition will also happen only when you hit 'Next'."
                                    }
                                }
                                alertsAndNotificationsService.showBannerMessage(successMessage, 'success');
                            }
                            // vm.jobBoardDetailsCopy = angular.copy(vm.job.jobBoardDetails);
                            _createStepTempObject(currentStep);
                        }, function (error) {
                            vm.disableAllButtonsFlag = false;
                            vm.savingFlag = false;
                            if ($rootScope.isOnline) {
                                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                            }
                        });
                        break;
                    case 5:
                        vm.disableAllButtonsFlag = true;
                        vm.savingFlag = true;
                        var adjustedScore = $('.slider-adjust-4dot5-score').val();
                        vm.saveAdjusted4dot5Score(adjustedScore, function (data) {
                            vm.disableAllButtonsFlag = false;
                            vm.savingFlag = false;
                            // show the message that the score is updated
                            if ($rootScope.isOnline) {
                                var successMessage = 'The 4dot5 Intelligence score has been updated.';
                                alertsAndNotificationsService.showBannerMessage(successMessage, 'success');
                            }
                        }, function (error){
                            vm.disableAllButtonsFlag = false;
                            vm.savingFlag = false;
                            if ($rootScope.isOnline) {
                                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                            }
                        });
                        break;
                    default:
                        break;
                }
            }else{
                if ($rootScope.isOnline) {
                    alertsAndNotificationsService.showBannerMessage('Nothing to be saved!', 'info');
                }
            }
        }
    }

    function wizardCancel() {
        var currentStep = $("#uploadReqWizard").wizard('selectedItem');
        if(_hasStepDataChanged(currentStep.step)){
            bootbox.confirm({
                closeButton: false,
                title: "<div class='alert alert-warning' style='margin-bottom: 0px;'><i class='fa fa-warning fa-fw fa-lg'></i><strong>Warning!</strong></div>",
                message: "Unsaved data on the page will be lost. Do you still want to continue?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'Cancel',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result) {
                        vm.resetStepData(currentStep.step);
                    }
                }
            });
        }
    }

    function resetStepData(step){
        $timeout(function(){
            switch(step){
                case 1:
                    //do nothing
                    break;
                case 2:
                    vm.job.title = vm.step2DetailsTempObject.title;
                    if(_.isNull(vm.step2DetailsTempObject.clientOrBUId)){
                        vm.job.clientorbu = null;
                    }else{
                        angular.forEach(vm.clientorbulist, function(val,key){
                            if(val.id == vm.step2DetailsTempObject.clientOrBUId){
                                vm.job.clientorbu = val;
                            }
                        });
                    }
                    vm.job.requisitionNumber = vm.step2DetailsTempObject.requistionNumber;
                    vm.job.openings = vm.step2DetailsTempObject.openings;
                    vm.job.jobType = vm.step2DetailsTempObject.jobType;
                    _setJobType();
                    vm.job.sponsorships = vm.step2DetailsTempObject.sponsorships;
                    _setSponsorshipType();
                    if(vm.job.sponsorships.length == 0){
                        vm.showSponsorshipDetailsFlag = false;
                    }else{
                        vm.showSponsorshipDetailsFlag = true;
                    }
                    vm.job.recruiters = vm.step2DetailsTempObject.recruiters;
                    vm.job.salary = vm.step2DetailsTempObject.salary;
                    _setRecruiters();
                    break;
                case 3:
                    vm.job.score = vm.workflowStepsTempObject.score;
                    $('.slider-intelligence').val(vm.job.score);
                    $('#4dot5IntelligenceScoreDisplayValue').text(vm.job.score);
                    vm.job.autoMatch = vm.workflowStepsTempObject.autoMatch;
                    vm.job.recruiterScreening = vm.workflowStepsTempObject.recruiterScreening;
                    vm.job.techAssessmentId = vm.workflowStepsTempObject.techAssessmentId;
                    vm.job.techAssessment = vm.workflowStepsTempObject.techAssessment;
                    vm.job.techAssessmentScore = vm.workflowStepsTempObject.techAssessmentScore;
                    $('.slider-tech-assessment').val(vm.job.techAssessmentScore);
                    $('#techAssessmentScoreDisplayValue').text(vm.job.techAssessmentScore);
                    vm.job.valueAssessment = vm.workflowStepsTempObject.valueAssessment;
                    vm.job.techAssessmentScore = vm.workflowStepsTempObject.techAssessmentScore;
                    vm.job.valueAssessmentId = vm.workflowStepsTempObject.valueAssessmentId;
                    vm.job.eduVerification = vm.workflowStepsTempObject.eduVerification;
                    vm.job.expVeification = vm.workflowStepsTempObject.expVeification;
                    vm.job.phoneScreen = vm.workflowStepsTempObject.phoneScreen;
                    vm.job.interview = vm.workflowStepsTempObject.interview;
                    vm.job.imageProctoring = vm.workflowStepsTempObject.imageProctoring;
                    vm.job.addProctoringImagesToReport = vm.workflowStepsTempObject.addProctoringImagesToReport;
                    // vm.job.jobBoardDetails = angular.copy(vm.workflowStepsTempObject.jobBoardDetails);
                    // vm.job.resumesToPullPerJobBoardCount = vm.workflowStepsTempObject.resumesToPullPerJobBoardCount;
                    break;
                case 4:
                     vm.job.jobBoardDetails = angular.copy(vm.jobBoardDetailsCopy);
                     vm.job.resumesToPullPerJobBoardCount = vm.jobBoardsSettingsTempObject.resumesToPullPerJobBoardCount;
                     vm.job.isResumePullEnabled = vm.jobBoardsSettingsTempObject.isResumePullEnabled;
                     vm.job.radius = vm.jobBoardsSettingsTempObject.radius;
                     vm.job.recency = vm.jobBoardsSettingsTempObject.recency;
                    break;
                case 5:
                    $('.slider-adjust-4dot5-score').val(vm.job.score);
                    $('#adjust4dot5ScoreDisplayValue').text(vm.job.score);
                    break;
                default:
                    break;
            }
        }, 100);
    }

    /**
     * this function fetches all client or bu list from service
     */
    function getAllClientsOrBu (successCallback) {
        requisitionService.getAllClientsOrBu($rootScope.userDetails, function (data) {
            vm.clientorbulist = data;
            if(successCallback){
                successCallback();
            }
        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }

    function _setClientOrBu(clientorbu){
        if(!_.isNull(clientorbu)){
            vm.req.clientOrBU = {
                name: clientorbu.name,
                companyType: clientorbu.companyType,
                companyState: clientorbu.companyState,
                id: clientorbu.id,
                shortName: clientorbu.shortName
            };
        }else{
            vm.req.clientOrBU = null;
        }
    }

    function getAllJobTypes(successCallback){
        jobService.getAllJobTypes(function (data) {
            vm.jobTypeList = data;
            if(successCallback){
                successCallback();
            }
        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }

    function onJobTypeSelected() {
        if(vm.selectedJobTypes.length > 0){
            vm.job.jobType = vm.selectedJobTypes[0].value;
        }else{
            vm.job.jobType = null;
        }
    }

    function _setJobType() {
        if (angular.isDefined(vm.job.jobType)) {
            for (var k = 0; k < vm.jobTypeList.length; k++) {
                if (vm.job.jobType === vm.jobTypeList[k].value) {
                    vm.jobTypeList[k].selected = true;
                } else {
                    vm.jobTypeList[k].selected = false;
                }
            }
        } else {
            for (var i = 0; i < vm.jobTypeList.length; i++) {
                vm.jobTypeList[i].selected = false;
            }
        }
    }

    function getAllSponsorshipTypes(successCallback) {
        jobService.getAllSponsorshipTypes(function(data){
            vm.sponsorshipList = data;
            if(successCallback){
                successCallback();
            }
        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }

    function _setSponsorshipType() {
        angular.forEach(vm.sponsorshipList, function(val,key){
            if(vm.job.sponsorships.indexOf(val.value) > -1){
                val.selected = true;
            }else{
                val.selected = false;
            }
        });
    }

    function onSponsorshipCheckboxClick(){
        vm.job.sponsorships = [];
        if(vm.showSponsorshipDetailsFlag){
            angular.forEach(vm.selectedSponsorships,function (sponsorship, key) {
                vm.job.sponsorships.push(sponsorship.value);
            });
        }else{
            vm.selectedSponsorships = [];
        }
    }

    function onSponsorshipSelected() {
        vm.job.sponsorships = [];
        if(vm.selectedSponsorships.length > 0){
            angular.forEach(vm.selectedSponsorships,function (sponsorship, key) {
                vm.job.sponsorships.push(sponsorship.value);
            });
        }
    }

    function getAllTechAssessments(){
        assessmentService.getAllTechAssessments(vm.companyId, function(data){
            if(data.length > 0){
                vm.techAssessmentList = data;
            }
        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });

    }

    function getAllValueAssessments(){
        assessmentService.getAllValueAssessments(vm.companyId, function(data){
            if(data.length > 0){
                vm.valueAssessmentList = data;
            }
        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });

    }

    function getAllJobBoards(successCallback){
        resumeService.getResumeSourceTypes(vm.companyId, function(data){
            if(data.length > 0){
                angular.forEach(data, function(val, key){
                    val.boardId = val.id;
                    val.enabled = true;
                    delete val.id;
                });
                vm.jobBoardDetails = data;
                if(successCallback){
                    successCallback();
                }
            }
        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }

    function _setJobBoardDetails(){
        vm.job.jobBoardDetails = angular.copy(vm.jobBoardDetails);
        if(!_.isNull(vm.job.licensePreferences) && !_.isNull(vm.job.licensePreferences.jobBoardDetails)){
            angular.forEach(vm.job.jobBoardDetails, function (val, key) {
                if(_.findIndex(vm.job.licensePreferences.jobBoardDetails, { 'boardId': val.boardId, 'enabled': true }) == -1){
                    val.enabled = false;
                }
            });
        }

        angular.forEach(vm.job.jobBoardDetails, function (val, key) {
            if (val.enabled == true) {
                vm.isJobBoardSelected = true;
                console.log(vm.isJobBoardSelected);
            }
        });

        vm.jobBoardDetailsCopy =  angular.copy(vm.job.jobBoardDetails);
    }

    function getRecruiters(successCallback) {
        jobService.getAllRecruiters($scope.userDetails, $rootScope.userDetails.company, function(data){
            vm.recruiterList = data;
            if(successCallback){
                successCallback();
            }
        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }

    function onRecruitersSelected(){
        vm.job.recruiters = [];
        if(vm.selectedRecruiters.length > 0){
            angular.forEach(vm.selectedRecruiters,function (recruiter, key) {
                vm.job.recruiters.push(recruiter.userId);
            });
        }
    }

    function _setRecruiters(){
        angular.forEach(vm.recruiterList, function (val, key) {
            if(vm.job.recruiters.indexOf(val.userId) > -1){
                val.selected = true;
            }else{
                val.selected = false;
            }
        });
    }

    function onTechAssessmentCheckboxClick(){
        if(vm.job.techAssessment){
            $('.slider-tech-assessment').removeAttr('disabled');
        }else{
            $('.slider-tech-assessment').attr('disabled',true);
        }
    }

    function onImageProctoringCheckboxClick(){
        if(!vm.job.imageProctoring){
            vm.job.addProctoringImagesToReport = false;
        }
    }

    function getLicensePreferences(successCallback, errorCallback){
        jobService.getLicensePreferences($rootScope.userDetails.company, vm.job.clientorbu, vm.job.id, function(data){
            vm.licensePreferenceDetails = data;
        if (_.isNull(vm.job.licensePreferences) || _.isNull(vm.job.licensePreferences.resumesToPullPerJobBoardCount)) {
            vm.job.resumesToPullPerJobBoardCount = vm.licensePreferenceDetails.resumesToPullPerJobBoardCount;
            vm.resumesToPullPerJobBoardCountCopy = vm.job.resumesToPullPerJobBoardCount;
            vm.job.isResumePullEnabled = false;
            }
            successCallback();
        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
            if(errorCallback){
                errorCallback();
            }
        });
    }

    function _createWorkflowSliders(){
        // empty the existing sliders
        $('.slider-intelligence').empty();
        $('.slider-tech-assessment').empty();
        $(document).ready(function () {
            angular.forEach(vm.licensePreferenceDetails,function (val, key) {
                switch(key){
                    case 'fourDotFiveIntellList':
                        vm.intelligencePreferences = {
                            name: val.licensePrefDetailsList[0].name,
                            value: val.licensePrefDetailsList[0].licensePrefMarkerValue,
                            company: val.licensePrefDetailsList[0].markerCompanyName
                        };
                        // if not in edit mode or if the score has not been set yet
                        if(!vm.editModeFlag || _.isNull(vm.job.score)){
                            vm.job.score = val.licensePrefDetailsList[0].licensePrefMarkerValue;
                        }
                        $('.slider-intelligence').noUiSlider({
                            range: [00,100],
                            start: [vm.job.score],
                            handles: 1,
                            connect: 'lower',
                            step: 1,
                            slide: function () {
                                var val = $(this).val();
                                vm.job.score = val;
                                $('#4dot5IntelligenceScoreDisplayValue').text(val).addClass('slider-bold');
                            },
                            set: function () {
                                var val = $(this).val();
                                vm.job.score = val;
                                $('#4dot5IntelligenceScoreDisplayValue').text(val).removeClass('slider-bold');
                            }
                        });
                        // set the value onto the intelligence score value span which displays the current value
                        $('#4dot5IntelligenceScoreDisplayValue').text(vm.job.score);
                        break;
                    case 'techAssessmentList':
                        vm.techAssessmentPreferences = {
                            name: val.licensePrefDetailsList[0].name,
                            value: val.licensePrefDetailsList[0].licensePrefMarkerValue,
                            company: val.licensePrefDetailsList[0].markerCompanyName,
                            enabled: (val.licensePrefDetailsList[0].enabled && val.licensePrefDetailsList[0].selected)
                        };
                        if(vm.editModeFlag){
                            if(vm.techAssessmentPreferences.enabled){
                                // in edit mode, if license preferences have already been defined and techAssessment value is true
                                if(!_.isNull(vm.job.licensePreferences) && vm.job.licensePreferences.techAssessment){
                                    vm.job.techAssessment = true;
                                    // tech assessment score can be null for older assessments - then set the value to 0
                                    if(_.isNull(vm.job.techAssessmentScore)){
                                        vm.job.techAssessmentScore = 0;
                                    }
                                }else{
                                    vm.job.techAssessment = false;
                                    vm.job.techAssessmentScore = 0;
                                    vm.job.techAssessmentId = null;
                                }
                            }else{
                                vm.job.techAssessment = false;
                                vm.job.techAssessmentScore = 0;
                                vm.job.techAssessmentId = null;
                            }
                        }else{
                            // in add mode, if enabled, set tech assessment to true and score to the default value
                            if(!_.isNull(vm.job.licensePreferences) && vm.techAssessmentPreferences.enabled){
                                vm.job.techAssessment = true;
                                vm.job.techAssessmentScore = val.licensePrefDetailsList[0].licensePrefMarkerValue;
                            }else{
                                vm.job.techAssessment = false;
                                vm.job.techAssessmentScore = 0;
                                vm.job.techAssessmentId = null;
                            }
                        }
                        $('.slider-tech-assessment').noUiSlider({
                            range: [00,100],
                            start: [vm.job.techAssessmentScore],
                            handles: 1,
                            connect: 'lower',
                            step: 1,
                            slide: function () {
                                var val = $(this).val();
                                vm.job.techAssessmentScore = val;
                                $('#techAssessmentScoreDisplayValue').text(val).addClass('slider-bold');
                            },
                            set: function () {
                                var val = $(this).val();
                                vm.job.techAssessmentScore = val;
                                $('#techAssessmentScoreDisplayValue').text(val).removeClass('slider-bold');
                            }
                        });
                        // set the value onto the intelligence score value span which displays the current value
                        $('#techAssessmentScoreDisplayValue').text(vm.job.techAssessmentScore);
                        if(!vm.techAssessmentPreferences.enabled){
                            $('.slider-tech-assessment').attr("disabled","disabled");
                        }
                        break;
                    case 'valueAssessmentList':
                        vm.valueAssessmentPreferences = {
                            name: val.licensePrefDetailsList[0].name,
                            value: val.licensePrefDetailsList[0].licensePrefMarkerValue,
                            company: val.licensePrefDetailsList[0].markerCompanyName,
                            enabled: (val.licensePrefDetailsList[0].enabled && val.licensePrefDetailsList[0].selected)
                        };
                        if(vm.editModeFlag){
                            if(vm.valueAssessmentPreferences.enabled){
                                // in edit mode, if valueAssessment value is true
                                if(!_.isNull(vm.job.licensePreferences) && vm.job.licensePreferences.valueAssessment){
                                    vm.job.valueAssessment = true;
                                }else{
                                    vm.job.valueAssessment = false;
                                    vm.job.valueAssessmentId = null;
                                }
                            }else{
                                vm.job.valueAssessment = false;
                                vm.job.valueAssessmentId = null;
                            }
                        }else{
                            // in add mode, if enabled, set value assessment to true
                            if(vm.valueAssessmentPreferences.enabled){
                                vm.job.valueAssessment = true;
                            }else{
                                vm.job.valueAssessment = false;
                                vm.job.valueAssessmentId = null;
                            }
                        }
                        break;
                    case 'verificationList':
                        vm.verificationPreferences = {};
                        angular.forEach(val.licensePrefDetailsList, function(v, k){
                            switch(v['licensePrefEnum']){
                                case 'EDUCATION_VERIFICATION':
                                    vm.verificationPreferences.educationVerificationName = v.name;
                                    vm.verificationPreferences.educationVerificationEnabled = (v.enabled && v.selected);
                                    if(vm.verificationPreferences.educationVerificationEnabled) {
                                        // edit mode and license preferences have been defined already
                                        if(vm.editModeFlag && !_.isNull(vm.job.licensePreferences)) {
                                            vm.job.eduVerification = vm.job.licensePreferences.educationVerification;
                                        }else{
                                            vm.job.eduVerification = true;
                                        }
                                    }else{
                                        vm.job.eduVerification = false;
                                    }
                                    break;
                                case 'EXPERIENCE_VERIFICATION':
                                    vm.verificationPreferences.experienceVerificationName = v.name;
                                    vm.verificationPreferences.experienceVerificationEnabled = (v.enabled && v.selected);
                                    if(vm.verificationPreferences.experienceVerificationEnabled) {
                                        if(vm.editModeFlag && !_.isNull(vm.job.licensePreferences)) {
                                            vm.job.expVeification = vm.job.licensePreferences.experienceVerification;
                                        }else{
                                            vm.job.expVeification = true;
                                        }
                                    }else{
                                        vm.job.expVeification = false;
                                    }
                                    break;
                            }
                        });
                        break;
                    case 'phoneScreenList':
                        vm.phoneScreenPreferences = {
                            name: val.licensePrefDetailsList[0].name,
                            enabled: (val.licensePrefDetailsList[0].enabled && val.licensePrefDetailsList[0].selected)
                        };
                        if(vm.phoneScreenPreferences.enabled){
                            // edit mode and license preferences have been defined already
                            if(vm.editModeFlag && !_.isNull(vm.job.licensePreferences)) {
                                vm.job.phoneScreen = vm.job.licensePreferences.phoneScreen;
                            }else{
                                vm.job.phoneScreen = true;
                            }
                        }else{
                            vm.job.phoneScreen = false;
                        }
                        break;
                    case 'interviewList':
                        vm.interviewPreferences = {
                            name: val.licensePrefDetailsList[0].name,
                            enabled: (val.licensePrefDetailsList[0].enabled && val.licensePrefDetailsList[0].selected)
                        };
                        if(vm.interviewPreferences.enabled){
                            // edit mode and license preferences have been defined already
                            if(vm.editModeFlag && !_.isNull(vm.job.licensePreferences)) {
                                vm.job.interview = vm.job.licensePreferences.interview;
                            }else{
                                vm.job.interview = true;
                            }
                        }else{
                            vm.job.interview = false;
                        }
                        break;
                    case 'licenseAssessmentPreferences':
                        vm.imageProctoringPreferences = {
                            name: 'Image Proctoring',
                            enabled: ( val.imageProctoringEnabled && val.imageProctoring )
                        };
                        vm.addImagesToReportPreferences = {
                            name: 'Add Proctoring Images to Report',
                            enabled: ( val.addProctoringImagesToReportEnabled && val.addProctoringImagesToReport )
                        };
                        if(vm.imageProctoringPreferences.enabled){
                            // edit mode and license preferences have been defined already
                            if(vm.editModeFlag && !_.isNull(vm.job.licensePreferences)) {
                                vm.job.imageProctoring = vm.job.licensePreferences.imageProctoring;
                            }else{
                                vm.job.imageProctoring = true;
                            }
                        }else{
                            vm.job.imageProctoring = false;
                        }
                        if(vm.addImagesToReportPreferences.enabled){
                            // edit mode and license preferences have been defined already
                            if(vm.editModeFlag && !_.isNull(vm.job.licensePreferences)) {
                                vm.job.addProctoringImagesToReport = vm.job.licensePreferences.addProctoringImagesToReport;
                            }else{
                                vm.job.addProctoringImagesToReport = true;
                            }
                        }else{
                            vm.job.addProctoringImagesToReport = false;
                        }
                        break;
                    case 'recruiterScreening':
                        vm.recruiterScreeningPreferences = {
                            name: val.licensePrefDetailsList[0].name,
                            enabled: (val.licensePrefDetailsList[0].enabled && val.licensePrefDetailsList[0].selected)
                        };
                        if(vm.recruiterScreeningPreferences.enabled){
                            // edit mode and license preferences have been defined already
                            if(vm.editModeFlag && !_.isNull(vm.job.licensePreferences)) {
                                vm.job.recruiterScreening = vm.job.licensePreferences.recruiterScreening;
                            }else{
                                vm.job.recruiterScreening = true;
                            }
                        }else{
                            vm.job.recruiterScreening = false;
                        }
                        break;
                    case 'autoMatch':
                        var enabledFlag = val.licensePrefDetailsList[0].enabled && val.licensePrefDetailsList[0].selected;
                        vm.autoMatchingPreferences = {
                            name: val.licensePrefDetailsList[0].name,
                            enabled: enabledFlag
                        };
                        if(enabledFlag){
                            // edit mode and license preferences have been defined already
                            if(vm.editModeFlag && !_.isNull(vm.job.licensePreferences)) {
                                if(!_.isNull(vm.job.licensePreferences.autoMatch)){
                                    vm.job.autoMatch = vm.job.licensePreferences.autoMatch;
                                }else{
                                    vm.job.autoMatch = false;
                                }
                            }else{
                                vm.job.autoMatch = true;
                            }
                        }else{
                            vm.job.autoMatch = false;
                        }
                    default:
                        break;
                }
            });
        });
    }

    function _createCountSliders(){
        // empty the existing sliders
        $('.slider-current-4dot5-score').empty();
        $('.slider-adjust-4dot5-score').empty();
        // create the current score slider
        $('.slider-current-4dot5-score').noUiSlider({
            range: [00, 100],
            start: [vm.job.score],
            handles: 1,
            connect: 'lower',
            step: 1,
            slide: function () {
                var val = $(this).val();
                $(this).next('span').text(val).addClass('slider-bold');
            },
            set: function () {
                var val = $(this).val();
                //$scope.sliderIntelligenceValue = parseFloat(val);
                //$scope.requisitionDetails.licensePreferenceDetails.licensePreferenceDetails.fourDotFiveIntellList.licensePrefDetailsList[0].value = $scope.sliderIntelligenceValue;
                $(this).next('span').text(val).removeClass('slider-bold');
            }
        });
        // set the current score value to the display span
        $('.slider-current-4dot5-score').next('span').text(vm.job.score);
        // disable the current 4dot5 score slider.
        $('.slider-current-4dot5-score').attr("disabled","disabled");

        // create the slider to adjust the score.
        $('.slider-adjust-4dot5-score').noUiSlider({
            range: [00, 100],
            start: [vm.job.score],
            handles: 1,
            connect: 'lower',
            step: 1,
            slide: function () {
                var val = $(this).val();
                $(this).next('span').text(val).addClass('slider-bold');
                $timeout(function(){
                    vm.loadingAdjustedCandidatesCountFlag = true;
                }, 50);
            },
            set: function () {
                var val = $(this).val();
                $(this).next('span').text(val).removeClass('slider-bold');
                vm.getCandidateCount(val, 'adjustedScore');
            }
        });
        // set the current score value to the display span
        $('.slider-adjust-4dot5-score').next('span').text(vm.job.score);
    }


    function onClientChange (){
        // change the job to empty object
        vm.job = new Job();
        vm.updateActionButtons();
    }

    function _setFileInUploadDropZone(fileName, fileSize){
        var mockFile = {
            name: fileName,
            size: fileSize
        };
        vm.reqUploadDropzone.options.addedfile.call(vm.reqUploadDropzone, mockFile);
        // delete the DOM for 'remove button' option from the file in drop zone since job is now in 'open' status
        // find the remove button in the drop zone and delete it.
        $('.dz-remove').remove();
        // find the progress bar in the drop zone and delete it.
        $('.dz-progress').remove();
        // add the success tick mark onto the file
        $('.dz-file-preview.dz-preview').addClass('dz-success');
    }

    

    /**
     * save step2 details - requisition number, recruiters,job type, sponsorships
    * */
    function saveStep2Details(successCallback, errorCallback){
        if(_.isNull(vm.job.sponsorships)){
            vm.job.sponsorships = [];
        }
        var clientOrBUId = _.isNull(vm.job.clientorbu) ? null : vm.job.clientorbu.id;
        var step2DetailsObject = {
            jobId: vm.job.id,
            title: vm.job.title,
            clientOrBUId: clientOrBUId,
            requistionNumber: vm.job.requisitionNumber,
            openings: vm.job.openings,
            jobType: vm.job.jobType,
            sponsorships: vm.job.sponsorships,
            recruiters: vm.job.recruiters,
            tab: "ASSIGN_RECRUITERS"
        };
        var updateFlag = (vm.step2DetailsSavedFlag && (vm.currentJobStatus == 'OPEN'));
        jobService.createUpdateJobDetails(updateFlag, step2DetailsObject, vm.job.company.id, clientOrBUId, $rootScope.userDetails.id, function (data) {
            vm.editTitleFlag = false;
            vm.editBUFlag = false;
            if(_.isNull(clientOrBUId)){
                vm.req.clientOrBU = null;
            }else{
                angular.forEach(vm.clientorbulist, function (val, key) {
                    if(val.id == vm.job.clientorbu.id){
                        vm.req.clientOrBU = val;
                    }
                });
            }
            vm.step2DetailsSavedFlag = true;
            if(successCallback){
                successCallback();
            }
        }, function (error) {
            if(errorCallback){
                errorCallback(error);
            }
        });
    }

    /**
     * update the job's location
     */
    function updateLocation(){
        $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            backdrop: 'static',
            templateUrl: 'app/partials/missioncontrol/uploadreq/upload-req-update-location-modal.html',
            controller: ['$uibModalInstance','jobId','location','updateLocationCallback',function ($uibModalInstance, jobId, location, updateLocationCallback) {
                var vm = this;
                vm.jobId = jobId;
                vm.location = location;
                vm.closeModal = function () {
                    $uibModalInstance.close('cancel');
                };
                vm.saveCallback = function(locationObject){
                    vm.closeModal();
                    updateLocationCallback(locationObject);
                }
            }],
            controllerAs: 'updateLocationModal',
            size: 'lg',
            resolve:{
                jobId: function () {
                    return vm.job.id;
                },
                location: function () {
                    var locationObject = angular.copy(vm.job.location);
                    locationObject.city = _.isNull(locationObject.city) ? null: locationObject.city.name;
                    return locationObject;
                },
                updateLocationCallback: function () {
                    return vm.updateLocationCallback;
                }
            }
        });
    }

    function updateLocationCallback(locationObject){
        // update the location label
        vm.job.locationLabel = locationObject.city + ', ' + locationObject.state;
        // update the location object
        locationObject.zipcode = locationObject.zipCode;
        delete locationObject.zipCode;
        var cityName = locationObject.city;
        locationObject.city = {};
        locationObject.city.name = cityName;
        vm.job.location = locationObject;
    }

    /**
     *
     * save the workflow steps
     *
     */
    function saveWorkflowSteps(successCallback, errorCallback){
        var workflowStepsObject = {
            jobId: vm.job.id,
            score: vm.job.score,
            autoMatch: vm.job.autoMatch,
            recruiterScreening: vm.job.recruiterScreening,
            techAssessmentId: vm.job.techAssessmentId,
            techAssessmentScore: vm.job.techAssessmentScore,
            techAssessment: vm.job.techAssessment,
            valueAssessmentId: vm.job.valueAssessmentId,
            valueAssessment: vm.job.valueAssessment,
            eduVerification: vm.job.eduVerification,
            expVeification: vm.job.expVeification,
            phoneScreen: vm.job.phoneScreen,
            interview: vm.job.interview,
            imageProctoring: vm.job.imageProctoring,
            addProctoringImagesToReport: vm.job.addProctoringImagesToReport,
            // jobBoardDetails: vm.job.jobBoardDetails,
            // resumesToPullPerJobBoardCount: vm.job.resumesToPullPerJobBoardCount,
            tab: "WORKFLOW_STEPS"
        };
        var updateFlag = (vm.workflowStepsSavedFlag && (vm.currentJobStatus == 'OPEN'));
        var clientOrBUId = _.isNull(vm.job.clientorbu) ? null : vm.job.clientorbu.id;
        jobService.createUpdateJobDetails(updateFlag, workflowStepsObject, vm.job.company.id, clientOrBUId, $rootScope.userDetails.id, function (data) {
            vm.workflowStepsSavedFlag = true;
            if(successCallback){
                successCallback();
            }
        }, function (error) {
            if(errorCallback){
                errorCallback(error);
            }
        });
    }

    
    /**
     *
     * save the Job boards settings
     *
     */
    function saveJobBoardsSettings(buttonType, successCallback, errorCallback) {
        
        var jobBoardsObject = {
            jobId: vm.job.id,
            score: vm.job.score,
            autoMatch: vm.job.autoMatch,
            jobBoardDetails: vm.job.jobBoardDetails,
            resumesToPullPerJobBoardCount: vm.job.resumesToPullPerJobBoardCount,
            isResumePullEnabled: vm.job.isResumePullEnabled,
            radius: vm.job.radius,
            recency: vm.job.recency,
            buttonType: buttonType,
            tab: "JOB_BOARDS"
        };
        var updateFlag = (vm.jobBoardsSettingsSavedFlag && (vm.currentJobStatus == 'OPEN'));
        var clientOrBUId = _.isNull(vm.job.clientorbu) ? null : vm.job.clientorbu.id;
        jobService.createUpdateJobDetails(updateFlag, jobBoardsObject, vm.job.company.id, clientOrBUId, $rootScope.userDetails.id, function (data) {
            vm.jobBoardsSettingsSavedFlag = true;
            if(successCallback){
                successCallback();
            }
        }, function (error) {
            if(errorCallback){
                errorCallback(error);
            }
        });
    }

    /**
     * save the adjusted 4dot5 intelligence score
     */
    function saveAdjusted4dot5Score(score, successCallback, errorCallback){
        var saveObject = {
            jobId: vm.job.id,
            score: score
        };
        jobService.update4dot5Score(saveObject, function (data) {
            vm.job.scroe = score;
            if(successCallback){
                successCallback();
            }
        }, function (error) {
            if(errorCallback){
                errorCallback(error);
            }
        });
    }

    /**
     *  save the job details
     */
    function saveJob(successCallback, errorCallback) {
        var jobSaveObject = {};
        jobSaveObject.id = vm.job.id;
        jobSaveObject.requisitionNumber = vm.job.requisitionNumber;
        jobSaveObject.recruiters = vm.job.recruiters;
        jobSaveObject.jobType = 'FULLTIME';
        jobService.saveJob(jobSaveObject, function (data) {
            vm.job = data;
            successCallback();
        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
            errorCallback();
        });
    }

    /**
     * set job status to open
     */
    function setJobStatusToOpen(successCallback, errorCallback){
        jobService.setJobStatusToOpen(vm.job.id, function (data) {
            if(successCallback){
                successCallback();
            }
        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
            if(errorCallback){
                errorCallback();
            }
        });
    }

    /**
     * run job for scoring
     */
    function runJobForScoring(successCallback, errorCallback){
        jobService.runJobForScoring(vm.job.company.id, vm.job.id, vm.job.transactionId, function (data) {
            if(successCallback){
                successCallback();
            }
        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
            if(errorCallback){
                errorCallback();
            }
        });
    }

    /**
     * get Candidates Count
     */
    function getCandidateCount(score, scoreType, successCallback, errorCallback){
        if(scoreType == 'adjustedScore'){
            vm.loadingAdjustedCandidatesCountFlag = true;
        }
        jobService.getCandidateCount(vm.job.id,score,vm.job.transactionId, function (data) {
            if(scoreType == 'currentScore'){
                // set both the current score and adjusted score candidate counts
                vm.currentScoreCandidateCounts = data;
                vm.adjustedScoreCandidateCounts = data;
            }else if (scoreType == 'adjustedScore'){
                // set only the adjusted score candidate counts
                vm.adjustedScoreCandidateCounts = data;
                vm.loadingAdjustedCandidatesCountFlag = false;
            }
            if(successCallback){
                successCallback(data);
            }
        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
            if(errorCallback){
                errorCallback(error);
            }
        });
    }

    /**
     * get candidates by requisition transaction
     */
    function getCandidatesByRequisitionTransaction(countType, scoreType){
        vm.candidatesBasedOnRequisitionTransaction = [];
        var score = vm.job.score;
        if(scoreType == 'adjustedScore'){
            // set the adjusted score
            score = $('.slider-adjust-4dot5-score').val();
        }
        jobService.getCandidatesByRequisitionTransaction(vm.job.id,score,vm.job.transactionId,countType, function (data) {
            vm.candidatesBasedOnRequisitionTransaction = data;
            vm.candidateCardNotesModal = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                backdrop: 'static',
                templateUrl: 'app/partials/missioncontrol/uploadreq/requisition-transaction-candidates-modal.html',
                controller: ['$uibModalInstance','candidates','userId',function ($uibModalInstance, candidates, userId) {
                    var vm = this;
                    vm.candidates = candidates;
                    vm.userId = userId;
                    vm.closeModal = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }],
                controllerAs: 'candidatesModal',
                size: 'lg',
                resolve:{
                    candidates: function () {
                        return vm.candidatesBasedOnRequisitionTransaction;
                    },
                    userId: function () {
                        return $rootScope.userDetails.id;
                    }
                }
            });
        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }

    /**
     * update the action buttons
     */
    function updateActionButtons() {
        var wizardCurrentStep = $('#uploadReqWizard').wizard('selectedItem').step;
        switch(wizardCurrentStep){
            case 1:
                if(vm.clientOrBUIsRequiredFlag){
                    // if - client is chosen and file exists in drop zone and there is no job id
                    // Enable the upload button
                    if(angular.isDefined(vm.req.clientOrBU) && angular.isDefined(vm.req.clientOrBU.id) && vm.fileExistsFlag && ( (!_.isNull(vm.job)) && (_.isNull(vm.job.id)) )){
                        vm.disableUploadButtonFlag = false;
                    }else{
                        vm.disableUploadButtonFlag = true;
                    }
                }else{
                    if(vm.fileExistsFlag && ( (!_.isNull(vm.job)) && (_.isNull(vm.job.id)) )){
                        vm.disableUploadButtonFlag = false;
                    }else{
                        vm.disableUploadButtonFlag = true;
                    }
                }

                // save button is always disabled in this step
                vm.disableSaveButtonFlag = true;
                // if job is defined, that means an upload was done for current client. Hence the next button should be enabled.
                if(angular.isDefined(vm.job.id) && vm.job.id != null){
                    // enable the next button
                    vm.disableNextButtonFlag = false;
                }else{
                    // disable the next button
                    vm.disableNextButtonFlag = true;
                }
                break;
            case 2:
                vm.disableSaveButtonFlag = false;
                break;
            case 3:
                vm.disableSaveButtonFlag = false;
                break;
            case 5:
                vm.disableSaveButtonFlag = false;
                break;
            default:
                break;
        }
        $timeout(function () {
            $scope.$apply();
        }, 200);
    }

    /**
     * Process the file queue in the drop zone and upload the requisition
     */
    function uploadRequisition() {
        vm.reqUploadDropzone.processQueue();
    }

    function launchAdvancedJobBoardSearch(){
        vm.candidateCardNotesModal = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            backdrop: 'static',
            templateUrl: 'app/partials/missioncontrol/uploadreq/upload-req-advanced-job-search-modal.html',
            controller: ['$uibModalInstance','requisitionService',function ($uibModalInstance,requisitionService) {
                var vm = this;
                vm.loadingFlag = true;

                vm.closeModal = function () {
                    $uibModalInstance.dismiss('cancel');
                };

                requisitionService.getJobBoardsAdvancedSearchInterface({}, function (data) {
                    vm.loadingFlag = false;
                    vm.jobBoardResponseHTML = $sce.trustAsHtml(data.html);
                    vm.jobBoardResponseCSS = data.libs.css;
                    vm.jobBoardResponseParamsJS = data.libs.jb_params_jq;
                    vm.jobBoardResponseJS = data.js;
                },function (error) {
                    vm.loadingFlag = false;
                    if ($rootScope.isOnline) {
                        alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                    }
                });
            }],
            controllerAs: 'jobBoardSearchModal',
            size: 'lg'
        });
    }

    function useJobBoardChange() {
        if (!vm.job.isResumePullEnabled) {
            // vm.job.jobBoardDetails = angular.copy(vm.jobBoardDetailsCopy);
        }
    }

    function getResumesFromJobBoards(saveCallback, errorCallback) {
        jobService.getResumesFromJobBoards(vm.job.id, vm.job.jobBoardDetails,vm.job.resumesToPullPerJobBoardCount, function (data) {
            if(saveCallback){
                saveCallback();
            }

        }, function (error) {
            if(errorCallback){
                errorCallback(error);
            }
        });
    }

    function onJobBoardSelectionChange() {
        vm.isJobBoardSelected = false;
        angular.forEach(vm.job.jobBoardDetails, function (val, key) {
            if (val.enabled == true) {
                vm.isJobBoardSelected = true;
            }
        });
        console.log(vm.isJobBoardSelected);
    }

    var unbindStateChangeEvent = $scope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        event.preventDefault();
        var currentStep = $("#uploadReqWizard").wizard('selectedItem');
        if (_hasStepDataChanged(currentStep.step)) {
            bootbox.confirm({
                closeButton: false,
                title: "<div class='alert alert-warning' style='margin-bottom: 0px;'><i class='fa fa-warning fa-fw fa-lg'></i><strong>Warning!</strong></div>",
                message: "Unsaved data on the page will be lost. Do you still want to continue?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'Cancel',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        if ($rootScope.switchClicked) {
                            $rootScope.switchClicked = false;
                            $scope.getCompaniesForLoggedinUser(function (response) {
                                $('#myModal').modal('show');
                            });
                        }
                        else {
                            unbindStateChangeEvent();
                            if (toState.name === 'login') {
                                $rootScope.$emit("CallLogoutMethod", { logoutnow: true });
                            } else {
                                $state.go(toState.name, toParams, { reload: true });
                            }
                        }
                    }
                }
            });
        }
        else {
            if ($rootScope.switchClicked) {
                $rootScope.switchClicked = false;
                $scope.getCompaniesForLoggedinUser(function (response) {
                    $('#myModal').modal('show');
                });
            }
            else {
                unbindStateChangeEvent();
                if (toState.name === 'login') {
                    $rootScope.$emit("CallLogoutMethod", { logoutnow: true });
                } else {
                    $state.go(toState.name, toParams, { reload: true });
                }
            }
        }
    });
}
