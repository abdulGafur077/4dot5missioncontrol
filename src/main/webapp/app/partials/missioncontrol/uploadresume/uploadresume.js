/**
 * This module deals with Resume management operations
 */

var uploadresumemodule = angular.module('4dot5.missioncontrolmodule.uploadresumemodule',
    []);

uploadresumemodule.constant('UPLOADRESUMECONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.uploadresume',
        URL: '/uploadresume',
        CONTROLLER: 'UploadResumeController',
        CONTROLLERAS: 'uploadResume',
        TEMPLATEURL: 'app/partials/missioncontrol/uploadresume/uploadresume.html'
    }
});

uploadresumemodule.config(['$stateProvider', 'UPLOADRESUMECONSTANTS',
    function ($stateProvider, UPLOADRESUMECONSTANTS) {
        $stateProvider.state(UPLOADRESUMECONSTANTS.CONFIG.STATE, {
            url: UPLOADRESUMECONSTANTS.CONFIG.URL,
            templateUrl: UPLOADRESUMECONSTANTS.CONFIG.TEMPLATEURL,
            controller: UPLOADRESUMECONSTANTS.CONFIG.CONTROLLER,
            controllerAs: UPLOADRESUMECONSTANTS.CONFIG.CONTROLLERAS,
            data: {
                requireLogin: true
            },
            params: {
            }
        });
    }
]);

uploadresumemodule.controller('UploadResumeController', UploadResumeController);

uploadresumemodule.$inject = ['$scope', '$state', '$stateParams', '$rootScope', 'StorageService', 'Resume', 'resumeService',
    'CandidateCount','utilityService', 'MESSAGECONSTANTS', 'UPLOADRESUMECONSTANTS', 'alertsAndNotificationsService'];

function UploadResumeController ($scope, $state, $stateParams, $rootScope, StorageService, Resume, resumeService,
                              CandidateCount, utilityService, MESSAGECONSTANTS, UPLOADRESUMECONSTANTS, alertsAndNotificationsService) {

    var vm = this;
    vm.MESSAGECONSTANTS = MESSAGECONSTANTS;
    vm.UPLOADRESUMECONSTANTS = UPLOADRESUMECONSTANTS;
    vm.sourceTypeList = [];
    vm.resumeList = [];
    vm.sourceType = null;
    vm.sourceTypeId = null;
    vm.uploadResumeResponse = null;
    vm.totalUploads = null;
    vm.candidatesList = [];
    vm.candidateCount = null;
    // flags
    vm.disableUploadButtonFlag = true;
    vm.fileExistsFlag = false;
    vm.disablePreviousButtonFlag = false;
    vm.disableSaveButtonFlag = true;
    vm.disableNextButtonFlag = true;
    vm.disableCancelButtonFlag = false;
    vm.fileUploadInProgressFlag = false;
    vm.mandatoryFieldsError = false;
    vm.disableSourceTypesFlag = false;
    vm.resumeScoringDoneFlag = false;
    vm.scoringCandidatesFlag = false;
    //controller methods
    vm.initialize = initialize;
    vm.createResumeUploadDropzone = createResumeUploadDropzone;
    vm.createUploadResumeWizard = createUploadResumeWizard
    vm.getAllSourceTypes = getAllSourceTypes;
    vm.uploadResume = uploadResume;
        // wizard steps
    vm.wizardCancel = wizardCancel;
        // save / update methods
    vm.updateActionButtons = updateActionButtons;
        // change methods
    vm.sourceTypeChanged = sourceTypeChanged;

    vm.initialize();

    function initialize() {
        //initialize the variables
        vm.resumeList = [];
        // get the source types
        vm.getAllSourceTypes();
        //create the wizard
        vm.uploadResumeWizard = vm.createUploadResumeWizard();
        //initialize the drop zone configuration
        vm.resumeUploadDropzone = vm.createResumeUploadDropzone();
    }


    /**
     * creates the resume upload drop zone.
     */
    function createResumeUploadDropzone(){
        var resumeUploadDropzone = new Dropzone("form#resumeUploadDropzone", {
            url: resumeService.getUploadUrl(),
            headers: {
                Authorization: 'Bearer ' + $rootScope.userDetails.accesstoken
            },
            paramName: function () {
                return "file";
            },
            acceptedFiles: ".pdf,.xlsx,.docx",
            maxFiles: 100,
            maxFilesize: 10,
            maxThumbnailFilesize: 10,
            dictFileSizeUnits: 'kb',
            addRemoveLinks: true,
            autoProcessQueue: false,
            uploadMultiple: true,
            parallelUploads: 100
        });

        // add event handlers for the drop zone
        resumeUploadDropzone.on('addedfile', function (file, response) {
            vm.fileExistsFlag = true;
            vm.updateActionButtons();
            $scope.$apply();
        });

        resumeUploadDropzone.on('removedfile', function (file, response) {
            console.log('number of files in drop zone -', vm.resumeUploadDropzone.files.length);
            if(vm.resumeUploadDropzone.files.length === 0){
                vm.fileExistsFlag = false;
            }
            vm.updateActionButtons();
            $scope.$apply();
        });

        resumeUploadDropzone.on('sendingmultiple', function (file, xhr, formData) {
            // disable the upload button to avoid multiple clicks
            vm.disableUploadButtonFlag = true;
            // indicate that file upload is in progress
            vm.fileUploadInProgressFlag = true;
            var resumeUpload = {};
            resumeUpload.companyId = $rootScope.userDetails.company.companyId;
            resumeUpload.userId = $rootScope.userDetails.id;
            resumeUpload.resumeSourceTypeId = vm.sourceTypeId;
            resumeUpload.buIdList = [];
            //formData.append('resumeUpload', JSON.stringify(resumeUpload));
            formData.append("companyId",resumeUpload.companyId);
            formData.append("resumeSourceTypeId",resumeUpload.resumeSourceTypeId);
            formData.append("userId",resumeUpload.userId);
            formData.append("buIdList",resumeUpload.buIdList);
        });

        resumeUploadDropzone.on('successmultiple', function (file, response) {
            // remove file upload in progress flag
            vm.fileUploadInProgressFlag = false;
            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.UPLOAD.RESUME_UPLOAD_SUCCESS, 'success');
            vm.uploadResumeResponse = response;
            vm.totalUploads = response.successCount + response.failureCount;
            // go over the list, merge each item into a resume object and add to resume list
            if(angular.isDefined(response.files) && response.files.length > 0){
                angular.forEach(response.files,function (val, key) {
                    if(val.isSuccess){
                        vm.resumeList.push(angular.merge(new Resume(),val));
                    }
                });
            }
            vm.updateActionButtons();
        });

        return resumeUploadDropzone;
    }

    /**
     * create the wizard for the upload resume workflow
     */

    function createUploadResumeWizard() {
        var uploadResumeWizard = $("#uploadResumeWizard").wizard();

        $('#uploadResumeWizard').on('actionclicked.fu.wizard', function (evt, data) {
            if(data.direction == "next"){
                switch(data.step){
                    case 1:
                        // remove the remove button from the file.
                        $('.dz-remove').remove();
                        if(!vm.resumeScoringDoneFlag){
                            // disable previous and next (complete) button when processing
                            vm.disableNextButtonFlag = true;
                            vm.disablePreviousButtonFlag = true;
                            // initialize the loading flag
                            vm.scoringCandidatesFlag = true;
                            // run resume for scoring.
                            resumeService.runResumesForScoring(vm.uploadResumeResponse, function (data) {
                                vm.candidatesList = data;
                                vm.scoringCandidatesFlag = false;
                                vm.resumeScoringDoneFlag = true;
                                // enable previous and next (complete) button after processing
                                vm.disableNextButtonFlag = false;
                                vm.disablePreviousButtonFlag = false;
                                _getCandidateCounts();
                            }, function (error) {
                                // do nothing
                            });
                        }else{
                            _getCandidateCounts();
                        }
                        break;
                    case 2:
                        // complete click
                        // handled in finish event
                        break;
                    default:
                        break;
                }
            }else if(data.direction == "previous"){
                switch(data.step){
                    case 1:
                        // not possible
                        break;
                    case 2:
                        updateActionButtons();
                        break;
                    default:
                        break;
                }
            }
        });

        $('#uploadResumeWizard').on('finished.fu.wizard', function (evt, data) {
            $state.go('missioncontrol.candidates',{teamMemberName: 'All', candidatesType: 'All'});
        });

        return uploadResumeWizard;
    }

    function wizardCancel() {
        var r = confirm("Are you sure you want to cancel ?");
        if (r == true) {
            $state.go('missioncontrol.candidates',{teamMemberName: 'All', candidatesType: 'All'});
        } else {
            // do nothing
        }
    }

    /**
     * update the action buttons
     */
    function updateActionButtons() {
        var wizardCurrentStep = $('#uploadResumeWizard').wizard('selectedItem').step;
        switch(wizardCurrentStep){
            case 1:
                // if resumes exist and scoring is completed, then the functions on this tab are disabled.
                if(vm.resumeList.length > 0){
                    vm.disableSourceTypesFlag = true;
                    vm.resumeUploadDropzone.clickable = false;
                    vm.disableUploadButtonFlag = true;
                    vm.disableNextButtonFlag = false;
                }else{
                    // if - source type is chosen and files exist in drop zone
                    // Enable the upload button
                    if((vm.sourceTypeId !== null) && vm.fileExistsFlag){
                        vm.disableUploadButtonFlag = false;
                    }else{
                        vm.disableUploadButtonFlag = true;
                    }
                    // save button is always disabled in this step
                    vm.disableSaveButtonFlag = true;
                    // if resumeList has items, that means a successful upload was done. Hence the next button should be enabled.
                    if(vm.resumeList.length > 0){
                        // enable the next button
                        vm.disableNextButtonFlag = false;
                    }else{
                        // disable the next button
                        vm.disableNextButtonFlag = true;
                    }
                }
                break;
            case 2:
                break;
            default:
                break;
        }
    }

    function _getCandidateCounts(){
        // get counts of candidates that matched jobs.
        // call the get counts API
        resumeService.getCandidateCount(vm.uploadResumeResponse.transactionID, function (data) {
            vm.candidateCount = angular.merge(new CandidateCount(), data);
            vm.updateActionButtons();
        }, function (error) {
            // do nothing
        });
    }

    function getAllSourceTypes() {
        resumeService.getAllSourceTypes(function (data) {
            vm.sourceTypeList = data;
        }, function (error) {
            if ($rootScope.isOnline) {
                alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
            }
        });
    }

    function sourceTypeChanged() {
        if(angular.isDefined(vm.sourceType) && angular.isDefined(vm.sourceType.id)){
            vm.sourceTypeId = vm.sourceType.id;
        }else{
            vm.sourceTypeId = null;
        }
    }

    function uploadResume() {
        vm.resumeUploadDropzone.processQueue();
    }
}
