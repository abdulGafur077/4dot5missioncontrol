/**
 * This module deals with user management operations
 * i.e. create user, view user and update user.
 * @author Ashok
 */

var usermanagementmodule = angular.module('4dot5.missioncontrolmodule.usermanagementmodule',
    []);

usermanagementmodule.constant('USERMANAGEMENTCONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.usermanagement',
        URL: '/user',
        CONTROLLER: 'UserManagementController',
        TEMPLATEURL: 'app/partials/missioncontrol/usermanagement/usermanagement.html',
    },
    CONTROLLER: {
        GET_ROLES_INCOMPANY_CREATE: 'api/user/getcreateroles',
        GET_ROLES_INCOMPANY_UPDATE: 'api/user/getupdateroles',
        GET_FORWARD_RECIPIENTS: 'api/user/getforwardrecipientuser',
        ASSIGN_MANAGER: 'api/user/assignmanager',
        GET_MANAGER_LIST: 'api/user/getmanagers',
        GET_MANAGER_LIST_WHILE_DEMOTING_ADMIN: 'api/user/getmanagerswhiledemotingcompanyadmin',
        GET_ENTITY_SCOPE_DETAILS: 'api/user/getuserentityscope',
        SET_ENTITY_SCOPE_DETAILS: 'api/user/setuserentityscope',
        SAVE_USER: 'api/user/saveuser',
        UPDATE_USER: 'api/user/updateuser',
        GET_COMPANY_DETAILS: 'api/company/getcompanydetails',
        SAVE_FORWARD_RECIPIENTS : 'api/user/saveforwardrecipient'
    }
});

usermanagementmodule.filter('unique', function () {
    return function (collection, keyname) {
        var output = [],
            keys = [];
        angular.forEach(collection, function (item) {
            var key = item[keyname];
            if (keys.indexOf(key) === -1) {
                keys.push(key);
                output.push(item);
            }
        });
        return output;
    };
});

usermanagementmodule.config(
    ['$stateProvider',
        'USERMANAGEMENTCONSTANTS',
        function ($stateProvider, USERMANAGEMENTCONSTANTS) {
            $stateProvider.state(USERMANAGEMENTCONSTANTS.CONFIG.STATE, {
                url: USERMANAGEMENTCONSTANTS.CONFIG.URL,
                templateUrl: USERMANAGEMENTCONSTANTS.CONFIG.TEMPLATEURL,
                controller: USERMANAGEMENTCONSTANTS.CONFIG.CONTROLLER,
                data: {
                    requireLogin: true
                },
                params: {
                    accessMode: null,
                    userObject: null,
                    manager: null,
                    purpose : null
                }
            });
        }
    ]);

usermanagementmodule.controller('UserManagementController',
    ['$scope',
        '$state',
        '$timeout',
        '$filter',
        '$compile',
        '$q',
        'moment',
        '$interval',
        '$stateParams',
        'StorageService',
        'utilityService',
        '$rootScope',
        'genericService',
        'USERMANAGEMENTCONSTANTS',
        'MESSAGECONSTANTS',
        'alertsAndNotificationsService',
        function ($scope, $state, $timeout, $filter, $compile, $q, moment, $interval, $stateParams, StorageService, utilityService, $rootScope, genericService, USERMANAGEMENTCONSTANTS, MESSAGECONSTANTS, alertsAndNotificationsService) {
            $scope.form = {};
            $scope.userprofile = {};
            $scope.userprofilereset = {};
            $scope.userprofile.userDetails = {};
            $scope.userprofile.userScopeDetails = {};
            $scope.userprofile.userForwardRecipientDetails = {};
            $scope.companytype = $rootScope.userDetails.company.companyType;
            $scope.forwardrecipients = [];
            $scope.selectedforwardRecipient = [];
            $scope.geoLocations = [];
            $scope.geoLocationscopy = [];
            $scope.laststep = false;
            $scope.profilePic = '';
            $scope.roleChanged = false;
            $scope.oldRole = null;
            $scope.managerChanged = false;
            $scope.oldManager = null;
            $scope.userCreated = false;
            $scope.roles = [];
            $scope.clientsOrBUList = [];
            $scope.switchClicked = false;
            var showConfirmationPopup = false;
            $scope.copyCompanyAddress = false;
            $scope.zipcodeCalling = false;
            $scope.zipcodeCallCompleted = false;

            $scope.formsubmitted = false;
            var valid = false;
            $scope.selectedscope = '';
            var triggercount = 0;
            $scope.updatedManager = null;

            $scope.telephoneNumberFormat = '+1 201-555-0123';
            $scope.mobileNumberFormat = '+1 201-555-0123';
            $scope.countryCodeforTelephone = 'us';
            $scope.countryCodeforMobile = 'us';

            $scope.fromUserProfile = angular.copy($stateParams.purpose);
            
            $scope.isValidWorkTelephoneEntered = false;
            $scope.isValidMobileNumberEntered = false;
            $scope.mobileNumberDirty = false;
            $scope.workTelephoneDirty = false;
            
            var nextButtonFlag = 0;
            var saveButtonFlag = 0;
            var recentButtonClick = 'nextButton';
            var dateTimeCount = 0;
            var saveButtonClicked = false;
            var nextButtonClicked = false;
            var cancelClicked = false;
            $scope.isCountyExists = false;

            $scope.single = function () {
            }

            $scope.setExamples = function () {
                $scope.telephoneNumberFormat = document.getElementById("workTelephone").placeholder;
                $scope.mobileNumberFormat = document.getElementById("mobileNumber").placeholder;
            }
            
            var isRoleExistsforUser = function(){
            	return angular.isDefined($scope.userprofile.userDetails) && angular.isDefined($scope.userprofilereset.userDetails) &&
										($scope.userprofile.userDetails !== null) && ($scope.userprofilereset.userDetails !== null) &&
										angular.isDefined($scope.userprofile.userDetails.role) && angular.isDefined($scope.userprofilereset.userDetails.role) &&
										($scope.userprofile.userDetails.role !== null) && ($scope.userprofilereset.userDetails.role !== null);
            }
            
            var isManagerExistsforUser = function(){
            	return isRoleExistsforUser() &&
            			angular.isDefined($scope.userprofile.userDetails.manager) && angular.isDefined($scope.userprofilereset.userDetails.manager);
            }
            
            /**
             * This method works for triggerng button clicks on save button
             *  only for on blur validations
             */
            var saveButtonTrigger = function () {
            	var savebtnclick = $timeout(function () {
                    angular.element('.btn-save').triggerHandler('click');
                }, 0).then(function () {
                    saveButtonClicked = false;
                    nextButtonClicked = false;
                    $timeout.cancel(savebtnclick);
                });
            };
            
            /**
             * This method works for triggerng button clicks on save button
             *  only for on blur validations
             */
            var nextButtonTrigger = function () {
            	var nextbtnclick = $timeout(function () {
                    angular.element('.create-next').triggerHandler('click');
                }, 0).then(function () {
                	$scope.zipcodeCalling = false;
                    $scope.zipcodeCallCompleted = false;
                    nextButtonClicked = false;
                    saveButtonClicked = false;
                    $timeout.cancel(nextbtnclick);
                });
            };

            $scope.isWorkTelephoneValid = function (workTelephoneValue) {
                if (workTelephoneValue !== "") {
                    if ($("#workTelephone").intlTelInput("isValidNumber")) {
                        $scope.isValidWorkTelephoneEntered = true;
                        $scope.userprofile.userDetails.workTelephone = $filter('phonenumber')(workTelephoneValue, $scope.telephoneNumberFormat);
                        $scope.form.userDetailsform.workTelephone.$setValidity('ng-intl-tel-input', true);
                    } else {
                        $scope.emptyUserworkTelephone = false;
                        $scope.invalidUserworkTelephone = true;
                        $scope.isValidWorkTelephoneEntered = false;
                        $scope.workTelephoneValueTrim = document.getElementById('workTelephone').value;
                        $scope.userprofile.userDetails.workTelephone =  $scope.workTelephoneValueTrim.replace(/ |-/gm,'');
                        $scope.form.userDetailsform.workTelephone.$setValidity('ng-intl-tel-input', false);
                    }
                } else {
                    $scope.emptyUserworkTelephone = true;
                    $scope.invalidUserworkTelephone = false;
                    $scope.isValidWorkTelephoneEntered = false;
                }
            }
            
            $("#workTelephone").keyup(function () {
                if (document.getElementById('workTelephone').value.length > 0) {
                    $scope.workTelephoneDirty = true;
                } else {
                    $scope.workTelephoneDirty = false;
                }
            });
            
            $("#mobileNumber").keyup(function () {
                if (document.getElementById('mobileNumber').value.length > 0) {
                    $scope.mobileNumberDirty = true;
                } else {
                    $scope.mobileNumberDirty = false;
                }
            });

            $scope.isMobileNumberValid = function (mobileNumberValue) {
                if (mobileNumberValue !== "") {
                    if ($("#mobileNumber").intlTelInput("isValidNumber")) {
                        $scope.isValidMobileNumberEntered = true;
                        $scope.userprofile.userDetails.mobileNumber = $filter('phonenumber')(mobileNumberValue, $scope.mobileNumberFormat);
                        $scope.form.userDetailsform.mobileNumber.$setValidity('ng-intl-tel-input', true);
                    } else {
                        $scope.emptyUsermobileNumber = false;
                        $scope.invalidUsermobileNumber = true;
                        $scope.isValidMobileNumberEntered = false;
                        $scope.mobileNumberValueTrim = document.getElementById('mobileNumber').value;
                        $scope.userprofile.userDetails.mobileNumber =  $scope.mobileNumberValueTrim.replace(/ |-/gm,'');
                        $scope.form.userDetailsform.mobileNumber.$setValidity('ng-intl-tel-input', false);
                    }
                } else {
                    $scope.emptyUsermobileNumber = true;
                    $scope.invalidUsermobileNumber = false;
                    $scope.isValidMobileNumberEntered = false;
                }

            }

            $scope.setWorkTelephoneCountryCode = function () {
                $("#workTelephone").intlTelInput("setCountry", $scope.countryCodeforTelephone);
                $scope.telephoneNumberFormat = document.getElementById("workTelephone").placeholder;
                $scope.safeApply();
                $scope.isWorkTelephoneValid(document.getElementById("workTelephone").value);
            }

            $scope.setMobileNumberCountryCode = function () {
                $("#mobileNumber").intlTelInput("setCountry", $scope.countryCodeforMobile);
                $scope.mobileNumberFormat = document.getElementById("mobileNumber").placeholder;
                $scope.safeApply();
                $scope.isMobileNumberValid(document.getElementById("mobileNumber").value);
            }

            /**
             * This method is a helper method for returning current step of the WIZARD
             */
            var getCurrentStep = function () {
                return $('#myWizard').wizard('selectedItem').step;
            };

            /**
             * This is a helper method written to check all 'null' and 'empty'
             * conditions for forword recipients date change and from date to date validations
             */
            var forwardRecipientExistingCheck = function (userprofile) {
                /**
                 * Check for forward recipient obj exists in userprofile object
                 */
                var exists = angular.isDefined(userprofile.userForwardRecipientDetails) && !angular.equals(userprofile.userForwardRecipientDetails, null) &&
                    angular.isDefined(userprofile.userForwardRecipientDetails.forwardRecipient) && !angular.equals(userprofile.userForwardRecipientDetails.forwardRecipient, null) &&
                    angular.isDefined(userprofile.userForwardRecipientDetails.forwardRecipient.firstName) && !angular.equals(userprofile.userForwardRecipientDetails.forwardRecipient.firstName, null) &&
                    !angular.equals(userprofile.userForwardRecipientDetails.forwardRecipient.firstName, '');
                return exists;
            }
            
            var isStepChanged = function(stepno){
                switch (stepno) {
                    case 1: return !angular.equals($scope.userprofile.userDetails, $scope.userprofilereset.userDetails) && $scope.form.userDetailsform.$dirty;
                    case 2: return !angular.equals($scope.userprofile.userScopeDetails, $scope.userprofilereset.userScopeDetails) && $scope.form.userScopeDetailsform.$dirty;
                    case 3: 
                    		if(forwardRecipientExistingCheck($scope.userprofile)){
                    			return !angular.equals($scope.userprofile.userForwardRecipientDetails, $scope.userprofilereset.userForwardRecipientDetails) && $scope.form.userForwardRecipientsform.$dirty;
                    		}else{
                    			return $scope.form.userForwardRecipientsform.$dirty && 
                    			(!angular.equals($scope.userprofile.userForwardRecipientDetails.forwardRecipient, $scope.userprofilereset.userForwardRecipientDetails.forwardRecipient) ||
                    			 !angular.equals($scope.userprofile.userForwardRecipientDetails.fromDate, $scope.userprofilereset.userForwardRecipientDetails.fromDate) ||
                    			 !angular.equals($scope.userprofile.userForwardRecipientDetails.toDate, $scope.userprofilereset.userForwardRecipientDetails.toDate));
                    		}
                    default: alertsAndNotificationsService.showBannerMessage('Something went wrong.. Please contact system admin.','danger');
                }
            }

            var addressDetailsFetchSuccess = function(type,data){
                if (type === 'zipchanged') {
                    $scope.userprofile.userDetails.city = data[0].city;
                    $scope.userprofile.userDetails.state = data[0].state;
                    $scope.userprofile.userDetails.county = data[0].county;
                    $scope.userprofile.userDetails.country = data[0].country;
                    $scope.countryCodeforTelephone = data[0].countryCode;
                    $scope.countryCodeforMobile = data[0].countryCode;
                    $scope.setWorkTelephoneCountryCode();
                    $scope.setMobileNumberCountryCode();
                }else {
                    if($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.CREATE){
                        $scope.userprofile.userDetails.workTelephone = angular.copy($scope.userprofilereset.userDetails.workTelephone);
                        $scope.userprofile.userDetails.mobileNumber = angular.copy($scope.userprofilereset.userDetails.mobileNumber);
                    }else if(($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.EDIT) || ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.VIEW)){
                        $scope.userprofile.userDetails.workTelephone = angular.copy($stateParams.userObject.userDetails.workTelephone);
                        $scope.userprofile.userDetails.mobileNumber = angular.copy($stateParams.userObject.userDetails.mobileNumber);
                    }
                }
                $scope.geoLocations = angular.copy(data);
                $scope.geoLocationscopy = angular.copy(data);
                $scope.$apply();
            }

            var addressDetailsFetchError = function(){
                $scope.invalidZipcode = true;
                $scope.validzipcode = false;
                $scope.geoLocations = [];
                $scope.geoLocationscopy = [];
                $scope.userprofile.userDetails.city = '';
                $scope.userprofile.userDetails.state = '';
                $scope.userprofile.userDetails.county = '';
                $scope.userprofile.userDetails.country = '';
                $scope.telephoneNumberFormat = '';
                $scope.mobileNumberFormat = '';
            }

            $scope.getAddressDetailsUpdated = function(zipcode, type){
                $scope.validzipcode = true;
                if (angular.isDefined(zipcode) && !angular.equals(zipcode, '')) {
                    utilityService.getAddressDetails(zipcode).then(function (data) {
                        addressDetailsFetchSuccess(type,data);
                    }, function (error) {
                    	if (error.status === 500) {
                            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                        }else{
                        	console.log('error : ' + angular.toJson(error));
                        	addressDetailsFetchError();
                        }
                    });
                }else{
                    $scope.geoLocations = [];
                    $scope.geoLocationscopy = [];
                }
            }

            /**
             * This method calls google api for getting address details
             * based on Zipcode.
             * will be called 
             */
            $scope.getAddressDetails = function (zipcode, type, callback) {
                $scope.validzipcode = true;
                var locations = [];
                
                console.log('zipcode : ' + zipcode);
                if (type != 'copycomanyaddress') {
                    document.getElementById("copyCompanyAddress").checked = false;
                }
                $scope.copyCompanyAddress = false;
                var url = 'maps.googleapis.com/maps/api/geocode/json?address=' + zipcode + '&key=' + MESSAGECONSTANTS.GEOCODING.API_KEY;

                if (angular.isDefined(zipcode) && !angular.equals(zipcode, '')) {
                    $.getJSON({
                        url: 'https://maps.googleapis.com/maps/api/geocode/json?address=' + zipcode + '&key=' + MESSAGECONSTANTS.GEOCODING.API_KEY,
                        data: {
                            sensor: false,
                        },
                        success: function (data, textStatus) {
                            var createGeoLocation = function (addressdetails, postalname) {
                                var addressobj = {};
                                for (var p = 0; p < addressdetails.address_components.length; p++) {
                                    for (var t = 0; t < addressdetails.address_components[p].types.length; t++) {
                                		switch (addressdetails.address_components[p].types[t]) {
                                            case 'country':
                                                addressobj.country = addressdetails.address_components[p].long_name;
                                                addressobj.countryCode = addressdetails.address_components[p].short_name;
                                                break;
                                            case 'administrative_area_level_2':
                                                addressobj.county = addressdetails.address_components[p].long_name;
                                                break;
                                            case 'administrative_area_level_1':
                                                addressobj.state = addressdetails.address_components[p].long_name;
                                                break;
                                            case 'locality':
                                                if (angular.isDefined(postalname) && postalname !== '') {
                                                    addressobj.city = postalname;
                                                } else {
                                                    addressobj.city = addressdetails.address_components[p].long_name;
                                                }
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                }

                                if ((angular.isUndefined(addressobj.city) || angular.isUndefined(addressobj.state) || angular.isUndefined(addressobj.country))) {
                                    for (var p = 0; p < addressdetails.address_components.length; p++) {
                                        for (var t = 0; t < addressdetails.address_components[p].types.length; t++) {
                                            switch (addressdetails.address_components[p].types[t]) {
                                                case 'locality':
                                                    if (angular.isUndefined(addressobj.state)) {
                                                        addressobj.state = addressdetails.address_components[p].long_name;
                                                    }
                                                    if (angular.isUndefined(addressobj.country)) {
                                                        addressobj.country = addressdetails.address_components[p].long_name;
                                                        addressobj.countryCode = addressdetails.address_components[p].short_name;
                                                    }
                                                    break;
                                                case 'administrative_area_level_2':
                                                    if (angular.isUndefined(addressobj.city)) {
                                                        if (angular.isDefined(postalname) && postalname !== '') {
                                                            addressobj.city = postalname;
                                                        } else {
                                                            addressobj.city = addressdetails.address_components[p].long_name;
                                                        }
                                                    }
                                                    if (angular.isUndefined(addressobj.state)) {
                                                        addressobj.state = addressdetails.address_components[p].long_name;
                                                    }
                                                    if (angular.isUndefined(addressobj.country)) {
                                                        addressobj.country = addressdetails.address_components[p].long_name;
                                                        addressobj.countryCode = addressdetails.address_components[p].short_name;
                                                    }

                                                    break;
                                                case 'administrative_area_level_3':
                                                    if (angular.isUndefined(addressobj.city)) {
                                                        if (angular.isDefined(postalname) && postalname !== '') {
                                                            addressobj.city = postalname;
                                                        } else {
                                                            addressobj.city = addressdetails.address_components[p].long_name;
                                                        }
                                                    }
                                                    break;
                                                case 'administrative_area_level_4':
                                                    if (angular.isUndefined(addressobj.city)) {
                                                        if (angular.isDefined(postalname) && postalname !== '') {
                                                            addressobj.city = postalname;
                                                        } else {
                                                            addressobj.city = addressdetails.address_components[p].long_name;
                                                        }
                                                    }
                                                    break;
                                                case 'sublocality' || 'sublocality_level_1' || 'sublocality_level_5':
                                                    if (angular.isDefined(postalname) && postalname !== '') {
                                                        addressobj.city = postalname;
                                                    } else {
                                                        addressobj.city = addressdetails.address_components[p].long_name;
                                                    }
                                                    break;
                                                case 'administrative_area_level_1':
                                                    if (angular.isUndefined(addressobj.city)) {
                                                        if (angular.isDefined(postalname) && postalname !== '') {
                                                            addressobj.city = postalname;
                                                        } else {
                                                            addressobj.city = addressdetails.address_components[p].long_name;
                                                        }

                                                        if (angular.isUndefined(addressobj.country)) {
                                                            addressobj.country = addressdetails.address_components[p].long_name;
                                                            addressobj.countryCode = addressdetails.address_components[p].short_name;
                                                        }
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                }
                                if(angular.isUndefined(addressobj.county)){
                                    addressobj.county = null;
                                }
                                return addressobj;
                            }
                            var checkCountyExists = function(locations,profile){
                            	for(var i = 0; i < locations.length; i++){
                            		if(angular.equals(locations[i].city,profile.userDetails.city) &&
                            		angular.isDefined(locations[i].county) && (locations.county!==null) &&
                            		angular.isDefined(profile.userDetails.county) &&
                            		(profile.userDetails.county!==null) && angular.equals(locations[i].county,profile.userDetails.county)){
                            			return true;
                            		}
                            	}
                            	return false;
                            }
                            if (data.status === 'OK') {
                            	console.log('data.status OK');
                                $scope.validzipcode = true;
                                for (var i = 0; i < data.results.length; i++) {
                                    if (angular.isDefined(data.results[i].postcode_localities) && (data.results[i].postcode_localities.length > 0)) {
                                        for (var k = 0; k < data.results[i].postcode_localities.length; k++) {
                                        	if((data.results[i].types[0] === 'postal_code')){
                                            	var location = createGeoLocation(data.results[i], data.results[i].postcode_localities[k]);
                                            	locations.push(angular.copy(location));
                                            }
                                        }
                                    } else {
                                        if((data.results[i].types[0] === 'postal_code')){
                                        	var location = createGeoLocation(data.results[i]);
                                        	locations.push(angular.copy(location));
                                        }
                                    }
                                }
                                if ((type === 'zipchanged') && !cancelClicked && locations.length) {
                                	console.log('zipchanged && !cancelClicked && locations.length');
                                    $scope.userprofile.userDetails.city = locations[0].city;
                                    $scope.userprofile.userDetails.state = locations[0].state;
                                    $scope.userprofile.userDetails.county = locations[0].county;
                                    if(angular.isDefined(locations[0].county) && (locations[0].county!==null)){
                                    	$scope.isCountyExists = true;
                                    }else{
                                    	$scope.isCountyExists = false;
                                    }
                                    $scope.userprofile.userDetails.country = locations[0].country;
                                    $scope.countryCodeforTelephone = locations[0].countryCode;
                                    $scope.countryCodeforMobile = locations[0].countryCode;
                                    $scope.setWorkTelephoneCountryCode();
                                    $scope.setMobileNumberCountryCode();
                                    $scope.zipcodeCallCompleted = true;
                                } else if ((type === 'copycomanyaddress') && !cancelClicked && locations.length) {
                                	console.log('copycomanyaddress && !cancelClicked && locations.length');
                                    $scope.userprofile.userDetails.city = locations[0].city;
                                    $scope.userprofile.userDetails.state = locations[0].state;
                                    $scope.userprofile.userDetails.county = locations[0].county;
                                    if(angular.isDefined(locations[0].county) && (locations[0].county!==null)){
                                    	$scope.isCountyExists = true;
                                    }else{
                                    	$scope.isCountyExists = false;
                                    }
                                    $scope.userprofile.userDetails.country = locations[0].country;
                                    $scope.countryCodeforTelephone = locations[0].countryCode;
                                    $scope.countryCodeforMobile = locations[0].countryCode;
                                    $scope.setWorkTelephoneCountryCode();
                                    $scope.setMobileNumberCountryCode();
                                    $scope.zipcodeCallCompleted = true;
                                } else if (cancelClicked) {
                                    locations = angular.copy($scope.copyOfGeoLocationsCopy);
                                    $scope.geoLocations = angular.copy($scope.copyOfGeoLocationsCopy);
                                    console.log('in cancelClicked $scope.geoLocations :' + angular.toJson(locations));
                                    $scope.userprofile.userDetails.city = $scope.userprofilereset.userDetails.city;
                                    $scope.userprofile.userDetails.state = $scope.userprofilereset.userDetails.state;
                                    $scope.userprofile.userDetails.county = $scope.userprofilereset.userDetails.county;
                                    if(checkCountyExists(angular.copy(locations),angular.copy($scope.userprofilereset))){
                                    	$scope.isCountyExists = true;
                                    }else{
                                    	$scope.isCountyExists = false;
                                    }
                                    $scope.userprofile.userDetails.country = $scope.userprofilereset.userDetails.country;
                                    $scope.zipcodeCallCompleted = true;
                                } else if(locations.length){
                                	console.log('locations.length with locations : ' + angular.toJson(locations));
                                	console.log('$scope.userprofilereset.userDetails : ' + angular.toJson($scope.userprofilereset.userDetails));
                                	if(checkCountyExists(angular.copy(locations),angular.copy($scope.userprofilereset))){
                                    	$scope.isCountyExists = true;
                                    }else{
                                    	$scope.isCountyExists = false;
                                    }
                                    if ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.CREATE) {
                                        /**
                                         * enters when clicked on cancel in create flow
                                         */
                                        $scope.userprofile.userDetails.workTelephone = angular.copy($scope.userprofilereset.userDetails.workTelephone);
                                        $scope.userprofile.userDetails.mobileNumber = angular.copy($scope.userprofilereset.userDetails.mobileNumber);
                                    } else if (($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.EDIT) || ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.VIEW)) {
                                        /**
                                         * enters when starting of update flow
                                         */
                                        $scope.userprofile.userDetails.workTelephone = angular.copy($stateParams.userObject.userDetails.workTelephone);
                                        $scope.userprofile.userDetails.mobileNumber = angular.copy($stateParams.userObject.userDetails.mobileNumber);
                                    }
                                    $scope.zipcodeCallCompleted = true;
                                }
                            } else {
                            	console.log('else');
                                if(!cancelClicked){
                                	console.log('!cancelClicked');
                                    $scope.validzipcode = false;
                                    locations = [];
                                    $scope.geoLocations = [];
                                    $scope.userprofile.userDetails.city = '';
                                    $scope.userprofile.userDetails.state = '';
                                    $scope.userprofile.userDetails.county = '';
                                    $scope.isCountyExists = false;
                                    $scope.userprofile.userDetails.country = '';
                                    $scope.zipcodeCallCompleted = true;
                                }else{
                                	console.log('!cancelClicked else');
                                    locations = angular.copy($scope.copyOfGeoLocationsCopy);
                                    $scope.geoLocations = angular.copy($scope.copyOfGeoLocationsCopy);
                                    $scope.userprofile.userDetails.city = $scope.userprofilereset.userDetails.city;
                                    $scope.userprofile.userDetails.state = $scope.userprofilereset.userDetails.state;
                                    $scope.userprofile.userDetails.county = $scope.userprofilereset.userDetails.county;
                                    if(checkCountyExists(angular.copy(locations),angular.copy($scope.userprofilereset))){
                                    	$scope.isCountyExists = true;
                                    }else{
                                    	$scope.isCountyExists = false;
                                    }
                                    $scope.userprofile.userDetails.country = $scope.userprofilereset.userDetails.country;
                                    $scope.zipcodeCallCompleted = true;
                                }
                            }
                            $scope.geoLocations = angular.copy(locations);
                            if(!$scope.geoLocations.length){
                            	console.log('!$scope.geoLocations.length');
                            	$scope.validzipcode = false;
                            	$scope.userprofile.userDetails.city = '';
                                $scope.userprofile.userDetails.state = '';
                                $scope.userprofile.userDetails.county = '';
                                $scope.isCountyExists = false;
                                $scope.userprofile.userDetails.country = '';
                            	$scope.zipcodeCallCompleted = true;
                            }
                            console.log('$scope.geoLocations : ' + angular.toJson($scope.geoLocations));
                            $scope.$apply();
                            if (type === 'start') {
                            	$scope.form.userDetailsform.$setPristine();
                            }
                            if(typeof callback != "undefined"){
                            	return callback($scope.geoLocations);
                            }
                        },
                        error: function (data) {
                            $scope.validzipcode = false;
                            $scope.geoLocations = [];
                            $scope.isCountyExists = false;
                            if(typeof callback != "undefined"){
                            	return callback('success');
                            }
                        }
                    });
                } else {
                    $scope.geoLocations = [];
                    $scope.isCountyExists = false;
                    if(typeof callback != "undefined"){
                    	return callback('success');
                    }
                }
            }

            $scope.getZipCodeDetails = function (zipcode, type) {
                 nextButtonClicked = false;
                 saveButtonClicked = false;
                if ($scope.form.userDetailsform.zipcode.$valid) {
                	$scope.zipcodeCalling = true;
                	$scope.zipcodeCallCompleted = false;
                    if (angular.isDefined(zipcode)) {
                        cancelClicked = false;
                    }
                    $scope.getAddressDetails(zipcode, type,function(response){
                    	if(angular.isDefined($scope.userprofilereset.userDetails.county) && ($scope.userprofilereset.userDetails.county !== null)
                    			&& ($scope.userprofile.userDetails.zipcode === $scope.userprofilereset.userDetails.zipcode)){
                			$scope.userprofile.userDetails.county = $scope.userprofilereset.userDetails.county;
                			$scope.safeApply();
                        }
                    	if(saveButtonClicked){
                    		saveButtonTrigger();
                    	}else if(nextButtonClicked){
                    		nextButtonTrigger();
                    	}
                    });
                } else {
                    $scope.geoLocations = [];
                    saveButtonClicked = false;
                    nextButtonClicked = false;
                    $scope.userprofile.userDetails.city = '';
                    $scope.userprofile.userDetails.state = '';
                    $scope.userprofile.userDetails.county = '';
                    $scope.isCountyExists = false;
                    $scope.userprofile.userDetails.country = '';
                }
            }

            var changeForwordRecipientsArray = function(frobj){
                frobj.ticked = false;
                frobj.current = null;
                var output = $scope.getFullName(frobj.firstName,frobj.lastName);
                if (output.length > 57) {
                    frobj.fullName = output.substring(0, 57) + "...";
                } else {
                    frobj.fullName = output;
                }
                return frobj;
            }

            var populateForwardRecipient = function () {
                if (!angular.equals($scope.userprofile.userForwardRecipientDetails, null)) {
                    if (!angular.equals($scope.userprofile.userForwardRecipientDetails.forwardRecipient, null)) {
                        if (angular.isDefined($scope.userprofile.userForwardRecipientDetails.forwardRecipient.userId)) {
                            if($scope.userprofile.userForwardRecipientDetails.forwardRecipient.lastName != null){
                                var output = $scope.userprofile.userForwardRecipientDetails.forwardRecipient.firstName+' '+$scope.userprofile.userForwardRecipientDetails.forwardRecipient.lastName;
                            }else{
                                var output = $scope.userprofile.userForwardRecipientDetails.forwardRecipient.firstName;
                            }
                            if (output.length > 57) {
                                $scope.userprofile.userForwardRecipientDetails.forwardRecipient["fullName"] = output.substring(0, 57) + "...";
                            }else{
                                $scope.userprofile.userForwardRecipientDetails.forwardRecipient["fullName"] = output;
                            }
                            $scope.forwardrecipients.push(angular.copy($scope.userprofile.userForwardRecipientDetails.forwardRecipient));
                        }
                    }
                }
            }

            /**
             * This condition seperates whether the module is loaded
             * on account of create or update or to just view.
             * @access for create, view and update
             */
            if (($stateParams.manager !== null) && ($stateParams.accessMode !== null) && ($stateParams.userObject === null)) {
                var managerDetails = angular.copy($stateParams.manager);
                if (!angular.equals(managerDetails, null) && !angular.equals(managerDetails.lastName, null)) {
                    managerDetails.firstName = managerDetails.firstName + ' ' + managerDetails.lastName;
                }
                $scope.accessMode = angular.copy($stateParams.accessMode);
                StorageService.set('useraccessmode', $scope.accessMode);
                StorageService.remove('userprofile');
                StorageService.set('manager', managerDetails);
                $scope.userprofilereset = angular.copy($scope.userprofile);
            } else if (($stateParams.userObject !== null) && ($stateParams.accessMode !== null) && ($stateParams.manager === null)) {
                //view or update user access
                //Setting the Phone number values to be true
                $scope.isValidMobileNumberEntered = true;
                $scope.isValidWorkTelephoneEntered = true;
                $scope.getAddressDetails(angular.copy($stateParams.userObject.userDetails.zipcode), 'start' ,function(response){
                	$scope.geoLocationscopy = angular.copy($scope.geoLocations);
                });
                $scope.accessMode = angular.copy($stateParams.accessMode);
                StorageService.set('useraccessmode', $scope.accessMode);
                StorageService.remove('manager');
                StorageService.set('userprofile', angular.copy($stateParams.userObject));
                $scope.userprofile = angular.copy($stateParams.userObject);
                if ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.VIEW) {
                    //access mode view
                    angular.element('.access').attr({ 'readonly': true, 'disabled': true });
                    if (!angular.equals($scope.userprofile.userForwardRecipientDetails, null)) {
                        if (!angular.equals($scope.userprofile.userForwardRecipientDetails.forwardRecipient, null)) {
                            if (angular.isDefined($scope.userprofile.userForwardRecipientDetails.forwardRecipient.userId)) {
                                $scope.userprofile.userForwardRecipientDetails.forwardRecipient.ticked = true;
                                if ($scope.userprofile.userForwardRecipientDetails.forwardRecipient.lastName != null) {
                                    var output = $scope.userprofile.userForwardRecipientDetails.forwardRecipient.firstName + " " + $scope.userprofile.userForwardRecipientDetails.forwardRecipient.lastName;
                                } else {
                                    var output = $scope.userprofile.userForwardRecipientDetails.forwardRecipient.firstName;
                                }
                                if (output.length > 57) {
                                    $scope.userprofile.userForwardRecipientDetails.forwardRecipient["fullName"] = output.substring(0, 57) + "...";
                                } else {
                                    $scope.userprofile.userForwardRecipientDetails.forwardRecipient["fullName"] = output;
                                }
                                $scope.forwardrecipients.push($scope.userprofile.userForwardRecipientDetails.forwardRecipient);
                            }
                        }
                    }
                } else if ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.EDIT) {
                    //access mode edit
                    angular.element('#email').attr({ 'readonly': true, 'disabled': true });
                    if (($scope.userprofile.userDetails.userId === $rootScope.userDetails.id)) {
                        angular.element('#role').attr({ 'readonly': true, 'disabled': true });
                    }
                    if($scope.isCompanyAdmin($scope.userprofile.userDetails.role)){
                        angular.element('#manager').attr({ 'readonly': true, 'disabled': true });
                    }
                    var originalManagerObj = angular.copy($scope.userprofile.userDetails.manager);
                    if (!angular.equals(originalManagerObj, null) && !angular.equals(originalManagerObj.lastName, null)) {
                        originalManagerObj.firstName = originalManagerObj.firstName.split(" ")[0];
                    }
                    populateForwardRecipient();
                }
                $scope.userprofilereset = angular.copy($scope.userprofile);
            } else {
                // when refresh
                var useraccessmode = StorageService.get('useraccessmode');
                var userprofile = StorageService.get('userprofile');
                var manager = StorageService.get('manager');
                if ((manager !== null) && (useraccessmode !== null) && (userprofile === null)) {
                    // create mode
                    var managerDetails = manager;
                    $scope.accessMode = useraccessmode;
                    $scope.userprofilereset = angular.copy($scope.userprofile);
                } else if ((userprofile !== null) && (useraccessmode !== null) && (manager === null)) {
                    //update or view mode
                    $scope.userprofile = userprofile;
                    $scope.accessMode = useraccessmode;
                    StorageService.remove('manager');
                    if ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.VIEW) {
                        angular.element('.access').attr({ 'readonly': true, 'disabled': true });
                    } else if ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.EDIT) {
                        //access mode edit
                    }
                    $scope.userprofilereset = angular.copy($scope.userprofile);
                } else {
                    // if objects are not there both in storage and stateparams
                    console.log('objects are not there both in storage and stateparams');
                }
            }

            /**
             * This method helps to get all roles in company.
             * Assign all roles to a scope variable and use in roles dropdown
             * @access for create, view and update
             */
            var getRolesIntheCompany = function () {
                if ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.CREATE) {
                    var mgrid = managerDetails.roleId;
                }
                if ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.CREATE) {
                    genericService.getObjects(USERMANAGEMENTCONSTANTS.CONTROLLER.GET_ROLES_INCOMPANY_CREATE + '/' + mgrid + '/' + $rootScope.userDetails.company.companyType).then(function (data) {
                        $scope.roles = data;
                        if ($scope.isinHost()) {
                            for (var i = 0; i < $scope.roles.length; i++) {
                                if ($scope.roles[i].name === $rootScope.MESSAGECONSTANTS.ROLES.FOURDOTFIVEADMIN) {
                                    $scope.userprofile.userDetails.role = $scope.roles[i];
                                    $scope.userprofilereset.userDetails.role = $scope.roles[i];
                                }
                            }
                        } else if ($scope.isinStaffing() || $scope.isinCorporate()) {
                            if ($scope.roles.length === 1) {
                                $scope.userprofile.userDetails.role = $scope.roles[0];
                                $scope.userprofilereset.userDetails.role = $scope.roles[0];
                            } else if ($scope.roles.length > 1) {
                                for (var i = 0; i < $scope.roles.length; i++) {
                                    if ($scope.roles[i].name === $rootScope.MESSAGECONSTANTS.ROLES.RECRUITER) {
                                        $scope.userprofile.userDetails.role = angular.copy($scope.roles[i]);
                                        $scope.userprofilereset.userDetails.role = angular.copy($scope.roles[i]);
                                    }
                                }
                            }
                        }
                        
                        if (!(($scope.roles.length === 1) && $scope.isCompanyAdmin($scope.roles[0]))) {
                            $scope.userprofile.userDetails.manager = managerDetails;
                            $scope.userprofilereset.userDetails.manager = managerDetails;
                        }
                    }, function (error) {
                    	if($rootScope.isOnline){
                        	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                        }
                    });
                } else if ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.EDIT) {
                    //Setting the Phone number values to be true
                    $scope.isValidMobileNumberEntered = true;
                    $scope.isValidWorkTelephoneEntered = true;
                    //Setting manager role id
                    var managertogetroles = StorageService.get('managers');
                    var loggedInUserRole = $rootScope.userDetails.roleScreenRef.role.name;
                    if (angular.isDefined(managertogetroles) && (managertogetroles !== null) && (managertogetroles.length > 0)) {
                        mgrid = managertogetroles[0].roleId;
                    }
                    
                    if ($scope.isFourDotFiveAdmin($rootScope.userDetails.roleScreenRef.role) || $scope.isSuperUser($rootScope.userDetails.roleScreenRef.role)) {
                        if($scope.isFourDotFiveAdmin($rootScope.userDetails.roleScreenRef.role) && $scope.isFourDotFiveAdmin($scope.userprofilereset.userDetails.role)){
                            $scope.roles = [];
                            $scope.roles.push($scope.userprofilereset.userDetails.role);
                        }else {
                            genericService.getObjects(USERMANAGEMENTCONSTANTS.CONTROLLER.GET_ROLES_INCOMPANY_UPDATE + '/' + mgrid + '/' + $rootScope.userDetails.company.companyType).then(function (data) {
                                $scope.roles = data;
                                $scope.form.userDetailsform.$setPristine();
                            }, function (error) {
                            	if($rootScope.isOnline){
                                	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                                }
                            });
                        }
                        
                    }else {
                        if ($scope.userprofile.userDetails.manager !== null) {
                            genericService.getObjects(USERMANAGEMENTCONSTANTS.CONTROLLER.GET_ROLES_INCOMPANY_UPDATE + '/' + $scope.userprofilereset.userDetails.manager.roleId + '/' + $rootScope.userDetails.company.companyType).then(function (data) {
                                $scope.roles = data;
                                $scope.form.userDetailsform.$setPristine();
                            }, function (error) {
                            	if($rootScope.isOnline){
                                	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                                }
                            });
                        } else {
                            $scope.roles.push($scope.userprofile.userDetails.role);
                        }
                    }
                } else if ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.VIEW) {
                    $scope.roles.push($scope.userprofile.userDetails.role);
                }
            };
            
            getRolesIntheCompany();

            /**
             * This method gets all managers list and assign to scope variable
             * @access for create, view and update
             */
            var getManagersList = function (userid, roleid) {
                genericService.getObjects(USERMANAGEMENTCONSTANTS.CONTROLLER.GET_MANAGER_LIST + '/' + userid + '/' + roleid).then(function (data) {
                    $scope.managerList = data;
                    if (($scope.userprofile.userDetails.role.name === $rootScope.MESSAGECONSTANTS.ROLESNAMES.FOURDOTFIVEADMIN_NAME)) {
                        $scope.managerList.push($scope.userprofile.userDetails.manager);
                    }
                }, function (error) {
                	if($rootScope.isOnline){
                    	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    }
                    console.log('error : ' + angular.toJson(error));
                });
            };

            if ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.EDIT) {
                if (!$scope.isCompanyAdmin($scope.userprofilereset.userDetails.role)) {
                    getManagersList(angular.copy($stateParams.userObject.userDetails.userId), angular.copy($stateParams.userObject.userDetails.role.id));
                }
            }

            $("#workTelephone").on("countrychange", function (e, countryData) {
                $scope.countryCodeforTelephone = countryData.iso2;
                $scope.form.userDetailsform.workTelephone.$setDirty();
                $scope.setWorkTelephoneCountryCode();
            });

            $("#mobileNumber").on("countrychange", function (e, countryData) {
                $scope.countryCodeforMobile = countryData.iso2;
                $scope.form.userDetailsform.mobileNumber.$setDirty();
                $scope.setMobileNumberCountryCode();
            });

            /**
             * This method is used for creating dummy user entity scope
             * This should be used only for company admin.
             */
            var createEntityScopeForAdmin = function () {
                var entityObj = {};
                entityObj.allIndustries = true;
                entityObj.allJobRequisitionTitleList = true;
                entityObj.allclientOrBU = true;
                entityObj.requisitionCategory = false;
                entityObj.requisitionCategoryList = [];
                if ($scope.companytype === MESSAGECONSTANTS.COMPANY_TYPES.STAFFING) {
                    entityObj.requisitionCategoryList[0] = {};
                    entityObj.requisitionCategoryList[0].name = 'Retained';
                    entityObj.requisitionCategoryList[0].isSelected = true;
                    entityObj.requisitionCategoryList[1] = {};
                    entityObj.requisitionCategoryList[1].name = 'Contingent';
                    entityObj.requisitionCategoryList[1].isSelected = true;
                    entityObj.requisitionCategoryList[2] = {};
                    entityObj.requisitionCategoryList[2].name = 'Both';
                    entityObj.requisitionCategoryList[2].isSelected = true;
                } else if ($scope.companytype === MESSAGECONSTANTS.COMPANY_TYPES.CORPORATE) {
                    entityObj.requisitionCategoryList[0] = {};
                    entityObj.requisitionCategoryList[0].name = 'Retained';
                    entityObj.requisitionCategoryList[0].isSelected = true;
                    entityObj.requisitionCategoryList[1] = {};
                    entityObj.requisitionCategoryList[1].name = 'Contingent';
                    entityObj.requisitionCategoryList[1].isSelected = true;
                    entityObj.requisitionCategoryList[2] = {};
                    entityObj.requisitionCategoryList[2].name = 'Both';
                    entityObj.requisitionCategoryList[2].isSelected = true;
                }
                return entityObj;
            }

            var getESfor4dot5Admin = function(callback){
                utilityService.getCompaniesforUser(angular.copy($scope.userprofile.userDetails.role),angular.copy($scope.userprofile.userDetails.userId)).then(function (data) {
                    $scope.userprofile.userScopeDetails = data;
                    $scope.userprofilereset.userScopeDetails = angular.copy($scope.userprofile.userScopeDetails);
                    StorageService.set('wizardoperations', true);
                    $scope.stepTrigger('nextbtnclick');
                }, function (error) {
                	if($rootScope.isOnline){
                    	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    }
                });
            }

            /**
             * This method gets all entity scope datails 
             * based on company type.
             * Assign to scope variable and finally when leaving 
             * from step assign values to userprofile object.
             * @access for only create. No need for view and edit access as 
             * details will be a part of user object
             */
            var getEntityScopeDetails = function () {
                StorageService.remove('wizardoperations');
                if($scope.isinHost()){
                    getESfor4dot5Admin();
                }else if($scope.isFourDotFiveAdmin($scope.userprofile.userDetails.role) || $scope.isCompanyAdmin($scope.userprofile.userDetails.role)){
                    $scope.userprofile.userScopeDetails = createEntityScopeForAdmin();
                    $scope.userprofilereset.userScopeDetails = angular.copy($scope.userprofile.userScopeDetails);
                    if ($scope.isFourDotFiveAdmin($scope.userprofile.userDetails.role)) {
                         $scope.laststep = true;
                    }
                    nextButtonClicked = false;
                    saveButtonClicked = false;
                    StorageService.set('wizardoperations', true);
                    $scope.stepTrigger('nextbtnclick');
                }else{
                    if(($scope.userprofile.userScopeDetails === null) || ($scope.roleChanged) || ($scope.managerChanged)){
                        utilityService.getUserRoleEntityScope(angular.copy($scope.userprofile)).then(function (data) {
                            $scope.userprofile.userScopeDetails = data;
                            $scope.userprofilereset.userScopeDetails = angular.copy($scope.userprofile.userScopeDetails);
                            StorageService.set('wizardoperations', true);
                            $scope.stepTrigger('nextbtnclick');
                        }, function (error) {
                        	if($rootScope.isOnline){
                            	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                            }
                            console.log('error');
                        });
                    }else{
                        StorageService.set('wizardoperations', true);
                        $scope.stepTrigger('nextbtnclick');
                    }
                }
            };

            /**
             * This method gets all forward recipients list and include data as typeahead search
             * Also triggers the next step initiation
             * @access for create and update only. No need for view access as 
             * details will be a part of user object
             */
            var getFarwardRecipients = function () {
                genericService.getObjects(USERMANAGEMENTCONSTANTS.CONTROLLER.GET_FORWARD_RECIPIENTS + '/' + $scope.userprofile.userDetails.userId).then(function (data) {
                    $scope.form.userForwardRecipientsform.$setPristine();
                    var recipients = [];
                    if (($scope.forwardrecipients.length > 0) && (data.length > 0)) {
                        for (var i = 0; i < data.length; i++) {
                            var frExistArray = $filter('filter')($scope.forwardrecipients,{userId:data[i].userId}, true);
                            if(!frExistArray.length){
                                recipients.push(changeForwordRecipientsArray(data[i]));
                            }
                        }
                        for (var p = 0; p < recipients.length; p++) {
                        	if(recipients[p].lastName != null){
                        		 var output = recipients[p].firstName+" "+recipients[p].lastName;
                        	}else{
                        		 var output = recipients[p].firstName;
                            }
                            if (output.length > 57) {
                                    recipients[p]["fullName"] = output.substring(0, 57) + "...";
                            }else {
                        		 recipients[p]["fullName"] = output;
                            }
                            $scope.forwardrecipients.push(angular.copy(recipients[p]));
                        }
                    } else if (($scope.forwardrecipients.length === 0) && (data.length > 0)) {
                    	for (var p = 0; p < data.length; p++) {
                            recipients.push(changeForwordRecipientsArray(data[p]));
                        }
                        $scope.forwardrecipients = recipients;
                    }
                    $scope.forwardRecipenetsCopy = angular.copy($scope.forwardrecipients);
                    nextButtonClicked = false;
                    saveButtonClicked = false;
                    StorageService.set('wizardoperations', true);
                    $scope.laststep = true;
                    if (angular.equals($scope.userprofile.userForwardRecipientDetails, null)) {
                        $scope.userprofile.userForwardRecipientDetails = {};
                    }
                    $scope.userprofilereset = angular.copy($scope.userprofile);
                    $scope.formsubmitted = false;
                    $scope.stepTrigger('nextbtnclick');
                }, function (error) {
                	if($rootScope.isOnline){
                    	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    }
                    console.log('error : ' + angular.toJson(error));
                });
            };

            /**
             * This method helps to go to next step by checking
             * what are all the data needed for next step
             * @access for only view access
             */
            $scope.getNextStepData = function () {
                if (triggercount === 0) {
                	nextButtonClicked = false;
                	saveButtonClicked = false;
                    triggercount = angular.copy(triggercount + 1);
                    $scope.currentstep = getCurrentStep();
                    StorageService.remove('wizardoperations');
                    if ($scope.currentstep === 1) {
                        getEntityScopeDetails();
                    } else if ($scope.currentstep === 2) {
                        triggercount = 0
                        if ($scope.isinHost()) {
                            StorageService.remove('wizardoperations');
                            $scope.userprofile.userDetails.manager.firstName = $scope.userprofile.userDetails.manager.firstName.split(" ")[0];
                            $state.go('missioncontrol.teammembers', { managerObj: $scope.userprofile.userDetails.manager }, { reload: true });
                        } else {
                            StorageService.set('wizardoperations', true);
                        }
                    } else if ($scope.currentstep === 3) {
                        triggercount = 0
                        StorageService.remove('wizardoperations');
                        if (angular.isDefined($scope.userprofile.userDetails.manager) && !angular.equals($scope.userprofile.userDetails.manager, null) && !angular.equals($scope.userprofile.userDetails.manager.lastName, null)) {
                            $scope.userprofile.userDetails.manager.firstName = $scope.userprofile.userDetails.manager.firstName.split(" ")[0];
                        }
                        $state.go('missioncontrol.teammembers', { managerObj: $scope.userprofile.userDetails.manager }, { reload: true });
                    }
                }
            }

            var showBootboxNotification = function (messagedata) {
                var deferred = $q.defer();
                bootbox.confirm({
                    closeButton: false,
                    message: messagedata,
                    buttons: {
                        confirm: {
                            label: 'Yes',
                            className: 'btn-success'
                        },
                        cancel: {
                            label: 'Cancel',
                            className: 'btn-danger'
                        }
                    },
                    callback: function (result) {
                        deferred.resolve(result);
                    }
                });
            }

            /**
             * This method gets all managers list while demoting company admin and assign to scope variable
             * @access for update
             */
            var getManagerListWhileDemotingAdmin = function (userid) {
                genericService.getObjects(USERMANAGEMENTCONSTANTS.CONTROLLER.GET_MANAGER_LIST_WHILE_DEMOTING_ADMIN + '/' + userid + '/' + $rootScope.userDetails.company.companyId).then(function (data) {
                    if (data.length === 0) {
                        alertsAndNotificationsService.showBannerMessage('Cannot change the role of the only existing Admin !', 'danger');
                        $scope.userprofile.userDetails.role = angular.copy($scope.userprofilereset.userDetails.role);
                        $scope.form.userScopeDetailsform.$setPristine();
                    } else {
                        $scope.managerList = data;
                    }
                }, function (error) {
                	if($rootScope.isOnline){
                    	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    }
                    console.log('error : ' + angular.toJson(error));
                });
            };

            /**
             * This method is used to take confimation input
             * from users for role change immediately after
             * changing role dropdown in UI
             */
            var showRoleChangeConfirmation = function (message) {
                var setNewUserRole = function () {
                    if ($scope.isCompanyAdmin($scope.userprofilereset.userDetails.role)) {
                        $scope.userprofile.userDetails.manager = null;
                        getManagerListWhileDemotingAdmin($scope.userprofile.userDetails.userId);
                        angular.element('#manager').attr({ 'readonly': false, 'disabled': false });
                    } else {
                        getManagersList($scope.userprofile.userDetails.userId, $scope.userprofile.userDetails.role.id);
                    }
                }
                bootbox.confirm({
                    closeButton: false,title: "<div class='alert alert-warning' style='margin-bottom: 0px;'><i class='fa fa-warning fa-fw fa-lg'></i><strong>Warning!</strong></div>",message: message,
                    buttons: {
                        confirm: {
                            label: 'Yes',className: 'btn-success'
                        },cancel: {
                            label: 'No',className: 'btn-danger'
                        }
                    },
                    callback: function (result) {
                        if (result) {
                            setNewUserRole();
                        } else {
                            $scope.userprofile.userDetails.role = angular.copy($scope.userprofilereset.userDetails.role);
                            $scope.form.userDetailsform.role.$setPristine();
                        }
                    }
                });
            }

            var showRoleChangeAlert = function (message) {
                bootbox.alert({ 
                    closeButton: false,
                    title: '<div class="alert alert-danger"><i class="fa fa-times-circle fa-fw fa-lg"></i><strong>Oh snap!</strong></div>',
                    message: message,
                    callback: function () {
                        $scope.userprofile.userDetails.role = angular.copy($scope.userprofilereset.userDetails.role);
                        
                        $scope.form.userDetailsform.role.$setPristine();
                    }
                });
            }

            $scope.checkRoleChange = function(){
                if($scope.isSeniorRecruiter($scope.userprofilereset.userDetails.role)){
                    showRoleChangeConfirmation(MESSAGECONSTANTS.NOTIFICATIONS.USER.USER_ALERT_WITH_NO_REPORTEES);
                }else if($scope.isRecruiter($scope.userprofilereset.userDetails.role)){
                    showRoleChangeConfirmation(MESSAGECONSTANTS.NOTIFICATIONS.USER.USER_ALERT_WITH_NO_REPORTEES);
                }else if($scope.isRecruitingManager($scope.userprofilereset.userDetails.role)){
                    if($scope.isRecruiter($scope.userprofile.userDetails.role) || $scope.isSeniorRecruiter($scope.userprofile.userDetails.role)){
                        if (angular.isDefined($scope.userprofilereset.reporteeCount) && ($scope.userprofilereset.reporteeCount > 0)) {
                            showRoleChangeAlert(MESSAGECONSTANTS.NOTIFICATIONS.USER.USER_ALERT_WITH_REPORTEES);
                        } else if (angular.isDefined($scope.userprofile.reporteeCount) && ($scope.userprofile.reporteeCount === 0)) {
                            showRoleChangeConfirmation(MESSAGECONSTANTS.NOTIFICATIONS.USER.USER_ALERT_WITH_NO_REPORTEES);
                        }
                    }else if($scope.isCompanyAdmin($scope.userprofile.userDetails.role)){
                    	 if ($scope.userprofile.reporteeCount > 0) {
                    		 showRoleChangeConfirmation(MESSAGECONSTANTS.NOTIFICATIONS.USER.CHANGE_MANAGER_TO_ADMIN_REPORTEES);
                         } else {
                        	 showRoleChangeConfirmation(MESSAGECONSTANTS.NOTIFICATIONS.USER.USER_ALERT_WITH_NO_REPORTEES);
                         }
                    }
                }else if($scope.isCompanyAdmin($scope.userprofilereset.userDetails.role)){
                    if($scope.isRecruiter($scope.userprofile.userDetails.role) || $scope.isSeniorRecruiter($scope.userprofile.userDetails.role)){
                        if (angular.isDefined($scope.userprofilereset.reporteeCount) && ($scope.userprofilereset.reporteeCount > 0)) {
                            showRoleChangeAlert(MESSAGECONSTANTS.NOTIFICATIONS.USER.USER_ALERT_WITH_REPORTEES);
                        } else if (angular.isDefined($scope.userprofile.reporteeCount) && ($scope.userprofile.reporteeCount === 0)) {
                            showRoleChangeConfirmation(MESSAGECONSTANTS.NOTIFICATIONS.USER.USER_ALERT_WITH_NO_REPORTEES);
                        }
                    }else if($scope.isRecruitingManager($scope.userprofile.userDetails.role)){
                        showRoleChangeConfirmation(MESSAGECONSTANTS.NOTIFICATIONS.USER.USER_ALERT_WITH_NO_REPORTEES);
                    }
                }else{
                    alert('Something went wrong ! please try again later');
                }
            }

            /**
             * This method will be called when ever role is changed from ui
             */
            $scope.changeRoleObject = function () {
                var roleid = angular.copy($scope.userprofile.userDetails.role.id);
                for (var i = 0; i < $scope.roles.length; i++) {
                    if (roleid === $scope.roles[i].id) {
                        $scope.userprofile.userDetails.role = angular.copy($scope.roles[i]);
                    }
                }
                var rolechageifvalid = ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.EDIT) ||
                    (($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.CREATE) && ((nextButtonFlag > 0) || (saveButtonFlag > 0)));
                if (rolechageifvalid && ($scope.userprofile.userDetails.role.id !== $scope.userprofilereset.userDetails.role.id)) {
                    if ($scope.isCompanyAdmin($scope.userprofilereset.userDetails.role)) {
                        $scope.userprofile.userDetails.manager = null;
                        genericService.getObjects(USERMANAGEMENTCONSTANTS.CONTROLLER.GET_MANAGER_LIST_WHILE_DEMOTING_ADMIN + '/' + $scope.userprofilereset.userDetails.userId + '/' + $rootScope.userDetails.company.companyId).then(function (data) {
                            if (data.length === 0) {
                                showRoleChangeAlert('Cannot change the role of the only existing Admin.');
                            } else {
                                $scope.checkRoleChange();
                            }
                        }, function (error) {
                        	if($rootScope.isOnline){
                            	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                            }
                        });
                    } else {
                        $scope.checkRoleChange();
                    }
                }else if(rolechageifvalid){
                    getManagersList($scope.userprofile.userDetails.userId, $scope.userprofile.userDetails.role.id);
                }
            }

            var assignOrChangeManager = function (managerid, userid) {
                genericService.addObject(USERMANAGEMENTCONSTANTS.CONTROLLER.ASSIGN_MANAGER + '/' + managerid + '/' + userid).then(function (data) {
                    $scope.userprofile.userDetails.manager = angular.copy(data.userDetails.manager);
                    if (!angular.equals($scope.userprofile.userDetails.manager, null) && !angular.equals($scope.userprofile.userDetails.manager.lastName, null)) {
                        $scope.userprofile.userDetails.manager.firstName = $scope.userprofile.userDetails.manager.firstName + ' ' + $scope.userprofile.userDetails.manager.lastName;
                    }
                    $scope.userprofilereset = angular.copy($scope.userprofile);
                    $scope.form.userDetailsform.$setPristine();
                }, function (error) {
                	if($rootScope.isOnline){
                    	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    }
                    $scope.userprofile.userDetails.manager = angular.copy($scope.userprofilereset.userDetails.manager);
                });
            }

            /**
             * This method will be called when ever Manager is changed from ui
             */
            $scope.changeManagerObject = function () {
                if ($scope.userprofile.reporteeCount > 0) {
                    var changeManagerMessage = MESSAGECONSTANTS.NOTIFICATIONS.USER.CHANGE_MANAGER_REPORTEES;
                } else {
                    var changeManagerMessage = MESSAGECONSTANTS.NOTIFICATIONS.USER.CHANGE_MANAGER;
                }
                bootbox.confirm({
                    closeButton: false,
                    title: "<div class='alert alert-warning' style='margin-bottom: 0px;'><i class='fa fa-warning fa-fw fa-lg'></i><strong>Warning!</strong></div>",
                    message: changeManagerMessage,
                    buttons: {
                        confirm: {
                            label: 'Yes',
                            className: 'btn-success'
                        },
                        cancel: {
                            label: 'No',
                            className: 'btn-danger'
                        }
                    },
                    callback: function (result) {
                        if (result) {
                            var managerid = angular.copy($scope.userprofile.userDetails.manager.userId);
                            for (var i = 0; i < $scope.managerList.length; i++) {
                                if (managerid === $scope.managerList[i].userId) {
                                    $scope.userprofile.userDetails.manager = angular.copy($scope.managerList[i]);
                                    $scope.updatedManager = angular.copy($scope.userprofile.userDetails.manager);
                                }
                            }
                        } else {
                            $scope.userprofile.userDetails.manager = angular.copy($scope.userprofilereset.userDetails.manager);
                        }
                    }
                });
            }

            var uncheckSelectedFR = function (selecteduser) {
                for (var i = 0; i < $scope.forwardrecipients.length; i++) {
                    if ($scope.forwardrecipients[i].userId === selecteduser.userId) {
                        $scope.forwardrecipients[i].ticked = false;
                    }
                }
            }

            /**
             * This method will have a track of whick forward recipient is selected
             */
            $scope.selectForwardRecipient = function () {
            	dateTimeCount = 1;
            	saveButtonClicked = false;
            	nextButtonClicked = false;
                if (angular.isDefined($scope.userprofile.userForwardRecipientDetails.forwardRecipient) && !angular.equals($scope.userprofile.userForwardRecipientDetails.forwardRecipient, null) && angular.isDefined($scope.selectedforwardRecipient[0])) {
                    if (angular.equals($scope.userprofile.userForwardRecipientDetails.forwardRecipient.userId, angular.copy($scope.selectedforwardRecipient[0].userId))) {
                        uncheckSelectedFR($scope.selectedforwardRecipient[0]);
                        $scope.userprofile.userForwardRecipientDetails.forwardRecipient = null;
                        $scope.userprofile.userForwardRecipientDetails.fromDate = null;
                        $scope.userprofile.userForwardRecipientDetails.toDate = null;
                        $scope.form.userForwardRecipientsform.$setValidity('date', true);
                        $scope.form.userForwardRecipientsform.fromDate.$setValidity('date', true);
                        $scope.form.userForwardRecipientsform.toDate.$setValidity('date', true);
                        $scope.form.userForwardRecipientsform.$setValidity('time', true);
                        $scope.form.userForwardRecipientsform.fromTime.$setValidity('time', true);
                        $scope.form.userForwardRecipientsform.toTime.$setValidity('time', true);
                        $scope.form.userForwardRecipientsform.$setDirty();
                    } else {
                        $scope.userprofile.userForwardRecipientDetails.forwardRecipient = angular.copy($scope.selectedforwardRecipient[0]);
                        $scope.form.userForwardRecipientsform.$setDirty();
                    }
                } else {
                    $scope.userprofile.userForwardRecipientDetails.forwardRecipient = angular.copy($scope.selectedforwardRecipient[0]);
                    $scope.form.userForwardRecipientsform.$setDirty();
                }
            }

            var getChangesForRoleandManager = function () {
                if (!angular.equals($scope.userprofile.userDetails.role, $scope.userprofilereset.userDetails.role)) {
                    $scope.roleChanged = true;
                    $scope.oldRole = angular.copy($scope.userprofilereset.userDetails.role);
                } else {
                    $scope.roleChanged = false;
                    $scope.oldRole = null;
                }
                if (!angular.equals($scope.userprofile.userDetails.manager, $scope.userprofilereset.userDetails.manager)) {
                    $scope.managerChanged = true;
                    $scope.oldManager = angular.copy($scope.userprofilereset.userDetails.manager);
                } else {
                    $scope.managerChanged = false;
                    $scope.oldManager = null;
                }
            }

            $scope.onZipcodeKeyPress = function(zipcode,type){
                if (event.charCode == 13){
                } 
            }

            var showUserDetailsSaveMessage = function (buttonclicked) {
                var userDetailsSaveMessageCheck = ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.EDIT) ||
                    (($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.CREATE) && ((nextButtonFlag > 0) || (saveButtonFlag > 0)));
                    
                if (userDetailsSaveMessageCheck) {
                    if ($scope.roleChanged && $scope.managerChanged) {
                        if($scope.isCompanyAdmin($scope.userprofilereset.userDetails.role)){
                            var successmessage = "The role for the user '<b><i class='wrap-word'>" + $scope.getFullName($scope.userprofile.userDetails.firstName,$scope.userprofile.userDetails.lastName) + "</i></b>' was successfully changed from '<b><i class='wrap-word'>" + $scope.oldRole.description +  "</i></b>' to '<b><i class='wrap-word'>" + $scope.userprofile.userDetails.role.description + "</i></b>' <br> " +
                                            "The manager of the user '<b><i class='wrap-word'>" + $scope.getFullName($scope.userprofile.userDetails.firstName,$scope.userprofile.userDetails.lastName) + "</i></b>' has been set to '<b><i class='wrap-word'>" + 
                                            $scope.getFullName($scope.userprofile.userDetails.manager.firstName,null) + "</i></b>'.";
                        }else{
                            var successmessage = "The role for the user '<b><i class='wrap-word'>" + $scope.getFullName($scope.userprofile.userDetails.firstName,$scope.userprofile.userDetails.lastName) + "</i></b>' was successfully changed from '<b><i class='wrap-word'>" + $scope.oldRole.description + "</i></b>' to '<b><i class='wrap-word'>" + $scope.userprofile.userDetails.role.description + "</i></b>' <br> " +
                                            "The manager of the user '<b><i class='wrap-word'>" + $scope.getFullName($scope.userprofile.userDetails.firstName,$scope.userprofile.userDetails.lastName) + "</i></b>' was changed from '<b><i class='wrap-word'>" + 
                                            $scope.getFullName($scope.oldManager.firstName,null) + "</i></b>' to '<b><i class='wrap-word'>" +
                                            $scope.getFullName($scope.userprofile.userDetails.manager.firstName,null) + "</i></b>'.";
                        }
                        
                        alertsAndNotificationsService.showBannerMessage(successmessage, 'success');
                    } else if ($scope.roleChanged) {
                        var successmessage = "The role for the user '<b><i class='wrap-word'>" + $scope.getFullName($scope.userprofile.userDetails.firstName,$scope.userprofile.userDetails.lastName) + "</i></b>' was successfully changed from '<b><i class='wrap-word'>" + $scope.oldRole.description + "</i></b>' to '<b><i class='wrap-word'>" + $scope.userprofile.userDetails.role.description + "'.";
                        alertsAndNotificationsService.showBannerMessage(successmessage, 'success');
                    } else if ($scope.managerChanged) {
                        var successmessage = "The manager of the user '<b><i class='wrap-word'>" + $scope.getFullName($scope.userprofile.userDetails.firstName,$scope.userprofile.userDetails.lastName) + "</i></b>' was changed from '<b><i class='wrap-word'>" +
                        					$scope.getFullName($scope.oldManager.firstName,null) + "</i></b>' to '<b><i class='wrap-word'>" +
                        					$scope.getFullName($scope.userprofile.userDetails.manager.firstName,null) + "</i></b>'.";
                        alertsAndNotificationsService.showBannerMessage(successmessage, 'success');
                    } else if(buttonclicked === 'save'){
                        if (($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.CREATE)) {
                            if($scope.userCreated){
                                alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.USER.USER + " '<b><i class='wrap-word'>" + $scope.getFullName($scope.userprofile.userDetails.firstName,$scope.userprofile.userDetails.lastName) + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.USER.SAVESUCCESS, 'success');
                            }else{
                                alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.USER.USER + " '<b><i class='wrap-word'>" + $scope.getFullName($scope.userprofile.userDetails.firstName,$scope.userprofile.userDetails.lastName) + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.USER.CREATESUCCESS, 'success');
                            }
                        } else {
                            if($scope.isLogedinUser($scope.userprofile.userDetails)){
                                alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.USER.OWN_PROFILE_UPDATE, 'success');
                            }else{
                                alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.USER.USER + " '<b><i class='wrap-word'>" + $scope.getFullName($scope.userprofile.userDetails.firstName,$scope.userprofile.userDetails.lastName)+ "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.USER.UPDATESUCCESS, 'success');
                            }
                        }
                    }
                }
            }

            var changeFRSelectedAfterApi = function(data,status){
            	saveButtonClicked = false;
            	nextButtonClicked = false;
                if(status === 'success'){
                	var newFRArray = [];
                    if ((data.userForwardRecipientDetails.forwardRecipient !== null) && ($scope.forwardrecipients.length > 0)) {
                        for (var i = 0; i < $scope.forwardrecipients.length; i++) {
                        		if($scope.forwardrecipients[i].lastName != null){
                        			var output = $scope.forwardrecipients[i].firstName+' '+$scope.forwardrecipients[i].lastName;
                        		}else{
                        			var output = $scope.forwardrecipients[i].firstName
                        		}
                        		if(output.length > 57){
                        			$scope.forwardrecipients[i]['fullName'] = output.substring(0, 57) + "...";
                        		}else{
                        			$scope.forwardrecipients[i]['fullName'] = output;
                        		}
                            if (!angular.equals($scope.forwardrecipients[i].userId,data.userForwardRecipientDetails.forwardRecipient.userId)) {
                                newFRArray.push(changeForwordRecipientsArray($scope.forwardrecipients[i]));
                            }else{
                            	if(data.userForwardRecipientDetails.forwardRecipient.lastName != null){
                            		var output = data.userForwardRecipientDetails.forwardRecipient.firstName+' '+data.userForwardRecipientDetails.forwardRecipient.lastName;
                            	}else{
                            		var output = data.userForwardRecipientDetails.forwardRecipient.firstName;
                            	}
                            	if(output.length > 57){
                            		data.userForwardRecipientDetails.forwardRecipient['fullName'] = output.substring(0, 57) + "...";
                        		}else{
                        			data.userForwardRecipientDetails.forwardRecipient['fullName'] = output;
                        		}
                                newFRArray.push(data.userForwardRecipientDetails.forwardRecipient);
                            }
                        }
                    }else if((data.userForwardRecipientDetails.forwardRecipient === null) && ($scope.forwardrecipients.length > 0)){
                    	for (var i = 0; i < $scope.forwardrecipients.length; i++) {
                    		if($scope.forwardrecipients[i].lastName != null){
                    			var output = $scope.forwardrecipients[i].firstName+' '+$scope.forwardrecipients[i].lastName;
                    		}else{
                    			var output = $scope.forwardrecipients[i].firstName
                    		}
                    		if(output.length > 57){
                    			$scope.forwardrecipients[i]['fullName'] = output.substring(0, 57) + "...";
                    		}else{
                    			$scope.forwardrecipients[i]['fullName'] = output;
                    		}
                    		$scope.forwardrecipients[i]['current'] = null;
                    		newFRArray.push($scope.forwardrecipients[i]);
                    	}
                    }
                    $scope.forwardrecipients = newFRArray;
                    $scope.forwardRecipenetsCopy = angular.copy($scope.forwardrecipients);
                }else if( status === 'error'){
                	console.log("error");
                    if ((data.userForwardRecipientDetails.forwardRecipient !== null) && ($scope.forwardrecipients.length > 0)) {
                        var newFRArray = [];
                        for (var i = 0; i < $scope.forwardrecipients.length; i++) {
                            if (!angular.equals($scope.forwardrecipients[i].userId,data.userForwardRecipientDetails.forwardRecipient.userId)) {
                                newFRArray.push(changeForwordRecipientsArray($scope.forwardrecipients[i]));
                            }else{
                                //$scope.selectedforwardRecipient.push(data.userForwardRecipientDetails.forwardRecipient);
                                newFRArray.push(data.userForwardRecipientDetails.forwardRecipient);
                            }
                        }
                        $scope.selectedforwardRecipient = [];
                        $scope.forwardrecipients = newFRArray;
                        $scope.forwardRecipenetsCopy = angular.copy($scope.forwardrecipients);
                    }else {
                        var newFRArray = [];
                        for (var i = 0; i < $scope.forwardrecipients.length; i++) {
                            newFRArray.push(changeForwordRecipientsArray($scope.forwardrecipients[i]));
                        }
                        $scope.selectedforwardRecipient = [];
                        $scope.forwardrecipients = newFRArray;
                        $scope.forwardRecipenetsCopy = angular.copy($scope.forwardrecipients);
                    }
                }
            }

            /**
             * This method will be called from manageStepData() if some thing is changed on ui in step1;
             * This method deals with saving of user step1 details and assigning manager for saved user.
             * @access to CREATE MODE and EDIT MODE when clicked on 'NEXT' Button
             */
            var manageUserDetails = function (url) {
                var roleOrManagerChangeCheck = ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.EDIT) ||
                    (($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.CREATE) && ((nextButtonFlag > 0) || (saveButtonFlag > 0)));
                if(roleOrManagerChangeCheck){
                    getChangesForRoleandManager();
                }
                utilityService.saveUserProfileObject(url, angular.copy($scope.userprofile)).then(function (data) {
                    $scope.formsubmitted = false;
                    $scope.userprofile = data;
                    nextButtonFlag++;
                    recentButtonClick = 'nextButton';
                    nextButtonClicked = false;
                    showUserDetailsSaveMessage('next');
                    $scope.geoLocationscopy = angular.copy($scope.geoLocations);
                    $scope.userprofilereset = angular.copy($scope.userprofile);
                    $scope.form.userDetailsform.$setPristine();
                    getEntityScopeDetails();
                }, function (error) {
                    triggercount = 0;
                    $scope.formsubmitted = false;
                    if(error.statusCode === '410'){
                    	if ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.CREATE) {
                    		if (angular.isDefined(managerDetails) && !angular.equals(managerDetails, null) && !angular.equals(managerDetails.lastName, null)) {
                                managerDetails.firstName = managerDetails.firstName.split(" ")[0];
                            }
                            var managerReturnObj = managerDetails;
                    	}else if ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.EDIT) {
                    		var managerReturnObj = originalManagerObj;
                    	}
                    	StorageService.remove('wizardoperations');
                        if(angular.copy($stateParams.purpose) !== null){
                       	 	$state.go('missioncontrol.dashboard', null , { reload: true });
                        }else{
                        	$state.go('missioncontrol.teammembers', { managerObj: managerReturnObj }, { reload: false });
                        }
                    }else{
                    	if($rootScope.isOnline){
                    		alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    	}
                    	if(isRoleExistsforUser() && isManagerExistsforUser()){
                        	$scope.userprofile.userDetails = angular.copy($scope.userprofilereset.userDetails);
                        }
                    }
                });
            };
            
            /**
             * This method will be called from manageStepData() if some thing is changed on ui in step2;
             * This method deals with saving of user step2 details.
             * @access to CREATE MODE and EDIT MODE when clicked on 'NEXT' Button
             */
            var manageUserScopeDetails = function () {
                //$scope.userprofilereset.userScopeDetails = angular.copy($scope.userprofile.userScopeDetails);
                utilityService.setUserRoleEntityScope(angular.copy($scope.userprofile)).then(function (data) {
                    $scope.userprofile = data;
                    nextButtonFlag++;
                    recentButtonClick = 'nextButton';
                    nextButtonClicked = false;
                    $scope.userprofilereset = angular.copy($scope.userprofile);
                    $scope.form.userScopeDetailsform.$setPristine();
                    if ($scope.isFourDotFiveAdmin($scope.userprofilereset.userDetails.role)) {
                        if ($scope.accessMode === $rootScope.MESSAGECONSTANTS.ACCESS_MODES.CREATE) {
                            if (!$scope.userCreated) {
                                $scope.userCreated = true;
                                alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.USER.USER + " '<b><i class='wrap-word'>" + $scope.getFullName($scope.userprofile.userDetails.firstName,$scope.userprofile.userDetails.lastName) + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.USER.CREATESUCCESS, 'success');
                            } else {
                                alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.USER.USER + " '<b><i class='wrap-word'>" + $scope.getFullName($scope.userprofile.userDetails.firstName,$scope.userprofile.userDetails.lastName) + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.USER.SAVESUCCESS, 'success');
                            }
                            managerDetails.firstName = managerDetails.firstName.split(" ")[0];
                            var managerReturnObj = managerDetails;
                        }else{
                        	 if(angular.copy($stateParams.purpose) !== null){
                                 StorageService.remove('wizardoperations');
                                 $state.go('missioncontrol.dashboard', null , { reload: true });
                             }else{
                            	 alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.USER.USER + " '<b><i>" + $scope.getFullName($scope.userprofile.userDetails.firstName,$scope.userprofile.userDetails.lastName) + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.USER.UPDATESUCCESS, 'success');
                            	 var managerReturnObj = originalManagerObj;
                             }
                        }
                        StorageService.remove('wizardoperations');
                        $state.go('missioncontrol.teammembers', { managerObj: managerReturnObj }, { reload: false });
                    } else {
                        $scope.roleChanged = false;
                        $scope.managerChanged = false;
                        getFarwardRecipients();
                    }
                }, function (error) {
                	triggercount = 0;
                    $scope.formsubmitted = false;
                	if($rootScope.isOnline){
                    	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    }
                	$scope.userprofile.userScopeDetails = angular.copy($scope.userprofilereset.userScopeDetails);
                });
            };
            
            /**
             * This method will be called from manageStepData() if some thing is changed on ui in step3;
             * This method deals with saving of user step3 details and returning to team members page again.
             * @access to CREATE MODE and EDIT MODE when clicked on 'NEXT' Button
             */
            var manageUserForwardRecipientDetails = function (url) {
                utilityService.setForwardRecipientDetails(url, angular.copy($scope.userprofile)).then(function (data) {
                    $scope.formsubmitted = false;
                    $scope.userprofile = data;
                    nextButtonFlag++;
                    recentButtonClick = 'nextButton';
                    nextButtonClicked = false;
                    $scope.form.userForwardRecipientsform.$setPristine();
                    $scope.userprofilereset = angular.copy($scope.userprofile);
                    if ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.CREATE) {
                        if (!$scope.userCreated) {
                            $scope.userCreated = true;
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.USER.USER + " '<b><i class='wrap-word'>" + $scope.getFullName($scope.userprofile.userDetails.firstName,$scope.userprofile.userDetails.lastName) + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.USER.CREATESUCCESS, 'success');
                        } else {
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.USER.USER + " '<b><i class='wrap-word'>" + $scope.getFullName($scope.userprofile.userDetails.firstName,$scope.userprofile.userDetails.lastName) + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.USER.SAVESUCCESS, 'success');
                        }
                        if (angular.isDefined(managerDetails) && !angular.equals(managerDetails, null) && !angular.equals(managerDetails.lastName, null)) {
                            managerDetails.firstName = managerDetails.firstName.split(" ")[0];
                        }
                        var managerReturnObj = managerDetails;
                    } else if ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.EDIT) {
                        if($scope.isLogedinUser($scope.userprofile.userDetails)){
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.USER.OWN_PROFILE_UPDATE, 'success');
                        }else{
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.USER.USER + " '<b><i class='wrap-word'>" + $scope.getFullName($scope.userprofile.userDetails.firstName,$scope.userprofile.userDetails.lastName)+ "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.USER.UPDATESUCCESS, 'success');
                        }
                        var managerReturnObj = originalManagerObj;
                    }
                    StorageService.remove('wizardoperations');
                    if(angular.copy($stateParams.purpose) !== null){
                   	 	$state.go('missioncontrol.dashboard', null , { reload: true });
                    }else{
                    	$state.go('missioncontrol.teammembers', { managerObj: managerReturnObj }, { reload: false });
                    }
                }, function (error) {
                    console.log('error');
                    $scope.formsubmitted = false;
                    triggercount = 0;
                    $scope.userprofile.userForwardRecipientDetails = angular.copy($scope.userprofilereset.userForwardRecipientDetails);
                    if ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.CREATE) {
                        //alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.USER.CREATEERROR, 'danger');
                    	if (error.status === 500) {
                    		alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                        }else {
                        	dateTimeCount = 1;
                        	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                            changeFRSelectedAfterApi(angular.copy($scope.userprofilereset),'error');
                        }
                    } else if ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.EDIT) {
                    	if (error.status === 500) {
                    		alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                        } else {
                        	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                            changeFRSelectedAfterApi(angular.copy($scope.userprofilereset),'error');
                        }
                    }
                    $scope.form.userForwardRecipientsform.$setPristine();
                });
            };

            /**
             * This method will be called whenever user clicked on 'NEXT' buttton
             * This method checks whether any changes are made in UI with respective steps
             */
            $scope.manageStepData = function () {
                if (triggercount === 0) {
                    StorageService.remove('wizardoperations');
                    triggercount = angular.copy(triggercount + 1);
                    $scope.formsubmitted = true;
                    nextButtonClicked = true;
                    saveButtonClicked = false;
                    valid = false;
                    var currentStep = getCurrentStep();
                    switch (currentStep) {
                        case 1:
                            valid = $scope.form.userDetailsform.$valid && !$scope.isContentinObjSame($scope.userprofile.userDetails.address1,$scope.userprofile.userDetails.address2) && $scope.isValidMobileNumberEntered && $scope.isValidWorkTelephoneEntered && (!$scope.zipcodeCalling || ($scope.zipcodeCalling && $scope.zipcodeCallCompleted));
                            if (valid && (angular.equals($scope.userprofile.userDetails, $scope.userprofilereset.userDetails))) {
                                getEntityScopeDetails();
                                //getClientsOrBu();
                            } else if (valid) {
                                $scope.laststep = false;
                                if ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.CREATE) {
                                    var url = USERMANAGEMENTCONSTANTS.CONTROLLER.SAVE_USER + '/' + $rootScope.userDetails.company.companyId;
                                } else if ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.EDIT) {
                                    var url = USERMANAGEMENTCONSTANTS.CONTROLLER.UPDATE_USER + '/' + $rootScope.userDetails.company.companyId;
                                }
                                manageUserDetails(url);
                            } else {
                            	$scope.form.userDetailsform.workTelephone.visited = true;
                            	$scope.form.userDetailsform.mobileNumber.visited = true;
                                triggercount = 0;
                            }
                            break;
                        case 2:
                            valid = $scope.form.userDetailsform.$valid && $scope.form.userScopeDetailsform.$valid && $scope.isValidMobileNumberEntered && $scope.isValidWorkTelephoneEntered;
                            if (valid && (angular.equals($scope.userprofile.userScopeDetails, $scope.userprofilereset.userScopeDetails))) {
                                if ($scope.isFourDotFiveAdmin($scope.userprofilereset.userDetails.role)) {
                                    if ($scope.accessMode === $rootScope.MESSAGECONSTANTS.ACCESS_MODES.CREATE) {
                                        if (!$scope.userCreated) {
                                            $scope.userCreated = true;
                                            if((nextButtonFlag > 0) && (recentButtonClick === 'nextButton')){
                                                alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.USER.USER + " '<b><i class='wrap-word'>" + $scope.getFullName($scope.userprofile.userDetails.firstName,$scope.userprofile.userDetails.lastName) + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.USER.CREATESUCCESS, 'success');
                                            }
                                        } else {
                                            if((nextButtonFlag > 0) && (recentButtonClick === 'nextButton')){
                                                alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.USER.USER + " '<b><i class='wrap-word'>" + $scope.getFullName($scope.userprofile.userDetails.firstName,$scope.userprofile.userDetails.lastName) + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.USER.SAVESUCCESS, 'success');
                                            }
                                        }
                                        managerDetails.firstName = managerDetails.firstName.split(" ")[0];
                                        var managerReturnObj = managerDetails;
                                    }else{
                                        if((nextButtonFlag > 0) && (recentButtonClick === 'nextButton')){
                                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.USER.USER + " '<b><i class='wrap-word'>" + $scope.getFullName($scope.userprofile.userDetails.firstName,$scope.userprofile.userDetails.lastName) + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.USER.UPDATESUCCESS, 'success');
                                        }else{
                                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.USER.NOTHING_UPDATED + " '<b><i class='wrap-word'>" + $scope.getFullName($scope.userprofile.userDetails.firstName,$scope.userprofile.userDetails.lastName) + "</i></b>' ", 'success'); 
                                        }
                                        var managerReturnObj = originalManagerObj;
                                    }
                                    $timeout(function () {
                                        StorageService.remove('wizardoperations');
                                        if(angular.copy($stateParams.purpose) !== null){
                                            $state.go('missioncontrol.dashboard', null , { reload: true });
                                        }else{
                                            $state.go('missioncontrol.teammembers', { managerObj: managerReturnObj }, { reload: false });
                                        }
                                    }, 4000);
                                } else {
                                    getFarwardRecipients();
                                }
                            } else if (valid) {
                                manageUserScopeDetails();
                            } else {
                                triggercount = 0;
                            }
                            break;
                        case 3:
                            valid = $scope.form.userDetailsform.$valid && $scope.form.userScopeDetailsform.$valid && $scope.form.userForwardRecipientsform.$valid && $scope.isValidMobileNumberEntered && $scope.isValidWorkTelephoneEntered;
                            if (valid && !isStepChanged(currentStep)) {
                                if ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.EDIT) {
                                    if((nextButtonFlag > 0) || (saveButtonFlag > 0)){
                                        if($scope.isLogedinUser($scope.userprofile.userDetails)){
                                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.USER.OWN_PROFILE_UPDATE, 'success');
                                        }else{
                                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.USER.USER + " '<b><i class='wrap-word'>" + $scope.getFullName($scope.userprofile.userDetails.firstName,$scope.userprofile.userDetails.lastName)+ "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.USER.UPDATESUCCESS, 'success');
                                        }
                                    }else{
                                       alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.NOTHING_TO_UPDATE, 'success');
                                    }
                                    var managerReturnObj = originalManagerObj;
                                } else {
                                    if (!$scope.userCreated) {
                                        $scope.userCreated = true;
                                        if((nextButtonFlag > 0) && (recentButtonClick === 'nextButton')){
                                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.USER.USER + " '<b><i class='wrap-word'>" + $scope.getFullName($scope.userprofile.userDetails.firstName,$scope.userprofile.userDetails.lastName) + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.USER.CREATESUCCESS, 'success');
                                        }
                                    } else {
                                        if((nextButtonFlag > 0) || (saveButtonFlag > 0)){
                                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.USER.USER + " '<b><i class='wrap-word'>" + $scope.getFullName($scope.userprofile.userDetails.firstName,$scope.userprofile.userDetails.lastName) + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.USER.SAVESUCCESS, 'success');
                                        }
                                    }
                                    if (angular.isDefined(managerDetails) && !angular.equals(managerDetails, null) && !angular.equals(managerDetails.lastName, null)) {
                                        managerDetails.firstName = managerDetails.firstName.split(" ")[0];
                                    }
                                    var managerReturnObj = managerDetails;
                                }
                                $timeout(function () {
                                    StorageService.remove('wizardoperations');
                                    if(angular.copy($stateParams.purpose) !== null){
                                        $state.go('missioncontrol.dashboard', null , { reload: true });
                                    }else{
                                        $state.go('missioncontrol.teammembers', { managerObj: managerReturnObj }, { reload: false });
                                    }
                                }, 4000);
                            } else if (valid) {
                                var url = USERMANAGEMENTCONSTANTS.CONTROLLER.SAVE_FORWARD_RECIPIENTS + '/' + $rootScope.userDetails.company.companyId;
                                manageUserForwardRecipientDetails(url);
                            } else {
                                triggercount = 0;
                            }
                            break;
                        default:
                            $scope.text = "Something went wrong";
                    }
                }
            }

            /**
             * This method will be called from saveStepData() if some thing is changed on ui in step1;
             * This method deals with saving of user step1 details and assigning manager for saved user.
             * @access to CREATE MODE and EDIT MODE when clicked on 'SAVE' Button
             */
            var saveUserDetails = function (url) {
                var roleOrManagerChangeCheck = ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.EDIT) ||
                    (($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.CREATE) && ((nextButtonFlag > 0) || (saveButtonFlag > 0)));
                if(roleOrManagerChangeCheck){
                    getChangesForRoleandManager();
                }
                utilityService.saveUserProfileObject(url, angular.copy($scope.userprofile)).then(function (data) {
                    $scope.formsubmitted = false;
                    $scope.userprofile = data;
                    saveButtonFlag++;
                    recentButtonClick = 'saveButton';
                    saveButtonClicked = false;
                    showUserDetailsSaveMessage('save');
                    $scope.geoLocationscopy = angular.copy($scope.geoLocations);
                    $scope.userprofilereset = angular.copy($scope.userprofile);
                    if ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.CREATE) {
                        $scope.userCreated = true;
                    } else {
                        $scope.userCreated = false;
                    }
                    $scope.form.userDetailsform.$setPristine();
                }, function (error) {
                	triggercount = 0;
                    $scope.formsubmitted = false;
                    if(error.statusCode === '410'){
                    	if ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.CREATE) {
                    		if (angular.isDefined(managerDetails) && !angular.equals(managerDetails, null) && !angular.equals(managerDetails.lastName, null)) {
                                managerDetails.firstName = managerDetails.firstName.split(" ")[0];
                            }
                            var managerReturnObj = managerDetails;
                    	}else if ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.EDIT) {
                    		var managerReturnObj = originalManagerObj;
                    	}
                    	StorageService.remove('wizardoperations');
                        if(angular.copy($stateParams.purpose) !== null){
                       	 	$state.go('missioncontrol.dashboard', null , { reload: true });
                        }else{
                        	$state.go('missioncontrol.teammembers', { managerObj: managerReturnObj }, { reload: false });
                        }
                    }else{
                    	if($rootScope.isOnline){
                    		alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    	}
                    	if(isRoleExistsforUser() && isManagerExistsforUser()){
                        	$scope.userprofile.userDetails = angular.copy($scope.userprofilereset.userDetails);
                        }
                    }
                });
            };

            /**
             * This method will be called from saveStepData() if some thing is changed on ui in step2;
             * This method deals with saving of user step2 details.
             * @access to CREATE MODE and EDIT MODE when clicked on 'SAVE' Button
             */
            var saveUserScopeDetails = function (url) {
                //$scope.userprofilereset.userScopeDetails = angular.copy($scope.userprofile.userScopeDetails);
                utilityService.setUserRoleEntityScope(angular.copy($scope.userprofile)).then(function (data) {
                    $scope.userprofile = angular.copy(data);
                    saveButtonFlag++;
                    recentButtonClick = 'saveButton';
                    saveButtonClicked = false;
                    $scope.userprofilereset = angular.copy($scope.userprofile);
                    $scope.form.userScopeDetailsform.$setPristine();
                    if ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.EDIT) {
                        if($scope.isLogedinUser($scope.userprofile.userDetails)){
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.USER.OWN_PROFILE_UPDATE, 'success');
                        }else{
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.USER.USER + " '<b><i class='wrap-word'>" + $scope.getFullName($scope.userprofile.userDetails.firstName,$scope.userprofile.userDetails.lastName)+ "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.USER.UPDATESUCCESS, 'success');
                        }
                    } else {
                        alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.USER.USER + " '<b><i class='wrap-word'>" + $scope.getFullName($scope.userprofile.userDetails.firstName,$scope.userprofile.userDetails.lastName)+ "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.USER.SAVESUCCESS, 'success');
                    }
                    $scope.roleChanged = false;
                    $scope.managerChanged = false;
                }, function (error) {
                	triggercount = 0;
                    $scope.formsubmitted = false;
                	if($rootScope.isOnline){
                    	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    }
                	$scope.userprofile.userScopeDetails = angular.copy($scope.userprofilereset.userScopeDetails);
                });
            };

            /**
             * This method will be called from saveStepData() if some thing is changed on ui in step3;
             * This method deals with saving of user step2 details.
             * @access to CREATE MODE and EDIT MODE when clicked on 'SAVE' Button
             */
            var saveUserForwardRecipientDetails = function (url) {
                utilityService.setForwardRecipientDetails(url, angular.copy($scope.userprofile)).then(function (data) {
                    $scope.formsubmitted = false;
                    $scope.userprofile = data;
                    saveButtonFlag++;
                    recentButtonClick = 'saveButton';
                    saveButtonClicked = false;
                    $scope.userprofilereset = angular.copy($scope.userprofile);
                    $scope.form.userForwardRecipientsform.$setPristine();
                    if ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.EDIT) {
                        if($scope.isLogedinUser($scope.userprofile.userDetails)){
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.USER.OWN_PROFILE_UPDATE, 'success');
                        }else{
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.USER.USER + " '<b><i class='wrap-word'>" + $scope.getFullName($scope.userprofile.userDetails.firstName,$scope.userprofile.userDetails.lastName)+ "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.USER.UPDATESUCCESS, 'success');
                        }
                    } else {
                        if($scope.userCreated){
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.USER.USER + " '<b><i class='wrap-word'>" + $scope.getFullName($scope.userprofile.userDetails.firstName,$scope.userprofile.userDetails.lastName)+ "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.USER.SAVESUCCESS, 'success');
                        }else{
                        	$scope.userCreated = true;
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.USER.USER + " '<b><i class='wrap-word'>" + $scope.getFullName($scope.userprofile.userDetails.firstName,$scope.userprofile.userDetails.lastName) + "</i></b>' " + MESSAGECONSTANTS.NOTIFICATIONS.USER.CREATESUCCESS, 'success');
                        }
                    }
                    changeFRSelectedAfterApi(angular.copy(data),'success');
                }, function (error) {
                    $scope.formsubmitted = false;
                    $scope.userprofile.userForwardRecipientDetails = angular.copy($scope.userprofilereset.userForwardRecipientDetails);
                    if ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.EDIT) {
                    	if (error.status === 500) {
                            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                        } else {
                            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                            changeFRSelectedAfterApi(angular.copy($scope.userprofilereset),'error');
                        }
                    } else {
                    	if (error.status === 500) {
                            alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                        } else {
                        	 dateTimeCount = 1;
                             alertsAndNotificationsService.showBannerMessage(error.message, 'danger');
                             changeFRSelectedAfterApi(angular.copy($scope.userprofilereset),'error');
                         }
                    }
                    $scope.form.userForwardRecipientsform.$setPristine();
                });
            };

            /**
             * This method will be called whenever user clicked on 'SAVE' buttton
             * This method checks whether any changes are made in UI with respective steps
             */
            $scope.saveStepData = function () {
                $scope.formsubmitted = true;
                nextButtonClicked = false;
                saveButtonClicked = true;
                valid = false;
                var currentStep = getCurrentStep();
                switch (currentStep) {
                    case 1:
                        //Cheking mobile and telephone validations also.
                        valid = $scope.form.userDetailsform.$valid && !$scope.isContentinObjSame($scope.userprofile.userDetails.address1,$scope.userprofile.userDetails.address2) && $scope.isValidMobileNumberEntered && $scope.isValidWorkTelephoneEntered && (!$scope.zipcodeCalling || ($scope.zipcodeCalling && $scope.zipcodeCallCompleted));
                        if (valid && (angular.equals($scope.userprofile.userDetails, $scope.userprofilereset.userDetails))) {
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.NOTHING_TO_SAVE, 'success');
                            $scope.formsubmitted = false;
                            saveButtonClicked = false;
                        } else if (valid) {
                            if ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.CREATE) {
                                var url = USERMANAGEMENTCONSTANTS.CONTROLLER.SAVE_USER + '/' + $rootScope.userDetails.company.companyId;
                            } else if ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.EDIT) {
                                var url = USERMANAGEMENTCONSTANTS.CONTROLLER.UPDATE_USER + '/' + $rootScope.userDetails.company.companyId;
                            }
                            saveUserDetails(url);
                        }else{
                        	$scope.form.userDetailsform.workTelephone.visited = true;
                        	$scope.form.userDetailsform.mobileNumber.visited = true;
                        }
                        break;
                    case 2:
                        valid = $scope.form.userDetailsform.$valid && $scope.form.userScopeDetailsform.$valid && $scope.isValidMobileNumberEntered && $scope.isValidWorkTelephoneEntered;
                        if (valid && (angular.equals($scope.userprofile.userScopeDetails, $scope.userprofilereset.userScopeDetails))) {
                        	$scope.formsubmitted = false;
                            saveButtonClicked = false;
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.NOTHING_TO_SAVE, 'success');
                        } else if (valid) {
                            saveUserScopeDetails();
                        }
                        break;
                    case 3:
                        valid = $scope.form.userDetailsform.$valid && $scope.form.userScopeDetailsform.$valid && $scope.form.userForwardRecipientsform.$valid && $scope.isValidMobileNumberEntered && $scope.isValidWorkTelephoneEntered;;
                        if (valid && !isStepChanged(currentStep)) {
                            alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.NOTIFICATIONS.NOTHING_TO_SAVE, 'success');
                            $scope.formsubmitted = false;
                            saveButtonClicked = false;
                        } else if (valid) {
                            var url = USERMANAGEMENTCONSTANTS.CONTROLLER.SAVE_FORWARD_RECIPIENTS + '/' + $rootScope.userDetails.company.companyId;
                            saveUserForwardRecipientDetails(url);
                        }
                        break;
                    default:
                        $scope.text = "Something went wrong";
                }
            }

            /**
             * This method is called, when user clicked on 'PREV' button
             * This will take care of changes to be made to 'userprofile' object when step changes
             */
            $scope.prevStepClicked = function () {
            	saveButtonClicked = false;
            	nextButtonClicked = false;
                var currentStep = getCurrentStep();
                switch (currentStep) {
                    case 1:
                        break;
                    case 2:
                        StorageService.set('wizardoperations', true);
                        $scope.laststep = false;
                        break;
                    case 3:
                        $scope.laststep = false;
                        StorageService.set('wizardoperations', true);
                        break;
                    default:
                        $scope.text = "Something went wrong";
                }
            };

            /**
             * This method is called when user clicks on
             * cancel button.
             * This method works according to access permissions
             * of user when he enters into this module.
             * @access for create, view and update
             */
            $scope.cancel = function () {
                showConfirmationPopup = true;
                if (showConfirmationPopup) {
                    bootbox.confirm({
                        closeButton: false,
                        title: "<div class='alert alert-warning' style='margin-bottom: 0px;'><i class='fa fa-warning fa-fw fa-lg'></i><strong>Warning!</strong></div>",
                        message: "Unsaved data on the page will be lost. Do you still want to continue?",
                        buttons: {
                            confirm: {
                                label: 'Yes',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'Cancel',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            if (result) {
                                $scope.clearData();
                            }
                             else {
                                showConfirmationPopup = false;
                                $rootScope.switchClicked = false;
                            }
                        }
                    });
                }
            };

            $scope.clearData = function () {
                    cancelClicked = false;
                    saveButtonClicked = false;
                    nextButtonClicked = false;
                    $scope.copyOfGeoLocations = angular.copy($scope.geoLocations);
                    $scope.copyOfGeoLocationsCopy = angular.copy($scope.geoLocationscopy);
                    var currentStep = getCurrentStep();
                    showConfirmationPopup = false;
                    $rootScope.switchClicked = false;
                    switch (currentStep) {
                        case 1:
                            if (isStepChanged(currentStep)) {
                                cancelClicked = true;
                                $scope.formsubmitted = false;
                                if (!angular.equals($scope.userprofile.userDetails.zipcode, $scope.userprofilereset.userDetails.zipcode)) {
                                    console.log('getAddressDetails with zipchanged');
                                    $scope.getAddressDetails($scope.userprofilereset.userDetails.zipcode, 'zipchanged');
                                } else {
                                    console.log('getAddressDetails with start');
                                    $scope.getAddressDetails($scope.userprofilereset.userDetails.zipcode, 'start');
                                }
                                $scope.userprofile = angular.copy($scope.userprofilereset);
                                if (!angular.equals($scope.userprofile.userDetails, {}) && angular.isDefined($scope.userprofile.userDetails.userId) && angular.isDefined($scope.userprofile.userDetails.role)) {
                                    getManagersList($scope.userprofile.userDetails.userId, $scope.userprofile.userDetails.role.id);
                                } else {
                                    $scope.countryCodeforTelephone = angular.copy($rootScope.defaultCountryCode);
                                    $scope.countryCodeforMobile = angular.copy($rootScope.defaultCountryCode);
                                    $scope.setWorkTelephoneCountryCode();
                                    $scope.setMobileNumberCountryCode();
                                }
                                if(angular.isDefined($scope.userprofilereset.userDetails.workTelephone) && ($scope.userprofilereset.userDetails.workTelephone !== null)){
                                    document.getElementById("workTelephone").value = $scope.userprofilereset.userDetails.workTelephone;
                                    $scope.safeApply();
                                    $scope.setWorkTelephoneCountryCode();
                                }
                                if(angular.isDefined($scope.userprofilereset.userDetails.mobileNumber) && ($scope.userprofilereset.userDetails.mobileNumber !== null)){
                                    document.getElementById("mobileNumber").value = $scope.userprofilereset.userDetails.mobileNumber;
                                    $scope.safeApply();
                                    $scope.setMobileNumberCountryCode();
                                }
                                $scope.form.userDetailsform.$setPristine();
                                console.log('$scope.isCountyExists : ' + $scope.isCountyExists);
                            } else {
                                $scope.formsubmitted = false;
                                if(angular.equals($scope.userprofile.userDetails, {})){
                                    $scope.countryCodeforTelephone = angular.copy($rootScope.defaultCountryCode);
                                    $scope.countryCodeforMobile = angular.copy($rootScope.defaultCountryCode);
                                    $scope.setWorkTelephoneCountryCode();
                                    $scope.setMobileNumberCountryCode();
                                    $scope.isValidWorkTelephoneEntered = false;
                                    $scope.isValidMobileNumberEntered = false;
                                }else{
                                    
                                }
                                $scope.form.userDetailsform.$setPristine();
                            }
                            angular.element('.companyaddresscopyenabled').attr({ 'readonly': false, 'disabled': false });
                            break;
                        case 2:
                            if (isStepChanged(currentStep)) {
                                cancelClicked = true;
                                $scope.formsubmitted = false;
                                $scope.userprofile = angular.copy($scope.userprofilereset);
                                $scope.form.userScopeDetailsform.$setPristine();
                            }
                            break;
                        case 3:
                            if (isStepChanged(currentStep)) {
                                cancelClicked = true;
                                $scope.userprofile = angular.copy($scope.userprofilereset);
                                $scope.forwardrecipients = angular.copy($scope.forwardRecipenetsCopy);
                                $scope.formsubmitted = false;
                                $('#fromDate').datepicker('update', $scope.userprofilereset.userForwardRecipientDetails.fromDate);
                                $('#toDate').datepicker('update', $scope.userprofilereset.userForwardRecipientDetails.toDate);
                                $scope.form.userForwardRecipientsform.$setPristine();
                                dateTimeCount = 1;
                            }
                            break;
                        default:
                            cancelClicked = false;
                            alert("Something went wrong...Please Logout and try again");
                    }
            }

            $scope.cancelSteps = function () {
                $scope.cancel();
            }

            /**
             * This method is called, when user clicked on 'Step Names' directly
             * This will take care of changes to be made to 'userprofile' object when step changes
             */
            $scope.getStepChanges = function (stepclicked) {
            	saveButtonClicked = false;
            	nextButtonClicked = false;
                var presentStep = getCurrentStep();
                switch (stepclicked) {
                    case 1:
                        $scope.laststep = false;
                        break;
                    case 2:
                        $scope.laststep = false;
                        break;
                    case 3:
                        if (presentStep === 3) {
                            $scope.laststep = true;
                        }
                        break;
                    default:
                        $scope.laststep = false;
                        console.log('something went wrong');
                }
            }

            /**
             * This method is called when changed in CITY OR STATE OR COUNTY OR COUNTRY dropdowns.
             * This method will take care of changes to be made to other attributes when one
             * is changed among above specified one's.
             */
            $scope.changeAddressObject = function (type, obj) {
            	saveButtonClicked = false;
            	nextButtonClicked = false;
                switch (type) {
                    case 'city':
                        for (var p = 0; p < $scope.geoLocationscopy.length; p++) {
                            if ($scope.geoLocationscopy[p].city === obj) {
                                $scope.userprofile.userDetails.state = $scope.geoLocationscopy[p].state;
                                $scope.userprofile.userDetails.country = $scope.geoLocationscopy[p].country;
                                $scope.userprofile.userDetails.county = $scope.geoLocationscopy[p].county;
                                if(angular.isDefined($scope.geoLocationscopy[p].county) && ($scope.geoLocationscopy[p].county!==null)){
                                	$scope.isCountyExists = true;
                                }else{
                                	$scope.isCountyExists = false;
                                }
                                $scope.countryCode = $scope.geoLocationscopy[p].countryCode;
                                $scope.setWorkTelephoneCountryCode();
                                $scope.setMobileNumberCountryCode();
                            }
                        }
                        break;
                    case 'state':
                        for (var p = 0; p < $scope.geoLocationscopy.length; p++) {
                            if ($scope.geoLocationscopy[p].state === obj) {
                                $scope.userprofile.userDetails.city = $scope.geoLocationscopy[p].city;
                                $scope.userprofile.userDetails.country = $scope.geoLocationscopy[p].country;
                                $scope.userprofile.userDetails.county = $scope.geoLocationscopy[p].county;
                                if(angular.isDefined($scope.geoLocationscopy[p].county) && ($scope.geoLocationscopy[p].county!==null)){
                                	$scope.isCountyExists = true;
                                }else{
                                	$scope.isCountyExists = false;
                                }
                                $scope.countryCode = $scope.geoLocationscopy[p].countryCode;
                                $scope.setWorkTelephoneCountryCode();
                                $scope.setMobileNumberCountryCode();
                            }
                        }
                        break;
                    case 'country':
                        for (var p = 0; p < $scope.geoLocationscopy.length; p++) {
                            if ($scope.geoLocationscopy[p].country === obj) {
                                $scope.userprofile.userDetails.city = $scope.geoLocationscopy[p].city;
                                $scope.userprofile.userDetails.state = $scope.geoLocationscopy[p].state;
                                $scope.userprofile.userDetails.county = $scope.geoLocationscopy[p].county;
                                if(angular.isDefined($scope.geoLocationscopy[p].county) && ($scope.geoLocationscopy[p].county!==null)){
                                	$scope.isCountyExists = true;
                                }else{
                                	$scope.isCountyExists = false;
                                }
                                $scope.countryCode = $scope.geoLocationscopy[p].countryCode;
                                $scope.setWorkTelephoneCountryCode();
                                $scope.setMobileNumberCountryCode();
                            }
                        }
                        break;
                    case 'county':
                        for (var p = 0; p < $scope.geoLocationscopy.length; p++) {
                            if ($scope.geoLocationscopy[p].county === obj) {
                                $scope.userprofile.userDetails.city = $scope.geoLocationscopy[p].city;
                                $scope.userprofile.userDetails.country = $scope.geoLocationscopy[p].country;
                                $scope.userprofile.userDetails.state = $scope.geoLocationscopy[p].state;
                                $scope.countryCode = $scope.geoLocationscopy[p].countryCode;
                                $scope.setWorkTelephoneCountryCode();
                                $scope.setMobileNumberCountryCode();
                            }
                        }
                        break;
                }
            }

            /**
             * This method works for triggerng
             * button clicks and resetting timeout
             * and remaining variables to normal
             * @access for create, view and update
             */
            $scope.stepTrigger = function (target) {
                if (target === 'nextbtnclick') {
                    var nextbtnclick = $timeout(function () {
                        if ($scope.accessMode !== MESSAGECONSTANTS.ACCESS_MODES.VIEW){
                            angular.element('.create-next').triggerHandler('click');
                        }else{
                            angular.element('.view-next').triggerHandler('click');
                        }
                    }, 0).then(function () {
                        triggercount = 0;
                        $scope.formsubmitted = false;
                        StorageService.remove('wizardoperations');
                        if (($scope.userprofile.userDetails.role.name !== $rootScope.MESSAGECONSTANTS.ROLES.FOURDOTFIVEADMIN) && $scope.laststep) {
                            $scope.form.userForwardRecipientsform.$setPristine();
                        }
                        $timeout.cancel(nextbtnclick);
                    });
                } else if (target === 'cancelflow') {
                    var steponeclick = $timeout(function () {
                        angular.element('#1').trigger('click');
                    }, 500).then(function () {
                        triggercount = 0;
                        $scope.formsubmitted = false;
                        StorageService.remove('wizardoperations');
                        $timeout.cancel(steponeclick);
                    });
                } else if (target === 'viewnext') {
                    var viewnextbtnclick = $timeout(function () {
                        angular.element('.view-next').triggerHandler('click');
                    }, 0).then(function () {
                        StorageService.remove('wizardoperations');
                        triggercount = 0;
                        $timeout.cancel(viewnextbtnclick);
                    });
                }
            };

            /**
             * Forward Recipients fromDate and toDate validations are done in this method
             */
            $scope.checkDateValidation = function () {
                var enter = !angular.equals($scope.userprofile.userForwardRecipientDetails, null) &&
                    angular.isDefined($scope.userprofile.userForwardRecipientDetails.forwardRecipient) &&
                    !angular.equals($scope.userprofile.userForwardRecipientDetails.forwardRecipient, null) &&
                    angular.isDefined($scope.userprofile.userForwardRecipientDetails.fromDate) &&
                    !angular.equals($scope.userprofile.userForwardRecipientDetails.fromDate, null) &&
                    angular.isDefined($scope.userprofile.userForwardRecipientDetails.toDate) &&
                    !angular.equals($scope.userprofile.userForwardRecipientDetails.toDate, null) &&
                    !angular.equals(new Date($scope.userprofile.userForwardRecipientDetails.fromDate).toString(), 'Invalid Date') &&
                    !angular.equals(new Date($scope.userprofile.userForwardRecipientDetails.toDate).toString(), 'Invalid Date');
                if (enter) {
                    var keys = Object.keys($scope.userprofile.userForwardRecipientDetails.forwardRecipient);
                    var last = keys.slice(-2)[0];
                    //delete $scope.userprofile.userForwardRecipientDetails.forwardRecipient[last];
                    var datevalid = new Date($scope.userprofile.userForwardRecipientDetails.fromDate).getTime() <= new Date($scope.userprofile.userForwardRecipientDetails.toDate).getTime();
                    if (!datevalid) {
                        $scope.form.userForwardRecipientsform.$setValidity('date', false);
                        $scope.form.userForwardRecipientsform.fromDate.$setValidity('date', false);
                        $scope.form.userForwardRecipientsform.toDate.$setValidity('date', false);
                    } else {
                        $scope.form.userForwardRecipientsform.$setValidity('date', true);
                        $scope.form.userForwardRecipientsform.fromDate.$setValidity('date', true);
                        $scope.form.userForwardRecipientsform.toDate.$setValidity('date', true);
                        var timevalid = angular.equals(new Date($scope.userprofile.userForwardRecipientDetails.toDate + ' ' + $scope.userprofile.userForwardRecipientDetails.toTime).getTime(), NaN) ||
                            angular.equals(new Date($scope.userprofile.userForwardRecipientDetails.fromDate + ' ' + $scope.userprofile.userForwardRecipientDetails.fromTime).getTime(), NaN) ||
                            (new Date($scope.userprofile.userForwardRecipientDetails.toDate + ' ' + $scope.userprofile.userForwardRecipientDetails.toTime).getTime() > new Date($scope.userprofile.userForwardRecipientDetails.fromDate + ' ' + $scope.userprofile.userForwardRecipientDetails.fromTime).getTime());
                        if (!timevalid) {
                            if (!angular.equals(new Date($scope.userprofile.userForwardRecipientDetails.toDate + ' ' + $scope.userprofile.userForwardRecipientDetails.toTime).getTime(), NaN) &&
                                !angular.equals(new Date($scope.userprofile.userForwardRecipientDetails.fromDate + ' ' + $scope.userprofile.userForwardRecipientDetails.fromTime).getTime(), NaN)) {
                                $scope.form.userForwardRecipientsform.$setValidity('time', false);
                                $scope.form.userForwardRecipientsform.fromTime.$setValidity('time', false);
                                $scope.form.userForwardRecipientsform.toTime.$setValidity('time', false);
                            }
                        } else {
                            $scope.form.userForwardRecipientsform.$setValidity('time', true);
                            $scope.form.userForwardRecipientsform.fromTime.$setValidity('time', true);
                            $scope.form.userForwardRecipientsform.toTime.$setValidity('time', true);
                        }
                    }
                }else{
                	if(dateTimeCount <= 1){  
                		var date = new Date();
                		var time = date.toTimeString().split(' ')[0].split(':');
                		$scope.userprofile.userForwardRecipientDetails.fromTime = time[0]+':'+time[1]+':'+time[2];
                		$scope.userprofile.userForwardRecipientDetails.toTime = time[0]+':'+time[1]+':'+time[2];
                		dateTimeCount++;
                	}else{
                		dateTimeCount++;
	                	  var timevalid = angular.equals(new Date($scope.userprofile.userForwardRecipientDetails.toDate + ' ' + $scope.userprofile.userForwardRecipientDetails.toTime).getTime(), NaN) ||
	                	  					angular.equals(new Date($scope.userprofile.userForwardRecipientDetails.fromDate + ' ' + $scope.userprofile.userForwardRecipientDetails.fromTime).getTime(), NaN) ||
	                	  					(new Date($scope.userprofile.userForwardRecipientDetails.toDate + ' ' + $scope.userprofile.userForwardRecipientDetails.toTime).getTime() > new Date($scope.userprofile.userForwardRecipientDetails.fromDate + ' ' + $scope.userprofile.userForwardRecipientDetails.fromTime).getTime());
	                         $scope.form.userForwardRecipientsform.fromTime.$setValidity('time', true);
	                         $scope.form.userForwardRecipientsform.toTime.$setValidity('time', true);
	                  }
                }
            }

            /**
             * This method is called when present state start changing.
             * used for displaying confirmation popup before leaving this page. 
             */
            var unbindStateChangeEvent = $scope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
                event.preventDefault();
                var confirmationAtStep = getCurrentStep();

                if (angular.copy(showConfirmationPopup)) {
                    showConfirmationPopup = false;
                } else if ($scope.accessMode !== MESSAGECONSTANTS.ACCESS_MODES.VIEW) {
                    if ((confirmationAtStep === 1) && isStepChanged(confirmationAtStep)) {
                        showConfirmationPopup = true;
                    } else if ((confirmationAtStep === 2) && isStepChanged(confirmationAtStep)) {
                        showConfirmationPopup = true;
                    } else if ((confirmationAtStep === 3) && isStepChanged(confirmationAtStep)) {
                        showConfirmationPopup = true;
                    }
                }

                if (showConfirmationPopup) {
                    bootbox.confirm({
                        closeButton: false,
                        title: "<div class='alert alert-warning' style='margin-bottom: 0px;'><i class='fa fa-warning fa-fw fa-lg'></i><strong>Warning!</strong></div>",
                        message: "Unsaved data on the page will be lost. Do you still want to continue?",
                        buttons: {
                            confirm: {
                                label: 'Yes',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'Cancel',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            if (result) {
                                if ($rootScope.switchClicked) {
                                    //$('#myModal').modal('show');
                                    $scope.clearData();
                                    $scope.getCompaniesForLoggedinUser(function(response){
                                    	$('#myModal').modal('show');
                                    });
                                } else {
                                    unbindStateChangeEvent();
                                    if (toState.name === 'login') {
                                        $rootScope.$emit("CallLogoutMethod", { logoutnow: true });
                                    } else {
                                        $scope.form.userDetailsform.$setPristine();
                                        StorageService.remove('wizardoperations');
                                        $state.go(toState.name, toParams, { reload: true });
                                    }
                                }
                            } else {
                                showConfirmationPopup = false;
                                $rootScope.switchClicked = false;
                            }
                        }
                    });
                } else {
                    if ($rootScope.switchClicked) {
                        $scope.getCompaniesForLoggedinUser(function(response){
                        	$('#myModal').modal('show');
                        });
                    } else {
                        unbindStateChangeEvent();
                        $rootScope.switchClicked = false;
                        if (toState.name === 'login') {
                            $rootScope.$emit("CallLogoutMethod", { logoutnow: true });
                        } else {
                            StorageService.remove('wizardoperations');
                            $state.go(toState.name, toParams, { reload: true });
                        }
                    }
                }
            });

            $scope.disableCountyFieldIfViewMode = function(){
                if ($scope.accessMode === MESSAGECONSTANTS.ACCESS_MODES.VIEW) {
                    angular.element('#county').attr({ 'readonly': true, 'disabled': true });
                }
            }

            /**
             * Toggle company address to user checkbox.
             */
            $scope.toggleCopyCompanyAddress = function () {
                $scope.copyCompanyAddress = document.getElementById("copyCompanyAddress").checked;
                var companyAddressZipcode;
                if ($scope.copyCompanyAddress) {
                    genericService.getObjects(USERMANAGEMENTCONSTANTS.CONTROLLER.GET_COMPANY_DETAILS + '/' + $rootScope.userDetails.company.companyId).then(function (data) {
                        companyAddressZipcode = data.company.zipcode;
                        $scope.userprofile.userDetails.address1 = data.company.address1;
                        $scope.userprofile.userDetails.address2 = data.company.address2;
                        $scope.userprofile.userDetails.zipcode = companyAddressZipcode;
                        cancelClicked = false;
                        $scope.getAddressDetails(companyAddressZipcode, 'copycomanyaddress',function(response){
                        	angular.element('.companyaddresscopyenabled').attr({ 'readonly': true, 'disabled': true });
                        });
                    }, function (error) {
                    	if($rootScope.isOnline){
                        	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                        }
                    });
                } else {
                    $scope.copyCompanyAddress = false;
                    angular.element('.companyaddresscopyenabled').attr({ 'readonly': false, 'disabled': false });
                    if ($scope.accessMode == MESSAGECONSTANTS.ACCESS_MODES.EDIT) {
                    	$scope.userprofile.userDetails.address1 = $scope.userprofilereset.userDetails.address1;
                		$scope.userprofile.userDetails.address2 = $scope.userprofilereset.userDetails.address2;
                		$scope.userprofile.userDetails.zipcode = $scope.userprofilereset.userDetails.zipcode;
                        $scope.getAddressDetails($scope.userprofile.userDetails.zipcode, 'zipchanged',function(response){
                        	if(angular.isDefined($scope.userprofilereset.userDetails.county) && ($scope.userprofilereset.userDetails.county !== null)
                        			&& ($scope.userprofile.userDetails.zipcode === $scope.userprofilereset.userDetails.zipcode)){
                    			$scope.userprofile.userDetails.county = $scope.userprofilereset.userDetails.county;
                    			$scope.safeApply();
                            }
                        });
                    } else {
                	    $scope.userprofile.userDetails.address1 = $scope.userprofilereset.userDetails.address1;
                		$scope.userprofile.userDetails.address2 = $scope.userprofilereset.userDetails.address2;
                		$scope.userprofile.userDetails.zipcode = $scope.userprofilereset.userDetails.zipcode;
                        $scope.getAddressDetails($scope.userprofilereset.userDetails.zipcode, 'zipchanged',function(response){
                        	if(angular.isDefined($scope.userprofilereset.userDetails.county) && ($scope.userprofilereset.userDetails.county !== null)
                        			&& ($scope.userprofile.userDetails.zipcode === $scope.userprofilereset.userDetails.zipcode)){
                    			$scope.userprofile.userDetails.county = $scope.userprofilereset.userDetails.county;
                    			$scope.safeApply();
                            }
                        });
                    }
                }
            }

            $timeout(function () {
                $scope.form.userDetailsform.$setPristine();
                if (document.getElementById('workTelephone').value.length > 0) {
                    $scope.workTelephoneDirty = true;
                }
                if (document.getElementById('mobileNumber').value.length > 0) {
                    $scope.mobileNumberDirty = true;
                }
            }, 1500);



        }]);

usermanagementmodule.factory('userManagementService', ['$q',
    '$http',
    '$state',
    '$filter',
    'moment',
    '$rootScope',
    'StorageService',
    'USERMANAGEMENTCONSTANTS',
    function ($q, $http, $state, $filter, moment, $rootScope, StorageService, USERMANAGEMENTCONSTANTS) {

        var factory = {};

       
        return factory;
    }
]);
