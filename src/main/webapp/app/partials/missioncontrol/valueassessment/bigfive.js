var bigfivemodule = angular.module('4dot5.missioncontrolmodule.bigfivemodule',
    []);

bigfivemodule.constant('BIGFIVECONSTANTS', {
    CONFIG: {
        STATE: 'missioncontrol.bigfive',
        URL: '/bigfive/:jobMatchId',
        CONTROLLER: 'ValueAssessmentController',
        TEMPLATEURL: 'app/partials/missioncontrol/valueassessment/bigfive.html',
    },
      CONTROLLER: {

        }
    });

    bigfivemodule.config(
        ['$stateProvider',
            'BIGFIVECONSTANTS',
            function ($stateProvider, BIGFIVECONSTANTS) {
                $stateProvider.state(BIGFIVECONSTANTS.CONFIG.STATE, {
                    url: BIGFIVECONSTANTS.CONFIG.URL,
                    templateUrl: BIGFIVECONSTANTS.CONFIG.TEMPLATEURL,
                    controller: BIGFIVECONSTANTS.CONFIG.CONTROLLER,
                    data: {
                        requireLogin: true
                    },
                    params:{
                    	jobMatchId : null
                    }
                });
            }
        ]);

     bigfivemodule.directive('valueAssessment', function(){
         return {
           restrict: 'EA',
           scope: {
             list: '=',
             questionlist: '=',
             saveClick: '&',
             goPrevious: '&',
             goNext:'&',
             saveButton:'&',
             showButton:'&'
           },
           templateUrl: 'app/partials/missioncontrol/valueassessment/valueassessment.html'

         };
       });

    bigfivemodule.controller('ValueAssessmentController',
        ['$scope',
            '$state',
            '$window',
            '$compile',
            '$rootScope',
            '$stateParams',
            'StorageService',
            'genericService',
            'BIGFIVECONSTANTS',
            'MESSAGECONSTANTS',
            '$timeout',
            '$filter',
            'alertsAndNotificationsService',
            function ($scope, $state, $window, $compile, $rootScope, $stateParams, StorageService, genericService, BIGFIVECONSTANTS, MESSAGECONSTANTS, $timeout,$filter, alertsAndNotificationsService) {


            $scope.candidatebigFiveResponses = [];
            $scope.bigFiveQuestions = [];
            $scope.candidatebigFiveResponsesDto;
            console.log('In bigfive controller');


            var jobMatchId  = $stateParams.jobMatchId;
            genericService.getObjects("api/valueassessment/getcandidatebigfiveresponses/"+  jobMatchId).then(function (data) {
            $scope.candidatebigFiveResponsesDto = angular.copy(data);
            $scope.candidatebigFiveResponses = $scope.candidatebigFiveResponsesDto.candidateBigFiveResponses;
            $scope.bigFiveQuestions = $scope.candidatebigFiveResponsesDto.bigFiveQuestionsList;


            }, function (error) {
                if($rootScope.isOnline){
                        alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    }
            });

             $scope.nextQn =  function(number) {
                   var divId = 'quest' + number;
                   var nextDivId = 'quest' + (parseInt(number)+1);
                   $("#"+divId).slideUp(1000);
                   $("#"+nextDivId).slideDown(1000);
                   $('#theprogressbar').width(parseInt(number) + "%").attr('aria-valuenow', parseInt(number));


        }


           $scope.prevQtn =  function(number) {
                   var divId = 'quest' + number;
                   var nextDivId = 'quest' + (parseInt(number)-1);
                   $("#"+divId).slideUp(1000);
                   $("#"+nextDivId).slideDown(1000);
                   $('#theprogressbar').width(parseInt(number) + "%").attr('aria-valuenow', parseInt(number));

        }

           $scope.saveSelection = function(qno,val) {
                 var candidateResponse= $scope.candidatebigFiveResponses[parseInt(qno)-1];

                 candidateResponse.response = val;
                 candidateResponse.responseDateTime = new Date();
                 genericService.addObject("api/valueassessment/savecandidatebigfiveresponses",candidateResponse).then(function (data) {

                }, function (error) {
                    //Have to do something
                });

                if (qno != $scope.candidatebigFiveResponses.length) {
                  var number = qno;
                  var divId = 'quest' + number;
                  var nextDivId = 'quest' + (parseInt(number)+1);
                  $("#"+divId).slideUp(1000);
                  $("#"+nextDivId).slideDown(1000);
                }
                $('#theprogressbar').width(parseInt(qno) + "%").attr('aria-valuenow', parseInt(qno));
           }


               $scope.saveResponse = function(qno) {
                  var cntr = 0;
                   for (var i = 0; i < $scope.candidatebigFiveResponses.length; i++) {
                    if ($scope.candidatebigFiveResponses[i].response == null) {
                     cntr = cntr  +1;
                   }
                  }
                  if (cntr > 1) {
                        alertsAndNotificationsService.showBannerMessage("Please answer " + cntr + " questions",'danger');
                  } else
                  var candidateResponse= $scope.candidatebigFiveResponses[parseInt(qno)-1];
                    candidateResponse.responseDateTime = new Date();
                    genericService.addObject("api/valueassessment/savecandidatebigfiveresponses",candidateResponse).then(function (data) {
                            alertsAndNotificationsService.showBannerMessage("Big Five Questionnaire Completed", 'success');
                              $("#questionarea").hide();
                              $("#successmsg").show();

                    }, function (error) {
                       if($rootScope.isOnline){
                        alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    }
                    });
               }


               $scope.showPreviousButton = function(qno){
                   if (qno > 1)
                       return true;
                   else
                       return false;

           }


               $scope.showNextButton = function(qno) {
                        var sizeArray = $scope.candidatebigFiveResponses.length;
                           if(qno >= 1 && qno <= sizeArray-1 ) {
                            return true;
                        }
                        else {

                            return false;
                        }
                }

               $scope.showSaveButton = function(qno) {
                     var sizeArray = $scope.candidatebigFiveResponses.length;
                    if(qno == sizeArray) {
                        return true;
                    }
                    else {

                        return false;
                    }
            }

            $scope.displayButton = function(qno,val) {
                var questionNumber = parseInt(qno);
                if (val == 1) {
                    return $scope.showPreviousButton(questionNumber);
                } else if(val == 2) {
                    return $scope.showNextButton(questionNumber);
                } else {
                   return $scope.showSaveButton(questionNumber);
                }
            }

 }]);
