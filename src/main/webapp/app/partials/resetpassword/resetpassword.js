/**
 * All the Functionalities Related to Reset password
 * Module is included here
 */

var resetpasswordmodule = angular.module('4dot5.resetpasswordmodule',
    []);

resetpasswordmodule.constant('RESETPASSWORDCONSTANTS', {
    CONFIG: {
        STATE: 'resetpassword',
        URL: '/changepassword',
        CONTROLLER: 'ResetPasswordController',
        TEMPLATEURL: 'app/partials/resetpassword/resetpassword.html',
    },
    CONTROLLER: {
        CHANGE_PASSWORD: 'auth/changepassword',
        AUTHENTICATE_OTP: 'auth/verifyonetimepassword'
    }
});

resetpasswordmodule.config(
    ['$stateProvider',
        'RESETPASSWORDCONSTANTS',
        function ($stateProvider, RESETPASSWORDCONSTANTS) {
            $stateProvider.state(RESETPASSWORDCONSTANTS.CONFIG.STATE, {
                url: RESETPASSWORDCONSTANTS.CONFIG.URL,
                templateUrl: RESETPASSWORDCONSTANTS.CONFIG.TEMPLATEURL,
                controller: RESETPASSWORDCONSTANTS.CONFIG.CONTROLLER,
                data: {
                    requireLogin: false
                },
                params: {
                    message: null
                }
            });
        }
    ]);

resetpasswordmodule.directive('validPasswordC', function () {
    return {
        require: 'ngModel',
        scope: {
            reference: '=validPasswordC'
        },
        link: function (scope, elm, attrs, ctrl) {
            ctrl.$parsers.unshift(function (viewValue, $scope) {
                var noMatch = viewValue != scope.reference
                ctrl.$setValidity('noMatch', !noMatch);
                return (noMatch) ? noMatch : !noMatch;
            });
            scope.$watch("reference", function (value) {
                ctrl.$setValidity('noMatch', value === ctrl.$viewValue);
            });
        }
    }
});

resetpasswordmodule.controller('ResetPasswordController',
    ['$scope',
        '$state',
        '$window',
        '$location',
        '$stateParams',
        '$rootScope',
        'StorageService',
        'genericService',
        'RESETPASSWORDCONSTANTS',
        'MESSAGECONSTANTS',
        'ngNotify',
        '$timeout',
        'alertsAndNotificationsService',
        function ($scope, $state, $window, $location, $stateParams, $rootScope, StorageService, genericService, RESETPASSWORDCONSTANTS,MESSAGECONSTANTS, ngNotify, $timeout, alertsAndNotificationsService) {

            //console.log('ResetPasswordController');
            $scope.form = {};
            $scope.user = {};

            if ($stateParams.message !== null) {
                $scope.message = angular.copy($stateParams.message);
                $stateParams.message = null;
            } else {
                var resetpasswordbackup = StorageService.extract('resetpasswordbackup');
                if (resetpasswordbackup !== null) {
                    $scope.message = resetpasswordbackup.message;
                } else {
                    //Coming to reset password without any stateparams or for first time
                }
            };

            $scope.$watchGroup(['user.currentPassword','user.newPassword'],function(newValue, oldValue, scope){
                if($scope.user.currentPassword === $scope.user.newPassword){
                    $scope.form.resetpasswordform.$setValidity('unMatched', false);
                    $scope.form.resetpasswordform.newpassword.$setValidity('unMatched', false);
                }else {
                    $scope.form.resetpasswordform.$setValidity('unMatched', true);
                    $scope.form.resetpasswordform.newpassword.$setValidity('unMatched', true);
                }
            })

            $scope.resetPassword = function () {
                var userDetails = StorageService.getCookie('userDetails');
                $scope.user.email = userDetails.emailId;
                $scope.user.verifyPassword = $scope.user.newPassword;
                genericService.addObject(RESETPASSWORDCONSTANTS.CONTROLLER.CHANGE_PASSWORD, $scope.user).then(function (data) {
                    $rootScope.userActive = true;
                    userDetails.accountState = $rootScope.MESSAGECONSTANTS.USER_STATES.ACTIVE;
                    StorageService.setCookie('userDetails', angular.copy(userDetails), { 'expires': new Date(Date.now() + $rootScope.tokenvalidity) });
                    alertsAndNotificationsService.showBannerMessage(MESSAGECONSTANTS.RESETPASSWORD.RESET_PASSWORD_SUCCESSFULLY, 'success');
                    $state.go('missioncontrol.dashboard', null, { reload: false });
                }, function (error) {
                    if(error.statusCode === '401'){
                    	$scope.form.resetpasswordform.$setPristine();
                    	$scope.errormessage = error.message;
                    }else if($rootScope.isOnline){
                    	alertsAndNotificationsService.showBannerMessage(error.message,'danger');
                    }
                });
            }
        }
    ]);