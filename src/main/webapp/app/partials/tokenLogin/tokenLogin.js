/**
 * This module deals with Token Based Login Workflow
 */

var tokenLoginWorkflowModule = angular.module('4dot5.tokenLoginWorkflowModule', []);

tokenLoginWorkflowModule.constant('TOKENLOGINWORKFLOWCONSTANTS', {
    CONFIG: {
        STATE: 'tokenLogin',
        URL: '/authenticateToken?a&t',
        CONTROLLER: 'TokenLoginController',
        CONTROLLERAS: 'tokenLogin',
        TEMPLATEURL: 'app/partials/tokenLogin/tokenLogin.html'
    }
});

tokenLoginWorkflowModule.config(['$stateProvider', 'TOKENLOGINWORKFLOWCONSTANTS',
    function ($stateProvider, TOKENLOGINWORKFLOWCONSTANTS) {
        $stateProvider.state(TOKENLOGINWORKFLOWCONSTANTS.CONFIG.STATE, {
            url: TOKENLOGINWORKFLOWCONSTANTS.CONFIG.URL,
            templateUrl: TOKENLOGINWORKFLOWCONSTANTS.CONFIG.TEMPLATEURL,
            controller: TOKENLOGINWORKFLOWCONSTANTS.CONFIG.CONTROLLER,
            controllerAs: TOKENLOGINWORKFLOWCONSTANTS.CONFIG.CONTROLLERAS,
            data: {
                requireLogin: false
            }
        });
    }
]);

tokenLoginWorkflowModule.controller('TokenLoginController', TokenLoginController);

tokenLoginWorkflowModule.$inject = ['$rootScope', '$stateParams', '$state', '$scope','$uibModal', 'authService', 'StorageService', 'MESSAGECONSTANTS'];

function TokenLoginController($rootScope, $stateParams, $state, $scope, $uibModal, authService, StorageService, MESSAGECONSTANTS) {
    var vm = this;
    // params
    vm.token = $stateParams.t;
    vm.action = $stateParams.a;
    vm.userId = '';
    vm.tokenData = {};
    // flags
    vm.authenticatedFlag = false;
    // functions
    vm.successCallback = successCallback;
    vm.errorCallback = errorCallback;
    vm.actionSaveCallback = actionSaveCallback;

    function _getTokenPayload(successCallback, errorCallback){
        authService.getTokenPayload(vm.token, function (data) {
            successCallback(data);
        }, function (error) {
            errorCallback(error);
        });
    }

    /**
     * This method is invoked from login success call.
     * It manages cookis storage and rememberme status
     */
    var manageCookiesandRememberMe = function (data) {
        $scope.rememberme = true;
        if ($scope.rememberme) {
            StorageService.setCookie('rememberme', true, { 'expires': new Date(Date.now() + $rootScope.refreshtokenvalidity) });
        } else {
            StorageService.setCookie('rememberme', false, { 'expires': new Date(Date.now() + $rootScope.refreshtokenvalidity) });
        }
        StorageService.setCookie('refreshtoken', data.refreshToken, { 'expires': new Date(Date.now() + $rootScope.refreshtokenvalidity) });
    };

    var createUserDetailsObject = function(data){
        var obj = {};
        obj.company = {};
        obj.id = data.id;
        obj.firstname = data.firstname;
        obj.lastname = data.lastname;
        obj.emailId = data.emailId;
        obj.companyId = data.companyId;
        obj.accesstoken = data.token;
        obj.company.companyId = data.company.companyId;
        obj.company.companyName = data.company.companyName;
        obj.company.companyType = data.company.companyType;
        obj.company.companyState = 'Active';
        obj.accountState = data.accountState;
        obj.shouldPopupBeDisplayed = data.shouldPopupBeDisplayed;
        //obj.roleScreenRef = createRoleScreenRefObj(data.roleScreenRef);
        if(data.roleScreenRef.role.name !=='4dot5SuperUser' && data.roleScreenRef.role.name !=='4dot5Admin'){
            obj.company.companySelected = true;
        }else {
            obj.company.companySelected = false;
        }
        return obj;
    };

    function _logoutAndRedirectUser(){
        var rememberMeStatus = angular.copy(StorageService.getCookie('rememberme'));
        var userDetails = angular.copy(StorageService.getCookie('userDetails'));
        StorageService.resetBrowserStorage();
        if (angular.isDefined(rememberMeStatus) && (rememberMeStatus !== null) && (rememberMeStatus === true)) {
            if (angular.isDefined(userDetails) && (userDetails !== null)) {
                StorageService.setCookie('logoutemail', userDetails.emailId);
            }
        }
        $state.go('login', null, { reload: true });
    }

    function actionSaveCallback(){
        // var label = '';
        // switch(vm.action){
        //
        // }
        // $uibModal.open({
        //     animation: false,
        //     ariaLabelledBy: 'modal-title',
        //     ariaDescribedBy: 'modal-body',
        //     templateUrl: '/app/',
        //     backdrop: 'static',
        //     controller: ['$uibModalInstance','jobMatchDetailsArray','statusType','saveCallback',function ($uibModalInstance, jobMatchDetailsArray, statusType, saveCallback) {
        //         var vm = this;
        //         vm.jobMatchDetailsArray = jobMatchDetailsArray;
        //         vm.statusType = statusType;
        //         vm.saveCallback = function(){
        //             $uibModalInstance.dismiss('cancel');
        //             saveCallback();
        //         };
        //         vm.closeModal = function () {
        //             $uibModalInstance.dismiss('cancel');
        //         };
        //     }],
        //     controllerAs: 'setStatusModal',
        //     size: 'lg',
        //     resolve:{
        //         jobMatchDetailsArray: function () {
        //             var tempArray = [];
        //             var tempObject = {};
        //             tempObject.jobMatchId = vm.jobMatchId;
        //             tempObject.candidateId = vm.candidateId;
        //             tempArray.push(tempObject);
        //             return tempArray;
        //         },
        //         statusType: function () {
        //             return statusType;
        //         },
        //         saveCallback: function () {
        //             return vm.saveCallback;
        //         }
        //     }
        // });
        //alert(vm.action + ' completed successfully.');
        _logoutAndRedirectUser();
    }

    function successCallback(tokenResponse){
        StorageService.removeCookie('logoutemail');
        manageCookiesandRememberMe(tokenResponse);
        var userObj = createUserDetailsObject(tokenResponse);
        // create the user cookie
        StorageService.setCookie('userDetails', userObj, { 'expires': new Date(Date.now() + $rootScope.tokenvalidity) });
        $rootScope.userDetails = userObj;
        //manageUserState(tokenResponse);
        vm.userId = tokenResponse.id;
        _getTokenPayload(function (tokenData) {
            vm.authenticatedFlag = true;
            vm.tokenData = tokenData;
        }, function (error) {
            // do nothing
        });

    }

    function errorCallback(tokenResponse) {
        alert('error callback');
    }
}