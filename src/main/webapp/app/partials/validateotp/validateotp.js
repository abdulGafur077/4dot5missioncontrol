/**
 * All the Functionalities Related to Forget password
 * Module is Included Here
 */

var validateotpmodule = angular.module('4dot5.validateotpmodule',
    []);

validateotpmodule.constant('VALIDATEOTPCONSTANTS', {
    CONFIG: {
        STATE: 'validateotp',
        URL: '/validateotp',
        CONTROLLER: 'ValidateOtpController',
        TEMPLATEURL: 'app/partials/forgetpassword/validateotp.html',
    },
    SERVICE: {
    	VALIDATE_OTP: 'auth/login'
    }
});

validateotpmodule.config(
    ['$stateProvider',
        'VALIDATEOTPCONSTANTS',
        function ($stateProvider, VALIDATEOTPCONSTANTS) {
            $stateProvider.state(VALIDATEOTPCONSTANTS.CONFIG.STATE, {
                url: VALIDATEOTPCONSTANTS.CONFIG.URL,
                templateUrl: VALIDATEOTPCONSTANTS.CONFIG.TEMPLATEURL,
                controller: VALIDATEOTPCONSTANTS.CONFIG.CONTROLLER,
                data: {
                    requireLogin: false
                },
                params: {
                    message: null
                }
            });
        }
    ]);

validateotpmodule.controller('ValidateOtpController',
    ['$scope',
        '$state',
        '$stateParams',
        '$rootScope',
        'StorageService',
        'validateOtpService',
        'genericService',
        'VALIDATEOTPCONSTANTS',
        function ($scope, $state, $stateParams, $rootScope, StorageService, validateOtpService, genericService, VALIDATEOTPCONSTANTS) {
            console.log('ValidateOtpController');
            $scope.form = {};
            $scope.user = {};
            if ($stateParams.message !== null) {
                $scope.message = angular.copy($stateParams.message);
            }


            $scope.validate = function (form) {
                var otp = $scope.user.otp;
                console.log('Requested for Password' + angular.toJson($scope.user));
                validateOtpService.login($scope.user).then(function(data){
    	 			console.log('data : '+angular.toJson(data));
    	 			//$state.go('missioncontrol.dashboard',null,{reload : false}); 
    	 		},function(error){
    	 			console.log('controller error : '+angular.toJson(error));
                    $scope.exception = error.message;
    	 		});
            }
        }
    ]);
validateotpmodule.factory('validateOtpService', ['$q',
    '$http',
    'StorageService',
    'VALIDATEOTPCONSTANTS',
    function ($q, $http, StorageService, VALIDATEOTPCONSTANTS) {

        var factory = {};
        /**
         * This will allow to login by calling backend API
         */
        factory.login = function (user) {
            var deferred = $q.defer();
            $http.post(VALIDATEOTPCONSTANTS.SERVICE.VALIDATE_OTP,user).success(function (data, status, headers, config) {
                console.log('service success : '+angular.toJson(data));
                if(angular.isDefined(data.statusCode)){
                	if(angular.equals(data.statusCode, 200)){
                        deferred.resolve(data);
                    } else{
                        deferred.reject(data);
                    }
                }else {
                	deferred.resolve(data);
                }
            }).error(function (data, status, headers, config) {
                console.log('service error : '+angular.toJson(data));
                deferred.reject(data);
            });
            return deferred.promise;
        };
        return factory;
    }
]);