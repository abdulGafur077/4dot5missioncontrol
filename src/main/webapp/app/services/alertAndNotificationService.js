servicesmodule = angular.module('4dot5.missioncontrol.servicesmodule');

/**
 * The service to provide app wide alerts and notifications.
 * Contains all the methods needed for alerts and notifications.
 */

servicesmodule.factory('alertsAndNotificationsService',
    [
        'ngNotify',
        'StorageService',
        'genericService',
        function (ngNotify, StorageService, genericService) {

            var urlConstants = {
                GET_NOTIFICATIONS: 'api/notification/findall',
                SET_NOTIFICATION_STATE: 'api/notification/changenotificationstate'
            };

            var factory = {
                showBannerMessage: showBannerMessage,
                getNotifications: getNotifications,
                setNotificationsState: setNotificationsState
            };

            return factory;

            function showBannerMessage(messageData, messageType){
                switch (messageType) {
                    case 'success':
                        ngNotify.set('&nbsp;' + messageData, {
                            type: 'success',
                            html: true,
                            button: true
                        });
                        break;
                    case 'info':
                        ngNotify.set('&nbsp;' + messageData, {
                            type: 'info',
                            html: true,
                            button: true
                        });
                        break;
                    case 'warning':
                        ngNotify.set('&nbsp;' + messageData, {
                            type: 'warn',
                            html: true,
                            button: true
                        });
                        break;
                    case 'danger':
                        ngNotify.set('&nbsp;' + messageData, {
                            type: 'error',
                            html: true,
                            sticky: true
                        });
                        break;
                    default:
                        ngNotify.set("Something went wrong ...... Please contact admin");
                }
            }

            function getNotifications(filterObject, successCallback, errorCallback){
                var promise = genericService.addObject(StorageService.get('baseurl') + urlConstants.GET_NOTIFICATIONS, filterObject);

                promise.then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });

                return promise;
            }

            function setNotificationsState(stateObject, successCallback, errorCallback){
                genericService.addObject(StorageService.get('baseurl') + urlConstants.SET_NOTIFICATION_STATE, stateObject).then(function(data){
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }
        }
    ]
);

