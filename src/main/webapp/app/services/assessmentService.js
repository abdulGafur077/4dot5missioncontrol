servicesmodule = angular.module('4dot5.missioncontrol.servicesmodule');

/**
 * The service for assessments.
 * Contains all the methods needed for assessments.
 */

servicesmodule.factory('assessmentService',
    [
        'StorageService',
        'genericService',
        function (StorageService, genericService) {
            var urlConstants = {
                GET_ALL_TECH_ASSESSMENTS: 'api/assessment/getalltechassessments',
                GET_ALL_VALUE_ASSESSMENTS: 'api/assessment/getallvalueassessments',
                GET_ALL_UNASSIGNED_ASSESSSMENTS: 'api/assessment/getallunassignedassessments',
                SYNC_ASSESSMENTS: 'api/assessment/syncassessments',
                ASSIGN_ASSESSMENTS: 'api/assessment/assignassessments',
                REMOVE_ASSIGNMENT: 'api/assessment/removeassignedassessment',
                GET_ASSESSMENT_DETAILS: 'api/candidate/getcandidateassessmentdetail',
                GET_SENT_ASSESSMENT_INFO: 'api/candidate/sentassessmentdetails'
            };

            var factory = {
                getAllTechAssessments: getAllTechAssessments,
                getAllValueAssessments: getAllValueAssessments,
                syncAssessments: syncAssessments,
                assignAssessments: assignAssessments,
                getAllUnassignedAssessments: getAllUnassignedAssessments,
                removeAssignment: removeAssignment,
                getAssessmentDetails: getAssessmentDetails,
                getSentAssessmentInfo: getSentAssessmentInfo
            };

            return factory;

            function getAllTechAssessments(companyId, successCallback, errorCallback) {
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_ALL_TECH_ASSESSMENTS + '/' + companyId).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getAllValueAssessments(companyId, successCallback, errorCallback) {
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_ALL_VALUE_ASSESSMENTS + '/' + companyId).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function syncAssessments(companyId, successCallback, errorCallback){
                var url = StorageService.get('baseurl') + urlConstants.SYNC_ASSESSMENTS;
                if(!_.isUndefined(companyId) && !_.isNull(companyId)){
                    url += ( '?companyId=' + companyId );
                }
                genericService.putObject(url, null).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function assignAssessments(assignArray, successCallback, errorCallback){
                genericService.putObject(StorageService.get('baseurl') + urlConstants.ASSIGN_ASSESSMENTS, assignArray).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getAllUnassignedAssessments(companyId, successCallback, errorCallback) {
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_ALL_UNASSIGNED_ASSESSSMENTS + '/' + companyId).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function removeAssignment(companyId, assessmentId, successCallback, errorCallback) {
                genericService.deleteObject(StorageService.get('baseurl') + urlConstants.REMOVE_ASSIGNMENT + '/' + companyId + '/' + assessmentId, null).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getAssessmentDetails(assessmentDetailsObject, successCallback, errorCallback){
                genericService.addObject(StorageService.get('baseurl') + urlConstants.GET_ASSESSMENT_DETAILS, assessmentDetailsObject).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getSentAssessmentInfo(jobMatchId, assessmentType, successCallback, errorCallback){
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_SENT_ASSESSMENT_INFO + '/' + jobMatchId + '?assessmentType=' + assessmentType).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }
        }
    ]
);

