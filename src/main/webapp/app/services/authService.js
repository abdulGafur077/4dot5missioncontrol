servicesmodule = angular.module('4dot5.missioncontrol.servicesmodule');

/**
 * The service for assessments.
 * Contains all the methods needed for authentication.
 */

servicesmodule.factory('authService',
    [
        'StorageService',
        'genericService',
        function (StorageService, genericService) {
            var urlConstants = {
                AUTHENTICATE_TOKEN: 'auth/tokenlogin',
                GET_TOKEN_PAYLOAD: 'api/user/gettokenpayload'
            };

            var factory = {
                authenticateToken: authenticateToken,
                getTokenPayload: getTokenPayload
            };

            return factory;

            function authenticateToken(tokenObject, successCallback, errorCallback){
                genericService.addObject(StorageService.get('baseurl') + urlConstants.AUTHENTICATE_TOKEN, tokenObject).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getTokenPayload(token, successCallback, errorCallback){
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_TOKEN_PAYLOAD + '/' + token).then(function (data) {
                    if(successCallback){
                        successCallback(data);
                    }
                }, function (error) {
                    if(errorCallback){
                        errorCallback(error);
                    }
                });
            }
        }
    ]
);

