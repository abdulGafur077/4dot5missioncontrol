servicesmodule = angular.module('4dot5.missioncontrol.servicesmodule');

/**
 * The service for browser related methods.
 * Contains all the methods needed for browser detection.
 */

servicesmodule.factory('browserService',
    [
        function (StorageService, genericService) {

            var factory = {
                isGoogleChrome: isGoogleChrome
            };

            return factory;

            function isGoogleChrome(){
                // IE11 now returns undefined again for window.chrome
                // and new Opera 30 outputs true for window.chrome
                // but needs to check if window.opr is not undefined
                // and new IE Edge outputs to true now for window.chrome
                // and if not iOS Chrome check
                // so use the below updated condition
                var isChromium = window.chrome;
                var winNav = window.navigator;
                var vendorName = winNav.vendor;
                var isOpera = typeof window.opr !== "undefined";
                var isIEedge = winNav.userAgent.indexOf("Edge") > -1;
                var isIOSChrome = winNav.userAgent.match("CriOS");

                if (isIOSChrome) {
                    return true;
                } else if(isChromium !== null && typeof isChromium !== "undefined" &&
                    vendorName === "Google Inc." && isOpera === false && isIEedge === false) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    ]
);

