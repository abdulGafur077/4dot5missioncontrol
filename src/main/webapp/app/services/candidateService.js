servicesmodule = angular.module('4dot5.missioncontrol.servicesmodule');

/**
 * The service for candidates.
 * Contains all the methods needed for candidate management.
 */

servicesmodule.factory('candidateService',
    [
        'StorageService',
        'genericService',
        function (StorageService, genericService) {
            var urlConstants = {
                QUERY_CANDIDATE_ID_NAMES: 'api/candidate/findidnames',
                GET_CANDIDATE_CARD: 'api/candidate/candidatecard',
                GET_ACTIVE_CANDIDATE_JOB_MATCHES: 'api/job/jobmatchbycandidate',
                GET_ALL_CLIENT_OR_BU: 'api/requisition/getclientorbulist',
                GET_CANDIDATE_PROFILE: 'api/candidate/candidateprofile',
                GET_CANDIDATE_COUNTS: 'api/candidate/statuscount',
                GET_CANDIDATE_REPORT: 'api/candidate/candidatepdfreport',
                GET_CANDIDATE_ACTIVITIES: 'api/candidate/candidateActivity',
                GET_CANDIDATE_CLIENT_OR_BU: 'api/candidate/getcandidateclientorbus',
                GET_CANDIDATE_JOBS: 'api/candidate/getcandidatejobandrole',
                ADD_CANDIDATES: 'api/candidate/addcandidates',
                SET_CANDIDATE_AVAILABILITY: 'api/candidate/setcandidateavailability',
                DELETE_CANDIDATE: 'api/candidate/delete',
                ADD_UPDATE_CANDIDATE_EMAIL: 'api/candidate/addorupdatecontactemail',
                ADD_UPDATE_CANDIDATE_PHONE: 'api/candidate/addorupdatecontactphone',
                ADD_UPDATE_CANDIDATE_ADDRESS: 'api/candidate/addorupdatecontactaddress'
            };

            var factory = {
                queryCandidateIdNames: queryCandidateIdNames,
                getCandidateCard: getCandidateCard,
                getCandidateJobMatches: getCandidateJobMatches,
                getCandidateActivities: getCandidateActivities,
                getAllClientsOrBu: getAllClientsOrBu,
                getCandidateClientsOrBu: getCandidateClientsOrBu,
                getCandidateJobs: getCandidateJobs,
                getCandidateProfile: getCandidateProfile,
                getCandidateCounts: getCandidateCounts,
                getCandidateReport: getCandidateReport,
                addCandidates: addCandidates,
                setCandidateAvailability: setCandidateAvailability,
                deleteCandidate: deleteCandidate,
                addUpdateCandidateEmail: addUpdateCandidateEmail,
                addUpdateCandidatePhone: addUpdateCandidatePhone,
                addUpdateCandidateAddress: addUpdateCandidateAddress
            };

            return factory;

            function queryCandidateIdNames(filterObject, successCallback, errorCallback) {
                var promise = genericService.addObject(StorageService.get('baseurl') + urlConstants.QUERY_CANDIDATE_ID_NAMES, filterObject);
                promise.then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
                return promise;
            }

            function getCandidateCard(candidateId, userId, filterParams, successCallback, errorCallback) {
                var promise = genericService.addObject(StorageService.get('baseurl') + urlConstants.GET_CANDIDATE_CARD + '/' + candidateId + '/' + userId, filterParams);
                promise.then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
                return promise;
            }

            function getCandidateJobMatches(candidateId, top5Flag, page, size, searchTerm, successCallback, errorCallback) {
                var queryString = "";
                if (top5Flag) {
                    queryString = "top5Flag=true";
                } else {
                    queryString = "top5Flag=false&size=" + size + "&page=" + page + "&searchTerm=" + searchTerm;
                }
                genericService.addObject(StorageService.get('baseurl') + urlConstants.GET_ACTIVE_CANDIDATE_JOB_MATCHES + '/' + candidateId + '?' + queryString).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getCandidateActivities(candidateFilterParams, successCallback, errorCallback) {
                genericService.addObject(StorageService.get('baseurl') + urlConstants.GET_CANDIDATE_ACTIVITIES, candidateFilterParams).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
                successCallback(data);
            }

            function getCandidateClientsOrBu(candidateId, successCallback, errorCallback){
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_CANDIDATE_CLIENT_OR_BU + '/' + candidateId).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getCandidateJobs(candidateId, successCallback, errorCallback){
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_CANDIDATE_JOBS + '/' + candidateId).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getAllClientsOrBu(userDetails, successCallback, errorCallback){
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_ALL_CLIENT_OR_BU + '/' + userDetails.id + '/' + userDetails.company.companyId).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getCandidateProfile(candidateId, successCallback, errorCallback){
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_CANDIDATE_PROFILE + '/' + candidateId).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getCandidateCounts(userId, companyId, visibility, successCallback, errorCallback){
                var url = StorageService.get('baseurl') + urlConstants.GET_CANDIDATE_COUNTS + '/' + userId + '/' + companyId;
                if(!_.isUndefined(visibility) && visibility.toString().trim() != ''){
                    url += '?visibility=' + visibility;
                }
                genericService.getObjects(url).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getCandidateReport(filepath, successCallback, errorCallback) {
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_CANDIDATE_REPORT + '?filepath=' + filepath, {responseType:'arraybuffer'}).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function addCandidates(candidatesArray, successCallback, errorCallback){
                genericService.addObject(StorageService.get('baseurl') + urlConstants.ADD_CANDIDATES, candidatesArray).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function setCandidateAvailability(candidateAvailabilityObject, successCallback, errorCallback){
                genericService.addObject(StorageService.get('baseurl') + urlConstants.SET_CANDIDATE_AVAILABILITY, candidateAvailabilityObject).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function deleteCandidate(deleteCandidateObject, successCallback, errorCallback){
                genericService.deleteObject(StorageService.get('baseurl') + urlConstants.DELETE_CANDIDATE, deleteCandidateObject).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function addUpdateCandidatePhone(contactObject, successCallback, errorCallback){
                genericService.addObject(StorageService.get('baseurl') + urlConstants.ADD_UPDATE_CANDIDATE_PHONE, contactObject).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function addUpdateCandidateEmail(contactObject, successCallback, errorCallback){
                genericService.addObject(StorageService.get('baseurl') + urlConstants.ADD_UPDATE_CANDIDATE_EMAIL, contactObject).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }
            
            function addUpdateCandidateAddress(contactObject, successCallback, errorCallback){
                genericService.addObject(StorageService.get('baseurl') + urlConstants.ADD_UPDATE_CANDIDATE_ADDRESS, contactObject).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }
        }
    ]
);

