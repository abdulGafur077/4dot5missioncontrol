servicesmodule = angular.module('4dot5.missioncontrol.servicesmodule');

/**
 * The service for companies.
 * Contains all the methods needed for company management.
 */

servicesmodule.factory('companyService',
    [
        'StorageService',
        'genericService',
        function (StorageService, genericService) {
            var urlConstants = {
                DELETE_COMPANY: 'api/company/deletecompanyorclientorbu',
                GET_DELETE_COMPANY_INFO: 'api/company/companyorclientorbudeleteinfos'
            };

            var factory = {
                deleteCompany: deleteCompany,
                getDeleteCompanyInfo: getDeleteCompanyInfo,
                getParsedCompanyLocationText: getParsedCompanyLocationText
            };

            return factory;

            function getDeleteCompanyInfo(companyId, successCallback, errorCallback) {
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_DELETE_COMPANY_INFO + '/'+ companyId).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function deleteCompany(deleteCompanyObject, successCallback, errorCallback){
                genericService.deleteObject(StorageService.get('baseurl') + urlConstants.DELETE_COMPANY, deleteCompanyObject).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getParsedCompanyLocationText(location, isHtml) {
                    var locationText = "";
                    if (isHtml) {
                        if (!(_.isNull(location.city) && _.isNull(location.city.name))) {
                            locationText = "<span><b> @ </b>" + location.city.name + "</span>";
                        }
                        if (!_.isNull(location.state)) {
                            locationText = locationText + "<span>, " + location.state + "</span>";
                        }
                    } else {
                        if (!(_.isNull(location.city) && _.isNull(location.city.name))) {
                            locationText = " @ " + location.city.name;
                        }
                        if (!_.isNull(location.state)) {
                            locationText = locationText + ", " + location.state;
                        }
                    }
                    return locationText;
            }
        }
    ]
);

