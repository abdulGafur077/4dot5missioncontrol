servicesmodule = angular.module('4dot5.missioncontrol.servicesmodule');

/**
 * The service for date time utilities.
 *  Contains all the methods needed for date time manipulations and time zone handling.
 */

servicesmodule.factory('dateTimeUtilityService',
    [
        'moment',
        function (moment) {

            var factory = {
                getDateTimeInAPIFormat: getDateTimeInAPIFormat,
                getCurrentTimeStampInAPIFormat: getCurrentTimeStampInAPIFormat,
                getTimeStampInDisplayFormat: getTimeStampInDisplayFormat,
                getDateInDisplayFormat: getDateInDisplayFormat,
                getTimeInDisplayFormat: getTimeInDisplayFormat,
                guessBrowserTimeZoneName: guessBrowserTimeZoneName
            };

            function getDateTimeInAPIFormat(javascriptDateObject){
                return moment(javascriptDateObject).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
            }

            function getCurrentTimeStampInAPIFormat() {
                return getDateTimeInAPIFormat(new Date());
            }

            function getTimeStampInDisplayFormat(javascriptDateObject){
                return moment(javascriptDateObject).format("MMM DD, YYYY hh:mm A");
            }

            function getDateInDisplayFormat(javascriptDateObject){
                return moment(javascriptDateObject).format("MMM DD, YYYY");
            }

            function getTimeInDisplayFormat(javascriptDateObject){
                return moment(javascriptDateObject).format("hh:mm A");
            }

            function guessBrowserTimeZoneName() {
                return moment.tz.guess();
            }

            return factory;

        }
    ]
);

