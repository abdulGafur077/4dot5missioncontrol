servicesmodule = angular.module('4dot5.missioncontrol.servicesmodule');

servicesmodule.factory('feedbackExecutionService',
    [
        '$q',
        '$http',
        'moment',
        '$filter',
        '$rootScope',
        'StorageService',
        'genericService',
        function ($q, $http, moment, $filter, $rootScope, StorageService, genericService) {
            var urlConstants = {
                SHOW_RECRUITER_QUESTIONS: 'api/feedbackanswers/getrecruiterscreeningquestions',
                SAVE_RECRUITER_QUESTIONS: 'api/feedbackanswers/saverecruiterscreeningquestions',
                SAVE_JOB_MATCH_STATE: 'api/feedbackanswers/savejobmatchstate',
                GET_USER_LIST:'api/feedbackanswers/getuserlist',
                GET_CANDIDATE_DETAILS:'api/feedbackanswers/getcandidatedetails'
            };

            var factory = {
                getRecruiterQuestions: getRecruiterQuestions,
                saveRecruiterQuestions: saveRecruiterQuestions,
                saveJobMatchState: saveJobMatchState,
                getUserList: getUserList,
                getCandidateDetails: getCandidateDetails
            };

            return factory;


            function getCandidateDetails (jobMatchId, successCallback,errorCallback) {

                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_CANDIDATE_DETAILS + '/' + jobMatchId ).then(function (data) {
                    successCallback(data);
                 }, function (error) {
                      errorCallback(error);
                });
            }

            function getUserList (jobMatchId, userId, successCallback,errorCallback) {

                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_USER_LIST + '/' + jobMatchId + '/' + userId).then(function (data) {
                    successCallback(data);
                 }, function (error) {
                      errorCallback(error);
                });
            }

            function getRecruiterQuestions (jobMatchId, userId, successCallback,errorCallback) {

                genericService.getObjects(StorageService.get('baseurl') + urlConstants.SHOW_RECRUITER_QUESTIONS +  '/' + jobMatchId + '/' + userId).then(function (data) {
                    successCallback(data);
                 }, function (error) {
                      errorCallback(error);
                });
            }

            function saveRecruiterQuestions (feedbackExecutionResult, jobMatchId, successCallback,errorCallback) {
               genericService.addObject("api/feedbackanswers/saverecruiterscreeningquestions/" + jobMatchId ,  feedbackExecutionResult).then(function (data) {
                     successCallback(data);
                },  function (error) {
                        errorCallback(error);
            });


            }


            function saveJobMatchState (listOfjobdetails, statusOfSave, questionType , successCallback,errorCallback) {
                    genericService.addObject("api/feedbackanswers/savejobmatchstate/"  + statusOfSave ,  listOfjobdetails).then(function (data) {
                        successCallback(data);
                   }, function (error) {
                        errorCallback(error);
                });

            }


        }
    ]
);

