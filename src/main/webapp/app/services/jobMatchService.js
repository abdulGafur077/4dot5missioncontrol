servicesmodule = angular.module('4dot5.missioncontrol.servicesmodule');

/**
 * This is factory for the job match service
 */

servicesmodule.factory('jobMatchService',
    [
        '$q',
        '$http',
        'moment',
        '$filter',
        '$rootScope',
        'StorageService',
        'genericService',
        function ($q, $http, moment, $filter, $rootScope, StorageService, genericService) {
            var urlConstants = {
                SAVE_NOTES: 'api/jobmatch/notesandcommunications/save',
                GET_NOTES: 'api/jobmatch/contactandnotes',
                DELETE_NOTES: 'api/jobmatch/notesandcommunications/delete',
                GET_CANDIDATE_JOB_COMPARISON: 'api/job/candidateprofilecomparison',
                GET_JOB_MATCH_CANDIDATES: 'api/jobmatch/jobmatchcardview',
                GET_CANDIDATE_JOB_MATCH_CARD: 'api/jobmatch/candidateworkflowcard',
                SET_NOT_INTERESTED: 'api/candidate/setnotinterested',
                SET_RELEASED: 'api/candidate/setreleased',
                MOVE_STEP_BACKWARD: 'api/candidate/movebackward',
                MOVE_STEP_FORWARD: 'api/candidate/moveforward',
                SEND_TECH_ASSESSMENT: 'api/candidate/sendtechassessment',
                SEND_VALUE_ASSESSMENT: 'api/candidate/sendvalueassessment',
                RESEND_ASSESSMENT: 'api/candidate/resendassessment',
                CANCEL_TECH_ASSESSMENT: 'api/candidate/canceltechassessment',
                CANCEL_VALUE_ASSESSMENT: 'api/candidate/cancelvalueassessment',
                SCHEDULE_PHONE_SCREEN: 'api/candidate/schedulephonescreen',
                SCHEDULE_INTERVIEW: 'api/candidate/scheduleinterview',
                GET_PHONE_SCREENERS: 'api/jobmatch/getphonescreeners',
                GET_INTERVIEWERS: 'api/jobmatch/getinterviewers',
                GET_MEETING_DETAILS: 'api/jobmatch/getmeetingscheduledetails',
                RESCHEDULE_MEETING:'api/candidate/reschedulemeeting',
                CANCEL_MEETING:'api/candidate/cancelscheduledmeeting',
                JOB_MATCH_RECRUITERS: 'api/candidate/recruiters',
                ASSIGN_RECRUITER: 'api/candidate/setrecruiter',
                CREATE_JOB_MATCHES: 'api/jobmatch/createjobmatches',
                GET_ALL_JOB_MATCH_STATES: 'api/jobmatch/getallcandidateworkflowcardstatus'
            };

            var factory = {
                getNotesAndCommunications: getNotesAndCommunications,
                saveNoteOrCommunication: saveNoteOrCommunication,
                deleteNoteOrCommunication: deleteNoteOrCommunication,
                getCandidateJobComparison: getCandidateJobComparison,
                getJobMatchCandidates: getJobMatchCandidates,
                getCandidateJobMatchCard: getCandidateJobMatchCard,
                setJobMatchStatus: setJobMatchStatus,
                moveStepBackward: moveStepBackward,
                moveStepForward: moveStepForward,
                sendAssessment: sendAssessment,
                resendAssessment: resendAssessment,
                cancelAssessment: cancelAssessment,
                schedulePhoneScreenOrInterview: schedulePhoneScreenOrInterview,
                getPhoneScreeners: getPhoneScreeners,
                getInterviewers: getInterviewers,
                getMeetingDetails: getMeetingDetails,
                rescheduleMeeting: rescheduleMeeting,
                cancelMeeting: cancelMeeting,
                getJobMatchRecruiters: getJobMatchRecruiters,
                assignRecruiter: assignRecruiter,
                createJobMatches: createJobMatches,
                getAllJobMatchStates: getAllJobMatchStates
            };

            return factory;

            function getNotesAndCommunications(jobMatchId, successCallback, errorCallback){
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_NOTES + '/' + jobMatchId).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function saveNoteOrCommunication(noteOrCommunication, successCallback, errorCallback) {
                genericService.addObject(StorageService.get('baseurl') + urlConstants.SAVE_NOTES + '/' + noteOrCommunication.jobMatchId, noteOrCommunication).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function deleteNoteOrCommunication(noteOrCommunication, successCallback, errorCallback) {
                genericService.deleteObject(StorageService.get('baseurl') + urlConstants.DELETE_NOTES + '/' + noteOrCommunication.jobMatchId + '/' + noteOrCommunication.noteId).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getCandidateJobComparison(jobMatchId, successCallback, errorCallback){
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_CANDIDATE_JOB_COMPARISON + '/' + jobMatchId).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getJobMatchCandidates(userId, companyId, page, size, requisitionId, filterObject, successCallback, errorCallback){
                page = (_.isNull(page) || _.isUndefined(page)) ? 1 : page;
                size = (_.isNull(size) || _.isUndefined(size)) ? 5 : size;
                requisitionId = (_.isNull(requisitionId) || _.isUndefined(requisitionId)) ? '' : requisitionId;
                var urlParams = "?userId=" + userId + "&companyId=" + companyId + "&page=" + page + "&size=" + size + "&requisitionId=" + requisitionId;
                var promise = genericService.addObject(StorageService.get('baseurl') + urlConstants.GET_JOB_MATCH_CANDIDATES + urlParams, filterObject);
                promise.then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
                return promise;
            }

            function getCandidateJobMatchCard(jobMatchId, successCallback, errorCallback){
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_CANDIDATE_JOB_MATCH_CARD + '/' + jobMatchId).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function setJobMatchStatus(status, jobMatchesDataArray, successCallback, errorCallback) {
                var url = StorageService.get('baseurl');
                switch(status){
                    case 'Release':
                        url += urlConstants.SET_RELEASED;
                        break;
                    case 'Not Interested':
                        url += urlConstants.SET_NOT_INTERESTED;
                        break;
                    default:
                        break;
                }
                genericService.addObject(url, jobMatchesDataArray).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function moveStepBackward(moveData, successCallback, errorCallback) {
                genericService.addObject(StorageService.get('baseurl') + urlConstants.MOVE_STEP_BACKWARD, moveData).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function moveStepForward(moveData, successCallback, errorCallback) {
                genericService.addObject(StorageService.get('baseurl') + urlConstants.MOVE_STEP_FORWARD, moveData).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function sendAssessment(assessmentType, assessmentData, successCallback, errorCallback) {
                var url = urlConstants.SEND_TECH_ASSESSMENT;
                if(assessmentType == 'Value'){
                    url = urlConstants.SEND_VALUE_ASSESSMENT;
                }
                genericService.addObject(StorageService.get('baseurl') + url, assessmentData).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function resendAssessment(assessmentData, successCallback, errorCallback) {
                genericService.addObject(StorageService.get('baseurl') + urlConstants.RESEND_ASSESSMENT, assessmentData).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function cancelAssessment(assessmentType, assessmentData, successCallback, errorCallback) {
                var url = urlConstants.CANCEL_TECH_ASSESSMENT;
                if(assessmentType == 'Value'){
                    url = urlConstants.CANCEL_VALUE_ASSESSMENT;
                }
                genericService.addObject(StorageService.get('baseurl') + url, assessmentData).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function schedulePhoneScreenOrInterview(type, scheduleData, successCallback, errorCallback){
                var url = '';
                if(type == 'phoneScreen'){
                    url = urlConstants.SCHEDULE_PHONE_SCREEN;
                }else if (type == 'interview'){
                    url = urlConstants.SCHEDULE_INTERVIEW;
                }else{
                    //
                }
                genericService.addObject(StorageService.get('baseurl') + url, scheduleData).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getPhoneScreeners(jobMatchId, getCancelledMeetingsFlag, successCallback, errorCallback) {
                getCancelledMeetingsFlag = _.isUndefined(getCancelledMeetingsFlag) ? false : getCancelledMeetingsFlag;
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_PHONE_SCREENERS + '/' + jobMatchId + '?isCancelledMeetingScheduleRequired=' + getCancelledMeetingsFlag).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getInterviewers(jobMatchId, getCancelledMeetingsFlag, successCallback, errorCallback) {
                getCancelledMeetingsFlag = _.isUndefined(getCancelledMeetingsFlag) ? false : getCancelledMeetingsFlag;
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_INTERVIEWERS + '/' + jobMatchId + '?isCancelledMeetingScheduleRequired=' + getCancelledMeetingsFlag).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getMeetingDetails(meetingId, successCallback, errorCallback) {
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_MEETING_DETAILS + '/' + meetingId).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function rescheduleMeeting(meetingId, meetingObject, successCallback, errorCallback) {
                genericService.addObject(StorageService.get('baseurl') + urlConstants.RESCHEDULE_MEETING + '/' + meetingId, meetingObject).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function cancelMeeting(meetingId, meetingObject, successCallback, errorCallback){
                genericService.deleteObject(StorageService.get('baseurl') + urlConstants.CANCEL_MEETING + '/' + meetingId, meetingObject).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }
            
            function getJobMatchRecruiters(jobMatchIdsArray, successCallback, errorCallback ) {
                genericService.addObject(StorageService.get('baseurl') + urlConstants.JOB_MATCH_RECRUITERS, jobMatchIdsArray).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function assignRecruiter(jobMatchIdToRecruiterIdMap, successCallback, errorCallback ) {
                genericService.addObject(StorageService.get('baseurl') + urlConstants.ASSIGN_RECRUITER, jobMatchIdToRecruiterIdMap).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }
            
            function createJobMatches(createJobMatchesObject, successCallback, errorCallback) {
                genericService.addObject(StorageService.get('baseurl') + urlConstants.CREATE_JOB_MATCHES, createJobMatchesObject).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getAllJobMatchStates(successCallback, errorCallback){
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_ALL_JOB_MATCH_STATES).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }
        }
    ]
);

