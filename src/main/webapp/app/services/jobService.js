servicesmodule = angular.module('4dot5.missioncontrol.servicesmodule');

/**
 * This is factory for this module
 */

servicesmodule.factory('jobService',
    [
        '$q',
        '$http',
        'moment',
        '$filter',
        '$rootScope',
        'StorageService',
        'genericService',
        function ($q, $http, moment, $filter, $rootScope, StorageService, genericService) {
            var urlConstants = {
                GET_ALL_JOB_TYPES: 'api/requisition/jobtypes',
                GET_ALL_SPONSORSHIPS: 'api/requisition/sponsorshiptypes',
                GET_RECRUITERS: 'api/requisition/getrecruiterlist',
                GET_LICENSE_PREFERENCE: 'api/requisition/getlicensepreferences',
                GET_JOB:'api/requisition/findjob',
                SAVE_JOB: 'api/requisition/savejob',
                RUN_JOB_FOR_SCORING: 'api/requisition/runjobforscoring',
                SET_JOB_STATUS: 'api/requisition/setjobstatus',
                GET_CANDIDATE_COUNT: 'api/requisition/getcandidatecountbyreqtransaction',
                CREATE_JOB_DETAILS: 'api/job/createjob',
                UPDATE_JOB_DETAILS: 'api/job/updatejob',
                GET_RECRUITERS: 'api/requisition/getrecruiterlist',
                GET_ALL_STATUS_TYPES: 'api/job/statustypes',
                SEARCH_BY_REQNUMS: 'api/job/searchbyreqnum',
                SEARCH_BY_TEXT_JOBS: 'api/job/searchjob',
                GET_JOB_PROFILE: 'api/job/jobdetails',
                GET_CANDIDATES_BY_REQ_TRANSACTION: 'api/requisition/getcandidatesbyreqtransaction',
                GET_ACTIVITY_TYPES: 'api/job/activitytypes',
                GET_ENABLED_WORKFLOW_STEPS: 'api/job/enabledsteps',
                GET_DASHBOARD_JOB_COUNT: 'api/job/dashboardreqcount',
                MULTI_CANDIDATE_SINGLE_JOB_COMPARISON: 'api/job/multiplecandidateprofilesinglejobcomparison',
                FIND_MATCHING_CANDIDATES_FOR_JOB: 'api/candidate/findcandidatesforrequisition',
                GET_ASSESSMENT_ACCESS_TIME: 'api/assessment/getaccesstime',
                UPDATE_JOB_4DOT5_SCORE: 'api/job/updatefourdotfiveintelligencescore',
                UPDATE_JOB_LOCATION: 'api/job/updatelocation',
                GET_RESUMES_FROM_JOB_BOARDS: 'api/job/getMoreJBCandidates',
                GET_REQUISITION_CARD: 'api/requisition/getRequisitionCard',
                GET_JOB_BOARD_PARAMETERS: 'api/requisition/getjobboardparameters',
                GET_RADIUS_AND_RECENCY_OPTIONS: 'api/requisition/getradiusandrecencyoptions',
                DELETE_JOB: 'api/job/delete',
                GET_SEARCH_INTERFACE: 'api/job/getsearchinterface',
                GET_GET_MORE_RESUMES_JOB_STATUS: 'api/job/isgetmoreresumesinprogress',
                SAVE_DAXTRA_PARAMETERS: 'api/job/savedaxtraparameters',
                GET_JOB_SEARCH_HISTORY: 'api/job/getjobboardsearchhistory',
                GET_CANDIDATE_COUNT_BY_JOB_ID: 'api/job/getcandidatecountbyreqid',
                GET_CANDIDATE_JOB_MATCH_RESULTS: 'api/job/getcandidatejobmatchinfo'
            };



            var factory = {
                getAllJobTypes: getAllJobTypes,
                getAllSponsorshipTypes: getAllSponsorshipTypes,
                getAllRecruiters: getAllRecruiters,
                getLicensePreferences: getLicensePreferences,
                getJob: getJob,
                getJobProfile: getJobProfile,
                saveJob: saveJob,
                setJobStatusToOpen: setJobStatusToOpen,
                runJobForScoring: runJobForScoring,
                getCandidateCount: getCandidateCount,
                createUpdateJobDetails: createUpdateJobDetails,
                getAllRecruiters: getAllRecruiters,
                getAllStatusTypes: getAllStatusTypes,
                searchByReqNum: searchByReqNum,
                searchJobsByText: searchJobsByText,
                getCandidatesByRequisitionTransaction: getCandidatesByRequisitionTransaction,
                getActivityTypes: getActivityTypes,
                getEnabledWorkflowSteps: getEnabledWorkflowSteps,
                getJobCountByStatus: getJobCountByStatus,
                multipleCandidatesSingleJobComparison: multipleCandidatesSingleJobComparison,
                findMatchingCandidatesForJob: findMatchingCandidatesForJob,
                getAssessmentAccessTime: getAssessmentAccessTime,
                update4dot5Score: update4dot5Score,
                updateJobLocation: updateJobLocation,
                getGetMoreResumesJobStatus: getGetMoreResumesJobStatus,
                getResumesFromJobBoards: getResumesFromJobBoards,
                getRequisitionDetails: getRequisitionDetails,
                getJobBoardParameters: getJobBoardParameters,
                getJobBoardSearchInterface: getJobBoardSearchInterface,
                getJobBoardsSearchHistory: getJobBoardsSearchHistory,
                getRadiusAndRecencyOptions: getRadiusAndRecencyOptions,
                deleteJob: deleteJob,
                saveDaxtraParameters: saveDaxtraParameters,
                getJobBoardPullCounts: getJobBoardPullCounts,
                getJobBoardPullMatchInfo: getJobBoardPullMatchInfo
            };

            return factory;

            function getJob(jobId, successCallback, errorCallback) {
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_JOB + '/' + jobId).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getAllJobTypes(successCallback, errorCallback) {
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_ALL_JOB_TYPES).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }
            
            function getAllSponsorshipTypes(successCallback, errorCallback) {
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_ALL_SPONSORSHIPS).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getAllRecruiters(userDetails, company, successCallback,errorCallback) {
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_RECRUITERS + '/' + userDetails.id + '/' + company.companyId).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getLicensePreferences(company, clientorbu, jobId, successCallback, errorCallback) {
                var url = StorageService.get('baseurl') + urlConstants.GET_LICENSE_PREFERENCE + '/' + company.companyId + '/' + jobId;
                if(!_.isNull(clientorbu)){
                    url = url + '?clientorbuid=' + clientorbu.id;
                }
                genericService.getObjects(url).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function saveJob(job, successCallback, errorCallback){
                genericService.putObject(StorageService.get('baseurl') + urlConstants.SAVE_JOB, job).then(function (data) {
                    successCallback(data);
                    //goToNextTab();
                }, function (error) {
                    errorCallback(error);
                });
            }

            function setJobStatusToOpen(jobId, successCallback, errorCallback) {
                var jobStateObject = {
                    "jobId": jobId,
                    "jobStateType": "OPEN"
                };
                genericService.addObject(StorageService.get('baseurl') + urlConstants.SET_JOB_STATUS, jobStateObject).then(function (data) {
                    successCallback(data)
                }, function(error){
                    errorCallback(error);
                });
            }

            function runJobForScoring(companyId, jobId, transactionId, successCallback, errorCallback) {
                var transactionType = 'RequisitionTransaction';
                genericService.addObject(StorageService.get('baseurl') + urlConstants.RUN_JOB_FOR_SCORING + '/' + companyId + '/' + jobId + '/' + transactionType + '/' + transactionId + '/' + transactionId, {}).then(function (data) {
                    successCallback(data)
                }, function(error){
                    errorCallback(error);
                });
            }

            function getCandidateCount(jobId, score, transactionId, successCallback, errorCallback) {
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_CANDIDATE_COUNT + '/' + jobId + '?score=' + score + '&requisitionTransactionId=' + transactionId).then(function (data) {
                    successCallback(data)
                }, function(error){
                    errorCallback(error);
                });
            }

            function createUpdateJobDetails(updateFlag, saveObject, companyId, clientOrBuId, userId, successCallback, errorCallback){
                var url = urlConstants.CREATE_JOB_DETAILS + '/' + companyId;
                if(updateFlag){
                    url = urlConstants.UPDATE_JOB_DETAILS + '/' + companyId + '/' + userId;
                }
                genericService.addObject(StorageService.get('baseurl') + url + '?clientOrBuid=' + clientOrBuId, saveObject).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function searchByReqNum(userId, companyId, searchText, successCallback, errorCallback) {
                genericService.addObject(StorageService.get('baseurl') + urlConstants.SEARCH_BY_REQNUMS + '/' + userId + '/' + companyId+ '/' + searchText).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function searchJobsByText(userId, companyId, searchText, filterObj, successCallback, errorCallback) {
                filterObj.searchText = searchText;
                genericService.addObject(StorageService.get('baseurl') + urlConstants.SEARCH_BY_TEXT_JOBS + '/' + userId + '/' + companyId, filterObj).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getAllStatusTypes(successCallback, errorCallback) {
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_ALL_STATUS_TYPES).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getJobProfile(jobId, successCallback, errorCallback) {
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_JOB_PROFILE + '/' + jobId).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getCandidatesByRequisitionTransaction(jobId, score, reqTransactionId, countType, successCallback, errorCallback) {
                var requestParams = '?' + 'requisitionTransactionId=' + reqTransactionId + '&' + 'score=' + score;
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_CANDIDATES_BY_REQ_TRANSACTION + '/' + jobId + '/' + countType + requestParams).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                })
            }

            function getActivityTypes(successCallback, errorCallback) {
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_ACTIVITY_TYPES).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getEnabledWorkflowSteps(jobId, successCallback, errorCallback) {
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_ENABLED_WORKFLOW_STEPS + '/' + jobId).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getJobCountByStatus(userId, companyId, successCallback, errorCallback){
                var url = StorageService.get('baseurl') + urlConstants.GET_DASHBOARD_JOB_COUNT + '/' + userId + '/' + companyId;
                genericService.getObjects(url).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function multipleCandidatesSingleJobComparison(jobId, stringOfCandidateIds, successCallback, errorCallback) {
                if(_.isNull(stringOfCandidateIds) || _.isUndefined(stringOfCandidateIds)){
                    stringOfCandidateIds = '';
                }
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.MULTI_CANDIDATE_SINGLE_JOB_COMPARISON + '/' + jobId + '?candidateIds=' + stringOfCandidateIds).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function findMatchingCandidatesForJob(searchObject, successCallback, errorCallback){
                var promise = genericService.addObject(StorageService.get('baseurl') + urlConstants.FIND_MATCHING_CANDIDATES_FOR_JOB, searchObject);
                promise.then(function (data){
                    successCallback(data);
                }, function(error){
                    errorCallback(error);
                });
                return promise;
            }

            function getAssessmentAccessTime(jobId, licensePreferenceEnum, successCallback, errorCallback) {
                var url = StorageService.get('baseurl') + urlConstants.GET_ASSESSMENT_ACCESS_TIME;
                var payload = {};
                payload.jobId = jobId;
                payload.licensePreferenceEnum = licensePreferenceEnum;
                genericService.addObject(url, payload).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function update4dot5Score(scoreObject, successCallback, errorCallback){
                genericService.putObject(StorageService.get('baseurl') + urlConstants.UPDATE_JOB_4DOT5_SCORE, scoreObject).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function updateJobLocation(locationObject, successCallback, errorCallback){
                genericService.addObject(StorageService.get('baseurl') + urlConstants.UPDATE_JOB_LOCATION, locationObject).then(function(data){
                   successCallback(data);
                }, function(error){
                    errorCallback(error);
                });
            }

            function getGetMoreResumesJobStatus(jobId, successCallback, errorCallback) {
                var url = StorageService.get('baseurl') + urlConstants.GET_GET_MORE_RESUMES_JOB_STATUS+ '/'+ jobId;
                genericService.getObjects(url).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getResumesFromJobBoards(jobId, jobBoardDetails, resumesToPullPerJobBoardCount, successCallback, errorCallback) {
                var url = StorageService.get('baseurl') + urlConstants.GET_RESUMES_FROM_JOB_BOARDS;
                var payload = {};
                payload.jobId = jobId;
                payload.jobBoardDetails = jobBoardDetails;
                payload.resumesToPullPerJobBoardCount = resumesToPullPerJobBoardCount;
                genericService.addObject(url, payload).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getRequisitionDetails(jobId, companyId, successCallback, errorCallback) {
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_REQUISITION_CARD + '/' + jobId + '/' + companyId).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getJobBoardParameters(successCallback, errorCallback) {
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_JOB_BOARD_PARAMETERS).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getJobBoardSearchInterface(searchObject, successCallback, errorCallback){
                genericService.addObject(StorageService.get('baseurl') + urlConstants.GET_SEARCH_INTERFACE, searchObject).then(function(data){
                    successCallback(data);
                }, function(error){
                    errorCallback(error);
                });
            }

            function getJobBoardsSearchHistory(jobId, page, size, successCallback, errorCallback) {
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_JOB_SEARCH_HISTORY + '/' + jobId + '?page=' + page + '&size=' + size).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function deleteJob(deleteJobObject, successCallback, errorCallback){
                genericService.deleteObject(StorageService.get('baseurl') + urlConstants.DELETE_JOB, deleteJobObject).then(function(data){
                   successCallback(data);
                }, function(error){
                    errorCallback(error);
                });
            }

            function getRadiusAndRecencyOptions(successCallback, errorCallback) {
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_RADIUS_AND_RECENCY_OPTIONS).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function saveDaxtraParameters(jobId, jobBoardParamsObject, successCallback, errorCallback) {
                genericService.addObject(StorageService.get('baseurl') + urlConstants.SAVE_DAXTRA_PARAMETERS + '/' + jobId, jobBoardParamsObject).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getJobBoardPullCounts(jobId, jbiTransactionId, successCallback, errorCallback){
                var url = StorageService.get('baseurl') + urlConstants.GET_CANDIDATE_COUNT_BY_JOB_ID + '/' + jobId;
                if(!_.isNull(jbiTransactionId)){
                    url += '?jbiTransactionId=' + jbiTransactionId;
                }
                genericService.getObjects(url).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getJobBoardPullMatchInfo(jbiTransactionId, successCallback, errorCallback){
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_CANDIDATE_JOB_MATCH_RESULTS + '/' + jbiTransactionId).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }
        }
    ]
);

