servicesmodule = angular.module('4dot5.missioncontrol.servicesmodule');

/**
 * The service for modals.
 * Contains all the methods needed for managing modals.
 */

servicesmodule.factory('modalService',
    [
        '$uibModalStack',
        '$timeout',
        function ($uibModalStack, $timeout) {

            var factory = {
                isAnyModalOpen: isAnyModalOpen,
                addModalOpenClassToBodyIfAnyModalIsOpen: addModalOpenClassToBodyIfAnyModalIsOpen
            };

            return factory;

            function isAnyModalOpen(){
                if($uibModalStack.getTop()){
                    return true;
                }
                return false;
            }

            function addModalOpenClassToBodyIfAnyModalIsOpen(){
                if(isAnyModalOpen()){
                    // add back modal-open class to body to enable scrolling of the modal
                    $timeout(function(){
                        $('body').addClass('modal-open');
                    }, 500);
                }
            }
        }
    ]
);

