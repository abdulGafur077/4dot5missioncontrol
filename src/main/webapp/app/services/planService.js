servicesmodule = angular.module('4dot5.missioncontrol.servicesmodule');

/**
 * The service for plans.
 * Contains all the methods needed for plan management.
 */

servicesmodule.factory('planService',
    [
        'StorageService',
        'genericService',
        function (StorageService, genericService) {
            var urlConstants = {
                GET_ALL_PLANS: 'api/plan/getallplanfeatures',
                GET_PLAN_BY_COMPANY: 'api/plan/findplanbycompanyid',
                GET_PLAN: 'api/plan/findplanfeaturescapabilitybyid',
                GET_COMPANY_PLAN_DETAILS: 'api/plan/getplandetails',
                SAVE_PLAN: 'api/plan/saveplan'
            };

            var factory = {
                getAllPlans: getAllPlans,
                getCurrentPlan: getCurrentPlan,
                getPlan: getPlan,
                savePlan: savePlan,
                getCompanyPlanDetails: getCompanyPlanDetails
            };

            return factory;

            function getAllPlans(successCallback, errorCallback) {
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_ALL_PLANS).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getCurrentPlan(companyId, successCallback, errorCallback) {
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_PLAN_BY_COMPANY + '/' + companyId).then(function (data) {
                    successCallback(data);
                }, function(error){
                    errorCallback(error);
                });
            }
            
            function getPlan(companyId, successCallback, errorCallback) {
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_PLAN + '/' + companyId).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function savePlan(selectedPlan, successCallback, errorCallback) {
                genericService.addObject(StorageService.get('baseurl') + urlConstants.SAVE_PLAN, selectedPlan).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getCompanyPlanDetails(companyId, successCallback, errorCallback) {
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_COMPANY_PLAN_DETAILS + '/' + companyId).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }
        }
    ]
);

