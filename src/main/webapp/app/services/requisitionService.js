servicesmodule = angular.module('4dot5.missioncontrol.servicesmodule');

/**
 * This is factory for this module
 */

servicesmodule.factory('requisitionService',
    [
        '$q',
        '$http',
        '$httpParamSerializerJQLike',
        'moment',
        '$filter',
        '$rootScope',
        'StorageService',
        'genericService',
        function ($q, $http, $httpParamSerializerJQLike, moment, $filter, $rootScope, StorageService, genericService) {
            var urlConstants = {
                GET_ALL_CLIENT_OR_BU: 'api/requisition/getclientorbulist',
                UPLOAD_REQUISITION: 'api/requisition/uploadrequisitiondoc',
                GET_REQUISITION_ACTIVITY: 'api/requisition/requisitionActivity',
                JOB_BOARD_ADVANCED_SEARCH: 'http://jbws-az-4dot5.daxtra.com/jwb/dispatch'
            };

            var factory = {
                uploadRequisition: uploadRequisition,
                getAllClientsOrBu: getAllClientsOrBu,
                getUploadUrl: getUploadUrl,
                getRequisitionActivity: getRequisitionActivity,
                getJobBoardsAdvancedSearchInterface: getJobBoardsAdvancedSearchInterface
            };

            return factory;

            function uploadRequisition(requisition) {

            }

            function getAllClientsOrBu(userDetails, successCallback, errorCallback){
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_ALL_CLIENT_OR_BU + '/' + userDetails.id + '/' + userDetails.company.companyId).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getUploadUrl() {
                return StorageService.get('baseurl') + urlConstants.UPLOAD_REQUISITION;
            }

            function getRequisitionActivity(jobId, page, size, successCallback, errorCallback){
                if(_.isNull(page) || _.isUndefined(page)){
                    page = 1;
                }
                if(_.isNull(size) || _.isUndefined(size)){
                    size = 10;
                }
                var url = urlConstants.GET_REQUISITION_ACTIVITY  + '/' + jobId + '?page=' + page + '&size=' + size;
                genericService.addObject(StorageService.get('baseurl') + url).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getJobBoardsAdvancedSearchInterface(searchObject, successCallback, errorCallback) {
                searchObject = {
                    action: "search_interface",
                    db: "jw_fourdotfive",
                    codes: "IND",
                    js_lib: "jquery",
                    ajax_updater: "/jb/advancedsearch/updater",
                    ajax_auto_completer: "/jb/advancedsearch/autoCompleter",
                    libs: "jquery,jquery-ui,jb_params_jq,css"
                };
                var additionalParams = {
                    "headers": {
                        "Content-Type": "application/x-www-form-urlencoded"
                    }
                };
                genericService.addObject(urlConstants.JOB_BOARD_ADVANCED_SEARCH, $httpParamSerializerJQLike(searchObject), additionalParams).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }
        }
    ]
);

