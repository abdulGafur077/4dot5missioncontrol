servicesmodule = angular.module('4dot5.missioncontrol.servicesmodule');

/**
 * The service for resumes.
 * Contains all the methods needed for resume management.
 */

servicesmodule.factory('resumeService',
    [
        'StorageService',
        'genericService',
        function (StorageService, genericService) {
            var urlConstants = {
                GET_ALL_SOURCE_TYPES: 'api/resume/resumesourcetypes',
                UPLOAD_RESUME: 'api/resume/upload',
                RUN_RESUME_FOR_SCORING: 'api/resume/runresumesforscoring',
                GET_CANDIDATE_COUNT: 'api/resume/getcandidatecountbyresumetransaction',
                GET_RESUME_SOURCE_TYPES: 'api/resume/resumesourcetypes'
            };

            var factory = {
                getAllSourceTypes: getAllSourceTypes,
                getUploadUrl: getUploadUrl,
                runResumesForScoring: runResumesForScoring,
                getCandidateCount: getCandidateCount,
                getResumeSourceTypes: getResumeSourceTypes
            };

            return factory;

            function getAllSourceTypes(successCallback, errorCallback) {
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_ALL_SOURCE_TYPES).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getUploadUrl() {
                return StorageService.get('baseurl') + urlConstants.UPLOAD_RESUME;
            }

            function runResumesForScoring(parsingOutputObject, jobId, successCallback, errorCallback) {
                var url = StorageService.get('baseurl') + urlConstants.RUN_RESUME_FOR_SCORING + '/' + 'ResumeTransaction';
                if(!_.isNull(jobId)){
                    url += '?jobId=' + jobId;
                }
                genericService.addObject(url, parsingOutputObject).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getCandidateCount(transactionId, successCallback, errorCallback) {
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_CANDIDATE_COUNT + '/' + transactionId).then(function (data) {
                    successCallback(data);
                }, function(error){
                    errorCallback(error);
                });
            }

            function getResumeSourceTypes(companyId, successCallback, errorCallback) {
                var url = StorageService.get('baseurl') + urlConstants.GET_RESUME_SOURCE_TYPES;
                if(!_.isNull(companyId) && !_.isUndefined(companyId)){
                    url += '?companyId=' + companyId;
                }
                genericService.getObjects(url).then(function (data) {
                    successCallback(data);
                }, function(error){
                    errorCallback(error);
                });
            }
        }
    ]
);

