servicesmodule = angular.module('4dot5.missioncontrol.servicesmodule');

/**
 * The service for user notifications.
 * Contains the logic for initializing and getting user notifications.
 */

servicesmodule.factory('userNotificationsService',
    [
        '$q',
        '$rootScope',
        '$timeout',
        'StorageService',
        function ($q, $rootScope, $timeout, StorageService) {

            var urlConstants = {
                WEB_SOCKET_CONNECTION_URL: 'register',
                ECHO_URL: '/topic/echo',
                USER_NOTIFICATIONS: '/queue/' + $rootScope.userDetails.id + '/notifications',
                SEND_URL: '/send/url'
            };

            var listener = $q.defer();
            var socket = {
                client: null,
                stomp: null
            };
            var messageIds = [];


            var reconnect = function() {
                $timeout(function() {
                    initialize();
                }, 30000);
            };

            var initialize = function() {
                socket.client = new SockJS(StorageService.get('baseurl') + urlConstants.WEB_SOCKET_CONNECTION_URL);
                socket.stomp = Stomp.over(socket.client);
                socket.stomp.connect({}, startListener);
                socket.stomp.onclose = reconnect;
            };

            function receive() {
                return listener.promise;
            }

            function send(message) {
                var id = Math.floor(Math.random() * 1000000);
                socket.stomp.send(urlConstants.SEND_URL, {
                    priority: 9
                }, JSON.stringify({
                    message: message,
                    id: id
                }));
                messageIds.push(id);
            }

            var getMessage = function(data) {
                var message = JSON.parse(data.body);
                // out = {};
                // out.message = message.message;
                // out.time = new Date(message.time);
                // if (_.contains(messageIds, message.id)) {
                //     out.self = true;
                //     messageIds = _.remove(messageIds, message.id);
                // }
                //console.log('message recived text - ',message);
                return message;
            };

            var startListener = function() {
                socket.stomp.subscribe(urlConstants.USER_NOTIFICATIONS, function(data) {
                    listener.notify(getMessage(data));
                });
            };

            var factory = {
                receive: receive,
                send: send
            };

            initialize();

            return factory;

        }
    ]
);

