servicesmodule = angular.module('4dot5.missioncontrol.servicesmodule');

/**
 * The service for user role related methods.
 */

servicesmodule.factory('userRoleService',
    [
        '$rootScope',
        function ($rootScope) {

            var factory = {
                isLoggedInUserSuperUser: isLoggedInUserSuperUser,
                isLoggedInUser4dot5Admin: isLoggedInUser4dot5Admin,
                isLoggedInUserSuperUserOr4dot5Admin: isLoggedInUserSuperUserOr4dot5Admin,
                isLoggedInUserCompanyAdmin: isLoggedInUserCompanyAdmin
            };

            return factory;

            function isLoggedInUserSuperUser(){
                return $rootScope.userDetails.roleScreenRef.role.name == '4dot5SuperUser';
            }

            function isLoggedInUser4dot5Admin(){
                return $rootScope.userDetails.roleScreenRef.role.name == '4dot5Admin';
            }

            function isLoggedInUserSuperUserOr4dot5Admin(){
                return $rootScope.userDetails.roleScreenRef.role.name == '4dot5SuperUser' || $rootScope.userDetails.roleScreenRef.role.name == '4dot5Admin';
            }

            function isLoggedInUserCompanyAdmin(){
                return $rootScope.userDetails.roleScreenRef.role.name == 'CorporateAdmin' || $rootScope.userDetails.roleScreenRef.role.name == 'StaffingAdmin';
            }
        }
    ]
);

