servicesmodule = angular.module('4dot5.missioncontrol.servicesmodule');

/**
 * The service for users.
 * Contains all the methods needed for users.
 */

servicesmodule.factory('userService',
    [
        'StorageService',
        'genericService',
        function (StorageService, genericService) {
            var urlConstants = {
                GET_FILTERED_USERS: 'api/user/getfiltereduserdetails',
                DELETE_USER: 'api/user/delete',
                GET_DELETE_USER_INFO: 'api/user/deleteinfos'
            };

            var factory = {
                getFilteredUsers: getFilteredUsers,
                getFilteredUsersPromiseForTypeAhead: getFilteredUsersPromiseForTypeAhead,
                deleteUser: deleteUser,
                getDeleteUserInfo: getDeleteUserInfo
            };

            return factory;

            function getFilteredUsers(userFilterObject, successCallback, errorCallback){
                genericService.addObject(StorageService.get('baseurl') + urlConstants.GET_FILTERED_USERS, userFilterObject).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getFilteredUsersPromiseForTypeAhead(userFilterObject){
                return genericService.addObject(StorageService.get('baseurl') + urlConstants.GET_FILTERED_USERS, userFilterObject);
            }

            function deleteUser(deleteUserObject, successCallback, errorCallback){
                genericService.deleteObject(StorageService.get('baseurl') + urlConstants.DELETE_USER, deleteUserObject).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }

            function getDeleteUserInfo(userId, successCallback, errorCallback){
                genericService.getObjects(StorageService.get('baseurl') + urlConstants.GET_DELETE_USER_INFO + '/' + userId).then(function (data) {
                    successCallback(data);
                }, function (error) {
                    errorCallback(error);
                });
            }
        }
    ]
);

