
var USER_MGMT = {
		/**
		 * State Configurations
		 */
		MODULENAME : '4dot5.missioncontrolmodule.usermanagementmodule',
		CONSTATNTSNAME : 'USERMANAGEMENTCONSTANTS',
		STATE : 'usermanagement', 
		URL : '/user',
		CONTROLLER : 'UserManagementController',
		TEMPLATEURL : 'app/partials/missioncontrol/usermanagement/usermanagement.html',
}
