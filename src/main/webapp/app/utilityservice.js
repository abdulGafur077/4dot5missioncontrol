/**
 * This is utility module which can be reused, this mainly contains
 * factory methods related,
 * post, put and delete.
 * Note: All the function here follows the REST standard.
 *
 * @author Ashok Kumar KV
 *
 * @CopyRights Reserved with Spaneos Software Solutions Pvt. Ltd.
 *
 * All of the code within the Spaneos Software Solutions is developed and copyrighted
 * by Spaneos Software Solutions Pvt. Ltd., and may not be copied,replicated, or used in any
 * other software or application without prior permission from Spaneos Software Solutions Pvt. Ltd.
 * All usage must coincide with the Spaneos Software Solutions pvt ltd End User License Agreement.
 */
utilityservice = angular.module('UtilityServiceModule', []);
/**
 * All module level constants goes here
 */
utilityservice.constant('UTILITYSERVICECONSTANTS', {
    SERVICE: {
        GET_METHOD: 'GET',
        POST_METHOD: 'POST',
        PUT_METHOD: 'PUT',
        DELETE_METHOD: 'DELETE'
    },
    COMPANY_MGMT : {
        GET_COMPANIES : 'api/company/getallcompanies'
    },
    USER_MGMT: {
        SAVE_USER: 'api/user/saveuser',
        UPDATE_USER: 'api/user/updateuser',
        ASSIGN_MANAGER: 'api/user/assignmanager',
        GET_ENTITY_SCOPE_DETAILS: 'api/user/getuserentityscope',
        SET_ENTITY_SCOPE_DETAILS: 'api/user/setuserentityscope'
    },
    TEAM_MEMBERS: {
        GET_USER_DETAILS: 'api/user/getuserdetails'
    },
    CLIENT_OR_BU_MGMT: {
        GETCLIENT_OR_BU: 'api/company/getclientsorbu',
    },
    NOTIFICATION_PREFS_MGMT: {
        SET_NOTIFICATION_PREFS: 'api/notification/savenotificationprefs',
    },
    COMPANY_RES : {
        GET_RES: 'api/company/getroleentityscope',
        SET_RES: 'api/company/createroleentityscope',
        UPDATE_RES: 'api/company/updateroleentityscope'
    },
    FILTERS : {
        GET_FILTERS_DATA:'api/filters/filterdata',
        GET_ROLEANDRECRITER_FROM_CLIENTS:'api/filters/filterData/recruiterandrole/',
        GET_CLIENTANDROLE_FROM_RECRUITERS:'api/filters/filterData/clientandrole/',
        GET_CLIENTANDRECRITER_FROM_ROLES:'api/filters/filterData/clientandrecruiter/',
        GET_REQ_NUMBERS : 'api/requisitions/searchbynumber/'
    }
});
/**
 * This is do nothing configuration for this module,
 * if customization is required then can be configured accordingly.
 * Ex: Adding interceptors
 */
utilityservice.config([function () {

}]);

/**
 * This is factory for this module
 */
utilityservice.factory('utilityService',
    ['$q',
        '$http',
        'moment',
        '$filter',
        '$rootScope',
        'StorageService',
        'genericService',
        'MESSAGECONSTANTS',
        'UTILITYSERVICECONSTANTS',
        function ($q, $http, moment, $filter, $rootScope, StorageService, genericService, MESSAGECONSTANTS, UTILITYSERVICECONSTANTS) {

            var factory = {};

            /**
             * This is a helper method written to change companies list object as needed by UI
             * @ called for checking 4dot5 user entity scope.
             * @ author Ashok
             */
            var companiesBasedOnType = function(allcompanies){
                var companies = {};
                companies.staffingCompanies = [];
                companies.corporateCompanies = [];
                companies.hostCompanies = [];
                for(var i = 0; i < allcompanies.length; i++){
                    if(allcompanies[i].companyType === 'StaffingCompany'){
                        companies.staffingCompanies.push(allcompanies[i]);
                    }else if(allcompanies[i].companyType === 'Corporation'){
                        companies.corporateCompanies.push(allcompanies[i]);
                    }else if(allcompanies[i].companyType === 'Host'){
                        companies.hostCompanies.push(allcompanies[i]);
                    }
                }
                return companies;
            }

            /**
             * This is a helper method written to change client or BU list object as needed by UI
             * @ called when getting and setting of user entity scope to change api return object
             * @ author Ashok
             */
            var changeUserScopeObject = function(userscope){
                var ownscope = [];
                var forwardrecipientscope = [];
                // console.log('userscope : ' + angular.toJson(userscope));
                var scopeexists = angular.isDefined(userscope) && (userscope !== null) &&
                    angular.isDefined(userscope.clientOrBUList) && (userscope.clientOrBUList !== null) && userscope.clientOrBUList.length;
                if(scopeexists){
                    for(var i = 0; i < userscope.clientOrBUList.length; i++){
                        if(userscope.clientOrBUList[i].clientOwner === 'USER'){
                            ownscope.push(userscope.clientOrBUList[i]);
                        }else if(userscope.clientOrBUList[i].clientOwner === 'FORWARDED_RECIPIENT'){
                            forwardrecipientscope.push(userscope.clientOrBUList[i]);
                        }
                    }
                    userscope.clientOrBUList = [];
                    userscope.clientOrBUList[0] = ownscope;
                    userscope.clientOrBUList[1] = forwardrecipientscope;
                }
                return userscope;
            }

            /**
             * This is a helper method written to change client or BU list object as needed by API
             * @ called when setting of user entity scope to change api return object
             * @ author Ashok
             */
            var changeUserScopeObjecttoSave = function(userscope){
                var userscopearray = [];
                var scopeexiststosave = angular.isDefined(userscope) && (userscope !== null) &&
                    angular.isDefined(userscope.clientOrBUList) && (userscope.clientOrBUList !== null) && userscope.clientOrBUList.length;
                if(scopeexiststosave){
                    if(userscope.clientOrBUList[0].length){
                        // console.log('userscope.clientOrBUList[0].length : ' + userscope.clientOrBUList[0].length);
                        for(var i = 0; i < userscope.clientOrBUList[0].length; i++){
                            userscopearray.push(userscope.clientOrBUList[0][i]);
                        }
                    }else{
                        delete userscope.clientOrBUList[0];
                    }
                    if(userscope.clientOrBUList[1].length){
                        // console.log('userscope.clientOrBUList[1].length : ' + userscope.clientOrBUList[1].length);
                        for(var j = 0; j < userscope.clientOrBUList[1].length; j++){
                            userscopearray.push(userscope.clientOrBUList[1][j]);
                        }
                    }else{
                        delete userscope.clientOrBUList[1];
                    }
                    userscope.clientOrBUList = userscopearray;
                }
                return userscope;
            }

            /**
             * This is a helper method written to check null and empty conditions for forwardrecipient object.
             * @ author Ashok
             */
            var forwardRecipientExistCheck = function (userprofile) {
                /**
                 * Check for forward recipient obj exists in userprofile object
                 */
                var exists = angular.isDefined(userprofile.userForwardRecipientDetails) && !angular.equals(userprofile.userForwardRecipientDetails,null) &&
                    angular.isDefined(userprofile.userForwardRecipientDetails.forwardRecipient)  && !angular.equals(userprofile.userForwardRecipientDetails.forwardRecipient,null) &&
                    angular.isDefined(userprofile.userForwardRecipientDetails.forwardRecipient.firstName) && !angular.equals(userprofile.userForwardRecipientDetails.forwardRecipient.firstName,null) &&
                    !angular.equals(userprofile.userForwardRecipientDetails.forwardRecipient.firstName,'') &&
                    angular.isDefined(userprofile.userForwardRecipientDetails.fromDate) && !angular.equals(userprofile.userForwardRecipientDetails.fromDate,null) ;
                return exists;
            }

            /**
             * This method checks whether the user is 4dot5Admin or not
             * @param user - to which check has to be happen for role
             */
            var isFourDotFiveAdmin = function (role) {
                // console.log('role : ' + angular.toJson(role));
                if (angular.equals(role.id, MESSAGECONSTANTS.ROLEIDS.FOURDOTFIVE_ADMIN)) {
                    return true;
                }
                return false;
            }

            /**
             * This method checks whether the user is CompanyAdmin or not
             * @param user - to which check has to be happen for role
             */
            var isCompanyAdmin = function (role) {
                //  console.log('role : ' + angular.toJson(role));
                if (angular.equals(role.id, MESSAGECONSTANTS.ROLEIDS.STAFFING_ADMIN) || angular.equals(role.id, MESSAGECONSTANTS.ROLEIDS.CORPORATE_ADMIN)) {
                    return true;
                }
                return false;
            }

            /**
             * This is a helper method used to
             * convert userprofile object used in UI to user profile object needed to call saveuser api.
             */
            var changeUserObjectBeforeApi = function (userprofile) {
                /**
                 * change Manager (firstname = firstname + lastname) to (firstname = firstname) to save properly in db
                 */
                if (angular.isDefined(userprofile.userDetails.manager) && !angular.equals(userprofile.userDetails.manager, null) && !angular.equals(userprofile.userDetails.manager.lastName, null)) {
                    userprofile.userDetails.manager.firstName = userprofile.userDetails.manager.firstName.split(" ")[0];
                }

                /**
                 * change forward recipients Normal date format to ISO string
                 * which is accepted by severside api
                 */
                if (forwardRecipientExistCheck(userprofile)) {
                    var fromdate = new Date(userprofile.userForwardRecipientDetails.fromDate + ' ' + userprofile.userForwardRecipientDetails.fromTime).toISOString();
                    userprofile.userForwardRecipientDetails.fromDate = fromdate;
                    delete userprofile.userForwardRecipientDetails.fromTime;
                    var todate = new Date(userprofile.userForwardRecipientDetails.toDate + ' ' + userprofile.userForwardRecipientDetails.toTime).toISOString();
                    userprofile.userForwardRecipientDetails.toDate = todate;
                    delete userprofile.userForwardRecipientDetails.toTime;
                    delete userprofile.userForwardRecipientDetails.fullName;
                } else {
                    userprofile.userForwardRecipientDetails.forwardRecipient = null;
                    userprofile.userForwardRecipientDetails.fromDate = null;
                    userprofile.userForwardRecipientDetails.toDate = null;
                    if(angular.isDefined(userprofile.userForwardRecipientDetails.fromTime)){
                        delete userprofile.userForwardRecipientDetails.fromTime;
                    }
                    if(angular.isDefined(userprofile.userForwardRecipientDetails.toTime)){
                        delete userprofile.userForwardRecipientDetails.toTime;
                    }
                    if(angular.isDefined(userprofile.userForwardRecipientDetails.fullName)){
                        delete userprofile.userForwardRecipientDetails.fullName;
                    }
                }

                if (isFourDotFiveAdmin(userprofile.userDetails.role) || isCompanyAdmin(userprofile.userDetails.role)) {
                    userprofile.userScopeDetails = null;
                }

                userprofile.userScopeDetails = changeUserScopeObjecttoSave(userprofile.userScopeDetails);

                return userprofile;
            };

            /**
             * This is a helper method used to
             * convert userprofile object got from api to user profile object used in UI.
             */
            var changeUserObjectAfterApi = function (data) {
                /**
                 * change Manager firstname to firstname + lastname to show on UI
                 */
                if (angular.isDefined(data.userDetails) && !angular.equals(data.userDetails.manager, null) && !angular.equals(data.userDetails.manager.lastName, null)) {
                    data.userDetails.manager.firstName = data.userDetails.manager.firstName + ' ' + data.userDetails.manager.lastName;
                }

                /**
                 * change forward recipients date format to normal from ISO string to show on UI
                 */
                if (forwardRecipientExistCheck(data)) {
                    //console.log(data.userForwardRecipientDetails.fromDate);
                    data.userForwardRecipientDetails.fromTime = angular.copy($filter('date')(data.userForwardRecipientDetails.fromDate, "HH:mm:ss"));
                    data.userForwardRecipientDetails.toTime = angular.copy($filter('date')(data.userForwardRecipientDetails.toDate, "HH:mm:ss"));
                    data.userForwardRecipientDetails.fromDate = moment(data.userForwardRecipientDetails.fromDate).format("MMM D, YYYY");
                    data.userForwardRecipientDetails.toDate = moment(data.userForwardRecipientDetails.toDate).format("MMM D, YYYY");
                    data.userForwardRecipientDetails.forwardRecipient.ticked = true;
                    var toDate = new Date(data.userForwardRecipientDetails.toDate + ' ' + data.userForwardRecipientDetails.toTime);
                    var isafter = moment(toDate).isAfter(new Date());
                    if (isafter) {
                        data.userForwardRecipientDetails.forwardRecipient.current = '(current)';
                    }else{
                        data.userForwardRecipientDetails.forwardRecipient.current = null;
                    }
                    if (data.userForwardRecipientDetails.forwardRecipient.lastName != null) {
                        var frfullname = data.userForwardRecipientDetails.forwardRecipient.firstName + " " + data.userForwardRecipientDetails.forwardRecipient.lastName;
                    } else {
                        var frfullname = data.userForwardRecipientDetails.forwardRecipient.firstName;
                    }
                    data.userForwardRecipientDetails.forwardRecipient.fullName = frfullname;
                }else {
                    data.userForwardRecipientDetails.forwardRecipient = null;
                    data.userForwardRecipientDetails.fromDate = null;
                    data.userForwardRecipientDetails.toDate = null;
                }

                /**
                 * If loged in user changes his details.. Then updating in cookies and rootscope
                 * for immediate change in header and other places.
                 */
                if (data.userDetails.userId === $rootScope.userDetails.id) {
                    $rootScope.userDetails.firstname = data.userDetails.firstName;
                    $rootScope.userDetails.lastname = data.userDetails.lastName;
                    StorageService.set('userDetails', $rootScope.userDetails);
                }

                data.userScopeDetails = changeUserScopeObject(data.userScopeDetails);

                return data;
            };

            /**
             * This is Company Management Part of Utility Service
             */

            /**
             * This method is used to get all companies, to show in companies pop up
             */
            factory.getCompaniesforUser = function (role,userid,page,size,sort,sortDir) {
                page = 0;
                size = 10000;
                sort = 'name';
                sortDir = 'asc';
                deferred = $q.defer();
                genericService.getObjects(UTILITYSERVICECONSTANTS.COMPANY_MGMT.GET_COMPANIES + '/' + role.id + '/' + userid +  '?page=' + page + '&size=' + size + '&sort=' + sort + '&sortDir=' + sortDir).then(function (data) {
                    deferred.resolve(companiesBasedOnType(data));
                }, function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            }

            /**
             * This is Team Members Part of Utility Service
             */

            /**
             * This will allow to add object to DB by calling the api using http POST method.
             */
            factory.getUserProfile = function (user) {
                var deferred = $q.defer();
                genericService.getObjects(UTILITYSERVICECONSTANTS.TEAM_MEMBERS.GET_USER_DETAILS + '/' + user.userId).then(function (data) {
                    if(isFourDotFiveAdmin(data.userDetails.role)){
                        StorageService.set('4dot5Admin',true);
                    }else{
                        StorageService.remove('4dot5Admin');
                    }
                    deferred.resolve(changeUserObjectAfterApi(data));
                }, function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            };

            /**
             * This is User Management Part of Utility Service
             */

            /**
             * This will allow to add object to DB by calling the api using http POST method.
             * @param url at which api is mapped
             * @param object to be added
             */
            factory.saveUserProfileObject = function (url, userprofile) {
                var deferred = $q.defer();
                genericService.addObject(url,changeUserObjectBeforeApi(userprofile)).then(function (data) {
                    if (data.statusCode == "409") {
                        deferred.reject(data);
                    } else {
                        deferred.resolve(changeUserObjectAfterApi(data));
                    }
                }, function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            };

            /**
             * This will allow to update object to DB by calling the api using http POST method.
             * @param url at which api is mapped
             * @param object to be added
             */
            factory.updateUserProfileObject = function (userprofile) {
                var deferred = $q.defer();
                genericService.addObject(USERMANAGEMENTCONSTANTS.CONTROLLER.UPDATE_USER ,changeUserObjectBeforeApi(userprofile)).then(function (data) {
                    deferred.resolve(changeUserObjectAfterApi(data));
                }, function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            };

            /**
             * This service is used to assign manager to user.
             * @param managerid tells which manager to be assigned to user
             * @param userid to which user manager to be assigned
             */
            factory.assignManager = function (managerid, userid) {
                var deferred = $q.defer();
                genericService.addObject(UTILITYSERVICECONSTANTS.USER_MGMT.ASSIGN_MANAGER + '/' + managerid + '/' + userid).then(function (data) {
                    if (!angular.equals(data.userDetails.manager, null) && !angular.equals(data.userDetails.manager.lastName, null)) {
                        data.userDetails.manager.firstName = data.userDetails.manager.firstName + ' ' + data.userDetails.manager.lastName;
                    }
                    deferred.resolve(data);
                }, function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            };

            /**
             * This is used to get role entity scope details for user
             * @param userprofile to be added
             */
            factory.getUserRoleEntityScope = function (userprofile) {
                var deferred = $q.defer();
                genericService.getObjects(UTILITYSERVICECONSTANTS.USER_MGMT.GET_ENTITY_SCOPE_DETAILS + '/' + userprofile.userDetails.userId + '/' +$rootScope.userDetails.company.companyId + '/'+ userprofile.userDetails.role.id).then(function (data) {
                    // console.log('getUserRoleEntityScope : ' + angular.toJson(data));
                    deferred.resolve(changeUserScopeObject(data));
                }, function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            };

            /**
             * This is used to set role entity scope details for user
             * @param userprofile to be added
             */
            factory.setUserRoleEntityScope = function (userprofile) {
                //  console.log('in service setUserRoleEntityScope');
                var deferred = $q.defer();
                genericService.addObject(UTILITYSERVICECONSTANTS.USER_MGMT.SET_ENTITY_SCOPE_DETAILS + '/' + userprofile.userDetails.userId+ '/' +$rootScope.userDetails.company.companyId ,changeUserScopeObjecttoSave(userprofile.userScopeDetails)).then(function (data) {
                    deferred.resolve(changeUserObjectAfterApi(data));
                }, function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            };

            /**
             * This is used to get client or BU List Related to Company
             */
            factory.getClientsOrBU = function () {
                var deferred = $q.defer();
                genericService.getObjects(UTILITYSERVICECONSTANTS.CLIENT_OR_BU_MGMT.GETCLIENT_OR_BU ).then(function (data) {
                    deferred.resolve(data);
                }, function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            };

            /**
             * This is used to set forward recipient details for user
             * @param userprofile to be added
             */
            factory.setForwardRecipientDetails = function (url,userprofile) {
                // console.log('in service setUserRoleEntityScope');
                var deferred = $q.defer();
                genericService.addObject(url,changeUserObjectBeforeApi(userprofile)).then(function (data) {
                    deferred.resolve(changeUserObjectAfterApi(data));
                }, function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            };

            /**
             * This method is used to set or update Notification preferences for particular user
             */
            factory.saveNotificationPrefs = function (userid, companyid, notificationPrefsObj) {
                var deferred = $q.defer();
                genericService.addObject(UTILITYSERVICECONSTANTS.NOTIFICATION_PREFS_MGMT.SET_NOTIFICATION_PREFS + '/' + userid + '/' + companyid, notificationPrefsObj).then(function (data) {
                    deferred.resolve(data);
                }, function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            };

            /**
             * This method is used to get or Role Entity Scope for Company
             */
            factory.getCompanyRoleEntityScope = function () {
                var deferred = $q.defer();
                genericService.getObjects(UTILITYSERVICECONSTANTS.COMPANY_RES.GET_RES+'/'+$rootScope.userDetails.company.companyId).then(function (data) {
                    if(data.newScope){
                        for(var i = 0; i < data.scopeList.length; i++){
                            data.scopeList[i].allClientsOrBU = '';
                            data.scopeList[i].clientOrBUList = '';
                        }
                    }
                    deferred.resolve(data);
                }, function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            };

            /**
             * This part of Utility service is used for Filters
             */

            var getIDsfromArray = function(objarray){
                // console.log('objarray : ' + angular.toJson(objarray));
                var idarray = [];
                for(var i = 0; i < objarray.length; i++){
                    idarray.push(objarray[i].id);
                }
                //console.log('idarray : ' + angular.toJson(idarray));
                return idarray;
            }

            var getNamesfromArray = function(objarray){
                // console.log('objarray : ' + angular.toJson(objarray));
                var namearray = [];
                for(var i = 0; i < objarray.length; i++){
                    namearray.push(objarray[i].name);
                }
                // console.log('namearray : ' + angular.toJson(namearray));
                return namearray;
            }

            var handlefilterObjforClientorBUChanges = function(data,filterObj){
                /**
                 * Handling changes for recruiters
                 */
                for(var i = 0; i < data.recruiters.length; i++){
                    for(var k = 0; k < filterObj.recruiters.length; k++){
                        if((data.recruiters[i].id === filterObj.recruiters[k].id) && filterObj.recruiters[k].ticked){
                            data.recruiters[i].ticked = true;
                            break;
                        }
                    }
                }

                /**
                 * Handling changes for role titles
                 */
                var obj = {};
                obj.roleList = [];
                if(filterObj.roleList.length>0){
                    for(var i = 0; i < data.roleList.length; i++){
                        for(var k = 0; k < filterObj.roleList.length; k++){
                            obj.roleList[i] = {};
                            obj.roleList[i].name = data.roleList[i];
                            if((data.roleList[i] === filterObj.roleList[k].name) && filterObj.roleList[k].ticked){
                                obj.roleList[i].ticked = true;
                                break;
                            }
                        }
                    }
                    data.roleList = angular.copy(obj.roleList);
                }else{
                    for(var i = 0; i < data.roleList.length; i++){
                        obj.roleList[i] = {};
                        obj.roleList[i].name = data.roleList[i];
                    }
                    data.roleList = angular.copy(obj.roleList);
                }


                // console.log('data : ' + angular.toJson(data));
                return data;
            }

            var handlefilterObjforRoleChanges = function(data,filterObj){
                /**
                 * Handling changes for recruiters
                 */
                for(var i = 0; i < data.recruiters.length; i++){
                    for(var k = 0; k < filterObj.recruiters.length; k++){
                        if((data.recruiters[i].id === filterObj.recruiters[k].id) && filterObj.recruiters[k].ticked){
                            data.recruiters[i].ticked = true;
                            break;
                        }
                    }
                }

                /**
                 * Handling changes for clients or bus
                 */
                for(var i = 0; i < data.clientOrBU.length; i++){
                    for(var k = 0; k < filterObj.clientOrBU.length; k++){
                        if((data.clientOrBU[i].id === filterObj.clientOrBU[k].id) && filterObj.clientOrBU[k].ticked){
                            data.clientOrBU[i].ticked = true;
                            break;
                        }
                    }
                }


                return data;
            }

            var handlefilterObjforRecruiterChanges = function(data,filterObj){
                /**
                 * Handling changes for clients or bus
                 */
                for(var i = 0; i < data.clientOrBU.length; i++){
                    for(var k = 0; k < filterObj.clientOrBU.length; k++){
                        if((data.clientOrBU[i].id === filterObj.clientOrBU[k].id) && filterObj.clientOrBU[k].ticked){
                            data.clientOrBU[i].ticked = true;
                            break;
                        }
                    }
                }

                /**
                 * Handling changes for role titles
                 */
                var obj = {};
                obj.roleList = [];
                for(var i = 0; i < data.roleList.length; i++){
                    for(var k = 0; k < filterObj.roleList.length; k++){
                        obj.roleList[i] = {};
                        obj.roleList[i].name = data.roleList[i];
                        if((data.roleList[i] === filterObj.roleList[k].name) && filterObj.roleList[k].ticked){
                            obj.roleList[i].ticked = true;
                            break;
                        }
                    }
                }
                data.roleList = angular.copy(obj.roleList);
                // console.log('data : ' + angular.toJson(data));
                return data;
            }
            /* var changedFilterDataObject;*/
            var changingFilterDataMethod = function(changedFilterDataObject){
                var obj = {};
                obj.candidateStatus = [];
                obj.roleList = [];
                obj.requisitionStates = [];
                obj.requisitionNums = [];
                obj.workflowStates = [];
                for(var i = 0; i < changedFilterDataObject.candidateStatus.length; i++){
                    // console.log("entered ")
                    obj.candidateStatus[i] = {};
                    obj.candidateStatus[i].name = changedFilterDataObject.candidateStatus[i];
                }
                for(var i = 0; i < changedFilterDataObject.roleList.length; i++){
                    obj.roleList[i] = {};
                    obj.roleList[i].name = changedFilterDataObject.roleList[i];
                }
                for(var i = 0; i < changedFilterDataObject.requisitionStates.length; i++){
                    obj.requisitionStates[i] = {};
                    /* if(($rootScope.toState.name === 'missioncontrol.activecandidatesjobcardview' || $rootScope.toState.name === 'missioncontrol.passivecandidatesjobcardview') && changedFilterDataObject.requisitionStates[i] === "Draft"){
                         continue;
                     }else{
                         obj.requisitionStates[i].name = changedFilterDataObject.requisitionStates[i];
                     }*/
                    obj.requisitionStates[i].name = changedFilterDataObject.requisitionStates[i];
                }
                for(var i = 0; i < changedFilterDataObject.requisitionNums.length; i++){
                    obj.requisitionNums[i] = {};
                    obj.requisitionNums[i].id = changedFilterDataObject.requisitionNums[i];
                }
                for(var i = 0; i < changedFilterDataObject.workflowStates.length; i++){
                    obj.workflowStates[i] = {};
                    obj.workflowStates[i].name = changedFilterDataObject.workflowStates[i];
                }
                changedFilterDataObject.candidateStatus = obj.candidateStatus;
                changedFilterDataObject.roleList = obj.roleList;
                changedFilterDataObject.requisitionStates = obj.requisitionStates;
                changedFilterDataObject.requisitionNums = obj.requisitionNums;
                changedFilterDataObject.workflowStates = obj.workflowStates;
                // console.log('changedFilterDataObject : ' + angular.toJson(changedFilterDataObject));
                return angular.copy(changedFilterDataObject);
            }
            /**
             * This service is used to get Filter Data
             */
            factory.getFilterData = function(){
                //console.log($rootScope.userDetails.company.companyId)
                var deferred = $q.defer();
                //console.log("State ",$rootScope.toState.name);
                if($rootScope.toState.name === 'missioncontrol.activecandidatesjobcardview'){
                    genericService.getObjects(StorageService.get('baseurl') + UTILITYSERVICECONSTANTS.FILTERS.GET_FILTERS_DATA + '/' +  $rootScope.userDetails.company.companyId + '/' + $rootScope.userDetails.id+'/active').then(function (data) {
                        deferred.resolve(changingFilterDataMethod(angular.copy(data)));
                    }, function (error) {
                        deferred.reject(error);
                    });
                }else if($rootScope.toState.name === 'missioncontrol.passivecandidatesjobcardview'){
                    genericService.getObjects(StorageService.get('baseurl') + UTILITYSERVICECONSTANTS.FILTERS.GET_FILTERS_DATA + '/' +  $rootScope.userDetails.company.companyId + '/' + $rootScope.userDetails.id+'/passive').then(function (data) {
                        deferred.resolve(changingFilterDataMethod(angular.copy(data)));
                    }, function (error) {
                        deferred.reject(error);
                    });
                }else{
                    genericService.getObjects(StorageService.get('baseurl') + UTILITYSERVICECONSTANTS.FILTERS.GET_FILTERS_DATA + '/' +  $rootScope.userDetails.company.companyId + '/' + $rootScope.userDetails.id+'/other').then(function (data) {
                        deferred.resolve(changingFilterDataMethod(angular.copy(data)));
                    }, function (error) {
                        deferred.reject(error);
                    });
                }
                return deferred.promise;
            }

            /**
             * This service is used to get Filter Data
             */
            factory.getRoleandRecrforClient = function(selectedFilterObj,filterObj){
                var deferred = $q.defer();
                genericService.addObject( StorageService.get('baseurl')+ UTILITYSERVICECONSTANTS.FILTERS.GET_ROLEANDRECRITER_FROM_CLIENTS + $rootScope.userDetails.company.companyId + '/' + $rootScope.userDetails.id, getIDsfromArray(selectedFilterObj.clientOrBU)).then(function (data) {
                    //console.log('in utility: ' + angular.toJson(data));
                    deferred.resolve(handlefilterObjforClientorBUChanges(angular.copy(data),filterObj));
                }, function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            }

            /**
             * This service is used to get Filter Data
             */
            factory.getRoleandRecrforBU = function(selectedFilterObj,filterObj){
                var deferred = $q.defer();
                genericService.addObject( StorageService.get('baseurl')+ UTILITYSERVICECONSTANTS.FILTERS.GET_ROLEANDRECRITER_FROM_CLIENTS + $rootScope.userDetails.company.companyId + '/' + $rootScope.userDetails.id, getIDsfromArray(selectedFilterObj.clientOrBU)).then(function (data) {
                    deferred.resolve(handlefilterObjforClientorBUChanges(angular.copy(data),filterObj));
                }, function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            };

            /**
             * This service is used to get Filter Data
             */
            factory.getClientandRoleforRecr = function(selectedFilterObj,filterObj){
                var deferred = $q.defer();
                genericService.addObject( StorageService.get('baseurl')+ UTILITYSERVICECONSTANTS.FILTERS.GET_CLIENTANDROLE_FROM_RECRUITERS + $rootScope.userDetails.company.companyId + '/' + $rootScope.userDetails.id, getIDsfromArray(selectedFilterObj.recruiters)).then(function (data) {
                    deferred.resolve(handlefilterObjforRecruiterChanges(angular.copy(data),filterObj));
                }, function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            };

            /**
             * This service is used to get Filter Data
             */
            factory.getClientandRecrforRole = function(selectedFilterObj,filterObj){
                var deferred = $q.defer();
                genericService.addObject( StorageService.get('baseurl') + UTILITYSERVICECONSTANTS.FILTERS.GET_CLIENTANDRECRITER_FROM_ROLES + $rootScope.userDetails.company.companyId + '/' + $rootScope.userDetails.id, getNamesfromArray(selectedFilterObj.roleList)).then(function (data) {
                    deferred.resolve(handlefilterObjforRoleChanges(angular.copy(data),filterObj));
                }, function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            };

            factory.getrequisitionNumsAPI = function(recruiterNums,filterObj){
                var deferred = $q.defer();
                genericService.getObjects(StorageService.get('baseurl') + UTILITYSERVICECONSTANTS.FILTERS.GET_REQ_NUMBERS + recruiterNums + '/' + $rootScope.userDetails.id + '/'+ $rootScope.userDetails.company.companyId).then(function (data) {
                    var obj = {};
                    obj.requisitionNums = [];
                    for(var i = 0; i < data.length; i++){
                        obj.requisitionNums[i] =  {};
                        obj.requisitionNums[i].id = data[i];
                    }
                    deferred.resolve(obj);
                }, function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            };

            /**
             * Upload Requisition Part
             */

            var getJobOpeningsArray = function(requisitionObject){
                var jobOpeningsArray = [];
                /* console.log('get job openings array method--------------------------->'+angular.toJson(requisitionObject));*/
                if (requisitionObject.assignRecruiters.jobOpeningsAvailable === 'AllAvailable') {
                    for (var i = 0; i < requisitionObject.assignRecruiters.availableRequisitions.length; i++) {
                        var jobOpeningsObj = {
                            jobOpeningId: null,
                            openingNumber: 0,
                            requisitionOpeningNumber: "",
                            assignedRecruiter: null,
                            fillCandidate: null,
                            joinCandidate: null,
                            assignedRecruiters: [],
                            note: null
                        };
                        if(requisitionObject.assignRecruiters.jobOpenings[i] != null){
                            //console.log('job opening id: '+requisitionObject.assignRecruiters.jobOpenings[i].jobOpeningId);
                            jobOpeningsObj.jobOpeningId = requisitionObject.assignRecruiters.jobOpenings[i].jobOpeningId;
                        }
                        jobOpeningsObj.openingNumber = i + 1;
                        jobOpeningsObj.requisitionOpeningNumber = angular.copy(requisitionObject.assignRecruiters.availableRequisitions[i]);
                        jobOpeningsObj.assignedRecruiters = angular.copy(requisitionObject.assignRecruiters.selectedrecruiters);
                        jobOpeningsArray.push(angular.copy(jobOpeningsObj));
                        /*jobOpeningsObj.assignedRecruiter = angular.copy(requisitionObject.assignRecruiters.jobOpenings.assignedRecruiter);
                        jobOpeningsObj.fillCandidate = angular.copy(requisitionObject.assignRecruiters.jobOpenings.fillCandidate);
                        jobOpeningsObj.joinCandidate = angular.copy(requisitionObject.assignRecruiters.jobOpenings.joinCandidate);
                        jobOpeningsObj.note = angular.copy(requisitionObject.assignRecruiters.jobOpenings.note);*/
                    }
                } else {
                    for (var i = 0; i < requisitionObject.assignRecruiters.selectedRecReqObjects.length; i++) {
                        for (var j = 0; j < requisitionObject.assignRecruiters.selectedRecReqObjects[i].requisitions.length; j++) {
                            jobOpeningsObj = {
                                jobOpeningId: null,
                                openingNumber: 0,
                                requisitionOpeningNumber: "",
                                assignedRecruiter: null,
                                fillCandidate: null,
                                joinCandidate: null,
                                assignedRecruiters: [],
                                note: null
                            }
                            if(requisitionObject.assignRecruiters.jobOpenings[i] != null){
                                //console.log('job opening id: '+requisitionObject.assignRecruiters.jobOpenings[i].jobOpeningId);
                                jobOpeningsObj.jobOpeningId = requisitionObject.assignRecruiters.jobOpenings[i].jobOpeningId;
                            }
                            jobOpeningsObj.openingNumber = parseInt(requisitionObject.assignRecruiters.selectedRecReqObjects[i].requisitions[j].split("-")[1]);
                            jobOpeningsObj.requisitionOpeningNumber = angular.copy(requisitionObject.assignRecruiters.selectedRecReqObjects[i].requisitions[j])
                            for (var k = 0; k < requisitionObject.assignRecruiters.selectedRecReqObjects[i].recruiters.length; k++) {
                                jobOpeningsObj.assignedRecruiters.push(angular.copy(requisitionObject.assignRecruiters.selectedRecReqObjects[i].recruiters[k]));
                            }
                            jobOpeningsArray.push(angular.copy(jobOpeningsObj));
                            /*jobOpeningsObj.assignedRecruiter = angular.copy(requisitionObject.assignedRecruiter.jobOpenings.assignedRecruiter);
                            jobOpeningsObj.fillCandidate = angular.copy(requisitionObject.assignedRecruiter.jobOpenings.fillCandidate);
                            jobOpeningsObj.joinCandidate = angular.copy(requisitionObject.assignedRecruiter.jobOpenings.joinCandidate);
                            jobOpeningsObj.note = angular.copy(requisitionObject.assignedRecruiter.jobOpenings.note);*/
                        }
                    }
                }
                return jobOpeningsArray;
            };

            var getSponsorshipArray = function(sponsorship){
                var sponsorshipArray = [];
                for(var i = 0; i < sponsorship.length; i++){
                    sponsorshipArray.push(sponsorship[i].name);
                }
            };

            var getWorkflowStepData = function(requisitionObject){
                if(angular.isDefined(requisitionObject.licensePreferenceDetails) && angular.isDefined(requisitionObject.licensePreferenceDetails.fourDotFiveIntellList)){
                    delete requisitionObject.licensePreferenceDetails.fourDotFiveIntellList.sliderValue;
                }
                if(angular.isDefined( requisitionObject.licensePreferenceDetails) && angular.isDefined(requisitionObject.licensePreferenceDetails.techAssessmentList !== null)){
                    delete requisitionObject.licensePreferenceDetails.techAssessmentList.sliderValue;
                }
                return requisitionObject.licensePreferenceDetails;
            };

            var changeReqObjBeforeApi  = function (requisitionObject) {
                console.log("changeReqObjBeforeApi ", requisitionObject.currentJobState);
                var reqTempObj = {};

                reqTempObj = {
                    id : angular.copy(requisitionObject.id),
                    totalJobOpenings : angular.copy(requisitionObject.assignRecruiters.totalJobOpenings),
                    title :angular.copy(requisitionObject.assignRecruiters.title),
                    jobOpeningsAvailable : angular.copy(requisitionObject.assignRecruiters.jobOpeningsAvailable),
                    requisitionNumber : angular.copy(requisitionObject.assignRecruiters.requisitionNumber),
                    jobType : '',
                    sponsorship : [],
//     				sourceTypeDto : angular.copy(requisitionObject.uploadResumes.sourceTypeDto),
                    clientorbu : angular.copy(requisitionObject.uploadReq.clientorbu),
                    company : angular.copy(requisitionObject.uploadReq.company),
                    jobProfile : angular.copy(requisitionObject.assignRecruiters.jobProfile),
                    //licensePrefDtoList : getWorkflowStepData(angular.copy(requisitionObject)),
                    jobOpenings : getJobOpeningsArray(requisitionObject),
                    jobRequisitions : angular.copy(requisitionObject.jobRequisitions),
                    techAssesmentSelect : angular.copy(requisitionObject.techAssesmentSelect),
                    currentJobState  : angular.copy(requisitionObject.currentJobState)
//     				resumeList : []
                };
                console.log("reqTempObj " + reqTempObj);
                if(reqTempObj.clientorbu !== null){
                    if(angular.isDefined(reqTempObj.clientorbu.disabled)){
                        delete reqTempObj.clientorbu.disabled;
                        delete reqTempObj.clientorbu.selected;
                        delete reqTempObj.clientorbu.clientOwner;
                    }
                }
                for (var i = 0; i < requisitionObject.assignRecruiters.sponsorship.length; i++) {
                    reqTempObj.sponsorship.push(angular.copy(requisitionObject.assignRecruiters.sponsorship[i].name));
                }
                for (var i = 0; i < reqTempObj.jobOpenings.length; i++) {
                    for (var j = 0; j < reqTempObj.jobOpenings[i].assignedRecruiters.length; j++) {
                        delete reqTempObj.jobOpenings[i].assignedRecruiters[j].selected;
                        delete reqTempObj.jobOpenings[i].assignedRecruiters[j].disabled;
                    }
                }
                //	console.log("requisitionObject.techAssessmentMasterDto"+angular.toJson(requisitionObject.techAssesmentSelect));
                if(angular.isDefined(requisitionObject.techAssesmentSelect.assessmentName)){
                    reqTempObj.techAssessmentMasterDto = {
                        id: null,
                        assessmentName: angular.copy(requisitionObject.techAssesmentSelect.assessmentName),
                        testURL: null,
                        companyDto: angular.copy(requisitionObject.clientorbu)
                    }
                }else{
                    delete reqTempObj.techAssessmentMasterDto;
                }
                if(requisitionObject.assignRecruiters.jobType.length > 0){
                    reqTempObj.jobType = requisitionObject.assignRecruiters.jobType[0].name;
                }else{
                    reqTempObj.jobType = '';
                }
                /* console.log('---------------------------------->'+angular.toJson(reqTempObj));*/
                return reqTempObj;
            };

            var getAvailableRequisitions = function(requisitionObject){
                var availableRequisitions = [];
                for (i = 1; i <= requisitionObject.totalJobOpenings; i++) {
                    //availableRequisitions.push(requisitionObject.requisitionNumber+'-' + i);
                }
                return availableRequisitions;
            };

            var isAlreadyExists = function(selectedrecruiters,recr){
                for(var i = 0; i < selectedrecruiters.length; i++){
                    if(selectedrecruiters[i].userId === recr.userId){
                        return true;
                    }
                }
                return false;
            }

            var getSelectedRecruiters = function(requisitionObject){
                var selectedRecruiters = [];
                if(requisitionObject.jobOpenings){
                    for(i = 0; i < requisitionObject.jobOpenings.length; i++){
                        for(var k = 0; k < requisitionObject.jobOpenings[i].assignedRecruiters.length; k++){
                            if(!isAlreadyExists(selectedRecruiters,requisitionObject.jobOpenings[i].assignedRecruiters[k])){
                                selectedRecruiters.push(requisitionObject.jobOpenings[i].assignedRecruiters[k]);
                            }
                        }
                    }
                }
                return selectedRecruiters;
            };

            var getSelectedRecReqObjects = function(requisitionObject){
                var selectedRecReqObjects = [];
                var isExists = function(recruiter){
                    for( var k = 0; k < selectedRecReqObjects.length; k++){
                        for( var l = 0; l < selectedRecReqObjects[k].recruiters.length; l++){
                            if(recruiter.userId === selectedRecReqObjects[k].recruiters[l].userId){
                                return k;
                            }
                        }
                    }
                    return null;
                };
                var recReqObject = {};
                if(requisitionObject.jobOpenings){
                    for(var i = 0; i < requisitionObject.jobOpenings.length; i++){
                        recReqObject.requisitions = [];
                        recReqObject.recruiters = [];
                        for(var j = 0; j < requisitionObject.jobOpenings[i].assignedRecruiters.length; j++){
                            var indexExists = isExists(angular.copy(requisitionObject.jobOpenings[i].assignedRecruiters[j]));
                            if(indexExists === null){
                                recReqObject.recruiters.push(requisitionObject.jobOpenings[i].assignedRecruiters[j]);
                            }else{
                                recReqObject.recruiters = [];
                                selectedRecReqObjects[indexExists].requisitions.push(requisitionObject.jobOpenings[i].requisitionOpeningNumber);
                                break;
                            }
                        }
                        if(recReqObject.recruiters.length > 0){
                            recReqObject.requisitions.push(requisitionObject.jobOpenings[i].requisitionOpeningNumber);
                            selectedRecReqObjects.push(angular.copy(recReqObject));
                        }
                    }
                    //console.log("recReqObject.requisitions "+angular.toJson(recReqObject))
                }

                return selectedRecReqObjects;
            };

            var getJobTypeObject = function(requisitionObject){
                var jobTypeArray = [];
                var jobTypeObj = {};
                jobTypeObj.name = requisitionObject.jobType;
                jobTypeObj.selected = true;
                jobTypeArray.push(jobTypeObj);
                return jobTypeArray;
            };
            var sponsorshipList = [{"name":"CPT"},{"name":"OPT"},{"name":"H1"},{"name":"B1"},{"name":"L1"},{"name":"H1 Transfer"},{"name":"Permanent Resident/Green Card"},{"name":"Citizen only"},{"name":"Other"}];
            var getSponsorShipArray = function(requisitionObject){
                var sponsorshipArray = [];
                if(requisitionObject.sponsorships){
                    for(var s = 0; s < sponsorshipList.length; s++){
                        for(var t = 0; t < requisitionObject.sponsorships.length; t++){
                            var sponsorship = {};
                            if(sponsorshipList[s].name == requisitionObject.sponsorships[t]){
                                sponsorship.name = requisitionObject.sponsorships[t];
                                sponsorship.selected = true;
                                sponsorshipArray.push(sponsorship);
                            }
                        }
                    }
                }
                return sponsorshipArray;
            };

            var changeReqObjAfterApi  = function (requisitionObject,oldRequisitionObject) {
                var reqTempObj = {};
                reqTempObj = {
                    id: angular.copy(requisitionObject.id),
                    uploadReq : {},
                    assignRecruiters : {},
                    jobRequisitionList: angular.copy(requisitionObject.jobRequisitionList[0]),
                    uploadResumes: {},
                    viewResults : {},
                    currentJobState : angular.copy(requisitionObject.currentJobState)
                };

                var clientorbu = requisitionObject.clientorbu;
                //? requisitionObject.clientCompany : requisitionObject.clientorbu;
                // var tempClientOrCompany = {};
                // tempClientOrCompany.aliasName = null;
                // tempClientOrCompany.companyId = clientorbu.id;
                // tempClientOrCompany.companyName = clientorbu.name;
                // tempClientOrCompany.companyState = clientorbu.companyState;
                // tempClientOrCompany.companyType = clientorbu.companyType;

                reqTempObj.uploadReq = {
                    clientorbu : clientorbu,
                    company : angular.copy(requisitionObject.company),
                    uploadedFile : angular.copy(oldRequisitionObject.uploadReq.uploadedFile)
                };

                console.log('reqtempobj.uploadReq.clientorbu',reqTempObj.uploadReq.clientorbu);

                reqTempObj.assignRecruiters = {
                    totalJobOpenings: angular.copy(requisitionObject.numberOfOpenings),
                    title :angular.copy(requisitionObject.title),
                    jobType: getJobTypeObject(angular.copy(requisitionObject)),
                    jobOpeningsAvailable: angular.copy(requisitionObject.jobOpeningsAvailable),
                    requisitionNumber: angular.copy(requisitionObject.requisitionNumber),
                    sponsorship: getSponsorShipArray(angular.copy(requisitionObject)),
                    jobProfile: angular.copy(requisitionObject.jobProfile),
                    jobOpenings: requisitionObject.jobOpenings,
                    availableRequisitions : [],
                    selectedrecruiters : [],
                    selectedRecReqObjects : [],
                    isSponsorship: false
                };

                if(requisitionObject.sponsorships && requisitionObject.sponsorships.length > 0){
                    reqTempObj.assignRecruiters.isSponsorship = true;
                }else{
                    reqTempObj.assignRecruiters.isSponsorship = false;
                }

                if(angular.isDefined(oldRequisitionObject) && angular.isDefined(oldRequisitionObject.assignRecruiters.availableRequisitions)){
                    if(oldRequisitionObject.assignRecruiters.availableRequisitions.length > 0){
                        reqTempObj.assignRecruiters.availableRequisitions = getAvailableRequisitions(requisitionObject);
                        reqTempObj.assignRecruiters.selectedRecReqObjects = [];
                    }
                    if(oldRequisitionObject.assignRecruiters.selectedrecruiters.length > 0){
                        reqTempObj.assignRecruiters.selectedrecruiters = getSelectedRecruiters(requisitionObject);
                    }
                    if(oldRequisitionObject.assignRecruiters.selectedRecReqObjects.length > 0){
                        reqTempObj.assignRecruiters.selectedRecReqObjects = getSelectedRecReqObjects(requisitionObject);
                        reqTempObj.assignRecruiters.availableRequisitions = [];
                    }
                }
                reqTempObj.techAssesmentSelect = {};
                reqTempObj.licensePreferenceDetails = angular.copy(requisitionObject.licensePreferenceDetails);
                if(requisitionObject.techAssessmentMasterDto){
                    reqTempObj.techAssesmentSelect = {
                        id: null,
                        assessmentName: angular.copy(requisitionObject.techAssessmentMasterDto.assessmentName),
                        testURL: null,
                        companyDto: angular.copy(requisitionObject.clientorbu)
                    }
                }else{
                    reqTempObj.techAssesmentSelect = {};
                }
                console.log('reqTemp Obj -', reqTempObj);

                return reqTempObj;
            };



            factory.getRequisitionUIobject = function(requisitionObject,oldRequisitionObject){
                return changeReqObjAfterApi(requisitionObject,oldRequisitionObject);
            };

            factory.assignReqforRecs = function(requisitionObject){
                var deferred = $q.defer();
                genericService.putObject(StorageService.get('baseurl') + 'api/requisition/jobOpeningsAndRecruiter', changeReqObjBeforeApi(requisitionObject)).then(function (data) {
                    //deferred.resolve(changeReqObjAfterApi(angular.copy(data),angular.copy(requisitionObject)));
                    deferred.resolve(changeReqObjAfterApi(angular.copy(requisitionObject),angular.copy(requisitionObject)));
                }, function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            };

            factory.updateRequisitionClientName = function(requisitionObject){
                var deferred = $q.defer();
                genericService.putObject(StorageService.get('baseurl') + 'api/requisitions/postrecruiter/' + requisitionObject.id, changeReqObjBeforeApi(requisitionObject)).then(function (data) {
                    deferred.resolve(changeReqObjAfterApi(angular.copy(data),angular.copy(requisitionObject)));
                }, function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            };

            factory.saveWorkFlowSteps = function(requisitionObject){
                console.log("saveWorkFlowSteps " + requisitionObject.currentJobState);
                var deferred = $q.defer();
                genericService.putObject(StorageService.get('baseurl') + 'api/requisition/setlicensepreferences/' + requisitionObject.id,changeReqObjBeforeApi(requisitionObject)).then(function (data) {
                    //deferred.resolve(changeReqObjAfterApi(angular.copy(data),angular.copy(requisitionObject)));
                    deferred.resolve(angular.copy(requisitionObject));
                }, function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            };

            factory.saveResumesforReq = function(requisitionObject){
                console.log("saveResumesforReq " + requisitionObject.currentJobState);
                var deferred = $q.defer();
                genericService.putObject(StorageService.get('baseurl') + 'api/requisitions/setlicensepreferences/' + requisitionObject.id +'/' + $rootScope.userDetails.company.companyId,requisitionObject.workflowSteps).then(function (data) {
                    deferred.resolve(data);
                }, function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            }

            factory.removeResumesforReq = function(formData){
                var deferred = $q.defer();
                genericService.uploadFile(StorageService.get('baseurl') + 'api/requisitions/removeresume',formData).then(function (data) {
                    deferred.resolve(data);
                }, function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            }

            factory.saveViewResultsData = function(requisitionObject){
                var deferred = $q.defer();
                genericService.putObject(StorageService.get('baseurl') + 'api/requisitions/viewresult/' + requisitionObject.id,changeReqObjBeforeApi(requisitionObject)).then(function (data) {
                    //genericService.putObject(StorageService.get('baseurl') + 'api/requisitions/viewresult/' + requisitionObject.id + '?score=' + requisitionObject.viewResults.dafaultQualifyingMarkers.value).then(function (data) {
                    //console.log('in saveViewResultsData api success : ' + angular.toJson(data));
                    deferred.resolve(changeReqObjAfterApi(angular.copy(data),angular.copy(requisitionObject)));
                }, function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            }


            /**
             * Update Requisition Part
             */

            var changeUpdateReqObjAfterApi  = function (requisitionObject) {
                var reqTempObj = {};

                reqTempObj = {
                    id : angular.copy(requisitionObject.id),
                    createdUserId : angular.copy(requisitionObject.createdUser),
                    uploadReq : {},
                    assignRecruiters : {},
                    jobRequisitionList: angular.copy(requisitionObject.jobRequisitionList),
//		 				uploadResumes: angular.copy(requisitionObject.resumeList),
                    uploadResumes:{},
                    viewResults : {},
                    recruiterList : requisitionObject.recruiterList,
                    canEdit : angular.copy(requisitionObject.canEdit),
                    currentJobState : angular.copy(requisitionObject.currentJobState)
                };

                reqTempObj.uploadReq = {
                    clientorbu : angular.copy(requisitionObject.clientorbu),
                    company : angular.copy(requisitionObject.company),
//		 				uploadedFile : angular.copy(requisitionObject.uploadReq.uploadedFile)
                };

                reqTempObj.assignRecruiters = {
                    totalJobOpenings: angular.copy(requisitionObject.totalJobOpenings),
                    title :angular.copy(requisitionObject.title),
                    jobType: getJobTypeObject(angular.copy(requisitionObject)),
                    jobOpeningsAvailable: angular.copy(requisitionObject.jobOpeningsAvailable),
                    requisitionNumber: angular.copy(requisitionObject.requisitionNumber),
                    sponsorship: getSponsorShipArray(angular.copy(requisitionObject)),
                    jobProfile: angular.copy(requisitionObject.jobProfile),
                    jobOpenings: requisitionObject.jobOpenings,
                    availableRequisitions : getAvailableRequisitions(requisitionObject),
                    selectedrecruiters : getSelectedRecruiters(requisitionObject),
                    selectedRecReqObjects : getSelectedRecReqObjects(requisitionObject),
                    isSponsorship: false
                };

                reqTempObj.techAssesmentSelect = {};
                reqTempObj.licensePreferenceDetails = angular.copy(requisitionObject.licensePreferenceDetails);

                if(requisitionObject.techAssessmentMasterDto !== null){
                    reqTempObj.techAssesmentSelect = {
                        id: null,
                        assessmentName: angular.copy(requisitionObject.techAssessmentMasterDto.assessmentName),
                        testURL: null,
                        companyDto: angular.copy(requisitionObject.clientorbu)
                    }
                }else{
                    reqTempObj.techAssesmentSelect = {};
                }

                reqTempObj.uploadResumes = {
                    resumeSourceType: null,
                    uploadedFiles: {}
                    //uploadedFiles: reqTempObj.uploadResumes.uploadedFiles
                };

                if(requisitionObject.sponsorships && requisitionObject.sponsorships.length > 0){
                    reqTempObj.assignRecruiters.isSponsorship = true;
                }else{
                    reqTempObj.assignRecruiters.isSponsorship = false;
                }
                return reqTempObj;
            };


            factory.getUpdateRequisitionUIobject = function(requisitionObject){
                return changeUpdateReqObjAfterApi(requisitionObject);
            };


            /* factory.assignReqforRecsInUpdate = function(requisitionObject){
                    var deferred = $q.defer();
                    genericService.putObject(StorageService.get('baseurl') + 'api/requisitions/updatejob/', changeReqObjBeforeApi(requisitionObject)).then(function (data) {
                        deferred.resolve(changeReqObjAfterApi(angular.copy(data),angular.copy(requisitionObject)));
                    }, function (error) {
                        deferred.reject(error);
                     });
                    return deferred.promise;
                 }*/
            return factory;
        }
    ]);
